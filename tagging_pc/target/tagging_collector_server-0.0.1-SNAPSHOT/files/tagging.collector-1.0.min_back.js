$.support.cors = true; // IE8~9 support

var tagging={};
tagging.collector=function(tagType,obj){
	
	try {
		var referrer;

		if(obj.referrer == null){
			if(document.referrer!=null){
				referrer=document.referrer;
			}else{
				referrer="";
			}
		}else{
			referrer = obj.referrer;
		}
		
		var deviceId = getTaggingCookie();
		
		var dataObj={
			deviceId:deviceId,
			custNo:obj.custNo,
			tagType:tagType,
			products:obj.products,
			referrer:referrer,
			channel:obj.channel,
			url:window.location.href,
			timestamp:obj.timestamp,
			cust_group:obj.cust_group,
		};
		
		$.ajax({
			type:"POST",
//			url:"http://127.0.0.1:8080/tagging/collectData.json",		// local
			url:"http://61.111.54.88:38080/tagging/collectData.json",       // dev
//			url:"http://112.220.85.26:38080/tagging/collectData.json",	// unpl
//			url:"http://112.220.85.26:38080/tagging/collectData.json",	// real
			dataType:'json',
			data:JSON.stringify(dataObj),
			contentType:'application/json; charset=utf-8',
			timeout:3000,
			success:function(data){},
			error:function(jqXHR,testStatus,errorThrown){}
		})
	} catch(err) {
	}
	
	function getTaggingCookie(){
		try {
			var bd_search = "tagging";
			var bd_i, bd_x, bd_y, bd_ARRcookies = document.cookie.split(';');
			
			for(var bd_i=0; bd_i < bd_ARRcookies.length; bd_i++) {
				bd_x= bd_ARRcookies[bd_i].substr(0, bd_ARRcookies[bd_i].indexOf('='));
				bd_y= bd_ARRcookies[bd_i].substr(bd_ARRcookies[bd_i].indexOf('=') + 1);
				bd_x = bd_x.replace(/^\s+|\s+$/g, '');
				if(bd_x == bd_search) {
					return unescape(bd_y);
				}
			}
			
			var bd_exdate = new Date();
			bd_exdate.setTime(bd_exdate.getTime() + 1000 * 3600 * 24 * 365);
			var bd_expires = "; expires="+bd_exdate.toGMTString();
			var bd_uuid = getUuid();
			var bd_cookie_value = escape(bd_uuid) + bd_expires + "; path=/";
			
			document.cookie = "tagging=" + bd_cookie_value;
			
			return bd_uuid;
		} catch(err) {
		}
		
		return "";
	}
	
	function taggingGuid() {
		try {
			return ((1 + Math.random()) * 0x10000 | 0).toString(16).substring(1);
		} catch(err) {
		}
	}
	
	function getUuid(){
		try {
			return taggingGuid() + taggingGuid() + '-' + taggingGuid() +'-' + taggingGuid() + '-' + taggingGuid() + '-' + taggingGuid() + taggingGuid() + taggingGuid();
		} catch(err) {
		}
	}
	
};
