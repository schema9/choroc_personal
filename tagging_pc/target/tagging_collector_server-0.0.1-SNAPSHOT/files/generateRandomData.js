/**
 * 
 */
var getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

var getRandom10 = function(min, max) {
  return getRandomInt(min / 10, max / 10) * 10;
}

var getRandomChannel = function() {
	if (((Math.floor(Math.random() * 10)) % 2) == 0) {
		return "P";
	} else {
		return "M";
	}
}

var getRandomMemberId = function() {
	
	var memberIdList = ['I999999', 'I100000', 'I101010', 'I200000', 'I202020'];
	
	return memberIdList[getRandomInt(0, 4)];
}


var getRandomItemInfo = function() {
	
	var ItemList = [
		{itemId: '000011', itemName: '무농약무우', price: '2500', thumb: 'http://attach.choroki.com/img2/1/img1/000011.JPG', cateA1Cd: '01', cateA2Cd: '0102', cateA3Cd: '010213', cateA4Cd: '010213007'},
		{itemId: '000019', itemName: '태안자염', price: '9900', thumb: 'http://attach.choroki.com/img2/1/img1/000019.jpg', cateA1Cd: '07', cateA2Cd: '0702', cateA3Cd: '070202', cateA4Cd: '070202001'},
		{itemId: '000027', itemName: '영광굴비 딱돔1호', price: '9900', thumb: 'http://attach.choroki.com/img2/1/img1/000027.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699002'},
		{itemId: '000028', itemName: '영광굴비 딱돔2호', price: '9900', thumb: 'http://attach.choroki.com/img2/1/img1/000028.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699002'},
		{itemId: '000029', itemName: '영광굴비 딱돔4호', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000029.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699002'},
		{itemId: '000030', itemName: '통영참전복세트(대)', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000030.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010601', cateA4Cd: '010601009'},
		{itemId: '000031', itemName: '통영참전복세트(중)', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000031.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010601', cateA4Cd: '010601009'},
		{itemId: '000032', itemName: '제주건옥돔 2kg', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000032.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010601', cateA4Cd: '010601001'},
		{itemId: '000033', itemName: '제주건옥돔3kg', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000033.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010601', cateA4Cd: '010601001'},
		{itemId: '000038', itemName: '돌김세트', price: '22000', thumb: 'http://attach.choroki.com/img2/1/img1/000038.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699004'},
		{itemId: '000039', itemName: '김혼합세트1호(중단)', price: '37000', thumb: 'http://attach.choroki.com/img2/1/img1/000039.JPG', cateA1Cd: '02', cateA2Cd: '0202', cateA3Cd: '020299', cateA4Cd: '020299001'},
		{itemId: '000040', itemName: '김혼합세트2호', price: '38000', thumb: 'http://attach.choroki.com/img2/1/img1/000040.JPG', cateA1Cd: '02', cateA2Cd: '0202', cateA3Cd: '020299', cateA4Cd: '020299001'},
		{itemId: '000041', itemName: '고급국물멸치세트(중단)', price: '37000', thumb: 'http://attach.choroki.com/img2/1/img1/000041.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699003'},
		{itemId: '000042', itemName: '남해바다멸치세트(혼합)', price: '55000', thumb: 'http://attach.choroki.com/img2/1/img1/000042.jpg', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699003'},
		{itemId: '000042', itemName: '남해바다멸치세트(혼합)', price: '55000', thumb: 'http://attach.choroki.com/img2/1/img1/000042.jpg', cateA1Cd: '06', cateA2Cd: '0602', cateA3Cd: '060203', cateA4Cd: '060203002'},
		{itemId: '000043', itemName: '울릉도남양동오징어1호', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000043.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010603', cateA4Cd: '010603003'},
		{itemId: '000044', itemName: '울릉도남양동오징어2호', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000044.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010603', cateA4Cd: '010603003'},
		{itemId: '000045', itemName: '울릉도남양동오징어3호', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000045.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010603', cateA4Cd: '010603003'},
		{itemId: '000046', itemName: '울릉도남양동오징어4호', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000046.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010603', cateA4Cd: '010603003'},
		{itemId: '000047', itemName: '실속3종세트', price: '40500', thumb: 'http://attach.choroki.com/img2/1/img1/000047.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699003'},
		{itemId: '000048', itemName: '건해산물세트', price: '5500', thumb: 'http://attach.choroki.com/img2/1/img1/000048.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699004'},
		{itemId: '000049', itemName: '고급멸치세트', price: '2500', thumb: 'http://attach.choroki.com/img2/1/img1/000049.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699003'},
		{itemId: '000050', itemName: '남해바다멸치세트(혼합5종)', price: '65000', thumb: 'http://attach.choroki.com/img2/1/img1/000050.jpg', cateA1Cd: '06', cateA2Cd: '0602', cateA3Cd: '060203', cateA4Cd: '060203002'},
		{itemId: '000051', itemName: '고급안주멸치세트(중단)', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000051.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699003'},
		{itemId: '000052', itemName: '울릉도남양동 건오징어 1.2', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000052.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010603', cateA4Cd: '010603003'},
		{itemId: '000053', itemName: '울릉도 남양동 건오징어', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000053.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010603', cateA4Cd: '010603003'},
		{itemId: '000054', itemName: '울릉도남양동 건오징어 2.0', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000054.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010603', cateA4Cd: '010603003'},
		{itemId: '000055', itemName: '울릉도남양동덜마른 오징어2.0', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000055.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010603', cateA4Cd: '010603003'},
		{itemId: '000056', itemName: '약간덜마른말랑말랑울릉도오징어', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000056.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010603', cateA4Cd: '010603003'},
		{itemId: '000057', itemName: '약간덜마른말랑말랑오징어 ', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000057.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010603', cateA4Cd: '010603003'},
		{itemId: '000062', itemName: '추자도 죽염참굴비 1호', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000062.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699002'},
		{itemId: '000063', itemName: '추자도죽염참굴비 2호', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000063.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699002'},
		{itemId: '000064', itemName: '추자도 죽염참굴비 3호', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000064.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699002'},
		{itemId: '000067', itemName: '멸치세트(특판)', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000067.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699003'},
		{itemId: '000070', itemName: '네오피시1호', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000070.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010601', cateA4Cd: '010601001'},
		{itemId: '000071', itemName: '네오피시2호', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000071.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010601', cateA4Cd: '010601001'},
		{itemId: '000072', itemName: '네오피시 3호', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000072.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010601', cateA4Cd: '010601001'},
		{itemId: '000073', itemName: '네오 참굴비 20미', price: '10000', thumb: 'http://attach.choroki.com/img2/1/img1/000073.JPG', cateA1Cd: '01', cateA2Cd: '0106', cateA3Cd: '010699', cateA4Cd: '010699002'},
		{itemId: '000095', itemName: '친환경초록사과', price: '8900', thumb: 'http://attach.choroki.com/img2/1/img1/000095.jpg', cateA1Cd: '06', cateA2Cd: '0601', cateA3Cd: '060103', cateA4Cd: '060103001'},
		{itemId: '000099', itemName: '무농약이상_블루베리', price: '5500', thumb: 'http://attach.choroki.com/img2/1/img1/000099.jpg', cateA1Cd: '06', cateA2Cd: '0601', cateA3Cd: '060103', cateA4Cd: '060103004'},
		{itemId: '000101', itemName: '무농약방울토마토', price: '2900', thumb: 'http://attach.choroki.com/img2/1/img1/000101.jpg', cateA1Cd: '06', cateA2Cd: '0601', cateA3Cd: '060103', cateA4Cd: '060103005'},
		{itemId: '000102', itemName: '[산지직송]무농약찰토마토', price: '10900', thumb: 'http://attach.choroki.com/img2/1/img1/000102.jpg', cateA1Cd: '06', cateA2Cd: '0601', cateA3Cd: '060103', cateA4Cd: '060103006'},
	];

	return ItemList[getRandomInt(0, 30)];
}