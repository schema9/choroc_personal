package com.choroc.logging.tagging.server.vo;

public class ProductsVO {

	// product variable ( 상품정보 VO )
	private String itemId = ""; 	
	private String itemName = ""; 
	private String search = ""; 	
	private String price = ""; 	
	private String qty = ""; 		
	private String area = ""; 	
	private String thumb = ""; 	
	private String cateA1Cd = ""; 	
	private String cateA2Cd = ""; 	
	private String cateA3Cd = ""; 	
	private String cateA4Cd = ""; 	
	private String cateB1Cd = ""; 	
	private String cateB2Cd = ""; 	
	private String cateB3Cd = ""; 	
	private String cateB4Cd = ""; 	
	private String brandCd = ""; 	
	private String ItemType = "";
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getThumb() {
		return thumb;
	}
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}
	public String getCateA1Cd() {
		return cateA1Cd;
	}
	public void setCateA1Cd(String cateA1Cd) {
		this.cateA1Cd = cateA1Cd;
	}
	public String getCateA2Cd() {
		return cateA2Cd;
	}
	public void setCateA2Cd(String cateA2Cd) {
		this.cateA2Cd = cateA2Cd;
	}
	public String getCateA3Cd() {
		return cateA3Cd;
	}
	public void setCateA3Cd(String cateA3Cd) {
		this.cateA3Cd = cateA3Cd;
	}
	public String getCateA4Cd() {
		return cateA4Cd;
	}
	public void setCateA4Cd(String cateA4Cd) {
		this.cateA4Cd = cateA4Cd;
	}
	public String getCateB1Cd() {
		return cateB1Cd;
	}
	public void setCateB1Cd(String cateB1Cd) {
		this.cateB1Cd = cateB1Cd;
	}
	public String getCateB2Cd() {
		return cateB2Cd;
	}
	public void setCateB2Cd(String cateB2Cd) {
		this.cateB2Cd = cateB2Cd;
	}
	public String getCateB3Cd() {
		return cateB3Cd;
	}
	public void setCateB3Cd(String cateB3Cd) {
		this.cateB3Cd = cateB3Cd;
	}
	public String getCateB4Cd() {
		return cateB4Cd;
	}
	public void setCateB4Cd(String cateB4Cd) {
		this.cateB4Cd = cateB4Cd;
	}
	public String getBrandCd() {
		return brandCd;
	}
	public void setBrandCd(String brandCd) {
		this.brandCd = brandCd;
	}
	public String getItemType() {
		return ItemType;
	}
	public void setItemType(String itemType) {
		ItemType = itemType;
	} 
}
