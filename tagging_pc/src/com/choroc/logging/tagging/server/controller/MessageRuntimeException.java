package com.choroc.logging.tagging.server.controller;

public class MessageRuntimeException extends RuntimeException {
	private static final long serialVersionUID = -1383947438345686274L;

	public MessageRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public MessageRuntimeException(String message) {
		super(message);
	}
	
	public MessageRuntimeException(Throwable cause){
		super(cause);
	}

}
