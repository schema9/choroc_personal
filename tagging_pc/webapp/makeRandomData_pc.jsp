<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%--
	// // 디바이스 아이디 쿠키 설정 
	Cookie cookie = new Cookie("p_deviceId", "디바이스 아이디");
	cookie.setMaxAge(60 * 60 * 60);
	cookie.setPath("/");
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="files/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="files/tagging.collector-1.0.min.pc.js"></script>
<script type="text/javascript" src="files/generateRandomData.js"></script>


<title>제품 구매페이지</title>
</head>

<body>
<script>
	// 제품추천서비스
	var tagging_data = {};
	var tagging_data_header = {};
	var tagging_data_arr = new Array();
	
	var itemInfo = getRandomItemInfo();
	
	tagging_data = {
		  itemId	: itemInfo.itemId  		    // 제품코드
		, itemName  : itemInfo.itemName 		// 제품명
		, price     : itemInfo.price 	     	// 가격                        
		, qty       : getRandomInt(1, 100)	    // 수량                 
		, thumb     : itemInfo.thumb    		// 상품 이미지 URL              
		, cateA1Cd  : itemInfo.cateA1Cd 		// 카테고리대           
		, cateA2Cd  : itemInfo.cateA2Cd 		// 카태고리중           
		, cateA3Cd  : itemInfo.cateA3Cd 		// 카테고리소           
		, cateA4Cd  : itemInfo.cateA4Cd	        // 카테고리세	                 
		, itemType  : "A" 						// 아이템 타입(A:일반, B:백화점, C:TV, D:데이터 방송)
	};
	
	tagging_data_arr.push(tagging_data);
	
	tagging_data_header = {
		  products 	: tagging_data_arr			
		, custNo   	: getRandomMemberId()     	// 고객코드
		, cust_group: getRandomInt(1, 5)      	// 고객그룹	
		, channel  	: "P"						// 체널 구분
		, referrer 	: "http://www.referrer.com" // 이전페이지
		, url      	: "http://test.do"        	// URL
		, ageGrp 	: getRandom10(10, 60) 	   	// 연령대
		, campatinRecordCd : "999999"			// 캠페인추적코드 
	};
	
	// 태그 종류
	// RC_VIEW:상품상세, RC_CART:장바구니, RC_ZZIM:찜, RC_BUY:구매, RC_LOGIN:구매, RC_SEARCH:검색, RC_NO_SEARCH:실패 검색
	tagging.collector("RC_VIEW", tagging_data_header);   
	
</script>
	
	<h2>check your logfile..</h2>
	
</body>
</html>