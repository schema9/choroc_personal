package com.choroc.batch.util;


import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	public static String getCurrentDateWithPattern(String pattern) {
		return new SimpleDateFormat(pattern).format(new Date());
	}
}
