package com.choroc.batch.dao;

import java.sql.SQLException;
import java.util.Map;

import com.choroc.batch.dto.ApiInfo;

public interface TagHhChannelStatDao {
	
	Map<String,Object> tagHhChannelStatScheduler(ApiInfo apiParam) throws SQLException;
	
	
}
