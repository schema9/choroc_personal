package com.choroc.batch.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.choroc.batch.dto.ApiInfo;


/**
 * TODO > 맵퍼파일 - mappers\batch\tagStatMapper.xml 
 * 파일명   > TagHhChannelStatDaoImpl.java
 * 작성일   > 2019. 11. 20.
 * 작성자   > Copyright CCmediaService
 * 수정일   > 
 * 수정자   > 
 */
@Repository
public class TagHhChannelStatDaoImpl implements TagHhChannelStatDao {

	@Inject
	private SqlSession sqlSession;
	
	private static final String SQL_PATH_AREA_PREVIEW = "batch.tagHhChannelStatMapper.";
	
	public Map<String,Object> tagHhChannelStatScheduler(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();
		List selectList = null;
		int totalCnt = 0;
		String returnMsg = "";
				
		System.out.println("TagHhChannelStatDaoImpl.tagHhChannelStatScheduler :::::::::::::::::::::::::::::::::::::::::::: ");
		
		selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "tagHhChannelStatScheduler", apiParam);
		
//		selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemViewViewAll", apiParam);
		
		totalCnt = selectList.size();
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", totalCnt);
		mapObjTotal.put("returnMsg", returnMsg);
		return mapObjTotal;
	}

}
