package com.choroc.batch.dao;

import java.sql.SQLException;
import java.util.Map;

import com.choroc.batch.dto.ApiInfo;

public interface TagHhStatDao {
	
	Map<String,Object> tagHhStatScheduler(ApiInfo apiParam) throws SQLException;
	
	
}
