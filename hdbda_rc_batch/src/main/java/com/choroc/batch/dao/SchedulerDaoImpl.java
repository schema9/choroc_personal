package com.choroc.batch.dao;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.choroc.batch.dto.Scheduler;
import com.choroc.batch.dto.SchedulerTask;

/**
 * TODO >
 * 파일명   > SchedulerDaoImpl.java
 * 작성일   > 2018. 12. 4.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
@Repository
public class SchedulerDaoImpl implements SchedulerDao {

	@Inject
	private SqlSession sqlSession;
	private static final String namespace = "system.SchedulerMapper";
	
	
	/* 잡 수동 실행 */
	@Override
	public int insertRedo(SchedulerTask task) throws Exception {
		return sqlSession.insert(namespace + ".insertTaskInfo", task);
	}
	
	/*  태스크 기록 수정 */
	@Override
	public int updateJobHisStatus(SchedulerTask task) throws Exception {
		return sqlSession.update(namespace + ".updateJobHisStatus", task); 
	}
	
	@Override
	public int deleteAll(Scheduler scheduler) throws Exception {
		return sqlSession.delete(namespace + ".delete", scheduler); 
	}
	
}
