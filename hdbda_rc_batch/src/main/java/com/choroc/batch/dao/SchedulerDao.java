package com.choroc.batch.dao;

import com.choroc.batch.dto.Scheduler;
import com.choroc.batch.dto.SchedulerTask;

/**
 * TODO > 스케줄러 DAO
 * 파일명   > SchedulerDao.java
 * 작성일   > 2018. 12. 4.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public interface SchedulerDao {

	/* 잡 수동 실행 */
	public int insertRedo(SchedulerTask task) throws Exception;
	
	/* 스케줄러 태스크 기록 수정 */
	public int updateJobHisStatus(SchedulerTask task) throws Exception;
	
	/* 잡 삭제 */
	public int deleteAll(Scheduler scheduler) throws Exception;
	
}
