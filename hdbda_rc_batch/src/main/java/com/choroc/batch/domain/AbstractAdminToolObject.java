package com.choroc.batch.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public abstract class AbstractAdminToolObject {
	
	private String useYn;

	private String delYn;
	
	private String createId;

	private Date createDt;
	
	private String modifyId;

	private Date modifyDt;
	

	/**
	 * @return the useYn
	 */
	public String getUseYn() {
		return useYn;
	}

	/**
	 * @param useYn the useYn to set
	 */
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	/**
	 * @return the delYn
	 */
	public String getDelYn() {
		return delYn;
	}

	/**
	 * @param delYn the delYn to set
	 */
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	/**
	 * @return the createId
	 */
	public String getCreateId() {
		return createId == null ? "NONE" : createId;
	}

	/**
	 * @param createId the createId to set
	 */
	public void setCreateId(String createId) {
		this.createId = createId;
	}

	/**
	 * @return the modifyId
	 */
	public String getModifyId() {
		return modifyId == null ? "NONE" : modifyId;
	}

	/**
	 * @param modifyId the modifyId to set
	 */
	public void setModifyId(String modifyId) {
		this.modifyId = modifyId;
	}


	/**
	 * @return the createDt
	 */
	public Date getCreateDt() {
		return createDt;
	}

	/**
	 * @param createDt the createDt to set
	 */
	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	/**
	 * @return the modifyDt
	 */
	public Date getModifyDt() {
		return modifyDt;
	}

	/**
	 * @param modifyDt the modifyDt to set
	 */
	public void setModifyDt(Date modifyDt) {
		this.modifyDt = modifyDt;
	}

	/**
	 * 해당 객체의 toString 오버라이드.
	 * 
	 * @return 객체의 각 프로퍼티와 해당 값을 text로 제공
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	/**
	 * 해당 객체의 equals 오버라이드.
	 * 
	 * @param obj 비교 대상 객체
	 * @return 비교 대상 객체와 각 프로퍼티값이 동일한 경우 <code>true</code>반환
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	/**
	 * 해당 객체의 hashCode 오버라이드.
	 * 
	 * @return the int
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
