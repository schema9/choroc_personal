package com.choroc.batch.domain;

import java.io.Serializable;

/**
 * TODO > 스케줄러  파라미터
 * 파일명   > SchedulerParam.java
 * 작성일   > 2018. 12. 4.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public class SchedulerParam extends CommonAdminToolParam implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int totalCount;
	private String searchText;
	private String searchOptionYn;
	private String searchOptionNm;
	private String searchOptionStat;
	private int jobId;
	
	
	public String getSearchOptionStat() {
		return searchOptionStat;
	}

	public void setSearchOptionStat(String searchOptionStat) {
		this.searchOptionStat = searchOptionStat;
	}

	public String getSearchOptionYn() {
		return searchOptionYn;
	}

	public void setSearchOptionYn(String searchOptionYn) {
		this.searchOptionYn = searchOptionYn;
	}

	public String getSearchOptionNm() {
		return searchOptionNm;
	}

	public void setSearchOptionNm(String searchOptionNm) {
		this.searchOptionNm = searchOptionNm;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {		
		return "SchedulerParam:" + "[searchOptionYn:" + searchOptionYn + " searchOptionNm:" + searchOptionNm + " searchText:" + searchText +"]";
		//return super.toString();
	}
	
}
