package com.choroc.batch.dto;

import java.util.Date;

/**
 * TODO > 스케줄러
 * 파일명   > Scheduler.java
 * 작성일   > 2018. 12. 11.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public class Scheduler {
	
	private String 	flag;
	private int 	job_id;
	private String 	agent_id;
	private String 	job_desc;
	private String 	cron_expr;
	private String 	exec_prog;
	private String 	param_str;
	private String 	option_str;
	private String 	etl_tgt_sys;
	private String 	etl_tgt_tbl;
	private String 	lst_chg_dat_cond;
	private String 	acvt_yn;
	private String 	reg_id;
	private String 	udt_id;
	private String 	udt_ddtm;
	private char 	del_yn;
	private String 	del_id;
	private Date 	del_ddtm;
	private String 	job_nm;
	private String 	job_type;
	private String 	reg_ddtm;
	
//	private String 	stts_cd;
	private String	sts_cd;
//	private String 	end_ddtm;
	private String	to_time;
		
	private String 	table_nm;
	
	private String 	excute_ddtm;
	
	
	// 에이전트 관련
	private String 	agent_ip;
	private String 	agent_host;
	
	// 메타 테이블 관련
	private String 	load_type;
	private String 	chg_dat_bas_col;
	private String 	chg_dat_cond_ptrn;
	
	// 스키마 단위 ETL 수동실행 관련
	private String[] etl_tgt_syss;

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public int getJob_id() {
		return job_id;
	}

	public void setJob_id(int job_id) {
		this.job_id = job_id;
	}

	public String getAgent_id() {
		return agent_id;
	}

	public void setAgent_id(String agent_id) {
		this.agent_id = agent_id;
	}

	public String getJob_desc() {
		return job_desc;
	}

	public void setJob_desc(String job_desc) {
		this.job_desc = job_desc;
	}

	public String getCron_expr() {
		return cron_expr;
	}

	public void setCron_expr(String cron_expr) {
		this.cron_expr = cron_expr;
	}

	public String getExec_prog() {
		return exec_prog;
	}

	public void setExec_prog(String exec_prog) {
		this.exec_prog = exec_prog;
	}

	public String getParam_str() {
		return param_str;
	}

	public void setParam_str(String param_str) {
		this.param_str = param_str;
	}

	public String getOption_str() {
		return option_str;
	}

	public void setOption_str(String option_str) {
		this.option_str = option_str;
	}

	public String getEtl_tgt_sys() {
		return etl_tgt_sys;
	}

	public void setEtl_tgt_sys(String etl_tgt_sys) {
		this.etl_tgt_sys = etl_tgt_sys;
	}

	public String getEtl_tgt_tbl() {
		return etl_tgt_tbl;
	}

	public void setEtl_tgt_tbl(String etl_tgt_tbl) {
		this.etl_tgt_tbl = etl_tgt_tbl;
	}

	public String getLst_chg_dat_cond() {
		return lst_chg_dat_cond;
	}

	public void setLst_chg_dat_cond(String lst_chg_dat_cond) {
		this.lst_chg_dat_cond = lst_chg_dat_cond;
	}

	public String getAcvt_yn() {
		return acvt_yn;
	}

	public void setAcvt_yn(String acvt_yn) {
		this.acvt_yn = acvt_yn;
	}

	public String getReg_id() {
		return reg_id;
	}

	public void setReg_id(String reg_id) {
		this.reg_id = reg_id;
	}

	public String getUdt_id() {
		return udt_id;
	}

	public void setUdt_id(String udt_id) {
		this.udt_id = udt_id;
	}

	public String getUdt_ddtm() {
		return udt_ddtm;
	}

	public void setUdt_ddtm(String udt_ddtm) {
		this.udt_ddtm = udt_ddtm;
	}

	public char getDel_yn() {
		return del_yn;
	}

	public void setDel_yn(char del_yn) {
		this.del_yn = del_yn;
	}

	public String getDel_id() {
		return del_id;
	}

	public void setDel_id(String del_id) {
		this.del_id = del_id;
	}

	public Date getDel_ddtm() {
		return del_ddtm;
	}

	public void setDel_ddtm(Date del_ddtm) {
		this.del_ddtm = del_ddtm;
	}

	public String getJob_nm() {
		return job_nm;
	}

	public void setJob_nm(String job_nm) {
		this.job_nm = job_nm;
	}

	public String getJob_type() {
		return job_type;
	}

	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}

	public String getReg_ddtm() {
		return reg_ddtm;
	}

	public void setReg_ddtm(String reg_ddtm) {
		this.reg_ddtm = reg_ddtm;
	}

	public String getSts_cd() {
		return sts_cd;
	}

	public void setSts_cd(String sts_cd) {
		this.sts_cd = sts_cd;
	}

	public String getTo_time() {
		return to_time;
	}

	public void setTo_time(String to_time) {
		this.to_time = to_time;
	}

	public String getTable_nm() {
		return table_nm;
	}

	public void setTable_nm(String table_nm) {
		this.table_nm = table_nm;
	}

	public String getExcute_ddtm() {
		return excute_ddtm;
	}

	public void setExcute_ddtm(String excute_ddtm) {
		this.excute_ddtm = excute_ddtm;
	}

	public String getAgent_ip() {
		return agent_ip;
	}

	public void setAgent_ip(String agent_ip) {
		this.agent_ip = agent_ip;
	}

	public String getAgent_host() {
		return agent_host;
	}

	public void setAgent_host(String agent_host) {
		this.agent_host = agent_host;
	}

	public String getLoad_type() {
		return load_type;
	}

	public void setLoad_type(String load_type) {
		this.load_type = load_type;
	}

	public String getChg_dat_bas_col() {
		return chg_dat_bas_col;
	}

	public void setChg_dat_bas_col(String chg_dat_bas_col) {
		this.chg_dat_bas_col = chg_dat_bas_col;
	}

	public String getChg_dat_cond_ptrn() {
		return chg_dat_cond_ptrn;
	}

	public void setChg_dat_cond_ptrn(String chg_dat_cond_ptrn) {
		this.chg_dat_cond_ptrn = chg_dat_cond_ptrn;
	}

	public String[] getEtl_tgt_syss() {
		return etl_tgt_syss;
	}

	public void setEtl_tgt_syss(String[] etl_tgt_syss) {
		this.etl_tgt_syss = etl_tgt_syss;
	}
	
}
