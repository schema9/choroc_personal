package com.choroc.batch.dto;

public class SchedulerTask {
	
	private String task_id;
	private String job_id;
	private String cron_expr;
	private String exec_prog;
	private String param_str;
	private String option_str;
	private String etl_tgt_sys;
	private String etl_tgt_tbl;
	private String lst_chg_dat_cond;
//	private String reg_ddtm;
//	private String stts_cd;
//	private String stts_dtl;
//	private String strt_ddtm;
//	private String end_ddtm;
	private String reg_time;
	private String sts_cd;
	private String sts_dtl_cd;
	private String fr_time;
	private String to_time;
	private String job_type;
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}
	public String getCron_expr() {
		return cron_expr;
	}
	public void setCron_expr(String cron_expr) {
		this.cron_expr = cron_expr;
	}
	public String getExec_prog() {
		return exec_prog;
	}
	public void setExec_prog(String exec_prog) {
		this.exec_prog = exec_prog;
	}
	public String getParam_str() {
		return param_str;
	}
	public void setParam_str(String param_str) {
		this.param_str = param_str;
	}
	public String getOption_str() {
		return option_str;
	}
	public void setOption_str(String option_str) {
		this.option_str = option_str;
	}
	public String getEtl_tgt_sys() {
		return etl_tgt_sys;
	}
	public void setEtl_tgt_sys(String etl_tgt_sys) {
		this.etl_tgt_sys = etl_tgt_sys;
	}
	public String getEtl_tgt_tbl() {
		return etl_tgt_tbl;
	}
	public void setEtl_tgt_tbl(String etl_tgt_tbl) {
		this.etl_tgt_tbl = etl_tgt_tbl;
	}
	public String getLst_chg_dat_cond() {
		return lst_chg_dat_cond;
	}
	public void setLst_chg_dat_cond(String lst_chg_dat_cond) {
		this.lst_chg_dat_cond = lst_chg_dat_cond;
	}
	public String getReg_time() {
		return reg_time;
	}
	public void setReg_time(String reg_time) {
		this.reg_time = reg_time;
	}
	public String getSts_cd() {
		return sts_cd;
	}
	public void setSts_cd(String sts_cd) {
		this.sts_cd = sts_cd;
	}
	public String getSts_dtl_cd() {
		return sts_dtl_cd;
	}
	public void setSts_dtl_cd(String sts_dtl_cd) {
		this.sts_dtl_cd = sts_dtl_cd;
	}
	public String getFr_time() {
		return fr_time;
	}
	public void setFr_time(String fr_time) {
		this.fr_time = fr_time;
	}
	public String getTo_time() {
		return to_time;
	}
	public void setTo_time(String to_time) {
		this.to_time = to_time;
	}
	public String getJob_type() {
		return job_type;
	}
	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}
	
}
