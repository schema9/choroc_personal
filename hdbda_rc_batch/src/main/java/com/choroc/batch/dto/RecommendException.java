package com.choroc.batch.dto;

public class RecommendException {
	
	
	private String sortation;
	private String content_cd;
	private String create_dt;
	private String create_id;
	
	
	public String getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(String create_dt) {
		this.create_dt = create_dt;
	}
	public String getCreate_id() {
		return create_id;
	}
	public void setCreate_id(String create_id) {
		this.create_id = create_id;
	}
	public String getSortation() {
		return sortation;
	}
	public void setSortation(String sortation) {
		this.sortation = sortation;
	}
	public String getContent_cd() {
		return content_cd;
	}
	public void setContent_cd(String content_cd) {
		this.content_cd = content_cd;
	}
	

}
