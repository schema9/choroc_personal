package com.choroc.batch.dto;

import java.io.Serializable;


import com.choroc.batch.domain.AbstractAdminToolObject;
import com.choroc.batch.dto.RecommendException;

public class ApiInfo extends AbstractAdminToolObject implements Serializable {

	public static final String getAlgrthCd = null;
	// 기본세팅
	private String userId; // USER_ID; 사용자아이디;
	private int roleId;
	
	//공용 조회조건 관련
	private String batchExcuteTime;
	private String batchStartTime;
	private String batchEndTime;
	
	private int totalCount;
	private String firstIndex;
	private String lastIndex;
	private int firstIndexInt;
	private int lastIndexInt;
	private String hh;
	private String firstYmd;
	private String lastYmd;
	private String listOrder;
	private String searchText;
	private String searchOption;
	private String searchOption1;
	private String searchOption2;
	private String searchOption3;
	private String searchService;
	private int searchTextInt;
	private int dsplyNo;
	private int dsplyCnt;
	
	//테이블 정보 관련
	private String etlStatus;		//api파라메터
	private String api;				//api파라메터
	private String productCd;
	private String categoryCd;
	private String categoryNm;
	private String itemId;
	private String itemId2;
	private String tableNm;
	private String[] algrthCd;
	
	// 메인대시보드 관련
	private String date;
	private String dateType;
	
	// 추천 제외관리 관련
	private String searchOptionNm;
	private String recordCountPerPage;
	private String currentPageNo;
    private String sortation;
    private String content_cd;
    private RecommendException rc_except;

    //네이버 검색키워드 관련
    private String callDt;
    private String catCd;
    private String kwdType;
    
	

	public String getBatchExcuteTime() {
		return batchExcuteTime;
	}

	public void setBatchExcuteTime(String batchExcuteTime) {
		this.batchExcuteTime = batchExcuteTime;
	}
	public String getBatchStartTime() {
		return batchStartTime;
	}

	public void setBatchStartTime(String batchStartTime) {
		this.batchStartTime = batchStartTime;
	}
	public String getBatchEndTime() {
		return batchEndTime;
	}

	public void setBatchEndTime(String batchEndTime) {
		this.batchEndTime = batchEndTime;
	}
	

	public RecommendException getRc_except() {
		return rc_except;
	}

	public void setRc_except(RecommendException rc_except) {
		this.rc_except = rc_except;
	}

	public String getContent_cd() {
		return content_cd;
	}

	public void setContent_cd(String content_cd) {
		this.content_cd = content_cd;
	}

	public String getSortation() {
		return sortation;
	}

	public void setSortation(String sortation) {
		this.sortation = sortation;
	}

	public int getDsplyNo() {
		return dsplyNo;
	}

	public void setDsplyNo(int dsplyNo) {
		this.dsplyNo = dsplyNo;
	}

	public int getDsplyCnt() {
		return dsplyCnt;
	}

	public void setDsplyCnt(int dsplyCnt) {
		this.dsplyCnt = dsplyCnt;
	}

	public String getDateType() {
		return dateType;
	}

	public void setDateType(String dateType) {
		this.dateType = dateType;
	}

	public String getSearchOptionNm() {
		return searchOptionNm;
	}

	public void setSearchOptionNm(String searchOptionNm) {
		this.searchOptionNm = searchOptionNm;
	}

	public String getRecordCountPerPage() {
		return recordCountPerPage;
	}

	public void setRecordCountPerPage(String recordCountPerPage) {
		this.recordCountPerPage = recordCountPerPage;
	}

	public String getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(String currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getEtlStatus() {
		return etlStatus;
	}

	public void setEtlStatus(String etlStatus) {
		this.etlStatus = etlStatus;
	}

	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}

	public String getProductCd() {
		return productCd;
	}

	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}

	public String getCategoryCd() {
		return categoryCd;
	}

	public void setCategoryCd(String categoryCd) {
		this.categoryCd = categoryCd;
	}

	public String getCategoryNm() {
		return categoryNm;
	}

	public void setCategoryNm(String categoryNm) {
		this.categoryNm = categoryNm;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getListOrder() {
		return listOrder;
	}

	public void setListOrder(String listOrder) {
		this.listOrder = listOrder;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}

	public String getSearchService() {
		return searchService;
	}

	public void setSearchService(String searchService) {
		this.searchService = searchService;
	}

	public String getSearchOption1() {
		return searchOption1;
	}

	public void setSearchOption1(String searchOption1) {
		this.searchOption1 = searchOption1;
	}

	public String getSearchOption2() {
		return searchOption2;
	}

	public void setSearchOption2(String searchOption2) {
		this.searchOption2 = searchOption2;
	}

	public String getSearchOption3() {
		return searchOption3;
	}

	public void setSearchOption3(String searchOption3) {
		this.searchOption3 = searchOption3;
	}

	public String getFirstIndex() {
		return firstIndex;
	}

	public void setFirstIndex(String firstIndex) {
		this.firstIndex = firstIndex;
	}

	public String getLastIndex() {
		return lastIndex;
	}

	public void setLastIndex(String lastIndex) {
		this.lastIndex = lastIndex;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public int getSearchTextInt() {
		return searchTextInt;
	}

	public void setSearchTextInt(int searchTextInt) {
		this.searchTextInt = searchTextInt;
	}

	public String getTableNm() {
		return tableNm;
	}

	public void setTableNm(String tableNm) {
		this.tableNm = tableNm;
	}

	public String getFirstYmd() {
		return firstYmd;
	}

	public void setFirstYmd(String firstYmd) {
		this.firstYmd = firstYmd;
	}

	public String getLastYmd() {
		return lastYmd;
	}

	public void setLastYmd(String lastYmd) {
		this.lastYmd = lastYmd;
	}

	public String getItemId2() {
		return itemId2;
	}

	public void setItemId2(String itemId2) {
		this.itemId2 = itemId2;
	}
	
	public String getHh() {
		return hh;
	}

	public void setHh(String hh) {
		this.hh = hh;
	}

	public String getDateFormat(String date) {
		String year = date.substring(0,4);
		String month = date.substring(4, 6);
		String day = date.substring(6, 8);
		String formatDate = year + "-" + month + "-" + day;
		return formatDate;
	}

	public String[] getAlgrthCd() {
		return algrthCd;
	}

	public void setAlgrthCd(String[] algrthCd) {
		this.algrthCd = algrthCd;
	}

	public String getCallDt() {
		return callDt;
	}

	public void setCallDt(String callDt) {
		this.callDt = callDt;
	}

	public String getCatCd() {
		return catCd;
	}

	public void setCatCd(String catCd) {
		this.catCd = catCd;
	}

	public String getKwdType() {
		return kwdType;
	}

	public void setKwdType(String kwdType) {
		this.kwdType = kwdType;
	}

	public int getFirstIndexInt() {
		return firstIndexInt;
	}

	public void setFirstIndexInt(int firstIndexInt) {
		this.firstIndexInt = firstIndexInt;
	}

	public int getLastIndexInt() {
		return lastIndexInt;
	}

	public void setLastIndexInt(int lastIndexInt) {
		this.lastIndexInt = lastIndexInt;
	}
}

	
