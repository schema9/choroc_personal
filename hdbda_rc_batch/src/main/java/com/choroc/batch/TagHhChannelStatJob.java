package com.choroc.batch;
 
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.choroc.batch.dto.ApiInfo;
import com.choroc.batch.dto.Scheduler;
import com.choroc.batch.service.SchedulerService;
import com.choroc.batch.service.TagHhChannelStatService;
import com.choroc.batch.util.DateUtil;
 
public class TagHhChannelStatJob {
 
    private static final Logger logger = LoggerFactory.getLogger(TagHhChannelStatJob.class);
 
	@Autowired
	private TagHhChannelStatService tagHhChannelStatService;
	
	@Autowired
	private SchedulerService schedulerService;
 
    @Scheduled(cron="0/30 * * * * *") // 30초 테스트
//  @Scheduled(cron="0 40 8 * * ?") // 매일 오전 4시 
    public void scheduleRun() {
 
     Calendar calendar = Calendar.getInstance();
 
     SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
     
     int insRtn = scheduleLogger("INS");
 
     logger.info("TagStatJob 배치 시작 시간 ::: " + dateFormat.format(calendar.getTime()) + "#######################################");
     
     ApiInfo param = new ApiInfo();
     param.setBatchExcuteTime(dateFormat.format(calendar.getTime()));
     
     
     
     tagHhChannelStatService.tagHhChannelStatScheduler(param);
     
     
     
     int updRtn = scheduleLogger("UPD");
     
     logger.info("TagStatJob 배치 종료 시간 ::: " + dateFormat.format(calendar.getTime()) + "#######################################");
 
    }
    
    
    public int scheduleLogger(String flag) {
    	int rtnVal = 0;
    	
    	
    	
    	
		Scheduler scheduler = new Scheduler();
		scheduler.setFlag(flag);
		scheduler.setJob_id(100);
		scheduler.setEtl_tgt_sys("SEPP");
		scheduler.setEtl_tgt_tbl("TAG_HH_CHANNEL_STAT");
		
		
    	if("DEL".equals(flag)) {
//    		rtnVal = schedulerService.deleteAll(scheduler);
    	}
    	
    	if("UPD".equals(flag)) {
    		String start_ddtm = DateUtil.getCurrentDateWithPattern("yyyyMMddHHmmss");
    		scheduler.setExcute_ddtm(start_ddtm);
    		rtnVal = schedulerService.updateJobHisRedo(scheduler);
    	}
    	
    	if("INS".equals(flag)) {
    		String end_ddtm = DateUtil.getCurrentDateWithPattern("yyyyMMddHHmmss");
    		scheduler.setExcute_ddtm(end_ddtm);
    		rtnVal = schedulerService.updateJobHisRedo(scheduler);
    	}
    	
		return rtnVal;
    	
    }
    
 
}
