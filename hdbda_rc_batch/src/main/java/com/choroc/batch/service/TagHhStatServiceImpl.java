package com.choroc.batch.service;

import java.sql.SQLException;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.choroc.batch.dao.TagHhStatDao;
import com.choroc.batch.dto.ApiInfo;
import com.choroc.batch.util.DateUtil;

/**
 * TODO > Batch tag_stat 관련 서비스 
 * 파일명   > TagHhStatServiceImpl.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2019. 11. 9.
 * 수정일   > 
 * 수정자   > 
 */
@Service("tagHhStatService")
public class TagHhStatServiceImpl implements TagHhStatService {

	@Inject
	private TagHhStatDao tagHhStatDao;	
	
	private static final Logger logger = LoggerFactory.getLogger(TagHhStatServiceImpl.class);
	
	
	@Override
	public Map<String,Object> tagHhStatScheduler(ApiInfo param) {
		Map<String,Object> result = null;
		
		try {
			String excute_ddtm = DateUtil.getCurrentDateWithPattern("yyyyMMdd");
			
			ApiInfo apiParam = new ApiInfo();
//			apiParam.setBatchStartTime(excute_ddtm);
//			apiParam.setBatchEndTime(excute_ddtm);
			// RC_EOAD_TAG_LOG 조회 - 오늘 날짜
			result = tagHhStatDao.tagHhStatScheduler(apiParam);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;				
	}
	
	
}
