package com.choroc.batch.service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.choroc.batch.dao.TagHhChannelStatDao;
import com.choroc.batch.dto.ApiInfo;
import com.choroc.batch.util.DateUtil;
import com.choroc.batch.util.ZValue;

/**
 * TODO > Batch tag_stat 관련 서비스 
 * 파일명   > TagHhChannelStatServiceImpl.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2019. 11. 9.
 * 수정일   > 
 * 수정자   > 
 */
@Service("tagHhChannelStatService")
public class TagHhChannelStatServiceImpl implements TagHhChannelStatService {

	@Inject
	private TagHhChannelStatDao tagHhChannelStatDao;	
	
	private static final Logger logger = LoggerFactory.getLogger(TagHhChannelStatServiceImpl.class);
	
	
	@Override
	public Map<String,Object> tagHhChannelStatScheduler(ApiInfo param) {
		Map<String,Object> result = null;
		
		try {
			String excute_ddtm = DateUtil.getCurrentDateWithPattern("yyyyMMdd");
			
			ApiInfo apiParam = new ApiInfo();
//			apiParam.setBatchStartTime(excute_ddtm);
//			apiParam.setBatchEndTime(excute_ddtm);
			// RC_EOAD_TAG_LOG 조회 - 오늘 날짜
			result = tagHhChannelStatDao.tagHhChannelStatScheduler(apiParam);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;				
	}
	
	
}
