package com.choroc.batch.service;

import com.choroc.batch.dto.Scheduler;

/**
 * TODO > 스케줄러 서비스
 * 파일명   > SchedulerService.java
 * 작성일   > 2018. 12. 4.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public interface SchedulerService {

	public int deleteAll(Scheduler scheduler);
	
	public int insertRedo(Scheduler shceduler);
	
	public int updateJobHisRedo(Scheduler shceduler);
	
	

}
