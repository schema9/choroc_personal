package com.choroc.batch.service;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.choroc.batch.dao.SchedulerDao;
import com.choroc.batch.dto.Scheduler;
import com.choroc.batch.dto.SchedulerTask;
import com.choroc.batch.util.DateUtil;

/**
 * TODO > 스케줄러 서비스 구현
 * 파일명   > SchedulerServiceImpl.java
 * 작성일   > 2018. 12. 4.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */

@Configuration
@Component
@PropertySource("classpath:application-context.properties")
@Service
public class SchedulerServiceImpl implements SchedulerService{

	@Inject
	private SchedulerDao schedulerDao; 
	
	@Autowired
	private Environment env;
	
	private static final Logger logger = LoggerFactory.getLogger(SchedulerServiceImpl.class);
	
	/* 잡 데이터 (job, task, his 테이블)삭제 */
	@Transactional
	@Override
	public int deleteAll(Scheduler scheduler) {
		int result = 0;
		try {
			result = schedulerDao.deleteAll(scheduler);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	/* 스케줄러 수동 실행 */
	@Transactional
	@Override
	public int insertRedo(Scheduler scheduler) {
		int result = 0;
		SchedulerTask task =  new SchedulerTask();
		
		try {
			
			// 카프카 메시지 전송
			String curr = DateUtil.getCurrentDateWithPattern("yyyyMMddHHmmss");
			
			// TASK 테이블 컬럼
			String jobId = Integer.toString(scheduler.getJob_id());
			String taskId = String.format("%s_%s_%s_%s", scheduler.getEtl_tgt_sys(), scheduler.getEtl_tgt_tbl(), jobId, curr);
			String agentId = scheduler.getAgent_id();
			String cron_expr = scheduler.getCron_expr();
			String execProg = scheduler.getExec_prog();
			String paramStr = scheduler.getParam_str();
			String optionStr = scheduler.getOption_str();
			String etlTgtSys = scheduler.getEtl_tgt_sys();
			String etlTgtTbl = scheduler.getEtl_tgt_tbl();
			String lstChgDatCond = scheduler.getLst_chg_dat_cond();
			String jobType = scheduler.getJob_type();
			
			
			// RC_BD_SCHD_TASK 입력
			task.setTask_id(taskId);
			task.setJob_id(jobId);
			task.setCron_expr(cron_expr);
			task.setExec_prog(execProg);
			if(task.getParam_str() != null) {
				task.setParam_str(paramStr);
			}
//			task.setParam_str(paramStr);
			if(task.getOption_str() != null) {
				task.setOption_str(optionStr);
			}
			task.setEtl_tgt_sys(etlTgtSys);
			task.setEtl_tgt_tbl(etlTgtTbl);
			if(task.getLst_chg_dat_cond() != null) {
				task.setLst_chg_dat_cond(lstChgDatCond);
			}
			task.setReg_time(curr);
			task.setJob_type(jobType);
			
			// TASK 테이블 INSERT
//			result = schedulerDao.insertRedo(task);
			
			// 카프카 메시지 전송
			
			// RC_BD_SCHD_EXEC_HIS 수정
//			task.setStts_cd("S");
//			task.setStts_dtl("메시지 전송");
//			task.setStrt_ddtm(curr);
			task.setSts_cd("S");
			task.setSts_dtl_cd("메시지 전송");
			task.setFr_time(curr);
			
			result += schedulerDao.updateJobHisStatus(task);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;		
	}
	
	
	@Transactional
	@Override
	public int updateJobHisRedo(Scheduler scheduler) {
		int result = 0;
		SchedulerTask task =  new SchedulerTask();
		
		try {
			
			String flag = scheduler.getFlag();
			// TASK 테이블 컬럼
			String jobId = Integer.toString(scheduler.getJob_id());
			String taskId = String.format("%s_%s_%s_%s", scheduler.getEtl_tgt_sys(), scheduler.getEtl_tgt_tbl(), jobId, scheduler.getExcute_ddtm());
			// RC_BD_SCHD_TASK 입력
			task.setTask_id(taskId);
			task.setJob_id(jobId);
			
			if("INS".equals(flag)) {
				// RC_BD_SCHD_EXEC_HIS 수정
//				task.setStts_cd("I");
//				task.setStts_dtl("배치중");
//				task.setStrt_ddtm(scheduler.getExcute_ddtm());
				task.setSts_cd("I");
				task.setSts_dtl_cd("배치중");
				task.setFr_time(scheduler.getExcute_ddtm());
				task.setReg_time(scheduler.getExcute_ddtm());
			}
			
			if("UPD".equals(flag)) {
				// RC_BD_SCHD_EXEC_HIS 수정
//				task.setStts_cd("S");
//				task.setStts_dtl("배치완료");
//				task.setEnd_ddtm(scheduler.getExcute_ddtm());
				task.setSts_cd("S");
				task.setSts_dtl_cd("배치완료");
				task.setTo_time(scheduler.getExcute_ddtm());
			}
						
			result += schedulerDao.updateJobHisStatus(task);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;		
	}
	
}
