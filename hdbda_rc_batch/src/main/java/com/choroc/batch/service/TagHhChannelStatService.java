package com.choroc.batch.service;

import java.util.Map;

import com.choroc.batch.dto.ApiInfo;

/**
 * TODO  >
 * 파일명   > TagHhChannelStatService.java
 * 작성일   > 2019. 11. 20.
 * 작성자   > Copyright CCmediaService
 * 수정일   > 
 * 수정자   > 
 */
public interface TagHhChannelStatService {
	
	// tag_stat scheduler
	public Map<String, Object> tagHhChannelStatScheduler(ApiInfo param);
	
	
}
