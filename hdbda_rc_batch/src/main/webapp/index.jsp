<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String ossvi = (String)request.getParameter("ossvi");
	String ossts = (String)request.getParameter("ossts");
	String ossct = (String)request.getParameter("ossct");

	String ossdi = (String)request.getParameter("ossdi");
	String ossdwp = (String)request.getParameter("ossdwp");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>추천 서비스</title>
<script src="/js/jquery-1.12.4.js"></script>
</head>
<body>
<script>
$(document).ready(function(){
	if(opener){
		history.replaceState({}, null, location.pathname);
		
		$('#frm').clone().attr('id', 'frm_pop').insertAfter( $('#frm') );
		$("#frm_pop").hide();
		$("#user_opt").val("SSO");
		
		$('#frm_pop').append('<input type="hidden" name="ossvi" id="ossvi" value="<%=ossvi%>" />');
		$('#frm_pop').append('<input type="hidden" name="ossts" id="ossts" value="<%=ossts%>" />');
		$('#frm_pop').append('<input type="hidden" name="ossct" id="ossct" value="<%=ossct%>" />');
		
		$('#frm_pop').append('<input type="hidden" name="ossdi" id="ossdi" value="<%=ossdi%>" />');
		$('#frm_pop').append('<input type="hidden" name="ossdwp" id="ossdwp" value="<%=ossdwp%>" />');
		$('#frm_pop').attr('target', '_self');
		$('#frm_pop').attr('method', 'post');
		$('#frm_pop').attr('action', "/login/login.do");
		$('#frm_pop').submit();		
	} else {
		location.href="/login/login.do";
	}	
});
</script>


<form id="frm" name="frm" method="post"></form>
</body>
</html>
