package com.choroc.logging.tagging.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.choroc.logging.tagging.server.vo.TaggingVO;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

	@ExceptionHandler(value = Throwable.class)								
	public @ResponseBody TaggingVO defaultErrorHandler(Throwable ex) {
		logger.error(ex.getMessage(), ex);

		TaggingVO vo = new TaggingVO();
		vo.setResult_code("ERR");
		vo.setResult_message("");
		vo.setResult_value("");

		// TODO: exception message
		if (ex instanceof MessageRuntimeException) {
			vo.setResult_message(ex.getMessage());
		}

		return vo;
	}
}
