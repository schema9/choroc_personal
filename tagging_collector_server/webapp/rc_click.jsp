<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%--
	// // 디바이스 아이디 쿠키 설정 
	Cookie cookie = new Cookie("p_deviceId", "디바이스 아이디");
	cookie.setMaxAge(60 * 60 * 60);
	cookie.setPath("/");
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="files/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="files/tagging.collector-1.0.min.js"></script>
<script type="text/javascript" src="files/generateRandomData.js"></script>


<title>제품 장바구니 페이지</title>
</head>

<body>

<script>
	// 제품추천서비스
	var tagging_data_obj = {};
	var tagging_data = {};
	var tagging_data_arr = new Array();
	
	tagging_data_obj.itemId = "2071492596"; // 제품코드
	tagging_data_obj.itemName  = "[DPC] 루즈 퓨르 엑스퍼트 컬렉션 (립 6종세트)"; // 제품명
	tagging_data_obj.price ="48000"; // 가격
	tagging_data_obj.qty = "1";	// 수량
	tagging_data_obj.thumb = "https://imagessl.com/static/5/2/49/71/2071492596_0_480.jpg"; // 이미지주소
	tagging_data_obj.cateA1Cd = "R5"; // 카테고리 대 코드(A)
	tagging_data_obj.cateA2Cd = "R501"; // 카태고리 중 코드(A)
	tagging_data_obj.cateA3Cd = "R50103"; // 카테고리 소 코드(A)
	tagging_data_obj.cateA4Cd = "173266"; // 카테고리 소 코드(B)
	tagging_data_obj.itemType = "A"; // 아이템 타입(A:일반, B:백화점, C:TV, D:데이터 방송)
	
	tagging_data_arr.push(tagging_data_obj);
	
	tagging_data_obj = {};
	
	tagging_data_obj.itemId = "222222222"; // 제품코드
	tagging_data_obj.itemName  = "제품명"; // 제품명
	tagging_data_obj.price ="99999"; // 가격
	tagging_data_obj.qty = "3";	// 수량
	tagging_data_obj.thumb = "thumb"; // 이미지주소
	tagging_data_obj.cateA1Cd = "R4"; // 카테고리 대 코드(A)
	tagging_data_obj.cateA2Cd = "R401"; // 카태고리 중 코드(A)
	tagging_data_obj.cateA3Cd = "R40103"; // 카테고리 소 코드(A)
	tagging_data_obj.cateA4Cd = "473266"; // 카테고리 소 코드(B)
	tagging_data_obj.itemType = "B"; // 아이템 타입(A:일반, B:백화점, C:TV, D:데이터 방송)
	
	tagging_data_arr.push(tagging_data_obj);
	
	
	tagging_data.products = tagging_data_arr;
	tagging_data.channel = getRandomChannel(); //"P"; // 체널 구분[P:PC, M:Moblie, AI:App(IOS), AA-App(Android)]	
	tagging_data.referrer = "http://테스트";
	tagging_data.url = "http://test.do";
	
	tagging_data.custNo = "고객번호";
	tagging_data.cust_group = getRandomInt(1, 5); //"고객 그룹";
	tagging_data.ageGrp = getRandom10(10, 60); 
	
	tagging.collector("RC_CLICK", tagging_data);
</script>

	<h2>check your logfile..</h2>
	
</body>
</html>