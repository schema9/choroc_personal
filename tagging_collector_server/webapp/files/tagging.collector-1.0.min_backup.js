$.support.cors = true;
var tagging = {};
tagging.collector = function(tagType, obj) {
  try {
      var bdtrk_referrer;
      if (document.referrer != null) {
          bdtrk_referrer = document.referrer;
      }
      var bdtrk_deviceId = getTaggingCookie();
      var bdtrkArea;
      var bdTrkAreaUrl = window.location.href;
      var x, y;
      if (bdTrkAreaUrl.indexOf("bdTrkArea") > -1 && bdTrkAreaUrl.indexOf('?') > -1) {
          bdTrkAreaUrl = bdTrkAreaUrl.substr(bdTrkAreaUrl.indexOf('?') + 1);
          var bdTrkAreaUrlArr = bdTrkAreaUrl.split('&');
          for (var i = 0; i < bdTrkAreaUrlArr.length; i++) {
              x = bdTrkAreaUrlArr[i].substr(0, bdTrkAreaUrlArr[i].indexOf('='));
              y = bdTrkAreaUrlArr[i].substr(bdTrkAreaUrlArr[i].indexOf('=') + 1);
              x = x.replace(/^\s+|\s+$/g, '');
              if (x == "bdTrkArea") {
                  bdtrkArea = unescape(y);
              }
          }
      }
      var dataObj = {
          deviceId: bdtrk_deviceId,
          custNo: getTaggingCustNo(),
          tagType: tagType,
          bdtrkArea: bdtrkArea,
          products: [{
              "itemId": obj.itemId,
              "itemName": obj.itemName,
              "search": obj.search,
              "price": obj.price,
              "qty": obj.qty,
              "thumb": obj.thumb,
              "cateA1Cd": obj.cateA1Cd,
              "cateA2Cd": obj.cateA2Cd,
              "cateA3Cd": obj.cateA3Cd,
              "cateA4Cd": obj.cateA4Cd,
              "cateB1Cd": obj.cateB1Cd,
              "cateB2Cd": obj.cateB2Cd,
              "cateB3Cd": obj.cateB3Cd,
              "cateB4Cd": obj.cateB4Cd,
              "brandCd": obj.brandCd,
              "itemType": obj.itemType
          }],
          channel: obj.channel,
          referrer: bdtrk_referrer,
          url: window.location.href,
          timestamp: obj.timestamp,
          campTracCd: obj.campTracCd
      };
      var bdtrk_url, bdtrk_protocol = location.protocol;
      var bdtrk_chkUrl = window.location.href;
      if ('' != bdtrk_chkUrl) {
          if (bdtrk_chkUrl.indexOf('?') > -1) {
              bdtrk_chkUrl = bdtrk_chkUrl.substr(bdtrk_chkUrl.indexOf('?') + 1);
          }
          if (bdtrk_chkUrl.indexOf("m.") > -1 || bdtrk_chkUrl.indexOf("www.") > -1) {
              if ('http:' == bdtrk_protocol) {
                  bdtrk_url = 'http://bdtrk.hyundaihmall.com:8080/tagging/collectData.json';
              } else {
                  bdtrk_url = 'https://bdtrk.hyundaihmall.com:8443/tagging/collectData.json';
              }
          } else {
              if ('http:' == bdtrk_protocol) {
                  bdtrk_url = 'http://bdtrk.hyundaihmall.com:8080/tagging/collectData.json';
              } else {
                  bdtrk_url = 'https://bdtrk.hyundaihmall.com:8443/tagging/collectData.json';
              }
          }
      } else {
          return false;
      }
      $.ajax({
          type: "POST",
          url: bdtrk_url,
          dataType: 'json',
          data: JSON.stringify(dataObj),
          contentType: 'application/json; charset=utf-8',
          timeout: 3000,
          success: function(data) {},
          error: function(jqXHR, testStatus, errorThrown) {}
      })
  } catch (err) {}
};

function taggingGuid() {
  try {
      return ((1 + Math.random()) * 0x10000 | 0).toString(16).substring(1);
  } catch (err) {}
}

function getUuid() {
  try {
      return taggingGuid() + taggingGuid() + '-' + taggingGuid() + '-' + taggingGuid() + '-' + taggingGuid() + '-' + taggingGuid() + taggingGuid() + taggingGuid();
  } catch (err) {}
}

function getTaggingCookie() {
  try {
      var bdtrk_search = "hmallTagging";
      var bdtrk_i, bdtrk_x, bdtrk_y, bdtrk_ARRcookies = document.cookie.split(';');
      for (var bdtrk_i = 0; bdtrk_i < bdtrk_ARRcookies.length; bdtrk_i++) {
          bdtrk_x = bdtrk_ARRcookies[bdtrk_i].substr(0, bdtrk_ARRcookies[bdtrk_i].indexOf('='));
          bdtrk_y = bdtrk_ARRcookies[bdtrk_i].substr(bdtrk_ARRcookies[bdtrk_i].indexOf('=') + 1);
          bdtrk_x = bdtrk_x.replace(/^\s+|\s+$/g, '');
          if (bdtrk_x == bdtrk_search) {
              return unescape(bdtrk_y);
          }
      }
      var bdtrk_exdate = new Date();
      bdtrk_exdate.setTime(bdtrk_exdate.getTime() + 1000 * 3600 * 24 * 365);
      var bdtrk_expires = "; expires=" + bdtrk_exdate.toGMTString();
      var bdtrk_uuid = getUuid();
      var bdtrk_cookie_value = escape(bdtrk_uuid) + bdtrk_expires + "; path=/";
      document.cookie = "hmallTagging=" + bdtrk_cookie_value;
      return bdtrk_uuid;
  } catch (err) {}
  return "";
}

function getTaggingCustNo() {
  try {
      var bdtrk_search = "EHCustNO";
      var bdtrk_i, bdtrk_x, bdtrk_y, ARRcookies = document.cookie.split(';');
      for (var bdtrk_i = 0; bdtrk_i < ARRcookies.length; bdtrk_i++) {
          bdtrk_x = ARRcookies[bdtrk_i].substr(0, ARRcookies[bdtrk_i].indexOf('='));
          bdtrk_y = ARRcookies[bdtrk_i].substr(ARRcookies[bdtrk_i].indexOf('=') + 1);
          bdtrk_x = bdtrk_x.replace(/^\s+|\s+$/g, '');
          if (bdtrk_x == bdtrk_search) {
              return unescape(bdtrk_y);
          }
      }
  } catch (err) {}
  return "";
}

function getTaggingAutoCustNo() {
  try {
      var bdtrk_search = "TagLoginAuto";
      var bdtrk_i, bdtrk_x, bdtrk_y, ARRcookies = document.cookie.split(';');
      for (var bdtrk_i = 0; bdtrk_i < ARRcookies.length; bdtrk_i++) {
          bdtrk_x = ARRcookies[bdtrk_i].substr(0, ARRcookies[bdtrk_i].indexOf('='));
          bdtrk_y = ARRcookies[bdtrk_i].substr(ARRcookies[bdtrk_i].indexOf('=') + 1);
          bdtrk_x = bdtrk_x.replace(/^\s+|\s+$/g, '');
          if (bdtrk_x == bdtrk_search) {
              document.cookie = 'TagLoginAuto=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
              return true;
          }
      }
  } catch (err) {}
  return false;
}
try {
  var bdtrk_campUrl = window.location.href;
  var bdtrk_i, bdtrk_x, bdtrk_y, bdtrk_campTracCd = '';
  if ('' != bdtrk_campUrl) {
      var bdtrk_search = "campTracCd";
      if (bdtrk_campUrl.indexOf(bdtrk_search) > -1 && bdtrk_campUrl.indexOf('?') > -1) {
          bdtrk_campUrl = bdtrk_campUrl.substr(bdtrk_campUrl.indexOf('?') + 1);
          var campUrlArr = bdtrk_campUrl.split('&');
          for (var bdtrk_i = 0; bdtrk_i < campUrlArr.length; bdtrk_i++) {
              bdtrk_x = campUrlArr[bdtrk_i].substr(0, campUrlArr[bdtrk_i].indexOf('='));
              bdtrk_y = campUrlArr[bdtrk_i].substr(campUrlArr[bdtrk_i].indexOf('=') + 1);
              bdtrk_x = bdtrk_x.replace(/^\s+|\s+$/g, '');
              if (bdtrk_x == bdtrk_search) {
                  bdtrk_campTracCd = unescape(bdtrk_y);
              }
          }
      }
  }
  var bdtrk_referrer = document.referrer;
  var bdtrk_loginMUrl = 'smLoginP.do';
  var bdtrk_loginPUrl = 'loginSuccessPup.do';
  var autoLoginCheck = getTaggingAutoCustNo();
  if ('' != bdtrk_referrer) {
      var bdtrk_channel = '';
      if (bdtrk_referrer.indexOf(bdtrk_loginMUrl) > -1) {
          bdtrk_channel = 'M';
      } else if (bdtrk_referrer.indexOf(bdtrk_loginPUrl) > -1) {
          bdtrk_channel = 'P';
      } else if (autoLoginCheck) {
          var bdtrk_ua = navigator.userAgent.toLowerCase();
          if (bdtrk_ua.match('android') != null) bdtrk_channel = 'AA';
          elsebdtrk_channel = 'AI';
      }
      if ('' != bdtrk_channel) {
          var hbdaRc_trk_login_data = {};
          hbdaRc_trk_login_data.channel = bdtrk_channel;
          tagging.collector("RC_LOGIN", hbdaRc_trk_login_data);
      }
  }
  if ('' != bdtrk_campTracCd) {
      var hbdaRc_trk_camp_data = {};
      hbdaRc_trk_camp_data.campTracCd = bdtrk_campTracCd;
      var trkRcMobile = false,
          trkRcMobileKeyWords = new Array('iPhone', 'iPad', 'Android', 'Mobile', 'Tablet', 'Touch', 'iPod', 'BlackBerry', 'Windows CE', 'Windows Phone', 'Symbian', 'webOS', 'Opera Mini', 'Opera Mobi', 'POLARIS', 'IEMobile', 'nokia', 'LG', 'lgtelecom', 'MOT', 'SAMSUNG', 'Samsung', 'SonyEricsson');
      for (var word in trkRcMobileKeyWords) {
          if (navigator.userAgent.match(trkRcMobileKeyWords[word]) != null) {
              trkRcMobile = true;
              break;
          }
      }
      if (trkRcMobile) {
          if (navigator.userAgent.indexOf('HmallApp') > -1) {
              var bdtrk_ua = navigator.userAgent.toLowerCase();
              if (bdtrk_ua.match('android') != null) hbdaRc_trk_camp_data.channel = "AA";
              elsehbdaRc_trk_camp_data.channel = "AI";
          } else {
              hbdaRc_trk_camp_data.channel = "M";
          }
      } else {
          hbdaRc_trk_camp_data.channel = "P";
      }
      hbdaRc_trk_camp_data.url = window.location.href;
      tagging.collector("RC_LANDING", hbdaRc_trk_camp_data);
  }
} catch (err) {}