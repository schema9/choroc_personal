<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%--
	// // 디바이스 아이디 쿠키 설정 
	Cookie cookie = new Cookie("p_deviceId", "디바이스 아이디");
	cookie.setMaxAge(60 * 60 * 60);
	cookie.setPath("/");
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="files/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="files/tagging.collector-1.0.min.mobile.js"></script>
<script type="text/javascript" src="files/generateRandomData.js"></script>


<title>제품 구매페이지</title>
</head>

<body>
<script>
	// 제품추천서비스
	var tagging_data = {};
	var tagging_data_header = {};
	var tagging_data_arr = new Array();
	
	
	var itemInfo = getRandomItemInfo();
	
	tagging_data.itemId    = itemInfo.itemId; // 제품코드
	tagging_data.itemName  = itemInfo.itemName; // 제품명
	tagging_data.price     = itemInfo.price; // 가격
	tagging_data.qty       = getRandomInt(1, 100);	// 수량
	tagging_data.thumb     = itemInfo.thumb // 이미지주소
	tagging_data.cateA1Cd  = itemInfo.cateA1Cd; // 카테고리 대 코드(A)
	tagging_data.cateA2Cd  = itemInfo.cateA2Cd; // 카태고리 중 코드(A)
	tagging_data.cateA3Cd  = itemInfo.cateA3Cd; // 카테고리 소 코드(A)
 	tagging_data.cateA4Cd  = itemInfo.cateA4Cd;
	tagging_data.itemType  = "A"; // 아이템 타입(A:일반, B:백화점, C:TV, D:데이터 방송)
	tagging_data_arr.push(tagging_data);
	
	
	tagging_data_header.products = tagging_data_arr;
	//tagging_data_header.channel = getRandomChannel(); //"P"; // 체널 구분[P:PC, M:Moblie, AI:App(IOS), AA-App(Android)]	
	tagging_data_header.channel = "P";
	tagging_data_header.referrer = "http://www.referrer.com";
	tagging_data_header.url = "http://test.do";
	
	tagging_data_header.custNo = getRandomMemberId();
	tagging_data_header.cust_group = getRandomInt(1, 5); //"고객 그룹";
	tagging_data_header.ageGrp = getRandom10(10, 60); 

	var flg = getRandomInt(1, 100) % 3;
	if (flg % 3 == 1) {
		tagging.collector("RC_CLICK", tagging_data_header);   //lib호출.
	} else if (flg % 3 == 2) {
		tagging.collector("RC_BUY", tagging_data_header);   //lib호출.
	} else {
		tagging.collector("RC_VIEW", tagging_data_header);   //lib호출.
	}
</script>

	<h2>check your logfile..</h2>
	
	<!--
	<select id="chennelType">
		<option value="P">PC</option>
		<option value="M">Mobile</option>
	</select>
	-->
</body>
</html>