package com.choroc.logging.tagging.listener;

import java.net.InetAddress;
import java.net.UnknownHostException;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.LoggerContextListener;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.spi.ContextAwareBase;
import ch.qos.logback.core.spi.LifeCycle;

/*
 * logback 구성 파일에 환경변수 설정
 * */
public class LoggerStartupListener extends ContextAwareBase implements LoggerContextListener, LifeCycle {

    private boolean started = false;

    @Override
    public void start() {
        
        try {
        
        	if (started) return;

        	Context context = getContext();

			InetAddress inetAddress = InetAddress.getLocalHost();
			
			context.putProperty("HOST_ADDR", inetAddress.getHostAddress());
//	        context.putProperty("CHANNEL", "pc");
	        context.putProperty("CHANNEL", "mobile");

	        started = true;
	        
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
        
    }

    @Override
    public void stop() {
    }

    @Override
    public boolean isStarted() {
        return started;
    }

    @Override
    public boolean isResetResistant() {
        return true;
    }

    @Override
    public void onStart(LoggerContext context) {
    }

    @Override
    public void onReset(LoggerContext context) {
    }

    @Override
    public void onStop(LoggerContext context) {
    }

    @Override
    public void onLevelChange(Logger logger, Level level) {
    }
}