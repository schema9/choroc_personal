package com.choroc.logging.tagging.server.vo;

import java.util.ArrayList;
import java.util.List;

public class TaggingVO {
	private String result_code = "";				// 결과  S: 성공, F: 실패
	private String result_message = "";				// (result_code가 F이면) 에러 메시지  
	private Object result_value = "";

	// tagging variable
	private String deviceId = "";					// 디바이스 아이디
	private String custNo = "";						// 고객번호
	private String tagType = "";					// 태깅 타입
	private String itemId = "";						// 상풍아이디
	private String itemName = "";					// 상풍명
	private String search = "";						
	private String price = "";						// 상품 가격
	private String qty = "";						// 수량
	private String thumb = "";						// 상품이미지
	private String cateA1Cd = "";					// 카테고리 depth1  코드
	private String cateA2Cd = "";					// 카테고리 depth2  코드
	private String cateA3Cd = "";					// 카테고리 depth3 코드
	private String cateA4Cd = "";					// 카테고리 depth4 코드
	private String cateB1Cd = "";					
	private String cateB2Cd = "";
	private String cateB3Cd = "";
	private String cateB4Cd = "";
	private String ageGrp = "";		      			// 연령대
	private String brandCd = "";
	private String itemType = "";					// P:상품, S:서비스
	private String channel = "";					// P:Pc, M:Moblie
	private String referrer = "";					// 이전페이지
	private String url = "";						// URL
	private String timestamp = "";					// 날짜 및 시간
	private String campTracCd = "";				
	private String bdtrkArea = "";
	private String campatinRecordCd = ""; 			// 캠페인 추적코드
	
	// 고객 구룹
	private String cust_group = "";					// 고객 그룹

	// product variable
	private List<ProductsVO> products = new ArrayList<ProductsVO>(); 

	public String getResult_code() {
		return result_code;
	}

	public String getCust_group() {
		return cust_group;
	}

	public void setCust_group(String cust_group) {
		this.cust_group = cust_group;
	}

	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}

	public String getResult_message() {
		return result_message;
	}

	public void setResult_message(String result_message) {
		this.result_message = result_message;
	}

	public Object getResult_value() {
		return result_value;
	}

	public void setResult_value(Object result_value) {
		this.result_value = result_value;
	}

	public List<ProductsVO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductsVO> products) {
		this.products = products;
	}
		
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getTagType() {
		return tagType;
	}

	public void setTagType(String tagType) {
		this.tagType = tagType;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	public String getCateA1Cd() {
		return cateA1Cd;
	}

	public void setCateA1Cd(String cateA1Cd) {
		this.cateA1Cd = cateA1Cd;
	}

	public String getCateA2Cd() {
		return cateA2Cd;
	}

	public void setCateA2Cd(String cateA2Cd) {
		this.cateA2Cd = cateA2Cd;
	}

	public String getCateA3Cd() {
		return cateA3Cd;
	}

	public void setCateA3Cd(String cateA3Cd) {
		this.cateA3Cd = cateA3Cd;
	}

	public String getCateA4Cd() {
		return cateA4Cd;
	}

	public void setCateA4Cd(String cateA4Cd) {
		this.cateA4Cd = cateA4Cd;
	}

	public String getCateB1Cd() {
		return cateB1Cd;
	}

	public void setCateB1Cd(String cateB1Cd) {
		this.cateB1Cd = cateB1Cd;
	}

	public String getCateB2Cd() {
		return cateB2Cd;
	}

	public void setCateB2Cd(String cateB2Cd) {
		this.cateB2Cd = cateB2Cd;
	}

	public String getCateB3Cd() {
		return cateB3Cd;
	}

	public void setCateB3Cd(String cateB3Cd) {
		this.cateB3Cd = cateB3Cd;
	}

	public String getCateB4Cd() {
		return cateB4Cd;
	}

	public void setCateB4Cd(String cateB4Cd) {
		this.cateB4Cd = cateB4Cd;
	}

	public String getBrandCd() {
		return brandCd;
	}

	public void setBrandCd(String brandCd) {
		this.brandCd = brandCd;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getReferrer() {
		return referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getCampTracCd() {
		return campTracCd;
	}

	public void setCampTracCd(String campTracCd) {
		this.campTracCd = campTracCd;
	}

	public String getBdtrkArea() {
		return bdtrkArea;
	}

	public void setBdtrkArea(String bdtrkArea) {
		this.bdtrkArea = bdtrkArea;
	}
	
	public String getAgeGrp() {
		return ageGrp;
	}

	public void setAgeGrp(String ageGrp) {
		this.ageGrp = ageGrp;
	}
	
	public String getCampatinRecordCd() {
		return campatinRecordCd;
	}

	public void setCampatinRecordCd(String campatinRecordCd) {
		this.campatinRecordCd = campatinRecordCd;
	}

	// TaggingCollectorController.java tagging 구분자 dataDelimiter = "$^";
	public String toStringView(String dataDelimiter) {
		
		String logFormat = "%s" + dataDelimiter + "%s"  + dataDelimiter + "%s"  + dataDelimiter + "%s"  + dataDelimiter + "%s" + dataDelimiter 
						 + "%s" + dataDelimiter + "%s"  + dataDelimiter + "%s"  + dataDelimiter + "%s"  + dataDelimiter + "%s" + dataDelimiter
						 + "%s" + dataDelimiter + "%s"  + dataDelimiter + "%s"  + dataDelimiter + "%s"  + dataDelimiter + "%s" + dataDelimiter
						 + "%s" + dataDelimiter + "%s"  + dataDelimiter + "%s"  + dataDelimiter + "%s"  + dataDelimiter + "%s"  + dataDelimiter
						 + "%s"  + dataDelimiter + "%s%s"
						 ;

		String toString = String.format(logFormat,
				  deviceId, custNo, cust_group, tagType, itemId
				, itemName, price, qty, thumb, cateA1Cd
				, cateA2Cd, cateA3Cd, cateA4Cd, ageGrp, brandCd
				, itemType, channel, referrer, url, timestamp
				, campatinRecordCd, result_code, result_message
				);
		
		// ============================================================================================================
		/*
		String printParmeter  	= "deviceId" + dataDelimiter + "custNo" 	+ dataDelimiter + "cust_group" 	+ dataDelimiter + "tagType"  + dataDelimiter + "itemId" 	+ dataDelimiter 
						 		+ "itemName" + dataDelimiter + "price" 		+ dataDelimiter + "qty" 		+ dataDelimiter + "thumb" 	 + dataDelimiter + "cateA1Cd" 	+ dataDelimiter 
						 		+ "cateA2Cd" + dataDelimiter + "cateA3Cd" 	+ dataDelimiter + "cateA4Cd" 	+ dataDelimiter + "ItemType" + dataDelimiter + "channel" 	+ dataDelimiter 
						 		+ "referrer" + dataDelimiter + "url" 		+ dataDelimiter + "timestamp" 	+ dataDelimiter 
						 		+ "result_code " + dataDelimiter + "result_message"
						 		;		
		*/
		// ============================================================================================================
		
		return toString;
	}

	@Override
	public String toString() {
		return "TaggingVO [result_code=" + result_code + ", result_message=" + result_message + ", result_value="
				+ result_value + ", deviceId=" + deviceId + ", custNo=" + custNo + ", tagType=" + tagType + ", itemId="
				+ itemId + ", itemName=" + itemName + ", search=" + search + ", price=" + price + ", qty=" + qty
				+ ", thumb=" + thumb + ", cateA1Cd=" + cateA1Cd + ", cateA2Cd=" + cateA2Cd + ", cateA3Cd=" + cateA3Cd
				+ ", cateA4Cd=" + cateA4Cd + ", cateB1Cd=" + cateB1Cd + ", cateB2Cd=" + cateB2Cd + ", cateB3Cd="
				+ cateB3Cd + ", cateB4Cd=" + cateB4Cd + ", ageGrp=" + ageGrp + ", brandCd=" + brandCd + ", itemType="
				+ itemType + ", channel=" + channel + ", referrer=" + referrer + ", url=" + url + ", timestamp="
				+ timestamp + ", campTracCd=" + campTracCd + ", bdtrkArea=" + bdtrkArea + ", cust_group=" + cust_group
				+ ", products=" + products + "]";
	}	
	
}
