package com.choroc.logging.tagging.server.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.crypto.URIDereferencer;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.choroc.logging.tagging.server.vo.ProductsVO;
import com.choroc.logging.tagging.server.vo.TaggingVO;


@Controller
@RequestMapping("/tagging_mobile/tagging")
public class TaggingCollectorControllerMobile {
	
	private static final Logger taggingLogger = LoggerFactory.getLogger("tagging_collector_logger");	
	private static final Logger logger = LoggerFactory.getLogger(TaggingCollectorControllerMobile.class);
	
	// Tag Types 
	private String RC_VIEW = "RC_VIEW";					// 상품 상세  
	private String RC_CART = "RC_CART";					// 장바구니 
	private String RC_ZZIP = "RC_ZZIM";					// 찜 
	private String RC_BUY = "RC_BUY";					// 구매 
	private String RC_LOGIN = "RC_LOGIN";				// 로그인 
	private String RC_SEARCH = "RC_SEARCH";				// 검색 
	private String RC_NO_SEARCH = "RC_NO_SEARCH";		// 실패 검색
	private String RC_CLICK = "RC_CLICK";    	     	// 추천상품 클릭


	
	@RequestMapping(value = "/collectData.json", method = RequestMethod.POST)
	@ResponseBody
	@CrossOrigin
	public void recommendTaggingDataCollector(@RequestBody TaggingVO taggingVO) throws NullPointerException {
		
		String dataDelimiter = "$^";		// 태그 구분자
		String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
		TaggingVO resultVO = new TaggingVO();						// 로그에 찍을 결과를 담을 VO

		
		try{
			resultVO.setResult_code("S");							// 결과값을 초기로  S 설정
			StringBuilder str = new StringBuilder();				// 결과 메시지를 담을 StringBuilder 생성 (에러시에만 메시지 표출)
			
			resultVO.setDeviceId(taggingVO.getDeviceId());			// deviceId 태깅 값을 taggingVO에 저장
			
			if (resultVO.getDeviceId().isEmpty()) {					// deviceId 값이 없으면 태깅 로그 데이터에 에러메시지 표출
				str.append("deviceId, ");
				resultVO.setResult_code("F");
				resultVO.setResult_message(dataDelimiter + "[MISSING_DATA] : deviceId");
			}
			
			// 넘어온 태깅 타입 값을 taggingVO에 저장
			resultVO.setTagType(taggingVO.getTagType());
			
			// 넘어온 tagType 값이 상품 상세, 장바구니, 찜, 구매, 검색, 실패 검색이 아닐경우 태깅 로그 데이터에 에러메시지 표출
			if(!RC_VIEW.equals(resultVO.getTagType()) 
					&& !RC_CART.equals(resultVO.getTagType()) 
					&& !RC_ZZIP.equals(resultVO.getTagType())
					&& !RC_BUY.equals(resultVO.getTagType()) 
					&& !RC_LOGIN.equals(resultVO.getTagType()) 
					&& !RC_SEARCH.equals(resultVO.getTagType()) 
					&& !RC_NO_SEARCH.equals(resultVO.getTagType()) 
					&& !RC_CLICK.equals(resultVO.getTagType())){	
				str.append("tagType, ");
				resultVO.setResult_code("F");
				resultVO.setResult_message(dataDelimiter + "PROGRAM_ERROR");
				throw new MessageRuntimeException("tagType Error!!!");
			}
			
			// 넘어온 channel 값을 taggingVO에 저장
			resultVO.setChannel(taggingVO.getChannel());
			if(resultVO.getChannel().isEmpty()){							// channel 값이 없으면 태깅 로그 데이터에 에러메시지 표출
				str.append("channel, ");
				resultVO.setResult_code("F");
				resultVO.setResult_message(dataDelimiter + "[MISSING_DATA] : channel");
			}
			resultVO.setReferrer(taggingVO.getReferrer());	// 선택값 예외처리x
			// 특수문자 치완
			if(!resultVO.getReferrer().isEmpty()) {										// referrer값이 존재하면
				resultVO.setReferrer(resultVO.getReferrer().replaceAll("\\|", "_^_"));	// 해당 값에서 문자  \\|를  _^_로 변경
			}
			resultVO.setUrl(taggingVO.getUrl()); // url 값 셋팅
			if(resultVO.getUrl().isEmpty()) {								// url 값이 없으면 태깅 로그 데이터에 에러메시지 표출
				str.append("url, ");
				resultVO.setResult_code("F");
				resultVO.setResult_message(dataDelimiter + "[MISSING_DATA] : url");
			} else {
				resultVO.setUrl(resultVO.getUrl().replaceAll("\\|", "_^_"));			// url 값이 있으면, 해당 값에서 문자  \\|를  _^_로 변경
			}
			
			// timestamp
			resultVO.setTimestamp(timestamp);		// 해당 데이터 row가 쌓이는 시간

			// product variable	( 태깅된 상품 정보 taggingVO를 resultVO 에 저장 
			for (int i = 0; i < taggingVO.getProducts().size(); i++) {
				resultVO.setItemId(taggingVO.getProducts().get(i).getItemId()); // 상품아이디
				resultVO.setItemName(taggingVO.getProducts().get(i).getItemName()); // 상품명
				resultVO.setPrice(taggingVO.getProducts().get(i).getPrice()); // 상품가격
				resultVO.setThumb(taggingVO.getProducts().get(i).getThumb()); // 상품이미지
				resultVO.setCateA1Cd(taggingVO.getProducts().get(i).getCateA1Cd()); // 카테고리대
				resultVO.setCateA2Cd(taggingVO.getProducts().get(i).getCateA2Cd()); // 카테고리중
				resultVO.setCateA3Cd(taggingVO.getProducts().get(i).getCateA3Cd()); // 카테고리소
				resultVO.setCateA4Cd(taggingVO.getProducts().get(i).getCateA4Cd()); // 카테고리세
				resultVO.setItemType(taggingVO.getProducts().get(i).getItemType()); // 아이템 타입
				

				// tagType == RC_VIEW, RC_CART, RC_JJIM, RC_BUY ( 태깅 타입이 RC_VIEW, RC_CART, RC_JJIM, RC_BUY 이면 )
				if (RC_VIEW.equals(taggingVO.getTagType()) 
						|| RC_CART.equals(taggingVO.getTagType())
						|| RC_BUY.equals(taggingVO.getTagType())
						|| RC_CLICK.equals(taggingVO.getTagType())) {
					
					// 태깅 타입이 RC_VIEW일 경우 수량은 1로 고정
					if(RC_VIEW.equals(taggingVO.getTagType())) {
						resultVO.setQty("1");
					}
					
//					// exception handling 
					if(resultVO.getItemId().isEmpty()){								// itemId값이 비어있으면  태깅 로그 데이터에 에러메시지 표출
						str.append("itemId, ");
						resultVO.setResult_code("F");
						resultVO.setResult_message(dataDelimiter + "[MISSING_DATA] : itemId");
					}
					if(resultVO.getItemName().isEmpty()){							// itemName값이 비어있으면  태깅 로그 데이터에 에러메시지 표출
						str.append("itemName, ");
						resultVO.setResult_code("F");
						resultVO.setResult_message(dataDelimiter + "[MISSING_DATA] : itemName");
					// 특수문자 치완
					} else {
						resultVO.setItemName(resultVO.getItemName().replaceAll("\\|", "_^_")); // 해당 값에서 문자  \\|를  _^_로 변경
					}
						
					if(resultVO.getPrice().isEmpty()){								// Price값이 비어있으면  태깅 로그 데이터에 에러메시지 표출
						str.append("price, ");
						resultVO.setResult_code("F");
						resultVO.setResult_message(dataDelimiter + "[MISSING_DATA] : price");
					}
					if(resultVO.getThumb().isEmpty()){								// Thumb값이 비어있으면  태깅 로그 데이터에 에러메시지 표출	
						str.append("thumb, ");
						resultVO.setResult_code("F");
						resultVO.setResult_message(dataDelimiter + "[MISSING_DATA] : thumb");
					}
					if(resultVO.getCateA1Cd().isEmpty()){							// CateA1값이 비어있으면  태깅 로그 데이터에 에러메시지 표출
						str.append("cateA1Cd, ");
						resultVO.setResult_code("F");
						resultVO.setResult_message(dataDelimiter + "[MISSING_DATA] : cateA1Cd");
					}
					if(resultVO.getCateA2Cd().isEmpty()){							// CateA2값이 비어있으면  태깅 로그 데이터에 에러메시지 표출
						str.append("cateA2Cd, ");
						resultVO.setResult_code("F");
						resultVO.setResult_message(dataDelimiter + "[MISSING_DATA] : cateA2Cd");
					}
					if(resultVO.getCateA3Cd().isEmpty()){							// CateA3값이 비어있으면  태깅 로그 데이터에 에러메시지 표출
						str.append("cateA3Cd, ");
						resultVO.setResult_code("F");
						resultVO.setResult_message(dataDelimiter + "[MISSING_DATA] : cateA3Cd");
					}
					
					resultVO.setAgeGrp(taggingVO.getAgeGrp());				       // 연령대
					if (resultVO.getAgeGrp().isEmpty()) {
						resultVO.setAgeGrp("");
					}
					
					resultVO.setBrandCd(taggingVO.getBrandCd());				    // 브랜드
					if (resultVO.getBrandCd().isEmpty()) {
						resultVO.setBrandCd("");
					}
					
					if(resultVO.getItemType().isEmpty()){							// itemType값이 비어있으면  태깅 로그 데이터에 에러메시지 표출
						str.append("itemType, ");
						resultVO.setResult_code("F");
						resultVO.setResult_message(dataDelimiter + "[MISSING_DATA] : itemType");
					}
					
					// 고객그룹
					resultVO.setCust_group(taggingVO.getCust_group());				// 고객그룹 값이 있으면 넣어주고 없으면 공백 처리
					if (resultVO.getCust_group().isEmpty()) {
						resultVO.setCust_group("");
					}

					
					// 고객그룹
					resultVO.setCust_group(taggingVO.getCust_group());				// 고객그룹 값이 있으면 넣어주고 없으면 공백 처리
					if (resultVO.getCust_group().isEmpty()) {
						resultVO.setCust_group("");
					}

					// 고객번호
					resultVO.setCustNo(taggingVO.getCustNo());						// 고객번호 값이 있으면 넣어주고 없으면 공백 처리
					if(resultVO.getCustNo().isEmpty()) {
						resultVO.setCustNo("");
					}
					
					// tagType == RC_BUY, RC_CART pty collect	( 태깅 타입이 RC_BUY, RC_CART이면 수량 값 입력
					if (RC_CART.equals(taggingVO.getTagType()) || RC_BUY.equals(taggingVO.getTagType())) {
						resultVO.setQty(taggingVO.getProducts().get(i).getQty()); // 수량
					}
					
					resultVO.setCampatinRecordCd(taggingVO.getCampatinRecordCd());				       // 캠페인 코드
					if (resultVO.getCampatinRecordCd().isEmpty()) {
						resultVO.setCampatinRecordCd("");
					}
					
				}

				else if (RC_LOGIN.equals(taggingVO.getTagType())) {
					// not necessary variable	( 필요없는 파라미터들 공백 처리 )
					resultVO.setItemId("");
					resultVO.setItemName("");
					resultVO.setPrice("");
					resultVO.setQty("");
					resultVO.setThumb("");
					resultVO.setCateA1Cd("");
					resultVO.setCateA2Cd("");
					resultVO.setCateA3Cd("");
					resultVO.setCateA4Cd("");
					resultVO.setItemType("");
					resultVO.setCust_group("");
					resultVO.setCustNo("");
					resultVO.setAgeGrp("");
					resultVO.setBrandCd("");
					resultVO.setCampatinRecordCd("");
				}
				
				taggingLogger.info(resultVO.toStringView(dataDelimiter));	// logFile에 데이터 row입력.

			}
		}catch(Throwable ex){									// 예외발생시 태깅 수집 tomcat 로그에 에러 표출
			throw new MessageRuntimeException("tagging collector program error!!", ex);
		}
	}
}
