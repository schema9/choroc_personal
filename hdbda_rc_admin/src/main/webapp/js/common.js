

//========================================================================
//===== window.load ======================================================
//========================================================================
$(window).load(function(){

    fn_util_setTextType();

});

//========================================================================
//=====  Menu Controls====================================================
//========================================================================
/* left Menu 컨트롤 */
$(function(){

	$(".nav ul > li > a").click(function(){
        var menuId = "";
        menuId = $(this).attr('id');
        
        if($(this).parent('li').hasClass('allMenu')){

            if($("#viewAllMenu").hasClass('openM')){
                allMenuOff();
            }else{
                allMenuOn();
            }

        }else{

        	menuOpen();

            if( $(this).parent('li').hasClass('active') ){

                $(this).parent('li').removeClass('active');
                $(this).parent('li').find('ul').slideUp('300');
                $(this).parent('li').find('ul').removeClass('onMenu');

                $(this).parent('li').find('i.ti-angle-up').attr('class','ti-angle-down');

            }else{

                $(this).parent('li').addClass('active');
                $(this).parent('li').find('ul').slideDown('300');
                $(this).parent('li').find('ul').addClass('onMenu');
                $(this).parent('li').find('i.ti-angle-down').attr('class','ti-angle-up');

            }

        }
	});

	$("#topBtn").click(function(){
		menuSet();
	});
});

/*
jQuery(document).ready(
    function () {
      jQuery(window).bind('beforeunload',
        function (e) {
            if ((event.clientY < 0) ||(event.altKey) ||(event.ctrlKey)||((event.clientY < 129) && (event.clientY>107))) {
            $.ajax({
                url : "/lreco/login/logout.do",
                type:'POST'
            });
            }
        }
      );

    }
  );
*/

/*
description : Define column type for sort items from data-base result data
return      : boolean
parameters  : columnId : string ; MUST BE MATCHED with
                        [ ID of table head ] which the tag 'TH'
*/
function fn_util_isInteger(columnId){
    var result;
    switch(columnId){
        case "rank":
        case "itemId":
        case "price":
        case "viewCount":
        case "recoCount":
        case "ctr":
            result = true;
            break;
        case "itemTitle":
        case "thumbnailUrl":
            result = false;
            break;
        default:
            result = false;
    }
    return result;
}

/*
description : Add comma for integer type
             e.g. 123000123 >> 123,000,123
return      : void
usage       : give "moneyType" class to the Tag
Note        : must be called after AJAX call
*/
function fn_util_setTextType(){
    $('.moneyType').each(function(){
        if($(this).text().isBlank()){
            $(this).val($(this).val().money());
        }else{
            $(this).text($(this).text().money());
        }
    });
}

/*
description : layer control the pop-up using maskOn/maskOff
return      : void
parameter   : divID     : division ID of the layer to pop-up
              popDivUp  : boolean; meaning of pop-up/close-down;
note        : division ID must be given to the layer
*/
function fn_util_divPopUpControl(divID, popDivUp){
    if(popDivUp){
        maskOn();
        $('#'+divID).fadeIn();
    }else{
        maskOff();
        $('#'+divID).fadeOut();
    }
}

/*
description : add zero '0' if value length is 1
return      : string
parameter   : inputValue : number or string
*/
function fn_util_numberToString_prefixZero(inputValue){
    inputValue = parseInt(inputValue);
    if(inputValue.toString().length < 2){
        inputValue = '0' + inputValue;
    }
    return inputValue;
}

/*
description : make json string by combine table data
return      : string;   combined json string;
parameter   : tbodyID   : identify tbody tag
note        : <tbody> <tr>  <td> key </td> <td> value </td> </tr> </tbody>
*/
function fn_util_combineJsonInTbody(tbodyID){
    var trLength = $('#'+tbodyID+' tr').length;
    var json = '';
    //alert('trLength: '+trLength);

    json = '{';
    $('#'+tbodyID+' tr').each(function(idx){
        var trVal = '',
            key = $(this).find('td .txtJsonKey').val(),
            val = $(this).find('td .txtJsonValue').val();
        trVal = '"'+key+'":"'+val+'"';
        if(idx != trLength -1){
            trVal +=',';
        }
        json +=trVal;
    });
    json += '}';

    return json;
}

/*
duplicated functions    : fn_setPaging1 >> renderDataByHtml1
                          fn_setPaging2 >> renderDataByHtml2
description : set option value for list to display
              and page number with given parameters
parameters  : objDataLength : Integer       ; the number of rows of result data from data-base
              optLimitVal   : 10 / 20 ...   ; the number of items to display in a list
              inputPageNo   : 1 ~ ...or Forward or Backward       ; requested page number
              shouldReload  : true / false  ; set true when re-load paging
              idForPaging   : 'divPageNo'
*/
function fn_setPaging1(objDataLength, optLimitVal, inputPageNo, shouldReload, idForPaging){

    var maxPageNo = 0;
    var limitOption = 0;
    var currentPageNo = 0;
    var movePageNo = 0;
    var doChange = false;
    var innerHtml = '';
    var suffix = 'A'; // when paging1 then A when paging2 then B

    maxPageNo = Math.ceil(objDataLength/optLimitVal);

    if(shouldReload){
        movePageNo = inputPageNo;
        doChange = true;

        // make paging
        $('#'+idForPaging).html('');
        innerHtml = '<a href="#" onclick="return false;" id="aPageNo'+ suffix+'Backward"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>';
        for(var i=1; i <= maxPageNo; i++){
            innerHtml += '<a href="#" onclick="return false;" id="aPageNo'+ suffix + i + '" class="aPageNo'+ suffix+'">' + i + '</a>';
        }
        innerHtml += '<a href="#" onclick="return false;" id="aPageNo'+ suffix+'Forward"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>';
        $('#'+idForPaging).html(innerHtml);

    }else{
        currentPageNo = parseInt(($('#'+idForPaging+' .pageActive').attr('id')).replace('aPageNo'+ suffix,''));
        if(inputPageNo == 'Forward'){
            if(currentPageNo != maxPageNo){
                doChange = true;
                movePageNo = currentPageNo+1;
            }
        }else if(inputPageNo == 'Backward'){
            if(currentPageNo != 1){
                doChange = true;
                movePageNo = currentPageNo-1;
            }

        }else{
            if(inputPageNo != currentPageNo){
                movePageNo = inputPageNo;
                doChange = true;
            }
        }
    }// end of if(shouldReload)

    if(doChange){
        $('#'+idForPaging+' .aPageNo'+ suffix).removeClass('pageActive');
        $('#aPageNo'+ suffix+movePageNo).addClass('pageActive');
        $('#aPageNo'+ suffix+movePageNo).blur();

        renderDataByHtml1(optLimitVal, movePageNo);
    }

    leftH();
}

function fn_setPaging2(objDataLength, optLimitVal, inputPageNo, shouldReload, idForPaging){

    var maxPageNo = 0;
    var limitOption = 0;
    var currentPageNo = 0;
    var movePageNo = 0;
    var doChange = false;
    var innerHtml = '';
    var suffix = 'B'; // when paging1 then A when paging2 then B

    maxPageNo = Math.ceil(objDataLength/optLimitVal);

    if(shouldReload){
        movePageNo = inputPageNo;
        doChange = true;

        // make paging
        $('#'+idForPaging).html('');
        innerHtml = '<a href="#" onclick="return false;" id="aPageNo'+ suffix+'Backward"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>';
        for(var i=1; i <= maxPageNo; i++){
            innerHtml += '<a href="#" onclick="return false;" id="aPageNo'+ suffix + i + '" class="aPageNo'+ suffix+'">' + i + '</a>';
        }
        innerHtml += '<a href="#" onclick="return false;" id="aPageNo'+ suffix+'Forward"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>';
        $('#'+idForPaging).html(innerHtml);

    }else{
        currentPageNo = parseInt(($('#'+idForPaging+' .pageActive').attr('id')).replace('aPageNo'+ suffix,''));
        if(inputPageNo == 'Forward'){
            if(currentPageNo != maxPageNo){
                doChange = true;
                movePageNo = currentPageNo+1;
            }
        }else if(inputPageNo == 'Backward'){
            if(currentPageNo != 1){
                doChange = true;
                movePageNo = currentPageNo-1;
            }

        }else{
            if(inputPageNo != currentPageNo){
                movePageNo = inputPageNo;
                doChange = true;
            }
        }
    }// end of if(shouldReload)

    if(doChange){
        $('#'+idForPaging+' .aPageNo'+ suffix).removeClass('pageActive');
        $('#aPageNo'+ suffix+movePageNo).addClass('pageActive');
        $('#aPageNo'+ suffix+movePageNo).blur();

        renderDataByHtml2(optLimitVal, movePageNo);
    }

    leftH();
}

Paging1 = function(totalCnt, dataSize, pageNo, token){
    totalCnt = parseInt(totalCnt);// 전체레코드수
    dataSize = parseInt(dataSize); // 페이지당 보여줄 데이타수
    var pageSize = parseInt(10); // 페이지 그룹 범위 1 2 3 5 6 7 8 9 10
    pageNo = parseInt(pageNo); // 현재페이지

    var innerHtml = '';

    if(totalCnt == 0){
        $("#"+token).empty().html("");
        return "";
    } // 페이지 카운트

    var pageCnt = totalCnt % dataSize;
    if(pageCnt == 0){
        pageCnt = parseInt(totalCnt / dataSize);
    }else{
        pageCnt = parseInt(totalCnt / dataSize) + 1;
    } var pRCnt = parseInt(pageNo / pageSize);

    if(pageNo % pageSize == 0){
        pRCnt = parseInt(pageNo / pageSize) - 1;
    }

    //이전 화살표
    if(pageNo > pageSize){
        var s2;
        if(pageNo % pageSize == 0){
            s2 = pageNo - pageSize;
        }else{
            s2 = pageNo - pageNo % pageSize;
        }
        innerHtml += '<a href=javascript:goPaging_' + token + '("';
        innerHtml += s2;
        innerHtml += '");>';
        innerHtml += '<i class="fa fa-chevron-left" aria-hidden="true"></i>';
        innerHtml += "</a>";
    }else{
        innerHtml += '<a href=javascript:goPaging_' + token + '("';
        innerHtml += 1;
        innerHtml += '");>';
        innerHtml += '<i class="fa fa-chevron-left" aria-hidden="true"></i>';
        innerHtml += '</a>';
    }

    //paging Bar
    for(var index=pRCnt * pageSize + 1;index<(pRCnt + 1)*pageSize + 1;index++) {
        var active='';
        if(index == pageNo){
            active = 'class="pageActive"';
        }
        innerHtml += '<a href=javascript:goPaging_' + token + '("';
        innerHtml += index;
        innerHtml += '"); '+active+' >';
        innerHtml += index;
        innerHtml += '</a>';

        if(index == pageCnt){
            break;
        }
    }

    //다음 화살표
    if(pageCnt > (pRCnt + 1) * pageSize){
        innerHtml += '<a href=javascript:goPaging_' + token + '("';
        innerHtml += (pRCnt + 1)*pageSize+1;
        innerHtml += '");>';
        innerHtml += '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
        innerHtml += '</a>';
    }else{
        innerHtml += '<a href=javascript:goPaging_' + token + '("';
        innerHtml += pageCnt;
        innerHtml += '");>';
        innerHtml += '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
        innerHtml += '</a>';
    }

    $("#"+token).empty().html(innerHtml);

    renderDataByHtml1(dataSize, pageNo);

    leftH();
}

Paging2 = function(totalCnt, dataSize, pageNo, token){
    totalCnt = parseInt(totalCnt);// 전체레코드수
    dataSize = parseInt(dataSize); // 페이지당 보여줄 데이타수
    var pageSize = parseInt(10); // 페이지 그룹 범위 1 2 3 5 6 7 8 9 10
    pageNo = parseInt(pageNo); // 현재페이지

    var innerHtml = '';

    if(totalCnt == 0){
        $("#"+token).empty().html("");
        return "";
    } // 페이지 카운트

    var pageCnt = totalCnt % dataSize;
    if(pageCnt == 0){
        pageCnt = parseInt(totalCnt / dataSize);
    }else{
        pageCnt = parseInt(totalCnt / dataSize) + 1;
    } var pRCnt = parseInt(pageNo / pageSize);

    if(pageNo % pageSize == 0){
        pRCnt = parseInt(pageNo / pageSize) - 1;
    }

    //이전 화살표
    if(pageNo > pageSize){
        var s2;
        if(pageNo % pageSize == 0){
            s2 = pageNo - pageSize;
        }else{
            s2 = pageNo - pageNo % pageSize;
        }
        innerHtml += '<a href=javascript:goPaging_' + token + '("';
        innerHtml += s2;
        innerHtml += '");>';
        innerHtml += '<i class="fa fa-chevron-left" aria-hidden="true"></i>';
        innerHtml += "</a>";
    }else{
        innerHtml += '<a href=javascript:goPaging_' + token + '("';
        innerHtml += 1;
        innerHtml += '");>';
        innerHtml += '<i class="fa fa-chevron-left" aria-hidden="true"></i>';
        innerHtml += '</a>';
    }

    //paging Bar
    for(var index=pRCnt * pageSize + 1;index<(pRCnt + 1)*pageSize + 1;index++) {
        var active='';
        if(index == pageNo){
            active = 'class="pageActive"';
        }
        innerHtml += '<a href=javascript:goPaging_' + token + '("';
        innerHtml += index;
        innerHtml += '"); '+active+' >';
        innerHtml += index;
        innerHtml += '</a>';

        if(index == pageCnt){
            break;
        }
    }

    //다음 화살표
    if(pageCnt > (pRCnt + 1) * pageSize){
        innerHtml += '<a href=javascript:goPaging_' + token + '("';
        innerHtml += (pRCnt + 1)*pageSize+1;
        innerHtml += '");>';
        innerHtml += '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
        innerHtml += '</a>';
    }else{
        innerHtml += '<a href=javascript:goPaging_' + token + '("';
         innerHtml += pageCnt;
        innerHtml += '");>';
        innerHtml += '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
        innerHtml += '</a>';
    }

    $("#"+token).empty().html(innerHtml);

    renderDataByHtml2(dataSize, pageNo);

    leftH();
}

/*
description : Initialize value and toggle readonly property
                for input tag by OPTION value
return      : EVENT
parameter   : optID   : identify option tag
              textID  : identify input tag
*/
function fn_util_event_changeOption(optID,inpID){
    var $opt = $('#'+optID), $txt = $('#'+inpID);

    $opt.unbind('change').bind('change',function(){
        if( fn_util_isValid_value(optID) ){
            $txt.prop('readonly',false);
        }else{
            $txt.val('');
            $txt.prop('readonly',true);
        }
    });
    $opt.trigger('change');
}

/*
Excel Download
*/
function fn_util_submitForDownloadExcel(controller){
    $("#excelForm").attr({
        action : controller + "/downloadExcel.do",
        method : "POST"
    }).submit();
}

/*
description : Remove ALL attributes except id, class
*/

$.fn.removeAttributes = function() {
  return this.each(function() {
    var attributes = $.map(this.attributes, function(item) {
      return item.name;
    });
    var $target = $(this);
    $.each(attributes, function(i, item) {
    if(item.toUpperCase() != 'ID'
        && item.toUpperCase() != 'CLASS'){
    	$target.removeAttr(item);
    }
    });
  });
}
