function lineChart(chartId, json_data, title_name, p_color, p_unit){

  // echart 관련 js 위치
    require.config({
        paths: {
            echarts: '/js/echarts'
        }
    });

    var col_title = ""; // 열 이름 제목, 첫 번째 열 고정
    var col_data = [] ; // json 데이터에서 두 번째 행부터 시작, 값 필드  ex) ["value","value1"];
    var col_data_name =[]; // 두 번째 행부터 시작, 값 필드 ex) ["판매","2갑"];

    var chart_title =new Array(); // title 배열
    var chart_data = new Array(); // 값 배열


    // 열 머리글, 필드 이름의 값 목록
    var col = 0;
    for(var key in json_data[0]){
        if(col==0)
            col_title = key;
        else
        {
            col_data.push(key);
            col_data_name.push(key);
        }
        col++;
    }

    // 필드 할당 값
    for(var i =0;i<col_data.length;i++){
        chart_data[i] = {
            "name": title_name, // [name1, name2, name3]
            "type":"line",
            "data": [], //[5, 20, 40, 10, 10, 20]
            markPoint : {
                data : [
                    {type : 'max', name: '최대값'},
                    {type : 'min', name: '최소값'}
                ]
            },
            markLine : {
                data : [
                    {type : 'average', name: '평균'}
                ]
            }
        };
    }

    // 제목과 각 데이터 값을 기입
    for(var i=0;i<json_data.length;i++){
        chart_title.push(json_data[i]["title"]);
        for(var j =0;j<col_data.length;j++){
            var col_name = col_data[j];
            chart_data[j].data.push(json_data[i][col_name]);
            //chart_data[1].data.push(json_data[i]["value1"]);
        };
    };

	require(
		[
			'echarts',
			'echarts/theme/macarons',   // 사용 테마 js
			'echarts/chart/line'        // 사용 차트 js
		],
		function (ec) {
			// chartId 에 차트를 그리기 위해 선언
			var myChart = ec.init(document.getElementById(chartId));

			//테마
			myChart.setTheme('macarons')

			//반응형
			window.addEventListener('resize', function () {
            	myChart.resize()
            });

			option = {
			    color:p_color,
				title : {
					show:false,             // tilte 숨김
					text: 'title',          // title 내용
					subtext: 'sub title'    // sub title 내용
				},
				tooltip : {
					trigger: 'axis',
					formatter : '{b} <br/>{a0} : {c0}',
				},
				/*
				legend: {
					show:true,
					data:[title_name] // []
				},
				*/
				toolbox: {
					show : true,
					feature : {
						mark : {show: false},
						dataView : {show: true, readOnly: false, title : '데이터보기', lang: ['데이터보기', '닫기','새로고침']},
						magicType : {show: false, type: ['line', 'bar']},
						restore : {show: true, title : '새로고침'},
						saveAsImage : {show: true, title : '이미지', type : 'png', lang : ['저장']}
					}
				},
				calculable : true,
				xAxis : [
					{
						type : 'category',
						boundaryGap : false,
						data : chart_title, // ['4/1','4/2','4/3','4/4','4/5','4/6','4/7'],
						axisLabel : {
							textStyle:{
								fontFamily:'NanumBarunGothic',
								fontSize:'15'
							}
						},
                         axisLine : {
                             show: true,
                             lineStyle: {
                                 color: 'black'
                             }
                         }
					}
				],
				yAxis : [
					{
						type : 'value',
						scale: true,
                        axisLabel:{
                            formatter:function (params) {
                                var reg = /(^[+-]?\d+)(\d{3})/;
                                params += '';
                                while (reg.test(params))
                                    params = params.replace(reg, '$1' + ',' + '$2');
                                return params + p_unit
                            }
                        },
                         axisLine : {
                             show: true,
                             lineStyle: {
                                 color: 'black'
                             }
                         }
					}
				],
				grid: {y: 80, y2:30, x:100,x2:80},   // 그래프 grid margin
				series : chart_data
			};
			// echarts 객체 데이터를 로드
			myChart.setOption(option);
		}
	);
}

function lineMutiChart(chartId, title_name, dataVal, chart_title){
  // echart 관련 js 위치
    require.config({
        paths: {
            echarts: '/js/echarts'
        }
    });

    var chart_data = new Array(); // 값 배열
    // 필드 할당 값
    for(var i =0;i<title_name.length;i++){
        chart_data[i] = {
            name: title_name[i], // [name1, name2, name3]
            type:"line",
            data: dataVal[i], //[5, 20, 40, 10, 10, 20]
            markPoint : {
                data : [
                    {type : 'max', name: '최대값'},
                    {type : 'min', name: '최소값'}
                ]
            }
        };
    }
	require(
		[
			'echarts',
			'echarts/theme/macarons',   // 사용 테마 js
			'echarts/chart/line'        // 사용 차트 js
		],
		function (ec) {
			// chartId 에 차트를 그리기 위해 선언
			var myChart = ec.init(document.getElementById(chartId));

			//테마
			myChart.setTheme('macarons')

			//반응형
			window.addEventListener('resize', function () {
            	myChart.resize()
            });

			option = {
				title : {
					show:false,             // tilte 숨김
					text: 'title',          // title 내용
					subtext: 'sub title'    // sub title 내용
				},
				tooltip : {
					trigger: 'axis'
				},
				legend: {
					show:true,
					data:title_name,
                    y: 'bottom'
				},
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: false},
                        dataView : {show: true, readOnly: false, title : '데이터보기', lang: ['데이터보기', '닫기','새로고침']},
                        magicType : {show: false, type: ['line', 'bar']},
                        restore : {show: true, title : '새로고침'},
                        saveAsImage : {show: true, title : '이미지', type : 'png', lang : ['저장']}
                    }
                },
				calculable : true,
				xAxis : [
					{
						type : 'category',
						boundaryGap : false,
						data : chart_title,
						axisLine : {
                            show: true,
                            lineStyle: {
                                color: 'black'
                            }
                        }
					}
				],
				yAxis : [
					{
						type : 'value',
						scale: true,
                        axisLabel:{
                            formatter:function (params) {
                                var reg = /(^[+-]?\d+)(\d{3})/;
                                params += '';
                                while (reg.test(params))
                                    params = params.replace(reg, '$1' + ',' + '$2');
                                return params + ' 건'
                            }
                        },
                        axisLine : {
                            show: true,
                            lineStyle: {
                                color: 'black'
                            }
                        }
					}
				],
				grid: {y: 70, y2:60, x:80,x2:80},   // 그래프 grid margin
				series : chart_data
			};
			// echarts 객체 데이터를 로드
			myChart.setOption(option);
		}
	);
}

function lineMutiChart2(chartId, title_name, dataVal, chart_title, p_color){

    // echart 관련 js 위치
    require.config({
        paths: {
            echarts: '/js/echarts'
        }
    });

    var chart_data = new Array(); // 값 배열

    // 필드 할당 값
    for(var i =0;i<title_name.length;i++){

        if(i == title_name.length -2 || i == title_name.length -1) {

            // alert("dataVal[i]: " + dataVal[i][0]);
            // 무제한 99,999,999
            if(dataVal[i][0] != 99999999){

                chart_data[i] = {
                    name: title_name[i], // [name1, name2, name3]
                    type:"line",
                    data: dataVal[i], //[5, 20, 40, 10, 10, 20]
                    "yAxisIndex":[]
                };

                chart_data[i].yAxisIndex.push('1');
            }
        } else{

            chart_data[i] = {
                name: title_name[i], // [name1, name2, name3]
                type:"line",
                data: dataVal[i], //[5, 20, 40, 10, 10, 20]
                markPoint : {
                    data : [
                        {type : 'max', name: '최대값'},
                        {type : 'min', name: '최소값'}
                    ]
                },
                "yAxisIndex":[]
            };

            chart_data[i].yAxisIndex.push('0');
        }
        if(i == title_name.length -2) {
            p_color[i] = '#e7c41e';
        } else if(i == title_name.length -1){
            p_color[i] = '#af466e';
        }

    }

	require(
		[
			'echarts',
			'echarts/theme/macarons',   // 사용 테마 js
			'echarts/chart/line'        // 사용 차트 js
		],
		function (ec) {
			// chartId 에 차트를 그리기 위해 선언
			var myChart = ec.init(document.getElementById(chartId));

			//테마
			myChart.setTheme('macarons')

			//반응형
			window.addEventListener('resize', function () {
            	myChart.resize()
            });

			option = {
			    color:p_color,
				title : {
					show:false,             // tilte 숨김
					text: 'title',          // title 내용
					subtext: 'sub title'    // sub title 내용
				},
				tooltip : {
					trigger: 'axis'
				},
				legend: {
					show:true,
					data:title_name,
                    y: 'bottom'
				},
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: false},
                        dataView : {show: true, readOnly: false, title : '데이터보기', lang: ['데이터보기', '닫기','새로고침']},
                        magicType : {show: false, type: ['line', 'bar']},
                        restore : {show: true, title : '새로고침'},
                        saveAsImage : {show: true, title : '이미지', type : 'png', lang : ['저장']}
                    }
                },
				calculable : true,
				xAxis : [
					{
						type : 'category',
						boundaryGap : false,
						data : chart_title,
						axisLine : {
                            show: true,
                            lineStyle: {
                                color: 'black'
                            }
                        }
					}
				],
				yAxis : [
					{
						type : 'value',
						scale: true,
                        axisLabel:{
                            formatter:function (params) {
                                var reg = /(^[+-]?\d+)(\d{3})/;
                                params += '';
                                while (reg.test(params))
                                    params = params.replace(reg, '$1' + ',' + '$2');
                                return params + ' 건'
                            }
                        },
                        axisLine : {
                            show: true,
                            lineStyle: {
                                color: 'black'
                            }
                        }
					},
                     {
                         type : 'value',
                         splitNumber: 10,
                         splitLine : {
                             show: false
                         },
                         axisLabel:{
                            formatter:function (params) {
                                    var reg = /(^[+-]?\d+)(\d{3})/;
                                    params += '';
                                    while (reg.test(params))
                                        params = params.replace(reg, '$1' + ',' + '$2');
                                    return params + ' 건'
                                }
                            },
                         axisLine : {
                             show: true,
                             lineStyle: {
                                 color: 'black'
                             }
                         }
                     }
				],
				grid: {y: 70, y2:60, x:80,x2:80},   // 그래프 grid margin
				series : chart_data
			};
			// echarts 객체 데이터를 로드
			myChart.setOption(option);
		}
	);
}

function barChart(chartId, json_data, titleData, title_name, yIndex, p_color){

  // echart 관련 js 위치
    require.config({
        paths: {
            echarts: '/js/echarts'
        }
    });

    var col_data = [] ; // json 데이터에서 두 번째 행부터 시작, 값 필드  ex) ["value","value1"];
    var col_data_name =[]; // 두 번째 행부터 시작, 값 필드 ex) ["판매","2갑"];

    var chart_title =new Array(); // title 배열
    var chart_data = new Array(); // 값 배열


    // 열 머리글, 필드 이름의 값 목록
    for(var key in json_data[0]){
        col_data.push(key);
    }

    // 필드 할당 값
    for(var i =0;i<col_data.length;i++){
        chart_data[i] = {
            "name": titleData[i],
            "type":"bar",
            "data": [], //[5, 20, 40, 10, 10, 20]
            "itemStyle" : { normal: {label : {show: true, position: 'inside',textStyle:{color: 'black'}}}},
            "yAxisIndex":[]
        };
        if(i == yIndex) {
            chart_data[i].yAxisIndex.push('1');
        } else{
            chart_data[i].yAxisIndex.push('0');
        }
    }

    // 제목과 각 데이터 값을 기입
    for(var i=0;i<json_data.length;i++){
        for(var j =0;j<col_data.length;j++){
            var col_name = col_data[j];
            chart_data[j].data.push(json_data[i][col_name]);
            //chart_data[1].data.push(json_data[i]["value1"]);
        };
    };

	require(
		[
			'echarts',
			'echarts/theme/macarons',   // 사용 테마 js
			'echarts/chart/bar'        // 사용 차트 js
		],
		function (ec) {
			// chartId 에 차트를 그리기 위해 선언
			var myChart = ec.init(document.getElementById(chartId));

			//테마
			myChart.setTheme('macarons')

			//반응형
			window.addEventListener('resize', function () {
            	myChart.resize()
            });

			option = {
			    color:p_color,
				title : {
					show:false,             // tilte 숨김
					text: 'title',          // title 내용
					subtext: 'sub title'    // sub title 내용
				},
				tooltip : {
					trigger: 'axis'
				},
				legend: {
					show:true,
					data:titleData,
                    y: 'bottom'
				},
				toolbox: {
					show : true,
					feature : {
						mark : {show: false},
						dataView : {show: true, readOnly: false, title : '데이터보기', lang: ['데이터보기', '닫기','새로고침']},
						magicType : {show: false, type: ['line', 'bar']},
						restore : {show: true, title : '새로고침'},
						saveAsImage : {show: true, title : '이미지', type : 'png', lang : ['저장']}
					}
				},
				calculable : true,
				xAxis : [
					{
						type : 'category',
						data : [title_name],
                        axisLine : {
                            show: true,
                            lineStyle: {
                               color: 'black'
                            }
                        }
					}
				],
				yAxis : [
					{
						type : 'value',
						position: 'left',
                        splitArea : {
                             show: true
                        },
                        axisLabel:{
                            formatter:function (params) {
                                var reg = /(^[+-]?\d+)(\d{3})/;
                                params += '';
                                while (reg.test(params))
                                    params = params.replace(reg, '$1' + ',' + '$2');
                                return params + ' 건수'
                            }
                        },
                         axisLine : {
                             show: true,
                             lineStyle: {
                                 color: 'black'
                             }
                         }
					},
                    {
                        type : 'value',
                        splitNumber: 10,
                        splitLine : {
                            show: false
                        },
                        axisLabel:{formatter:'{value} %'},
                        axisLine : {
                            show: true,
                            lineStyle: {
                                color: 'black'
                            }
                        }
                    }
				],
				grid: {y: 70, y2:60, x:100,x2:80},   // 그래프 grid margin
				series : chart_data
			};
			// echarts 객체 데이터를 로드
			myChart.setOption(option);
		}
	);
}

function barMutiChart(chartId, json_data, title_name, yIndex, p_color, barGubun, p_unit){

  // echart 관련 js 위치
    require.config({
        paths: {
            echarts: '/js/echarts'
        }
    });

    var col_title = ""; // 열 이름 제목, 첫 번째 열 고정
    var col_data = [] ; // json 데이터에서 두 번째 행부터 시작, 값 필드  ex) ["value","value1"];
    var col_data_name =[]; // 두 번째 행부터 시작, 값 필드 ex) ["판매","2갑"];

    var chart_title =new Array(); // title 배열
    var chart_data = new Array(); // 값 배열


    // 열 머리글, 필드 이름의 값 목록
    var col = 0;
    for(var key in json_data[0]){
        if(col==0)
            col_title = key;
        else
        {
            col_data.push(key);
            col_data_name.push(key);
        }
        col++;
    }

    // 필드 할당 값
    for(var i =0;i<col_data.length;i++){
        if("N" == barGubun) {
            chart_data[i] = {
                "name": title_name[i], // [name1, name2, name3]
                "type":"bar",
                "data": [], //[5, 20, 40, 10, 10, 20]
            };
        } else {
            chart_data[i] = {
                "name": title_name[i], // [name1, name2, name3]
                "type":"bar",
                "data": [], //[5, 20, 40, 10, 10, 20]
                "itemStyle" : { normal: {label : {show: true, position: 'inside',textStyle:{color: 'black'}}}},
                "yAxisIndex":[]
            };
        }
        if("N" != barGubun) {
            if(i == yIndex) {
                chart_data[i].yAxisIndex.push('1');
            } else{
                chart_data[i].yAxisIndex.push('0');
            }
        }
    }

    // 제목과 각 데이터 값을 기입
    for(var i=0;i<json_data.length;i++){
        chart_title.push(json_data[i]["title"]);
        for(var j =0;j<col_data.length;j++){
            var col_name = col_data[j];
            chart_data[j].data.push(json_data[i][col_name]);
            //chart_data[1].data.push(json_data[i]["value1"]);
        };
    };

	require(
		[
			'echarts',
			'echarts/theme/macarons',   // 사용 테마 js
			'echarts/chart/bar'
		],
		function (ec) {
			// chartId 에 차트를 그리기 위해 선언
			var myChart = ec.init(document.getElementById(chartId));

			//테마
			myChart.setTheme('macarons')

			//반응형
			window.addEventListener('resize', function () {
            	myChart.resize()
            });

            $("#topBtn").click(function() {
                setTimeout(function(){ myChart.resize(); }, 500);
            });

			option = {
			    color:p_color,
				title : {
					show:false,             // tilte 숨김
					text: 'title',          // title 내용
					subtext: 'sub title'    // sub title 내용
				},
				tooltip : {
					trigger: 'axis'
				},
				legend: {
					show:true,
					data:title_name,
					y: 'bottom'
				},
				toolbox: {
					show : true,
					feature : {
						mark : {show: false},
						dataView : {show: true, readOnly: false, title : '데이터보기', lang: ['데이터보기', '닫기','새로고침']},
						magicType : {show: false, type: ['line', 'bar']},
						restore : {show: true, title : '새로고침'},
						saveAsImage : {show: true, title : '이미지', type : 'png', lang : ['저장']}
					}
				},
				calculable : true,
				xAxis : [
					{
						type : 'category',
						data : chart_title,
                        axisLine : {
                            show: true,
                            lineStyle: {
                                color: 'black'
                            }
                        }
					}
				],
				yAxis : [
					{
						type : 'value',
                        position: 'left',
                        splitArea : {
                             show: true
                        },
                        axisLabel:{
                            formatter:function (params) {
                                var reg = /(^[+-]?\d+)(\d{3})/;
                                params += '';
                                while (reg.test(params))
                                    params = params.replace(reg, '$1' + ',' + '$2');
                                return params + p_unit
                            }
                        },
                         axisLine : {
                             show: true,
                             lineStyle: {
                                 color: 'black'
                             }
                         }
					},
                    {
                        type : 'value',
                        splitNumber: 10,
                        splitLine : {
                            show: false
                        },
                        axisLabel:{formatter:'{value} %'},
                        axisLine : {
                            show: true,
                            lineStyle: {
                                color: 'black'
                            }
                        }
                    }
				],
				grid: {y: 70, y2:60, x:100,x2:80},   // 그래프 grid margin
				series : chart_data
			};
			// echarts 객체 데이터를 로드
			myChart.setOption(option);
		}
	);
}

function barLineChart(chartId, titleData, xData, data1, data2, p_color){

  // echart 관련 js 위치
    require.config({
        paths: {
            echarts: '/js/echarts'
        }
    });

	require(
		[
			'echarts',
			'echarts/theme/macarons',   // 사용 테마 js
			'echarts/chart/bar',        // 사용 차트 js
			'echarts/chart/line'        // 사용 차트 js
		],
		function (ec) {
			// chartId 에 차트를 그리기 위해 선언
			var myChart = ec.init(document.getElementById(chartId));

			//테마
			myChart.setTheme('macarons')

			//반응형
			window.addEventListener('resize', function () {
            	myChart.resize()
            });

			option = {
			    color:p_color,
				title : {
				    show:false,             // tilte 숨김
					text: '추천구매액(원) / 추천구매비중(%)',          // title 내용
				},
				tooltip : {
					trigger: 'axis'
				},
				legend: {
					show:true,
					data:titleData,
					y: 'bottom'
				},
				toolbox: {
					show : true,
					feature : {
						mark : {show: false},
						dataView : {show: true, readOnly: false, title : '데이터보기', lang: ['데이터보기', '닫기','새로고침']},
						magicType : {show: false, type: ['line', 'bar']},
						restore : {show: true, title : '새로고침'},
						saveAsImage : {show: true, title : '이미지', type : 'png', lang : ['저장']}
					}
				},
				calculable : true,
				xAxis : [
					{
						type : 'category',
						data : xData,
						axisLabel : {
							textStyle:{
								fontFamily:'NanumBarunGothic',
								fontSize:'15'
							}
						},
                         axisLine : {
                             show: true,
                             lineStyle: {
                                 color: 'black'
                             }
                         }
					},
					{
                        type : 'category',
                        axisLine: {show:false},
                        axisTick: {show:false},
                        axisLabel: {show:false},
                        splitArea: {show:false},
                        splitLine: {show:false},
                        data : xData
                    }
				],
				yAxis : [
				    {
						type : 'value',
						position: 'left',
					    splitArea : {
                            show: true
                        },
                        axisLabel:{
                            formatter:function (params) {
                                var reg = /(^[+-]?\d+)(\d{3})/;
                                params += '';
                                while (reg.test(params))
                                    params = params.replace(reg, '$1' + ',' + '$2');
                                return params + ' 원'
                            }
                        },
                         axisLine : {
                             show: true,
                             lineStyle: {
                                 color: 'black'
                             }
                         }
					},
                    {
                        type : 'value',
                        splitNumber: 10,
                        splitLine : {
                            show: false
                        },
                        axisLabel:{formatter:'{value} %'},
                        axisLine : {
                            show: true,
                            lineStyle: {
                                color: 'black'
                            }
                        }
                    }
				],
				grid: {y: 70, y2:60, x:100,x2:80},   // 그래프 grid margin
				series : [
                    {
                        name: titleData[0],
                        type:'bar',
                        data: data1
                    },
                    {
                        name: titleData[1],
                        type:'line',
                        yAxisIndex: 1,
                        data: data2
                    }
                ]
			};
			// echarts 객체 데이터를 로드
			myChart.setOption(option);
		}
	);
}

function pieChart(chartId, json_data, title_name, dataTitle, p_color){

  // echart 관련 js 위치
    require.config({
        paths: {
            echarts: '/js/echarts'
        }
    });

    var col_title = ""; // 열 이름 제목, 첫 번째 열 고정
    var col_data = [] ; // json 데이터에서 두 번째 행부터 시작, 값 필드  ex) ["value","value1"];
    var col_data_name =[]; // 두 번째 행부터 시작, 값 필드 ex) ["판매","2갑"];

    var chart_title =new Array(); // title 배열
    var chart_data = new Array(); // 값 배열
    var sumVal = 0;

    for(var i =0;i<json_data.length;i++){
        sumVal += json_data[i].value;
    }
    sumVal += '';
    var reg = /(^[+-]?\d+)(\d{3})/;
    while (reg.test(sumVal)) {
        sumVal = sumVal.replace(reg, '$1' + ',' + '$2');
    }

	require(
		[
			'echarts',
			'echarts/chart/pie'        // 사용 차트 js
		],
		function (ec) {
			// chartId 에 차트를 그리기 위해 선언
			var myChart = ec.init(document.getElementById(chartId));

			//테마
			myChart.setTheme('macarons')

			//반응형
			window.addEventListener('resize', function () {
            	myChart.resize()
            });

			option = {
			    color:p_color,
				title : {
					show:false,             // tilte 숨김
					text: 'title',          // title 내용
					subtext: 'sub title'    // sub title 내용
				},
				tooltip : {
					trigger: 'item',
					formatter: "{b}<br/> {c} ({d}%)"
				},
				legend: {
					show:true,
                    data: dataTitle,
                    y: 'bottom'
				},
				toolbox: {
					show : true,
					feature : {
						mark : {show: false},
						dataView : {show: true, readOnly: false, title : '데이터보기', lang: ['데이터보기', '닫기','새로고침']},
						type: ['pie', 'funnel'],
						restore : {show: true, title : '새로고침'},
						saveAsImage : {show: true, title : '이미지', type : 'png', lang : ['저장']}
					}
				},
				calculable : false,
				series : [
				    {
				        name: title_name,
                        type:'pie',
                        radius : ['50%', '70%'],

                        itemStyle : {
                            normal : {
                                label : {
                                    position : 'center',
                                    formatter : function () {
                                        var reg = /(^[+-]?\d+)(\d{3})/;
                                        sumVal += '';
                                        while (reg.test(sumVal))
                                            sumVal = sumVal.replace(reg, '$1' + ',' + '$2');
                                        return sumVal + ' 원'
                                    },
                                    textStyle : {
                                        color : '#d95350',
                                        fontSize : '17',
                                        fontWeight : 'bold'
                                    }
                                },
                                labelLine : {
                                    show : false
                                }
                            },
                            emphasis : {
                                label : {
                                    data:json_data,
                                    show : true,
                                    position : 'outside',
                                    formatter : '{b}',
                                    textStyle : {
                                        fontSize : '13'
                                    }
                                },
                                labelLine : {
                                    show : true
                                }
                            }
                        },
                        data: json_data //[5, 20, 40, 10, 10, 20]
				    }
				]
			};
			// echarts 객체 데이터를 로드
			myChart.setOption(option);
		}
	);
}

function pieChart2(chartId, json_data, title_name, dataTitle, p_color){

  // echart 관련 js 위치
    require.config({
        paths: {
            echarts: '/js/echarts'
        }
    });

    var col_title = ""; // 열 이름 제목, 첫 번째 열 고정
    var col_data = [] ; // json 데이터에서 두 번째 행부터 시작, 값 필드  ex) ["value","value1"];
    var col_data_name =[]; // 두 번째 행부터 시작, 값 필드 ex) ["판매","2갑"];

    var chart_title =new Array(); // title 배열
    var chart_data = new Array(); // 값 배열
    var sumVal = 0;
    var sumVal1 = 0;

    for(var i =0;i<json_data.length;i++){
        sumVal += json_data[i].value;
    }
    sumVal1 = sumVal;
    sumVal += '';
    var reg = /(^[+-]?\d+)(\d{3})/;
    while (reg.test(sumVal)) {
        sumVal = sumVal.replace(reg, '$1' + ',' + '$2');
    }

	require(
		[
			'echarts',
			'echarts/chart/pie'        // 사용 차트 js
		],
		function (ec) {
			// chartId 에 차트를 그리기 위해 선언
			var myChart = ec.init(document.getElementById(chartId));

			//테마
			myChart.setTheme('macarons')

			//반응형
			window.addEventListener('resize', function () {
            	myChart.resize()
            });

			option = {
			    color:p_color,
				title : {
					show:false,             // tilte 숨김
					text: 'title',          // title 내용
					subtext: 'sub title'    // sub title 내용
				},
				tooltip : {
					trigger: 'item',
					formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
					show:true,
                    data: dataTitle,
                    y: 'bottom'
				},
				toolbox: {
					show : true,
					feature : {
						mark : {show: false},
						dataView : {show: true, readOnly: false, title : '데이터보기', lang: ['데이터보기', '닫기','새로고침']},
						type: ['pie', 'funnel'],
						restore : {show: true, title : '새로고침'},
						saveAsImage : {show: true, title : '이미지', type : 'png', lang : ['저장']}
					}
				},
				calculable : false,
				series : [
				    {
                        name:'추천구매총액',
                        type:'pie',
                        radius : 85,
                        itemStyle : {
                            normal : {
                                color : '#FFFFFF',
                                label : {
                                    position : 'center',
                                    formatter : function () {
                                        var reg = /(^[+-]?\d+)(\d{3})/;
                                        sumVal += '';
                                        while (reg.test(sumVal))
                                            sumVal = sumVal.replace(reg, '$1' + ',' + '$2');
                                        return sumVal + ' 원'
                                    },
                                    textStyle : {
                                        color : 'red',
                                        fontSize : '18',
                                        fontWeight : 'bold'
                                    }
                                },
                                labelLine : {
                                    show : false
                                }
                            }
                        },
                        data:[
                            {value: sumVal1, name:'추천구매총액'},
                        ]
                    },
                    {
				        name: title_name, //
                        type:'pie',
                        radius : ['50%', '75%'],
                        data: json_data //[5, 20, 40, 10, 10, 20]
				    }
				]
			};
			// echarts 객체 데이터를 로드
			myChart.setOption(option);
		}
	);
}

function clusteredBarLineChart(chartId, titleData, xData, data, p_color){

  // echart 관련 js 위치
    require.config({
        paths: {
            echarts: '/js/echarts'
        }
    });

	require(
		[
			'echarts',
			'echarts/theme/macarons',   // 사용 테마 js
			'echarts/chart/bar',        // 사용 차트 js
			'echarts/chart/line'        // 사용 차트 js
		],
		function (ec) {
			// chartId 에 차트를 그리기 위해 선언
			var myChart = ec.init(document.getElementById(chartId));

			//테마
			myChart.setTheme('macarons')

			//반응형
			window.addEventListener('resize', function () {
            	myChart.resize()
            });

			option = {
			    color:p_color,
				title : {
				    show:false,             // tilte 숨김
					text: '',          // title 내용
				},
				tooltip : {
					trigger: 'axis'
				},
				legend: {
					show:true,
					data:titleData,
					y: 'bottom'
				},
				toolbox: {
					show : true,
					feature : {
						mark : {show: false},
						dataView : {show: true, readOnly: false, title : '데이터보기', lang: ['데이터보기', '닫기','새로고침']},
						magicType : {show: false, type: ['line', 'bar']},
						restore : {show: true, title : '새로고침'},
						saveAsImage : {show: true, title : '이미지', type : 'png', lang : ['저장']}
					}
				},
				calculable : true,
				xAxis : [
					{
						type : 'category',
						data : xData,
                        axisLine : {
                            show: true,
                            lineStyle: {
                                color: 'black'
                            }
                        }
					},
					{
                        type : 'category',
                        axisLine: {show:false},
                        axisTick: {show:false},
                        axisLabel: {show:false},
                        splitArea: {show:false},
                        splitLine: {show:false},
                        data : xData
                    }
				],
				yAxis : [
				    {
						type : 'value',
						position: 'left',
					    splitArea : {
                            show: true
                        },
                         axisLabel:{
                             formatter:function (params) {
                                 var reg = /(^[+-]?\d+)(\d{3})/;
                                 params += '';
                                 while (reg.test(params))
                                     params = params.replace(reg, '$1' + ',' + '$2');
                                 return params + ' 건'
                             }
                         },
                          axisLine : {
                              show: true,
                              lineStyle: {
                                  color: 'black'
                              }
                          }
					},
                    {
                        type : 'value',
                        splitNumber: 10,
                        splitLine : {
                            show: false
                        },
                         axisLabel:{
                             formatter:function (params) {
                                 var reg = /(^[+-]?\d+)(\d{3})/;
                                 params += '';
                                 while (reg.test(params))
                                     params = params.replace(reg, '$1' + ',' + '$2');
                                 return params + ' sec'
                             }
                         },
                          axisLine : {
                              show: true,
                              lineStyle: {
                                  color: 'black'
                              }
                          }
                    }
				],
				grid: {y: 70, y2:60, x:100,x2:80},   // 그래프 grid margin
				series : [
                    {
                        name: titleData[0],
                        type:'bar',
                        xAxisIndex:1,
                        itemStyle: {normal: {label:{show:true}}},
                        data: data[0]
                    },
                    {
                        name: titleData[1],
                        type:'bar',
                        itemStyle: {normal: {label:{show:true}}},
                        data: data[1]
                    },
                    {
                        name: titleData[2],
                        type:'bar',
                        itemStyle: {normal: {label:{show:true}}},
                        data: data[2]
                    },
                    {
                        name: titleData[3],
                        type:'bar',
                        itemStyle: {normal: {label:{show:true}}},
                        xAxisIndex:1,
                        data: data[3]
                    },
                    {
                        name: titleData[4],
                        type:'line',
                        yAxisIndex: 1,
                        data: data[4]
                    }
                ]
			};
			// echarts 객체 데이터를 로드
			myChart.setOption(option);
		}
	);
}

function barStackChart(chartId, json_data, title_name, title, p_color, p_unit){

  // echart 관련 js 위치
    require.config({
        paths: {
            echarts: '/js/echarts'
        }
    });

    var col_title = ""; // 열 이름 제목, 첫 번째 열 고정
    var col_data = [] ; // json 데이터에서 두 번째 행부터 시작, 값 필드  ex) ["value","value1"];
    var col_data_name =[]; // 두 번째 행부터 시작, 값 필드 ex) ["판매","2갑"];

    var chart_title =new Array(); // title 배열
    var chart_data = new Array(); // 값 배열

    // 필드 할당 값
    for(var i =0;i<json_data.length;i++){
        chart_data[i] = {
            "name": json_data[i]['title2'], // [name1, name2, name3]
            "type":"bar",
            "stack":json_data[i]['title1'],
            "data": json_data[i]['value1'] //[5, 20, 40, 10, 10, 20]
        };
    }

	require(
		[
			'echarts',
			'echarts/theme/macarons',   // 사용 테마 js
			'echarts/chart/bar'
		],
		function (ec) {
			// chartId 에 차트를 그리기 위해 선언
			var myChart = ec.init(document.getElementById(chartId));

			//테마
			myChart.setTheme('macarons')

			//반응형
			window.addEventListener('resize', function () {
            	myChart.resize()
            });

            $("#topBtn").click(function() {
                setTimeout(function(){ myChart.resize(); }, 500);
            });

			option = {
			    color:p_color,
				title : {
					show:false,             // tilte 숨김
					text: 'title',          // title 내용
					subtext: 'sub title'    // sub title 내용
				},
				tooltip : {
					trigger: 'axis'
				},
				legend: {
					show:true,
					data:title_name,
					y: 'bottom'
				},
				toolbox: {
					show : true,
					feature : {
						mark : {show: false},
						dataView : {show: true, readOnly: false, title : '데이터보기', lang: ['데이터보기', '닫기','새로고침']},
						magicType : {show: false, type: ['line', 'bar','stack']},
						restore : {show: true, title : '새로고침'},
						saveAsImage : {show: true, title : '이미지', type : 'png', lang : ['저장']}
					}
				},
				calculable : true,
				xAxis : [
					{
						type : 'category',
						data : title,
                        axisLine : {
                            show: true,
                            lineStyle: {
                                color: 'black'
                            }
                        }
					}
				],
				yAxis : [
					{
						type : 'value',
                        position: 'left',
                        splitArea : {
                             show: true
                        },
                        axisLabel:{
                            formatter:function (params) {
                                var reg = /(^[+-]?\d+)(\d{3})/;
                                params += '';
                                while (reg.test(params))
                                    params = params.replace(reg, '$1' + ',' + '$2');
                                return params + p_unit
                            }
                        }
					}
				],
				grid: {y: 70, y2:60, x:100,x2:80},   // 그래프 grid margin
				series : chart_data
			};
			// echarts 객체 데이터를 로드
			myChart.setOption(option);
		}
	);
}

function pieMutiChart(chartId, json_data1, json_data2, title_name, dataTitle, p_color){

  // echart 관련 js 위치
    require.config({
        paths: {
            echarts: '/js/echarts'
        }
    });

    var col_title = ""; // 열 이름 제목, 첫 번째 열 고정
    var col_data = [] ; // json 데이터에서 두 번째 행부터 시작, 값 필드  ex) ["value","value1"];
    var col_data_name =[]; // 두 번째 행부터 시작, 값 필드 ex) ["판매","2갑"];

    var chart_title =new Array(); // title 배열
    var chart_data = new Array(); // 값 배열

	require(
		[
			'echarts',
			'echarts/chart/pie'        // 사용 차트 js
		],
		function (ec) {
			// chartId 에 차트를 그리기 위해 선언
			var myChart = ec.init(document.getElementById(chartId));

			//테마
			myChart.setTheme('macarons')

			//반응형
			window.addEventListener('resize', function () {
            	myChart.resize()
            });

			option = {
			    color:p_color,
				title : {
					show:false,             // tilte 숨김
					text: 'title',          // title 내용
					subtext: 'sub title'    // sub title 내용
				},
				tooltip : {
					trigger: 'item',
					formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
					show:true,
                    data: dataTitle,
                    y: 'bottom'
				},
				toolbox: {
					show : true,
					feature : {
						mark : {show: false},
						dataView : {show: true, readOnly: false, title : '데이터보기', lang: ['데이터보기', '닫기','새로고침']},
						type: ['pie', 'funnel'],
						restore : {show: true, title : '새로고침'},
						saveAsImage : {show: true, title : '이미지', type : 'png', lang : ['저장']}
					}
				},
				calculable : false,
				series : [
				    {
				        name: title_name[0], //
                        type:'pie',
                        selectedMode: 'single',
                        radius : ['0', '90'],
                        x: '20%',
                        width: '40%',
                        funnelAlign: 'right',
                        max: 1548,
                        itemStyle : {
                            normal : {
                                label : {
                                    position : 'inner'
                                },
                                labelLine : {
                                    show : false
                                }
                            }
                        },
                        data: json_data1
				    },
				    {
                        name: title_name[1], //
                        type:'pie',
                        radius : ['90', '120'],
                        x: '60%',
                        width: '35%',
                        funnelAlign: 'left',
                        max: 1048,
                        data: json_data2
                    }
				]
			};
			// echarts 객체 데이터를 로드
			myChart.setOption(option);
		}
	);
}


function multibarChart(chartId, json_data, title_name, yIndex, p_color,	barGubun, p_unit) {
	// echart 관련 js 위치
	require.config({
		paths : {
			echarts : '/js/echarts'
		}
	});

	var col_title = ""; // 열 이름 제목, 첫 번째 열 고정
	var col_data = []; // json 데이터에서 두 번째 행부터 시작, 값 필드  ex) ["value","value1"];
	var col_data_name = []; // 두 번째 행부터 시작, 값 필드 ex) ["판매","2갑"];

	var chart_title = new Array(); // title 배열
	var chart_data = new Array(); // 값 배열

	// 열 머리글, 필드 이름의 값 목록
	var col = 0;
	for ( var key in json_data[0]) {
		if (col == 0)
			col_title = key;
		else {
			col_data.push(key);
			col_data_name.push(key);
		}
		col++;
	}

	// 필드 할당 값
	for (var i = 0; i < col_data.length; i++) {
		if ("N" == barGubun) {
			chart_data[i] = {
				"name" : title_name[i], // [name1, name2, name3]		
				"type" : "bar",
				"data" : [], //[5, 20, 40, 10, 10, 20]
				"itemStyle" : {
					normal : {
						label : {
							show : true,
							position : 'top'
						}
					}
				}			
			};
		}
		
/*		if ("N" != barGubun) {
			if (i == yIndex) {
				chart_data[i].yAxisIndex.push('1');
			} else {
				chart_data[i].yAxisIndex.push('0');
			}
		}*/
	}

	// 제목과 각 데이터 값을 기입
	for (var i = 0; i < json_data.length; i++) {
		chart_title.push(json_data[i]["title"]);
		for (var j = 0; j < col_data.length; j++) {
			var col_name = col_data[j];
			chart_data[j].data.push(json_data[i][col_name]);
			//chart_data[1].data.push(json_data[i]["value1"]);
		}
		;
	}
	;

	require([ 'echarts', 'echarts/theme/macarons', // 사용 테마 jss
	'echarts/chart/bar' ], function(ec) {
		// chartId 에 차트를 그리기 위해 선언
		var myChart = ec.init(document.getElementById(chartId));

		//테마
		myChart.setTheme('macarons')

		//반응형
		window.addEventListener('resize', function() {
			myChart.resize()
		});

		$("#topBtn").click(function() {
			setTimeout(function() {
				myChart.resize();
			}, 500);
		});

		option = {
			color : p_color,
			title : {
				show : false, // tilte 숨김
				text : 'title', // title 내용
				subtext : 'sub title' // sub title 내용
			},
			tooltip : {
				trigger : 'axis'
			},
			legend : {
				show : true,
				data : title_name,
				y : 'bottom'
			},
			toolbox : {
				show : true,
				feature : {
					mark : {
						show : false
					},
					dataView : {
						show : true,
						readOnly : false,
						title : '데이터보기',
						lang : [ '데이터보기', '닫기', '새로고침' ]
					},
					magicType : {
						show : false,
						type : [ 'line', 'bar' ]
					},
					restore : {
						show : true,
						title : '새로고침'
					},
					saveAsImage : {
						show : true,
						title : '이미지',
						type : 'png',
						lang : [ '저장' ]
					}
				}
			},
			calculable : true,
			xAxis : [ {
				type : 'category',
				data : chart_title,
				axisLine : {
					show : true,
					lineStyle : {
						color : 'black'
					}
				}
			} ],
			yAxis : [ {
				type : 'value',
				position : 'left',
				splitArea : {
					show : true
				},
				axisLabel : {
					formatter : function(params) {
						var reg = /(^[+-]?\d+)(\d{3})/;
						params += '';
						while (reg.test(params))
							params = params.replace(reg, '$1' + ',' + '$2');
						return params + p_unit
					}
				},
				axisLine : {
					show : true,
					lineStyle : {
						color : 'black'
					}
				}
			}, {
				type : 'value',
				splitNumber : 10,
				splitLine : {
					show : false
				},
				axisLabel : {
					formatter : '{value} %'
				},
				axisLine : {
					show : true,
					lineStyle : {
						color : 'black'
					}
				}
			} ],
			grid : {
				y : 70,
				y2 : 60,
				x : 100,
				x2 : 80
			}, // 그래프 grid margin
			series : chart_data
			
		};
		// echarts 객체 데이터를 로드		
		myChart.setOption(option);
		
	});
}



function barOverlabChart(chartId, json_data, title_name, yIndex, p_color,
		barGubun, p_unit) {
	// echart 관련 js 위치
	require.config({
		paths : {
			echarts : '/js/echarts'
		}
	});

	var col_title = ""; // 열 이름 제목, 첫 번째 열 고정
	var col_data = []; // json 데이터에서 두 번째 행부터 시작, 값 필드  ex) ["value","value1"];
	var col_data_name = []; // 두 번째 행부터 시작, 값 필드 ex) ["판매","2갑"];

	var chart_title = new Array(); // title 배열
	var chart_data = new Array(); // 값 배열

	// 열 머리글, 필드 이름의 값 목록
	var col = 0;
	for ( var key in json_data[0]) {
		if (col == 0)
			col_title = key;
		else {
			col_data.push(key);
			col_data_name.push(key);
		}
		col++;
	}

	// 필드 할당 값
	for (var i = 0; i < col_data.length; i++) {
		if ("N" == barGubun) {
			chart_data[i] = {
				"name" : title_name[i], // [name1, name2, name3]
				"type" : "bar",
				"data" : [], //[5, 20, 40, 10, 10, 20]
			};
		} else {
			chart_data[i] = {
				"name" : title_name[i], // [name1, name2, name3]
				"type" : "bar",
				"barGap" : "-100%",
				"data" : [], //[5, 20, 40, 10, 10, 20]
				"itemStyle" : {
					normal : {
						label : {
							show : true,
							position : 'top'
						}
					}
				},
				"yAxisIndex" : []
			};
		}
		if ("N" != barGubun) {
			if (i == yIndex) {
				chart_data[i].yAxisIndex.push('1');
			} else {
				chart_data[i].yAxisIndex.push('0');
			}
		}
	}

	// 제목과 각 데이터 값을 기입
	for (var i = 0; i < json_data.length; i++) {
		chart_title.push(json_data[i]["title"]);
		for (var j = 0; j < col_data.length; j++) {
			var col_name = col_data[j];
			chart_data[j].data.push(json_data[i][col_name]);
			//chart_data[1].data.push(json_data[i]["value1"]);
		}
		;
	}
	;

	require([ 'echarts', 'echarts/theme/macarons', // 사용 테마 jss
	'echarts/chart/bar' ], function(ec) {
		// chartId 에 차트를 그리기 위해 선언
		var myChart = ec.init(document.getElementById(chartId));

		//테마
		myChart.setTheme('macarons')

		//반응형
		window.addEventListener('resize', function() {
			myChart.resize()
		});

		$("#topBtn").click(function() {
			setTimeout(function() {
				myChart.resize();
			}, 500);
		});

		option = {
			color : p_color,
			title : {
				show : false, // tilte 숨김
				text : 'title', // title 내용
				subtext : 'sub title' // sub title 내용
			},
			tooltip : {
				trigger : 'axis'
			},
			legend : {
				show : true,
				data : title_name,
				y : 'bottom'
			},
			toolbox : {
				show : true,
				feature : {
					mark : {
						show : false
					},
					dataView : {
						show : true,
						readOnly : false,
						title : '데이터보기',
						lang : [ '데이터보기', '닫기', '새로고침' ]
					},
					magicType : {
						show : false,
						type : [ 'line', 'bar' ]
					},
					restore : {
						show : true,
						title : '새로고침'
					},
					saveAsImage : {
						show : true,
						title : '이미지',
						type : 'png',
						lang : [ '저장' ]
					}
				}
			},
			calculable : true,
			xAxis : [ {
				type : 'category',
				data : chart_title,
				axisLine : {
					show : true,
					lineStyle : {
						color : 'black'
					}
				}
			} ],
			yAxis : [ {
				type : 'value',
				position : 'left',
				splitArea : {
					show : true
				},
				axisLabel : {
					formatter : function(params) {
						var reg = /(^[+-]?\d+)(\d{3})/;
						params += '';
						while (reg.test(params))
							params = params.replace(reg, '$1' + ',' + '$2');
						return params + p_unit
					}
				},
				axisLine : {
					show : true,
					lineStyle : {
						color : 'black'
					}
				}
			}, {
				type : 'value',
				splitNumber : 10,
				splitLine : {
					show : false
				},
				axisLabel : {
					formatter : '{value} %'
				},
				axisLine : {
					show : true,
					lineStyle : {
						color : 'black'
					}
				}
			} ],
			grid : {
				y : 70,
				y2 : 60,
				x : 100,
				x2 : 80
			}, // 그래프 grid margin
			series : chart_data
			
		};
		// echarts 객체 데이터를 로드		
		myChart.setOption(option);
		
	});
}