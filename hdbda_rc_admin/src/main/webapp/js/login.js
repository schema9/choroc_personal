var emailRegExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
var domainUrlRegExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
var re_url = /^(https?:\/\/)?([a-z\d\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?$/; // URL 검사식

$(window).ready(function(){
	maskOn();
	$('.chgPsdBox').fadeIn();
	
});

$(function(){
	$('.user-add a').click(function(){
		$('.persinfoBox').fadeIn();
		maskOn();
	});

	$('.persinfoCancel,.closePop').click(function(){
        $("#persinfoCheck").attr('checked', false) ;
        $("#mallName").val("");
        $("#domainUrl").val("");
        $("#delegateEmail").val("");
		$('.persinfoBox').fadeOut();
		maskOff();
	});

	$('.logbtn').click(function(){
		$('.chgPsdBox').fadeOut();
		maskOff();
	})
});