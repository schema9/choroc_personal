//========================================================================
//===== Utilities Ajax ===================================================
//========================================================================

/*
description : ajax in basic
parameter   : param     : jon object; {'id' : id}
              url       : string;
              onSuccess_fn  : function; fn_setSomething
              onError_fn    : function; fn_setSomething
note        : the type is FIXED to POST
*/
function fn_util_ajax_basic(param, url, onSuccess_fn, onError_fn){
	
    $.ajax({
        type: 'POST',
        url: url,
        data: param,
        async:false,
        success: onSuccess_fn,
        error : onError_fn
    });
}

function fn_util_ajax_basic_return(param, url){
    var result = false;
    $.ajax({
        type: 'POST',
        url: url,
        data: param,
        async:false,
        success: function(data){
            if(data != false){
                result = true;
            }
        },
        error : function(e){

        }
    });
    return result;
}

/*
description : ajax in basic
parameter   : formID    : string
              url       : string;
note        : the type is FIXED to POST
*/
function fn_util_ajax_serialize(formID, url){
		
    var param = $('#'+formID).serialize();
    $.ajax({
        type: 'POST',
        url: url,
        data: param,
        crossDomain:true,
        async:false,
        success:function(resultData){
        	
            if(resultData != null){
            	/*                
  				if(0 != resultData.HashMap.result) {
                    alert('처리되었습니다.');
                } else {
                    alert('관리자에게 문의 바랍니다.');
                }*/
                location.reload();
            }
        },
        error:function(e){
            alert('서버와의 통신에 실패하였습니다.');
        }
    });
}

function fn_ajax_serialize(formID, url){
	
    var param = $('#'+formID).serialize(); 
    $.ajax({
        type: 'POST',
        url: url,
        data: param,
        crossDomain:true,
        async:false,
        success:function(resultData){

            if(resultData != null){
                if(0 != resultData.HashMap.result) {
                    alert('처리되었습니다.');
                } else {
                    alert('관리자에게 문의 바랍니다.');
                }
                location.reload(true);
            }
        },
        error:function(e){
            alert('서버와의 통신에 실패하였습니다.');
        }
    });
}

function fn_util_ajax_onError_errorMsg(e){
    alert('error: ' + e.responseText);
}

function fn_util_ajax_onError_alert(e){
    alert('서버와의 통신에 실패하였습니다.');
}

function fn_util_ajax_onError_return(e){
    alert('서버와의 통신에 실패하였습니다.');
    return false;
}