//========================================================================
//===== Utilities Date ===================================================
//========================================================================

/* INDEX
#datePickerId1  :   basic
#datePickerId2  :   duplicated with '#datePickerId1'
#datePickerId3  :
#datePickerId4  :   Ranking pages ; enable to select 'A DAY' which means
                    able to select same day for start and end date
*/
$(function() {

        var cb = function(start, end, label) {
		    dateFunction1(start.format('YYYYMMDD'), end.format('YYYYMMDD'));
	    }

        $('#datePickerId1').daterangepicker({
            "ranges": {
                "지난60일": [
                    moment().subtract(60, 'days'),
                    moment().subtract(1, 'days')
                ],
                "지난30일": [
                    moment().subtract(30, 'days'),
                    moment().subtract(1, 'days')
                ],
                "지난7일": [
                    moment().subtract(7, 'days'),
                    moment().subtract(1, 'days')
                ],
                "이번달": [
                    moment().startOf('month'),
                    moment().subtract(1, 'days')
                ]
            },
            locale: {
              format: 'YYYY/MM/DD'
            },
            "startDate": moment().subtract(7, 'days'),
            "endDate": moment().subtract(1, 'days'),
            "opens": "left"
        }, cb);
});


$(function() {

    var cb = function(start, end, label) {
	    dateFunction10(start.format('YYYYMMDD'), end.format('YYYYMMDD'));
    }
    
    $('#datePickerId10').daterangepicker10({

        locale: {
          format: 'YYYY/MM/DD'
        },

    "startDate": moment().subtract(6, 'days'),
    "endDate": moment().subtract(0, 'days'),
    "opens": "left"
    }, cb);
});


$(function() {

    var cb = function(start, end, label) {
        dateFunction2(start.format('YYYYMMDD'), end.format('YYYYMMDD'));
    }

    $('#datePickerId2').daterangepicker({
        "ranges": {
            "지난60일": [
                moment().subtract(60, 'days'),
                moment().subtract(1, 'days')
            ],
            "지난30일": [
                moment().subtract(30, 'days'),
                moment().subtract(1, 'days')
            ],
            "지난7일": [
                moment().subtract(7, 'days'),
                moment().subtract(1, 'days')
            ],
            "이번달": [
                moment().startOf('month'),
                moment().subtract(1, 'days')
            ]
        },
        locale: {
          format: 'YYYY/MM/DD'
        },
        "startDate": moment().subtract(7, 'days'),
        "endDate": moment().subtract(1, 'days'),
        "opens": "left"
    }, cb);
});

$(function() {

    var cb = function(start, label) {
               dateFunction3(start.format('YYYYMMDD'));
             }

        $('#datePickerId3').daterangepicker({

            locale: {
              format: 'YYYY/MM/DD'
            },
        "singleDatePicker": true,

        "startDate": moment().subtract(1, 'days'),

        "opens": "left"
        }, cb);
});

$(function() {

    var cb = function(start, end, label) {
        dateFunction4(start.format('YYYYMMDD'), end.format('YYYYMMDD'));
    }

    $('#datePickerId4').daterangepicker({
        "ranges": {
            "지난60일": [
                moment().subtract(60, 'days'),
                moment().subtract(1, 'days')
            ],
            "지난30일": [
                moment().subtract(30, 'days'),
                moment().subtract(1, 'days')
            ],
            "지난7일": [
                moment().subtract(7, 'days'),
                moment().subtract(1, 'days')
            ],
            "이번달": [
                moment().startOf('month'),
                moment().subtract(1, 'days')
            ]
        },
        locale: {
          format: 'YYYY/MM/DD'
        },
        "startDate": moment().subtract(1, 'days'),
        "endDate": moment().subtract(1, 'days'),
        "opens": "left"
    }, cb);
});

$(function() {

    var cb = function(start, end, label) {
        dateFunction5(start.format('YYYYMMDD'), end.format('YYYYMMDD'));
    }

    $('#datePickerId5').daterangepicker({
        locale: {
          format: 'YYYY/MM/DD',
        },
        "startDate": moment().subtract(1, 'days'),
        "endDate": moment().subtract(1, 'days'),
        "opens": "left",
        "minDate":  moment().subtract(60, 'days'),
        "maxDate":  moment().subtract(0, 'days')
    }, cb);
});

$(function() {

    var cb = function(start, end, label) {
        dateFunction6(start.format('YYYYMMDD'), end.format('YYYYMMDD'));
    }

    $('#datePickerId6').daterangepicker_unlimit({
        "ranges": {
            "1년": [
                moment().subtract(0, 'days'),
                moment().subtract(-365, 'days')
            ],
            "2년": [
                moment().subtract(0, 'days'),
                moment().subtract(-730, 'days')
            ],
            "3년": [
                moment().subtract(0, 'days'),
                moment().subtract(-1095, 'days')
            ],
            "5년": [
                moment().subtract(0, 'days'),
                moment().subtract(-1825, 'days')
            ],
            "10년": [
                moment().startOf('month'),
                moment().subtract(-3650, 'days')
            ]
        },
        locale: {
          format: 'YYYY/MM/DD',
        },
        "startDate": moment().subtract(1, 'days'),
        "endDate": moment().subtract(0, 'days'),
        "opens": "left",
        "minDate":  moment().subtract(0, 'days'),
        "maxDate":  moment().subtract(-36500, 'days')
    }, cb);
});


$(function() {

    var cb = function(start, end, label) {
        dateFunction7(start.format('YYYYMMDD'), end.format('YYYYMMDD'));
    }

    $('#datePickerId7').daterangepicker_unlimit({
        "ranges": {
            "1년": [
                moment().subtract(0, 'days'),
                moment().subtract(-365, 'days')
            ],
            "2년": [
                moment().subtract(0, 'days'),
                moment().subtract(-730, 'days')
            ],
            "3년": [
                moment().subtract(0, 'days'),
                moment().subtract(-1095, 'days')
            ],
            "5년": [
                moment().subtract(0, 'days'),
                moment().subtract(-1825, 'days')
            ],
            "10년": [
                moment().startOf('month'),
                moment().subtract(-3650, 'days')
            ]
        },
        locale: {
          format: 'YYYY/MM/DD',
        },
        "startDate": moment().subtract(1, 'days'),
        "endDate": moment().subtract(0, 'days'),
        "opens": "left",
        "minDate":  moment().subtract(0, 'days'),
        "maxDate":  moment().subtract(-36500, 'days')
    }, cb);
});

$(function() {

        var cb = function(start, end, label) {
		    dateFunction8(start.format('YYYY/MM'));
	    }

        $('#datePickerId8').daterangepicker_unlimit({
            locale: {
              format: 'YYYY/MM'
            },
            "opens": "left",
            "singleDatePicker": true,
            "minDate":  moment().subtract(36500, 'days'),
            "maxDate":  moment().subtract(0, 'days')
        }, cb);
});


$(function() {

    var cb = function(start, end, label) {
	    dateFunction9(start.format('YYYYMMDD'), end.format('YYYYMMDD'));
    }

    $('#datePickerId9').daterangepicker({
        "ranges": {
            "지난60일": [
                moment().subtract(60, 'days'),
                moment().subtract(1, 'days')
            ],
            "지난30일": [
                moment().subtract(30, 'days'),
                moment().subtract(1, 'days')
            ],
            "지난7일": [
                moment().subtract(7, 'days'),
                moment().subtract(1, 'days')
            ],
            "이번달": [
                moment().startOf('month'),
                moment().subtract(1, 'days')
            ]
        },
        locale: {
          format: 'YYYY/MM/DD'
        },
        "startDate": moment().subtract(7, 'days'),
        "endDate": moment().subtract(1, 'days'),
        "opens": "left"
    }, cb);
});



/*
description : parse dateString from data-base
parameter   : date          : string; dateString (1452178800000)
              displayDate   : boolean; if true for return date
              displayTime   : boolean; if true for return time
              separator     : string; date separator; '-', '/', etc.
example     : (1452223870000) -> (2016-01-08 12:31:10)
*/
function fn_util_formatDate(date, displayDate, displayTime, separator) {
    var result  = '',
        dt      = new Date(date),
        year    = dt.getFullYear(),
        month   = '' + (dt.getMonth() + 1),
        day     = '' + dt.getDate(),
        hh      = dt.getHours(),
        mi      = dt.getMinutes(),
        ss      = dt.getSeconds();

    month = fn_util_numberToString_prefixZero(month);
    day = fn_util_numberToString_prefixZero(day);
    hh = fn_util_numberToString_prefixZero(hh);
    mi = fn_util_numberToString_prefixZero(mi);
    ss = fn_util_numberToString_prefixZero(ss);

    if(displayDate){
        result = [year, month, day].join(separator);
    }
    if(displayTime){
        result += ' '+hh+':'+mi+':'+ss;
    }
    return result;
}

/*
description : parse dateString from data-base by given format
return      : string
parameter   : date          : string; dateString (1452178800000)
              fmt   : string;  format form;
note        : [ fmt ] allowed formats as follow
              yyyy   : for Year with 4 digit number (2016)
              mm     : for month with 2 digit number (01)
              dd     : for day with 2 digit number (08)
              hh     : for 24 hours with 2 digit number (12)
              mi     : for minutes with 2 digit number (31)
              ss     : for second with 2 digit number (10)
example     : ('1452223870000','yyyy-mm-dd hh:mi:ss') -> (2016-01-08 12:31:10)
*/
function fn_util_formatDate_toFmt(date, fmt) {
    var result  = '',
        dt      = new Date(date),
        year    = dt.getFullYear(),
        month   = '' + (dt.getMonth() + 1),
        day     = '' + dt.getDate(),
        hh      = dt.getHours(),
        mi      = dt.getMinutes(),
        ss      = dt.getSeconds();

    month = fn_util_numberToString_prefixZero(month);
    day = fn_util_numberToString_prefixZero(day);
    hh = fn_util_numberToString_prefixZero(hh);
    mi = fn_util_numberToString_prefixZero(mi);
    ss = fn_util_numberToString_prefixZero(ss);

    fmt = fmt.toLowerCase();
    result = fmt.replace('yyyy',year)
                .replace('mm',month).replace('dd',day)
                .replace('hh',hh).replace('mi',mi)
                .replace('ss',ss);

    return result;
}

/*
description : get date string by given format
return      : string
parameter   : ymd   : string;   yyyymmdd (20101010)
              fmt   : string;  format form; yyyy; mm; dd;
note        : [ ymd ] must be yyyymmdd format
              [ fmt ] allowed string 'yyyy', 'mm' and 'dd'
example     : '20100505','yyyy-mm/dd' => '2010-05/05'
              '20100505','mm_dd' => '05_05'
*/
function fn_util_getDate_ymdToFmt(ymd,fmt){
    var result  = '',
        year    = ymd.substr(0,4),
        month   = ymd.substr(4,2),
        day     = ymd.substr(6,2);
    if(!fn_util_isValid_date_ymd(ymd)){
        return result;
    }
    month = fn_util_numberToString_prefixZero(month);
    day = fn_util_numberToString_prefixZero(day);
    fmt = fmt.toLowerCase();
    result = fmt.replace('yyyy',year)
                .replace('mm',month).replace('dd',day);

    return result;
       
}

