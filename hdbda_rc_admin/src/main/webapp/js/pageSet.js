function adminMenu(){

/*
		<!-- 서비스만 있을 경우 -->
		<div class="adminTit" style="display:none">
			lotte.com Service 01 / lotte.com Service 01
		</div>
*/

var cDisable = 'disable';
var cActive = '';

var domainNm = '도메인 그룹을 선택하세요.';
var seriveNm = '서비스를 선택하세요.';

var html = "";

if( $("#pageSet").val() != 'COM_MAIN_001'){
	cDisable = '';
	cActive = 'active';
	domainNm = 'lotte.com';
	seriveNm = 'lotte.com Service 3';
}
			
			html += '		<div class="adminMenu" id="adminMenu">';

			html += '			<div class="tmBox totalBox"><a href="COM_MAIN_001.html">전체서비스 <span>12</span></a></div>';
			
			html += '			<div class="tmBox svList">';
			html += '				<div class="tmBoxTop" ><a href="#" id="domainGroup">'+domainNm+'</a><i class="ti-angle-down"></i></div>';
			html += '				<ul id="domainList">';
			html += '					<li>도메인 그룹1</li>';
			html += '					<li>도메인 그룹2</li>';
			html += '					<li class='+cActive+'>lotte.com</li>';
			html += '					<li>도메인 그룹4</li>';
			html += '					<li>도메인 그룹5</li>';
			html += '				</ul>';
			html += '			</div>';
			
			html += '			<div class="tmBox svList '+cDisable+'">';
			html += '				<div class="tmBoxTop" ><a href="#" id="serviceGroup">'+seriveNm+'</a><i class="ti-angle-down"></i></div>';
			html += '				<ul id="serviceList">';
			html += '					<li>lotte.com Service 1</li>';
			html += '					<li>lotte.com Service 2</li>';
			html += '					<li class='+cActive+'>lotte.com Service 3</li>';
			html += '					<li>lotte.com Service 4</li>';
			html += '					<li>lotte.com Service 5</li>';
			html += '				</ul>			';
			html += '			</div>';
			html += '		</div>';

			html += '				<div class="adminArea">';
			html += '					<ul>';
			html += '						<li class="member"><a href="#" onclick="return false;" id="mMenu"><b class="logInfo">Admindmindmin@ldcc.co.kr</b> 님 접속중</a></li>';
			html += '						<li><a href="COM_LOGI_001.html"><i class="ti-power-off"></i> 로그아웃</a></li>';
			html += '					</ul>';
			html += '				</div>';


	$("#lecoTop").html(html);


}

//function memberMenu(){
//	
//	if( $(".admSubmenu").css('display') == 'none' ){
//		$("#admSubmenu").slideDown();
//	}else{
//		$("#admSubmenu").slideUp();
//	}
//}
//
//$(function(){
//
//
//
//});


$(window).ready(function(){

	adminMenu();

//	$(".adminArea ul li.member a").click(function(){
//		memberMenu();
//	});
//
//	$('body').on('click', function(e) {
//
//		if(e.target.id != 'admSubmenu' && e.target.id != 'mMenu' && e.target.className != 'logInfo'){
//			$("#admSubmenu").slideUp();
//		}
//	});


	var svList = true;
/*
	$("#domainGroup").click(function(){
		if( !$("#domainGroup").parent().parent().hasClass('disable') ){
			$("#domainList").slideDown('300');
			$("#domainGroup").parent().find('i').attr('class','ti-angle-up');
		}
	});
*/

	$("#serviceGroup").click(function(){
		
		if( !$("#serviceGroup").parent().parent().hasClass('disable') ){
			$("#serviceList").slideDown('300');
			$("#serviceGroup").parent().find('i').attr('class','ti-angle-up');
		}
	});
	

	$('body').on('click', function(e) {
		
		if(e.target.id != 'serviceList' && e.target.id != 'serviceGroup'){
			$("#serviceList").slideUp('300');
			$("#serviceGroup").parent().find('i').attr('class','ti-angle-down');
		}

		if(e.target.id != 'domainList' && e.target.id != 'domainGroup'){
			$("#domainList").slideUp('300');
			$("#domainGroup").parent().find('i').attr('class','ti-angle-down');
		}
	});


	$("#domainList li").click(function(){
		
		$("#domainList li").removeClass('active');
		$(this).addClass('active');
		$("#domainGroup").text($(this).text());

		$("#serviceGroup").parent().parent().removeClass('disable');
	
	});

/*
	$("#serviceList li").click(function(){
		
		$("#serviceList li").removeClass('active');
		$(this).addClass('active');
		$("#serviceGroup").text($(this).text());
		
		location.href="COM_MAIN_002.html";
	
	});
*/
});