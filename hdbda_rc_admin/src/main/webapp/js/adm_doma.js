//메뉴
	var recoMenu = new Array(10);
	
	for (var i = 0; i < 10; i++) {
		recoMenu[i] = new Array(10); 
		for (var k = 0; k < 10; k++) {
			recoMenu[i][k] = new Array(10); 
		}
	}
	
	//메뉴 1-0-0
	recoMenu[1][0][0] = "서비스 현황분석";

	//메뉴 1-1-0	
	recoMenu[1][1][0] = "로그분석조회";
	recoMenu[1][1][1] = "페이지뷰 현황(PV/UV)";
	recoMenu[1][1][2] = "실시간 페이지뷰 현황(PV/UV)";

	//메뉴 1-2-0	
	recoMenu[1][2][0] = "사용자행동데이터조회";
	recoMenu[1][2][1] = "제품상세페이지뷰";
	recoMenu[1][2][2] = "제품구매페이지뷰";
	recoMenu[1][2][3] = "서비스 사용자 행동기록 통계";
	recoMenu[1][2][4] = "사용자별 최근 조회 제품";
	recoMenu[1][2][5] = "사용자별 최근 구매 제품";

	//메뉴 2-0-0		
	recoMenu[2][0][0] = "추천성과분석";

	//메뉴 2-1-0		
	recoMenu[2][1][0] = "추천";
	recoMenu[2][1][1] = "추천 대시보드";
	recoMenu[2][1][2] = "추천 영역별";
	recoMenu[2][1][3] = "추천 SET별";
	recoMenu[2][1][4] = "추천 알고리즘";
	recoMenu[2][1][5] = "방문자 대비 주문 페이지 통계";
	recoMenu[2][1][6] = "서비스 별 추천결과";
	recoMenu[2][1][7] = "추천영역 별 결과";

	//메뉴 3-0-0		
	recoMenu[3][0][0] = "랭킹 TOP 100";

	//메뉴 3-1-0			
	recoMenu[3][1][0] = "TOP 100";
	recoMenu[3][1][1] = "추천 페이지뷰 TOP 100";
	recoMenu[3][1][2] = "추천 클릭율 TOP 100";
	recoMenu[3][1][3] = "추천 유입율 TOP 100";
	recoMenu[3][1][4] = "추천 구매전환율 TOP 100";
	recoMenu[3][1][5] = "추천 베스트셀러 TOP 100";
	recoMenu[3][1][6] = "추천 알고리즘 페이지뷰 TOP 100";

	//메뉴 4-0-0			
	recoMenu[4][0][0] = "추천환경관리";

	//메뉴 4-1-0				
	recoMenu[4][1][0] = "추천알고리즘설정";
	recoMenu[4][1][1] = "제품기반 추천";
	recoMenu[4][1][2] = "사용자기반 추천";
	recoMenu[4][1][3] = "인기몰기반 추천";
	recoMenu[4][1][4] = "실시간 인기제품 추천";

	//메뉴 4-2-0				
	recoMenu[4][2][0] = "추천알고리즘SET설정";
	recoMenu[4][2][1] = "추천화면 알고리즘 SET";

	//메뉴 4-3-0				
	recoMenu[4][3][0] = "추천화면 설정";
	recoMenu[4][3][1] = "제품추천화면 관리";

	//메뉴 4-4-0				
	recoMenu[4][4][0] = "추천위젯 설정";
	recoMenu[4][4][1] = "추천화면 위젯설정";

	//메뉴 4-5-0				
	recoMenu[4][5][0] = "제품관리 설정";
	recoMenu[4][5][1] = "전체 제품 품질관리";

	//메뉴 5-0-0				
	recoMenu[5][0][0] = "시스템 관리";

	//메뉴 5-1-0			
	recoMenu[5][1][0] = "모니터링";
	recoMenu[5][1][1] = "트래픽 관리";

	//메뉴 5-2-0		
	recoMenu[5][2][0] = "배치관리";
	recoMenu[5][2][1] = "job Param 설정";

	//메뉴 5-3-0		
	recoMenu[5][3][0] = "알고리즘 관리";
	recoMenu[5][3][1] = "추천알고리즘 Type 관리";
	recoMenu[5][3][2] = "추천알고리즘 관리";

	//메뉴 5-4-0		
	recoMenu[5][4][0] = "시스템환경 관리";
	recoMenu[5][4][1] = "도메인DB 접속 관리";

	//메뉴 6-0-0		
	recoMenu[6][0][0] = "관리자 권한";
	
	//메뉴 6-1-0			
	recoMenu[6][1][0] = "사용자 관리";
	recoMenu[6][1][1] = "사용자 계정관리";
	recoMenu[6][1][2] = "사용자 메뉴 권한 관리";

	//메뉴 6-2-0			
	recoMenu[6][2][0] = "도메인 관리";
	recoMenu[6][2][1] = "도메인 관리";
	recoMenu[6][2][2] = "사이트 관리";
	recoMenu[6][2][3] = "서비스 ID 관리";

	//메뉴 6-3-0			
	recoMenu[6][3][0] = "Mall 관리";
	recoMenu[6][3][1] = "몰 전체 제품관리";
	recoMenu[6][3][2] = "제품상세정보";
	recoMenu[6][3][3] = "몰 전체 회원관리";

	//메뉴 7-0-0	
	recoMenu[7][0][0] = "MY정보/가이드";

	//메뉴 7-1-0	
	recoMenu[7][1][0] = "Profile";
	recoMenu[7][1][1] = "개인정보 수정";

	//메뉴 7-2-0	
	recoMenu[7][2][0] = "Notice";
	recoMenu[7][2][1] = "공지사항";
	recoMenu[7][2][2] = "자주묻는 질문(FAQ)";

	//메뉴 7-3-0	
	recoMenu[7][3][0] = "가이드";
	recoMenu[7][3][1] = "API 가이드";

//메뉴설정

function inTable(){

	var menuSetTbl = '';
	
	menuSetTbl += '<colgroup>';
	menuSetTbl += '<col width="80">';
	menuSetTbl += '<col width="80">';
	menuSetTbl += '<col width="400">';
	menuSetTbl += '<col width="400">';
	menuSetTbl += '<col width="200">';
	menuSetTbl += '<col width="200">';
	menuSetTbl += '<col width="120">';
	menuSetTbl += '</colgroup>';

	menuSetTbl += '<tr>';
	menuSetTbl += '<th>레벨</th>';
	menuSetTbl += '<th>순서</th>';
	menuSetTbl += '<th>메뉴</th>';
	menuSetTbl += '<th>주소</th>';
	menuSetTbl += '<th>등록일</th>';
	menuSetTbl += '<th>수정일</th>';
	menuSetTbl += '<th>상세관리</th>';
	menuSetTbl += '</tr>';

	var lvClass="";
	var lvDepth="";
	var lvIndex="";
	//lvAdepth

	for(var i='0'; i<'10'; i++){
		
		menuSetTbl += '<tbody class="tbGroup">';

		for(var j='0'; j<'10'; j++){
			for(var k='0'; k<'10'; k++){
				
				if(recoMenu[i][j][k]){
					
					if(i != '0' && j=='0' && k=='0'){
						lvClass="lvAdepth";
						lvDepth="1";
						lvIndex=i;
					}else if(i != '0' && j!='0' && k=='0'){
						lvClass="lvBdepth";
						lvDepth="2";
						lvIndex=j;
					}else if(i != '0' && j!='0' && k!='0'){
						lvClass="lvCdepth";
						lvDepth="3";
						lvIndex=k;
					}else{
						lvClass="";
					}

					menuSetTbl += '<tr class="'+lvClass+'">';
					menuSetTbl += '<td>'+lvDepth+'</td>';
					menuSetTbl += '<td>'+lvIndex+'</td>';
					menuSetTbl += '<td class="txtL">'+recoMenu[i][j][k]+'</td>';
					menuSetTbl += '<td class="txtL">ADM_USER_004.html</td>';
					menuSetTbl += '<td>2016-01-08 12:31:10</td>';
					menuSetTbl += '<td>&nbsp;</td>';
					menuSetTbl += '<td><button class="tblInBtn modyMenuBtn" data-mArray="'+i+'-'+j+'-'+k+'">수정</button></td>';
					menuSetTbl += '</tr>';

				}
			}
		}

		menuSetTbl += '</tbody>';
	}


	$("#menuSetTbl").html(menuSetTbl);
	leftH();

}

function resetPwBoxOff(){
	$(".pwResetBoxBtn").show();
	$(".pwResetBoxMail").hide();
}

function resetPwBoxOn(){
	$(".pwResetBoxBtn").hide();
	$(".pwResetBoxMail input[type='text']").val('');
	$(".pwResetBoxMail").show();
}

function toggleChk(){
	$(".fOn").html('<ul><li><i class="fa fa-toggle-on"></i></li><li><span>ON</span></li></ul>');
	$(".fOff").html('<ul><li><i class="fa fa-toggle-off"></i></li><li><span>OFF</span></li></ul>');
}

var selAlgUL = '';
var selAlgEQ = '';
var selAlgTx = '';
var prId = '';

//추천알고리즘 사용설정 - 알고리즘 선택
function algUseListSel(a){

	prId = $(a).parent().parent().parent().parent().attr('id');

	$("#"+prId+" .algUseList ul li").removeClass('active');
	$(a).addClass('active');


	selAlgUL = $(a).parent().attr('class');
	selAlgEQ = $(a).index();
	selAlgTx = $(a).text();
}

function algUseListMov(a){


	var liHtml = "<li>"+selAlgTx+"</li>"

	if(a=='setToNon'){

		if( !$("#"+prId+" ul.setAlg li").hasClass('active') ){
			alert('선택된 알고리즘이 없습니다.');
		}else{
			$("#"+prId+" ul.setAlg  li").eq(selAlgEQ).remove();
			$("#"+prId+" ul.noSetAlg").append(liHtml);
		}

	}else{

		if( !$("#"+prId+" ul.noSetAlg li").hasClass('active') ){
			alert('선택된 알고리즘이 없습니다.');
		}else{
			$("#"+prId+" ul.noSetAlg  li").eq(selAlgEQ).remove();
			$("#"+prId+" ul.setAlg").append(liHtml);
		}

	}

//	algUseListH();

}

function algUseListH(){

	if( $(".noSetAlg").height() > $(".setAlg").height() ){
		$(".setAlg").parent('.algUseList').height($(".noSetAlg").height());
	}else{
		$(".noSetAlg").parent('.algUseList').height($(".setAlg").height());
	}

}


// 메뉴관리 관련 function



// 메뉴수정뿌리기
function ipDepth(a){

	//메뉴리스트에서 메뉴순서 가져와서 구분하기
    var jbString = a;
    var jbSplit = jbString.split( '-' );

	//메뉴리스트에 들어갈 HTML 변수설정
	var aDepthList = "";
	var bDepthList = "";
	var cDepthList = "";

	//메뉴리스트에 들어갈 HTML 초기화
	$("#aDepth").html('');
	$("#bDepth").html('');
	$("#cDepth").html('');

	//메뉴 컨트롤 버튼 설정
	var cont = "<div class='cont'><i class='ti-arrow-up'></i><i class='ti-arrow-down'></i></div>"

	// Adepth
	for(var i='0'; i<'10'; i++){
		if(recoMenu[i][0][0]){
			aDepthList +="<li> <span>"+recoMenu[i][0][0]+"</span> "+cont+"</li>";
		}
	}
	$("#aDepth").html(aDepthList);
	$("#aDepth li:nth-child("+jbSplit[0]+")").addClass("active");
	if(jbSplit[1] == "0"){
		$("#aDepth li:nth-child("+jbSplit[0]+") .cont").show();
	}

	// Bdepth
	if(jbSplit[0] != "0" && jbSplit[1] != "0" ){

		for(var i='1'; i<'10'; i++){
			if(recoMenu[jbSplit[0]][i][0]){
				bDepthList +="<li> <span>"+recoMenu[jbSplit[0]][i][0]+"</span> "+cont+"</li>";
			}
		}
		$("#bDepth").html(bDepthList);
		$("#bDepth li:nth-child("+jbSplit[1]+")").addClass("active");
		if(jbSplit[2] == "0"){
			$("#bDepth li:nth-child("+jbSplit[1]+") .cont").show();
		}
	}

	// Cdepth
	if(jbSplit[0] != "0" && jbSplit[1] != "0" && jbSplit[2] != "0" ){
		for(var i='1'; i<'10'; i++){
			if(recoMenu[jbSplit[0]][jbSplit[1]][i]){
				cDepthList +="<li> <span>"+recoMenu[jbSplit[0]][jbSplit[1]][i]+"</span> "+cont+"</li>";
			}
		}
		$("#cDepth").html(cDepthList);
		$("#cDepth li:nth-child("+jbSplit[2]+")").addClass("active");
		$("#cDepth li:nth-child("+jbSplit[2]+") .cont").show();
	}
}


//메뉴추가 뿌리기
function modDepth(a,b){

	$("#menuDellBtn").hide();
	$("#menuAddBtn").show();

	//메뉴리스트에 들어갈 HTML 변수설정
	var aDepthList = "";
	var bDepthList = "";
	var cDepthList = "";

	if(a=='aDepthAdd'){
		// 첫번째 Depth 클릭
		$("#bDepthAdd").html('');
		$("#cDepthAdd").html('');

		// Bdepth
		for(var i='1'; i<'10'; i++){
			if(recoMenu[b][i][0]){
				bDepthList +="<li> <span>"+recoMenu[b][i][0]+"</span> </li>";
			}
		}
		$("#bDepthAdd").html(bDepthList);

	}else if(a=='bDepthAdd'){
		// 두번째 Depth 클릭
		var aDepthLV = "";
		var liLength = parseInt($("#aDepthAdd li").length) + parseInt('1');

		for(var j = '1'; j<liLength; j++){

			if( $("#aDepthAdd li:nth-child("+j+")").hasClass("active") ){
				aDepthLV = j;
			}
		}

		// Bdepth
		for(var i='1'; i<'10'; i++){
			if(recoMenu[aDepthLV][b][i]){
				cDepthList +="<li> <span>"+recoMenu[aDepthLV][b][i]+"</span> </li>";
			}
		}
		$("#cDepthAdd").html(cDepthList);

	}else{
		//기본셋팅

		//메뉴리스트에 들어갈 HTML 초기화
		$("#aDepthAdd").html('');
		$("#bDepthAdd").html('');
		$("#cDepthAdd").html('');

		// Adepth
		for(var i='0'; i<'10'; i++){
			if(recoMenu[i][0][0]){
				aDepthList +="<li> <span>"+recoMenu[i][0][0]+"</span> </li>";
			}
		}
		$("#aDepthAdd").html(aDepthList);

	}
}

function addMenuList(){

	var mLV = $("#menuDepth").val();
	var mAddLlist = "";
	var addMnFlag = false;

	//메뉴 컨트롤 버튼 설정
	var cont = "<div class='cont'><i class='ti-arrow-up'></i><i class='ti-arrow-down'></i></div>"

	if( !mLV ){
		alert("왼쪽에서 레벨을 선택해주세요!");
	}else{

		if(mLV == 'a'){

			addMnFlag = true;

		}else if(mLV == 'b'){

			if($("#aDepthAdd li").hasClass('active')){
				addMnFlag = true;
			}else{
				alert("LEFT MENU 2 DEPTH에 메뉴를 추가하시려면, LEFT MENU 1 DEPTH를 선택해주세요!");
			}

		}else if(mLV == 'c'){

			if($("#bDepthAdd li").hasClass('active')){
				addMnFlag = true;
			}else{
				alert("TOP MENU 3 DEPTH에 메뉴를 추가하시려면, LEFT MENU 2 DEPTH를 선택해주세요!");
			}

		}

		if(addMnFlag){
			$("#menuDellBtn").show();
			$("#menuAddBtn").hide();
			mAddLlist = $("#"+mLV+"DepthAdd");
			mAddLlist.append('<li class="active" id="addMenu"><span>'+$("#menuName").val()+'</span> '+cont+'</li>');
			$("#addMenu .cont").show();
		}
	}

}

function delMenuList(){
	$("#addMenu").remove();
	$("#menuDellBtn").hide();
	$("#menuAddBtn").show();
}



$(function(){

	//도메인 DB 접속 수정 팝업
	$('.modyDBBtn').click(function(){
		maskOn();
		$('.pop-modyDB').fadeIn();
	});

	//도메인 DB 접속 등록 팝업
	$('.DBAddBtn').click(function(){
		maskOn();
		$('.pop-addDB').fadeIn();
	});

	//도메인관리 추가 팝업
	$('.DomainAddBtn').click(function(){
		maskOn();
		$('.pop-addDomain').fadeIn();
	});

	//도메인관리 수정 팝업
	$('.modyDomainBtn').click(function(){
		maskOn();
		$('.pop-modifyDomain').fadeIn();
	});

	//도메인 관리자 ID 발급 팝업
	$('.issueIDBtn').click(function(){
		maskOn();
		$('.pop-issueID').fadeIn();
	});


	//인증키 관련 팝업 (발급/미발급)
	$('.issueKEYBtn, .notIssueKEYBtn').click(function(){
		maskOn();
		$('.pop-issueKEY').fadeIn();
	});

	//비밀번호 초기화
	$('.pwResetBtn').click(function(){
		maskOn();
		resetPwBoxOff();
		$('.pop-pwReset').fadeIn();
	});

	//비밀번호 초기화버튼 클릭
	$(".pwResetBoxBtn button").click(function(){
		resetPwBoxOn();
	});


	//사용자계정 수정
	$(".modyUserBtn").click(function(){
		maskOn();
		resetPwBoxOff();
		$('.pop-userModiPop').fadeIn();
	});

	//사용자계정 추가
	$(".userAddBtn").click(function(){
		maskOn();
		$('.pop-issueUSER').fadeIn();
	});





	//서비스 추가
	$(".serviceAddBtn").click(function(){
		maskOn();
		$('.pop-serviceAdd').fadeIn();
	});

	//서비스 수정
	$(".serviceModiBtn").click(function(){
		maskOn();
		$('.pop-serviceModi').fadeIn();
	});

	//서비스 복구
	$(".serviceBakBtn").click(function(){
		maskOn();
		$('.pop-serviceBak').fadeIn();
	});


	//추천알고리즘 사용설정
	$( ".algUseList ul" ).delegate( "li", "click", function() {
		algUseListSel(this);
	});

	//추천알고리즘 이동버튼 - 설정 > 미설정
	$(".algUseSetBtn i.ti-arrow-left").click(function(){
		algUseListMov('setToNon');
	});

	//추천알고리즘 이동버튼 - 미설정 > 설정
	$(".algUseSetBtn i.ti-arrow-right").click(function(){
		algUseListMov('nonToSet');
	});



	// 토글 관련
	$(".dToggle").click(function(){

		var toChk = confirm('변경하시겠습니까?');

		if(toChk){

			alert('변경되었습니다.');

			if( $(this).hasClass('dToggle fOn') ){
				$(this).attr('class','dToggle fOff');
			}else{
				$(this).attr('class','dToggle fOn');
			}

			toggleChk();

		}

	});



	//사용자 메뉴 권한 수정
	$("body").delegate('button','click',function(){

		if($(this).hasClass('modyMenuBtn')){
			maskOn();
			$('.pop-userModiMenu').fadeIn();
			ipDepth( $(this).attr('data-mArray') );
		}

	});



	//사용자 메뉴 권한 추가
	$(".userMenuAddBtn").click(function(){
		maskOn();
		$('.pop-userAddMenu').fadeIn();
		modDepth();
	});

	//사용제 메뉴 권한추가 팝업에서 메뉴 추가하기
	$("#menuAddBtn").click(function(){
		addMenuList();
	});

	//사용제 메뉴 권한추가 팝업에서 추가된 메뉴 삭제하기
	$("#menuDellBtn").click(function(){
		delMenuList();
	});




	//사용자메뉴 > 메뉴리스트 클릭
	$("body").delegate('span','click',function(){

		var depthUlID = $(this).parent('li').parent('ul').attr('id');
		var depthUlIdx = parseInt($(this).parent('li').index())+parseInt('1');

		if( !$(this).parent('li').parent('ul').hasClass("cDisable") ){

			boxListReset(depthUlID);
			$(this).parent('li').addClass('active');
			$(this).parent('li').find('.cont').show();

			if(depthUlID != 'cDepthAdd'){
				modDepth(depthUlID,depthUlIdx);
			}
		}

	});

	//사용자메뉴 > 메뉴리스트 상하 버튼 클릭
	$("body").delegate('i','click',function(){

		var bClass = $(this).attr('class');

		if(bClass == "ti-arrow-down"){
			 var after = $(this).parent().parent().next();
			$(this).parent().parent().insertAfter(after);
		}

		if(bClass == "ti-arrow-up"){
			 var before = $(this).parent().parent().prev();
			$(this).parent().parent().insertBefore(before);
		}
	});

});


function boxListReset(a){
	$("#"+a+" li").removeClass('active');
	$("#"+a+" li .cont").hide();
}

function clickDisable(){



}

$(window).ready(function(){
	//토글 뿌림
	toggleChk();
	inTable();




	clickDisable();

});