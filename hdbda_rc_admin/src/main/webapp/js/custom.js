
var mnChk = 'off';

//기존 메뉴 값 때문에 여백 포함해서 width에 기본적으로 80 적용해놓음
var liLenSum=80;

// 좌측메뉴 높이 설정
function leftH(){

	var rH = $(".right-col").height();

	var wH = parseInt(rH) + parseInt('100');


	if( wH < $(window).height() ){
		$(".left-col").height($(window).height());
	}else{
		$(".left-col").height(wH);
	}

}

//메뉴열기
function menuOpen(){
	$(".logoTop .topBtn").css("background","#274298");
	$(".logoTop").removeClass('hideMenuTop');
    $(".nav").removeClass('hideMenu');

	$(".onMenu").show();

	$(".left-col").css("left","0px");

	if( $(window).width() > '1024' ){
		$(".lecoTop").css("padding-left","250px");
		$(".right-col").css("padding-left","250px");
	}

	$(".left-col .nav ul").css("margin-left","0px");
	$(".left-col .nav ul li > i").show();

    $('.viewAllMenu').css('padding-left','250px');

}



//메뉴닫기
function menuClose(){
	$(".logoTop").addClass('hideMenuTop');
    $(".nav").addClass('hideMenu');
	$(".subMenu").hide();
	$(".left-col").css("left","-190px");

	$(".lecoTop").css("padding-left","60px");
	$(".right-col").css("padding-left","60px");

	$(".logoTop .topBtn").css("background","#274298");
	$(".left-col .nav ul").css("margin-left","189px");
	$(".left-col .nav ul li > i").hide();

	allMenuOff();

}

//메뉴를 자동으로 닫고 열고체크
function menuSet(){

	if( $(".logoTop").hasClass('logoTop hideMenuTop')){
		menuOpen();
		mnChk = 'off';
	}else{
		menuClose();
		mnChk = 'on';
	}
}

//전체메뉴넣기
function allMenuOn(){
	$(".nav ul li.allMenu > i").attr('class','ti-angle-left');
	$("#viewAllMenu").addClass('openM');
	$("#viewAllMenu").css('left','0px');

    //전체메뉴 펼치기 left css 잡기
    var offsetLeft = $('.left-col').offset();
    if(offsetLeft.left==-190){
        $('.viewAllMenu').css('padding-left','60px');
    }else{
         $('.viewAllMenu').css('padding-left','250px');
    }
}

//전체메뉴넣기
function allMenuOff(){
	$(".nav ul li.allMenu > i").attr('class','ti-angle-right');
	$("#viewAllMenu").removeClass('openM');
	$("#viewAllMenu").css('left','-1920px');
}

// 로딩박스
//function loadingBox(){
//
//	var lHtml = "";
//
//	lHtml += '<div class="sk-cube-grid">';
//	lHtml += '<div class="sk-cube sk-cube1"></div>';
//	lHtml += '<div class="sk-cube sk-cube2"></div>';
//	lHtml += '<div class="sk-cube sk-cube3"></div>';
//	lHtml += '<div class="sk-cube sk-cube4"></div>';
//	lHtml += '<div class="sk-cube sk-cube5"></div>';
//	lHtml += '<div class="sk-cube sk-cube6"></div>';
//	lHtml += '<div class="sk-cube sk-cube7"></div>';
//	lHtml += '<div class="sk-cube sk-cube8"></div>';
//	lHtml += '<div class="sk-cube sk-cube9"></div>';
//	lHtml += '</div>';
//
//	lHtml += '<div class="sk-cube-txt">e-store 36.5+</div>';
//
//	$("#loading").html(lHtml);
//
//}

function maskOn(){
	$('#mask').fadeIn();
	$('body').animate({scrollTop:0}, 300);
	leftH();
}

function maskOff(){
	$('#mask').fadeOut();
	leftH();
}


function maskOn2(){

	$('#mask2').fadeIn();
	$('body').animate({scrollTop:0}, 300);
	leftH();
}

function maskOff2(){
	$('#mask2').fadeOut();
	leftH();
}

// 상품추천관리 & 공지사항
function rec_view_ed(a){

	var trNum = $(a).attr('data-tblNm');

	if($(a).attr('class') == 'ti-arrow-circle-down'){

		$(a).attr('class','ti-arrow-circle-up')
		$("#tblNm"+trNum).fadeIn('300');

	}else{
		$(a).attr('class','ti-arrow-circle-down')
		$("#tblNm"+trNum).fadeOut('300');

	}

	leftH(); // 좌측메뉴높이
}

//x버튼 클릭 시 팝업 hide
function popOff(a){
	$(a).parents('.popUp').fadeOut();
}

// 체크박스 라디오박스 스타일
function rdChkCustom(){
	$(".rdSel").prepend('<i class="fa fa-dot-circle-o"></i><i class="fa fa-circle-o"></i>');
	$(".chkSel").prepend('<i class="fa fa-check-square-o"></i><i class="fa fa-square-o"></i>');
}

//추천성과분석 > 추천영역별,추천SET별,추천알고리즘 상세설정 체크된 박스 개수 가져오기
function countChkbox(a){
	var selCnt = $(a).parent().parent().find('input:checkbox:checked').length;
	return selCnt;
}

//추천성과분석 > 추천영역별,추천SET별,추천알고리즘 상세설정 열기
function setBoxListOpen(){
	$("#setBoxList").addClass('bActive');
	$(".setBoxListBox").slideDown('300');
	$("#setBoxList").html("상세설정닫기 <i class='ti-angle-up'></i>");
//	rdChkCustom();
}

//추천성과분석 > 추천영역별,추천SET별,추천알고리즘 상세설정 닫기
function setBoxListClose(){
	$("#setBoxList").removeClass('bActive');
	$(".setBoxListBox").slideUp('300');
	$("#setBoxList").html("상세설정보기 <i class='ti-angle-down'></i>");
}

//테이블 보기
function viewDataTbl(a){

	if( $("#"+a).hasClass('conBoxHide') ){
		$("#"+a).removeClass('conBoxHide');
	}else{
		$("#"+a).addClass('conBoxHide');
	}
	leftH();
}

function mainBoxSizeChk(){

	if ( !window.matchMedia('(max-width: 1835px)').matches || $(window).width() < 600 ) {
	  $(".mainTopBoxList").addClass('dfSize');
    } else {
	    $(".mainTopBoxList").removeClass('dfSize');
    }
}

// 설정 화면 보기
function algBoxOpen(){
	$("#algPreview").attr("class","col-md-9");
	$("#algSetBox").show();
	$(".algSetContBox").show();
	$(".algSetContTop i").attr('class','ti-angle-up');
	$(".algSetBoxTop i").attr('class','ti-angle-up');
}

// 설정 화면 닫기
function algBoxClose(){
	$("#algPreview").attr("class","col-md-12");
	$("#algSetBox").hide();
}

function infoPop(){
	maskOn();
	$('#helpPop').fadeIn();
}

// -----------------------------------------------------------------------
// -----  Events in custom.js START --------------------------------------
// -----------------------------------------------------------------------
function customJavascriptEvents(){
    // 달력아이콘 클릭시 Input box ID 값 가져와서 강제로 'click' evnet 호출
	$("i.ti-calendar").unbind('click').bind('click',function(){
		var calID = $(this).parent().find("input").attr('id');
		$("#"+calID).trigger('click');
	});

	// 팝업닫기
	$('.popUp .closePop,.popCancel').unbind('click').bind('click',function(){
		maskOff();
		popOff(this);
	});

	// 알고리즘 관련 설정화면 제어
	$(".algSetOpen").unbind('click').bind('click',function(){
        if( $(this).hasClass('viewAlg') ){
            $(this).removeClass('viewAlg');
            $(this).html('<i class="ti-layout-sidebar-right"></i> 설정화면 보기');
            algBoxClose();
        }else{
            $(this).addClass('viewAlg');
            $(this).html('<i class="ti-layout-sidebar-right"></i> 설정화면 닫기');
            algBoxOpen();
        }
    });

    // 알고리즘 서브 박스
    $(".algSetContTop i").unbind('click').bind('click',function(){
        var boxNum = $(this).parent().attr('data-topSet');
        if($(this).hasClass('ti-angle-down')){
            $(this).attr('class','ti-angle-up');
            $("#algSubBox"+boxNum).slideDown();
        }else{
            $(this).attr('class','ti-angle-down');
            $("#algSubBox"+boxNum).slideUp();
        }
    });

    // 알고리즘 설정 전체박스
    $(".algSetBoxTop i").unbind('click').bind('click',function(){
        if($(this).hasClass('ti-angle-down')){
            $(".ti-angle-down").attr('class','ti-angle-up');
            $("#algSubBox1").slideDown();
            $("#algSubBox2").slideDown();
            $("#algSubBox3").slideDown();
            $("#algSubBox4").slideDown();
            $("#algSubBox5").slideDown();
            $("#algSubBox6").slideDown();
        }else{
            $(".ti-angle-up").attr('class','ti-angle-down');
            $("#algSubBox1").slideUp();
            $("#algSubBox2").slideUp();
            $("#algSubBox3").slideUp();
            $("#algSubBox4").slideUp();
            $("#algSubBox5").slideUp();
            $("#algSubBox6").slideUp();
        }
    });
}
// -----------------------------------------------------------------------
// -----  Events in custom.js END ----------------------------------------
// -----------------------------------------------------------------------


// -----------------------------------------------------------------------
// ----- Page Render START -----------------------------------------------
// -----------------------------------------------------------------------
$(document).ready(function(){

	//메인에 카운트
    mainBoxSizeChk();

	// 체크박스 라디오박스 스타일
	rdChkCustom();

//	loadingBox();

	leftH(); // 좌측메뉴 높이 설정

	// 화면사이즈에 따라 메뉴열기/닫기
	if( $(window).width() > '1200' ){
		menuOpen();
	}else{
		menuClose();
	}

	customJavascriptEvents();

    //subTopMenu ul li의 전체 width 값인 liLenSum 구함
    for (var i = 1;i <= $('.subTopMenu ul li').length ;i++ ){
        var liLen = $('.subTopMenu ul li:nth-child('+i+')').outerWidth();
        liLenSum = liLenSum+liLen;
    }

    //subTopMenu ul li의 전체 width 값인 liLenSum보다 작은 경우에 다른 css 적용
    if ($(window).width() <= liLenSum) {
        $('.mConBox .subTopMenu ul').addClass('col-md-12');
        $('.mConBox .subTopMenu').addClass('subTopMobile');
    } else {
        $('.mConBox .subTopMenu ul').removeClass('col-md-12');
    }





});

$(window).load(function(){
		$("#loading").fadeOut('300');


});

$(window).resize(function(){
	leftH(); // 좌측메뉴 높이 설정

	if(mnChk == "off"){

  		// 화면사이즈에 따라 메뉴열기/닫기
		if( $(window).width() > '1200' ){
			menuOpen();
		}else{
			menuClose();
		}
	}

	//메인에 카운트
    mainBoxSizeChk();

    if ($(window).width() <= liLenSum) {
        $('.mConBox .subTopMenu ul').addClass('col-md-12');
        $('.mConBox .subTopMenu').addClass('subTopMobile');
    } else {
        $('.mConBox .subTopMenu ul').removeClass('col-md-12');
        $('.mConBox .subTopMenu').removeClass('subTopMobile');
    }

});
// -----------------------------------------------------------------------
// ----- Page Render END -------------------------------------------------
// -----------------------------------------------------------------------
