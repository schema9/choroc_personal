/*
description : validate for exist variable
return      : boolean;
*/
function fn_util_isValid_typeof(value){
    if(value == null || value == 'null'){
        return false;
    }
    switch(typeof value){
        case 'undefined':
        case 'object':
        case 'function':
            return false;
            break;
        case 'boolean':
        case 'number':
            return true;
            break;
        case 'string':
            if(value.isBlank()){
                return false;
            }else{
                return true;
            }
            break;
    }
}

/*
description : validate for date
return      : boolean;
parameter   : str : string;
note        : [str] must be format 'yyyymmdd' (20101010)
*/
function fn_util_isValid_date_ymd(str){
    var year    = str.substr(0,4),
        month   = str.substr(4,2) -1,
        day     = str.substr(6,2);
    var dt      = new Date(year, month, day);

    return (dt.getFullYear() == year
            && dt.getMonth() == month
            && dt.getDate() == day) ? true : false;
}

/*
description : validate for value
return      : boolean;
parameter   : targetID: identify the tag;
note        : allowed function .val() and .text()
*/
function fn_util_isValid_value(targetID){
    var $value;
    if('' == $('#'+targetID).val()) {
        return false;
    } else {
        if( fn_util_isValid_typeof($('#'+targetID).val()) ){
            $value = $('#'+targetID).val();
        }else{
            $value = $('#'+targetID).text();
        }

        if($value == '' || $value == 'undefined'
            || $value == null || $value == 0
            || $value == '0'){
                return false;
        }
    }
    return true;
}

/*
description : validate for search option value and text
return      : boolean;
parameter   : optID   : identify option tag
              textID  : identify input tag
*/
function fn_util_isValid_optionTextSearch(optID,inpID){
    var $opt = $('#'+optID), $txt = $('#'+inpID);
    if( fn_util_isValid_value(optID) && !fn_util_isValid_value(inpID) ){
        alert('검색어를 입력하세요');
        $txt.focus();
        return false;
    }else{
        return true;
    }
}

function fn_util_isValid_optionTextSearchWithAll(optID,inpID){
    var $opt = $('#'+optID), $txt = $('#'+inpID);
    if( fn_util_isValid_value(optID) && !fn_util_isValid_value(inpID) ){
        alert('검색어를 입력하세요');
        $txt.focus();
        return false;
    }else{
        return true;
    }
}