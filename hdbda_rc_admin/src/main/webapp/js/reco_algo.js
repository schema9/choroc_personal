//추천환경관리 JS

function algSet(){
	
	if( $("#algList").val() ){
		$("#algList").addClass('selected');
	}else{
		$("#algList").removeClass('selected');
	}
}

function algBoxOpen(){
	$("#algPreview").attr("class","col-md-9");
	$("#algSetBox").show();
//	$(".algSetOpen").hide();
	$(".algSetContBox").show();
	$(".algSetContTop i").attr('class','ti-angle-up');
}

function algBoxClose(){
	$("#algPreview").attr("class","col-md-12");
	$("#algSetBox").hide();
//	$(".algSetOpen").show();
}


function algAddList(){
	
	var algHtml = "";
	
	algHtml +='<li>';
	algHtml +='<select title="우선순위">';
	algHtml +='<option>우선순위</option>';
	algHtml +='<option>1</option>';
	algHtml +='<option>2</option>';
	algHtml +='<option>3</option>';
	algHtml +='<option>4</option>';
	algHtml +='<option>5</option>';
	algHtml +='</select>';
	algHtml +='<select title="알고리즘명">';
	algHtml +='<option>알고리즘명</option>';
	algHtml +='<option>popular</option>';
	algHtml +='</select>';
	algHtml +='<input title="비율" type="text" placeholder="비율입력">';
	algHtml +='</li>';

	if($("#algSetList ul li").length < 3){
		
		$("#algSetList ul").append(algHtml);
	}else{
		alert("알고리즘은 최대 3개까지 설정가능합니다.");
	}
}

function algDelList(){
	
	if($("#algSetList ul li").length > 1){
	
		$("#algSetList ul li:last-child").remove();
	}else{
		alert("알고리즘은 최소 1개는 설정되어야합니다.");
	}	

}




//x버튼 클릭 시 팝업 hide
function popOff(a){
	$(a).parents('.popUp').fadeOut();
}


$(function(){

	//알고리즘 선택
	$("#algList").change(function(){
		algSet();
	});


	$(".algSetOpen").click(function(){
		
		if( $(this).hasClass('viewAlg') ){
			$(this).removeClass('viewAlg');
			$(this).html('<i class="ti-layout-sidebar-right"></i> 설정화면 보기');
			algBoxClose();
		}else{
			$(this).addClass('viewAlg');
			$(this).html('<i class="ti-layout-sidebar-right"></i> 설정화면 닫기');
			algBoxOpen();
		}



	});

	//알고리즘 서브 박스
	$(".algSetContTop i").click(function(){
		
		var boxNum = $(this).parent().parent().attr('data-topSet');

		if($(this).hasClass('ti-angle-down')){
			$(this).attr('class','ti-angle-up');
			$("#algSubBox"+boxNum).slideDown();
		}else{
			$(this).attr('class','ti-angle-down');
			$("#algSubBox"+boxNum).slideUp();
		}

	});


	//상품추천화면관리
	$(".recViewTit i").click(function(){
		rec_view_ed(this);
	});


	//추천화면 추가등록 popup

	$('.ViewAddBtn').click(function(){
		maskOn();
		$('.pop-addView').fadeIn();		
	});

	$('.pop-addView .closePop,.pop-addViewCancel').click(function(){
		maskOff();
		popOff(this);
	});


	//추천 위젯 설정 JSON 팝업
	$('.jsonPopBtn').click(function(){
		maskOn();
		$('.pop-jsonPop').fadeIn();
	});


	//전체상품 풀절관리 팝업
	$('.pormBtn').click(function(){
		maskOn();
		$('.pop-promDetail').fadeIn();
	});

});

function paramAddList(){
	
	var paramHtml = "";

	paramHtml +='<tr>';
	paramHtml +='<td>';
	paramHtml +='<select name="" id="">';
	paramHtml +='<option value="">alg-set-id-3</option>';
	paramHtml +='<option value="">algorithm-set-c</option>';
	paramHtml +='</select>';
	paramHtml +='</td>';
	paramHtml +='<td>';
	paramHtml +='<select name="" id="">';
	paramHtml +='<option value="">20%</option>';
	paramHtml +='<option value="">30%</option>';
	paramHtml +='</select>';				
	paramHtml +='</td>';
	paramHtml +='<td class="txtC"><button class="tblInBtn deleteBtn" onclick="paramDelList(this)">삭제</button></td>';
	paramHtml +='</tr>';

	if($(".popInTblInner tr").length < 4){
	
		$(".popInTblInner").append(paramHtml);
	}else{
		alert("추천알고리즘 SET 추가는 최대 3개까지 가능합니다.");
	}
}

function paramDelList(a){
	
	if($(".popInTblInner tr").length > 2){
	
		$(a).parents('tr:first').remove();
	}else{
		alert("파라미터값은 최소 1개는 설정되어야합니다.");
	}	

}