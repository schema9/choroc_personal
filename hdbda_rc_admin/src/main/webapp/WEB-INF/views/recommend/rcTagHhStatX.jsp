<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	int columnCnt = 6;
%>
<table style="width:1200px;">
	<tr>
		<td>
			<table style="width:100%;" border="0">
				<tbody>
				<tr>
					<td colspan="<%=columnCnt%>" style="text-align:center;font-size:24px;font-weight:bold;">
					${excelTitle}
					</td>
				</tr>
				<tr>
					<td colspan="<%=columnCnt%>" style="heigth:20px;"></td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table style="width:100%;" border="1">
				<thead>
					<tr>
						<c:choose>
							<c:when test="${dateType == 'A'}">
								<th style="background-color:#1f3a93; color: white; ">일자</th>
							</c:when>
							<c:when test="${dateType == 'B'}">
								<th style="background-color:#1f3a93; color: white; ">일자</th>
								<th style="background-color:#1f3a93; color: white; ">시간</th>
							</c:when>
							<c:otherwise>
								<th style="background-color:#1f3a93; color: white; ">일자</th>
							</c:otherwise>
						</c:choose>
						<th style="background-color:#1f3a93; color: white; ">일반회원<br>접속건수</th>
						<th style="background-color:#1f3a93; color: white; ">공공기관<br>접속건수</th>
						<th style="background-color:#1f3a93; color: white; ">사회적경제기업<br>접속건수</th>
						<th style="background-color:#1f3a93; color: white; ">STORE 36.5매장<br>접속건수</th>
						<th style="background-color:#1f3a93; color: white; ">유통채널<br>접속건수</th>
					</tr>
				</thead>
			
				<tbody>
					<c:forEach items="${excelList}" var="list" varStatus="status">
						<tr>
							<c:choose>
								<c:when test="${dateType == 'A'}">
									<td style="text-align: center;">${list.yyyymmdd}</td>
								</c:when>
								<c:when test="${dateType == 'B'}">
									<td style="text-align: center;">${list.yyyymmdd}</td>
									<td style="text-align: center;">${list.hh}</td>
								</c:when>
								<c:otherwise>
									<td style="text-align: center;">${list.yyyymmdd}</td>
								</c:otherwise>
							</c:choose>
							<td><fmt:formatNumber value="${list.view_click_1}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.view_click_2}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.view_click_3}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.view_click_4}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.view_click_5}" pattern="#,###" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</td>
	</tr>
</table>

