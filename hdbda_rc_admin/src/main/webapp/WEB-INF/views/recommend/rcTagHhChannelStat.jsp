<%------------------------------------------------------------------------
 * TODO >  추천성과분석 > 추천 수집 분석 > 일별 시간대별 항목별
 * 파일명   > rcEoadTagHhChannelStat.jsp
 * 작성일   > 2019.09.01.
 * 작성자   > CCmedia Service Corp.
------------------------------------------------------------------------%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>

<script type="text/javascript">
var controller = multiApiSet();
var userActionCnt=0;
var page_resultData;

$(document).ready(function() {
	// 시작일 ,종료일, 데이터타입(일,시), 차트옵션 셋팅
    var startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
	var endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
	var dateType = $('.dActive').attr('data-dtype');
	var chartOption = $("#searchOptionNm option:selected" ).val();
	
	$('#datePickerId3').hide();
	$('#dateCal3').hide();
	
	getUserActionCnt(startDate, endDate, dateType, chartOption);
    initFunction();
});

function multiApiSet(){
	return "/collection/analysis";
	
	
	
	var api_url, api_protocol = location.protocol;
	var api_chkUrl = window.location.href;
	if('' != api_chkUrl) {
		if('http:' == api_protocol) {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
		} else {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
		}
	} else {
		return false;
	}
}

function initFunction() {
	// 10개,20개보기 콤보상자 선택
    $('#dsplyCnt1').change(function(){
        Paging1(userActionCnt, parseInt($(this).val()), 1, "divPageNo1");
    });

    // 일 시 선택
    $(".dateRangeBtn").click(function(){
        $(".dateRangeBtn").removeClass('dActive');
        $(this).addClass('dActive');
        
        var dateType = $(this).attr('data-dtype');
        var startDate = '';
        var endDate = '';
        var chartOption = $("#searchOptionNm option:selected" ).val();
        
        if("A" == dateType) {
        	$('#datePickerId3').hide();
        	$('#dateCal3').hide();
        	$('#datePickerId1').show();
        	$('#dateCal1').show();
        	$("#baseDate").text("일자");
            startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
            endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
        } else {
        	$('#datePickerId1').hide();
        	$('#dateCal1').hide();
        	$('#datePickerId3').show();
        	$('#dateCal3').show();
        	$("#baseDate").text("시간");
            startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
            endDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
        }            
    	getUserActionCnt(startDate, endDate, dateType, chartOption);
    });
}

//달력 선택
function dateFunction1(startDate, endDate) {
	var dateType = $('.dActive').attr('data-dtype');
    $('#dsplyCnt1').val("10");
    getUserActionCnt(startDate, endDate, dateType, chartOption);
}

//ajax 호출
function dateFunction3(startDate) {
	var chartOption = $("#searchOptionNm option:selected" ).val();
	var dateType = $('.dActive').attr('data-dtype');
	getUserActionCnt(startDate, '', dateType, chartOption);
}

//차트데이터 jsonArray 배열에 셋팅
function echartMakeData1(data, chartOption, value1, value2, value3) {					   
    var jsonArray = new Array();
    
    if(data && data.length > 0) {
        for(var key in data) {
            var itemInfo = data[key];
            var object = new Object();
			
            if("A" == $('.dActive').attr('data-dtype')) {
                var month = itemInfo.yyyymmdd.substring(4,6)
                var day = itemInfo.yyyymmdd.substring(6,8)
//             	var month = itemInfo.yyyymmdd.substring(5,7);
//             	var day = itemInfo.yyyymmdd.substring(8,10);
            	var date = month + "/" + day;         
            	object.title = date;
            } else {
            	object.title = itemInfo.hh + "시";
            }          

            if(chartOption == "PC"){
				object.value1 = itemInfo.pc_view;		
				object.value2 = itemInfo.pc_cart;			
				object.value3 = itemInfo.pc_buy;
            }
            else if(chartOption == "Mobile"){
            	object.value1 = itemInfo.mobile_view;
            	object.value2 = itemInfo.mobile_cart;
            	object.value3 = itemInfo.mobile_buy;
            }
            else if(chartOption == "IOS"){
            	object.value1 = itemInfo.ios_cnt;
            	object.value2 = itemInfo.ios_device;
            	object.value3 = itemInfo.ios_item;
            }
            else if(chartOption == "Android"){
            	object.value1 = itemInfo.android_cnt;
            	object.value2 = itemInfo.android_device;
            	object.value3 = itemInfo.android_item;
            }
            else{
            	alert('서버와의 통신에 실패하였습니다.');
            }
            jsonArray.push(object);
        }
     } 
     return jsonArray;
}

function getUserActionCnt(startDate, endDate, dateType, chartOption) {
	var param = new Object;
	var date = '';
	var url = '';
	
	if(dateType == "A") {
		date = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
		param.date = date;
		param.firstYmd = startDate;
		param.lastYmd = endDate;
		param.dateType = dateType;	
	}
	else if(dateType == "B") {
		date = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
		param.date = date;
		param.dateType = dateType;
	}

	param.api = "analysis-cate-count";
	url = controller + '/recommend/cateCount/' + param.api + ".do";
    
    $.ajax({
    	url:url,
    	type:'POST',
    	data:param,
        success: function(resultData) { 
            if( ! $.isEmptyObject(resultData) ) {
                userActionCnt = resultData.listCount;
                $('#totalCount').empty().text(userActionCnt);
                Paging1(userActionCnt, $("#dsplyCnt1").val(), 1, "divPageNo1");          
                if(0== userActionCnt) {
                    var html = makeGridHtml1('');
                    $('#pv_itemView_tbody1').children().remove();
                    $('#pv_itemView_tbody1').append(html);
                }
            }
        },
        error : function(request, status, error) {
            alert('서버와의 통신에 실패하였습니다.');
        }
    });
    dateFunction2(startDate, endDate, dateType, chartOption); 
}

//ajax 호출
function dateFunction2 (startDate, endDate, dateType, chartOption){
	var param = new Object;
	var date = '';
	var url = '';	
	
	if(dateType == "A") {
		date = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
		param.date = date;
		param.firstYmd = startDate;
		param.lastYmd = endDate;
		param.dateType = dateType;	
	}
	else if(dateType == "B") {
		date = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
		param.date = date;
		param.dateType = dateType;
	}
    
	param.api = "analysis-cate-chart";
	url = controller + '/recommand/analysis/' + param.api + ".do";
	
    $.ajax({
        url:url,
        type:'POST',
        data:param,
		beforeSend:function(){
			maskOn2();
	        $("#loading2").show();
		},
        success:function(resultData){
            if(resultData.resultList) {
                successData2(resultData.resultList, chartOption);
                fn_util_setTextType();
            } else {
                $('#graph-flot-points1').removeAttributes().html(nodata());
            }
			$("#loading2").hide();
            maskOff2();
        },
        error:function(e){
            $('#graph-flot-points1').removeAttributes().html(nodata());
            alert('서버와의 통신에 실패하였습니다.');
			$("#loading2").hide();
            maskOff2();
        }
    });
}


//ajax success function
function successData2(data, chartOption){
    if( ! $.isEmptyObject(data) ) {
        drawChart(data, chartOption);
    }  else {
        $('#graph-flot-points1').removeAttributes().html(nodata());
    }
}

function renderDataByHtml1(limitOption, pageNo){

    var startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
    var endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
    var dateType = $('.dActive').attr('data-dtype');
    var url = '';  
    var param = new Object;
    var date = '';
    
	if(dateType == "A") {
		date = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
		param.date = date;
		param.firstYmd = startDate;
		param.lastYmd = endDate;
		param.dateType = dateType;	
	}
	else if(dateType == "B") {
		date = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
		param.firstYmd = date;
		param.dateType = dateType;
	}
    
    //param.firstYmd = startDate;
    //param.lastYmd = endDate;
    param.dateType = dateType;
    param.dsplyCnt = limitOption;
    param.dsplyNo = pageNo;
    
    param.api = "analysis-cate-list";
    url = controller + '/recommend/cateList/' + param.api + ".do";
        
    $.ajax({
    	url:url,
    	type:'POST',
    	data:param,
		beforeSend:function(){
			maskOn2();
	        $("#loading2").show();
		},
        success:function(resultData) {
            if(resultData.list) {
                page_resultData = resultData.list;			
                var html = makeGridHtml1(resultData.list);
                $('#pv_itemView_tbody1').children().remove();
                $('#pv_itemView_tbody1').append(html);
                fn_util_setTextType();
            }
			$("#loading2").hide();
            maskOff2();
        },
        error:function(e){
            alert('서버와의 통신에 실패하였습니다.333');
            makeGridHtml1('');
            $("#loading2").hide();
            maskOff2();
        }
    });   
}


var goPaging_divPageNo1 = function(cPage){
    Paging1(userActionCnt, $("#dsplyCnt1").val(), cPage, "divPageNo1");
};

function makeGridHtml1(tableData) {

    html = '';
    var itemList = tableData;
    
    
	for (var i in itemList) {
		if (itemList.hasOwnProperty(i)) {
			console.log(itemList[i]);
		}
	}
    
    if(itemList && itemList.length > 0) 
    {
        for(var key in itemList) 
        {
            var itemInfo = itemList[key];
            


            html += '<tr>';
            if(itemInfo['yyyymmdd'] != null) {
				year = itemInfo['yyyymmdd'].substr(0,4);
				month = "-"+itemInfo['yyyymmdd'].substr(4,2);
				day = "-"+itemInfo['yyyymmdd'].substr(6,2);
				date = year+month+day;
				html += '<td>'+date+'</td>';
				
//             	html += '<td>' + itemInfo['yyyymmdd'] + '</td>';
            } else {
            	html += '<td>'+ itemInfo['hh'] +"시" + '</td>';
            }
            html += '	<td class="moneyType txtR">' + itemInfo['pc_view']     + '</td>';
            html += '	<td class="moneyType txtR">' + itemInfo['pc_cart']      + '</td>';
            html += '	<td class="moneyType txtR">' + itemInfo['pc_buy']       + '</td>';
            html += '	<td class="moneyType txtR">' + itemInfo['mobile_view'] + '</td>';
            html += '	<td class="moneyType txtR">' + itemInfo['mobile_cart']  + '</td>';
            html += '	<td class="moneyType txtR">' + itemInfo['mobile_buy']   + '</td>';
//             html += '	<td class="moneyType">' + itemInfo['ios_cnt']        + '</td>';
//             html += '	<td class="moneyType">' + itemInfo['ios_device']     + '</td>';
//             html += '	<td class="moneyType">' + itemInfo['ios_item']       + '</td>';
//             html += '	<td class="moneyType">' + itemInfo['android_cnt']    + '</td>';
//             html += '	<td class="moneyType">' + itemInfo['android_device'] + '</td>';
//             html += '	<td class="moneyType">' + itemInfo['android_item']   + '</td>';
            html += '</tr>';
        }
    } else {
        html += '<tr><td colspan="13" style="text-align:center">No data available in table</td>';
    }
	return html;
}


function nodata() {
    var html;
    html = ' <div class="noDataDiv">';
    html += ' <div class="dTxt">';
    html += ' <i class="ti-alert"></i>';
    html += ' <p>데이터가 없습니다.</p>';
    html += ' </div>';
    html += ' </div>';
    return html;
}


//echart 그리기
function drawChart(data, chartOption) {
	
	var chartOption = $("#searchOptionNm option:selected" ).val();
	
	for(var i=0; i<data.length; i++) {
		eval("date" + i + "= data[i].yyyymmdd");
	} 
	
    if(chartOption == "PC") {
    	var json_data = echartMakeData1(data, chartOption, '클릭 건수', '추천 제품 클릭 건수', '');
        var titleDate =['제품클릭 건수', '장바구니 건수', '구매 건수'];
        var p_color = ['#003366','#006699','#4cabce'];
        multibarChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'N', ' 건/명');
    } else if(chartOption == "Mobile") {
    	var json_data = echartMakeData1(data, chartOption, '제품상세 클릭건수', '', '');
    	var titleDate =['제품클릭 건수', '장바구니 건수', '구매 건수'];
    	var p_color = ['#8b4f1d','#a0a0ff','#ff8c0a'];
    	multibarChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'N', ' 건/명');
    } else if(chartOption == "IOS") {
    	var json_data = echartMakeData1(data, chartOption, '제품상세 클릭건수', '', '');
    	var titleDate =['클릭 건수', '고객 건수', '제품 건수'];R
    	var p_color = ['#ff8c0a','#a0a0ff','#2828cd'];
    	multibarChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'N', ' 건/명');
    } else if(chartOption == "Android") {
    	var json_data = echartMakeData1(data, chartOption, '제품상세 클릭건수', '', '');
    	var titleDate =['클릭 건수', '고객 건수', '제품 건수'];
    	var p_color = ['#84799f','#a0a0ff','#ff8c0a'];
    	multibarChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'N', ' 건/명');
    }    
	else {
    	alert('서버와의 통신에 실패하였습니다.');
    } 	 
}


function chartOption() {
	var option = $("#searchOptionNm option:selected" ).val();
	var dateType = $('.dActive').attr('data-dtype');
	var startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
	var endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
	getUserActionCnt(startDate, endDate, dateType, chartOption);
}

</script>
<div class="mConBox">
    <div class="mCont">
        <div class="row" >
            <div class="col-md-4" id="subtitleA">
                <h2><i class="ti-layers"></i> 전체 수집 분석 통계</h2>
            </div>
            <div class="col-md-8">
                <div class="pageRoute">
                    <ul>
                        <li><a href="#">HOME</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li><a href="#">서비스 현황 분석</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li><a href="#">사용자행동데이터 조회</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li class="current"><a href="#"> 전체 수집 분석 통계</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row" >
            <div class="col-md-12 text-right">
                <div class="conBox" style="min-height:400px;">
                    <div class="setBoxListArea">
                        <div id="setAreaIdLIstBox" class="setBoxListBox">
                            <ul date-limitSel='3'></ul>
                            <div class="setBoxListBtn">
                                <button class="darkGrayBtn" id="setBoxListSearch">조회</button>
                            </div>
                        </div>
                    </div>
					<div class="setBoxListArea mr10 pull-left">
                    <!-- <button class="dateRangeBtn" data-dtype="C">월</button> -->
					<div class="setBoxListArea mr10">                   
                    <select id="searchOptionNm" onchange="chartOption()">
                    	<option value="PC">PC</option>
                    	<option value="Mobile">Mobile</option>
<!--                     	<option value="IOS">IOS</option> -->
<!-- 						<option value="Android">Android</option> -->
                    </select>
                    </div>
                    </div>
                    <button class="dateRangeBtn dActive" data-dtype="A">일</button>
                    <button class="dateRangeBtn" data-dtype="B">시</button>
                    <div class="dateRange">
                        <input type="text" id="datePickerId1" />
                        <i class="ti-calendar" id="dateCal1"></i>
                    </div>
                    <div class="dateRange">
                        <input type="text" id="datePickerId3" />
                        <i class="ti-calendar" id="dateCal3"></i>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="gInBox" id="graph-flot-points1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" >
            <div class="col-md-12" id="subtitleB">
                <h2><i class="ti-layers"></i> 전체 수집 분석 통계 상세</h2>
            </div>
        </div>
        <div class="row" >
            <div class="col-md-12">
                <div class="conBox" style="min-height:400px;">
                    <div class="row">
                        <div class="col-md-6 select-set-3">
                            <p> 총 <b class="moneyType" id="totalCount"></b> 개</p>
                        </div>
                        <div class="col-md-6 text-right select-set-2">
                            <button type="button" id="btnExcel" class="exDwBtn">엑셀</button>
                            <select name="dsplyCnt1" id="dsplyCnt1">
                                <option value="10">10개씩 보기</option>
                                <option value="20">20개씩 보기</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="viewDateTbl">
                                <colgroup>
                                    <col width="">
                                    <col width="">
                                    <col width="">
<!--                                     <col width=""> -->
<!--                                     <col width=""> -->
<!--                                     <col width=""> -->
<!--                                     <col width=""> -->
                                </colgroup>
                                <thead id="headings1">
                                    <tr>
                                        <th rowspan="2" id="baseDate">일자<!--  <i class="fa fa-sort" aria-hidden="true"></i> --></th>
                                        <th colspan="3">PC</th>
                                        <th colspan="3">Mobile</th>
<!--                                         <th colspan="3">4IOS</th> -->
<!--                                         <th colspan="3">Android</th> -->
                                    </tr>
                                    <tr>
                                        <th id="pv" title="제품클릭 건수">제품클릭 </th>
                                        <th id="recoPV" title="장바구니 건수">장바구니</th>
                                        <th id="searchCO" title="구매 건수">구매</th>
                                        <th id="searchPV" title="제품클릭 건수">제품클릭</th>
                                        <th id="recoPvStayTime" title="장바구니 건수">장바구니</th>
                                        <th id="recoPvAvrgStayTime" title="제품클릭 건수">구매</th>
<!--                                         <th id="cartAditActionCO" title="장바구니에 제품을 추가한 수">클릭</th> -->
<!--                                         <th id="cartRecoAditActionCO" title="장바구니에 추천제품을 추가한 수">고객</th> -->
<!--                                         <th id="orderPV" title="제품구매수">제품</th> -->
<!--                                         <th id="recoOrderPV" title="추천제품 구매수">클릭</th> -->
<!--                                         <th id="recoOrderPV" title="추천제품 구매수">고객</th> -->
<!--                                         <th id="recoOrderPV" title="추천제품 구매수">제품</th> -->
                                    </tr>
                                </thead>
                                <tbody id="pv_itemView_tbody1"></tbody>
                            </table>
                        </div>
                    </div>
                   <!-- 페이징 네비게이션 -->						
	                    <div class="row">
	                        <div class="col-md-2"></div>
	                        <div id="divPageNo1" class="col-md-8 pageNum"></div>
	                    </div>
	               <!-- 페이징 네비게이션 -->	
                </div>
            </div>
        </div>
    </div>
</div>
<div id="loading2" style="z-index:1002; display:none;">
    <div class="sk-cube-grid" style="z-index:1002">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
    <div class="sk-cube-txt" style="z-index:1002">초록마을</div>
</div>

<form id="excelInitForm" name="excelInitForm" />

<script type="text/javascript">
var controller = multiApiSet();
var userActionCnt=0;
var page_resultData;

$(document).ready(function() {
	$("#btnExcel").click(function(){
		
	    var startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
	    var endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
	    var dateType = $('.dActive').attr('data-dtype');
	    var url = '';  
	    var param = new Object;
	    var date = '';
	    
	    var limitOption = $("#dsplyCnt1").val();
	    var pageNo = $("#divPageNo1 .pageActive").text();
	    
		if(dateType == "A") {		
			date = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
			param.date = date;
			param.firstYmd = startDate;
			param.lastYmd = endDate;
			param.dateType = dateType;
		}
		else if(dateType == "B") {	
			date = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
			param.date = date;
			param.dateType = dateType;
		}
	    
	    param.firstYmd = startDate;
	    param.lastYmd = endDate;
	    param.dateType = dateType;
	    param.dsplyCnt = limitOption;
	    param.dsplyNo = pageNo;
	    
	    param.excelFileName = "수집분석_일별시간대별_항목별";
	    param.excelTitle 	= "수집분석_일별시간대별_항목별";
	    param.api = "analysis-cate-list";
		
		$('#excelInitForm').clone().attr('id', 'excelFrm').css('display', 'none').insertAfter( $('#excelInitForm') );
		$('#excelFrm').append('<input type="hidden" name="firstYmd" id="firstYmd" value="' + param.firstYmd + '" />');
		$('#excelFrm').append('<input type="hidden" name="lastYmd" id="lastYmd" value="' + param.lastYmd + '" />');
		$('#excelFrm').append('<input type="hidden" name="date" id="date" value="' + param.date + '" />');
		$('#excelFrm').append('<input type="hidden" name="dateType" id="dateType" value="' + param.dateType + '" />');
		$('#excelFrm').append('<input type="hidden" name="dsplyCnt" id="dsplyCnt" value="' + param.dsplyCnt + '" />');
		$('#excelFrm').append('<input type="hidden" name="dsplyNo" id="dsplyNo" value="' + param.dsplyNo + '" />');
		$('#excelFrm').append('<input type="hidden" name="excelFileName" id="excelFileName" value="' + param.excelFileName + '" />');
		$('#excelFrm').append('<input type="hidden" name="excelTitle" id="excelTitle" value="' + param.excelTitle + '" />');
		$('#excelFrm').append('<input type="hidden" name="api" id="api" value="' + param.api + '" />');
		
		$('#param').val(param);
		$('#excelFrm').attr('action', '/common/excelDownload.do');
		$('#excelFrm').attr('method', 'post');
		$('#excelFrm').submit();
		$('#excelFrm').remove();
	});
});
</script>
