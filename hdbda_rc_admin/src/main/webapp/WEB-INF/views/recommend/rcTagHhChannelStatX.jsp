<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	int columnCnt = 7;
%>
<table style="width:1200px;">
	<tr>
		<td>
			<table style="width:100%;" border="0">
				<tbody>
				<tr>
					<td colspan="<%=columnCnt%>" style="text-align:center;font-size:24px;font-weight:bold;">
					${excelTitle}
					</td>
				</tr>
				<tr>
					<td colspan="<%=columnCnt%>" style="heigth:20px;"></td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table style="width:100%;" border="1">
				<thead>
					<tr>
						<c:choose>
							<c:when test="${dateType == 'A'}">
								<th rowspan="2" style="background-color:#1f3a93; color: white; ">일자</th>
							</c:when>
							<c:when test="${dateType == 'B'}">
								<th rowspan="2" style="background-color:#1f3a93; color: white; ">일자</th>
								<th rowspan="2" style="background-color:#1f3a93; color: white; ">시간</th>
							</c:when>
							<c:otherwise>
								<th rowspan="2" style="background-color:#1f3a93; color: white; ">일자</th>
							</c:otherwise>
						</c:choose>
						<th colspan="3" style="background-color:#1f3a93; color: white; ">PC</th>
						<th colspan="3" style="background-color:#1f3a93; color: white; ">Mobile</th>
					</tr>
					<tr>
						<th style="background-color:#1f3a93; color: white; ">클릭</th>
						<th style="background-color:#1f3a93; color: white; ">고객</th>
						<th style="background-color:#1f3a93; color: white; ">제품</th>
						<th style="background-color:#1f3a93; color: white; ">클릭</th>
						<th style="background-color:#1f3a93; color: white; ">고객</th>
						<th style="background-color:#1f3a93; color: white; ">제품</th>
					</tr>
				</thead>
			
				<tbody>
					<c:forEach items="${excelList}" var="list" varStatus="status">
						<tr>
							<c:choose>
								<c:when test="${dateType == 'A'}">
									<td style="text-align: center;">${list.yyyymmdd}</td>
								</c:when>
								<c:when test="${dateType == 'B'}">
									<td style="text-align: center;">${list.yyyymmdd}</td>
									<td style="text-align: center;">${list.hh}</td>
								</c:when>
								<c:otherwise>
									<td style="text-align: center;">${list.yyyymmdd}</td>
								</c:otherwise>
							</c:choose>
							
							<td><fmt:formatNumber value="${list.pc_cnt}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.pc_device}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.pc_item}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.mobile_cnt}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.mobile_device}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.mobile_item}" pattern="#,###" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</td>
	</tr>
</table>
