<%------------------------------------------------------------------------
 * TODO > 추천성과분석 > 추천 수집 분석 > 일별 시간대별
 * 파일명   > rcEoadTagHhStat.jsp
 * 작성일   > 2019.09.01.
 * 작성자   > CCmedia Service Corp.
------------------------------------------------------------------------%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>

<script type="text/javascript">
var controller = multiApiSet();
var userActionCnt=0;
var page_resultData;

$(document).ready(function() {
	// 시작일 ,종료일, 데이터타입(일,시), 차트옵션 셋팅
    var startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
	var endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
	var dateType = $('.dActive').attr('data-dtype');
	var chartOption = $("#searchOptionNm option:selected" ).val();
	
	$('#datePickerId3').hide();
	$('#dateCal3').hide();
	
	getUserActionCnt(startDate, endDate, dateType, chartOption);
    initFunction();
});

function multiApiSet(){
	return "/collection/analysis";
	
	
	
	var api_url, api_protocol = location.protocol;
	var api_chkUrl = window.location.href;
	if('' != api_chkUrl) {
		if('http:' == api_protocol) {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
		} else {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
		}
	} else {
		return false;
	}
}

function initFunction() {
	
    // 10개,20개보기 콤보상자 선택
    $('#dsplyCnt1').change(function(){
        Paging1(userActionCnt, parseInt($(this).val()), 1, "divPageNo1");
    });
    
    // 일시 선택
    $(".dateRangeBtn").click(function(){ 	
        $(".dateRangeBtn").removeClass('dActive');
        $(this).addClass('dActive');      
        
        var dateType = $(this).attr('data-dtype');
        var startDate = '';
        var endDate = '';
        var chartOption = $("#searchOptionNm option:selected" ).val();
        
        if("A" == dateType) {
        	$('#datePickerId3').hide();
        	$('#dateCal3').hide();
        	$('#datePickerId1').show();
        	$('#dateCal1').show();
        	$("#baseDate").text("일자");
            startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
            endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
        } else {
        	$('#datePickerId1').hide();
        	$('#dateCal1').hide();
        	$('#datePickerId3').show();
        	$('#dateCal3').show();
        	$("#baseDate").text("시간");
            startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
            endDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
        }
    	getUserActionCnt(startDate, endDate, dateType, chartOption); 	  	
    });
}

// 달력 선택
function dateFunction1(startDate, endDate) {
	var dateType = $('.dActive').attr('data-dtype');
    $('#dsplyCnt1').val("10");
	getUserActionCnt(startDate, endDate, dateType, chartOption);  
}

// ajax 호출
function dateFunction3(startDate) {
	var chartOption = $("#searchOptionNm option:selected" ).val();
	var dateType = $('.dActive').attr('data-dtype');
	getUserActionCnt(startDate, '', dateType, chartOption);
}

// 차트데이터 jsonArray 배열에 셋팅
function echartMakeData1(data, chartOption, value1, value2, value3) {				   
    var jsonArray = new Array();
    
    if(data && data.length > 0) {
        for(var key in data) {      	
            var itemInfo = data[key];
            var object = new Object();
			
            if("A" == $('.dActive').attr('data-dtype')) {
                var month = itemInfo.yyyymmdd.substring(4,6)
                var day = itemInfo.yyyymmdd.substring(6,8)
//             	var month = itemInfo.yyyymmdd.substring(5,7);
//             	var day = itemInfo.yyyymmdd.substring(8,10);
            	var date = month + "/" + day;           
            	object.title = date;
            } else {
            	object.title = itemInfo.hh + "시";
            }
            
            if(chartOption == "custGrp1") {
            	object.value1 = itemInfo.view_click_1;
            }
            else if(chartOption == "custGrp2") {
            	object.value1 = itemInfo.view_click_2;
            }
            else if(chartOption == "custGrp3") {
            	object.value1 = itemInfo.view_click_3;
            }
            else if(chartOption == "custGrp4") {
            	object.value1 = itemInfo.view_click_4;
            }
            else if(chartOption == "custGrp5") {
            	object.value1 = itemInfo.view_click_5;
            }            
            else{
            	alert('서버와의 통신에 실패하였습니다.');
            }       
            jsonArray.push(object); // 배열에 데이터개체 셋팅
        }
     }  
     return jsonArray;
}

 
function getUserActionCnt(startDate, endDate, dateType, chartOption) {
	var param = new Object;
	var date = '';
	var url = '';
    
	if(dateType == "A") {		
		date = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
		param.date = date;
		param.firstYmd = startDate;
		param.lastYmd = endDate;
		param.dateType = dateType;
	}
	else if(dateType == "B") {	
		date = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
		param.date = date;
		param.dateType = dateType;
	}
	
	param.api = "analysis-days-count";
	url = controller + '/recommend/daysCount/' + param.api + ".do";

// 	console.log("url >>" + url);
// 	for (var key in param) {
// 		console.log("key >>" + key + ", value >>" + param[key]);
// 	}
	
    $.ajax({
        url:url,
        type:'POST',
        data:param,
        success: function(resultData) {
            if( ! $.isEmptyObject(resultData) ) {
            	userActionCnt = resultData.list.listCount
//                 userActionCnt = resultData.listCount;
                
// 				for (var key in resultData) {
// 					console.log("Attributes : " + key + ", value : " + resultData[key]);
// 				}
                
                $('#totalCount').empty().text(userActionCnt);
                Paging1(userActionCnt, $("#dsplyCnt1").val(), 1, "divPageNo1");           
                if(0== userActionCnt) {
                    var html = makeGridHtml1('');
                    $('#pv_itemView_tbody1').children().remove();
                    $('#pv_itemView_tbody1').append(html);
                }
            }
        },
        error : function(request, status, error) {
        	console.log(request);
        	console.log(status);
        	console.log(error);
            alert('서버와의 통신에 실패하였습니다.');
        }
    });
     dateFunction2(startDate, endDate, dateType, chartOption);
}

function dateFunction2 (startDate, endDate, dateType, chartOption){
	
	var param = new Object;
	var date = '';
	var url = '';
	
	if(dateType == "A") {		
		date = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
		param.date = date;
		param.firstYmd = startDate;
		param.lastYmd = endDate;
		param.dateType = dateType;
	}
	else if(dateType == "B") {	
		date = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
		param.date = date;
		param.dateType = dateType;
	}

	param.api = "analysis-days-chart";
	url = controller + '/recommand/analysis/' + param.api + ".do";
	
// 	console.log("url >>" + url);
// 	for (var key in param) {
// 		console.log("key >>" + key + ", value >>" + param[key]);
// 	}
	
    $.ajax({
        url:url,
        type:'POST',
        data:param,
		beforeSend:function(){
			maskOn2();
	        $("#loading2").show();
		},
        success:function(resultData){
            if(resultData.resultList) {
            	
	for (var i in resultData.resultList) {  // in 연산자는 상속계통을 모두 검색하여 맴버가 존재하는지 확인한다. (반복문중 가장 느리다.)
		if (resultData.resultList.hasOwnProperty(i)) {   // hasOwnProperty를 통해 해당 객체의 맴버인지 확인을 한다.
			console.log(resultData.resultList[i]);
		}
	}
            	
            	
                successData2(resultData.resultList, chartOption);
                fn_util_setTextType();
            } else {
                $('#graph-flot-points1').removeAttributes().html(nodata());
            }              
			$("#loading2").hide();
            maskOff2();
        },
        error:function(e){
            $('#graph-flot-points1').removeAttributes().html(nodata());
            alert('서버와의 통신에 실패하였습니다.');
			$("#loading2").hide();
            maskOff2();
        }
    });
}



// ajax success function
function successData2(data, chartOption){
    if( ! $.isEmptyObject(data) ) {
        drawChart(data, chartOption);
    }  else {
        $('#graph-flot-points1').removeAttributes().html(nodata());
    }
}


//Paging1(userActionCnt, $("#dsplyCnt1").val(), 1, "divPageNo1") 함수 호출후  renderDataByHtml1 콜백
function renderDataByHtml1(limitOption, pageNo){
    var startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
    var endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
    var dateType = $('.dActive').attr('data-dtype');
    var url = '';  
    var param = new Object;
    var date = '';
    
    
	if(dateType == "A") {		
		date = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
		param.date = date;
		param.firstYmd = startDate;
		param.lastYmd = endDate;
		param.dateType = dateType;
	}
	else if(dateType == "B") {	
		date = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
		param.firstYmd = date;
		param.dateType = dateType;
	}
    
     
    //param.firstYmd = startDate;
    //param.lastYmd = endDate;
    param.dateType = dateType;
    param.dsplyCnt = limitOption;
    param.dsplyNo = pageNo;
    
    param.api = "analysis-days-list";
    url = controller + '/recommend/daysList/' + param.api + ".do";
    
// 	console.log("url >>" + url);
// 	for (var key in param) {
// 		console.log("[renderDataByHtml1] key >>" + key + ", value >>" + param[key]);
// 	}
	
    $.ajax({
    	url:url,
    	type:'POST',
    	data:param,
		beforeSend:function(){
			maskOn2();
			$("#loading2").show();
		},
        success:function(resultData) {
//     		for (var i in resultData) {  // in 연산자는 상속계통을 모두 검색하여 맴버가 존재하는지 확인한다. (반복문중 가장 느리다.)
// 				if (resultData.hasOwnProperty(i)) {   // hasOwnProperty를 통해 해당 객체의 맴버인지 확인을 한다.
// 					console.log(resultData[i]);
// 				}
// 			}
        	
            if(resultData.list) {
                page_resultData = resultData.list;			
                var html = makeGridHtml1(resultData.list);
                $('#pv_itemView_tbody1').children().remove();
                $('#pv_itemView_tbody1').append(html);
                fn_util_setTextType();
            }
			$("#loading2").hide();
            maskOff2();
        },
        error:function(e) {
            alert('서버와의 통신에 실패하였습니다.');
            makeGridHtml1('');
            $("#loading2").hide();
            maskOff2();
        }
    });
}

var goPaging_divPageNo1 = function(cPage){
    Paging1(userActionCnt, $("#dsplyCnt1").val(), cPage, "divPageNo1");
};

function makeGridHtml1(tableData) {

    html = '';
    var itemList = tableData;
   
    if(itemList && itemList.length > 0) 
    {
        for(var key in itemList) 
        {
            var itemInfo = itemList[key];
            
//     		for (var i in itemInfo) {  // in 연산자는 상속계통을 모두 검색하여 맴버가 존재하는지 확인한다. (반복문중 가장 느리다.)
// 			if (itemInfo.hasOwnProperty(i)) {   // hasOwnProperty를 통해 해당 객체의 맴버인지 확인을 한다.
// 				console.log(itemInfo[i]);
// 			}
// 		}

            html += '<tr>';
            if(itemInfo['yyyymmdd'] != null) {
            	
				year = itemInfo['yyyymmdd'].substr(0,4);
				month = "-"+itemInfo['yyyymmdd'].substr(4,2);
				day = "-"+itemInfo['yyyymmdd'].substr(6,2);
				date = year+month+day;
				html += '<td>'+date+'</td>';
            	
//             	html += '<td>' + itemInfo['yyyymmdd'] + '</td>';  // 일자
            } else { 
            	html += '<td>'+ itemInfo['hh'] +"시" + '</td>';    // 시간
            }
            html += '	<td class="moneyType txtR">' + itemInfo['view_click_1']     + '</td>';
            html += '	<td class="moneyType txtR">' + itemInfo['view_click_2']     + '</td>';
            html += '	<td class="moneyType txtR">' + itemInfo['view_click_3']     + '</td>';
            html += '	<td class="moneyType txtR">' + itemInfo['view_click_4']     + '</td>';
            html += '	<td class="moneyType txtR">' + itemInfo['view_click_5']     + '</td>';
            
            html += '</tr>';
        }
    } else {
        html += '<tr><td colspan="18" style="text-align:center">No data available in table</td></tr>';
    }
	return html;
}

function nodata() {
    var html;
    html = ' <div class="noDataDiv">';
    html += ' <div class="dTxt">';
    html += ' <i class="ti-alert"></i>';
    html += ' <p>데이터가 없습니다.</p>';
    html += ' </div>';
    html += ' </div>';
    return html;
}

//echart 그리기
function drawChart(data, chartOption) {
	
	var chartOption = $("#searchOptionNm option:selected" ).val();
	
	for(var i=0; i<data.length; i++) {
		eval("date" + i + "= data[i].yyyymmdd");
	} 
	
    if(chartOption == "custGrp1") {
    	var json_data = echartMakeData1(data, chartOption, '일반회원 접속건수', '', '');
        var titleDate =['일반회원 접속건수'];
        var p_color = ['#2828cd'];
        
        barOverlabChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'Y', ' 건/명');
    }
    else if(chartOption == "custGrp2") {
    	var json_data = echartMakeData1(data, chartOption, '공공기관 접속건수', '', '');
        var titleDate =['공공기관 접속건수'];
        var p_color = ['#8b4f1d'];
        barOverlabChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'Y', ' 건/명');
    }
    else if(chartOption == "custGrp3") {
    	var json_data = echartMakeData1(data, chartOption, '사회적기업 접속건수', '', '');
        var titleDate =['사회적기업 접속건수'];
        var p_color = ['#ff8c0a'];
        barOverlabChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'Y', ' 건/명');
    }
    else if(chartOption == "custGrp4") {
    	var json_data = echartMakeData1(data, chartOption, 'STORE36.5 매장 접속건수', '', '');
        var titleDate =['STORE36.5 매장 접속건수'];
        var p_color = ['#84799f'];
        barOverlabChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'Y', ' 건/명');
    }
    else if(chartOption == "custGrp5") {
    	var json_data = echartMakeData1(data, chartOption, '유통채널 접속건수', '', '');
        var titleDate =['유통채널 접속건수'];
        var p_color = ['#ca77b4'];
        barOverlabChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'Y', ' 건/명');
    }
	else {
    	alert('서버와의 통신에 실패하였습니다.');
    } 	 
}


function chartOption() {
	var option = $("#searchOptionNm option:selected" ).val();
	var dateType = $('.dActive').attr('data-dtype');
	var startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
	var endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
	getUserActionCnt(startDate, endDate, dateType, chartOption);
}

</script>
<div class="mConBox">
    <div class="mCont">
        <div class="row" >
            <div class="col-md-4" id="subtitleA">
                <h2><i class="ti-layers"></i>전체 수집 분석 통계</h2>
            </div>
            <div class="col-md-8">
                <div class="pageRoute">
                    <ul>
                        <li><a href="#">HOME</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li><a href="#">서비스 현황 분석</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li><a href="#">사용자행동데이터 조회</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li class="current"><a href="#"> 서비스 고객 행동기록 통계</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row" >
            <div class="col-md-12 text-right">
                <div class="conBox" style="min-height:400px;">
                    <div class="setBoxListArea">
                        <div id="setAreaIdLIstBox" class="setBoxListBox">
                            <ul date-limitSel='3'></ul>
                            <div class="setBoxListBtn">
                                <button class="darkGrayBtn" id="setBoxListSearch">조회</button>
                            </div>
                        </div>
                    </div>
					<div class="setBoxListArea mr10 pull-left">
					<div class="setBoxListArea mr10">                  
                    <select id="searchOptionNm" onchange="chartOption()">
                    	<option value="custGrp1">일반회원</option>
                    	<option value="custGrp2">공공기관</option>
                    	<option value="custGrp3">사회적기업</option>
                    	<option value="custGrp4">STORE36.5 매장</option>
                    	<option value="custGrp5">유통채널</option>
                    </select>
                    </div>
					</div>
					<button class="dateRangeBtn dActive" data-dtype="A">일</button>
                    <button class="dateRangeBtn" data-dtype="B">시</button>
                    <div class="dateRange">
                        <input type="text" id="datePickerId1" />
                        <i class="ti-calendar" id="dateCal1"></i>
                    </div>
                    <div class="dateRange">
                        <input type="text" id="datePickerId3" />
                        <i class="ti-calendar" id="dateCal3"></i>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="gInBox" id="graph-flot-points1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" >
            <div class="col-md-12" id="subtitleB">
                <h2><i class="ti-layers"></i> 전체 수집 분석 통계 상세</h2>
            </div>
        </div>
        <div class="row" >
            <div class="col-md-12">
                <div class="conBox" style="min-height:400px;">
                    <div class="row">
                        <div class="col-md-6 select-set-3">
                            <p> 총 <b class="moneyType" id="totalCount"></b> 개</p>
                        </div>
                        <div class="col-md-6 text-right select-set-2">
                        	<button type="button" id="btnExcel" class="exDwBtn">엑셀</button>
                            <select name="dsplyCnt1" id="dsplyCnt1">
                                <option value="10">10개씩 보기</option>
                                <option value="20">20개씩 보기</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="viewDateTbl">
                                <colgroup>
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                </colgroup>
                                <thead id="headings1">
                                    <tr>
                                        <th rowspan="2" id="baseDate">일자</th>
                                        <th -colspan="3">일반회원<br>접속건수</th>
                                        <th -colspan="3">공공기관<br>접속건수</th>
                                        <th -colspan="3">사회적경제기업<br>접속건수</th>
                                        <th -colspan="3">STORE 36.5매장<br>접속건수</th>
                                        <th -colspan="4">유통채널<br>접속건수</th>
                                </thead>
                                <tbody id="pv_itemView_tbody1"></tbody>
                            </table>
                        </div>
                    </div>
                   <!-- 페이지 네비게이션 -->						
	                    <div class="row">
	                        <div class="col-md-2"></div>
	                        <div id="divPageNo1" class="col-md-8 pageNum"></div>
	                    </div>
	               <!-- 페이지 네비게이션 -->	                    
                </div>
            </div>
        </div>
    </div>
</div>
<div id="loading2" style="z-index:1002; display:none;">
    <div class="sk-cube-grid" style="z-index:1002">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
    <div class="sk-cube-txt" style="z-index:1002">초록마을</div>
</div>

<form id="excelInitForm" name="excelInitForm" />

<script type="text/javascript">
var controller = multiApiSet();
var userActionCnt=0;
var page_resultData;

$(document).ready(function() {
	$("#btnExcel").click(function(){
		
	    var startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
	    var endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
	    var dateType = $('.dActive').attr('data-dtype');
	    var url = '';  
	    var param = new Object;
	    var date = '';
	    
	    var limitOption = $("#dsplyCnt1").val();
	    var pageNo = $("#divPageNo1 .pageActive").text();
	    
		if(dateType == "A") {		
			date = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
			param.date = date;
			param.firstYmd = startDate;
			param.lastYmd = endDate;
			param.dateType = dateType;
		}
		else if(dateType == "B") {	
			date = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
			param.date = date;
			param.dateType = dateType;
		}
	    
	    param.firstYmd = startDate;
	    param.lastYmd = endDate;
	    param.dateType = dateType;
	    param.dsplyCnt = limitOption;
	    param.dsplyNo = pageNo;
	    
	    param.excelFileName = "수집분석_일별시간대별";
	    param.excelTitle 	= "수집분석_일별시간대별";
	    param.api = "analysis-days-list";
		
		$('#excelInitForm').clone().attr('id', 'excelFrm').css('display', 'none').insertAfter( $('#excelInitForm') );
		$('#excelFrm').append('<input type="hidden" name="firstYmd" id="firstYmd" value="' + param.firstYmd + '" />');
		$('#excelFrm').append('<input type="hidden" name="lastYmd" id="lastYmd" value="' + param.lastYmd + '" />');
		$('#excelFrm').append('<input type="hidden" name="date" id="date" value="' + param.date + '" />');
		$('#excelFrm').append('<input type="hidden" name="dateType" id="dateType" value="' + param.dateType + '" />');
		$('#excelFrm').append('<input type="hidden" name="dsplyCnt" id="dsplyCnt" value="' + param.dsplyCnt + '" />');
		$('#excelFrm').append('<input type="hidden" name="dsplyNo" id="dsplyNo" value="' + param.dsplyNo + '" />');
		$('#excelFrm').append('<input type="hidden" name="excelFileName" id="excelFileName" value="' + param.excelFileName + '" />');
		$('#excelFrm').append('<input type="hidden" name="excelTitle" id="excelTitle" value="' + param.excelTitle + '" />');
		$('#excelFrm').append('<input type="hidden" name="api" id="api" value="' + param.api + '" />');
		
		$('#param').val(param);
		$('#excelFrm').attr('action', '/common/excelDownload.do');
		$('#excelFrm').attr('method', 'post');
		$('#excelFrm').submit();
		$('#excelFrm').remove();
	});
});
</script>
