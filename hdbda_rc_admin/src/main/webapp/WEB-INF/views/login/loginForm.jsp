<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>

<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!--     <link rel="shortcut icon" href="/img/favicon.ico"> -->
    <title>초록마을 제품추천</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
	<!-- fontawesome -->
	<link href="/fonts/font-awesome.css" rel="stylesheet">
    <!-- Custom -->
	<link href="/css/themify-icons.css" rel="stylesheet">
	<link href="/css/offset.css" rel="stylesheet">
	<link href="/css/custom.css" rel="stylesheet">
	<link href="/css/login.css" rel="stylesheet">
	<link href="/css/res.css" rel="stylesheet">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery-1.12.4.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
	<script src="/js/custom.js"></script>
	<script src="/js/login.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		if(opener){
			autoGoLogin();
		} else {
			$('.loginBg').show();
		}	
	});
	
	function autoGoLogin() {
		history.replaceState({}, null, location.pathname);
		
		$('#loginBg').hide();
		$("#user_opt").val("SSO");
		$("#ossvi").val("${ossvi}");
		$("#ossts").val("${ossts}");
		$("#ossct").val("${ossct}");
		
		$("#loginUserId").val("${ossdi}");
// 		$("#loginUserPassword").val("< % = ossdwp % >");
		
// 		saveid(loginForm);
		$("#loginForm").submit();
	}
	
	function goLogin() {
		
		if ($("#loginUserId").val() == "") {
				alert("아이디를 입력하세요.");
				$("#loginUserId").focus();
				return;
		}	
		if ($("#loginUserPassword").val() == "") {
			alert("비밀번호를 입력하세요.");
			$("#loginUserPassword").focus();
			return;
		}
		saveid(loginForm);
		$("#loginForm").submit();
	}

	// 쿠키에 로그인 ID 저장
	function saveid(form) {
		var expdate = new Date();

		// 아이디 저장 버튼 체크
		if (form.rememberMe.checked) {
			// 기본 30일 기억
			expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 30); // 30일.
		} else {
			// 쿠키 삭제
			expdate.setTime(expdate.getTime() - 1);
		}

		setCookie("com.ebro.adminAuthCookie", form.loginUserId.value, expdate);
	}

	function setCookie(name, value, expires) {
		document.cookie = name + "=" + escape(value) + "; path=/; expires=" + expires.toGMTString();
	}

	function getSavedId(form) {
		//아이디 입력창에 ookie에 저장된 id 설정
		form.loginUserId.value = getCookie("com.ebro.adminAuthCookie");
		// 아이디가 존재하면 아이디저장 체크박스 활성화
		form.rememberMe.checked = (form.loginUserId.value != "" ? true: false);
	}

	// 쿠키 값 추출
	function getCookie(Name) {
		var search = Name + "=";
		// 쿠키갯수 확인
		if (document.cookie.length > 0) {
			offset = document.cookie.indexOf(search);
			// 쿠키가 존재하면.
			if (offset != -1) {
				offset += search.length;

				end = document.cookie.indexOf(";", offset);

				// 쿠키 값의 마지막 위치 인덱스 번호 설정.
				if (end == -1)
					end = document.cookie.length;

				return unescape(document.cookie.substring(offset, end));
			}
		}
		return "";
	}
	</script>
	
	<!-- 로그인 처리 후 오류 메시지가 세팅된 경우 여기서 Alert -->
	<c:if test="${! empty systemMsg}">
		<script>
			if(opener){
				alert("${systemMsg}");
				window.close();
			} else {
				alert("${systemMsg}");
				$("#loginUserId").val("");
				$("#loginUserPassword").val("");
	            //location.href = location.href;
			}	

        </script>
	</c:if>
	
    <!-- 처음 로그인 메시지가 세팅된 경우 여기서 Alert -->
	<c:if test="${! empty firstLoginMsg}">
        <script>
           alert("${firstLoginMsg}");
        </script>
    </c:if>
    
    <!-- 비밀번호 재사용 경고 메시지 -->
	<c:if test="${! empty usedPwMsg}">
        <script>
           alert("${usedPwMsg}");
        </script>
    </c:if>
</head>

<div id="loading"></div>
<body id="loginWrap"  onLoad="getSavedId(loginForm)">
	<div class="loginBg" style="display:none;">
		<div class="loginBox">
            <div class="loginBoxTop">
            	초록마을
            	<span>관리자</span>
<!--             	<img src="/img/logo_hmall.png" alt="초록마을" class="logoImg"> -->
            </div>
            <div class="loginBoxList">
                <form action="/login/loginProcess.do" method="post" id="loginForm">
                    <ul>
                        <li>
                            <span><label for="loginUserId"><i class="ti-user"></i></label></span>
                            <input id="loginUserId" name="loginUserId" value="" type="text"  class="form-control" placeholder="아이디를 입력하세요." maxlength="25" />
                        </li>
                        <li>
                            <span><label for="loginUserPassword"><i class="ti-lock"></i></label></span>
                            <input id="loginUserPassword"  name="loginUserPassword" value="" type="password" class="form-control" placeholder="비밀번호를 입력하세요." maxlength="20" />
                            <input type="hidden" id="user_opt" name="user_opt" value="1"/>
                            <input type="hidden" id="ossvi" name="ossvi" value=""/>
                            <input type="hidden" id="ossts" name="ossts" value=""/>
                            <input type="hidden" id="ossct" name="ossct" value=""/>
                        </li>
                        <li class="pd-0 text-left">
                            <input type="checkbox"  class="rdChk" id="rememberMe" checked="checked" />
                            <label class="chkSel pullLeft" for="rememberMe">아이디저장</label>
                        </li>
                        <li class="pd-0">
                            <button onClick="goLogin();return false;">LOGIN</button>
                        </li>
                        <li class="pd-0">* 등록된 아이디와 비밀번호로 로그인 하실 수 있습니다.</li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</body>
</html>