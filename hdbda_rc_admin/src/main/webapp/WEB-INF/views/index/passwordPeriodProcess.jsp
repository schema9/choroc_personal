<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>

<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>초록마을 제품추천</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

	<!-- fontawesome -->
	<link href="/fonts/font-awesome.css" rel="stylesheet">

    <!-- Custom -->
	<link href="/css/themify-icons.css" rel="stylesheet">
	<link href="/css/offset.css" rel="stylesheet">
	<link href="/css/custom.css" rel="stylesheet">
	<link href="/css/login.css" rel="stylesheet">
	<link href="/css/res.css" rel="stylesheet">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery-1.12.4.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>

	<script src="/js/custom.js"></script>
	<script src="/js/login.js"></script>



    <script type="text/javascript">

        function passwordPeriodExtn(){

            $("#writeForm").submit();
        }
    </script>



</head>

<div id="loading"></div>

<body id="loginWrap">

	<div class="loginBg">
		<div class="loginBox">
            <div class="chgPsdBox" style="display:block">
                <div class="logBoxTop chgPsdBoxTop">e-store 36.5+</div>
                <div class="logBoxCon">
                    <form action="/loginCheck/passwordPeriodProcess.do" method="post" id="writeForm">
                        <input type="hidden" name="userId" id="userId" value="${sessionScope.userInfo.userId}" />

                        <p class="fw-b chgPsdInfo chgPsdTop" >회원님, <br /><span class=" chgPsd_redBig">주기적인 비밀번호 변경</span> 으로 <br />소중한 개인정보를 지켜주세요!</p>

                        <div class="logconBox chgPsdconBox">
                            <p>회원님은 3개월 동안 비밀번호를 변경하지 않으셨습니다. <br />
                            비밀번호는 <span class="chgPsd_red">MY정보/가이드 > PROFILE > 계정정보 수정</span>에서 변경 가능합니다. </p>
                        </div>

                        <p class="fw-b chgPsdInfo">안정한 정보보호를 위해 <span class="chgPsd_red">3개월</span>마다 <span class="chgPsd_red">비밀번호를 변경</span>해주세요.</p>

                        <div class="logBtn chgPsdBtn">
                             <button class="logbtn" onClick="passwordPeriodExtn();" type="button">30일간 보이지 않기</button>
                        </div>
                    </form>
                </div>
            </div>
		</div>
	</div>
</body>
</html>