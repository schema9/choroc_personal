<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>

<%
	String changePasswdMsg = request.getParameter("changePasswdMsg") == null ? "" : request.getParameter("changePasswdMsg");
	
	if (changePasswdMsg.equals("firstLogin")) {
		changePasswdMsg = "처음 로그인 시 비밀번호 변경후 사용 가능합니다.";
	}
	else if(changePasswdMsg.equals("overDatePw")) {
		changePasswdMsg = "90일간 비밀번호를 변경 하지 않았습니다. 비밀번호를 변경하세요.";
	}
	else if(changePasswdMsg.equals("overDateLogin")) {
		changePasswdMsg = "90일간 로그인 하지 않았습니다. 비밀번호를 변경하세요.";
	}
	else {
		changePasswdMsg = "비밀번호를 변경하세요.";
	}
	
	pageContext.setAttribute("msg", changePasswdMsg);
%>

<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>초록마을 제품추천</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--     <link rel="shortcut icon" href="/img/favicon.ico"> -->
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- fontawesome -->
    <link href="/fonts/font-awesome.css" rel="stylesheet">

    <!-- Custom -->
    <link href="/css/themify-icons.css" rel="stylesheet">
    <link href="/css/offset.css" rel="stylesheet">
	<link href="/css/custom.css" rel="stylesheet">
	<link href="/css/login.css" rel="stylesheet">
    <link href="/css/res.css" rel="stylesheet"

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery-1.12.4.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>

	<script src="/js/custom.js"></script>
	<script src="/js/login.js"></script>


    <script type="text/javascript">

        $(document).ready(function(){
			alert('<c:out value="${msg}"/>');            
        });
        
        function savePassword() {

            var pwRegExp = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,20}/;

            if ($("#userPassword").val().trim() == "") {
                alert("비밀번호를 입력하세요.");
                $("#userPassword").val("");
                $("#userPassword").focus();
                return false;
            }

            if ($("#userPassword2").val().trim() == "") {
                alert("비밀번호 확인을 입력하세요.");
                $("#userPassword2").focus();
                return false;
            }

            if($("#userPassword").val() != $("#userPassword2").val()){

                alert("비밀번호 확인이 다르게 입력 되었습니다.");
                $("#userPassword2").val("");
                $("#userPassword2").focus();
                return false;
            }                     
            
            if (!pwRegExp.test($("#userPassword").val())) 
            {
                alert("잘못된 비밀번호 형식입니다. 8~20자 이내의 영문자,숫자,특수문자를 혼합해서 입력하세요.");
                $("#userPassword").val("");
                $("#userPassword").focus();
                return false;
            }
            
            // 기존에 입력된 비밀번호 조회후 동일 비밀번호는 사용금지
/*             if( !pwdHisCntCheck()) {
            	alert("해당 비밀번호는 기존에 이미 사용했던 비밀번호입니다. 신규 비밀번호를 입력해 주시기 바랍니다.");
                $("#userPassword").val("");
                $("#userPassword").focus();
                return false;
            } */
           
            if (confirm("변경후 다시 로그인 해주셔야 합니다.변경 하시겠습니까?")) 
            {
              $("#writeForm").submit();
            }
            else {
				return false;
            }
        }
    </script>

<style type="text/css">

.loginBoxPwTop
{
	background:url('../img/mainBodyReset.jpg');
}

</style>

</head>

<div id="loading"></div>

<body id="loginWrap">

	<div class="loginBg">

    		<div class="loginBox changePw">
                <div class="loginBoxTop "><img src="../img/logo_hmall.png" alt="e-store 36.5+" class="logoImg"></div>
    			<div class="loginBoxPwTop">
                    <h3>비밀번호 변경</h3>
                </div>
    			<div class="loginBoxList">

                     <form action="/loginCheck/firstLoginProcess.do" method="post" id="writeForm">
                        <input type="hidden" name="hidden" id="hidden" value="" />
                        <ul>                  
                            <li class="pd-0 chPwCor">* 비밀번호 변경 후 로그인 하실 수 있습니다.</li>
                            <li>
                                <span><label for="userId"><i class="ti-user"></i></label></span>
                                <input type="text" id="userId" name="userId" value="${sessionScope.userInfo.userId}" readOnly>
                            </li>
                            <li>
                                <span><label for="userPassword"><i class="ti-lock"></i></label></span>
                                <input id="userPassword" name="userPassword" type="password" placeholder="변경할 비밀번호">
                            </li>

                            <li>
                                <span><label for="userPassword2"><i class="ti-check"></i></label></span>
                                <input id="userPassword2" name="userPassword2" type="password" placeholder="비밀번호확인">
                            </li>

                            <li class="pd-0">
                                <button class="logbtn" onClick="savePassword();" type="button">비밀번호 변경</button>
                            </li>
    				    </ul>
    				 </form>
    			</div>
    		</div>
    	</div>


	<c:if test="${lastLoginSuccesDt == true}">
        <script>
           alert("비밀번호 변경후 이용하실수 있습니다.");
        </script>
    </c:if>

</body>
</html>