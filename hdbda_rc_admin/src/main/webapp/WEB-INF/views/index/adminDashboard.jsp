<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>

<script type="text/javascript">
var apiUrl = multiApiSet();

    $(window).ready(function(){
		if(opener){
			$(".adminArea").hide();
			setInterval(function(){
//					alert(opener.document.getElementById("jsessionid").value);
			},2000);
			
//				setInterval(function(){
//				},1000);				
		} else {
		}
    	
    	// 선택한 날짜
        var startDate = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
        var chartOption = $("#searchOptionNm option:selected" ).val();
                             
        // 실적 ajax
        dateFunction3(startDate, chartOption);
    });

    function multiApiSet(){
    	return "/index";
		var api_url, api_protocol = location.protocol;
		var api_chkUrl = window.location.href;
		if('' != api_chkUrl) {
			if('http:' == api_protocol) {
				return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
			} else {
				return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
			}
		} else {
			return false;
		}
	}
    
    // echat 사용하기 위해 data를 jsonArray 담아 return 
    function echartMakeData1(data, chartOption, value1, value2, value3) {
						  
        var jsonArray = new Array();
        if(data && data.length > 0) {
            for(var key in data) {
                var itemInfo = data[key];
                var object = new Object();
				
                var month = itemInfo.yyyymmdd.substring(4,6)
                var day = itemInfo.yyyymmdd.substring(6,8)
                var date = month + "/" + day;
                
                object.title = date;
                
                // 차트 옵션 = RecoPV
                if(chartOption == "RecoPV"){
					object.value1 = itemInfo.view_cnt;				// 클릭 건수
					object.value2 = itemInfo.view_rc_cnt;			// 클릭 건수 (추천)
                }
                // 차트 옵션 = CTR
                else if(chartOption == "CTR"){
                	var rate = 0;
                	rate = ((itemInfo.view_rc_cnt / itemInfo.view_cnt) * 100).toFixed(2);	
                	object.value1 = rate;
                }
                // 차트 옵션 = CR
                else if(chartOption == "CR"){
                	var rate = 0;
                	rate = ((itemInfo.buy_rc_cnt / itemInfo.buy_cnt ) * 100).toFixed(2); 
                	object.value1 = rate;
                }
                // 차트 옵션 = OrderPrice
                else if(chartOption == "OrderPrice"){
                	object.value1 = itemInfo.buy_price_sum;
                	object.value2 = itemInfo.buy_price_rc_sum;
                }else{
                	alert('서버와의 통신에 실패하였습니다.');
                }
            jsonArray.push(object);
            }
         } else if(data != null) {
            var object = new Object();
            object.title = itemInfo[title];
            object.value1 = data[value1];
            object.value2 = data[value2];
            object.value3 = (data[value3]);
            jsonArray.push(object);
        }
        return jsonArray;
    }
    function echartMakeData2(data, value1) {

        var jsonArray = [] ;

        if(data && data.length > 0) {
            for(var key in data) {
                var itemInfo = data[key];
                jsonArray.push(itemInfo[value1]);
            }
         } else if(data != null) {
            jsonArray.push(data[value1]);
        }
        return jsonArray;
    }
    function echartMakeData3(ajaxDataVal, title, value1) {

        var jsonArray = new Array();
        var itemList = ajaxDataVal;

        if(itemList && itemList.length > 0) {
            for(var key in itemList) {
                var itemInfo = itemList[key];
                var object = new Object();
                object.value = itemInfo[value1];
                object.name = itemInfo[title];
                jsonArray.push(object);
            }
         } else if(itemList != null) {
            var object = new Object();
            object.value = itemList[value1];
            object.name = itemList[title];
            jsonArray.push(object);
         }
         return jsonArray;
     }

    // 달력 prevDay 이벤트
    function prevDay(){
        var date =  "";
        var prevDate ="";

        date = $('#datePickerId3').data('daterangepicker').startDate.subtract(1, 'days');
        $('#datePickerId3').data('daterangepicker').setStartDate(date);
        $('#datePickerId3').data('daterangepicker').setEndDate(date);

        prevDate = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
        var chartOption = $("#searchOptionNm option:selected" ).val();
        dateFunction3(prevDate, chartOption);

    }
    // 달력 nextDay 이벤트
    function nextDay(){
        var date =  "";
        var nextDate = "";

        date = $('#datePickerId3').data('daterangepicker').startDate.subtract(-1, 'days');
        $('#datePickerId3').data('daterangepicker').setStartDate(date);
        $('#datePickerId3').data('daterangepicker').setEndDate(date);

        nextDate = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
        var chartOption = $("#searchOptionNm option:selected" ).val();
        dateFunction3(nextDate, chartOption);

    }

    // ajax 호출
    function dateFunction3(startDate, chartOption)
    {
        var url = '';
        
        var date = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
        var param = new Object;
        param.date = date;
        param.api = "main-dashboard-performance";
        
        url =  apiUrl + "/report/performance/" + param.api + ".do";

        $.ajax({
            type:'POST',
            url:url,
            dataType: "json",
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            data:param,          
            success:function(resultData){
                if(resultData.resultList) {
                    successData(resultData.resultList);
                    fn_util_setTextType();
                }
            },
            error:function(e){
                alert('서버와의 통신에 실패하였습니다.');              
            }
        });

         dateFunction2(startDate, chartOption); 
    }

    // ajax 호출
    function dateFunction2 (startDate, chartOption){
		var param = new Object;
    	var date = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
		param.date = date;
		param.api = "main-dashboard-chart";
		
        url = apiUrl+"/report/performanceChart/" + param.api + ".do";

        $.ajax({
            url:url,
            type:'POST',
            data:param,
    		beforeSend:function(){
    			maskOn2();
    	        $("#loading2").show();
    		},
            success:function(resultData){
                if(resultData.resultList) {
                    successData2(resultData.resultList, chartOption);
                    fn_util_setTextType();
                } else {
                    $('#graph-flot-points1').removeAttributes().html(nodata());
                    $('#graph-flot-points2').removeAttributes().html(nodata());
                }
    			$("#loading2").hide();
                maskOff2();
            },
            error:function(e){
                $('#graph-flot-points1').removeAttributes().html(nodata());
                $('#graph-flot-points2').removeAttributes().html(nodata());
                alert('서버와의 통신에 실패하였습니다.');
    			$("#loading2").hide();
                maskOff2();
            }
        });
    }

    // ajax success function
    function successData(data){

        var todayMap = data[0];			
        var yesterdayMap = data[1];	    
         
        var recoPv = todayMap.view_rc_cnt ? todayMap.view_rc_cnt : 0;							// recoPv - 클릭 건수
        var beforeRecoPv = yesterdayMap.view_rc_cnt ? yesterdayMap.view_rc_cnt : 0;				
		
        
        var ctrRate = ( todayMap.view_rc_cnt / todayMap.view_cnt ) * 100;						// 이틀전 추천 제품 클릭 대비 추천 비율 
        var beforeCtrRate = ( yesterdayMap.view_rc_cnt / yesterdayMap.view_cnt ) * 100;			
        var ctr = ctrRate ? ctrRate : 0.00;														// 추천 제품 클릭
        var beforeCtr = beforeCtrRate ? beforeCtrRate : 0.00;									
        
        
        var crRate = ( todayMap.buy_rc_cnt / todayMap.buy_cnt) * 100;							//  추천 제품 구매 대비 추천 비율
        var beforeCrRate =  ( yesterdayMap.buy_rc_cnt / yesterdayMap.buy_cnt) * 100;			
        var cr = crRate ? crRate : 0.00;														// 구매건수
        var beforeCr = beforeCrRate ? beforeCrRate : 0.00;										
        
        
        var totalPrice = todayMap.buy_price_rc_sum ? todayMap.buy_price_rc_sum : 0;  				// totalPrice - 어제 구매 총액
        var beforeTotalPrice = yesterdayMap.buy_price_rc_sum ? yesterdayMap.buy_price_rc_sum : 0;   // 구매 - 총액

        
        var avgRecoPv = (recoPv+beforeRecoPv)/2;			
        avgRecoPv = avgRecoPv.toFixed(0)
        var avgTotalPrice = (totalPrice+beforeTotalPrice)/2;	
        avgTotalPrice = avgTotalPrice.toFixed(0) 

        var avgCr = (cr+beforeCr)/2;		// 구매 - 건수 
        var avgCtr = (ctr+beforeCtr)/2;		// 구매 - 건수추천 

        var yesterComparePv = $("#yesterComparePv");
        var yesterComparePvPer = $("#yesterComparePvPer");
        var todayPv = $("#todayPv");
        var yesterPv = $("#yesterPv");
        var avgPv = $("#avgPv");
        var yesterCompareCr = $("#yesterCompareCr");
        var todayCr = $("#todayCr");
        var yesterCr = $("#yesterCr");
        var avgCrObj = $("#avgCrObj");
        var yesterCompareCtr = $("#yesterCompareCtr");
        var todayCtr = $("#todayCtr");
        var yesterCtr = $("#yesterCtr");
        var avgCtrObj = $("#avgCtrObj");
        var yesterComparePrice = $("#yesterComparePrice");
        var yesterComparePricePer = $("#yesterComparePricePer");
        var todayPrice = $("#todayPrice");
        var yesterPrice = $("#yesterPrice");
        var avgPrice = $("#avgPrice");

        var pieceTogetherRecoPv = $("#pieceTogetherRecoPv");
        var pieceTogetherCr = $("#pieceTogetherCr");
        var pieceTogetherCtr = $("#pieceTogetherCtr");
        var pieceTogetherTotalPrice = $("#pieceTogetherTotalPrice");

        var pvPrefix = "", ctrPrefix = "", crPrefix = "", pricePrefix = "";

        if(recoPv > beforeRecoPv){
            pvPrefix = "+";
        }
        if(ctr > beforeCtr){
            ctrPrefix = "+";
        }
        if(cr > beforeCr){
            crPrefix = "+";
        }
        if(totalPrice > beforeTotalPrice){
            pricePrefix = "+";
        }

        //PV (추천 제품 클릭 건수 = View_rc_cnt)
        var comparePv = recoPv - beforeRecoPv;
        if(beforeRecoPv == 0 && comparePv != 0){
            yesterComparePv.empty().text(pvPrefix + comparePv + ' 건');
            yesterComparePvPer.empty().text(" (" + (100).toFixed(2) + "%) ");
        }else if(beforeRecoPv == 0 && comparePv == 0){
            yesterComparePv.empty().text(pvPrefix + comparePv + ' 건');
            yesterComparePvPer.empty().text(" (" + (0).toFixed(2) + "%) ");
        }else{
            yesterComparePv.empty().text(pvPrefix + comparePv + ' 건');
            yesterComparePvPer.empty().text(" (" + (comparePv / beforeRecoPv * 100).toFixed(2) + "%) ");
        }
        todayPv.empty().text(recoPv + ' 건');
        avgPv.empty().text(avgRecoPv + ' 건');
        yesterPv.empty().text(beforeRecoPv + ' 건');

        //CTR (추천 제품 클릭 대비 추천 비율 = (클릭 건수[추천] / 클릭 건수) * 100 )
        var compareCtr = ctr - beforeCtr;
        yesterCompareCtr.empty().text(ctrPrefix + (compareCtr).toFixed(2));
        todayCtr.empty().text((ctr).toFixed(2) + " %");
        avgCtrObj.empty().text((avgCtr).toFixed(2) + " %");
        yesterCtr.empty().text((beforeCtr).toFixed(2) + " %");

        //CR (추천 제품 구매 대비 추천 비율 = (구매 건수[추천] / 구매 건수) * 100)
        var compareCr = cr - beforeCr;
        yesterCompareCr.empty().text(crPrefix + (compareCr).toFixed(2));
        todayCr.empty().text((cr).toFixed(2) + " %");
        avgCrObj.empty().text((avgCr).toFixed(2) + " %");
        yesterCr.empty().text((beforeCr).toFixed(2) + " %");

        //TOTAL PRICE (추천 제품 구매 금액 = buy_price_rc_sum)
        var comparePrice = totalPrice - beforeTotalPrice;
        if(beforeTotalPrice == 0 && comparePrice != 0){
            yesterComparePrice.empty().text(pricePrefix + comparePrice + ' 원');
            yesterComparePricePer.empty().text(" (" + (100).toFixed(2) + "%)");
        }else if(beforeTotalPrice == 0 && comparePrice == 0){
            yesterComparePrice.empty().text(pricePrefix + comparePrice + ' 원');
            yesterComparePricePer.empty().text(" (" + (0).toFixed(2) + "%)");
        }else{
            yesterComparePrice.empty().text(pricePrefix + comparePrice + ' 원');
            yesterComparePricePer.empty().text(" (" + (comparePrice / beforeTotalPrice * 100).toFixed(2) + "%)");
        }
        todayPrice.empty().text(totalPrice + " 원");
        yesterPrice.empty().text(beforeTotalPrice + " 원");
        avgPrice.empty().text(avgTotalPrice + " 원");
    }

    // ajax success function
    function successData2(data, chartOption){
        if( ! $.isEmptyObject(data) ) {
            drawChart(data, chartOption);
        }  else {
            $('#graph-flot-points1').removeAttributes().html(nodata());
            $('#graph-flot-points2').removeAttributes().html(nodata());
        }
    }

    function nodata() {
        var html;

        html = ' <div class="noDataDiv">';
        html += ' <div class="dTxt">';
        html += ' <i class="ti-alert"></i>';
        html += ' <p>* 데이터가 없습니다.</p>';
        html += ' </div>';
        html += ' </div>';

        return html;
    }

    // echart 그리기
    function drawChart(data, chartOption){
    	var chartOption = $("#searchOptionNm option:selected" ).val();
    	
    	for(var i=0; i<data.length; i++){
    		eval("date" + i + "= data[i].yyyymmdd");
    	} 
    	// 차트 옵션 = RecoPV
        if(chartOption == "RecoPV"){
        	var json_data = echartMakeData1(data, chartOption, '접속 건수', '추천 제품 클릭 건수', '');
            var titleDate =['접속 건수', '추천 제품 클릭 건수'];
            var p_color = ['#2828cd','#a0a0ff'];
            barOverlabChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'Y', ' 건/명');
        }
        // 차트 옵션 = CTR
        else if(chartOption == "CTR"){
        	var json_data = echartMakeData1(data, chartOption, '추천대비 클릭비율', '', '');
            var titleDate =['추천대비 클릭비율'];
            var p_color = ['#8b4f1d'];
            barOverlabChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'Y', ' %');
        }
        // 차트 옵션 = CR
        else if(chartOption == "CR"){
        	var json_data = echartMakeData1(data, chartOption, '추천대비 구매비율', '', '');
            var titleDate =['추천대비 구매비율'];
            var p_color = ['#ff8c0a'];
            barOverlabChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'Y', ' %');
        }
        // 차트 옵션 = OrderPrice
        else if(chartOption == "OrderPrice"){
        	var json_data = echartMakeData1(data, chartOption, '구매 총액', '추천 구매 총액', '');
            var titleDate =['구매 총액', '추천 구매 총액'];
            var p_color = ['#84799f','#fde6be'];
            barOverlabChart('graph-flot-points1', json_data, titleDate, '10', p_color, 'Y', ' 원');
        }else{
        	alert('서버와의 통신에 실패하였습니다.');
        } 	 
    }
    
    function chartOption(){
    	var option = $("#searchOptionNm option:selected" ).val();
    	var startDate = $('#datePickerId3').data('daterangepicker').startDate.format('YYYYMMDD');
    	
    	dateFunction3(startDate, option);
    }

</script>
		<div class="mConBox">
			<div class="mCont">
				<div class="row">
					<div class="col-md-6"><h2><i class="fa fa-file-text-o"></i> 전체 서비스별 추천실적</h2></div>
				  <div class="col-md-6 text-right dateSingle">
                    <div class="setBoxListArea mr10">
                    
                    <select id="searchOptionNm" onchange="chartOption()">
                    	<option value="RecoPV">접속 건수(RecoPV)</option>
                    	<option value="CTR" >추천대비클릭비율(RCR)</option>
                    	<option value="CR">추천대비구매비율(RBR)</option>
                    	<option value="OrderPrice">구매 금액(OrderPrice)</option>
                    </select>
                    </div>
					<i class="ti-angle-right" title="다음날짜" onclick="return nextDay();"></i>
					<label class="labelHidden" for="datePickerId3"></label>
					<input type="text" class="dateSingle" id="datePickerId3">
					<i class="ti-angle-left" title="이전날짜" onclick="return prevDay();"></i>
					<i class="ti-calendar"></i>
				  </div>
				</div>
				<div class="row mainTopBoxList dfSize mainLeftCon">
					<div class="col-md-12 mainBoxRes">
						<div class="recoBoxBMain bgBboxC1">
							<div class="iconBox"><i class="ti-mouse-alt"><p>RecoPV</p></i></div>
							<div class="recoBoxBList">
								<h2><ii id="yesterComparePv" class="moneyType"></ii> <span id="yesterComparePvPer">건 (0.0%)</span></h2>
								<table class="recoBoxBTbl">
									<tr>
										<th >· 어제 &nbsp;&nbsp;&nbsp;: </th>
										<td id="todayPv" class="moneyType"></td>
									</tr>
									<tr>
										<th>· 이틀전 : </th>
										<td id="yesterPv" class="moneyType"></td>
									</tr>
									<tr>
										<th>· 평균 &nbsp;&nbsp;&nbsp;: </th>
										<td id="avgPv" class="moneyType"></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-12 mainBoxRes">
						<div class="recoBoxBMain bgBboxC2">
							<div class="iconBox"><i class="ti-home"><p>RCR</p></i></div>
							<div class="recoBoxBList">
                                <h2><ii id="yesterCompareCtr">0.0%</ii> <span> %</span></h2>
                                <table class="recoBoxBTbl">
                                    <tr>
                                        <th>· 어제 &nbsp;&nbsp;&nbsp;: </th>
                                        <td id="todayCtr"></td>
                                    </tr>
                                    <tr>
                                        <th>· 이틀전 : </th>
                                        <td id="yesterCtr"></td>
                                    </tr>
                                    <tr>
                                        <th>· 평균 &nbsp;&nbsp;&nbsp;: </th>
                                        <td id="avgCtrObj"></td>
                                    </tr>
                                </table>
                            </div>
						</div>
					</div>
					<div class="col-md-12 mainBoxRes">
						<div class="recoBoxBMain bgBboxC3">
						    <div class="iconBox"><i class="ti-package"><p>RBR</p></i></div>

                            <div class="recoBoxBList">
								<h2><ii id="yesterCompareCr">0.0%</ii> <span> %</span></h2>
								<table class="recoBoxBTbl">
									<tr>
										<th>· 어제 &nbsp;&nbsp;&nbsp;: </th>
										<td id="todayCr"></td>
									</tr>
									<tr>
										<th>· 이틀전 : </th>
										<td id="yesterCr"></td>
									</tr>
									<tr>
										<th>· 평균 &nbsp;&nbsp;&nbsp;: </th>
										<td id="avgCrObj"></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-12 mainBoxRes">
						<div class="recoBoxBMain bgBboxC4">
						    <div class="iconBox"><i class="ti-gift"><p>Order Price</p></i></div>

						    <div class="recoBoxBList">
                                <h2><ii id="yesterComparePrice" class="moneyType">0</ii> <span id="yesterComparePricePer"> (0.0%)</span></h2>
                                <table class="recoBoxBTbl">
                                    <tr>
                                        <th>· 어제 &nbsp;&nbsp;&nbsp;: </th>
                                        <td id="todayPrice" class="moneyType"></td>
                                    </tr>
                                    <tr>
                                        <th>· 이틀전 : </th>
                                        <td id="yesterPrice" class="moneyType"></td>
                                    </tr>
                                    <tr>
                                        <th>· 평균 &nbsp;&nbsp;&nbsp;: </th>
                                        <td id="avgPrice" class="moneyType"></td>
                                    </tr>
                                </table>
                            </div>
						</div>
					</div>
				</div><!-- row -->
                <div class="row mainRightCon">
                    <div class="col-md-12">
                        <div class="xonBox">
                            <div class="row pd20">
                                <div class="col-md-12">
                                    <div class="gInBox" id="graph-flot-points1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>		
			</div>
		</div>
<div id="loading2" style="z-index:1002; display:none;">
    <div class="sk-cube-grid" style="z-index:1002">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
    <div class="sk-cube-txt" style="z-index:1002">초록마을</div>
</div>