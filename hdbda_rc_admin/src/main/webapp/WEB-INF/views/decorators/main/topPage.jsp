<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script type="text/javascript">
	$(document).ready(function(){
		if(opener){
			$('.adminArea').hide();		
		} else {
			$('.adminArea').show();
		}	
	});

    function fnLogOut(menuUrl){
        document.topForm.method = "POST";
        var strUrl = menuUrl.split("?");
        document.topForm.action =strUrl[0];
        document.topForm.submit();
    }
</script>
<div class="adminArea">
    <ul>
        <li class="member"><a href="#" onclick="return false;" id="mMenu"><b class="logInfo">${sessionScope.userInfo.userId}</b> 님 접속중</a> (최근 접속시간 : <span id="beforeLoginDt">${sessionScope.userLoginTime}</span>)</li>
        <li><a href="#" onClick="fnLogOut('/login/logout.do')"><i class="ti-power-off"></i> 로그아웃</a></li>
    </ul>
</div>


<form method="post" name="topForm"  action="/index/serviceDashboard.do">
			<input type="hidden" name="activeDomainKey" id="activeDomainKey">
			<input type="hidden" name="activeServiceKey">
			<input type="hidden" name="activeServiceTitle">
			<input type="hidden" name="activeMenuKey">
			<input type="hidden" name="leftMenuKey">
</form>


