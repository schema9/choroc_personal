<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script>

	function runLeftBodyTabMenu(menuUrl, domainKey, serviceKey, serviceNm, topMenuKey, leftMenu, bodyTabMenuKey, domainNm ){

		cursor_wait();

		document.bodyTabForm.activeDomainKey.value = domainKey;
		document.bodyTabForm.activeServiceKey.value = serviceKey;
		document.bodyTabForm.activeServiceTitle.value = serviceNm;
		document.bodyTabForm.activeMenuKey.value = topMenuKey;
		document.bodyTabForm.leftMenuKey.value = leftMenu;
		document.bodyTabForm.bodyTabMenuKey.value = bodyTabMenuKey;

		document.bodyTabForm.activeDomainTitle.value = domainNm;

		document.bodyTabForm.method = "POST";
		var strUrl = menuUrl.split("?");
		document.bodyTabForm.action =strUrl[0];
		document.bodyTabForm.submit();

		cursor_clear();
	}
</script>

<div class="subTopMenu">
    <form method="post" name="bodyTabForm" >
        <input type="hidden" name="activeDomainKey">
        <input type="hidden" name="activeServiceKey">
        <input type="hidden" name="activeServiceTitle">
        <input type="hidden" name="activeMenuKey">
        <input type="hidden" name="leftMenuKey">
        <input type="hidden" name="bodyTabMenuKey">
        <input type="hidden" name="activeDomainTitle">

        <ul style="display:none" id="bodyTobUl">
            <c:set var="tabIdx" value="0"/>
            <c:forEach var="menu" items="${myLeftMenus}" varStatus="status">
                 <c:forEach var="depth2" items="${menu.depth2}" varStatus="status" >
                        <!--
                       ### ${status.index} ### ${depth2.menuTitle} ### ${depth2.menuId}### ${leftMenuKey}###
                        -->
                     <c:if test="${depth2.parentId == leftMenuKey}">
                        <script>
                            $("#bodyTobUl").show()
                        </script>

                        <c:set var="menuUrl" value="${depth2.menuUrl}?activeDomainKey=${activeDomainKey}&activeServiceKey=${activeServiceKey}&activeMenuKey=${depth2.parentId}&leftMenuKey=${leftMenuKey}&bodyTabMenuKey=${depth2.menuId}"/>

                        <!-- tab menu 선택된 메뉴일 경우 -->
                        <c:if test="${bodyTabMenuKey == depth2.menuId}">
                            1<li class="active"><a href="#"onClick="runLeftBodyTabMenu('${menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${activeMenuKey}', '${leftMenuKey}', ${depth2.menuId}, '${activeDomainTitle}')">${depth2.menuTitle}</a></li>
                        </c:if>

                        <!-- tab menu 선택된 메뉴가 아닐 경우 -->
                        <c:if test="${bodyTabMenuKey != depth2.menuId && bodyTabMenuKey != ''}">
                            <li><a href="#"onClick="runLeftBodyTabMenu('${menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${activeMenuKey}', '${leftMenuKey}', ${depth2.menuId}, '${activeDomainTitle}')">${depth2.menuTitle}</a></li>
                        </c:if>

                        <!-- left menu 에서 들어와서 tab menu 가 선택되지 않은 상태 일때  -->
                        <c:if test="${bodyTabMenuKey == ''}">

                            <!-- tab menu 가 선택되지 않은 상태 일때 index 0, 첫번째 tab menu를 선택  -->
                            <c:if test="${tabIdx == 0}">
                                <li class="active"><a href="#"onClick="runLeftBodyTabMenu('${menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${activeMenuKey}', '${leftMenuKey}', ${depth2.menuId}, '${activeDomainTitle}')">${depth2.menuTitle}</a></li>
                            </c:if>

                            <!-- tab menu 가 선택되지 않은 상태 일때 index 0 이 아닌, 첫번째가 아닌경우  모두 선택 안된것으로 처리  -->
                            <c:if test="${tabIdx != 0}">
                                <li><a href="#"onClick="runLeftBodyTabMenu('${menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${activeMenuKey}', '${leftMenuKey}', ${depth2.menuId}, '${activeDomainTitle}')">${depth2.menuTitle}</a></li>
                            </c:if>
                            <c:set var="tabIdx" value="${tabIdx + 1}"/>
                        </c:if>
                    </c:if>
                </c:forEach>
            </c:forEach>
       </ul>
    </form>
</div>


