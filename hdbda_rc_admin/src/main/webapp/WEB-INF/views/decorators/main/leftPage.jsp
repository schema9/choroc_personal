<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<script>
	var htmlSet ='';
	var list = new Array();

    $(window).ready(function(){
        var html = $("#nav ul").html();
        $("#viewAllMenu ul").empty().append(html);		
		if (subNaviMenu.dept1.menuId == undefined) {
			$("#sub_"+subNaviMenu.dept2.menuId).removeClass().addClass("subMenu onMenu");
			$("#subTab_"+subNaviMenu.dept3.menuId).addClass("active");

		} else {
			$("#sub_"+subNaviMenu.dept1.menuId).removeClass().addClass("subMenu onMenu");
			$("#subTab_"+subNaviMenu.dept2.menuId).addClass("active");
		}
		if (subNaviMenu.dept2.menuId == undefined){
			if($(location).attr('href').match("jobDetail.do")) {
				$("#sub_10").removeClass().addClass("subMenu onMenu");
				$("#subTab_13").addClass("active");	
			}
		}
    });
	

	function runLeftMenu(menuUrl, domainKey, serviceKey, serviceNm, topMenuKey, leftMenu, domainNm){

		cursor_wait();
		
		document.leftForm.activeDomainKey.value = domainKey;
		document.leftForm.activeServiceKey.value = serviceKey;
		document.leftForm.activeServiceTitle.value = serviceNm;
		document.leftForm.activeMenuKey.value = topMenuKey;
		document.leftForm.leftMenuKey.value = leftMenu;

		document.leftForm.activeDomainTitle.value = domainNm;

		document.leftForm.method = "POST";
		var strUrl = menuUrl.split("?");
		document.leftForm.action =strUrl[0];
		document.leftForm.submit();
		
		cursor_clear();
	}
	function showPopup(menuUrl) {

        var winHeight = document.body.clientHeight;	// 현재창의 높이
        var winWidth = document.body.clientWidth;	// 현재창의 너비
        var winX = window.screenX || window.screenLeft || 0;// 현재창의 x좌표
        var winY = window.screenY || window.screenTop || 0;	// 현재창의 y좌표

        var popX = winX + (winWidth - 1000)/2;
        var popY = winY + (winHeight - 700)/2;

        window.open(menuUrl, "a", "width=1000, height=700, left="+popX+", top="+popY);
     }
	
</script>

<div class="logoTop">
    <div class="logo">
        <c:if test="${sessionScope.userInfo.roleId == 1}">
            <a href="/index/adminDashboard.do" id="logo" >
<!--             	<img src="/img/logo_hmall.png"  alt="초록마을" class="logoImg"/> -->
				초록마을
            	<span>관리자</span>
            </a>
        </c:if>
         <c:if test="${sessionScope.userInfo.roleId == 2}">
                    <a href="/index/adminDashboard.do" id="logo" >
<!--                     	<img src="/img/logo_hmall.png"  alt="초록마을" class="logoImg"/> -->
						초록마을
            			<span>관리자</span>
                    </a>
                </c:if>
        <c:if test="${sessionScope.userInfo.roleId != 1 && sessionScope.userInfo.roleId != 2}">
            <c:choose>
                <c:when test="${currentDomain != null && currentDomain != '' && currentServiceId != null && currentServiceId != ''}">
                    <a href="javascript:runTopServiceLink('${currentDomain}','${currentServiceId}','${activeServiceTitle}')" id="logo" >
                    초록마을
            		<span>관리자</span>
                    </a>
                </c:when>
                <c:otherwise>
                    <a href="javascript:runEmptyTopServiceLink('${currentDomain}')" id="logo">
<!--                     	<img src="/img/logo_hmall.png"  alt="초록마을" class="logoImg"/> -->
					초록마을
	            	<span>관리자</span>
                    </a>
                </c:otherwise>
            </c:choose>
        </c:if>
    </div>
    <div class="topBtn" id="topBtn"><i class="ti-menu"></i></div>
</div>

<div class="nav" id="nav">

    <form method="post" name="leftForm" >
        <input type="hidden" name="activeDomainKey">
        <input type="hidden" name="activeServiceKey">
        <input type="hidden" name="activeServiceTitle">
        <input type="hidden" name="activeMenuKey">
        <input type="hidden" name="leftMenuKey">
        <input type="hidden" name="activeDomainTitle">
        
		<h2>제품추천</h2>
        <ul id="menuDepth1">
            <c:choose>
                <c:when test="true">                     			
                    <c:set var="idx" value="0"/>
                    <c:forEach var="menu" items="${myTopMenus}">
                        <c:choose>
                        	<c:when test="true">  					
                        		<c:if test="true">    
                        		    <c:choose>
                                        <c:when test="true">  
                                            <c:choose>
                                               <c:when test="${menu.displayLevel == 1}">
                                                   <c:if test="${idx != 0}">
                                                       </ul>
                                                           </li>
                                                   </c:if>
                                                   <c:set var="idx" value="${idx + 1}"/>
                                                   <li>
                                                      <a href='#' onclick='return false;'>
                                                           <i class='${menu.cssClass}'></i>
                                                           ${menu.menuTitle}
                                                       </a>                                                   
                                                       <!-- 메뉴 활성화 subMenu 에 onMenu 클래스 추가할 경우 활성화됨-->
                                                       <i class='ti-angle-down'></i>
                                                       <ul class='subMenu' id='sub_${menu.menuId}' >
                                                       <!-- 메뉴 활성화 -->                                                       
                                               </c:when>
                                               <c:when test="${menu.displayLevel == 2}">
                                                    <c:if test="${menu.menuId == leftMenuKey}">
                                                        <c:choose>
                                                            <c:when test="${menu.menuId == 46}">
                                                                <li class='active'><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <li class='active'><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:if>
                                                     <!-- 실제 일반 3댑스 메뉴가 뿌려지는 위치 -->
                                                    <c:if test="${menu.menuId != leftMenuKey}">
                                                        <c:choose>
                                                            <c:when test="${menu.menuId == 46}">
                                                                <li><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                            </c:when>
 															<c:when test="${menu.menuId == 9}">
                                                              <li id='subTab_${menu.menuId}'><a href='#' id='${menu.parentId}' onClick='window.open("/apiGuid/MYG_API_001.html","apiGuid")' >${menu.menuTitle}</a></li>
                                                           </c:when>
                                                            <c:otherwise>
                                                                <li id='subTab_${menu.menuId}'><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:if>
                                               </c:when>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                               <c:choose>
                                                    <c:when test="${menu.displayLevel == 1}">
                                                        <c:if test="${idx != 0}">
                                                                </ul>
                                                            </li>
                                                        </c:if>
                                                        <c:set var="idx" value="${idx + 1}"/>
                                                    <li>
                                                        <a href='#' id='${menu.menuId}' onclick='return false;'>
                                                            <i class='${menu.cssClass}'></i>
                                                            ${menu.menuTitle}#
                                                        </a>
                                                        <i class='ti-angle-down'></i>
                                                        <ul class='subMenu'>
                                                    </c:when>
                                                    <c:when test="${menu.displayLevel == 2}">
                                                        <c:if test="${menu.menuId == leftMenuKey}">
                                                            <c:choose>
                                                                <c:when test="${menu.menuId == 46}">
                                                                    <li class='active'><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <li class='active'><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:if>
                                                        <c:if test="${menu.menuId != leftMenuKey}">
                                                            <c:choose>
                                                                <c:when test="${menu.menuId == 46}">
                                                                    <li><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <li><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:if>
                                                    </c:when>
                                                </c:choose>
                                        </c:otherwise>
                                    </c:choose>

                        		</c:if>
                        	</c:when>

                        	<c:otherwise>
                                <c:choose>
                                    <c:when test="${menu.menuId == activeMenuKey}">
                                        <c:choose>
                                           <c:when test="${menu.displayLevel == 1}">
                                               <c:if test="${idx != 0}">
                                                   </ul>
                                                       </li>
                                               </c:if>

                                               <c:set var="idx" value="${idx + 1}"/>

                                               <li class='active'>
                                                  <a href='#' id='${menu.menuId}' onclick='return false;'>
                                                       <i class='${menu.cssClass}'></i>
                                                       ${menu.menuTitle}@
                                                   </a>
                                                   <i class='ti-angle-up'></i>
                                                   <ul class='subMenu onMenu' id="menuDepth2">
                                           </c:when>
                                           <c:when test="${menu.displayLevel == 2}">
                                               <c:if test="${menu.menuId == leftMenuKey}">
                                                    <c:choose>
                                                       <c:when test="${menu.menuId == 46}">
                                                           <li class='active'><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                       </c:when>
                                                       <c:otherwise>
                                                           <li class='active'><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                       </c:otherwise>
                                                    </c:choose>
                                               </c:if>
                                               <c:if test="${menu.menuId != leftMenuKey}">
                                                    <c:choose>
                                                       <c:when test="${menu.menuId == 46}">
                                                          <li><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                       </c:when>
                                                       <c:otherwise>
                                                          <li><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                       </c:otherwise>
                                                    </c:choose>
                                               </c:if>
                                           </c:when>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                           <c:choose>
                                                <c:when test="${menu.displayLevel == 1}">
                                                    <c:if test="${idx != 0}">
                                                            </ul>
                                                        </li>
                                                    </c:if>
                                                    <c:set var="idx" value="${idx + 1}"/>
                                                <li>
                                                    <a href='#' id='${menu.menuId}' onclick='return false;'>
                                                        <i class='${menu.cssClass}'></i>
                                                        ${menu.menuTitle}#
                                                    </a>
                                                    <i class='ti-angle-down'></i>
                                                    <ul class='subMenu'>
                                                </c:when>
                                                <c:when test="${menu.displayLevel == 2}">
                                                    <c:if test="${menu.menuId == leftMenuKey}">
                                                        <c:choose>
                                                            <c:when test="${menu.menuId == 46}">
                                                              <li class='active'><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                            </c:when>
                                                            <c:otherwise>
                                                              <li class='active'><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:if>
                                                    <c:if test="${menu.menuId != leftMenuKey}">
                                                        <c:choose>
                                                            <c:when test="${menu.menuId == 46}">
                                                                <li><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <li><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:if>
                                                </c:when>
                                            </c:choose>
                                    </c:otherwise>
                                </c:choose>
                        	</c:otherwise>
                        </c:choose>

                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <c:set var="idx" value="0"/>
                    <c:forEach var="menu" items="${myTopMenus}">
                       <!--
                        ### ${menu.menuId} ### ${activeMenuKey} ### BT:  ${bodyTabMenuKey} /////${entryPoint}///
                       -->

                        <c:choose>
                            <c:when test="${sessionScope.userInfo.roleId == 1 && ( entryPoint != null && entryPoint != '' )}">
                                <c:if test="${(menu.menuId == '40' || menu.parentId == '40')}">
                                    <c:choose>
                                        <c:when test="${menu.menuId == activeMenuKey}">
                                            <c:choose>
                                               <c:when test="${menu.displayLevel == 1}">
                                                   <c:if test="${idx != 0}">
                                                       </ul>
                                                           </li>
                                                   </c:if>

                                                   <c:set var="idx" value="${idx + 1}"/>

                                                   <li class='active'>
                                                      <a href='#' id='${menu.menuId}' onclick='return false;'>
                                                           <i class='${menu.cssClass}'></i>
                                                          ${menu.menuTitle}
                                                       </a>
                                                       <i class='ti-angle-up'></i>
                                                       <ul class='subMenu onMenu' id="menuDepth2">
                                               </c:when>
                                               <c:when test="${menu.displayLevel == 2}">
                                                   <c:if test="${menu.menuId == leftMenuKey}">
                                                       <c:choose>
                                                           <c:when test="${menu.menuId == 46}">
                                                               <li class='active'><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                           </c:when>
                                                           <c:otherwise>
                                                               <li class='active'><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}' , '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                           </c:otherwise>
                                                       </c:choose>
                                                   </c:if>
                                                   <c:if test="${menu.menuId != leftMenuKey}">
                                                        <c:choose>
                                                           <c:when test="${menu.menuId == 46}">
                                                              <li><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                           </c:when>
                                                           <c:otherwise>
                                                              <li><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}' , '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                           </c:otherwise>
                                                        </c:choose>
                                                   </c:if>
                                               </c:when>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                               <c:choose>
                                                    <c:when test="${menu.displayLevel == 1}">
                                                        <c:if test="${idx != 0}">
                                                                </ul>
                                                            </li>
                                                        </c:if>
                                                        <c:set var="idx" value="${idx + 1}"/>
                                                    <li>
                                                        <a href='#' id='${menu.menuId}' onclick='return false;'>
                                                            <i class='${menu.cssClass}'></i>
                                                            ${menu.menuTitle}
                                                        </a>
                                                        <i class='ti-angle-down'></i>
                                                        <ul class='subMenu'>
                                                    </c:when>
                                                    <c:when test="${menu.displayLevel == 2}">
                                                        <c:if test="${menu.menuId == leftMenuKey}">
                                                            <c:choose>
                                                                <c:when test="${menu.menuId == 46}">
                                                                  <li class='active'><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                                </c:when>
                                                                <c:otherwise>
                                                                  <li class='active'><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:if>
                                                        <c:if test="${menu.menuId != leftMenuKey}">
                                                            <c:choose>
                                                                <c:when test="${menu.menuId == 46}">
                                                                  <li><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                                </c:when>
                                                                <c:otherwise>
                                                                  <li><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:if>
                                                    </c:when>
                                                </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </c:when>

                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${menu.menuId == activeMenuKey}">
                                        <c:choose>
                                           <c:when test="${menu.displayLevel == 1}">
                                               <c:if test="${idx != 0}">
                                                   </ul>
                                                       </li>
                                               </c:if>

                                               <c:set var="idx" value="${idx + 1}"/>

                                               <li class='active>
                                                  <a href='#' id='${menu.menuId}' onclick='return false;'>
                                                       <i class='${menu.cssClass}'></i>
                                                      ${menu.menuTitle}
                                                   </a>
                                                   <i class='ti-angle-up'></i>
                                                   <ul class='subMenu onMenu' id="menuDepth2">
                                           </c:when>
                                           <c:when test="${menu.displayLevel == 2}">
                                               <c:if test="${menu.menuId == leftMenuKey}">
                                                   <c:choose>
                                                       <c:when test="${menu.menuId == 46}">
                                                         <li class='active'><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                       </c:when>
                                                       <c:otherwise>
                                                         <li class='active'><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}' , '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                       </c:otherwise>
                                                   </c:choose>
                                               </c:if>
                                               <c:if test="${menu.menuId != leftMenuKey}">
                                                    <c:choose>
                                                       <c:when test="${menu.menuId == 46}">
                                                        <li><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                       </c:when>
                                                       <c:otherwise>
                                                        <li><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}' , '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                       </c:otherwise>
                                                    </c:choose>
                                               </c:if>
                                           </c:when>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                           <c:choose>
                                                <c:when test="${menu.displayLevel == 1}">
                                                    <c:if test="${idx != 0}">
                                                            </ul>
                                                        </li>
                                                    </c:if>
                                                    <c:set var="idx" value="${idx + 1}"/>
                                                <li>
                                                    <a href='#' id='${menu.menuId}' onclick='return false;'>
                                                        <i class='${menu.cssClass}'></i>
                                                        ${menu.menuTitle}
                                                    </a>
                                                    <i class='ti-angle-down'></i>
                                                    <ul class='subMenu'>
                                                </c:when>
                                                <c:when test="${menu.displayLevel == 2}">
                                                    <c:if test="${menu.menuId == leftMenuKey}">
                                                        <c:choose>
                                                            <c:when test="${menu.menuId == 46}">
                                                                <li class='active'><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <li class='active'><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:if>
                                                    <c:if test="${menu.menuId != leftMenuKey}">
                                                        <c:choose>
                                                            <c:when test="${menu.menuId == 46}">
                                                                <li><a href='#' id='${menu.parentId}' onClick="showPopup('${menu.menuUrl}')" >${menu.menuTitle}</a></li>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <li><a href='#' id='${menu.parentId}' onClick="runLeftMenu('${menu.menuUrl}', '${activeDomainKey}', '${activeServiceKey}', '${activeServiceTitle}', '${menu.parentId}', '${menu.menuId}', '${activeDomainTitle}')" >${menu.menuTitle}</a></li>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:if>
                                                </c:when>
                                            </c:choose>
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>


                    </c:forEach>
                </c:otherwise>
            </c:choose>
        <c:choose>
        <c:when test="${activeServiceKey == 'ALL'}">
        </c:when>
        <c:otherwise>
              </ul>
            </li>
          <li class='allMenu' style="display:none;"><a href='#' onclick='return false;'><i class='ti-view-grid'></i> 전체 펄치기</a> <i class='ti-angle-right'></i></li>
        </c:otherwise>
        </c:choose>
       </ul>
    </form>
<!--메뉴-->
</div>


