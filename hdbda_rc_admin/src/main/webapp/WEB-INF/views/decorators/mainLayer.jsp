<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="../common/common.jsp"%>

<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>초록마을 제품추천</title>

    <!-- Bootstrap -->
    <link href="/css/dateArea.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-theme.css" rel="stylesheet">
    <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/daterangepicker.css" rel="stylesheet">
    <link href="/fonts/font-awesome.css" rel="stylesheet">
    <link href="/fonts/font-awesome.min.css" rel="stylesheet">
    <link href="/css/offset.css" rel="stylesheet">
    <link href="/css/popup.css" rel="stylesheet">
    <link href="/css/res.css" rel="stylesheet">
    <link href="/css/res_algo.css" rel="stylesheet">
    <link href="/css/spectrum.css" rel="stylesheet">
    <link href="/css/themify-icons.css" rel="stylesheet">
	
<!--     <link rel="shortcut icon" href="/img/favicon.ico"> -->



    <script type="text/javascript" src="/js/jquery-1.12.4.js"></script>
	<!-- 2017-05-12 admin 관련 js 제거
	<script type="text/javascript" src="/js/adm_doma.js"></script>
	-->
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/custom.js"></script>
    <script type="text/javascript" src="/js/moment.js"></script>
    <script type="text/javascript" src="/js/daterangepicker.js"></script>
    <script type="text/javascript" src="/js/daterangepicker_unlimit.js"></script>

	<script type="text/javascript" src="/js/pageSet.js"></script>
	<script type="text/javascript" src="/js/hashMap.js"></script>
	<script type="text/javascript" src="/js/spectrum.js"></script>

    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/util_ajax.js"></script>
    <script type="text/javascript" src="/js/util_date.js"></script>
    <script type="text/javascript" src="/js/util_prototype.js"></script>
    <script type="text/javascript" src="/js/util_validate.js"></script>

    <!-- chart js -->
    <script type="text/javascript" src="/js/echarts/echarts.js"></script>
    <script type="text/javascript" src="/js/charts.js"></script>


</head>
<script type="text/javascript">
	var _serviceId = '<%=serviceId%>';
	var _serviceNm = '<%=serviceNm%>';
	var _domain = '<%=domain%>';
	var _accessKey = 'STT_' + '<%=accessKey%>';
	var _rcmAccessKey = 'RCM_' + '<%=accessKey%>';
	var _roleId = '<%=roleId%>';
	var _contextPath = '<%=request.getContextPath()%>';
	var _apiServer = getApiServer() ;
	var _apiRecServer = getRecApiServer();
	var _beforeLoginDt = '<%=beforeLoginDt%>';
	var htmlSet ='';
	var list = new Array();
	var menuViewList = new Array();
	var subNaviMenu = {};
	subNaviMenu.dept1 = {};
	subNaviMenu.dept2 = {};
	subNaviMenu.dept3 = {};
	
	function getsubMenu(){
 		var listObj = {}
 		var menuViewObj = {}
		var isMultMenu = false;
 		<c:forEach items="${myTopMenus}" var="item">
 		listObj = {
 				'menuNm':'${item.menuNm}'
 				,'menuId':'${item.menuId}'
 				,'upperMenuId':'${item.upperMenuId}'
 				,'menuUrl':'${item.menuUrl}'
 				,'displayLevel':'${item.displayLevel}'
 				,'sortOrder':'${item.sortOrder}'
 		}
 		list.push(listObj);
 		</c:forEach>

		for (var i=0;i<list.length;i++){
			val = list[i].menuUrl;
			if (list[i].menuUrl != "/") {
				if ($(location).attr('href').match(val)) {
 					$("#3depthMenu").empty();
					menuViewObj={
								'menuUrl':list[i].menuUrl,
								'menuNm':list[i].menuNm,
								'menuActive':'on',
								'menuId':list[i].menuId,
						 		'upperMenuId':list[i].upperMenuId,
						 		'sortOrder':list[i].sortOrder
					};
					menuViewList.push(menuViewObj);
				
					if (list[i].displayLevel=='3'){
						for (var ii=0;ii<list.length;ii++){
 							val2 = list[ii].menuId;
 							if (val2 == list[i].upperMenuId) {			
//  									menuViewObj={
//  											'menuUrl':list[ii].menuUrl,
//  											'menuNm':list[ii].menuNm,
//  											'menuActive':'',
//  											'menuId':list[ii].menuId,
//  										 	'upperMenuId':list[ii].upperMenuId,
//												'sortOrder':list[ii].sortOrder
//  									};
//  									menuViewList.push(menuViewObj);
 									for (var iii=0;iii<list.length;iii++){
 										val3 = list[ii].menuId;
 										if (list[i].menuId != list[iii].menuId) {
 											if (val3 == list[iii].upperMenuId) {
 	 		 									menuViewObj={
 	 		 											'menuUrl':list[iii].menuUrl,
 	 		 											'menuNm':list[iii].menuNm,
 	 		 											'menuActive':'',
 	 		 											'menuId':list[iii].menuId,
 	 		 									 		'upperMenuId':list[iii].upperMenuId,
 	 		 									 		'sortOrder':list[iii].sortOrder
 	 		 									};
 	 		 									
 	 		 									if (menuViewObj.menuUrl != "/") {
 	 		 										if ($(location).attr('href').match(menuViewObj.menuUrl)) {
 	 		 											menuViewObj.menuActive='on';
 	 		 											menuViewList.shift();
 	 		 										}
 	 		 									}
 	 		 									isMultMenu = true;
 	 		 									menuViewList.push(menuViewObj);
//  	 		 									if (isMultMenu == true) {
// 	 		 					 					menuViewObj={
// 	 		 		 								'menuUrl':list[i].menuUrl,
// 	 		 		 								'menuNm':list[i].menuNm,
// 	 		 		 								'menuActive':'on',
// 	 		 		 								'menuId':list[i].menuId,
// 	 		 		 						 		'upperMenuId':list[i].upperMenuId,
//													'sortOrder':list[i].sortOrder
// 	 		 		 								};
// 	 		 		 								menuViewList.push(menuViewObj);
// 	 		 		 								isMultMenu= false;
//  	 		 									}
 	 										}
 										}
 									}
 							}
 							
 						}	
 					} else {
 						for (var ii=0;ii<list.length;ii++){
 							val2 = list[ii].upperMenuId;
 							if (val2 == list[i].menuId) {
 								isMultMenu = true;
 								menuViewObj={
											'menuUrl':list[ii].menuUrl,
											'menuNm':list[ii].menuNm,
											'menuActive':'',
											'menuId':list[ii].menuId,
										 	'upperMenuId':list[ii].upperMenuId,
										 	'sortOrder':list[ii].sortOrder
								};
 								
 								if (menuViewObj.menuUrl != "/") {
										if ($(location).attr('href').match(menuViewObj.menuUrl)) {
											menuViewObj.menuActive='on';
											menuViewList.shift();
										}
								}
 								menuViewList.push(menuViewObj);
 							}
 						}
 						
 					}
					menuViewOrder("desc");
					if (isMultMenu==true) {
						createSubNaviMenu();
					} else {
						$("#3depthMenu").empty();
						createSubNaviMenu();
					}
					return;
				}
			}
		}
		menuViewOrder("asc");
		createSubNaviMenu();
	}
	
	function createSubNaviMenu(){
		//3댑스
		for(var i=0;i<menuViewList.length;i++){
			if (menuViewList[i].menuActive == "on") {
				subNaviMenu.dept3 = {
						'menuUrl':menuViewList[i].menuUrl,
						'menuNm':menuViewList[i].menuNm,
						'menuId':menuViewList[i].menuId,
				 		'upperMenuId':menuViewList[i].upperMenuId,
				 		'sortOrder':menuViewList[i].sortOrder
				}
			}
		}
		
		//2댑스
		for(var i=0;i<list.length;i++){
			if (list[i].menuId == subNaviMenu.dept3.upperMenuId) {
				subNaviMenu.dept2 = {
						'menuUrl':list[i].menuUrl,
						'menuNm':list[i].menuNm,
						'menuId':list[i].menuId,
				 		'upperMenuId':list[i].upperMenuId,
				 		'sortOrder':list[i].sortOrder
				}	
			}
		}
		
		//1댑스
		for(var i=0;i<list.length;i++){
			if (list[i].menuId == subNaviMenu.dept2.upperMenuId) {
				subNaviMenu.dept1 = {
						'menuUrl':list[i].menuUrl,
						'menuNm':list[i].menuNm,
						'menuId':list[i].menuId,
				 		'upperMenuId':list[i].upperMenuId,
				 		'sortOrder':list[i].sortOrder
				}	
			}
		}
	}

	function menuViewOrder(val) {
		 	htmlSet = "";
			if (val == "asc") {
				menuViewList.sort(function(a,b){
					return(a.sortOrder > b.sortOrder) ? - 1 : (a.sortOrder < b.sortOrder) ? 1 : 0;
				});
			} else {
				//메뉴 오름차순 정렬
				menuViewList.sort(function(a,b){
					return(a.sortOrder < b.sortOrder) ? - 1 : (a.sortOrder > b.sortOrder) ? 1 : 0;
				});
			}
			htmlSet += ' <div class="subTopMenu" >';
			if(0 != menuViewList.length) {
				htmlSet += ' <ul>';
				for(var i=0;i<menuViewList.length;i++){
					if (menuViewList[i].menuActive == "on") {
						htmlSet += ' <li  class="active"><a href="'+menuViewList[i].menuUrl+'">'+menuViewList[i].menuNm+'</a></li> ';
					} else {
						htmlSet += ' <li><a href="'+menuViewList[i].menuUrl+'">'+menuViewList[i].menuNm+'</a></li> ';
					}
				}
				htmlSet += ' </ul>';
			}
			htmlSet += ' </div>';
			$("#3depthMenu").append(htmlSet);
	}
	
	function multiApiSet(){
		var api_url, api_protocol = location.protocol;
		var api_chkUrl = window.location.href;
		if('' != api_chkUrl) {
			if('http:' == api_protocol) {
				return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
			} else {
				return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
			}
		} else {
			return false;
		}
	}
	
	function getApiServer() {
		var apiServerLine = multiApiSet();
		var apiServers = apiServerLine.split(",");

		return apiServers[Math.floor( (Math.random() * apiServers.length) )];
	}

    function getRecApiServer() {
        var apiServerLine = multiApiSet();
        var apiServers = apiServerLine.split(",");

        return apiServers[Math.floor( (Math.random() * apiServers.length) )];
    }

	// set cursor to the waiting pointer
	function cursor_wait() {
		document.body.style.cursor = 'progress';
		//$('body').css('cursor','progress');

		//alert("Cursor waiting...");
	}

	// Returns the cursor to the default pointer
	function cursor_clear() {
		//alert("going Cursor default...");
		//$('body').css('cursor','default');
		document.body.style.cursor = 'default';
	}

	function rejectRightButtonClick() {
		if (event.button==2) {
			alert('죄송합니다. 마우스 오른쪽 버튼은 사용하실 수 없습니다.');
		}
	}
	//document.onmousedown=rejectRightButtonClick;


    $(window).ready(function(){
        var html ="";
		var bodyTitleA = "";
        getsubMenu();
        
        html += '<ul>';
        if (subNaviMenu.dept1.menuId != undefined) {
//  			html += '<li>&nbsp;<i class="ti-angle-right"></i>&nbsp;</li>';
            html += '<li>' + subNaviMenu.dept1.menuNm + '</li>';
        }
 		if (subNaviMenu.dept2.menuId != undefined) {
 			 if (subNaviMenu.dept1.menuId != undefined) {
 				html += '<li>&nbsp;<i class="ti-angle-right"></i>&nbsp;</li>';
 			 }
            html += '<li>' + subNaviMenu.dept2.menuNm + '</li>';
        }
 		if (subNaviMenu.dept3.menuId != undefined) {
 			 if (subNaviMenu.dept2.menuId != undefined) {
 				html += '<li>&nbsp;<i class="ti-angle-right"></i>&nbsp;</li>';
  			 }	
            html += '<li>' + subNaviMenu.dept3.menuNm + '</li>';
 		}
 		html += '</ul>';
//  		bodyTitleA = "";
//  		bodyTitleA += '<h2><i class="ti-layers">'+subNaviMenu.dept3.menuNm+'</i>';
//         bodyTitleA += $("#bodyTobUl > .active").text();
//         bodyTitleA += '</h2>';
//         $("#subtitleA").empty().append(bodyTitleA);
 		$('.pageRoute').empty().append(html);
 		$('#subtitleA').empty().append('<h2><i class="ti-layers"></i>'+subNaviMenu.dept3.menuNm+'</h2>');
    });

</script>
    <body>
    <div id="loading" >
		<div class="sk-cube-grid">
			<div class="sk-cube sk-cube1"></div>
			<div class="sk-cube sk-cube2"></div>
			<div class="sk-cube sk-cube3"></div>
			<div class="sk-cube sk-cube4"></div>
			<div class="sk-cube sk-cube5"></div>
			<div class="sk-cube sk-cube6"></div>
			<div class="sk-cube sk-cube7"></div>
			<div class="sk-cube sk-cube8"></div>
			<div class="sk-cube sk-cube9"></div>
		</div>
		<div class="sk-cube-txt">초록마을</div>  
    </div>
    <div id="loadingtest" style="z-index:1002; display:none;">

        <div class="sk-cube-grid" style="z-index:1002">
            <div class="sk-cube sk-cube1"></div>
            <div class="sk-cube sk-cube2"></div>
            <div class="sk-cube sk-cube3"></div>
            <div class="sk-cube sk-cube4"></div>
            <div class="sk-cube sk-cube5"></div>
            <div class="sk-cube sk-cube6"></div>
            <div class="sk-cube sk-cube7"></div>
            <div class="sk-cube sk-cube8"></div>
            <div class="sk-cube sk-cube9"></div>
        </div>

        <div class="sk-cube-txt" style="z-index:1002">초록마을</div>
    </div>

    <div id="lecoWrap">

		<div class="left-col">
           <page:applyDecorator name="_main_left" />
        </div>

        <!--전체메뉴-->
        <div class="viewAllMenu" id="viewAllMenu">
            <div class="allMenuInner" id="allMenuInner"><ul></ul></div>
        </div>

        <div class="lecoTop">
            <!-- START:TOP Area -->
            <page:applyDecorator name="_top" />
            <!-- END:TOP Area -->
        </div>

        <div class="right-col">
            <div class="mConBox">
            <span id="3depthMenu"></span>
                     <page:applyDecorator name="_body_tab" />
                <!-- END:TOP Area -->

                <!-- START:MAIN Area -->
                <decorator:body />
                <!-- END:MAIN Area -->

            </div>
        </div>
	</div>
	<div id="mask"></div>
	<div id="mask2"></div>

	<div class="md-overlay"></div><!-- the overlay element -->

</body>
</html>
