<%------------------------------------------------------------------------
- Menu  : 시스템 관리 > 배치 관리
- ID    : SYS_BATC_001
- Ref.  : -
- URI   : -
- History
- 2017-04-24 created by ih
------------------------------------------------------------------------%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<style>
.fingerPointer{
    cursor:pointer;
}
</style>

<script type="text/javascript">

var controller = '/system/jobParam';

$(window).ready(function(){
    pageEvents();
});

function pageEvents(){

    // close & cancel buttons
    $('#btnM-cancel, #btnA-cancel, .closePop i').unbind('click').bind('click',function(){
        fn_util_divPopUpControl($(this).parent().parent().parent().attr('id'),false);
        initFormValues();
    });

    // add button
    $('#btnPopUpAdd').unbind('click').bind('click',function(){
        fn_util_divPopUpControl('divPopUpA-JobParam',true);
    });

    // modify button
    $('.btnModifyRow').unbind('click').bind('click',function(){
        fn_util_divPopUpControl('divPopUpM-JobParam',true);

        var jobID = $(this).parent().parent().attr('id');
        var url = controller + '/jobParamSelectRow.do';
        var param = {'jobID' : jobID};

        fn_util_ajax_basic(param, url, fn_setJobParamRow, fn_util_ajax_onError_alert);
    });

    // In Modify form - save & delete buttons
    $('#btnM-delete, #btnM-update').unbind('click').bind('click',function(){
        fn_modifyJobParam( $(this).attr('id') );
    });

    // In Add form - save button
    $('#btnA-insert').unbind('click').bind('click',function(){
        fn_addJobParam();
    });

    // In Add form - duplication id check button
    $('#btnA-checkDuplication').unbind('click').bind('click',function(){
        fn_isValidate_jobID('popUpA-jobID','popUpA-isDuplicatedID-h',true);
    });

    $('#popUpA-jobID').unbind('change').bind('change',function(){
        $('#popUpA-isDuplicatedID-h').val('Y');
    });

    // Event for click SEARCH button
    $('#btnSearch').unbind('click').bind('click',function(){
        // 2017-06-19;  shIH; 전체 검색 시 검색 목록 내의 모든 항목으로 LIKE 검색 [권한관리,시스템관리,My정보];
        /*
        if( fn_util_isValid_optionTextSearch('optSearch','inpSearch') ){
            searchJobParam();
        }
        */
        searchJobParam();
    });

    // 2017-06-19;  shIH; 전체 검색 시 검색 목록 내의 모든 항목으로 LIKE 검색 [권한관리,시스템관리,My정보];
    //fn_util_event_changeOption('optSearch','inpSearch');
}

//------------------------------------------------------------------------
//----- Connection -------------------------------------------------
//------------------------------------------------------------------------

function fn_onSuccess_countJobID(cnt,s){
    cnt = parseInt(cnt.Integer);
    if(cnt > 0 ){
        alert('중복된 JOB ID 가 있습니다.'+
            '\n 다른 ID를 지정하시기 바랍니다.');
        $('#popUpA-isDuplicatedID-h').val('Y');
        $('#popUpA-jobID').focus();
    }else{
        alert('중복된 JOB ID 가 없습니다.');
        $('#popUpA-isDuplicatedID-h').val('N');
    }
}

//------------------------------------------------------------------------
//----- Select Job Param -------------------------------------------------
//------------------------------------------------------------------------
function fn_setJobParamRow(data,status){
    var resultData = data.JobParamInfo;
    var jobID = resultData.jobID;
    var jobDc = resultData.jobDc;
    var createDt = resultData.createDt;
    var modifyDt = resultData.modifyDt;
    var jobParamJson = resultData.jobParamJson;
    var innerHtml = '';

    jobDc = jobDc == null ? '' : jobDc;
    createDt = fn_util_formatDate(createDt,true,true,'/');
    modifyDt = modifyDt == null ? '' : fn_util_formatDate(modifyDt,true,true,'/');

    $('#popUpM-jobID').text(jobID);
    $('#popUpM-jobDc').val(jobDc);
    $('#popUpM-createDt').text(createDt);
    $('#popUpM-modifyDt').text(modifyDt);

    $.each($.parseJSON(jobParamJson.toString()), function(k, v) {
        innerHtml += '<tr>';
        innerHtml += '  <td><label><input type="text" class="txtJsonKey" value="'+k+'" /></label></td>';
        innerHtml += '  <td><label><input type="text" class="txtJsonValue w100p" value="'+v+'" /></label></td>';
        innerHtml += '  <td class="txtC">';
        innerHtml += '      <button type="button" class="tblInBtn deleteBtn" onclick="paramDelList(this)">삭제</button>';
        innerHtml += '  </td>';
        innerHtml += '</tr>';
    });

    $('#popUpM-tbodyData').html(innerHtml);
}
//------------------------------------------------------------------------
// -----  Modify Job Param -----------------------------------------------
//------------------------------------------------------------------------
// click button of save & delete in modify job param section
function fn_modifyJobParam(btnID){
    var url = controller + '/jobParamModifyRow.do';
    var jobParamJson;
    var modeMsg;

    // check jobID
    if(!fn_util_isValid_typeof( $('#popUpM-jobID').text() )){
        alert('JOB ID를 확인 바랍니다');
        return false;
    }else{
        $('#popUpM-jobID-h').val( $('#popUpM-jobID').text() );
    }

    if(btnID == 'btnM-delete'){         // delete job param by jobID
        $('#popUpM-controlMode-h').val('D');
        $('#popUpM-useYn-h').val('N');
        $('#popUpM-delYn-h').val('Y');
        modeMsg = '삭제';

    }else if(btnID == 'btnM-update'){  // update job param by jobID
        $('#popUpM-controlMode-h').val('M');
        $('#popUpM-useYn-h').val('Y');
        $('#popUpM-delYn-h').val('N');
        modeMsg = '수정';

        if( fn_isValidate_paramJson('popUpM-tbodyData') ){
            jobParamJson = fn_util_combineJsonInTbody('popUpM-tbodyData');
            $('#popUpM-jobParamJson-h').val(jobParamJson);
        }else{
            return false;
        }
    }
    // call ajax
    if (confirm(modeMsg+" 하시겠습니까?")) {
        fn_util_ajax_serialize('modifyForm',url);
    }
}

//------------------------------------------------------------------------
// -----  Add Job Param -----------------------------------------------
//------------------------------------------------------------------------
// click save button in add job param section
function fn_addJobParam(){
    var url = controller + '/jobParamAddRow.do';
    var jobParamJson;

    if( fn_isValidate_jobID('popUpA-jobID','popUpA-isDuplicatedID-h',false) ){

        $('#popUpA-useYn-h').val('Y');
        $('#popUpA-delYn-h').val('N');

        if( fn_isValidate_paramJson('popUpA-tbodyData') ){
            jobParamJson = fn_util_combineJsonInTbody('popUpA-tbodyData');
            $('#popUpA-jobParamJson-h').val(jobParamJson);

            // call ajax
            if (confirm("등록 하시겠습니까?")) {
                fn_util_ajax_serialize('addForm',url);
            }
        }
    }
}
//------------------------------------------------------------------------
// -----  Validations -----------------------------------------------
//------------------------------------------------------------------------

function fn_isValidate_jobID(targetID, confirmID, checkDuplication){
    var isValidate = false;
    var $jobID = $('#'+targetID);
    var $isDup = $('#'+confirmID);

    // check jobID
    if( !fn_util_isValid_typeof( $jobID.val() ) ){
        alert('입력된 JOB ID를 확인 바랍니다');
        $jobID.focus();
    }else{
        if(checkDuplication){
            var url = controller + '/jobParamJobIDCountForValidKey.do';
            var param = {'jobID' : $jobID.val() };
            fn_util_ajax_basic(param,url,fn_onSuccess_countJobID,fn_util_ajax_onError_alert);
        }
        if( $isDup.val() == 'N' ){
            isValidate = true;
        }
        if( !checkDuplication && $isDup.val() == 'Y' ){
            alert('JOB ID 의 중복 확인을 하시기 바랍니다.');
        }

    }

    return isValidate;
}

function fn_isValidate_paramJson(tbodyID){
    var isValidate = true;
    var trLength = $('#'+tbodyID+' tr').length;
    var $tr = $('#'+tbodyID+' tr');
    // validate [ BLANK ] job param json
    $($tr.get().reverse()).each(function(){
        if( $(this).find('td .txtJsonKey').val().isBlank() ){
            $(this).find('td .txtJsonKey').focus();
            isValidate = false;
        }else if( $(this).find('td .txtJsonValue').val().isBlank() ){
            $(this).find('td .txtJsonValue').focus();
            isValidate = false;
        }
    });

    // validate [ DUPLICATION ] job param json
    if(isValidate){
        var $param;
         $($tr.get().reverse()).each(function(idx1,b){
            $param = $(this).find('td .txtJsonKey');

            if(isValidate){
                $($tr.get().reverse()).each(function(idx2,b){
                     if( $param.val() == $(this).find('td .txtJsonKey').val()
                        && idx1 != idx2){
                        alert('중복된 파라미터 항목이 있습니다. \n 중복된 PARAM : '
                            +$param.val());
                        $param.focus();
                        isValidate = false;
                        return false;
                     }
                });
            }
        });
    }else{
        alert('JOB PARAMETER 에 빈 값이 있습니다.');
    }

    return isValidate;
}

function initFormValues(){
    $('#popUpM-controlMode-h').val('M');
    $('#popUpM-jobID-h').val('');
    $('#popUpM-jobParamJson-h').val('');
    $('#popUpM-useYn-h').val('Y');
    $('#popUpM-delYn-h').val('N');
    $('#popUpM-jobID').text('');
    $('#popUpM-jobDc').val('');
    $('#popUpM-tbodyData').html('');
    $('#popUpM-createDt').text('');
    $('#popUpM-modifyDt').text('');

    $('#popUpA-jobParamJson').val('');
    $('#popUpA-useYn-h').val('Y');
    $('#popUpA-delYn-h').val('N');
    $('#popUpA-isDuplicatedID-h').val('Y');
    $('#popUpA-jobID').val('');
    $('#popUpA-jobDc').val('');
    $('#popUpA-tbodyData').html('');
    paramAddList('paramTabl2');
}
//------------------------------------------------------------------------
function linkPage(pageNo) {
    $("#currentPageNo").val(pageNo);

    $("#listForm").attr({
        action : "jobParamList.do",
        method : "post"
    }).submit();
}

function goOrder(order) {
    $("#listOrder").val(order);

    $("#listForm").attr({
        action : "jobParamList.do",
        method : "post"
    }).submit();
}

function searchJobParam() {
    if( fn_util_isValid_optionTextSearchWithAll('optSearch','inpSearch') ){
        $("#currentPageNo").val("1");
        $("#recordCountPerPage").val("10");

        $("#listForm").attr({
            action : "jobParamList.do",
            method : "post"
        }).submit();
    }
}

function paramAddList(a){

    var paramHtml = "";
    paramHtml +='<tr>';
    paramHtml +='<td><label><input type="text" class="txtJsonKey" /></label></td>';
    paramHtml +='<td><label><input type="text" class="txtJsonValue w100p" /></label></td>';
    paramHtml +='<td class="txtC">';
    paramHtml +='<button type="button" class="tblInBtn deleteBtn" onclick="paramDelList(this)">삭제</button>';
    paramHtml +='</td>';
    paramHtml +='</tr>';

    if($("#"+a+" tr").length < 31){
        $("#"+a).append(paramHtml);
    }else{
        alert("파라미터 추가는 최대 30개까지 가능합니다.");
    }
}

function paramDelList(a){

    var prTblId = $(a).parent().parent().parent().parent().attr('id');

    if($("#"+prTblId+" tr").length > 2){
        $(a).parents('tr:first').remove();
    }else{
        alert("파라미터값은 최소 1개는 설정되어야합니다.");
    }

}
</script>

<div class="mConBox">

    <div class="mCont">
        <div class="row">
            <div class="col-md-4" id="subtitleA">
                <h2><i class="ti-layers"></i> Job Param 설정</h2>
            </div>
            <div class="col-md-8">
                <div class="pageRoute">
                    <ul>
                        <li><a href="#">HOME</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li><a href="#">시스템 관리</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li><a href="#">배치 관리</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li class="current"><a href="#">Job Param 설정</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end row -->
        <form:form action="jobParamList.do" method="get" modelAttribute="jobParamParam" id="listForm">
            <form:hidden path="listOrder"/>
            <input type="hidden" name="activeDomainKey" value="${activeDomainKey}" />
            <input type="hidden" name="activeServiceKey" value="${activeServiceKey}" />
            <input type="hidden" name="activeServiceTitle" value="${activeServiceTitle}" />
            <input type="hidden" name="activeMenuKey" value="${activeMenuKey}" />
            <input type="hidden" name="leftMenuKey"  value="${leftMenuKey}" />
            <input type="hidden" name="activeDomainTitle" value="${activeDomainTitle}" />

            <div class="row">
                <div class="col-md-12">

                    <div class="conBox" style="min-height:400px;">
                        <div class="row">
                            <div class="col-md-8 select-set-3">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-33 select-set-3">
                                <p> 총 <b>${jobParamParam.totalRecordCount}</b> Job Params</p>
                            </div>
                            <div class="col-md-9 text-right select-sys-1">
                                <form:select path="recordCountPerPage" onChange="linkPage(1);">
                                    <form:option value="10" >10개씩 보기</form:option>
                                    <form:option value="20" >20개씩 보기</form:option>
                                </form:select>
                                <form:select id="optSearch" path="searchOption">
                                    <form:option value="0">전체</form:option>
                                    <form:option value="jobID">Job ID</form:option>
                                    <form:option value="jobDc">설명</form:option>
                                </form:select>
                                <label class="labelHidden" for="inpSearch">검색어를 입력하세요</label>
                                <form:input path="searchText" type="text" id="inpSearch" name="searchText" class="searchTxt" placeholder="검색어를 입력하세요" />
                                <button type="button" id="btnSearch" class="searchBtn">
                                    <i class="ti-search"></i> 검색
                                </button>
                                <button type="button" id="btnPopUpAdd" class="AddDefaultBtn ParamAddBtn" title="job param을 추가할 수 있습니다.">
                                    <i class="fa fa-plus"></i> Job Param 추가등록
                                </button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <table class="viewDateTbl">
                                    <colgroup>
                                        <col style="width:20%;" />
                                        <col style="width:50%;" />
                                        <col style="width:10%;" />
                                        <col style="width:10%;" />
                                        <col style="width:10%;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <c:choose>
                                                <c:when test="${jobParamParam.listOrder == 'id_asc'}">
                                                    <th class="fingerPointer" onclick="goOrder('id_desc');return false;">Job ID <i class="fa fa-sort-asc" aria-hidden="true"></i></th>
                                                </c:when>
                                                <c:when test="${jobParamParam.listOrder == 'id_desc'}">
                                                    <th class="fingerPointer" onclick="goOrder('id_asc');return false;">Job ID <i class="fa fa-sort-desc" aria-hidden="true"></i></th>
                                                </c:when>
                                                <c:otherwise>
                                                    <th class="text-center fingerPointer" onclick="goOrder('id_desc');return false;">Job ID <i class="fa fa-sort" aria-hidden="true"></i></th>
                                                </c:otherwise>
                                            </c:choose>
                                            <th>Description</th>
                                            <c:choose>
                                                <c:when test="${jobParamParam.listOrder == 'regdate_asc'}">
                                                    <th class="fingerPointer" onclick="goOrder('regdate_desc');return false;">등록일 <i class="fa fa-sort-asc" aria-hidden="true"></i></th>
                                                </c:when>
                                                <c:when test="${jobParamParam.listOrder == 'regdate_desc'}">
                                                    <th class="fingerPointer" onclick="goOrder('regdate_asc');return false;">등록일 <i class="fa fa-sort-desc" aria-hidden="true"></i></th>
                                                </c:when>
                                                <c:otherwise>
                                                    <th class="fingerPointer" onclick="goOrder('regdate_desc');return false;">등록일 <i class="fa fa-sort" aria-hidden="true"></i></th>
                                                </c:otherwise>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${jobParamParam.listOrder == 'moddate_asc'}">
                                                    <th class="fingerPointer" onclick="goOrder('moddate_desc');return false;">수정일 <i class="fa fa-sort-asc" aria-hidden="true"></i></th>
                                                </c:when>
                                                <c:when test="${jobParamParam.listOrder == 'moddate_desc'}">
                                                    <th class="fingerPointer" onclick="goOrder('moddate_asc');return false;">수정일 <i class="fa fa-sort-desc" aria-hidden="true"></i></th>
                                                </c:when>
                                                <c:otherwise>
                                                    <th class="fingerPointer" onclick="goOrder('moddate_desc');return false;">수정일 <i class="fa fa-sort" aria-hidden="true"></i></th>
                                                </c:otherwise>
                                            </c:choose>
                                            <th>상세관리</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:if test="${empty jobParamList}">
                                        <tr><td colspan="5">No data available in table</td></tr>
                                    </c:if>
                                    <c:if test="${not empty jobParamList}">
                                        <c:forEach var="list" items="${jobParamList}" varStatus="status">
                                            <tr id="${list.jobID}">
                                                <td>${list.jobID}</td>
                                                <td>
                                                <c:choose>
                                                    <c:when test="${fn:length(list.jobDc) > 80 }">${fn:substring(list.jobDc, 0 , 80)} ...</c:when>
                                                    <c:otherwise>${list.jobDc}</c:otherwise>
                                                </c:choose>
                                                </td>
                                                <td>
                                                    <fmt:formatDate value="${list.createDt}" pattern="yyyy/MM/dd"/>
                                                    <br />
                                                    <fmt:formatDate value="${list.createDt}" pattern="HH:mm:ss"/>
                                                </td>
                                                <td>
                                                    <fmt:formatDate value="${list.modifyDt}" pattern="yyyy/MM/dd"/>
                                                    <br />
                                                    <fmt:formatDate value="${list.modifyDt}" pattern="HH:mm:ss"/>
                                                </td>
                                                <td>
                                                    <button type="button" class="tblInBtn btnModifyRow">수정</button>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8 pageNum">
                                <!-- Paging With Taglib -->
                                <ui:pagination paginationInfo="${jobParamParam}" type="css" jsFunction="linkPage" />
                                <form:hidden path="currentPageNo" />
                            </div>
                            <div class="col-md-2">
                            <!--
                                <button type="button" id="btnExcel" class="excelDw">
                                    <i class="fa fa-download"></i> 엑셀 다운로드
                                </button>
                            -->
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end conBox -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </form:form>
    </div>
    <!-- end mCont -->
</div>
<!-- end mConBox -->


<div id="divPopUpM-JobParam" class="popUp col-md-8">
    <div class="popUpTop">Job Param 수정
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="jobParamModifyRow.do" method="POST" modelAttribute="jobParamInfo" id="modifyForm">
                <input type="hidden" id="popUpM-controlMode-h"  name="controlMode"  value="M" />
                <input type="hidden" id="popUpM-jobID-h"        name="jobID"        value="" />
                <input type="hidden" id="popUpM-jobParamJson-h" name="jobParamJson" value="" />
                <input type="hidden" id="popUpM-useYn-h"        name="useYn"        value="Y" />
                <input type="hidden" id="popUpM-delYn-h"        name="delYn"        value="N" />
                <table class="popInTbl">
                    <colgroup>
                        <col width="20%"/>
                        <col width="30%" />
                        <col width="20%"/>
                        <col width="30%" />
                    </colgroup>
                    <tr>
                        <th>Job ID</th>
                        <td colspan="3" id="popUpM-jobID"></td>
                    </tr>
                    <tr>
                        <th>설명</th>
                        <td colspan="3">
                            <textarea id="popUpM-jobDc" name="jobDc" cols="30" rows="10"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>Job Parameter Json</th>
                        <td colspan="3">
                            <table id="paramTabl1" class="popInTblInner">
                                <colgroup>
                                    <col width="30%"/>
                                    <col width="55%" />
                                    <col width="15%"/>
                                </colgroup>
                                <thead>
                                <tr>
                                    <th>PARAM</th>
                                    <th>VALUE</th>
                                    <th>삭제</th>
                                </tr>
                                </thead>
                                <tbody id="popUpM-tbodyData"></tbody>
                            </table>

                            <div class="TblInBtnWrap">
                                <button type="button" class="tblInBtn darkGrayBtn" onclick="paramAddList('paramTabl1')">
                                    <i class="fa fa-plus" aria-hidden="true"></i> PARAM 추가
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>등록일</th>
                        <td id="popUpM-createDt"></td>
                        <th class="borderZtop">수정일</th>
                        <td id="popUpM-modifyDt"></td>
                    </tr>
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnM-delete" class="deleteBtn">삭제</button>
            <button type="button" id="btnM-update" class="darkGrayBtn">저장</button>
            <button type="button" id="btnM-cancel" class="lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>

<!--paramAdd-->
<div id="divPopUpA-JobParam" class="popUp col-md-8">
    <div class="popUpTop">Job Param 추가등록
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="jobParamAddRow.do" method="post" modelAttribute="jobParamInfo" id="addForm">
                <input type="hidden" id="popUpA-jobParamJson-h" name="jobParamJson" value="" />
                <input type="hidden" id="popUpA-useYn-h"        name="useYn"        value="Y" />
                <input type="hidden" id="popUpA-delYn-h"        name="delYn"        value="N" />
                <input type="hidden" id="popUpA-isDuplicatedID-h" value="Y" />
                <table class="popInTbl">
                    <colgroup>
                        <col width="20%"/>
                        <col width="80%" />
                    </colgroup>
                    <tr>
                        <th><label for="popUpA-jobID">Job ID</label></th>
                        <td>
                            <input type="text" id="popUpA-jobID" class="w70p" name="jobID" />
                            <button type="button" id="btnA-checkDuplication"
                                class="tblInBtn inputRightBtn darkGrayBtn w80px">중복확인</button>
                        </td>
                    </tr>
                    <tr>
                        <th>설명</th>
                        <td>
                            <textarea id="popUpA-jobDc" name="jobDc" cols="30" rows="10"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>Job Parameter Json</th>
                        <td>
                            <table id="paramTabl2" class="popInTblInner">
                                <colgroup>
                                    <col width="30%"/>
                                    <col width="55%" />
                                    <col width="15%"/>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>PARAM</th>
                                        <th>VALUE</th>
                                        <th>삭제</th>
                                    </tr>
                                </thead>
                                <tbody id="popUpA-tbodyData">
                                    <tr>
                                        <td><label><input type="text" class="txtJsonKey" /></label></td>
                                        <td><label><input type="text" class="txtJsonValue w100p" /></label></td>
                                        <td class="txtC"><button type="button" class="tblInBtn deleteBtn" onclick="paramDelList(this)">삭제</button></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="TblInBtnWrap">
                                <button type="button" class="tblInBtn darkGrayBtn" onclick="paramAddList('paramTabl2')">
                                    <i class="fa fa-plus" aria-hidden="true"></i> PARAM 추가
                                </button>
                            </div>
                        </td>
                    </tr>
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnA-insert" class="btn-style1 darkGrayBtn">저장</button>
            <button type="button" id="btnA-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>