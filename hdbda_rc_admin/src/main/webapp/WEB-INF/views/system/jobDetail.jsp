<%------------------------------------------------------------------------
 * TODO > 모니터링 리스트 상세보기
 * 파일명   > jobDetail.jsp
 * 작성일   > 2018. 12. 11
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   >  
------------------------------------------------------------------------%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>

<div class="mConBox">
	<div class="mCont">
		<div class="row">
			<div class="col-md-4">
				<h2>ETL_RAW_LOGGING</h2>
			</div>

			<div class="col-md-8">
				<div class="pageRoute">
					<ul>
						<li><a href="#">HOME</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li><a href="#">시스템 관리</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li><a href="#">모니터링</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li class="current"><a href="#">job</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end row -->

		<form id="listForm" method="post">
			<div class="row">
				<div class="col-md-12">
					<div class="conBox" style="min-height: 400px;">

						<div class="row">
							<div class="col-md-33 select-set-3">
								<p>
									<%-- 총 <b>${schedulerParam.totalRecordCount}</b> 개 --%>
								</p>
							</div>
							<div class="col-md-9 text-right select-sys-1">
								<button type="button" title="이전 페이지로 이동 합니다."	id="btnBack" class="AddDefaultBtn">
									<i class="ti-arrow-left"></i> 이전 페이지
								</button>
							</div>
						</div>
						<!-- end row > conBox > col-md-12 > row > form -->

						<div class="row">
							<div class="col-md-12">
								<table id="monotoringTable" class="viewDateTbl menuSetTbl">
									<colgroup>
										<col width="350">
										<col width="150">
										<col width="130">
										<col width="150">
										<col width="150">
										<col width="100">
									</colgroup>
									<thead>
										<tr>
											<th>태스크 아이디</th>
											<th>상태</th>
											<th>시스템 명</th>
											<th>테이블 명</th>
											<th>실행 시각</th>
											<th>상세보기</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<!-- end row -->

						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-2 select-set-3"></div>
						</div>
						<!-- end row -->
					</div>
					<!-- end conBox -->
				</div>
				<!-- end col-md-12 -->
			</div>
			<!-- end row -->
		</form>
	</div>
	<!-- end mCont -->
</div>
<!-- end mConBox -->


<div id="divPopUpM-AlgoMgmt" class="popUp col-md-8">
    <div class="popUpTop">모니터링 상세
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="schedulerUpdate.do" method="POST" modelAttribute="scheduler" id="modifyForm">
                <input type="hidden" id="popUpM-job_id" name="task_id" value="" />
                <table class="popInTbl">
                    <colgroup>
                        <col width="30%"/>
                        <col width="70%"/>
                    </colgroup>
                    <tr>
                        <th><label for="popUpM-jobDesc">태스크 ID</label></th>
                        <td>
                            <input type="text" id="txt-monitor-taskId" class="w100p" name="txt-monitor-taskId" value="" readonly="readonly"/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-tableNm">상태</label></th>
                        <td>
                            <input type="text" id="txt-monitor-status" class="w100p" name="txt-monitor-status" value="" readonly="readonly"/>
                        </td>
                    </tr>                  
                    <tr>
                        <th><label for="popUpM-etlSys">시스템명</label></th>
                        <td>
                            <input type="text" id="txt-monitor-sysNm" class="w100p" name="txt-monitor-sysNm" readonly="readonly"/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-etlTbl">테이블명</label></th>
                        <td>
                            <input type="text" id="txt-monitor-tblNm" class="w100p" name="txt-monitor-tblNm" readonly="readonly"/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-cronExpr">결과 값</label></th>
                        <td>
                            <input type="text" id="txt-monitor-exeVal" class="w100p" name="txt-monitor-exeVal" readonly="readonly"/>
                        </td>
                    </tr>                                        
                    <tr>
                        <th><label for="popUpM-paramStr">메시지</label></th>
                        <td>
                            <textarea id="txt-monitor-msg" name="txt-monitor-msg" cols="30" rows="10"></textarea>
                        </td>
                    </tr>                    
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnM-cancel" class="btn-style1 lightGrayBtn popCancel">닫기</button>
        </div>
    </div>
</div>


<script type="text/javascript">

var apiUrl = multiApiSet();

$(document).ready(function(){

	var server_jobID = '${task_id}'; 
	fn_Ajax_SearchByTaskID(server_jobID);
	pageEvents();
});

function pageEvents(){
	
	$('#btnBack').unbind('click').bind('click',function(){ 
		window.history.back();
	});
}


function multiApiSet(){
	var api_url, api_protocol = location.protocol;
	var api_chkUrl = window.location.href;
	if('' != api_chkUrl) {
		if('http:' == api_protocol) {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
		} else {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
		}
	} else {
		return false;
	}
}
	
	// 화면에서 보기 버튼  클릭
	function view(tag){
		var id = tag.id;
		var pkArray = id.split("|");
		var etl_load_dd = pkArray[0];
		var task_id = pkArray[1];
		var etl_status = pkArray[2];
		var url = apiUrl+'/logging/view?etl_load_dd='+etl_load_dd+'&task_id='+task_id+'&etl_status='+etl_status;
		
		$.ajax({
            url:url,
            crossDomain:true,
            type:'POST',
            async:false,
            success:function(result){
            	$('#txt-monitor-taskId').val(result.result.task_id); 
            	$('#txt-monitor-status').val(result.result.etl_status);
            	$('#txt-monitor-sysNm').val(result.result.sys_nm);
            	$('#txt-monitor-tblNm').val(result.result.tbl_nm);
            	$('#txt-monitor-exeVal').val(result.result.etl_value);
            	$('#txt-monitor-msg').val(result.result.etl_message);
            },
            error:function(e){
                alert('서버와의 통신에 실패하였습니다.');
            }
        });
		
	    fn_util_divPopUpControl('divPopUpM-AlgoMgmt',true);       
	}

function fn_Ajax_SearchByTaskID(task_ID) {
	
	var url      = apiUrl+'/task_id/'+task_ID;
    var succMs   = "테이블이 조회 되었습니다.";
    var errorMs  = "시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.";
    var netErrMs = "네트워크 통신 에러.."; 
	var param = {'task_ID':task_ID};
	
	 $.ajax({
	        type: 'POST',
	        url: url,
	        dataType:'JSON',
	        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
	        data:param,
	        async:false,
	        success: function(data){
	        	if(data.list != ""){
		            var results = data.list;
		            var str = '';            
		            $.each(results, function(i){          	
		            	str +='<TR>'+
		            		  '<TD>'+ results[i].task_id     + '</TD>' +
		            		  '<TD>'+ results[i].etl_status  + '</TD>' +
		            		  '<TD>'+ results[i].sys_nm      + '</TD>' +
		            		  '<TD>'+ results[i].tbl_nm      + '</TD>' +
		            		  '<TD>'+ results[i].reg_dt   + '</TD>' +
		            		  '<TD><button type="button" onclick="view(this);" class="tblInBtn btnModifyRow" id="'+results[i].etl_load_dd+'|'+results[i].task_id+'|'+results[i].etl_status+ '">보기</button></TD>';
		            	str +='</TR>';
		            });
		            $("#monotoringTable").append(str);
		            
		            leftH();
	        	}else if(data.list == ""){
	        		var str = '';
	 				str += '<tr><td colspan="7" style="text-align:center">No data available in table</td></tr>';
	                $('#monotoringTable').append(str);
	        	}
	        },
	        error : function(e){
	        	alert(netErrMs+": "+e);
	        }
	    });	
}
</script>