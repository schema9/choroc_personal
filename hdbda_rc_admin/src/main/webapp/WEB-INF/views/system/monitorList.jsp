<%------------------------------------------------------------------------
 * TODO > 모니터링 리스트 [RestAPI] 
 * 파일명   > monitorList.jsp
 * 작성일   > 2018. 12. 11
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 2019.03.15 달력추가
 * 수정자   > Kim.Sang.Su
------------------------------------------------------------------------%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript">
	var controller = '/system/monitoring';

	$(window).ready(function(){
		var startDate;
		var endDate;
		if('' == $('#strt_ddtm').val()) {
			startDate = $('#datePickerId10').data('daterangepicker').startDate.format('YYYY/MM/DD'); // 시작일
			endDate = $('#datePickerId10').data('daterangepicker').endDate.format('YYYY/MM/DD');     // 종료일
		} else {
			startDate1 = $('#strt_ddtm').val().substring(0,4);
			startDate2 = $('#strt_ddtm').val().substring(4,6);
			startDate3 = $('#strt_ddtm').val().substring(6,8);
			endDate1 = $('#end_ddtm').val().substring(0,4);
			endDate2 = $('#end_ddtm').val().substring(4,6);
			endDate3 = $('#end_ddtm').val().substring(6,8);
			$('#datePickerId10').val(startDate1+'/'+startDate2+'/'+startDate3+' - '+endDate1+'/'+endDate2+'/'+endDate3);
		}
	    pageEvents();
	});

	function pageEvents(){
	 	// 검색버튼 클릭 이벤트 [시작페이지"1",글목록수"10"셋팅]
	    $('#btnSearch').unbind('click').bind('click',function(){
	        $("#currentPageNo").val("1");
	        $("#listForm").attr({
	            action : "monitorList.do",
	            method : "post"
	        }).submit();
	    });
	}

	function linkPage(pageNo) {
		$("#currentPageNo").val(pageNo);
		$("#listForm").attr({
			action : "monitorList.do",
			method : "post"
		}).submit();
	}
	
	function go_MonitorControllerPage(task_ID) {
		$("#task_id").val(task_ID);
	    $("#listForm").attr({
	        action : "jobDetail.do",
	        method : "post"
	    }).submit();
	}
	
	function go_SchedulerDetailView(job_nm) {
		$("#job_nm").val(job_nm);
		
		var screenW = screen.availWidth;	// 스크린 가로사이즈
		var screenH = screen.availHeight;	// 스크린 세로사이즈
		var popW = 1000;		// 팝업창 가로사이즈
		var popH = 700;		// 팝업창 세로사이즈
		var posL = (screenW - popW) / 2;	
		var posT = (screenH - popH) / 2;		
		
		window.open("schedulerDetailView.do?job_nm=" + encodeURI(encodeURIComponent(job_nm)), "schedulerDetailView", "width=" + popW + ", height=" + popH +", top=" + posT + ", left="+ posL + ", menubar=no, location=no, toolbar=no, resizable=no, scrollbars=yes");
	}

	// 달력 선택
	function dateFunction10(startDate, endDate) {
		$('#strt_ddtm').val(startDate);
		$('#end_ddtm').val(endDate);
		console.log("dateFunction10 시작일:" + startDate + " " + "종료일:" + endDate);
	}	
		
</script>

<div class="mConBox">
	<div class="mCont">
		<div class="row">
			<div class="col-md-4" id="subtitleA">
				<h2>
					<i class="ti-layers"></i>
				</h2>
			</div>
			<div class="col-md-8">
				<div class="pageRoute">
					<ul>
						<li><a href="#">HOME</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li><a href="#">추천환경 관리</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li class="current"><a href="#">추천영역</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end row -->

		<form:form modelAttribute="MonitoringParam" id="listForm" method="POST">
			<div class="row">
				<div class="col-md-12">
					<div class="conBox" style="min-height: 400px;">
						<input type= "hidden" name= "task_id" id="task_id" value="">
						<input type= "hidden" name= "job_nm" id="job_nm" value="">
						<input type= "hidden" name= "strt_ddtm" id="strt_ddtm" value="${monitoringParam.strt_ddtm}">
						<input type= "hidden" name= "end_ddtm"  id="end_ddtm"  value="${monitoringParam.end_ddtm}">
						<div class="row">
							<div class="col-md-33 select-set-3">
								<p>
									총 <b>${monitoringParam.totalRecordCount}</b> 개
								</p>
							</div>
							<div class="col-md-9 text-right select-sys-1">
                    			<div class="dateRange">
                        			<input type="text" id="datePickerId10" />
                        			<i class="ti-calendar" id="dateCal1"></i>
                    			</div>
								<form:select path="recordCountPerPage" onChange="linkPage(1);" style="display:none;">
									<form:option value="10">10개씩 보기</form:option>
									<form:option value="20">20개씩 보기</form:option>
									<form:option value="50">50개씩 보기</form:option>
									<form:option value="100">100개씩 보기</form:option>
								</form:select>
								<form:select id="searchOption1" path="searchOption1" style="display:none;">
									<form:option value="">시스템 분류</form:option>
									<form:option value="EOAD">EOAD</form:option>
									<form:option value="HCAMP">HCAMP</form:option>
									<form:option value="HGCON">HGCON</form:option>
									<form:option value="HMART">HMART</form:option>
									<form:option value="HODS">HODS</form:option>
									<form:option value="HPROF">HPROF</form:option>
									<form:option value="WEBLOG">WEBLOG</form:option>
								</form:select>
								<form:select id="searchOption2" path="searchOption2" style="display:none;">
									<form:option value="">상태코드</form:option>
									<form:option value="S">메시지전송</form:option>
									<form:option value="W">실행대기중</form:option>
									<form:option value="R">실행중</form:option>
									<form:option value="F">실행완료</form:option>
									<form:option value="E">오류발생</form:option>
								</form:select>
								<form:select id="searchOption3" path="searchOption3" style="display:none;">
									<form:option value="">구분</form:option>
									<form:option value="TSK">태스크 아이디</form:option>
									<form:option value="JOB">잡 이름</form:option>
									<form:option value="TBL">영문 테이블명</form:option>
								</form:select>
								<form:input path="searchText" type="text" id="inpSearch"
									class="searchTxt" placeholder="검색어를 입력하세요" value=""/>
								<button type="button" id="btnSearch" class="searchBtn">
									<i class="ti-search"></i> 검색
								</button>
							</div>
						</div>
						<!-- end row > conBox > col-md-12 > row > form -->
						<div class="row">
							<div class="col-md-12">
								<table class="viewDateTbl" >
									<colgroup>
<%-- 										<col width="250"> --%>
										<col width="200">
<%-- 										<col width="200"> --%>
<%-- 										<col width="80"> --%>
										<col width="120">
										<col width="120">
										<col width="120">
										<!-- <col width="100"> -->
									</colgroup>
									<thead>
										<tr>
											<th>프로세스명</th>
<!-- 											<th>잡 이름</th> -->
<!-- 											<th>영문 테이블명</th> -->
<!-- 											<th>상태 코드</th> -->
											<th>실행일자</th>
											<th>시작일시</th>
											<th>종료일시</th>
										</tr>
									</thead>
									<c:if test="${empty resultList}">
										<tbody>
											<tr>
												<td colspan="6">No data available in table</td>
											</tr>
										</tbody>
									</c:if>
									<c:if test="${not empty resultList}">
										<c:forEach var="list" items="${resultList}" varStatus="status">
											<c:if test="${status.first}">
												<tbody class="tbGroup">
											</c:if>
											<td class="txtL">${list.task_id}</td>
<%-- 											<td><a  href="javascript:go_MonitorControllerPage('${list.task_id}')">${list.task_id}</a></td> --%>
<%-- 											<td><a  href="javascript:go_SchedulerDetailView('${list.job_nm}')">${list.job_nm}</a></td> --%>
<%-- 											<td>${list.etl_tgt_tbl}</td> --%>
<%-- 											<c:choose> --%>
<%-- 												<c:when test="${list.stts_cd eq 'S'}"> --%>
<!-- 													<td>메시지전송</td> -->
<%-- 												</c:when> --%>
<%-- 												<c:when test="${list.stts_cd eq 'W'}"> --%>
<!-- 													<td>실행대기중</td> -->
<%-- 												</c:when> --%>
<%-- 												<c:when test="${list.stts_cd eq 'R'}"> --%>
<!-- 													<td>실행중</td> -->
<%-- 												</c:when> --%>
<%-- 												<c:when test="${list.stts_cd eq 'F'}"> --%>
<!-- 													<td>실행완료</td> -->
<%-- 												</c:when> --%>
<%-- 												<c:when test="${list.stts_cd eq 'E'}"> --%>
<!-- 													<td>오류발생</td> -->
<%-- 												</c:when> --%>
<%-- 												<c:otherwise> --%>
<!-- 													<td></td> -->
<%-- 												</c:otherwise> --%>
<%-- 											</c:choose>	 --%>
											<td>${list.reg_ddtm}</td>
											<td>${list.reg_ddtm}</td>
											<td>${list.end_ddtm}</td>
<%-- 											<c:if test="${not empty list.end_ddtm}"> --%>
<%-- 												<td>${list.end_ddtm}</td> --%>
<%-- 											</c:if> --%>
<%-- 											<c:if test="${empty list.end_ddtm}"> --%>
<!-- 												<td></td> -->
<%-- 											</c:if> --%>
											</tr>
											<c:if test="${status.last}">
												</tbody>
											</c:if>
										</c:forEach>
									</c:if>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8 pageNum">
								<!-- Paging With Taglib -->
								<ui:pagination paginationInfo="${monitoringParam}" type="css"
									jsFunction="linkPage" />
								<form:hidden path="currentPageNo"
									value="${monitoringParam.currentPageNo }" />
							</div>
							<div class="col-md-2 select-set-3"></div>
						</div>						
					</div>
				</div>
			</div>
		</form:form>
	</div>
</div>
<!-- end mConBox -->