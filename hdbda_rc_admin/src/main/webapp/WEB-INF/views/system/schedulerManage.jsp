<%------------------------------------------------------------------------
 * TODO > 스케줄러 관리(조회 , 등록 , 수정)
 * 파일명   > menuManage.jsp
 * 작성일   > 2018. 12. 04.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   >  
------------------------------------------------------------------------%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>

<script type="text/javascript">

var controller = '/system/job';

$(window).ready(function(){
    pageEvents();
});

function pageEvents(){

    // 스케줄러 목록화면에서 스케줄러등록 버튼클릭
    $('#btnPopUpAdd').unbind('click').bind('click',function(){ 
        // 팝업창 값 초기화
        initFormValues();

        // 등록 팝업창 오픈
        fn_util_divPopUpControl('divPopUpA-AlgoMgmt',true);
        
		url = controller + '/selectAgentList.do';
        fn_util_ajax_basic("",url,fn_set_CmmCode_toSelectTag,fn_util_ajax_onError_alert);
        
    });
    
	 // 스케줄러 목록화면에서 스키마 단위 ETL 수동실행 버튼 클릭
    $('#btnPopUpetl').unbind('click').bind('click',function(){ 
        // 스키마 단위 ETL 수동실행 팝업 창 값 초기화
        initFormValues();

        // 스키마 단위 ETL 수동실행 팝업창 오픈
        fn_util_divPopUpControl('divPopUpE-AlgoMgmt',true);
    });
    
    // 스케줄러 목록화면에서 스케줄러 수정버튼  클릭
    $('.btnModifyRow').unbind('click').bind('click',function(){
    	
    	var job_id = $(this).parent().parent().attr('id');
    	
    	
        fn_util_divPopUpControl('divPopUpM-AlgoMgmt',true);       
        url = controller + '/schedulerSelect.do';
        param = {'job_id' : job_id};
        
        fn_util_ajax_basic(param,url,fn_setAlgoMgmtRow,fn_util_ajax_onError_alert);
    });
    
	$('.btnRedo').unbind('click').bind('click',function(){
    	
    	var job_id = $(this).parent().parent().attr('id');
    	
        fn_util_divPopUpControl('divPopUpP-AlgoMgmt',true);       
        url = controller + '/schedulerSelect.do';
        param = {'job_id' : job_id};
        fn_util_ajax_basic(param,url,fn_setAlgoRedoRow,fn_util_ajax_onError_alert);
    });    
        
    // 등록팝업창에서 저장 버튼 클릭 
    $('#btnA-insert').unbind('click').bind('click',function(){
    	
    	var url = controller + '/schedulerInsert.do';

        if (confirm("등록 하시겠습니까?")) {
            fn_util_ajax_serialize('addForm',url);
        }
        
        schedulerApply();
        
    });

    
    
    // 수정팝업창에서 수정 버튼 클릭
    $('#btnM-update').unbind('click').bind('click',function(){

    	var url = controller + '/schedulerUpdate.do';
    	
        if (confirm("수정 하시겠습니까?")) {
            fn_util_ajax_serialize('modifyForm',url);
        }
        
         schedulerApply();
    });
    
    // 즉시실행 팝업창에서 실행 버튼 클릭
    $('#btnP-redo').unbind('click').bind('click',function(){

    	var url = controller + '/schedulerRedo.do';
    	
        if (confirm("실행 하시겠습니까?")) {
        	var chk = '0';
        	var param = $('#redoForm').serialize();
            $.ajax({
                type: 'POST',
                url: url,
                data: param,
                crossDomain:true,
                async:false,
                success:function(resultData){
                    
                },
                error:function(e){
                	chk = '1';
                    alert('서버와의 통신에 실패하였습니다.');
                }
            });
            if(chk == '0') {
	            alert("실행되었습니다.");
                location.reload();
            }
        }
    });
    
 	// 스키마 단위 ETL 수동실행 팝업창에서 실행 버튼 클릭
    $('#btnE-etl').unbind('click').bind('click',function(){

    	// 체크박스 예외처리 (한개도 선택 안할 경우 alert)
    	if($("input[type=checkbox][name=etl_tgt_syss]:checked").length == 0){
    		alert("실행할 시스템 명을 한 개 이상 선택해주세요 ");
    	}else{
    		var url = controller + '/schedulerEtl.do';
    		var chk = '0';
    		
	        if (confirm("실행 하시겠습니까?")) {
	        	var param = $('#etlForm').serialize();
	            $.ajax({
	                type: 'POST',
	                url: url,
	                data: param,
	                crossDomain:true,
	                async:false,
	                success:function(resultData){
	                	
	                },
	                error:function(e){
	                	chk = '1';
	                    alert('서버와의 통신에 실패하였습니다.');
	                }
	            });
	            if(chk == '0') {
		            alert("실행되었습니다.");
	                location.reload();
	            }
	        }
    	}
    	
    });
 
    
    $('#btnP-cancel').unbind('click').bind('click',function(){
    	fn_util_divPopUpControl('divPopUpP-AlgoMgmt',false);
    	initFormValues();
    });
    
    $('#btnM-cancel').unbind('click').bind('click',function(){
    	fn_util_divPopUpControl('divPopUpM-AlgoMgmt',false);
    	initFormValues();
    });
    
    $('#btnA-cancel').unbind('click').bind('click',function(){
    	fn_util_divPopUpControl('divPopUpA-AlgoMgmt',false);
    	initFormValues();
    });
    
    $('#btnE-cancel').unbind('click').bind('click',function(){
    	fn_util_divPopUpControl('divPopUpE-AlgoMgmt',false);
    	initFormValues();
    });
    
    // In modify form - check box
    $('#popUpA-tbodyData-role').find('input[type=checkbox]').unbind('change').bind('change',function(){
        if( $('#popUpA-tbodyData-role').find('.rdChk').length == $('#popUpA-tbodyData-role').find('.rdChk:checked').length ){
            $('#popUpA-chkAll').prop('checked',true);
        }else{
            $('#popUpA-chkAll').prop('checked',false);
        }
    });
    
    // 검색버튼 클릭 이벤트 [시작페이지"1",글목록수"10"셋팅]
    $('#btnSearch').unbind('click').bind('click',function(){
    	
        $("#currentPageNo").val("1");
//         $("#recordCountPerPage").val("10");

        $("#listForm").attr({
            action : "schedulerList.do",
            method : "post"
        }).submit();
    });

}

function schedulerApply(){
	var url = controller + '/schedulerApply.do';
	$.ajax({
        type: 'POST',
        url: url,
        crossDomain:true,
        dataType : "json",
        async:false,
        success:function(resultData){

        },
        error:function(e){
            alert('서버와의 통신에 실패하였습니다.');
        }
    });
}


// 스케줄러 등록폼 에이전트 리스트 가져오기
function fn_set_CmmCode_toSelectTag(data){
	var resultData = data.result;
	
    // 에이전트 select 태그 생성
    var agentList = data.agentList;
    for(var i=0; i<agentList.length; i++){
		var id = agentList[i].agent_id;
		var option = $("<option value="+ id+">" + id +"</option>");
		$('#popUpA-agentId').append(option);
	 }
    
    var agentList = data.agentList;
    for(var i=0; i<agentList.length; i++){
		var id = agentList[i].agent_id;
		if(agent_id == id){
			var option = $("<option selected value="+ id+">" + id +"</option>");
		}else{
			var option = $("<option value="+ id+">" + id +"</option>");
		}
		$('#popUpM-agentId').append(option);
	 }
    
    
}

// 스케줄러 수정폼 값 셋팅
function fn_setAlgoMgmtRow(data){
	
    var resultData = data.result;
    var job_id      = resultData.job_id;      // 스케줄러(잡)ID
    var job_nm      = resultData.job_nm;      // 스케줄명  에이전트
    var job_desc    = resultData.job_desc;    // 스케줄러 설명
    var table_nm    = resultData.table_nm;    // 테이블 명
  	var jobType    = resultData.job_type;            // 작업분류
    var etl_tgt_sys = resultData.etl_tgt_sys; // ETL 대상 시스템명
    var etl_tgt_tbl = resultData.etl_tgt_tbl; // ETL 대상 테이블명
    var cron_expr   = resultData.cron_expr;   // 실행주기(cron 표현식)
    var exec_prog   = resultData.exec_prog;   // 실행프로그램 경로
    var param_str   = resultData.param_str;   // 전달 파라미터
    var acvt_yn = resultData.acvt_yn;	// 활성화 여부 
    var agent_id = resultData.agent_id;
    
    // 에이전트 select 태그 생성
    var agentList = data.agentList;
    for(var i=0; i<agentList.length; i++){
		var id = agentList[i].agent_id;
		if(agent_id == id){
			var option = $("<option selected value="+ id+">" + id +"</option>");
		}else{
			var option = $("<option value="+ id+">" + id +"</option>");
		}
		$('#popUpM-agentId').append(option);
	 }
    
    // 라디오 버튼 값 체크
    if(jobType == 'ETL'){
	  	$('#popUpM-ETL').attr('checked', 'checked');
    }else{
    	$('#popUpM-EXEC').attr('checked', 'checked');
    }
    
    if(acvt_yn == 'Y'){
    	$('#popUpM-on').attr('checked', 'checked');
    }else{
    	$('#popUpM-off').attr('checked', 'checked');
    }
    
    $('#popUpM-job_id').val(job_id);          // 수정폼 히든태그
    $('#popUpM-jobNm').val(job_nm);
    $('#popUpM-jobDesc').val(job_desc);
    $('#popUpM-etlSys').val(etl_tgt_sys);
    $('#popUpM-etlTbl').val(etl_tgt_tbl);
    $('#popUpM-cronExpr').val(cron_expr);
    $('#popUpM-execProg').val(exec_prog);
    $('#popUpM-paramStr').val(param_str);
    

}

function fn_setAlgoRedoRow(data,status){
	
    var resultData = data.result;
    var job_id      = resultData.job_id;      // 스케줄러(잡)ID
    var job_nm      = resultData.job_nm;      // 스케줄명  에이전트
    var job_desc    = resultData.job_desc;    // 스케줄러 설명
    var etl_tgt_sys = resultData.etl_tgt_sys; // ETL 대상 시스템명
    var etl_tgt_tbl = resultData.etl_tgt_tbl; // ETL 대상 테이블명
    var cron_expr   = resultData.cron_expr;   // 실행주기(cron 표현식)
  	var agent_id    = resultData.agent_id;    // 에이전트 ID
    var exec_prog   = resultData.exec_prog;   // 실행프로그램 경로
    var param_str   = resultData.param_str;   // 전달 파라미터
    var jobType = resultData.job_type;
    var agent_id = resultData.agent_id;
    var lst_chg_dat_cond = resultData.lst_chg_dat_cond;
//     if(resultData.lst_chg_dat_cond != null){
// 	    var lst_chg_dat_cond = resultData.lst_chg_dat_cond;    	
//     }
    
    
 // 에이전트 select 태그 생성
    var agentList = data.agentList;
    for(var i=0; i<agentList.length; i++){
		var id = agentList[i].agent_id;
		if(agent_id == id){
			var option = $("<option selected value="+ id+">" + id +"</option>");
		}else{
			var option = $("<option value="+ id+">" + id +"</option>");
		}
		$('#popUpP-agentId2').append(option);
	 }
    
    // 라디오 버튼 값 체크
    if(jobType == 'ETL'){
	  	$('#popUpP-ETL').attr('checked', 'checked');
    }else{
    	$('#popUpP-EXEC').attr('checked', 'checked');
    }
    
    $('#popUpP-job_id').val(job_id);          // 수정폼 히든태그
    $('#popUpP-jobNm').val(job_nm);
    $('#popUpP-jobDesc').val(job_desc);
    $('#popUpP-etlSys').val(etl_tgt_sys);
    $('#popUpP-etlTbl').val(etl_tgt_tbl);
    $('#popUpP-cronExpr').val(cron_expr);
  	$('#popUpP-agent_id').val(agent_id);    // 에이전트 아이디 
    $('#popUpP-execProg').val(exec_prog);
    $('#popUpP-paramStr').val(param_str);
    $('#popUpP-job_type').val(jobType);
    $('#popUpP-lst_chg_dat_cond').val(lst_chg_dat_cond);
//     if(lst_chg_dat_cond != null){
// 	    $('#popUpP-lst_chg_dat_cond').val(lst_chg_dat_cond);
//     }
}

// 시스템명 체크 메서드
function checkFn(obj) {
    var value = $('#etl_sys_check').is(':checked');
    if (obj == 'HODS'){
    	var chk = $("#etl_hods_check").prop("checked");
    	if(chk){
    		$("#etl_hods_check").prop("checked", false); 
    	}else{
    		$("#etl_hods_check").prop("checked", true);
    	}
    }else if(obj == 'HMART'){
    	var chk = $("#etl_hmart_check").prop("checked");
    	if(chk){
    		$("#etl_hmart_check").prop("checked", false); 
    	}else{
    		$("#etl_hmart_check").prop("checked", true);
    	}
    }else if(obj == 'HGCON'){
    	var chk = $("#etl_hcgon_check").prop("checked");
    	if(chk){
    		$("#etl_hcgon_check").prop("checked", false); 
    	}else{
    		$("#etl_hcgon_check").prop("checked", true);
    	}
    }else if(obj == 'HCAMP'){
    	var chk = $("#etl_hcamp_check").prop("checked");
    	if(chk){
    		$("#etl_hcamp_check").prop("checked", false); 
    	}else{
    		$("#etl_hcamp_check").prop("checked", true);
    	}
    }else{
    	alert("시스템 명 체크 중 오류 발생");
    }
}

//------------------------------------------------------------------------
// -----  Validations -----------------------------------------------
//------------------------------------------------------------------------

function initFormValues(){

	// 등록폼 초기화
    $('#popUpA-jobNm').val('');
    $('#popUpA-jobDesc').val('');
    $('#popUpA-etlSys').val('');
    $('#popUpA-etlTbl').val('');
    $('#popUpA-cronExpr').val('');
    $('#popUpA-execProg').val('');
    $('#popUpA-paramStr').val('');
	var selectTag = document.getElementById("popUpA-agentId");
	for(var i=0; i<selectTag.options.length; i++){
		selectTag.options[i] = null;
	}
	selectTag.options.length = 0;
	
	
	// 수정폼 초기화
    $('#popUpM-jobNm').val('');
    $('#popUpM-jobDesc').val('');
    $('#popUpM-etlSys').val('');
    $('#popUpM-etlTbl').val('');
    $('#popUpM-cronExpr').val('');
    $('#popUpM-execProg').val('');
    $('#popUpM-paramStr').val('');
    $('#popUpM-job_id').val('');           
	var selectTag = document.getElementById("popUpM-agentId");
	for(var i=0; i<selectTag.options.length; i++){
		selectTag.options[i] = null;
	}
	selectTag.options.length = 0;
	
    // 수동실행폼 초기화
    $('#popUpP-jobNm').val('');
    $('#popUpP-jobDesc').val('');
    $('#popUpP-etlSys').val('');
    $('#popUpP-etlTbl').val('');
    $('#popUpP-cronExpr').val('');
    $('#popUpP-agentId2').val('');
    $('#popUpP-execProg').val('');
    $('#popUpP-paramStr').val('');       
    
	// 팝업 Select 태그 초기화
	var selectTag = document.getElementById("popUpP-agentId2");
    
	for(var i=0; i<selectTag.options.length; i++){
		selectTag.options[i] = null;
	}
	selectTag.options.length = 0;
	
	// 스키마 단위 ETL 수동실행 폼 체크박스 초기화
	$("#etl_hods_check").prop("checked", false); 
	$("#etl_hmart_check").prop("checked", false); 
	$("#etl_hcgon_check").prop("checked", false); 
	$("#etl_hcamp_check").prop("checked", false); 
}

//------------------------------------------------------------------------
//---- submit ------------------------------------------------------------
//------------------------------------------------------------------------
function linkPage(pageNo) {
    $("#currentPageNo").val(pageNo);

    $("#listForm").attr({
        action : "schedulerList.do",
        method : "post"
    }).submit();
}

function goOrder(order) {
    $("#listOrder").val(order);

    $("#listForm").attr({
        action : "schedulerList.do",
        method : "post"
    }).submit();
}

function fn_delete(){
	var param = new Object;
	param.job_id = $('#popUpM-job_id').val();
    	var url = controller + '/schedulerDelete.do';
    	
        if (confirm("삭제 하시겠습니까?")) {
            $.ajax({
                type: 'POST',
                url: url,
                data: param,
                crossDomain:true,
                async:false,
                success:function(resultData){
                    if(resultData.result == 1){
                     	schedulerApply();
                        location.reload();
                    }
                },
                error:function(e){
                    alert('서버와의 통신에 실패하였습니다.');
                }
            });
       }
        
        
}

</script>


<div class="mConBox">
	<div class="mCont">
		<div class="row">
			<div class="col-md-4" id="subtitleA">
				<h2>
					<i class="ti-layers"></i> 사용자메뉴 권한관리
				</h2>
			</div>

			<div class="col-md-8">
				<div class="pageRoute">
					<ul>
						<li><a href="#">HOME</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li><a href="#">시스템 관리</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li><a href="#">스케줄러 관리</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li class="current"><a href="#">스케줄러 관리</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end row -->

		<form:form modelAttribute="schedulerParam" id="listForm" method="get">
			<div class="row">
				<div class="col-md-12">
					<div class="conBox" style="min-height: 400px;">

						<div class="row">
							<div class="col-md-33 select-set-3">
								<p>
									총 <b>${schedulerParam.totalRecordCount}</b> 개
								</p>
							</div>
							<div class="col-md-9 text-right select-sys-1">
								<form:select path="recordCountPerPage" onChange="linkPage(1);">
									<form:option value="10">10개씩 보기</form:option>
									<form:option value="20">20개씩 보기</form:option>
									<form:option value="50">50개씩 보기</form:option>
									<form:option value="100">100개씩 보기</form:option>
								</form:select>
								<form:select id="optSearchYn" path="searchOptionYn">
									<form:option value="">활성화 여부</form:option>
									<form:option value="Y">활성화</form:option>
									<form:option value="N">비활성화</form:option>
								</form:select>
								<form:select id="optSearchStat" path="searchOptionStat">
									<form:option value="">상태코드</form:option>
									<form:option value="S">메시지전송</form:option>
									<form:option value="W">실행대기중</form:option>
									<form:option value="R">실행중</form:option>
									<form:option value="F">실행완료</form:option>
									<form:option value="E">오류발생</form:option>
								</form:select>
								<form:select id="optSearchNm" path="searchOptionNm">
									<form:option value="">분류</form:option>
									<form:option value="S">스케줄러 명</form:option>
									<form:option value="A">에이전트 명</form:option>
								</form:select>
								<form:input path="searchText" type="text" id="inpSearch"
									class="searchTxt" placeholder="검색어를 입력하세요" />
								<button type="button" id="btnSearch" class="searchBtn">
									<i class="ti-search"></i> 검색
								</button>
								<button type="button" title="사용자 메뉴 권한을 추가할 수 있습니다."
									id="btnPopUpAdd" class="AddDefaultBtn">
									<i class="fa fa-plus"></i> 스케줄러 등록
								</button>
								<button type="button" id="btnPopUpetl" class="AddDefaultBtn"><i class="fa fa-plus"></i>스키마 단위 ETL 실행</button>
							</div>
						</div>
						<!-- end row > conBox > col-md-12 > row > form -->

						<div class="row">
							<div class="col-md-12">
								<table class="viewDateTbl menuSetTbl" id="menuSetTbl">
									<colgroup>
										<col width="100">
										<col width="140">
										<col width="140">
										<col width="150">
										<col width="300">
										<col width="150">
										<col width="140">
										<col width="140">
										<col width="120">
										<col width="120">
										<col width="120">
									</colgroup>
									<thead>
										<tr>
											<th>잡아이디</th>
											<th>스케줄러명</th>
											<th>에이전트명</th>
											<th>실행주기</th>
											<th>설명</th>
											<th>최종실행시각</th>
											<th>최종실행결과</th>
											<th>등록일자</th>
											<th>활성여부</th>
											<th>수동실행</th>
											<th>상세관리</th>
										</tr>
									</thead>
									<c:if test="${empty schedulerList}">
										<tbody>
											<tr>
												<td colspan="9">No data available in table</td>
											</tr>
										</tbody>
									</c:if>
									<c:if test="${not empty schedulerList}">
										<c:forEach var="list" items="${schedulerList}" varStatus="status">
											<c:if test="${status.first}">
												<tbody class="tbGroup">
											</c:if>
											<tr id="${list.job_id}">
											<td>${list.job_id}</td>
											<td>${list.job_nm}</td>
											<td class="txtL">${list.agent_id}</td>
											<td class="txtL">${list.cron_expr}</td>
											<td>${list.job_desc}</td>
											<c:if test="${not empty list.end_ddtm}">
												<td>${list.end_ddtm}</td>
											</c:if>
											<c:if test="${empty list.end_ddtm}">
												<td></td>
											</c:if>
											<c:choose>
												<c:when test="${list.stts_cd eq 'S'}">
													<td>메시지전송</td>
												</c:when>
												<c:when test="${list.stts_cd eq 'W'}">
													<td>실행대기중</td>
												</c:when>
												<c:when test="${list.stts_cd eq 'R'}">
													<td>실행중</td>
												</c:when>
												<c:when test="${list.stts_cd eq 'F'}">
													<td>실행완료</td>
												</c:when>
												<c:when test="${list.stts_cd eq 'E'}">
													<td>오류발생</td>
												</c:when>
												<c:otherwise>
													<td></td>
												</c:otherwise>
											</c:choose>
											
											<td>${list.reg_ddtm}</td>
											<c:if test="${list.acvt_yn eq 'Y'}">
												<td>활성화</td>
											</c:if>
											<c:if test="${list.acvt_yn eq 'N'}">
												<td>비활성화</td>
											</c:if>
											<td><button type="button" class="tblInBtn btnRedo" >실행</button></td>
											<td><button type="button" class="tblInBtn btnModifyRow">수정</button></td>
											</tr>
										<c:if test="${status.last}">
											</tbody>
										</c:if>
									</c:forEach>
									</c:if>
								</table>
							</div>
						</div>
						<!-- end row -->

						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8 pageNum">
								<!-- Paging With Taglib -->
								<ui:pagination paginationInfo="${schedulerParam}" type="css" jsFunction="linkPage" />
								<form:hidden path="currentPageNo" />
							</div>
						</div>
						<!-- end row -->
					</div>
					<!-- end conBox -->
				</div>
				<!-- end col-md-12 -->
			</div>
			<!-- end row -->
		</form:form>
	</div>
	<!-- end mCont -->
</div>
<!-- end mConBox -->


<div id="divPopUpA-AlgoMgmt" class="popUp col-md-8">
    <div class="popUpTop">스케줄러 등록
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="schedulerInsert.do" method="POST" modelAttribute="scheduler" id="addForm">
                <table class="popInTbl">
                    <colgroup>
                        <col width="30%"/>
                        <col width="70%"/>
                    </colgroup>
                    <tr>
                        <th><label for="popUpA-jobNm">잡 이름</label></th>
                        <td>
                            <input type="text" id="popUpA-jobNm" class="w100p" name="job_nm" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-agentId">에이전트 아이디</label></th>
                        <td>
                            <select id="popUpA-agentId" name="agent_id">
	                        </select>
                        </td>
                    </tr> 
                    <tr>
                        <th><label for="popUpA-jobDesc">설명</label></th>
                        <td>
                            <input type="text" id="popUpA-jobDesc" class="w100p" name="job_desc" value=""/>
                        </td>
                    </tr>
                    <tr title="* : 매분 매초 매분 등 모든 값을 뜻함.
? : 특정한 값이 없음을 뜻함.
- : 범위를 뜻함 (예) 월-수 = MON-WED.
, : 특별한 값일 때만 동작 (예) 월,수,금 MON,WED,FRI
/ : 시작시간 /단위 (예) 0분부터 매 5분 0/5
L : 일에서 사용하면 마지막 일, 요일에서 사용하면 마지막 요일(토요일)
W : 가장 가까운 평일 (예) 15W는 15일에서 가장 가까운 평일 (월~금)을 찾음
# : 몇째주의 무슨 요일을 표현 (예) 3#2 : 2번째 수요일">
                        <th><label for="popUpA-cronExpr">실행주기(cron 표현식)</label></th>
                        <td>
                            <input type="text" id="popUpA-cronExpr" class="w100p" name="cron_expr" />
                        </td>
                    </tr>    
                    <tr>
                        <th><label for="popUpA-execProg">실행프로그램 경로 (해당서버)</label></th>
                        <td>
                            <input type="text" id="popUpA-execProg" class="w100p" name="exec_prog" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-paramStr">전달 파라미터</label></th>
                        <td>
                            <input type="text" id="popUpA-paramStr" class="w100p" name="param_str" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-etlSys">ETL 대상 시스템명</label></th>
                        <td>
                            <input type="text" id="popUpA-etlSys" class="w100p" name="etl_tgt_sys" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-etlTbl">ETL 대상 테이블명</label></th>
                        <td>
                            <input type="text" id="popUpA-etlTbl" class="w100p" name="etl_tgt_tbl" />
                        </td>
                    </tr>        
                    <tr>
                        <th><label for="">활성화 여부</label></th>
                        <td>
                            <label for="popUpA-on">
                                <input type="radio" id="popUpA-on" name="acvt_yn" value="Y" />활성화
                            </label>
                            <label for="popUpA-off">
                                <input type="radio" id="popUpA-off" name="acvt_yn" value="N" />비활성화
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="">작업 분류</label></th>
                        <td>
                            <label for="popUpA-ETL">
                                <input type="radio" id="popUpA-ETL" name="job_type" value="ETL" />ETL
                            </label>
                            <label for="popUpA-EXEC">
                                <input type="radio" id="popUpA-EXEC" name="job_type" value="EXEC" />EXEC
                            </label>
                        </td>
                    </tr>
                    <input type="hidden" id="popUpA-reg_id" name="reg_id" value="${sessionScope.userInfo.userId}" />
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnA-insert" class="btn-style1 darkGrayBtn">저장</button>
            <button type="button" id="btnA-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>

<div id="divPopUpM-AlgoMgmt" class="popUp col-md-8">
    <div class="popUpTop">스케줄러 수정
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="schedulerUpdate.do" method="POST" modelAttribute="scheduler" id="modifyForm">
                <input type="hidden" id="popUpM-job_id" name="job_id" value="" />
                <input type="hidden" id="popUpA-reg_id" name="udt_id" value="${sessionScope.userInfo.userId}" />
                <table class="popInTbl">
                    <colgroup>
                        <col width="30%"/>
                        <col width="70%"/>
                    </colgroup>
                    <tr>
                        <th><label for="popUpM-jobNm">잡 이름</label></th>
                        <td>
                            <input type="text" id="popUpM-jobNm" class="w100p" name="job_nm" value="" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-agentId">에이전트</label></th>
                        <td>
                            <select id="popUpM-agentId" name='agent_id' >
	                        </select>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-jobDesc">설명</label></th>
                        <td>
                            <input type="text" id="popUpM-jobDesc" class="w100p" name="job_desc" value="" />
                        </td>
                    </tr>
                    <tr title="* : 매분 매초 매분 등 모든 값을 뜻함.
? : 특정한 값이 없음을 뜻함.
- : 범위를 뜻함 (예) 월-수 = MON-WED.
, : 특별한 값일 때만 동작 (예) 월,수,금 MON,WED,FRI
/ : 시작시간 /단위 (예) 0분부터 매 5분 0/5
L : 일에서 사용하면 마지막 일, 요일에서 사용하면 마지막 요일(토요일)
W : 가장 가까운 평일 (예) 15W는 15일에서 가장 가까운 평일 (월~금)을 찾음
# : 몇째주의 무슨 요일을 표현 (예) 3#2 : 2번째 수요일">
                        <th><label for="popUpM-cronExpr">실행주기(cron 표현식)</label></th>
                        <td>
                            <input type="text" id="popUpM-cronExpr" class="w100p" name="cron_expr" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-execProg">실행프로그램 경로 (해당서버)</label></th>
                        <td>
                            <input type="text" id="popUpM-execProg" class="w100p" name="exec_prog" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-paramStr">전달 파라미터</label></th>
                        <td>
                            <input type="text" id="popUpM-paramStr" class="w100p" name="param_str" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-etlSys">ETL 대상 시스템명</label></th>
                        <td>
                            <input type="text" id="popUpM-etlSys" class="w100p" name="etl_tgt_sys" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-etlTbl">ETL 대상 테이블명</label></th>
                        <td>
                            <input type="text" id="popUpM-etlTbl" class="w100p" name="etl_tgt_tbl" />
                        </td>
                    </tr>  
                    <tr>
                        <th><label for="">활성화 여부</label></th>
                        <td>
                            <label for="popUpM-on">
                                <input type="radio" id="popUpM-on" name="acvt_yn" value="Y" />활성화
                            </label>
                            <label for="popUpM-off">
                                <input type="radio" id="popUpM-off" name="acvt_yn" value="N" />비활성화
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="">작업 분류</label></th>
                        <td>
                            <label for="popUpM-job_type_etl">
                                <input type="radio" id="popUpM-ETL" name="job_type" value="ETL" />ETL
                            </label>
                            <label for="popUpM-job_type_exec">
                                <input type="radio" id="popUpM-EXEC" name="job_type" value="EXEC" />EXEC
                            </label>
                        </td>
                    </tr>
                                        
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
        	<button type="button" id="btnM-delete" class="" onclick = "fn_delete();">삭제</button>
            <button type="button" id="btnM-update" class="btn-style1 darkGrayBtn">수정</button>
            <button type="button" id="btnM-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>



<div id="divPopUpP-AlgoMgmt" class="popUp col-md-8">
    <div class="popUpTop">스케줄러 수동 실행
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="schedulerRedo.do" method="POST" modelAttribute="scheduler" id="redoForm">
                <input type="hidden" id="popUpP-job_id" name="job_id" value="" />
                <input type="hidden" id="popUpP-agent_id" name="agent_id" value=""/>
                <input type="hidden" id="popUpP-lst_chg_dat_cond" name="lst_chg_dat_cond" value=""/>
                <input type="hidden" id="popUpP-job_type" name="job_type" value=""/>
                <table class="popInTbl">
                    <colgroup>
                        <col width="30%"/>
                        <col width="70%"/>
                    </colgroup>
                    <tr>
                        <th><label for="popUpP-jobNm">스케줄러 명</label></th>
                        <td>
                            <input type="text" id="popUpP-jobNm" class="w100p" name="job_nm" value="" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpP-jobDesc">설명</label></th>
                        <td>
                            <input type="text" id="popUpP-jobDesc" class="w100p" name="job_desc" value="" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <th>작업 분류</th>
                        <td>
                            <label for="popUpM-on">
                                <input type="radio" id="popUpP-ETL" name="job_type" value="Y" disabled="disabled" />ETL
                            </label>
                            <label for="popUpM-off">
                                <input type="radio" id="popUpP-EXEC" name="job_type" value="N" disabled="disabled" />EXEC
                            </label>
                        </td>
                    </tr>                    
                    <tr>
                        <th><label for="popUpP-etlSys">ETL 대상 시스템명</label></th>
                        <td>
                            <input type="text" id="popUpP-etlSys" class="w100p" name="etl_tgt_sys" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpP-etlTbl">ETL 대상 테이블명</label></th>
                        <td>
                            <input type="text" id="popUpP-etlTbl" class="w100p" name="etl_tgt_tbl"  readonly/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpP-cronExpr">실행주기(cron 표현식)</label></th>
                        <td>
                            <input type="text" id="popUpP-cronExpr" class="w100p" name="cron_expr" readonly/>
                        </td>
                    </tr>                                        
                    <tr>
                        <th><label for="popUpP-agentId">에이전트</label></th>
                        <td>
                            <select id="popUpP-agentId2" name='agent_id' disabled="disabled">
	                        </select>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpP-execProg">실행프로그램 경로 (해당서버)</label></th>
                        <td>
                            <input type="text" id="popUpP-execProg" class="w100p" name="exec_prog" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpP-paramStr">전달 파라미터</label></th>
                        <td>
                            <input type="text" id="popUpP-paramStr" class="w100p" name="param_str" />
                        </td>
                    </tr>                    
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnP-redo" class="btn-style1 darkGrayBtn">실행</button>
            <button type="button" id="btnP-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>



<!-- 스키마 단위 ETL 수동실행 -->
<div id="divPopUpE-AlgoMgmt" class="popUp col-md-8">
    <div class="popUpTop"> 스키마 단위 ETL 수동실행
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="schedulerEtl.do" method="POST" modelAttribute="scheduler" id="etlForm">
                <table class="popInTbl">
                    <colgroup>
                        <col width="30%"/>
                        <col width="70%"/>
                    </colgroup>
                    <tr>
                        <th><label>시스템 명</label></th>
                        <td>
                        <input type="checkbox" id="etl_hods_check" name="etl_tgt_syss" value="HODS" class="rdChk">
                        <label class="chkSel pullLeft" for="checkUseYn" onclick="checkFn('HODS');"><span>HODS</span></label>
                        <br>
                        
                        <input type="checkbox" id="etl_hmart_check" name="etl_tgt_syss" value="HMART" class="rdChk">
                        <label class="chkSel pullLeft" for="checkUseYn" onclick="checkFn('HMART');"><span>HMART</span></label>
                        <br>
                        
                        <input type="checkbox" id="etl_hcamp_check" name="etl_tgt_syss" value="HCAMP" class="rdChk">
                        <label class="chkSel pullLeft" for="checkUseYn" onclick="checkFn('HCAMP');"><span>HCAMP</span></label>
                        <br>
                        
                        <input type="checkbox" id="etl_hcgon_check" name="etl_tgt_syss" value="HGCON" class="rdChk">
                        <label class="chkSel pullLeft" for="checkUseYn" onclick="checkFn('HGCON');"><span>HCGON</span></label>
						</td>
                    </tr>
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnE-etl" class="btn-style1 darkGrayBtn">실행</button>
            <button type="button" id="btnE-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>


