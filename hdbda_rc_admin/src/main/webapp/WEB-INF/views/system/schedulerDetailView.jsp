<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>


<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>초록마을 제품추천</title>

    <!-- Bootstrap -->
    <link href="/css/dateArea.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-theme.css" rel="stylesheet">
    <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/daterangepicker.css" rel="stylesheet">
    <link href="/fonts/font-awesome.css" rel="stylesheet">
    <link href="/fonts/font-awesome.min.css" rel="stylesheet">
    <link href="/css/offset.css" rel="stylesheet">
    <link href="/css/popup.css" rel="stylesheet">
    <link href="/css/res.css" rel="stylesheet">
    <link href="/css/res_algo.css" rel="stylesheet">
    <link href="/css/spectrum.css" rel="stylesheet">
    <link href="/css/themify-icons.css" rel="stylesheet">
	
<!--     <link rel="shortcut icon" href="/img/favicon.ico"> -->



    <script type="text/javascript" src="/js/jquery-1.12.4.js"></script>
	<!-- 2017-05-12 admin 관련 js 제거
	<script type="text/javascript" src="/js/adm_doma.js"></script>
	-->
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/custom.js"></script>
    <script type="text/javascript" src="/js/moment.js"></script>
    <script type="text/javascript" src="/js/daterangepicker.js"></script>
    <script type="text/javascript" src="/js/daterangepicker_unlimit.js"></script>

	<script type="text/javascript" src="/js/pageSet.js"></script>
	<script type="text/javascript" src="/js/hashMap.js"></script>
	<script type="text/javascript" src="/js/spectrum.js"></script>

    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/util_ajax.js"></script>
    <script type="text/javascript" src="/js/util_date.js"></script>
    <script type="text/javascript" src="/js/util_prototype.js"></script>
    <script type="text/javascript" src="/js/util_validate.js"></script>

    <!-- chart js -->
    <script type="text/javascript" src="/js/echarts/echarts.js"></script>
    <script type="text/javascript" src="/js/charts.js"></script>

</head>
<script>
	function window_close(){
		self.close();
	}
</script>

<html>
<body>
<!-- <div id="divPopUpP-AlgoMgmt" class="popUp col-md-8"> -->
    <div class="popUpTop">스케줄러 상세보기
<!--         <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span> -->
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="schedulerRedo.do" method="POST" modelAttribute="scheduler" id="redoForm">
                <input type="hidden" id="popUpP-job_id" name="job_id" value="" />
                <input type="hidden" id="popUpP-agent_id" name="agent_id" value=""/>
                <input type="hidden" id="popUpP-lst_chg_dat_cond" name="lst_chg_dat_cond" value=""/>
                <input type="hidden" id="popUpP-job_type" name="job_type" value=""/>
                <table class="popInTbl">
                    <colgroup>
                        <col width="30%"/>
                        <col width="70%"/>
                    </colgroup>
                    <tr>
                        <th><label for="popUpP-jobNm">스케줄러 명</label></th>
                        <td>
                            <input type="text" id="popUpP-jobNm" class="w100p" name="job_nm" value="${result.job_nm}" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpP-jobDesc">설명</label></th>
                        <td>
                            <input type="text" id="popUpP-jobDesc" class="w100p" name="job_desc" value="${result.job_desc}" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="">작업 분류</label></th>
						<c:if test="${result.job_type eq 'ETL'}">
							<td>
                            <label for="popUpM-on">
                                <input type="radio" id="popUpP-ETL" name="job_type" value="Y" checked="checked" disabled="disabled" />ETL
                            </label>
                            <label for="popUpM-off">
                                <input type="radio" id="popUpP-EXEC" name="job_type" value="N" disabled="disabled" />EXEC
                            </label>
                        </td>
						</c:if>
						<c:if test="${result.job_type eq 'EXEC'}">
						<td>
                            <label for="popUpM-on">
                                <input type="radio" id="popUpP-ETL" name="job_type" value="Y"  disabled="disabled" />ETL
                            </label>
                            <label for="popUpM-off">
                                <input type="radio" id="popUpP-EXEC" name="job_type" value="N" checked="checked" disabled="disabled" />EXEC
                            </label>
                        </td>
                        </c:if>
                    </tr>                    
                    <tr>
                        <th><label for="popUpP-etlSys">ETL 대상 시스템명</label></th>
                        <td>
                            <input type="text" id="popUpP-etlSys" class="w100p" name="etl_tgt_sys" value="${result.etl_tgt_sys}" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpP-etlTbl">ETL 대상 테이블명</label></th>
                        <td>
                            <input type="text" id="popUpP-etlTbl" class="w100p" name="etl_tgt_tbl" value="${result.etl_tgt_tbl}" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpP-cronExpr">실행주기(cron 표현식)</label></th>
                        <td>
                            <input type="text" id="popUpP-cronExpr" class="w100p" name="cron_expr" value="${result.cron_expr}" readonly/>
                        </td>
                    </tr>                                        
                    <tr>
                        <th><label for="popUpP-agentId">에이전트</label></th>
                        <td>
                            <select id="popUpP-agentId2" name='agent_id' disabled="disabled">
                            	<option>"${result.agent_id}"</option>
	                        </select>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpP-execProg">실행프로그램 경로 (해당서버)</label></th>
                        <td>
                            <input type="text" id="popUpP-execProg" class="w100p" name="exec_prog" value="${result.exec_prog}" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpP-paramStr">전달 파라미터</label></th>
                        <td>
                            <input type="text" id="popUpP-paramStr" class="w100p" name="param_str" value="${result.param_str}" readonly/>
                        </td>
                    </tr>                    
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnP-cancel" class="btn-style1 lightGrayBtn popCancel" onclick='window_close()' >확인</button>
        </div>
    </div>
</body>
</html>
