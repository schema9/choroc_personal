<%------------------------------------------------------------------------
- Menu  : 시스템 관리 > 메타 관리 > 메타 리스트
- ID    : ADM_META_001
- Ref.  : -
- URI   : -
- History
- 2018-11-29 created by ljy
------------------------------------------------------------------------%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<script type="text/javascript">
var controller = '/administrator/systemManager';
var columnSetRow = 0;
var columnSetRowIndex = 0;
var cssSet = "";
var maxLengChkObj={};
var chkType="";
$(window).ready(function(){
    pageEvents();
});

function pageEvents(){
    // close & cancel buttons
    $('#btnM-cancel, #btnA-cancel, .closePop i').unbind('click').bind('click',function(){
    	$("#popUpA-userId").removeAttr('disabled');
    	fn_util_divPopUpControl($(this).parent().parent().parent().attr('id'),false);
        initFormValues();
    });

    // add button
    $('#btnPopUpAdd').unbind('click').bind('click',function(){
    	$("#chgDatBasCol-I").val("");
    	$("#columnSetBodyMod").empty();	
    	$("#columnSetBody").empty();
    	$("#columnSetBodyFix").empty();
    	columnSetRow = 0;
    	columnSetRowIndex = 0;
        fn_util_divPopUpControl('divPopUpA-UserAccount',true);
//         $('#popUpA-domainNm').text( $('#domainNm-h').val() );
    });


    $('.userModifySet').unbind('click').bind('click',function(){
    	$("#chgDatBasCol").val("");
    	var str = $(this).parent().parent().attr('id');
    	var strArr = str.split('|');
    	var sysNm = strArr[0];//${list.sysNm}
    	var enTblNm = strArr[1];
    	var krTblNm = strArr[2];
    	var loadType = strArr[3];
    	var chgDatBasCol = strArr[4];
    	$('#popUpA-sysNm-mod').val(sysNm);
    	 $('#popUpA-enTblNmd-mod').val(enTblNm);
    	 $('#popUpA-krTblNm-mod').val(krTblNm);
    	 $('#chgDatBasCol').val(chgDatBasCol);
    	 $('#popUpA-loadTypeModA-mod').prop('checked',false);
    	 $('#popUpA-loadTypeModU-mod').prop('checked',false);
    	 $('#popUpA-loadTypeModT-mod').prop('checked',false);
    	 if (loadType == "A") {
    		 $('#popUpA-loadTypeModA-mod').prop('checked',true);
    	 }
    	 if (loadType == "U") {
    		 $('#popUpA-loadTypeModU-mod').prop('checked',true);
    	 }
    	 if (loadType == "T") {
    		 $('#popUpA-loadTypeModT-mod').prop('checked',true);
    	 }
    	 //$('#popUpA-loadTypeModA-mod').val(loadType);
    	fn_MetaDataDetailInfo();
    	
    	
    });
    
    // In Modify form - save button
    $('#btnM-update').unbind('click').bind('click',function(){
    	fn_saveForm("메타데이터 수정을 완료하였습니다.","데이터 수정에 실패했습니다.","M");
    });

    // In Modify form - reload button
    $('#btnM-reload').unbind('click').bind('click',function(){
    	fn_saveForm("재적재를 실행합니다.","재적재에 실패했습니다.","R");
    });
    
   	// In DEL form - save button
    $('#btnM-delete').unbind('click').bind('click',function(){
        fn_saveForm("메타데이터 삭제를 완료하였습니다.","데이터 삭제에 실패했습니다.","D");
    });

    // In Add form - save button
    $('#btnA-insert').unbind('click').bind('click',function(){
    	fn_saveForm("메타데이터 등록을 완료하였습니다.","데이터 등록에 실패했습니다.","I");
    });
    
    // Event for click SEARCH button
    $('#btnSearch').unbind('click').bind('click',function(){
        searchList();
    });

    // In modify form - check box
    $('#popUpM-tbodyData').find('input[type=checkbox]').unbind('change').bind('change',function(){
        if( $('#popUpM-tbodyData').find('.rdChk').length == $('#popUpM-tbodyData').find('.rdChk:checked').length ){
            $(this).closest('table').find('thead').find('input[type=checkbox]').prop('checked',true);
        }else{
            $(this).closest('table').find('thead').find('input[type=checkbox]').prop('checked',false);
        }
    });
}

//------------------------------------------------------------------------
//----- Connection -------------------------------------------------
//------------------------------------------------------------------------

function fn_MetaDataDetailInfo() {
	var url = controller + '/systemMetaDataDetail.do';
    var param = {
    		'sysNm':$('#popUpA-sysNm-mod').val() ,
    		'enTblNm':$('#popUpA-enTblNmd-mod').val() , 
    		'RecordCountPerPage':1,
    		'pageSize':10
    	};
    var succMs = "테이블이 조회 되었습니다.";
    var errorMs = "시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.";
    var netErrMs = "네트워크 통신 에러";
    fn_MetaDataDetail(param, url, succMs, errorMs, netErrMs);
}

function fn_MetaDataDetail(param, url, succMs, errorMs, netErrMs) {
	 $.ajax({
        type: 'POST',
        url: url, 
        data:param,
//         dataType:'JSON',
        async:false,
        success: function(data){
	        	if (data.messageCode=="OK") {
	        		$("#columnSetBodyMod").empty();
	        		columnSetRow = 0;
	            	columnSetRowIndex = 0;
	        		for (i=0;i<data.result.length;i++){
	        		//	data.result[i]
	        			paramModList(data.result[i],i);
	        		}
	        		$("#columnSetBodyMod").append(cssSet);
	        		$("input[type=checkbox]").click(function(){
	        			id=$(this).attr('id');
	                	//alert($(this).attr('id')+"1"+$(this).is(":checked"));
	        			if ($(this).is(":checked")==true){
	                		$("#"+id).val("Y");
	                		$("#org_"+id).val("Y");
	                		
	                	//	alert(id+":"+$("#"+id).val());
	                	} else {
	                		$("#"+id).val("N");
	                		$("#org_"+id).val("N");
	                	//	alert(id+":"+$("#"+id).val());
	                	}
	                });
	        		
	        		fn_util_divPopUpControl('divPopUpM-UserAccount',true);
	        		alert("메타 변경이 있을경우 해당 테이블의 데이터를 전체 재적재해야 합니다");
            } else {
            	alert(errorMs);	
            }
        },
        error : function(e){
        	alert(netErrMs+": "+e);
        }
    });
}

function paramModList(data, idx){
        var paramHtml = "";
        var selected = "";
        var seqNum = parseInt(idx)+1;
        var LoadTypeVal = $("#modifyForm").find("[name=loadType]:checked").val();	//체크된 적재타입 호출
        if (data.krColNm == null) data.krColNm = "";	//컬럼명 국문 null값 정리
        if (data.hvdatType == null) data.hvdatType = "";	//하이브데이터타입  null값 정리
        if (data.kdDatType == null) data.kdDatType = "";	//kudu데이터타입 null값 정리
        if (data.pkOrdr == null) data.pkOrdr = "";	//PK순서 null값 정리
        if (data.ptntColConvSntx == null) data.ptntColConvSntx = "";	//파티션 컬럼 변환식 null값 정리
        paramHtml +='<tr id="rowId'+columnSetRow+'">';
        paramHtml +='<td style="text-align:center"><input type="checkbox" name="chkColumnMod" value="'+idx+'"/></td>';
        paramHtml +='<td><input type="text" id="colOrdrMod'+idx+'" name="colOrdr" value='+data.colOrdr+' size="1" readonly /></td>';
        paramHtml +='<td><input type="text" id="enColNmMod'+idx+'" name="enColNm" value="'+data.enColNm+'" size="10" onKeyUp="maxLengthCheck(\'enColNmMod'+idx+'\',\'컬럼명(영문)\',30)" /></td>';
        paramHtml +='<td><input type="text" id="krColNmMod'+idx+'" value="'+data.krColNm+'" size="10" onKeyUp="maxLengthCheck(\'krColNmMod'+idx+'\',\'컬럼명(국문)\',100)" /></td>';
        paramHtml +='<td><input type="text" id="oraDatTypeMod'+idx+'" name="oraDatType" value="'+data.oraDatType+'" size="10" onKeyUp="maxLengthCheck(\'oraDatTypeMod'+idx+'\',\'ORACLE DATA TYPE\',30)" /></td>';
       	paramHtml +='<td><input type="text" id="hvdatTypeMod'+idx+'" name="hvdatType" value="'+data.hvdatType+'" size="10" onKeyUp="maxLengthCheck(\'hvdatTypeMod'+idx+'\',\'HIVE DATA TYPE\',30)" /></td>';
        paramHtml +='<td><input type="text" id="kdDatTypeMod'+idx+'" name="kdDatType" value="'+data.kdDatType+'" size="10" onKeyUp="maxLengthCheck(\'kdDatTypeMod'+idx+'\',\'KUDU DATA TYPE\',30)" /></td>';
        if (LoadTypeVal == "A" || LoadTypeVal == "T") {
        	 paramHtml +='<td style="text-align:center"><input type="hidden" id="pkYnMod'+idx+'" value="'+data.pkYn+'" /></td>';
        } else {
        	 paramHtml +='<td style="text-align:center"><input type="checkbox" id="pkYnMod'+idx+'" value="'+data.pkYn+'" /></td>'; 	
        }
       
//         paramHtml +='<td>';
		paramHtml +='<input type="hidden" id="org_krColNmMod'+idx+'" name="krColNm" value="'+data.krColNm+'"/>';
        paramHtml +='<input type="hidden" id="org_pkYnMod'+idx+'" name="pkYn" value="'+data.pkYn+'" />';
        paramHtml +='<input type="hidden" id="org_nullableMod'+idx+'" name="nullable" value="'+data.nullable+'" />';
//         paramHtml +='<input type="hidden" id="org_ptntColYnMod'+idx+'" name="ptntColYn" value="'+data.ptntColYn+'" />';
        paramHtml +='<input type="hidden" id="org_sstvInfColYnMod'+idx+'" name="sstvInfColYn" value="'+data.sstvInfColYn+'" />';
//         paramHtml +='org_chgDatBasColYnMod'+idx+'<input type="hidden" id="org_chgDatBasColYnMod'+idx+'" name="chgDatBasColYn" value="'+data.chgDatBasColYn+'" />';
//         paramHtml +='</td>';
		paramHtml +='<input type="hidden" id="org_pkOrdrMod'+idx+'" name="pkOrdr" value="'+data.pkOrdr+'"/>';
		paramHtml +='<input type="hidden" id="org_ptntColConvSntxMod'+idx+'" name="ptntColConvSntx" value="'+data.ptntColConvSntx+'"/>';
		if (LoadTypeVal == "A" || LoadTypeVal == "T") {
			paramHtml +='<td><input type="hidden" id="pkOrdrMod'+idx+'" value="'+data.pkOrdr+'" size="1" style="width:60px" onInput="maxNumberLengthCheck(this.id, 4)" />'+data.pkOrdr+'</td>';
		} else {
			paramHtml +='<td><input type="number" id="pkOrdrMod'+idx+'" value="'+data.pkOrdr+'" size="1" style="width:60px" onInput="maxNumberLengthCheck(this.id, 4)" disabled/></td>';	
		}
       
        paramHtml +='<td style="text-align:center"><input type="checkbox" id="nullableMod'+idx+'" value="'+data.nullable+'" /></td>';
        if (LoadTypeVal == "T") {
//         	 paramHtml +='<td style="text-align:center"><input type="hidden" id="ptntColYnMod'+idx+'" value="'+data.ptntColYn+'" /></td>';	//파티션 컬럼 여부
//              paramHtml +='<td><input type="hidden" id="ptntColConvSntxMod'+idx+'" value="'+data.ptntColConvSntx+'"disabled size="10" /></td>';	//파티션 컬럼 변환식
        } else {
//        	paramHtml +='<td style="text-align:center"><input type="checkbox" id="ptntColYnMod'+idx+'" value="'+data.ptntColYn+'" onChange="swChangeCheck(this,\'ptntColConvSntxMod'+idx+'\')" /></td>';	//파티션 컬럼 여부
//         	paramHtml +='<td style="text-align:center"><input type="checkbox" id="ptntColYnMod'+idx+'" value="'+data.ptntColYn+'" /></td>';	//파티션 컬럼 여부
//             paramHtml +='<td><input type="text" id="ptntColConvSntxMod'+idx+'" value="'+data.ptntColConvSntx+'" disabled size="10" onKeyUp="maxLengthCheck(\'ptntColConvSntxMod'+idx+'\',\'파티션 컬럼 변환식\',300)" /></td>';	//파티션 컬럼 변환식
            
        }
        paramHtml +='<td style="border-right: 1px solid #c7c7c7;text-align:center"><input type="checkbox" id="sstvInfColYnMod'+idx+'" value="'+data.sstvInfColYn+'" /></td>';//민감정보 포함여부
//         paramHtml +='<td>';
//         paramHtml +='<select id="sstvInfTypeMod'+idx+'" name="sstvInfType">';	//민감정보 분류
//         paramHtml +='<option value="개인정보" >개인정보</option>';
//         paramHtml +='</select>';
		
		if (LoadTypeVal == "A" || LoadTypeVal == "U") {
		// ETL 기준컬럼여부 체크박스 체크여부
			if(data.enColNm == $('#chgDatBasCol').val()){
				paramHtml +='<td style="text-align:center"><input type="checkbox" id="chgDatBasColYnMod'+idx+'" value="Y" onclick="chgDatBasColFuncM(this, '+idx+' )" name="chgDatBasColM"  /></td>';	//변경적재 기준컬럼여부
				$("#chgDatBasCol").val(data.enColNm);
				//alert("#chgDatBasColYnMod"+idx);
	 			//$("#chgDatBasColYnMod"+idx).attr('checked', true);
			}else{
		        paramHtml +='<td style="text-align:center"><input type="checkbox" id="chgDatBasColYnMod'+idx+'" value="N" onclick="chgDatBasColFuncM(this, '+idx+' )" name="chgDatBasColM"  /></td>';	//변경적재 기준컬럼여부
			}
		}else{
			paramHtml +='<td style="text-align:center"><input type="hidden" id="chgDatBasColYnMod'+idx+'" value="N" onclick="chgDatBasColFuncM(this, '+idx+' )" name="chgDatBasColM"  /></td>';	//변경적재 기준컬럼여부
		}
		
		
        paramHtml +='<input type="hidden" id="sysNmsMod'+idx+'" name="sysNms" value="'+$('#popUpA-sysNm-mod').val()+'" />';
        paramHtml +='<input type="hidden" id="enTblNmsMod'+idx+'" name="enTblNms" value="'+$('#popUpA-enTblNmd-mod').val()+'" />';
//         paramHtml +='</td>';
        paramHtml +='</tr>';

            $("#columnSetBodyMod").append(paramHtml);
          //  $("input:checkbox").addClass("rdChk");
//             paramHtml = "";
//             paramHtml += '<label for="pkYnMod'+idx+'" class="chkSel"></label>';
//             paramHtml += '<label for="nullableMod'+idx+'" class="chkSel"></label>';
//             paramHtml += '<label for="ptntColYnMod'+idx+'" class="chkSel"></label>';
            paramHtml += '<label for="sstvInfColYnMod'+idx+'" class="chkSel"></label>';
            paramHtml += '<label for="chgDatBasColYnMod'+idx+'" class="chkSel"></label>';
//             $("label").addClass("chkSel");

// 			cssSet = '<style>{';
// 			cssSet += 'label.chkSel,label.rdSel{margin:0 !important;line-height:40px;font-weight:normal;}}';
// 			cssSet += 'label.chkSel i,label.rdSel i{display:inline-block;width:40px;height:40px;text-align:center;cursor:pointer;line-height:40px;font-size:20px;color:#ccc;}';
// 			cssSet += 'input[type="radio"].rdRdo,input[type="checkbox"].rdChk{position:absolute !important;left:-100000px !important;}';
// 			cssSet += 'input[type=checkbox]:checked + label.chkSel i.fa-square-o,label.chkSel i.fa-check-square-o,input[type=radio]:checked + label.rdSel i.fa-circle-o,label.rdSel i.fa-dot-circle-o{display:none;}';
// 			cssSet += 'input[type=checkbox]:checked + label.chkSel i.fa-check-square-o,input[type="radio"]:checked + label.rdSel i.fa-dot-circle-o{color:#ff5340;display:block;}';
// 			cssSet += '}</style>';
// 			$("#columnSetBodyMod").append(paramHtml);
			
			$("input[type=checkbox]").unbind('click').bind('click',function(){
    			id=$(this).attr('id');
    			if (id != undefined) {
    				//pk여부 y체크시 pk순서가 enabled처리됨
             		if (id.match('pkYnMod')) {
             			row = id.replace('pkYnMod','');
             			if ($("#pkOrdrMod"+row).is(":disabled")){
             				$("#pkOrdrMod"+row).prop('disabled',false);	
             			} else {
             				$("#pkOrdrMod"+row).val('');
             				$("#pkOrdrMod"+row).prop('disabled',true);
             			}
             		}
             		//컬럼여부 y체크시 파티션컬럼변환식이 enabled처리됨
             		if (id.match('ptntColYnMod')) {
             			row = id.replace('ptntColYnMod','');
             			if ($("#ptntColConvSntxMod"+row).is(":disabled")){
             				$("#ptntColConvSntxMod"+row).prop('disabled',false);	
             			} else {
             				$("#ptntColConvSntxMod"+row).val('');
             				$("#ptntColConvSntxMod"+row).prop('disabled',true);
             			}
             		}
    			}
            });
            if ($("#pkYnMod"+idx).val() == "Y"){
            	$("#pkYnMod"+idx).prop('checked',true);
            	$("#pkOrdrMod"+idx).prop('disabled',false);	
            } else {
            	$("#pkYnMod"+idx).prop('checked',false);
            	$("#pkOrdrMod"+idx).prop('disabled',true);
            }
            if ($("#nullableMod"+idx).val() == "Y"){
            	$("#nullableMod"+idx).prop('checked',true);
            } else {
            	$("#nullableMod"+idx).prop('checked',false);
            }
            if ($("#ptntColYnMod"+idx).val() == "Y"){
            	$("#ptntColYnMod"+idx).prop('checked',true);
            	$("#ptntColConvSntxMod"+idx).prop('disabled',false);
            } else {
            	$("#ptntColYnMod"+idx).prop('checked',false);
            	$("#ptntColConvSntxMod"+idx).prop('disabled',true);
            }
            if ($("#sstvInfColYnMod"+idx).val() == "Y"){
            	$("#sstvInfColYnMod"+idx).prop('checked',true);
            } else {
            	$("#sstvInfColYnMod"+idx).prop('checked',false);
            }
            if ($("#chgDatBasColYnMod"+idx).val() == "Y"){
            	$("#chgDatBasColYnMod"+idx).prop('checked',true);
            } else {
            	$("#chgDatBasColYnMod"+idx).prop('checked',false);
            }
            columnSetRow++;
}

//컬럼이 존재하는 상태에서 적재타입 재선택시 전체 컬럼들을 각 타입에 맞게 초기화하는 펑션  
function swChangeCheck(mod){
	var subOption;
	if (mod=="I") {
		chkType = $("#addForm").find("[name=loadType]:checked").val();
		subOption = "";
	}
	if (mod=="M") {
		chkType = $("#modifyForm").find("[name=loadType]:checked").val();
		subOption = "Mod";
	}
	
	for(i=0;i<$("input[name='enColNm']").length;i++) {
		if (chkType == "A" || chkType == "T") {
			$('#pkYn'+subOption+i).prop('checked',false);
			$('#pkYn'+subOption+i).attr('type', 'hidden');
			$('#pkYn'+subOption+i).val('N');
			$('#org_pkYn'+subOption+i).val('N');
       } else {
    	   $('#pkYn'+subOption+i).attr('type', 'checkbox');
    	   $('#pkYn'+subOption+i).prop('checked',false);
    	   $('#pkYn'+subOption+i).val('N');
    	   $('#org_pkYn'+subOption+i).val('N');
       }
		if (chkType == "A" || chkType == "T") {
			$('#pkOrdr'+subOption+i).attr('type', 'hidden');
			$('#pkOrdr'+subOption+i).val('');
		} else {
			$('#pkOrdr'+subOption+i).val('');
			$('#pkOrdr'+subOption+i).attr('type', 'number');
			$('#pkOrdr'+subOption+i).prop('disabled',true);
		}
		if (chkType == "T") {
			$('#ptntColYn'+subOption+i).prop('checked',false);
			$('#ptntColYn'+subOption+i).attr('type', 'hidden');
			$('#ptntColYn'+subOption+i).val('N');
			$('#org_ptntColYn'+subOption+i).val('N');
			$('#ptntColConvSntx'+subOption+i).attr('type', 'hidden');
			$('#ptntColConvSntx'+subOption+i).val('');
			$('#chgDatBasColYn'+subOption+i).attr('type', 'hidden');
       	} else {
       		$('#ptntColYn'+subOption+i).attr('type', 'checkbox');
       		$('#ptntColYn'+subOption+i).prop('checked',false);
       		$('#ptntColYn'+subOption+i).val('N');
       		$('#org_ptntColYn'+subOption+i).val('N');
       		$('#ptntColConvSntx'+subOption+i).val('');
			$('#ptntColConvSntx'+subOption+i).attr('type', 'text');
			$('#ptntColConvSntx'+subOption+i).prop('disabled',true);
	    	$('#chgDatBasColYn'+subOption+i).attr('type', 'checkbox');
       	}
	}

	$("input[type=checkbox]").unbind('click').bind('click',function(){
		id=$(this).attr('id');
		if ($(this).is(":checked")==true){
    		$("#"+id).val("Y");
    		$("#org_"+id).val("Y");
    	} else {
    		$("#"+id).val("N");
    		$("#org_"+id).val("N");
    	}
		
 		if (id != undefined) {
 			//pk여부 y체크시 pk순서가 enabled처리됨
			if (id.match('pkYn'+subOption)) {
     			row = id.replace('pkYn'+subOption,'');
     			if ($("#pkYn"+subOption+row).val() == "N"){
     				$("#pkOrdr"+subOption+row).val('');
     				$("#pkOrdr"+subOption+row).prop('disabled',true);	
     			} else {
     				$("#pkOrdr"+subOption+row).prop('disabled',false);
     			}
     		}
			if (id.match('pkYnMod')) {
     			row = id.replace('pkYnMod'+subOption,'');
     			
     			if ($("#pkYnMod"+subOption+row).val() == "N"){
     				$("#pkOrdrMod"+subOption+row).val('');
     				$("#pkOrdrMod"+subOption+row).prop('disabled',true);	
     			} else {
     				$("#pkOrdrMod"+subOption+row).prop('disabled',false);
     			}
     			
     		}
     		//컬럼여부 y체크시 파티션컬럼변환식이 enabled처리됨
     		if (id.match('ptntColYn')) {
     			row = id.replace('ptntColYn'+subOption,'');
     			if ($("#ptntColYn"+subOption+row).val() == "N"){
     				$("#ptntColConvSntx"+subOption+row).val('');
     				$("#ptntColConvSntx"+subOption+row).prop('disabled',true);	
     			} else {
     				$("#ptntColConvSntx"+subOption+row).prop('disabled',false);
     			}
     		}
     		if (id.match('ptntColYnMod')) {
     			row = id.replace('ptntColYnMod'+subOption,'');
     			if ($("#ptntColYnMod"+subOption+row).val() == "N"){
     				$("#ptntColConvSntxMod"+subOption+row).val('');
     				$("#ptntColConvSntxMod"+subOption+row).prop('disabled',true);	
     			} else {
     				$("#ptntColConvSntxMod"+subOption+row).prop('disabled',false);
     			}
     		}
 	 		
 	 		
 		}
    });
	
}

function paramAddFix(){
        var paramHtml = "";
        paramHtml +='<input type="hidden" name="colOrdr" value="" />';
        paramHtml +='<input type="hidden" name="enColNm" value=""  />';
        paramHtml +='<input type="hidden" name="krColNm" value=""/>';
        paramHtml +='<input type="hidden" name="oraDatType" value=""/>';
       	paramHtml +='<input type="hidden" name="hvdatType" value="" />';
        paramHtml +='<input type="hidden" name="kdDatType" value=""/>';
        paramHtml +='<input type="hidden" name="pkYn" value=""/>';
        paramHtml +='<input type="hidden" name="nullable" value=""/>';
//         paramHtml +='<input type="hidden" name="ptntColYn" value=""/>';
        paramHtml +='<input type="hidden" name="sstvInfColYn" value=""/>';
        paramHtml +='<input type="hidden" name="chgDatBasColYn" value=""/>';

        paramHtml +='<input type="hidden" name="pkOrdr" value="" />';
        paramHtml +='<input type="hidden" name="ptntColConvSntx" value=""/>';
//         paramHtml +='<input type="hidden" name="sstvInfType" value=""/>';
        paramHtml +='<input type="hidden" name="sysNms" value=""/>';
        paramHtml +='<input type="hidden" name="enTblNms" value=""/>';
		$("#columnSetBody").append(paramHtml);
}

function seqOrderSet(){
	var seqNum=1;
	
	for(i=0;i<$("input[name='enColNm']").length;i++) {
		$("input:text[name=colOrdr]").eq(i).val(seqNum);
		seqNum++;
	}
}

function paramAddList(paramAddSetId, mod){
	if (mod == "I") {
		if ($("#popUpA-sysNm").val() == "") {
			alert("시스템 명을 등록해 주시기 바랍니다.");
			return;
		} else if ($("#popUpA-enTblNm").val() == "") {
			alert("테이블 명을 등록해 주시기 바랍니다.");	
			return;
		}
	}

	/*
	<tr>
	<td>선택</td>
	<td>순서</td>
	<td>컬럼명(영문)</td>
	<td>컬럼명(국문)</td>
	<td>ORACLE DATA TYPE</td>
	<td>HIVE DATA TYPE</td>
	<td>KUDU DATA TYPE</td>
	<td>PK여부</td>
	<td>PK순서</td>
	<td>NOT NULL 허용여부</td>
	<td>파티션 컬럼 여부</td>
	<td>파티션 컬럼 변환식</td>
	<td>민감정보 포함여부</td>
	<td>민감정보 분류</td>
	<td>ETL 기준컬럼여부</td>
	</tr>
	*/
//     if(columnSetData && columnSetData.length > 0) {

        var paramHtml = "";
        var selected = "";
        var seqNum = parseInt($("input[name='enColNm']").length)+1;
        var LoadTypeVal = "";
        paramHtml +='<tr id="rowId'+columnSetRow+'">';
        if (mod=="M") {
        	LoadTypeVal = $("#modifyForm").find("[name=loadType]:checked").val();	//체크된 적재타입 호출
        	paramHtml +='<td style="text-align:center"><input type="checkbox" name="chkColumnMod" value="'+columnSetRow+'"/></td>';
        } else {
        	LoadTypeVal = $("#addForm").find("[name=loadType]:checked").val();	//체크된 적재타입 호출
        	paramHtml +='<td style="text-align:center"><input type="checkbox" name="chkColumn" value="'+columnSetRow+'"/></td>';
        }
        paramHtml +='<td><input type="text" id="colOrdr'+columnSetRow+'" name="colOrdr" value='+seqNum+' size="1" readonly /></td>';
        paramHtml +='<td><input type="text" id="enColNm'+columnSetRow+'" name="enColNm" value="" size="10" onKeyUp="maxLengthCheck(\'enColNm'+columnSetRow+'\',\'컬럼명(영문)\',30)" /></td>';
        paramHtml +='<td><input type="text" id="krColNm'+columnSetRow+'" name="krColNm" value="" size="10" onKeyUp="maxLengthCheck(\'krColNm'+columnSetRow+'\',\'컬럼명(국문)\',100)" /></td>';
        paramHtml +='<td><input type="text" id="oraDatType'+columnSetRow+'" name="oraDatType" value="" size="10" onKeyUp="maxLengthCheck(\'oraDatType'+columnSetRow+'\',\'ORACLE DATA TYPE\',30)" /></td>';
       	paramHtml +='<td><input type="text" id="hvdatType'+columnSetRow+'" name="hvdatType" value="" size="10" onKeyUp="maxLengthCheck(\'hvdatType'+columnSetRow+'\',\'HIVE DATA TYPE\',30)" /></td>';
        paramHtml +='<td><input type="text" id="kdDatType'+columnSetRow+'" name="kdDatType" value="" size="10" onKeyUp="maxLengthCheck(\'kdDatType'+columnSetRow+'\',\'KUDU DATA TYPE\',30)" /></td>';
       
//         paramHtml +='<td>';
 		if (LoadTypeVal == "A" || LoadTypeVal == "T") {
 			
 			if (mod=="M") {
 				 paramHtml +='<td style="text-align:center"><input type="hidden" id="pkYnMod'+columnSetRow+'" value="N" /></td>';
 			} else {
 				 paramHtml +='<td style="text-align:center"><input type="hidden" id="pkYn'+columnSetRow+'" value="N" /></td>';
 			}
        } else {
        	if (mod=="M") {
        		paramHtml +='<td style="text-align:center"><input type="checkbox" id="pkYnMod'+columnSetRow+'" value="N" /></td>';
        	} else {
        		paramHtml +='<td style="text-align:center"><input type="checkbox" id="pkYn'+columnSetRow+'" value="N" /></td>';
        	}
        	 
        }
       
        paramHtml +='<input type="hidden" id="org_nullable'+columnSetRow+'" name="nullable" value="N" />';
        
        paramHtml +='<input type="hidden" id="org_sstvInfColYn'+columnSetRow+'" name="sstvInfColYn" value="N" />';
//         paramHtml +='org_chgDatBasColYn'+columnSetRow+'<input type="hidden" id="org_chgDatBasColYn'+columnSetRow+'" name="chgDatBasColYn" value="N" size="4"/>';
//         paramHtml +='</td>';
		
		if (mod=="M") {
			 paramHtml +='<input type="hidden" id="org_pkYnMod'+columnSetRow+'" name="pkYn" value="N" />';
			paramHtml +='<input type="hidden" id="org_pkOrdrMod'+columnSetRow+'" name="pkOrdr" value=""/>';
// 			paramHtml +='<input type="hidden" id="org_ptntColYnMod'+columnSetRow+'" name="ptntColYn" value="N" />';
// 			paramHtml +='<input type="hidden" id="org_ptntColConvSntxMod'+columnSetRow+'" name="ptntColConvSntx" value=""/>';	
		} else {
			 paramHtml +='<input type="hidden" id="org_pkYn'+columnSetRow+'" name="pkYn" value="N" />';
			paramHtml +='<input type="hidden" id="org_pkOrdr'+columnSetRow+'" name="pkOrdr" value=""/>';
// 			paramHtml +='<input type="hidden" id="org_ptntColYn'+columnSetRow+'" name="ptntColYn" value="N" />';
// 			paramHtml +='<input type="hidden" id="org_ptntColConvSntx'+columnSetRow+'" name="ptntColConvSntx" value=""/>';
		}
		if (LoadTypeVal == "A" || LoadTypeVal == "T") {
			if (mod=="M") {
				paramHtml +='<td><input type="hidden" id="pkOrdrMod'+columnSetRow+'" size="1" style="width:60px" onInput="maxNumberLengthCheck(this.id, 4)" disabled/></td>';
			} else {
				paramHtml +='<td><input type="hidden" id="pkOrdr'+columnSetRow+'" size="1" style="width:60px" onInput="maxNumberLengthCheck(this.id, 4)" disabled/></td>';
			}
			
       } else {
    	   if (mod=="M") {
    		   paramHtml +='<td><input type="number" id="pkOrdrMod'+columnSetRow+'" size="1" style="width:60px" onInput="maxNumberLengthCheck(this.id, 4)" disabled/></td>';
			} else {
				paramHtml +='<td><input type="number" id="pkOrdr'+columnSetRow+'" size="1" style="width:60px" onInput="maxNumberLengthCheck(this.id, 4)" disabled/></td>';
			}
    	   	
       }
        
        paramHtml +='<td style="text-align:center"><input type="checkbox" id="nullable'+columnSetRow+'" value="N" /></td>';
//			2019-03-14 주석
//         if (LoadTypeVal == "T") {
//             if (mod=="M") {
//             	 paramHtml +='<td style="text-align:center"><input type="hidden" id="ptntColYnMod'+columnSetRow+'" value="N" /></td>';
//             	 paramHtml +='<td><input type="hidden" id="ptntColConvSntxMod'+columnSetRow+'"  disabled size="10" onKeyUp="maxLengthCheck(\'ptntColConvSntx'+columnSetRow+'\',\'파티션 컬럼 변환식\',300)" /></td>';         	
//             } else {
//             	 paramHtml +='<td style="text-align:center"><input type="hidden" id="ptntColYn'+columnSetRow+'" value="N" /></td>';
//             	 paramHtml +='<td><input type="hidden" id="ptntColConvSntx'+columnSetRow+'"  disabled size="10" onKeyUp="maxLengthCheck(\'ptntColConvSntx'+columnSetRow+'\',\'파티션 컬럼 변환식\',300)" /></td>'; 	
//             }
//         } else {
//             if (mod=="M") {
//             	paramHtml +='<td style="text-align:center"><input type="checkbox" id="ptntColYnMod'+columnSetRow+'" value="N" /></td>';
//             	paramHtml +='<td><input type="text" id="ptntColConvSntxMod'+columnSetRow+'"  disabled size="10" onKeyUp="maxLengthCheck(\'ptntColConvSntx'+columnSetRow+'\',\'파티션 컬럼 변환식\',300)" /></td>';
//             } else {
//             	paramHtml +='<td style="text-align:center"><input type="checkbox" id="ptntColYn'+columnSetRow+'" value="N" /></td>'; 
//             	paramHtml +='<td><input type="text" id="ptntColConvSntx'+columnSetRow+'"  disabled size="10" onKeyUp="maxLengthCheck(\'ptntColConvSntx'+columnSetRow+'\',\'파티션 컬럼 변환식\',300)" /></td>';
//             }
//         }
        paramHtml +='<td style="border-right: 1px solid #c7c7c7;text-align:center"><input type="checkbox" id="sstvInfColYn'+columnSetRow+'" value="N" size="4"/></td>';
//         paramHtml +='<td>';
//         paramHtml +='<select id="sstvInfType'+columnSetRow+'" name="sstvInfType">';
//         paramHtml +='<option value="개인정보" >개인정보</option>';
//         paramHtml +='</select>';
//         paramHtml +='</td>';
		if (LoadTypeVal == "A" || LoadTypeVal == "U") {
        	paramHtml +='<td style="text-align:center"><input type="checkbox" id="chgDatBasColYn'+columnSetRow+'" value="N" onclick="chgDatBasColFuncM(this, '+columnSetRow+' )" name="chgDatBasColM" /></td>';
		}else{
			$("#chgDatBasCol").val("");
		}
        if (mod=="M") {
        	paramHtml +='<input type="hidden" id="sysNmsMod'+columnSetRow+'" name="sysNms" value="'+$('#popUpA-sysNm-mod').val()+'" />';
            paramHtml +='<input type="hidden" id="enTblNmsMod'+columnSetRow+'" name="enTblNms" value="'+$('#popUpA-enTblNmd-mod').val()+'" />';
            
        } else {
        	paramHtml +='<input type="hidden" id="sysNms'+columnSetRow+'" name="sysNms" value="'+$("#popUpA-sysNm").val()+'" />';
            paramHtml +='<input type="hidden" id="enTblNms'+columnSetRow+'" name="enTblNms" value="'+$("#popUpA-enTblNm").val()+'" />';
            
        }
        paramHtml +='</tr>';

//         if($("#columnSetBody tr").length < 3){
			// M = 수정, ELSE = 등록
			if (mod == "M") {
				$("#columnSetBodyMod").append(paramHtml);	
			} else {
				$("#columnSetBody").append(paramHtml);
			}
			$("input[type=checkbox]").unbind('click').bind('click',function(){
    			id=$(this).attr('id');
    			//alert("id:"+id);
            	if ($(this).is(":checked")==true){
            		$("#"+id).val("Y");
            		$("#org_"+id).val("Y");
            	} else {
            		$("#"+id).val("N");
            		$("#org_"+id).val("N");
            	}
        		if (id != undefined) {
        			//pk여부 y체크시 pk순서가 enabled처리됨
        			if (id.match('pkYn')) {
             			row = id.replace('pkYn','');
             			if ($("#pkYn"+row).val() == "N"){
             				$("#pkOrdr"+row).val('');
             				$("#pkOrdr"+row).prop('disabled',true);	
             			} else {
             				$("#pkOrdr"+row).prop('disabled',false);
             			}
//              			if ($("#pkOrdrMod"+row).is(":disabled")){
//              				$("#pkOrdrMod"+row).prop('disabled',false);	
//              			} else {
//              				$("#pkOrdrMod"+row).val('');
//              				$("#pkOrdrMod"+row).prop('disabled',true);
//              			}
             		}
        			if (id.match('pkYnMod')) {
        				//alert("chk");
             			row = id.replace('pkYnMod','');
             			
             			if ($("#pkYnMod"+row).val() == "N"){
             				$("#pkOrdrMod"+row).val('');
             				$("#pkOrdrMod"+row).prop('disabled',true);	
             			} else {
             				$("#pkOrdrMod"+row).prop('disabled',false);
             			}
             			
             		}
             		//컬럼여부 y체크시 파티션컬럼변환식이 enabled처리됨
             		if (id.match('ptntColYn')) {
             			row = id.replace('ptntColYn','');
             			if ($("#ptntColYn"+row).val() == "N"){
             				$("#ptntColConvSntx"+row).val('');
             				$("#ptntColConvSntx"+row).prop('disabled',true);	
             			} else {
             				$("#ptntColConvSntx"+row).prop('disabled',false);
             			}
             		}
             		if (id.match('ptntColYnMod')) {
             			row = id.replace('ptntColYnMod','');
             			if ($("#ptntColYnMod"+row).val() == "N"){
             				$("#ptntColConvSntxMod"+row).val('');
             				$("#ptntColConvSntxMod"+row).prop('disabled',true);	
             			} else {
             				$("#ptntColConvSntxMod"+row).prop('disabled',false);
             			}
             		}
        		}
        		
         		
            });
			
            columnSetRow++;
            columnSetRowIndex++;
            
//         }else{
//             alert("컬럼 추가는 최대 3개까지 가능합니다.");
//         }
//     } else {
//         $("#columnSetBody").append('');
//     }
}

function paramDelList(data, mod){
	removeId="";
	if (mod=="M"){
		if($("#columnSetBodyMod tr").length > 1){
			$("input:checkbox[name=chkColumnMod]:checked").each(function(i, elements){
				index = $(elements).index("input:checkbox[name=chkColumnMod]");
				removeId = $("input:checkbox[name=chkColumnMod]").eq(index).val();
				$("#rowId"+removeId).remove();
				columnSetRowIndex--;
			})
		}else{
			alert("컬럼 추가는 최소 1개는 설정되어야 합니다.");
		}
	} else {
		if($("#columnSetBody tr").length > 1){
			$("input:checkbox[name=chkColumn]:checked").each(function(i, elements){
				index = $(elements).index("input:checkbox[name=chkColumn]");
				removeId = $("input:checkbox[name=chkColumn]").eq(index).val();
				$("#rowId"+removeId).remove();
				columnSetRowIndex--;
			})
		}else{
			alert("컬럼 추가는 최소 1개는 설정되어야 합니다.");
		}
	}
	seqOrderSet();
}

function initFormValues(){

    $('#popUpA-roleId-h').val('');
    $('#popUpA-domainId-h').val('');
    $('#popUpA-useYn-h').val('Y');
    $('#popUpA-delYn-h').val('N');
    $('#popUpA-isDuplicatedID-h').val('Y');
    $('#popUpA-userId').val('');
    $('#popUpA-domainNm').text('');
    $('#popUpA-chkAll, #popUpA-chkAdmin').prop('checked',false);
    
    $('#popUpA-sysNm').val('');
    $('#popUpA-enTblNm').val('');
    $('#popUpA-krTblNm').val('');
    $('#loadType1').prop('checked', true);
    $('#actionTypeI').val('');
    $('#chgDatBasCol-I').val('');
    $("#chgDatBasCol").val("");

    $('#popUpM-userId-h').val('');
    $('#popUpM-on').val('Y');
    $('#popUpM-userId').text('');
    $('#popUpM-domainNm').text('');
    $('#popUpM-userRoleDc').text('');
    $('#popUpM-createDt').text('');
    $('#popUpM-modifyDt').text('');
    $('#popUpM-chkAll').prop('checked',false);
    $('#popUpM-tbodyData').find('input[type=checkbox]').prop('checked',false);

}

function linkPage(pageNo) {
    $("#currentPageNo").val(pageNo);
    $("#listForm").attr({
        action : "systemMetaList.do",
        method : "post"
    }).submit();
}

function searchList() {
	$("#currentPageNo").val(1);
        $("#listForm").attr({
            action : "systemMetaList.do",
            method : "post"
        }).submit();
}

function fn_saveForm(succMs, errorMs, mod) {
	var confirmMsg = "";
	if (mod=="I") {
		confirmMsg = "메타 데이터를 등록 하시겠습니까?";
		chkType = $("#addForm").find("[name=loadType]:checked").val();
		for(i=0;i<$("input[name='enColNm']").length;i++) {
			$("#org_pkOrdr"+i).val($("#pkOrdr"+i).val());		//disable옵션때문에 input을 이원화시켰음
			$("#org_ptntColConvSntx"+i).val($("#ptntColConvSntx"+i).val());
		}
	}
	if (mod=="M") {
// 		confirmMsg = "메타 데이터를 수정 하시겠습니까?";
		var tableName = $("#popUpA-enTblNmd-mod").val();
		confirmMsg = "메타 데이터를 수정 하시면 hbda_ods." + tableName + ", " + "hbda_dw." + tableName + "을 DROP 해야합니다. 메타 데이터를 수정하시겠습니까?";
		chkType = $("#modifyForm").find("[name=loadType]:checked").val();
		for(i=0;i<$("input[name='enColNm']").length;i++) {
			$("#org_krColNmMod"+i).val($("#krColNmMod"+i).val());
			$("#org_pkOrdrMod"+i).val($("#pkOrdrMod"+i).val());
			$("#org_ptntColConvSntxMod"+i).val($("#ptntColConvSntxMod"+i).val());
			if ($("#org_krColNmMod"+i).val() == "") $("#org_krColNmMod"+i).val("null");	//form에서 던지는 방식과 모델에서 받는 방식 때문에 update시에는 null일 경우를 강제로 주입시켜야함 
			if ($("#org_pkOrdrMod"+i).val() == "") $("#org_pkOrdrMod"+i).val("null");
			if ($("#org_ptntColConvSntxMod"+i).val() == "") $("#org_ptntColConvSntxMod"+i).val("null");
		}
	}
	if (mod=="D") {
		confirmMsg = "메타 데이터를 삭제 하시겠습니까?";
	}
	if (mod=="R") {
		confirmMsg = "해당 테이블을 초기 적재 하시겠습니까?";
	}
	
	if (confirm(confirmMsg)) {
		var form;
		var pixNum = 0;
		$("#currentPageNo").val(1);
		$("#actionTypeU").val(mod);
		errorReturn = fn_EmptyErrorAllRowMsgSet("enColNm","모든 컬럼에 '컬럼명(영문)'이 등록되어 있어야 합니다.",mod);
		if (errorReturn == true) return;
		
		errorReturn = fn_DupErrorMsgSet("enColNm","리스트상에 중복된 '컬럼명(영문)'이 있습니다.",mod);
		if (errorReturn == true) return;
		
		errorReturn = fn_EmptyErrorAllRowMsgSet("oraDatType","모든 컬럼에 'ORACLE DATA TYPE'이 등록되어 있어야 합니다.",mod);
		if (errorReturn == true) return;
		
		errorReturn = fn_EmptyErrorAllRowMsgSet("hvdatType","모든 컬럼에 'HIVE DATA TYPE'이 등록되어 있어야 합니다.",mod);
		if (errorReturn == true) return;
		
		errorReturn = fn_EmptyErrorAllRowMsgSet("kdDatType","모든 컬럼에 'KUDU DATA TYPE'이 등록되어 있어야 합니다.",mod);
		if (errorReturn == true) return;
		
// 		if (chkType == "A") {
// 			errorReturn = fn_EmptyErrorMsgSet("ptntColConvSntx"," Append 타입일 경우 최소 한개 이상의 '컬럼변환식'이 등록되어 있어야 합니다.",mod);
// 			if (errorReturn == true) return;
// 		}
		
		errorReturn = fn_useYnColErrorMsg("pkYn","체크한 'PK여부'의 PK순서를 입력해야 합니다.",mod,"pkOrdr");
		if (errorReturn == true) return;
		
		errorReturn = fn_useYnColErrorMsg("ptntColYn","체크한 '파티션 컬럼 여부'의 '파티션 컬럼 변환식'을 입력해야 합니다.",mod,"ptntColConvSntx");
		if (errorReturn == true) return;
		
		if (chkType == "U") {
			errorReturn = fn_EmptyErrorMsgSet("pkOrdr"," Upsert 타입일 경우 최소 한개 이상의 'PK순서'가 등록되어 있어야 합니다.",mod);
			if (errorReturn == true) return;
		}
		
		errorReturn = fn_DupErrorMsgSet("pkOrdr","리스트상에 중복된 'PK순서'가 있습니다.",mod);
		if (errorReturn == true) return;
		
 		errorReturn = fn_orderListErrorMsgSet("pkOrdr","리스트상에 기입된 'PK순서'가 올바르지 않습니다.",mod,"pkYn");
 		if (errorReturn == true) return;
		
		if (mod=="I") {
			paramAddFix();
			form = "addForm";
		}
		if (mod=="M") {
			form = "modifyForm";
		}
		if (mod=="D") {
			form = "modifyForm";
		}
		if (mod=="R"){
			form = "modifyForm";
		}
		
		$.ajax({
		    type:'post',
		    url:$("#"+form).attr('action'),
		    data:$("#"+form).serialize(),
		    dataType:'text',
		    success: function(data){
		    	alert(succMs);
		    	location.reload();
		    },
		    error : function(e){
		    	$("#columnSetBodyMod").empty();	
		    	$("#columnSetBody").empty();
		    	$("#columnSetBodyFix").empty();
		    	columnSetRow = 0;
		    	columnSetRowIndex = 0;
		    	alert(errorMs);
		    }
		    });  
	}
	
}

//checkbox y옵션 선택후 입력 안했을때 체크 
function fn_useYnColErrorMsg(val1,val2,mod,ynOption) {
	var errorFlag = false;
	if(mod != "D") {
		for (i = 0; i < $("input[name='" + val1 + "']").length; i++) {
			if ($("input[name='" + val1 + "']").eq(i).val() == "Y") {
				if ($("input[name='" + ynOption + "']").eq(i).val() == "" || $("input[name='" + ynOption + "']").eq(i).val() == "null") {
					errorFlag = true;
					alert(val2);
					return true;
				}
			}
		}	
	}
}

	//pkOrder의 row에서 중간에 누락된 숫자가 있을 경우 체크

	function fn_orderListErrorMsgSet(val1, val2, mod, ynOption) {
		if (mod != "D") {
			var errorFlag = 0;
			var pkOrder = 1;
			var pkOrderList = new Array();
			var index = 0;
			for (i = 0; i < $("input[name='" + val1 + "']").length; i++) {
				//alert("ynOption:"+$("input[name='"+ynOption+"']").eq(i).val());
				if ($("input[name='" + ynOption + "']").eq(i).val() == "Y") {
					pkOrderList[index] = $("input[name='" + val1 + "']").eq(i)
							.val();
					index++;
				}
			}

			for (i = 0; i < pkOrderList.length; i++) {
				for (ii = 0; ii < pkOrderList.length; ii++) {
					if (pkOrder == pkOrderList[ii]) {
						errorFlag++;
					}
				}
				pkOrder++;
			}
			if (errorFlag != pkOrderList.length) {
				alert(val2);
				return true;
			} else {
				return false;
			}
		}
	}

	//row에서 전체 필수 입력 체크
	function fn_EmptyErrorAllRowMsgSet(val1, val2, mod) {
		var errorFlag = false;
		if (mod == "I") {
			for (i = 0; i < $("input[name='" + val1 + "']").length; i++) {
				if ($("input[name='" + val1 + "']").eq(i).val() == "") {
					errorFlag = true;
				}
			}
			if (errorFlag == true) {
				alert(val2);
				return true;
			} else {
				return false;
			}
		}
		if (mod == "M") {
			for (i = 0; i < $("input[name='" + val1 + "']").length; i++) {
				if ($("input[name='" + val1 + "']").eq(i).val() == "") {
					errorFlag = true;
				}
			}
			if (errorFlag == true) {
				alert(val2);
				return true;
			} else {
				return false;
			}
			return false;
		}
		if (mod == "D") {
			return false;
		}

	}

	//row에서 최소 한개 입력 필수 입력 체크
	function fn_EmptyErrorMsgSet(val1, val2, mod) {
		var errorFlag = false;
		var chkNum = 0;
		if (mod == "I") {
			for (i = 0; i < $("input[name='" + val1 + "']").length; i++) {
				if ($("input[name='" + val1 + "']").eq(i).val() == "") {
					errorFlag = true;
					chkNum++;
				}
			}
			if ($("input[name='" + val1 + "']").length == chkNum) {
				alert(val2);
				return true;
			} else {
				return false;
			}
		}
		if (mod == "M") {
			for (i = 0; i < $("input[name='" + val1 + "']").length; i++) {
				if ($("input[name='" + val1 + "']").eq(i).val() == "null") {
					errorFlag = true;
					chkNum++;
				}
			}
			if ($("input[name='" + val1 + "']").length == chkNum) {
				alert(val2);
				return true;
			} else {
				return false;
			}
			return false;
		}
		if (mod == "D") {
			return false;
		}

	}
	function fn_DupErrorMsgSet(val1, val2, mod) {
		var errorFlag = false;
		var dupArray = new Array();
		if (mod == "I") {
			for (i = 0; i < $("input[name='" + val1 + "']").length; i++) {
				for (ii = i + 1; ii < $("input[name='" + val1 + "']").length; ii++) {
					if ($("input[name='" + val1 + "']").eq(i).val() == $(
							"input[name='" + val1 + "']").eq(ii).val()) {
						if ($("input[name='" + val1 + "']").eq(i).val() != ""
								&& $("input[name='" + val1 + "']").eq(i).val() != "null") {
							//alert(":"+$("input[name='"+val1+"']").eq(i).val() );
							errorFlag = true;
						}

					}
				}
			}
			if (errorFlag == true) {
				alert(val2);
				return true;
			} else {
				return false;
			}
		}
		if (mod == "M") {
			for (i = 0; i < $("input[name='" + val1 + "']").length; i++) {
				for (ii = i + 1; ii < $("input[name='" + val1 + "']").length; ii++) {
					if ($("input[name='" + val1 + "']").eq(i).val() == $(
							"input[name='" + val1 + "']").eq(ii).val()) {
						if ($("input[name='" + val1 + "']").eq(i).val() != ""
								&& $("input[name='" + val1 + "']").eq(i).val() != "null") {
							//	alert("d:"+$("input[name='"+val1+"']").eq(i).val() );
							errorFlag = true;
						}
					}
				}
			}
			if (errorFlag == true) {
				alert(val2);
				return true;
			} else {
				return false;
			}
		}
		if (mod == "D") {
			return false;
		}
	}

	function maxLengthCheck(id, title, maxByte) {
		var str = $("#" + id).val();
		// 	alert(id+","+"str:"+str);
		var cbyte = 0;
		var li_len = 0;
		var str_len = str.length;
		for (i = 0; i < str_len; i++) {
			var is_one_char = str.charAt(i);
			if (escape(is_one_char).length > 4) {
				cbyte += 2;
			} else {
				cbyte++;
			}
			if (cbyte <= maxByte) {
				li_len = i + 1;
			}
		}
		if (parseInt(cbyte) > parseInt(maxByte)) {
			alert("'" + title + "' 입력 필드에 허용된 글자수가 초과되었습니다. \r\n\n최대 '"
					+ maxByte + "' 바이트까지의 글자 크기수 허용");
			var str2 = str.substr(0, li_len);
			$("#" + id).val(str2);
			// 		for(i=0;i<$input.val().length;i++){
			// 			var is_one_char = $input.val().charAt(i);
			// 			if(escape(is_one_char).length > 4){
			// 				cbyte += 2;
			// 			} else {
			// 				cbyte++;
			// 			}
			// 		}		
		}

	}

	function maxNumberLengthCheck(id, maxLength) {
		if ($("#" + id).val().length > maxLength) {
			$("#" + id).val($("#" + id).val().slice(0, maxLength));
		}
	}
	
    // chgDatBasColYnMod (수정)  name=chgDatBasColM
    function chgDatBasColFuncM(tag, index){
    	if($("input[name='chgDatBasColM']:checked").length <2){
    		if($("#"+tag.id).is("checked")){
    			$("#"+tag.id).attr("checked", false);
    		}else{
	    		$("#"+tag.id).attr('checked', true);
	    		// 영문 컬렴명 가져오기
	    		if($('#enColNmMod'+index).val() != null && $('#enColNmMod'+index).val() != ""){
		    		var col_nm = $('#enColNmMod'+index).val();
		    		$("#chgDatBasCol").val(col_nm);
	    		}else if($('#enColNm'+index).val() != null && $('#enColNm'+index).val() != ""){
		    		var col_nm = $('#enColNm'+index).val();
		    		$("#chgDatBasCol-I").val(col_nm);
	    		}else{
	    			alert("컬럼명(영문)을 먼저 입력해주세요");
	    			$("#"+tag.id).attr("checked", false);
	    		}
    		}
    	}else{
    		alert(" ETL 변경 기준 컬럼은 한 개의 컬럼만 설정할 수 있습니다.");
    		$("#"+tag.id).attr('checked', false);
    	}
    }    
	
</script>

<div class="mConBox">
    <div class="mCont">
        <div class="row">
            <div class="col-md-4" id="subtitleA">
            </div>

            <div class="col-md-8">
                <div class="pageRoute">
                </div>
            </div>
        </div>
        <!-- end row -->

        <form:form modelAttribute="metaParam" id="listForm" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="conBox" style="min-height:400px;">

                        <div class="row">
                            <div class="col-md-33 select-set-3">
                                <p> 총 <b>${systemMetaParam.totalRecordCount}</b> 메타 테이블</p>
                            </div>
                            <div class="col-md-9 text-right select-sys-1">
                             	<form:select id="optSearch1" path="searchOption1" onChange="linkPage(1);">
                                    <form:option value="" >적재 타입</form:option>
                                    <form:option value="A" >A (append)</form:option>
                                    <form:option value="U" >U (upsert)</form:option>
                                    <form:option value="T" >T (truncate & insert)</form:option>
                                </form:select>
                               	<form:select id="optSearch2" path="searchOption2" onChange="linkPage(1);">
                                    <form:option value="" >사용 여부</form:option>
                                    <form:option value="Y" >사용</form:option>
                                    <form:option value="N" >미사용</form:option>
                                </form:select>
                                <form:select id="optSearch3" path="searchOption3">
                                    <form:option value="" >시스템 명</form:option>
                                    <form:option value="krTblNm" >한글 테이블 명</form:option>
                                    <form:option value="enTblNm" >영문 테이블 명</form:option>
                                </form:select>
                                
                                <form:input path="searchText" type="text" id="inpSearch" class="searchTxt" placeholder="검색어를 입력하세요" />
                                <button type="button" id="btnSearch" class="searchBtn">
                                    <i class="ti-search"></i> 검색
                                </button>
                                <button type="button" title="메타 테이블을 추가할 수 있습니다." id="btnPopUpAdd" class="AddDefaultBtn">
                                    <i class="fa fa-plus"></i> 메타등록
                                </button>
                            </div>
                        </div>
                        <!-- end row > conBox > col-md-12 > row > form -->

                        <div class="row">
                            <div class="col-md-12">
                                <table class="viewDateTbl">
                                    <colgroup>
                                        <col width="">
                                        <col width="">
                                        <col width="">
                                        <col width="">
                                        <col width="">
                                        <col width="">
                                    </colgroup>
                                    <thead>
                                        	<th>시스템 명칭</th>
                                            <th>테이블명(영문)</th>
                                            <th>테이블명(한글)</th>
                                            <th>적재타입</th>
<!--                                             <th>파티션테이블여부</th> -->
                                            <th>등록일시</th>
                                            <th>사용여부</th>
                                            <th>상세관리</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:if test="${empty systemMetaList}">
                                        <tr><td colspan="8">No data available in table</td></tr>
                                    </c:if>
                                    <c:if test="${not empty systemMetaList}">
                                        <c:forEach var="list" items="${systemMetaList}" varStatus="status">
                                            <tr id="${list.sysNm}|${list.enTblNm}|${list.krTblNm}|${list.loadType}|${list.chgDatBasCol}">
	                                            <td>${list.sysNm}</td>
	                                            <td>${list.enTblNm}</td>
	                                            <td>${list.krTblNm}</td>
	                                            <td>${list.loadType}</td>
<%-- 	                                            <td>${list.ptntTblYn}</td> --%>
	                                            <td>
	                                            <fmt:parseDate value="${list.regDdtm}" var="noticePostDate" pattern="yyyyMMddHHmmss"/>
	                                            <fmt:formatDate value="${noticePostDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
	                                            </td>
	                                            <td>${list.useYn}</td>
	                                            <td><button type="button" class="tblInBtn userModifySet">수정</button></td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8 pageNum">
                                <!-- Paging With Taglib -->
                                <ui:pagination paginationInfo="${systemMetaParam}" type="css" jsFunction="linkPage" />
                                <form:hidden path="currentPageNo" />
                            </div>
                            <div class="col-md-2 select-set-3"></div>
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end conBox -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </form:form>
    </div>
    <!-- end mCont -->
</div>
<!-- end mConBox -->


<!--사용자 계정 수정-->
<div id="divPopUpM-UserAccount" class="popUp col-md-8"  style="width:100%;margin-left:-50%">
    <div class="popUpTop">메타 수정
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="metaDataModSet.do" method="POST" modelAttribute="metaParam" id="modifyForm">
				<input type="hidden" id="actionTypeU" name="actionType" value="" />
				<input type="hidden" id="chgDatBasCol" name="chgDatBasCol" value="" />

               <table class="popInTbl">
                    <colgroup>
                        <col width="25%"/>
                        <col width="25%" />
                        <col width="25%" />
                        <col width="25%" />
                    </colgroup>
                    <tr>
                        <th><label for="popUpA-sysNm-mod">시스템 명</label></th>
                        <td>
                            <input type="text" id="popUpA-sysNm-mod" name="sysNm" class="w89p" value="" readonly onKeyUp='maxLengthCheck("popUpA-sysNm-mod", "시스템 명", 30);' />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-enTblNmd-mod">테이블 명(영문)</label></th>
                        <td>
                            <input type="text" id="popUpA-enTblNmd-mod" name="enTblNm" class="w89p" value="" readonly onKeyUp='maxLengthCheck("popUpA-enTblNmd-mod", "테이블 명(영문)", 30);' />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-krTblNm-mod">테이블 명(국문)</label></th>
                        <td>
                            <input type="text" id="popUpA-krTblNm-mod" name="krTblNm" class="w89p" value="" onKeyUp='maxLengthCheck("popUpA-krTblNm-mod", "테이블 명(국문)", 100);' />
                        </td>
                    </tr>
                     <tr>
                        <th><label for="popUpA-loadTypeMod-mod">적재타입</label></th>
                        <td>
                            Append <input type="radio" class="rdRdo" id="popUpA-loadTypeModA-mod" name="loadType" value="A" onClick="swChangeCheck('M')" checked>
                            <label for="popUpA-loadTypeModA-mod" class="rdSel"></label>
                            Upsert <input type="radio" class="rdRdo" id="popUpA-loadTypeModU-mod" name="loadType" value="U" onClick="swChangeCheck('M')" >
                            <label for="popUpA-loadTypeModU-mod" class="rdSel"></label>
                            Truncate & insert <input type="radio" class="rdRdo" id="popUpA-loadTypeModT-mod" name="loadType" value="T" onClick="swChangeCheck('M')" >
                        	<label for="popUpA-loadTypeModT-mod" class="rdSel"></label>
                        </td>
                    </tr>
                    
               		            <tr>
                       
                        <td colspan=2>
                        	<div class="TblInBtnWrap">
                                <button type="button" class="tblInBtn darkGrayBtn" onclick="paramAddList('', 'M');">
                                    <i class="fa fa-plus" aria-hidden="true"></i> 컬럼 추가
                                </button>
                                <button type="button" class="tblInBtn darkGrayBtn" onclick="paramDelList('', 'M');">
                                    <i class="fa fa-minus" aria-hidden="true"></i> 컬럼 삭제
                                </button>
                            </div>
                           
                            <table class="popInTblInner">
                                <colgroup>
                                   <col width="5%" />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col width="5%"  />
                                   <col />
                                    <col width="5%" />
                                    <col width="5%" />
<%--                                     <col /> --%>
                                    <col width="5%" />
                                </colgroup>
                                
                                <theader>
                                    <tr>
                                    	<th>선택</th>
                                        <th>순서</th>
                                        <th>컬럼명(영문)</th>
                                        <th>컬럼명(국문)</th>
                                        <th>ORACLE DATA TYPE</th>
                                        <th>HIVE DATA TYPE</th>
                                        <th>KUDU DATA TYPE</th>
                                        <th>PK여부</th>
                                        <th>PK순서</th>
                                        <th>NOT NULL 허용여부</th>
<!--                                         <th>파티션 컬럼 여부</th> -->
<!--                                         <th>파티션 컬럼 변환식</th> -->
                                        <th>민감정보 포함여부</th>
<!--                                         <th>민감정보 분류</th> -->
                                        <th>ETL 기준컬럼여부</th>
                                       
                                    </tr>
                                </theader>
                                <tbody id="columnSetBodyMod"></tbody>
                            </table>
                            
                        </td>
                    </tr>
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
        	<button type="button" id="btnM-reload" class="btn-style1 darkGrayBtn ">초기적재</button>
        	<button type="button" id="btnM-update" class="btn-style1 darkGrayBtn ">수정</button>
            <button type="button" id="btnM-delete" class="btn-style1 darkGrayBtn ">삭제</button>
            <button type="button" id="btnM-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>
<!--메타 수정-->

<!--메타 추가-->
<div id="divPopUpA-UserAccount" class="popUp col-md-8" style="width:100%;margin-left:-50%;text-align:center">
    <div class="popUpTop">메타 추가
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain" >
        <div class="popUpCon" >
            <form:form action="metaDataRegSet.do" method="POST" modelAttribute="metaParam" id="addForm">
				 <input type="hidden" id="actionTypeI" name="actionType" value="" />
				 <input type="hidden" id="chgDatBasCol-I" name="chgDatBasCol" value="" />
                <table class="popInTbl" style="text-align:center">
                    <colgroup>
                        <col width="25%"/>
                        <col width="25%" />
                        <col width="25%" />
                        <col width="25%" />
                    </colgroup>
                    <tr>
                        <th><label for="popUpA-sysNm">시스템 명</label></th>
                        <td>
                            <input type="text" id="popUpA-sysNm" name="sysNm" class="w89p" value="" onKeyUp='maxLengthCheck("popUpA-sysNm", "시스템 명", 30);' />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-enTblNm">테이블 명(영문)</label></th>
                        <td>
                            <input type="text" id="popUpA-enTblNm" name="enTblNm" class="w89p" value="" onKeyUp='maxLengthCheck("popUpA-enTblNm", "테이블 명(영문)", 30);' />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-krTblNm">테이블 명(국문)</label></th>
                        <td>
                            <input type="text" id="popUpA-krTblNm" name="krTblNm" class="w89p" value="" onKeyUp='maxLengthCheck("popUpA-krTblNm", "테이블 명(국문)", 100);' />
                        </td>
                    </tr>
                     <tr>
                        <th><label for="popUpA-loadType">적재타입</label></th>
                        <td>
                            Append<input type="radio" class="rdRdo" id="loadType1" name="loadType" value="A" onClick="swChangeCheck('I')"  checked>
                            <label for="loadType1" class="rdSel"></label>
                            Upsert<input type="radio" class="rdRdo" id="loadType2" name="loadType" value="U" onClick="swChangeCheck('I')">
                            <label for="loadType2" class="rdSel"></label>
                            Truncate & insert<input type="radio" class="rdRdo" id="loadType3" name="loadType" value="T" onClick="swChangeCheck('I')">
                            <label for="loadType3" class="rdSel"></label>
                        </td>
                    </tr>
                    <tr>
                       
                        <td colspan=2>
                        	<div class="TblInBtnWrap">
                                <button type="button" class="tblInBtn darkGrayBtn" onclick="paramAddList('', 'I');">
                                    <i class="fa fa-plus" aria-hidden="true"></i> 컬럼 추가
                                </button>
                                <button type="button" class="tblInBtn darkGrayBtn" onclick="paramDelList('', 'I');">
                                    <i class="fa fa-minus" aria-hidden="true"></i> 컬럼 삭제
                                </button>
                            </div>
                           
                            <table class="popInTblInner">
                                <colgroup>
                                    <col width="5%" />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col width="5%"  />
                                   <col />
                                    <col width="5%" />
                                    <col width="5%" />
<%--                                     <col /> --%>
                                    <col width="5%" />
                                </colgroup>
                                
                                <theader>
                                    <tr>
                                    	<th>선택</th>
                                        <th>순서</th>
                                        <th>컬럼명(영문)</th>
                                        <th>컬럼명(국문)</th>
                                        <th>ORACLE DATA TYPE</th>
                                        <th>HIVE DATA TYPE</th>
                                        <th>KUDU DATA TYPE</th>
                                        <th>PK여부</th>
                                        <th>PK순서</th>
                                        <th>NOT NULL 허용여부</th>
<!--                                         <th>파티션 컬럼 여부</th> -->
<!--                                         <th>파티션 컬럼 변환식</th> -->
                                        <th>민감정보 포함여부</th>
<!--                                         <th>민감정보 분류</th> -->
                                        <th>ETL 기준컬럼여부</th>
                                       
                                    </tr>
                                </theader>
                                <tbody id="columnSetBody"></tbody>
                                <tbody id="columnSetBodyFix"></tbody>
                            </table>
                            
                        </td>
                    </tr>
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
        	<button type="button" id="btnA-insert" class="btn-style1 darkGrayBtn">확인</button>
            <button type="button" id="btnA-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>

<div id="loading2" style="z-index:1002; display:none;">
    <div class="sk-cube-grid" style="z-index:1002">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
    <div class="sk-cube-txt" style="z-index:1002">초록마을</div>
</div>