<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../common/common.jsp"%>

<!doctype html>
<html lang="ko">
    <head>
    <meta charset="utf-8">
    <title>초록마을 제품추천</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
<!--         <link rel="shortcut icon" href="/img/favicon.ico"> -->

        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/fonts/font-awesome.css" rel="stylesheet">

        <link href="/css/themify-icons.css" rel="stylesheet">
        <link href="/css/offset.css" rel="stylesheet">
        <link href="/css/custom.css" rel="stylesheet">
        <link href="/css/res.css" rel="stylesheet">

        <script src="/js/jquery-1.12.4.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/custom.js"></script>

        <script>

        function goHome(){

            // var _domainNm = '<%=domainNm%>';
            var _roleId = '<%=roleId%>';

            if(_roleId == 1){

                document.errorForm.action = "/board/noticeList.do";
            }else if(_roleId == 2){

                document.errorForm.action = "/index/adminDashboard.do";
            }else if(_roleId == 3){

                document.errorForm.action = "/index/serviceDashboard.do";
            }

            document.errorForm.method = "POST";
            document.errorForm.submit();
        }

        </script>



    </head>

    <body>
    <div id="loading"></div>
        <div id="errorBox">
            <div class="errorInner">
                <div class="errorTop">
                    <i class="ti-alert"></i> ERROR
                </div>

                <div class="errorCont">
                    <p>시스템 에러입니다. 자세한 사항은 관리자에게 문의해주시기 바랍니다.<p>
                    <span>문의처 : 00-000-000</span>
                </div>

                <div class="errorBtn">
                    <form method="post"CommonAdminToolParam name="errorForm">
                        <button class="darkGrayBtn" onclick="history.back()"><i class="ti-back-left"></i> 돌아가기</button>
                        <button class="lightGrayBtn" onclick="goHome()"><i class="ti-home"></i> HOME</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>