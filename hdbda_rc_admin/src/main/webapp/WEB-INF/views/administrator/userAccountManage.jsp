<%------------------------------------------------------------------------
- Menu  : 권한 관리 > 사용자 관리 > 사용자 계정 관리
- ID    : ADM_USER_001
- Ref.  : -
- URI   : -
- History
- 2018-11-28 created by ljy
------------------------------------------------------------------------%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<script type="text/javascript">

var controller = '/administrator/useraccount';
var CreateUserDupCheckYn = false;
var roleIdCheck;
$(window).ready(function(){
    pageEvents();
});

function pageEvents(){

    // close & cancel buttons
    $('#btnM-cancel, #btnA-cancel, .closePop i').unbind('click').bind('click',function(){
    	$("#popUpA-userId").removeAttr('disabled');
    	fn_util_divPopUpControl($(this).parent().parent().parent().attr('id'),false);
        initFormValues();
    });

    // add button
    $('#btnPopUpAdd').unbind('click').bind('click',function(){
    	CreateUserDupCheckYn = false;
    	$("#popUpA-userId").val('');
    	$('#chkI_3').prop('checked',true);
    	$('#popUpA-useYnY').prop('checked',true);
        fn_util_divPopUpControl('divPopUpA-UserAccount',true);
        $('#popUpA-domainNm').text( $('#domainNm-h').val() );
    });

    $('.userModifySet').unbind('click').bind('click',function(){
    	var str = $(this).parent().parent().attr('id');
    	var strArr = str.split('|');
     	var userId = strArr[0];
     	var roleIdCheck = strArr[1];
     	var useYnCheck = strArr[2];
    	$('#popUpA-userId-mod').val(userId);
    	$('#chk_'+roleIdCheck).prop('checked',true);
    	 
    	 if (useYnCheck == "Y"){
    		 $('#popUpA-useYnY-mod').prop('checked',true);
    	 } else {
    		 $('#popUpA-useYnN-mod').prop('checked',true);
    	 }

    	fn_util_divPopUpControl('divPopUpM-UserAccount',true);
    });
    
    $('.userPwdReset').unbind('click').bind('click',function(){
    	var str = $(this).parent().parent().attr('id');
    	var strArr = str.split('|');
     	var userId = strArr[0];
    	fn_userPwdReset(userId);
    });

    // In Modify form - save button
    $('#btnM-update').unbind('click').bind('click',function(){
    	var userId = $(this).parent().parent().attr('id');
        fn_userModifySet(userId);
    });

    // In Add form - save button
    $('#btnA-insert').unbind('click').bind('click',function(){
        fn_CreateAccountUserSet();
    });

    // Event for click SEARCH button
    $('#btnSearch').unbind('click').bind('click',function(){
        searchAccount();
    });

    // toggle USE_YN
    $(".dToggle").unbind('click').bind('click',function(){
        var $i = $(this).find('ul li i:first');
        var $span = $(this).find('ul li span:first');
        var userId = $(this).closest('tr').attr('id');
        var useYn;
        if( confirm('변경하시겠습니까?') ){
            useYn = $i.hasClass('fa-toggle-on') ? 'N' : 'Y';

            if( fn_modifyUseYn(userId,useYn) ){
                if( $i.hasClass('fa-toggle-on') ){
                    $span.text('OFF');
                }else{
                    $span.text('ON');
                }

                $(this).toggleClass('fOn fOff');
                $i.toggleClass('fa-toggle-on fa-toggle-off');
                searchAccount();
            }
        }
    });

    // btnA-popId popUpA-userId
    $('#btnA-popId').unbind('click').bind('click',function(){
    	if ($('#popUpA-userId').val() == "") {
    		alert("아이디 입력이 필요합니다.");
    		return;	
    	}
    	
        var url = controller + '/isDuplicateUserId.do';
        var param =  {
        		'userId' : $('#popUpA-userId').val(), 
        		'RecordCountPerPage':1,
        		'pageSize':10			
        	};
        fn_isDuplicateUserId(param,url);
    });

    // In modify form - check box
    $('#popUpM-tbodyData').find('input[type=checkbox]').unbind('change').bind('change',function(){
        if( $('#popUpM-tbodyData').find('.rdChk').length == $('#popUpM-tbodyData').find('.rdChk:checked').length ){
            $(this).closest('table').find('thead').find('input[type=checkbox]').prop('checked',true);
        }else{
            $(this).closest('table').find('thead').find('input[type=checkbox]').prop('checked',false);
        }
    });

    $('#popUpM-chkAll').unbind('change').bind('change',function(){
        if ( $('#popUpM-chkAll').prop('checked') ){
            $('#popUpM-tbodyData').find('input[type=checkbox]').prop('checked',true);
        }else{
            $('#popUpM-tbodyData').find('input[type=checkbox]').prop('checked',false);
        }
    });

    // In Add form - check box
    $('#popUpA-chkAll, #popUpA-chkAdmin').unbind('change').bind('change',function(){
        if( $(this).prop('checked') ){
            $('#popUpA-chkAll, #popUpA-chkAdmin').prop('checked',true);
        }else{
            $('#popUpA-chkAll, #popUpA-chkAdmin').prop('checked',false);
        }
    });

    $("#btnM-resetPin").unbind('click').bind('click',function(){
        $('#divResetPin').toggle();
        $('#divResetEmail').toggle();
        $('#popUpM-email').val('');
    });

    $("#btnM-resetExecute").unbind('click').bind('click',function(){
        fn_resetPassword();
    });
}

//------------------------------------------------------------------------
//----- Connection -------------------------------------------------
//------------------------------------------------------------------------
function fn_popDataSet() {
	 var url = controller + '/isGetAuthCd.do';
     var param = {
    		'RecordCountPerPage':1,
     		'pageSize':10
     	};
     fn_isGetAuthData(param, url);
}

function fn_userModifySet(userId) {
	if (confirm("해당 사용자의 정보를 변경 하시겠습니까?")) {
		var userId = $('#popUpA-userId-mod').val();
		var roleId ="";
		var useYn = ""; 
		$('input[name=popUpAMod]:checked').each(function(){
			roleId = $(this).val();
		})
		$('input[name=useYnMod]:checked').each(function(){
			useYn = $(this).val();
		})
		
		var url = controller + '/userModifySet.do';
	    var param = {
	    		'userId': userId,
	    		'roleId': roleId, 
	    		'useYn':useYn,
	    		'RecordCountPerPage':1,
	    		'pageSize':10		
	    	};
		fn_isModifyAccountUser(param, url);
	}
}

function fn_userPwdReset(userId) {
	if (confirm("비밀번호는 해당 사용자의 ID와 동일하게 초기화 됩니다. \n비밀번호를 초기화 하시겠습니까?" )) {
		var url = controller + '/userPwdReset.do';
		var param = {
				'userId': userId,
				'RecordCountPerPage':1,
	    		'pageSize':10		
			};
	   
	    fn_isSetUserPassRest(param, url);
	}
}
	 
function fn_CreateAccountUserSet() {
	if (confirm("해당 사용자의 아이디를 신규 등록 하시겠습니까?")) {
		var userId = $('#popUpA-userId').val();
		var roleId ="";
		var useYn = "";
		$('input[name=popUpA]:checked').each(function(){
			roleId = $(this).val();
		})
		$('input[name=useYn]:checked').each(function(){
			useYn = $(this).val();
		})
		var url = controller + '/createAccountUser.do';
	    var param = {
	    		'userId': userId,
	    		'userPassword': userId,
	    		'roleId': roleId,
	    		'useYn': useYn,
	    		'RecordCountPerPage':1,
	    		'pageSize':10	
	    };
	    var isValid = true;
	    
	    if(CreateUserDupCheckYn==false){
	        alert('아이디 중복체크를 하셔야 사용자 등록이 가능 합니다.');
	    } else {
	    	fn_isCreateAccountUser(param, url);
	    }
	}
}	 

function fn_isModifyAccountUser(param, url){
    $.ajax({
            type: 'POST',
            url: url,
            data: param,
            dataType:'JSON',
            async:false,
            success: function(data){
	        	if (data.messageCode=="OK") {
	        		alert("사용자 정보가 수정 되었습니다.");
	        		location.reload();
                } else {
                	alert("시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.");	
                }
            },
            error : function(e){
            	alert("네트워크 통신 에러:"+JSON.stringify(e));
            }
        });
}

function fn_isSetUserPassRest(param, url){
    $.ajax({
            type: 'POST',
            url: url,
            data: param,
            dataType:'JSON',
            async:false,
            success: function(data){
                if (data.messageCode=="OK") {
                	alert("비밀번호를 초기화 하였습니다.");
                	location.reload();
                } else {
                	alert("시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.");	
                }
                
            },
            error : function(e){
            	alert("네트워크 통신 에러:"+JSON.stringify(e));
            }
        });
}

function fn_isCreateAccountUser(param, url){
    $.ajax({
            type: 'POST',
            url: url,
            data: param,
            dataType:'JSON',
            async:false,
            success: function(data){
	        	if (data.messageCode=="OK") {
	        		alert("사용자 계정이 등록 되었습니다.");
	        		location.reload();
                } else {
                	alert("시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.");	
                }
            },
            error : function(e){
            	alert("네트워크 통신 에러:"+JSON.stringify(e));
            }
        });
}

function fn_isGetAuthData(param, url){
    var innerHtml = '';
    $.ajax({
            type: 'POST',
            url: url,
            data: param,
            dataType:'JSON',
            contentType:'application/json;charset=UTF-8',
            async:false,
            success: function(data){
                for (i=0;i<data.result[i];i++){
                	innerHtml += '	 <tr>';
            	    innerHtml += '<td>'+data.result[i].userRoleDc+'</td>';
            	    innerHtml += '<td class="txtC">';
            	    innerHtml += '    <input type="radio" id="popUpA" name="popUpA" value="'+data.result[i].roleId+'">';
            	    innerHtml += '</td>';
            	    innerHtml += '</tr>';	
                }
        	    
        		$('#popUpA-theadData').html(innerHtml);
        		$('#popUpA-theadData-mod').html(innerHtml);
            },
            error : function(e){
            	alert("에러:"+JSON.stringify(e));
            }
        });
    return isValid;
}

function fn_isDuplicateUserId(param, url){
    var isValid = false;
    setTimeout(function(){
    $.ajax({
            type: 'POST',
            url: url,
            dataType:'JSON',
            data: param,
            async:false,
            success: function(data){
                if (data.messageCode=="OK") {
                	 if (data.result == 0) {
                		 $("#popUpA-userId").attr('disabled',true);
                		 CreateUserDupCheckYn=true;
                		 alert("사용할수 있는 아이디입니다.");
                	 } else {
                		 $("#popUpA-userId").removeAttr('disabled');
                		 CreateUserDupCheckYn=false;
                		 alert("이미 등록된 아이디입니다.");
                	 }
	        		
                } else {
                	alert("시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.");	
                }
            },
            error : function(e){
            	alert("에러:"+JSON.stringify(e));
				$("#popUpA-userId").removeAttr('disabled');
            	CreateUserDupCheckYn=false;
            }
        });
    return isValid;
    }, 1);
}

function fn_modifyUseYn(userId,useYn){
    var url = controller + '/accountModifyUseYn.do';
    var param = {
    		'userId':userId,
    		"useYn":useYn,
    		'RecordCountPerPage':1,
    		'pageSize':10		
    }
    var result;
    if( fn_validate_useYn(userId) ){
        $.ajax({
            type: 'POST',
            dataType:'JSON',
            url: url,
            data: param,
            async:false,
            success: function(data){
                if(data.Integer > 0){
                    result = true;
                }
            },
            error : function(e){

            }
        });
    }else{
        result = false;
    }
    return result;
}

function fn_resetPassword(){
    var url = controller + '/accountResetPassword.do';
    var param;
    var userId;
    var email = $('#popUpM-email').val();

    if( email.isBlank() || !email.isEmail() ){
        alert('유효하지 않은 이메일 입니다.');
        $('#popUpM-email').focus();
    }else{
        if(confirm('비밀번호를 초기화 하시겠습니까?')){

            maskOn2();
            $("#loading2").show();

            setTimeout(
                function(){

                    userId = $('#popUpM-userId').text();
                    param = {
                    		'userId':userId,
                    		'email':email,
                    		'RecordCountPerPage':1,
                    		'pageSize':10
                    		};

                    $.ajax({
                        url:url,
                        data:param,
                        dataType:'JSON',
                        type:'POST',
                        async:false,
                        success:function(resultData){

                            if(resultData != null){

                                var result = resultData.HashMap.result;
                                var emailMsg = resultData.HashMap.emailMsg;

                                if(result == 1){

                                    alert("비밀번호를 초기화 하였습니다.");
                                    alert(emailMsg);

                                    $("#btnM-resetPin").trigger('click');
                                    $('#popUpM-userPassword-h').val('1');

                                    $("#loading2").hide();
                                    maskOff2();
                                }else{

                                    alert("비밀번호를 초기화하지 못했습니다.\n확인 후 다시 진행해 주시기 바랍니다.");

                                    $("#loading2").hide();
                                    maskOff2();
                                }
                            }
                        },
                        error:function(e){
                            alert('서버와의 통신에 실패하였습니다.');
                            $("#loading2").hide();
                            maskOff2();
                        }
                    });
                }
            , 1000);
        }
    }
}
//------------------------------------------------------------------------
//----- Select  --------------------------------------------
//------------------------------------------------------------------------
function fn_setUserAccountRow(data,status){
    var resultData  = data.HashMap.accountInfo;
    var servicesD   = data.HashMap.serviceListOfDomain;
    var servicesU   = data.HashMap.serviceListOfUser;
    var userId      = resultData.userId;
    var userPassword= resultData.userPassword;
    var roleId      = resultData.roleId;
    var userRoleNm  = resultData.userRoleNm;
    var userRoleDc  = resultData.userRoleDc;
    var domainId    = resultData.domainId;
    var domainNm    = resultData.domainNm;
    var domainURL   = resultData.domainURL;
    var domainDc    = resultData.domainDc;
    var useYn       = resultData.useYn;
    var createDt    = resultData.createDt;
    var modifyDt    = resultData.modifyDt;

    createDt = fn_util_formatDate(createDt,true,true,'/');
    modifyDt = modifyDt == null ? '' : fn_util_formatDate(modifyDt,true,true,'/');

    if(roleId == 2){
        userRoleDc += ' / 서비스 사용자';
    }

    $('#popUpM-trUseYn input[name=useYn][value='+useYn+']').prop('checked',true);
    $('#popUpM-userId-h').val(userId);
    $('#popUpM-userId').text(userId);
    $('#popUpM-userPassword-h').val(userPassword);
    $('#popUpM-domainNm').text(domainNm);
    $('#popUpM-userRoleDc').text(userRoleDc);
    $('#popUpM-createDt').text(createDt);
    $('#popUpM-modifyDt').text(modifyDt);

    var innerHtml = '';
    $.each(servicesD, function(idx,val){
        innerHtml += '<tr>'
        innerHtml += '  <td class="txtC">';
        innerHtml += '    <input type="checkbox" class="rdChk" id="popUpM-'+val.serviceId+'" name="stringList" value="'+val.serviceId+'" />';
        innerHtml += '    <label class="chkSel" for="popUpM-'+val.serviceId+'">';
        innerHtml += '      <i class="fa fa-check-square-o"></i>';
        innerHtml += '      <i class="fa fa-square-o"></i>';
        innerHtml += '    </label>';
        innerHtml += '  </td>';
        innerHtml += '  <td>'+val.serviceNm+'</td>';
        innerHtml += '</tr>'
    });
    $('#popUpM-tbodyData').html(innerHtml);

    pageEvents();

    $.each(servicesU, function(idx,val){
        if(val.useYn == 'Y'){
            $("input:checkbox[id='popUpM-"+val.serviceId+"']").trigger('click');
        }
    });

}

//------------------------------------------------------------------------
// -----  Modify  ------------------------------------------
//------------------------------------------------------------------------
// click button of save & delete in modify section
function fn_modifyUserAccount(){
    var url = controller + '/accountModifyRow.do';
    var isValid = false;
    $('#popUpM-controlMode-h').val('M');
    var useYn = $('#popUpM-trUseYn input[name=useYn]:checked').val() == 'Y'
            ? true : false;
    var serviceLength = $('#popUpM-tbodyData').find('.rdChk:checked').length > 0
            ? true : false;
    var userPassword = $('#popUpM-userPassword-h').val() > 0
            ? true : false;

    if(useYn){
        if( !serviceLength || !userPassword ){
            alert('최초 비밀번호가 설정되지 않았거나'
                 +'\n 서비스가 할당되지 않았습니다.');
        }else{
            isValid = true;
        }
    }else{
        isValid = true;
    }

    if(isValid){
        // call ajax
        if (confirm("수정 하시겠습니까?")) {
            fn_util_ajax_serialize('modifyForm',url);
        }
    }
}

//------------------------------------------------------------------------
// -----  Add  ---------------------------------------------
//------------------------------------------------------------------------
// click save button in add domain db conn section
function fn_addUserAccount(){
    var url = controller + '/accountAddRow.do';
    var isValid = true;

    if( $('#popUpA-userId').val().isBlank() ){
        alert('ID를 생성하시기 바랍니다.');
        isValid = false;
    }

    if( isValid ){

        if( $('#popUpA-chkAdmin').prop('checked') ){
            $('#popUpA-roleId-h').val(2);    // 도메인 관리자
        }else{
            $('#popUpA-roleId-h').val(3);   // 서비스 사용자
        }

        $('#popUpA-domainId-h').val( $('#domainId').val() );

        // call ajax
        if (confirm("등록 하시겠습니까?")) {
            fn_util_ajax_serialize('addForm',url);
        }
    }
}

function fn_setUserID(data){
    $('#popUpA-userId').val(data.String);
}

//------------------------------------------------------------------------
// -----  Validations -----------------------------------------------
//------------------------------------------------------------------------

function initFormValues(){

    $('#popUpA-roleId-h').val('');
    $('#popUpA-domainId-h').val('');
    $('#popUpA-useYn-h').val('Y');
    $('#popUpA-delYn-h').val('N');
    $('#popUpA-isDuplicatedID-h').val('Y');
    $('#popUpA-userId').val('');
    $('#popUpA-domainNm').text('');
    $('#popUpA-chkAll, #popUpA-chkAdmin').prop('checked',false);

    $('#popUpM-userId-h').val('');
    $('#popUpM-on').val('Y');
    $('#popUpM-userId').text('');
    $('#popUpM-domainNm').text('');
    $('#popUpM-userRoleDc').text('');
    $('#popUpM-createDt').text('');
    $('#popUpM-modifyDt').text('');
    $('#popUpM-chkAll').prop('checked',false);
    $('#popUpM-tbodyData').find('input[type=checkbox]').prop('checked',false);

}

function fn_validate_useYn(userId){
    var isValid = false;
    $.ajax({
            type: 'POST',
            dataType:'JSON',
            url: controller + '/accountValidateUseYn.do',
            data: {'userId': userId},
            async:false,
            success: function(data){
                isValid = data.Boolean;
            },
            error : function(e){
                isValid = false;
            }
        });

    if(!isValid){
        alert('최초 비밀번호가 설정되지 않았거나'
         +'\n 서비스가 할당되지 않았습니다.');
    }
    return isValid;
}

//------------------------------------------------------------------------


function linkPage(pageNo) {
    $("#currentPageNo").val(pageNo);
    $("#listForm").attr({
        action : "accountList.do",
        method : "post"
    }).submit();
}

function goOrder(order) {
    $("#listOrder").val(order);
    $("#listForm").attr({
        action : "accountList.do",
        method : "post"
    }).submit();
}

function searchAccount() {
    if( fn_util_isValid_optionTextSearchWithAll('optSearch','inpSearch') ){
        $("#currentPageNo").val("1");
        $("#listForm").attr({
            action : "accountList.do",
            method : "post"
        }).submit();
    }
}
</script>

<div class="mConBox">
    <div class="mCont">
        <div class="row">
            <div class="col-md-4" id="subtitleA">
            </div>

            <div class="col-md-8">
                <div class="pageRoute">
                </div>
            </div>
        </div>
         <form:form modelAttribute="accountParam" id="listForm" method="post">
         <form:hidden path="listOrder"/>

            <div class="row">
                <div class="col-md-12">
                    <div class="conBox" style="min-height:400px;">

                        <div class="row">
                            <div class="col-md-33 select-set-3">
                                <p> 총 <b>${accountParam.totalRecordCount}</b> 사용자계정</p>
                            </div>
                            <div class="col-md-9 text-right select-sys-1">
                                <form:select path="recordCountPerPage" onChange="linkPage(1);">
                                    <form:option value="10" >10개씩 보기</form:option>
                                    <form:option value="20" >20개씩 보기</form:option>
                                </form:select>
                                <form:select id="optSearch" path="searchOption">
                                    <form:option value="0" >전체</form:option>
                                    <form:option value="id" >아이디</form:option>
                                    <form:option value="role" >권한</form:option>
                                </form:select>
                                <form:input path="searchText" type="text" id="inpSearch" class="searchTxt" placeholder="검색어를 입력하세요" />
                                <button type="button" id="btnSearch" class="searchBtn">
                                    <i class="ti-search"></i> 검색
                                </button>
                                <button type="button" title="사용자 계정을 추가할 수 있습니다." id="btnPopUpAdd" class="AddDefaultBtn">
                                    <i class="fa fa-plus"></i> 사용자계정 추가
                                </button>
                            </div>
                        </div>
                        <!-- end row > conBox > col-md-12 > row > form -->

                        <div class="row">
                            <div class="col-md-12">
                                <table class="viewDateTbl">
                                    <colgroup>
                                        <col width="">
                                        <col width="">
                                        <col width="">
                                        <col width="">
                                        <col width="">
                                        <col width="">
                                    </colgroup>
                                    <thead>
                                        <tr>
											<th>아이디</th>
                                            <th>권한</th>
                                            <th>등록일</th>
                                            <th>수정일</th>
                                            <th>비밀번호 초기화</th>
                                            <th>계정정보 수정</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:if test="${empty accountList}">
                                        <tr><td colspan="6">No data available in table</td></tr>
                                    </c:if>
                                    <c:if test="${not empty accountList}">
                                        <c:forEach var="list" items="${accountList}" varStatus="status">
                                            <tr id="${list.userId}|${list.roleId}|${list.useYn}">
                                                <td>${list.userId}</td>
                                                <td>
														${list.userRoleDc}
                                                </td>
                                          
                                                <td>
                                                    <fmt:formatDate value="${list.createDt}" pattern="yyyy-MM-dd"/>
                                                    <br />
                                                    <fmt:formatDate value="${list.createDt}" pattern="HH:mm:ss"/>
                                                </td>
                                                <td>
                                                    <fmt:formatDate value="${list.modifyDt}" pattern="yyyy-MM-dd"/>
                                                    <br />
                                                    <fmt:formatDate value="${list.modifyDt}" pattern="HH:mm:ss"/>
                                                </td>
                                                 <td><button type="button" class="tblInBtn userPwdReset">초기화</button></td>
                                                <td><button type="button" class="tblInBtn userModifySet">수정</button></td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8 pageNum">
                                <!-- Paging With Taglib -->
                                <ui:pagination paginationInfo="${accountParam}" type="css" jsFunction="linkPage" />
                                <form:hidden path="currentPageNo" />
                            </div>
                            <div class="col-md-2 select-set-3"></div>
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end conBox -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </form:form>
    </div>
    <!-- end mCont -->
</div>
<!-- end mConBox -->


<!--사용자 계정 수정-->
<div id="divPopUpM-UserAccount" class="popUp col-md-8">
    <div class="popUpTop">사용자계정 수정
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="accountModifyRow.do" method="POST" modelAttribute="accountParamInfo" id="modifyForm">
                <input type="hidden" id="popUpM-controlMode-h"  name="controlMode"  value="M" />
                <input type="hidden" id="popUpM-userId-h"       name="userId"       value="" />
                <input type="hidden" id="popUpM-userPassword-h" name="userPassword" value="" />

               <table class="popInTbl">
                    <colgroup>
                        <col width="20%"/>
                        <col width="80%" />
                    </colgroup>
                    <tr>
                        <th><label for="popUpA-userId-mod">아이디</label></th>
                        <td>
                            <input type="text" id="popUpA-userId-mod" name="userId" class="w89p" value="" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="">권한</label></th>
                        <td>
                            <table class="popInTblInner">
                                <colgroup>
                                    <col width="" />
                                    <col width="" />
                                </colgroup>
                                <tbody id="popUpA-tbodyData-mod">
									<c:if test="${not empty accountAuthCdList}">
                                        <c:forEach var="list" items="${accountAuthCdList}" varStatus="status">
                                            <tr>
                                                <td>${list.userRoleDc}</td>
                                                <td class="txtC">
                                               		<input type="radio" class="rdRdo" name="popUpAMod" id="chk_${list.roleId}" value="${list.roleId}" />
                                        			<label for="chk_${list.roleId}" class="rdSel"></label>
                                        		</td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    	<th scope="row"><label for="">사용여부</label></th>
						<td style="text-align:center">
							사용<input type="radio" class="rdRdo" id="popUpA-useYnY-mod" name="useYnMod" value="Y" checked>
							<label for="popUpA-useYnY-mod" class="rdSel"></label> 
							사용중지<input type="radio" class="rdRdo" id="popUpA-useYnN-mod" name="useYnMod" value="N">
							<label for="popUpA-useYnN-mod" class="rdSel"></label>
						</td>
					</tr>
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnM-update" class="btn-style1 darkGrayBtn ">저장</button>
            <button type="button" id="btnM-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>
<!--사용자 계정 수정-->

<!--사용자 계정 추가-->
<div id="divPopUpA-UserAccount" class="popUp col-md-8">
    <div class="popUpTop">사용자계정 추가
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="accountAddRow.do" method="POST" modelAttribute="accountParamInfo" id="addForm">
                <input type="hidden" id="popUpA-roleId-h"   name="roleId"   value="" />
                <input type="hidden" id="popUpA-domainId-h" name="domainId" value="" />
                <input type="hidden" id="popUpA-useYn-h"    name="useYn"    value="Y" />
                <input type="hidden" id="popUpA-delYn-h"    name="delYn"    value="N" />
                <input type="hidden" id="popUpA-isDuplicatedID-h" value="Y" />

                <table class="popInTbl">
                    <colgroup>
                        <col width="20%"/>
                        <col width="80%" />
                    </colgroup>
                    <tr>
                        <th><label for="popUpA-userId">아이디</label></th>
                        <td>
                            <input type="text" id="popUpA-userId" name="userId" class="w89p"style="width: 88% !important;" />
                            <button type="button" id="btnA-popId" class="tblInBtn inputRightBtn darkGrayBtn w7p">중복확인</button>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="">권한</label></th>
                        <td>
                            <table class="popInTblInner">
                                <colgroup>
                                    <col width="" />
                                    <col width="" />
                                </colgroup>
                                <tbody id="popUpA-tbodyData">
                                    <c:if test="${not empty accountAuthCdList}">
                                        <c:forEach var="list" items="${accountAuthCdList}" varStatus="status">
                                            <tr>
                                                <td>${list.userRoleDc}</td>
                                                <td class="txtC">
                                               		<input type="radio" class="rdRdo" name="popUpA" id="chkI_${list.roleId}" value="${list.roleId}" checked>
                                        			<label for="chkI_${list.roleId}" class="rdSel"></label>
                                        		</td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                     <tr>
                    	<th scope="row"><label for="">사용여부</label></th>
						<td style="text-align:center">
							사용<input type="radio" class="rdRdo" id="popUpA-useYnY" name="useYn" value="Y" checked>
							<label for="popUpA-useYnY" class="rdSel"></label> 
        					사용중지<input type="radio" class="rdRdo" id="popUpA-useYnN" name="useYn" value="N">
        					<label for="popUpA-useYnN" class="rdSel"></label>
						</td>	
					</tr>
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnA-insert" class="btn-style1 darkGrayBtn">저장</button>
            <button type="button" id="btnA-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>

<div id="loading2" style="z-index:1002; display:none;">
    <div class="sk-cube-grid" style="z-index:1002">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
    <div class="sk-cube-txt" style="z-index:1002">e-store 36.5+</div>
</div>
<!-- -->