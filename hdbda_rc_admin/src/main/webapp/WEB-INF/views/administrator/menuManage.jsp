<%-----------------------------------------------------------------------------------
 * TODO > 시스템 관리 > 메뉴 권한 관리
 * 파일명   > menuManage.jsp
 * 작성일   > 2018. 11. 27.
 * 작성자   > CCmedia Service Corp.
------------------------------------------------------------------------------------%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>

<script type="text/javascript">
var jsonList;
var controller = '/administrator/menu';
var MENU_ID;
var UPPER_MENU_ID;

$(document).ready(function(){
    pageEvents();   
});

function pageEvents(){
	// 1차메뉴 선택
	$('#popUpA-ulDepth1').find('li').unbind('click').bind('click',function() {				
		if(!$(this).hasClass('active') && !$(this).hasClass('firstNum')) {
			$(".newMenu").remove();
			$(".menuAdd").show();
			$(".menuDel").hide();		
	    	$('#popUpA-ulDepth1').find('li').removeClass('active');
	    	$(this).addClass('active');
	    	
			var menuIdDepths = $(this).attr('id').split('_');
			var menuId = menuIdDepths[0];
			var depths = menuIdDepths[1];
			var html1 = '<li class="firstNum">2차 메뉴</li> ';
			var html2 = '<li class="firstNum">3차 메뉴</li> ';	
			
			if(1== depths) {
				$('#popUpA-ulDepth2').find('li').remove();
				$('#popUpA-ulDepth3').find('li').remove();
			} 
			else {
				$('#popUpA-ulDepth3').find('li').remove();
			}
			
			jsonList.forEach(function (val, index, array) {
				if(1 == depths) {
					if(2 == val.displayLevel && menuId == val.upperMenuId) {           
						html1 += makeHtml(val.displayLevel, val.menuId, val.menuNm);
					}
				}
				else if(2 == depths) {
					if(3 == val.displayLevel && menuId == val.upperMenuId) {
						html2 += makeHtml(val.displayLevel, val.menuId, val.menuNm);
					}
				}
			});	
			$('#popUpA-ulDepth2').append(html1);
			$('#popUpA-ulDepth3').append(html2);
		
			pageEvents();
		}
	});
    
	// 2차메뉴 선택
    $('#popUpA-ulDepth2').find('li').unbind('click').bind('click',function() {  	
    	if(!$(this).hasClass('active') && !$(this).hasClass('firstNum')) {
			$(".newMenu").remove();
			$(".menuAdd").show();
			$(".menuDel").hide();
	    	$('#popUpA-ulDepth2').find('li').removeClass('active');
	    	$(this).addClass('active');
	    	
			var menuIdDepths = $(this).attr('id').split('_');
			var menuId = menuIdDepths[0];
			var depths = menuIdDepths[1];
			
			$('#popUpA-ulDepth3').find('li').remove();
			
			var html = '<li class="firstNum">3차메뉴</li> ';
			jsonList.forEach(function (val, index, array) {
				if(3 == val.displayLevel && menuId == val.upperMenuId) {
					html += makeHtml(val.displayLevel, val.menuId, val.menuNm);
				}
			});
			$('#popUpA-ulDepth3').append(html);
			
			pageEvents();
    	}
	});
    
	// 3차메뉴 선택
    $('#popUpA-ulDepth3').find('li').unbind('click').bind('click',function(){
    	if(!$(this).hasClass('active') && !$(this).hasClass('firstNum')) {
			$(".newMenu").remove();
			$(".menuAdd").show();
			$(".menuDel").hide();	
	    	$('#popUpA-ulDepth3').find('li').removeClass('active');
	    	$(this).addClass('active');
			pageEvents();
    	}
	});	
	
    // 메뉴 순서 이동
    $('.cont i').unbind('click').bind('click',function(){  
    	var $li = $(this).closest('li');       
        if($(this).attr('class')=="ti-arrow-up") {
        	if($li.prev().attr('class') != 'firstNum') {
        		$li.insertBefore($li.prev());	
        	}	
        } 	
        if($(this).attr('class')=="ti-arrow-down") 
        	$li.insertAfter($li.next());
    });
		
	// 등록 레이어팝업창 취소 선택
    $('#btnA-cancel, #btnM-cancel, .closePop i').unbind('click').bind('click',function(){        
        // 레이어팝업 창 값 초기화 
        initFormValues();      
        fn_util_divPopUpControl('divPopUpA-MenuMgmt',false);
    });

	// 수정 레이어팝업창 취소 선택
    $('#btnM-cancel, .closePop i').unbind('click').bind('click',function(){        
        // 레이어팝업 창 값 초기화 
        initFormValues();       
        fn_util_divPopUpControl('divPopUpM-MenuMgmt',false);
    });	

    // 사용자메뉴 권한 추가등록 선택
    $('#btnPopUpAdd').unbind('click').bind('click',function(){  	
    	fn_util_divPopUpControl('divPopUpA-MenuMgmt',true);
    	var url = controller + '/insertPopUpMenu.do';

	    $.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        contentType: "application/json",
	        success: function(resultData) 
	        {        	       					
				jsonList = JSON.parse(resultData.jsonList);
	        	var html = '';
	        	var authorid_html = '';
	        	// 1차 레벨 메뉴 
				$.each(resultData.result, function(index, item) {
					if(resultData.result[index].displayLevel=='1') {	
						html += makeHtml(resultData.result[index].displayLevel, resultData.result[index].seq, resultData.result[index].menuNm);
					}
				}); 
				$('#popUpA-ulDepth1').append(html);
				// ajax 호출후 다시 이벤트 생성.
				pageEvents(); 
	        },
	        error : function(request, status, error) 
	        {
	            alert('서버와의 통신에 실패하였습니다.');
	        }
	    });	
    });

    $('#btnM-update, #btnM-delete').unbind('click').bind('click',function(){
        updateMenu();
    });

    $('#btnA-insert').unbind('click').bind('click',function(){  
       createMenu();
    });
     
    $('.btn_move').unbind('click').bind('click',function(){ 
    	var $li = $(this).closest('li');
        if($(this).attr('class')=="btn_move up") {  
        	alert($li.prev().attr('class'));
        	if($li.prev().attr('class') != 'firstNum') 
        		$li.insertBefore($li.prev());
        }
        
        if($(this).attr('class')=="btn_move down") 
        	$li.insertAfter($li.next());
    });
        
     // 권한부여 체크박스 선택
     $('#popUpA-chkAdminP, #popUpA-chkAdminS, #popUpA-chkUserS, #popUpM-chkAdminP, #popUpM-chkAdminS, #popUpM-chkUserS').unbind('click').bind('click',function(){
   	 
    	 if($(this).prop("checked")) {	 
        	 if($('.newMenu').val()!=0) {
        		 alert('메뉴를 먼저 추가 하신 후에 해당 권한을 선택하세요.');
        		 $(this).prop("checked", false);
        		 return;
        	 }    		 
    		 if($(this).attr('auth')!='Y') {
    			 alert('상위메뉴에 해당권한을 먼저 추가 하신 후에 선택하세요.');
    			 $(this).prop("checked", false);
    			 return;
    		 }
    	 }
     });
        
 	// 메뉴 수정창 오픈
    $('.btnModifyRow').unbind('click').bind('click',function() {	
		var seq = $(this).parent().parent().attr('id');
		var upperSeq = $(this).parent().parent().attr('upper-id');
		var url = controller + '/menuSelect.do';
		var param = new Object();

		param.seq = seq;
		param.upperMenuId = upperSeq;
		MENU_ID = seq;
		UPPER_MENU_ID = upperSeq;
 	    fn_util_divPopUpControl('divPopUpM-MenuMgmt',true);
 	    
 		// RC_BD_MENU 테이블 seq필드 값 .
 	    if(seq == 15) {
 	    	// 메뉴권한관리 수정 화면에서 사용유무 숨김처리
 	    	$('#MenuUseYn').hide();
 	    }
	    
	    $.ajax({
	        type: "POST",
	        url: url,
	        data: param,
	        async:false,
	        success: function(resultData) 
	        {            	
	        	fn_setMenuMgmtRow(resultData);
	        	
	        	var htmlDepth1 = '';
	        	var htmlDepth2 = '';
	        	var htmlDepth3 = '';
	        	var upperMenuPath = '';
	        	
	        	// 선택한 메뉴 활성
				$.each(resultData.result, function(index, item) {
 					if(resultData.result[index].displayLevel=='1') {			
						if(resultData.menu.seq == resultData.result[index].seq) {						
							htmlDepth1 += '<li id="'+ resultData.result[index].seq + "_" + resultData.result[index].displayLevel +'"';
							htmlDepth1 += 'class="cont newMenu active">'+$("#popUpM-menuNm").val();
							htmlDepth1 += "&nbsp;&nbsp;&nbsp";
							htmlDepth1 += "<i class='ti-arrow-up'></i>";
							htmlDepth1 += "&nbsp;&nbsp;";
							htmlDepth1 += "<i class='ti-arrow-down'></i></li>";	
						} else {
							htmlDepth1 += makeHtml(resultData.result[index].displayLevel, resultData.result[index].seq, resultData.result[index].menuNm);						
						}
					}	
					if(resultData.result[index].displayLevel=='2') {
						if(index == 0) {
							   upperMenuPath = resultData.result[index].menuPath;
							   var meunAry = new Array();
							   menuAry = upperMenuPath.split('/');
							   htmlDepth1 += menuAry[0];
						}
						if(resultData.menu.seq == resultData.result[index].seq) {							
							htmlDepth2 += '<li id="'+ resultData.result[index].seq + "_" + resultData.result[index].displayLevel +'"';
							htmlDepth2 += 'class="cont newMenu active">'+$("#popUpM-menuNm").val();
							htmlDepth2 += "&nbsp;&nbsp;&nbsp";
							htmlDepth2 += "<i class='ti-arrow-up'></i>";
							htmlDepth2 += "&nbsp;&nbsp;";
							htmlDepth2 += "<i class='ti-arrow-down'></i></li>";							
						} else {						
							htmlDepth2 += makeHtml(resultData.result[index].displayLevel, resultData.result[index].seq, resultData.result[index].menuNm);							
						}
					}									
					if(resultData.result[index].displayLevel=='3') { 
						if(index == 0) {
							   upperMenuPath = resultData.result[index].menuPath;
							   var meunAry = new Array();
							   menuAry = upperMenuPath.split('/');
							   htmlDepth1 += menuAry[0];
							   htmlDepth2 += menuAry[1];
						}
						if(resultData.menu.seq == resultData.result[index].seq) {
							htmlDepth3 += '<li id="'+ resultData.result[index].seq + "_" + resultData.result[index].displayLevel +'"';
							htmlDepth3 += 'class="cont newMenu active">'+$("#popUpM-menuNm").val();
							htmlDepth3 += "&nbsp;&nbsp;&nbsp";
							htmlDepth3 += "<i class='ti-arrow-up'></i>";
							htmlDepth3 += "&nbsp;&nbsp;";
							htmlDepth3 += "<i class='ti-arrow-down'></i></li>";
						} else {
							htmlDepth3 += makeHtml(resultData.result[index].displayLevel, resultData.result[index].seq, resultData.result[index].menuNm);
						}
					} 
				});
	        	// 사용여부 체크
				if(resultData.menu.useYn == "Y")
					$("input:radio[id='indictYnM1']").prop("checked", true);
				else
					$("input:radio[id='indictYnM2']").prop("checked", true);
				
	        	// 권한부여 체크
				$.each(resultData.userAuthList, function(index, item) {
					// 관리자
 					if(resultData.userAuthList[index].role_id == 1)
						$("input:checkbox[id='popUpM-chkAdminP']").prop("checked", true);
					// 담당자
 					if(resultData.userAuthList[index].role_id == 2)
						$("input:checkbox[id='popUpM-chkAdminS']").prop("checked", true);
					// 사용자
 					if(resultData.userAuthList[index].role_id == 3)
						$("input:checkbox[id='popUpM-chkUserS']").prop("checked", true);
				});
	        	                
                // 1차메뉴 수정
                if(UPPER_MENU_ID == 0) {
                    $('#popUpM-chkAdminP').attr('auth', 'Y');
                    $('#popUpM-chkAdminS').attr('auth', 'Y');
                    $('#popUpM-chkUserS').attr('auth', 'Y');
                } else {
                	// 2차,3차 메뉴 수정
                	if(resultData.resultList.length > 0) {   
                		$.each(resultData.resultList, function(index, item) {
                    		if(resultData.resultList[index].AUTHORID=='1') 
                            	$('#popUpM-chkAdminP').attr('auth', 'Y');
                        	if(resultData.resultList[index].AUTHORID=='2') 
                            	$('#popUpM-chkAdminS').attr('auth', 'Y');
                        	if(resultData.resultList[index].AUTHORID=='3') 
                            	$('#popUpM-chkUserS').attr('auth', 'Y'); 
                    	});
                	}
                }	        	
				$('#popUpM-ulDepth1').append(htmlDepth1);
				$('#popUpM-ulDepth2').append(htmlDepth2);
				$('#popUpM-ulDepth3').append(htmlDepth3);			
				$('.ti-arrow-up, .ti-arrow-down').css('cursor','pointer'); // 커서 포인터
				pageEvents(); // 동적요소 추가후 이벤트 재등록
	        },
	        error : function(request, status, error) 
	        {
	            alert('서버와의 통신에 실패하였습니다.');
	        }
	    });		            
	});
}

//메뉴 수정폼 값 셋팅
function fn_setMenuMgmtRow(data,status){
    var resultData = data.menu;
    var seq = resultData.seq;
    var menuNm = resultData.menuNm;
    var menuDc = resultData.menuDc;
    var menuUrl = resultData.menuUrl;	
    var upperMenuId = resultData.upperMenuId;
    var sortOrder = resultData.sortOrder;
    var useYn = resultData.useYn;
    var delYn = resultData.delYn;   
    
    $('#popUpM-menuNm').val(menuNm);    // 메뉴명
    $('#popUpM-menuUrl').val(menuUrl);  // 메뉴주소
    $('#popUpM-menuDc').val(menuDc);    // 메뉴설명
}


function makeHtml(displayLevel, menuId, menuNm) { 	
	html = '' , id = '', cont = '', tail = '', head = '';
	id = menuId+'_'+displayLevel;
	cont = "<div class='cont'><i class='ti-arrow-up'></i><i class='ti-arrow-down'></i></div>";
	tail = '</span> '+cont+'</li>';
	head = '<li id="'+id+'"><span>';
	html = head + menuNm + tail; 	
	return html;
}

//리스트에 메뉴 추가
function menuAdd() {
	var menuLevel = ''; // 메뉴레벨
	var upperMenuId = ''; // 상위메뉴 아이디
	
	if('' == $('#popUpA-menuNm').val()) {
		alert('메뉴명을 입력해 주세요');
		$('#popUpA-menuNm').focus();
		return false;
	}
	
	if('' == $("#popUpA-menuDc").val()) {
		alert("메뉴설명을 입력해 주세요");
		$("#popUpA-menuDc").focus();
		return false;
	}
	
	if('' == $("#popUpA-menuUrl").val()) {
		alert("URL을 입력해 주세요");
		$("#popUpA-menuUrl").focus();
		return false;
	}
	
	if('0' == $("#popUpA-optDepth").val()) {
		alert("메뉴레벨을 선택해 주세요");
		$("#popUpA-optDepth").focus();
		return false;
	}
	
	// 2레벨을 선택시 1레벨이 비어있다면 취소.  클래스명 확인
	if('2' == $("#popUpA-optDepth").val()) {
		var cnt = 0;
		$('#popUpA-ulDepth1').find('li').each(function(){
			if($(this).hasClass('active')) cnt++; 
		});
		if(0 == cnt) {
			alert("1차 메뉴를 선택해주세요");
			return false;
		}
	}
	
	// 3레벨을 선택시 2레벨이 비어있다면 취소. 
	if('3' == $("#popUpA-optDepth").val()) {
		var cnt = 0;
		$('#popUpA-ulDepth2').find('li').each(function(){
			if($(this).hasClass('active')) cnt++; 
		});
		if(0 == cnt) {
			alert("2차 메뉴를 선택해주세요");
			return false;
		}
	}
	
	var id = '_'+$("#popUpA-optDepth").val();
	var html = "";
    html += '<li id="'+id+'"';
	html += 'class="cont newMenu active">'+$("#popUpA-menuNm").val();
	html += "&nbsp;&nbsp;&nbsp";
	html += "<i class='ti-arrow-up'></i>";
	html += "&nbsp;&nbsp;";
	html += "<i class='ti-arrow-down'></i></li>";
	
	$('#popUpA-ulDepth'+$("#popUpA-optDepth").val()).find('li').removeClass('active');		
	$('#popUpA-ulDepth'+$("#popUpA-optDepth").val()).append(html);
	$('.ti-arrow-up, .ti-arrow-down').css('cursor','pointer'); // 커서 포인터
	$(".menuAdd").hide();
	$(".menuDel").show();
	
	pageEvents();	
	menuLevel = $("#popUpA-optDepth").val();
	
	// 2차메뉴,3차메뉴일때 상위메뉴에 권한을 상속.
	if(menuLevel == '2' || menuLevel == '3') {
		if(menuLevel == '2') {		
			$('#popUpA-ulDepth1').find('li').each(function(){
				if($(this).hasClass('active')) {
					var menuIdDepths = $(this).attr('id').split('_');
					upperMenuId =menuIdDepths[0];
				}	
			});	
		}
		else if(menuLevel == '3') {
			$('#popUpA-ulDepth2').find('li').each(function(){
				if($(this).hasClass('active')) {
					var menuIdDepths = $(this).attr('id').split('_');
					upperMenuId =menuIdDepths[0];
				}	
			});		
		}
			
		// 상위메뉴 권한 조회
		var pUrl = "/administrator/menu/menuAuthChek.do";
		var param = new Object();
		param.menuId = upperMenuId;
	    
		$.ajax({
	        type: 'POST',
	        url: pUrl,
	        data: param,
	        async:false,
	        success:function(data) {
	        	if(data.resultList.length > 0) {
	        		
						$.each(data.resultList, function(index, item) {
 						if(data.resultList[index].AUTHORID=='1') 
 							$('#popUpA-chkAdminP').attr('auth', 'Y');
 						if(data.resultList[index].AUTHORID=='2') 
 							$('#popUpA-chkAdminS').attr('auth', 'Y');
 						if(data.resultList[index].AUTHORID=='3') 
 							$('#popUpA-chkUserS').attr('auth', 'Y'); 
 					});
	        	}
	        },
	        error:function(e){
	            alert('서버와의 통신에 실패하였습니다.');
	        }
	    });	
	}
	else {
		// 1차레벨 메뉴 추가 - 모든 권한 선택가능'
	    $('#popUpA-chkAdminP').attr('auth', 'Y');
	    $('#popUpA-chkAdminS').attr('auth', 'Y');
	    $('#popUpA-chkUserS').attr('auth', 'Y');
	}
}

function menuDel(){
	$(".newMenu").remove();	
	$(".menuAdd").show();
	$(".menuDel").hide();
	
	//권한체크 모두 해제
    $('#popUpA-chkAdminP').attr('auth', '');
    $('#popUpA-chkAdminS').attr('auth', '');
    $('#popUpA-chkUserS').attr('auth', '');
    $("input:checkbox[id='popUpA-chkAdminP']").prop("checked", false);
    $("input:checkbox[id='popUpA-chkAdminS']").prop("checked", false);
    $("input:checkbox[id='popUpA-chkUserS']").prop("checked", false);
	pageEvents();
}

// 메뉴 수정
function updateMenu() {
	var p_upperMenuId = UPPER_MENU_ID;  // 상위메뉴 아이디
	var p_updateMenuId = MENU_ID;   	// 수정대상 아이디
	var p_indictYn = '';                // 사용여부
	var p_menuId =  [];      			// 메뉴목록
	var p_auth =  [];        			// 권한 종류(1:관리자 ,2:담당자, 3:사용자)
	var authCnt = 0;                    // 권한체크
	
	$("input:checkbox").each(function( key ) {
		if($(this).prop("checked")) {
			p_auth[authCnt] = $(this).val();
			authCnt++;
		}
	});
	
	if(0 == authCnt) {
		alert('권한을 체크해 주세요');
		return false;
	}

	$("input:radio[name='indictYnM']").each(function( key ) {
		if($(this).prop("checked")) {	
			p_indictYn = $(this).val();		
		}	
	});
	
	var menuIdDepths = $(".newMenu").attr('id').split('_');
	var newMenuId = menuIdDepths[0];
	var depths = menuIdDepths[1];
	var index = 0;
	
	if(1 == depths) {
		p_upperMenuId = 0;
		$('#popUpM-ulDepth1').find('li').each(function(){
			if(!$(this).hasClass('firstNum')) {
				var menuIdDepth = $(this).attr('id').split('_');
				var menuId = menuIdDepth[0];
				p_menuId[index] = menuId;
				index++;
			}
		});
	} else if(2 == depths) {
		$('#popUpM-ulDepth2').find('li').each(function(){
 			if($(this).hasClass('active')) {
				var menuIdDepth = $(this).attr('id').split('_');
				p_upperMenuId = menuIdDepth[0];
			} 
		});
		$('#popUpM-ulDepth2').find('li').each(function(){
			if(!$(this).hasClass('firstNum')) {
				var menuIdDepth = $(this).attr('id').split('_');
				var menuId = menuIdDepth[0];
				p_menuId[index] = menuId;
				index++;
			}
		});
	} else if(3 == depths) {
	    $('#popUpM-ulDepth3').find('li').each(function(){	    	
 	    	if($(this).hasClass('active')) {
				var menuIdDepth = $(this).attr('id').split('_');
				p_upperMenuId = menuIdDepth[0];
	    	} 
		});
	    $('#popUpM-ulDepth3').find('li').each(function(){
	    	if(!$(this).hasClass('firstNum')) {
				var menuIdDepth = $(this).attr('id').split('_');
				var menuId = menuIdDepth[0];
				p_menuId[index] = menuId;
				index++;
	    	}
		});
	}
	
	var pUrl = '';
	var param = new Object();
		
	if (confirm("수정 하시겠습니까?")) {
		pUrl = "/administrator/menu/updateMenu.do";
		param = new Object();		
        param.menuNm = $("#popUpM-menuNm").val();
        param.description = $("#popUpM-menuDc").val();
        param.url = $("#popUpM-menuUrl").val();
        param.level = depths;
        param.indictYn = p_indictYn; // 사용여부
        (p_upperMenuId == 0)?param.upperMenuId = null:param.upperMenuId = UPPER_MENU_ID;
        param.updateMenuId = p_updateMenuId;		
        param.menuId = p_menuId;
        param.auth = p_auth;
		
		$.ajax({
	        type: 'POST',
	        url: pUrl,
	        data: param,
	        async:false,
	        success:function(resultData){
	            if(resultData != null){	            	
	            	location.reload();                
	            }
	        },
	        error:function(e){
	            alert('서버와의 통신에 실패하였습니다.');
	        }
	    }); 		
	}	
}


// 메뉴 저장
function createMenu() {
	var cnt = 0;
	$('#popUpA-ulDepth1').find('li').each(function(){
		if($(this).hasClass('newMenu')) {
			cnt++;
		}
	});
	
	$('#popUpA-ulDepth2').find('li').each(function(){
		if($(this).hasClass('newMenu')) {
			cnt++;
		}
	});
	
	$('#popUpA-ulDepth3').find('li').each(function(){
		if($(this).hasClass('newMenu')) {
			cnt++;
		}
	});
	
	if(0 == cnt) {
		alert('메뉴를 추가해주세요');
		return false;
	}
	
	var p_upperMenuId = '';  // 상위메뉴 아이디
	var p_indictYn = '';     // 사용여부
	var p_menuId =  [];      // 메뉴 아이디
	var p_auth =  [];        // 권한 종류(1:관리자, 2:매니져, 3:유저)
	var authCnt = 0;         // 권한체크
	
	// 권한부여 체크박스 선택
	$("input:checkbox").each(function( key ) {		
		if($(this).prop("checked")) {
			p_auth[authCnt] = $(this).val();
			authCnt++;
		}
	});
	
	if(0 == authCnt) {
		alert('권한을 체크해 주세요');
		return false;
	}
	
	// 사용여부 체크
 	$("input:radio").each(function( key ) {
		if($(this).prop("checked")) {
			p_indictYn = $(this).val();
		}
	});
	
	var menuIdDepths = $(".newMenu").attr('id').split('_');
	var newMenuId = menuIdDepths[0];
	var depths = menuIdDepths[1];
	var index = 0;
	
	if(1 == depths) {		
		p_upperMenuId = 0;
		$('#popUpA-ulDepth1').find('li').each(function(){
			if(!$(this).hasClass('firstNum')) {
				var menuIdDepth = $(this).attr('id').split('_');
				var menuId = menuIdDepth[0];
				p_menuId[index] = menuId;
				index++;
			}
		});
	} else if(2 == depths) {
		$('#popUpA-ulDepth1').find('li').each(function(){
			if($(this).hasClass('active')) {
				var menuIdDepth = $(this).attr('id').split('_');
				p_upperMenuId = menuIdDepth[0];
			}
		});
		$('#popUpA-ulDepth2').find('li').each(function(){
			if(!$(this).hasClass('firstNum')) {
				var menuIdDepth = $(this).attr('id').split('_');
				var menuId = menuIdDepth[0];
				p_menuId[index] = menuId;
				index++;
			}
		});
	} else if(3 == depths) {
	    $('#popUpA-ulDepth2').find('li').each(function(){	    	
	    	if($(this).hasClass('active')) {
				var menuIdDepth = $(this).attr('id').split('_');
				p_upperMenuId = menuIdDepth[0];
	    	}
		});
	    $('#popUpA-ulDepth3').find('li').each(function(){
	    	if(!$(this).hasClass('firstNum')) {
				var menuIdDepth = $(this).attr('id').split('_');
				var menuId = menuIdDepth[0];
				p_menuId[index] = menuId;
				index++;
	    	}
		});
	}
				
	// 상위메뉴 권한 체크
	var check = 0;
	var count = 0;
	var param = new Object();
	
	// 2차,3차메뉴 등록
	if(0 != p_upperMenuId) {
		var pUrl = "/administrator/menu/menuAuthChek.do";
		var checkCnt = 0;
		param.menuId = p_upperMenuId;
        
		$.ajax({
	        type: 'POST',
	        url: pUrl,
	        data: param,
	        async:false,
	        success:function(data) {
	        	count = data.resultList.length;
	        	$("input:checkbox").each(function( key ) {
	        		if($(this).prop("checked")) {
	        			var authId = $(this).val();
						for(var i in data.resultList) {
							if(authId == data.resultList[i].AUTHORID) {
								checkCnt++;
							 } 
						}
					}
					if(0 == checkCnt) {
						check++;
					}
	        	});
	        },
	        error:function(e){
	            alert('서버와의 통신에 실패하였습니다.');
	        }
	    });	
	}
			
	if (confirm("등록 하시겠습니까?")) {
		if (depths==1) {
			$("#popUpA-menuUrl").val("/");
		}
		pUrl = "/administrator/menu/insertMenu.do";
		param = new Object();		
        param.menuNm = $("#popUpA-menuNm").val();
        param.description = $("#popUpA-menuDc").val();
        param.url = $("#popUpA-menuUrl").val();
        param.level = depths;
        param.indictYn = p_indictYn; // 사용여부
        (p_upperMenuId == 0)?param.upperMenuId = null:param.upperMenuId = p_upperMenuId;
		param.menuId = p_menuId;
		param.auth = p_auth;
		
		$.ajax({
	        type: 'POST',
	        url: pUrl,
	        data: param,
	        async:false,
	        success:function(resultData){
	            if(resultData != null){	            	
	            	location.reload();                
	            }
	        },
	        error:function(e){
	            alert('서버와의 통신에 실패하였습니다.');
	        }
	    });  	       
   }
}
		
function initFormValues(){
	// 등록 팝업창 메뉴 내용 초기화
	$('#popUpA-menuNm').val('');
	$('#popUpA-menuDc').val('');
	$('#popUpA-menuUrl').val('');
	$('#popUpA-optDepth').val(0);
    $('#popUpA-ulDepth1').html('');
    $('#popUpA-ulDepth2').html('');
    $('#popUpA-ulDepth3').html('');
    				
 	// 등록 팝업창 사용여부 , 권한부여 초기화
    $("input:radio[id='indictYn1']").prop("checked", true);
    $("input:checkbox[id='popUpA-chkAdminP']").prop("checked", false);
    $("input:checkbox[id='popUpA-chkAdminS']").prop("checked", false);
    $("input:checkbox[id='popUpA-chkUserS']").prop("checked", false);

    // 수정 팝업창 메뉴 내용 초기화
    $('#popUpM-menuNm').val('');    // 메뉴명
    $('#popUpM-menuUrl').val('');   // 메뉴주소
    $('#popUpM-menuDc').val('');    // 메뉴설명
    $('#popUpM-ulDepth1').html('');
    $('#popUpM-ulDepth2').html('');
    $('#popUpM-ulDepth3').html('');
    
    // 수정팝업창 사용여부 , 권한부여 초기화
    $("input:checkbox[id='popUpM-chkAdminP']").prop("checked", false);
    $("input:checkbox[id='popUpM-chkAdminS']").prop("checked", false);
    $("input:checkbox[id='popUpM-chkUserS']").prop("checked", false);
    
    // 권한부여 초기화
    $('#popUpA-chkAdminP').attr('auth', '');
    $('#popUpA-chkAdminS').attr('auth', '');
    $('#popUpA-chkUserS').attr('auth', '');
    $('#popUpM-chkAdminP').attr('auth', '');
    $('#popUpM-chkAdminS').attr('auth', '');
    $('#popUpM-chkUserS').attr('auth', '');
    
    // 메뉴레벨(추가,삭제)버튼 
    $(".menuAdd").show();
    $(".menuDel").hide();
    
    // 메뉴권한관리 사용여부 
    $('#MenuUseYn').show();
}

function linkPage(pageNo) {
    $("#currentPageNo").val(pageNo);

    $("#listForm").attr({
        action : "menuList.do",
        method : "post"
    }).submit();
}

function goOrder(order) {
    $("#listOrder").val(order);

    $("#listForm").attr({
        action : "menuList.do",
        method : "post"
    }).submit();
}

</script>
<div class="mConBox">
    <div class="mCont">
        <div class="row">
            <div class="col-md-4" id="subtitleA">
                <h2><i class="ti-layers"></i>메뉴 권한관리</h2>
            </div>
            <div class="col-md-8">
                <div class="pageRoute">
                    <ul>
                        <li><a href="#">HOME</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li><a href="#">시스템 관리</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li><a href="#">시스템 관리</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li class="current"><a href="#">메뉴 권한관리</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end row -->
        <form:form modelAttribute="menuParam" id="listForm" method="get">          
             <input type="hidden" name="hidden_menuId"       id="hidden_menuId"       value=""/>
			 <input type="hidden" name="hidden_upperMenuId"  id="hidden_upperMenuId"  value=""/>
			 <input type="hidden" name="hidden_displayLevel" id="hidden_displayLevel" value=""/>

            <div class="row">
                <div class="col-md-12">
                    <div class="conBox" style="min-height:400px;">
                        <div class="row">
                            <div class="col-md-33 select-set-3">
                                <p> 총 <b>${menuParam.totalRecordCount}</b> 사용자 메뉴</p>
                            </div>
                            <div class="col-md-9 text-right select-sys-1">
                                <button type="button" title="사용자 메뉴 권한을 추가할 수 있습니다." id="btnPopUpAdd" class="AddDefaultBtn">
                                    <i class="fa fa-plus"></i> 사용자메뉴 권한 추가등록
                                </button>
                            </div>
                        </div>
                        <!-- end row > conBox > col-md-12 > row > form -->
                        <div class="row">
                            <div class="col-md-12">
                                <table class="viewDateTbl menuSetTbl" id="menuSetTbl">
                                    <colgroup>
                                        <col width="80">
                                        <col width="80">
                                        <col width="360">
                                        <col width="400">
                                        <col width="160">
                                        <col width="160">
                                        <col width="120">
                                        <col width="120">
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th>레벨</th>
                                            <th>순서</th>
                                            <th>메뉴</th>
                                            <th>주소</th>
                                            <th>등록일</th>
                                            <th>수정일</th>
                                            <th>사용여부</th>
                                            <th>상세관리</th>
                                        </tr>
                                    </thead>
                    <c:if test="${empty menuList}">
                                    <tbody><tr><td colspan="8">No data available in table</td></tr></tbody>
                    </c:if>                  
                    <c:if test="${not empty menuList}">
                        <c:forEach var="list" items="${menuList}" varStatus="status">
                            <c:if test="${status.first}">
                                    <tbody class="tbGroup">
                            </c:if>
                            <c:choose>
                                <c:when test='${list.displayLevel == 1}'>
                                    </tbody>
                                    <tbody class="tbGroup">
                                        <tr id="${list.seq}" upper-id="${list.upperMenuId}" class="lvAdepth" combinedDepth="${list.depth1st}_${list.depth2nd}_${list.depth3rd}">
                                </c:when>
                                <c:when test='${list.displayLevel == 2}'>
                                        <tr id="${list.seq}" upper-id="${list.upperMenuId}" class="lvBdepth" combinedDepth="${list.depth1st}_${list.depth2nd}_${list.depth3rd}">
                                </c:when>
                                <c:when test='${list.displayLevel == 3}'>
                                        <tr id="${list.seq}" upper-id="${list.upperMenuId}" class="lvCdepth" combinedDepth="${list.depth1st}_${list.depth2nd}_${list.depth3rd}">
                                </c:when>
                                <c:otherwise>
                                        <tr id="${list.seq}" upper-id="${list.upperMenuId}" combinedDepth="${list.depth1st}_${list.depth2nd}_${list.depth3rd}">
                                </c:otherwise>
                            </c:choose>
                                            <td>${list.displayLevel}</td>
                                            <td>${list.sortOrder}</td>
                                            <td class="txtL">${list.menuNm}</td>
                                            <td class="txtL">${list.menuUrl}</td>
                                            <td>
                                                <fmt:formatDate value="${list.createDt}" pattern="yyyy-MM-dd"/>
                                                <br />
                                                <fmt:formatDate value="${list.createDt}" pattern="HH:mm:ss"/>
                                            </td>
                                            <td>
                                                <fmt:formatDate value="${list.modifyDt}" pattern="yyyy-MM-dd"/>
                                                <br />
                                                <fmt:formatDate value="${list.modifyDt}" pattern="HH:mm:ss"/>
                                            </td>
                                            <c:choose>
                                            	 <c:when test='${list.useYn == "Y"}'>
                                            	 	<td>사용</td>
                                            	 </c:when>
                                            	 <c:otherwise>
                                            	 	<td>미사용</td>
                                            	 </c:otherwise>
                                            </c:choose>
                                            <td><button type="button" class="tblInBtn btnModifyRow">수정</button></td>
                                        </tr>
                            <c:if test="${status.last}">
                                    </tbody>
                            </c:if>
                        </c:forEach>
                    </c:if>
                                </table>
                            </div>
                        </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-md-2"></div>
							<%--                             
							<div class="col-md-8 pageNum">
                                <!-- Paging With Taglib -->
                                <ui:pagination paginationInfo="${menuParam}" type="css" jsFunction="linkPage" />
                                <form:hidden path="currentPageNo" />
                            </div> --%>
                            <div class="col-md-2 select-set-3"></div>
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end conBox -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </form:form>
    </div>
    <!-- end mCont -->
</div>
<!-- end mConBox -->

<!-- 메뉴권한 추가 팝업레이어 -->
<div id="divPopUpA-MenuMgmt" class="popUp col-md-8">
    <div class="popUpTop">사용자 메뉴권한 추가
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="accountAddRow.do" method="POST" modelAttribute="accountParamInfo" id="addForm" autocomplete="off">
                <input type="hidden" id="popUpA-combinedSeq-h"  name="combinedSeq"  value="" />
                <input type="hidden" id="popUpA-upperMenuId-h"  name="upperMenuId"  value="0" />
                <input type="hidden" id="popUpA-displayLevel-h" name="displayLevel" value="0" />       
                <input type="hidden" id="popUpA-indictYn" name="indictYn" value="Y" />
                <table id="tblPopUpA" class="popInTbl">
                    <colgroup>
                        <col width="20%"/>
                        <col width="80%" />
                    </colgroup>
                    <tr>
                        <th><label for="popUpA-menuNm">메뉴명</label></th>
                        <td colspan="3">
                            <input type="text" id="popUpA-menuNm" class="w100p" name="menuNm" placeholder="메뉴명을 입력하세요" autocomplete="off" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-menuDc">메뉴설명</label></th>
                        <td colspan="3">
                            <input type="text" id="popUpA-menuDc" class="w100p" name="description" placeholder="메뉴설명을 입력하세요" autocomplete="off" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-menuUrl">메뉴주소</label></th>
                        <td colspan="3">
                            <input type="text" id="popUpA-menuUrl" class="w100p" name="url" placeholder="메뉴주소를 입력하세요" autocomplete="off" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="">메뉴레벨</label></th>
                        <td colspan="3">
                            <select id="popUpA-optDepth" name="level">
                                <option value="0">레벨 선택</option>
                                <option value="1">1차 메뉴</option>
                                <option value="2">2차 메뉴</option>
                                <option value="3">3차 메뉴</option>
                            </select>
                            <button type="button" class="button bt2 menuAdd" onClick="javascript:menuAdd(); return false;">추가</button>
                            <button type="button" class="button bt2 red_l menuDel" style="display:none" onClick="javascript:menuDel(); return false;">삭제</button>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="">메뉴순서</label></th>
                        <td colspan="3" >
                            <div id="popUpA-menuTree" class="menuSetBox">
                                <p>* 메뉴명과 설명을 입력한 후에 메뉴위치를 선택해주세요!</p>
                                <div class="menuListBox">
                                    <div class="menuBoxInner">
                                        <ul id="popUpA-ulDepth1" >
											<li class="firstNum">1차 메뉴</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="menuListBox">
                                    <div class="menuBoxInner">
                                        <ul id="popUpA-ulDepth2" >
                                        <li class="firstNum">2차 메뉴</li>                                    
                                        </ul>
                                    </div>
                                </div>
                                <div class="menuListBox">
                                    <div class="menuBoxInner">
                                        <ul id="popUpA-ulDepth3" class="cDisable">
                                        	<li class="firstNum">3차 메뉴</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>               
					<tr>
                        <th scope="row"><label for="">사용여부</label></th>
                        <td>
                            <input type="radio" class="rdRdo" id="indictYn1" name="indictYn" value="Y" checked>사용
                            <label for="indictYn1" class="rdSel"></label>
                            <input type="radio" class="rdRdo" id="indictYn2" name="indictYn" value="N">미사용
                            <label for="indictYn2" class="rdSel"></label>
                        </td>
                    </tr>                  				
					 <tr>
                        <th scope="row"><label for="">권한부여</label></th>
                        <td colspan="3">
                            <table class="popInTblInner">
                                <colgroup>
                                    <col width="" />
                                    <col width="" />
                                </colgroup>
                                <tbody id="popUpA-tbodyData-role">
                                    <tr>
                                        <td>관리자 (ADMIN)</td>
                                        <td class="txtC">
                                            <input type="checkbox" class="rdChk" id="popUpA-chkAdminP" auth="" name="chkAdminP" value="1"/>
                                            <label class="chkSel" for="popUpA-chkAdminP"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>담당자 (MANAGER)</td>
                                        <td class="txtC">
                                            <input type="checkbox" class="rdChk" id="popUpA-chkAdminS" auth="" name="chkAdminS" value="2"/>
                                            <label class="chkSel" for="popUpA-chkAdminS"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>사용자 (USER)</td>
                                        <td class="txtC">
                                            <input type="checkbox" class="rdChk" id="popUpA-chkUserS" auth="" name="chkUserS" value="3"/>
                                            <label class="chkSel" for="popUpA-chkUserS"></label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>                  
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">      
			<button type="button" id="btnA-insert" class="btn-style1 darkGrayBtn">저장</button>
            <button type="button" id="btnA-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button> 
        </div>
    </div>
</div>
<!--메뉴권한 추가 팝업레이어-->

<!--메뉴권한 수정 팝업레이어-->
<div id="divPopUpM-MenuMgmt" class="popUp col-md-8">
    <div class="popUpTop">사용자메뉴 권한수정
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form action="menuModifyRow.do" method="POST" modelAttribute="menuInfo" id="modifyForm" autocomplete="off">
                <input type="hidden" id="popUpM-controlMode-h"  name="controlMode"  value="M" />
                <input type="hidden" id="popUpM-seq-h"          name="seq"          value="0" />
                <input type="hidden" id="popUpM-displayLevel-h" name="displayLevel" value="0" />
                <input type="hidden" id="popUpM-combinedSeq-h"  name="combinedSeq"  value="" />

                <table id="tblPopUpM" class="popInTbl">
                    <colgroup>
                        <col width="20%"/>
                        <col width="80%" />
                    </colgroup>                
                    <tr>
                        <th><label for="popUpM-menuNm">메뉴명</label></th>
                        <td colspan="3">
                            <input type="text" id="popUpM-menuNm" class="w100p" name="menuNm" autocomplete="off">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-menuDc">메뉴설명</label></th>
                        <td colspan="3">
                            <input type="text" id="popUpM-menuDc" class="w100p" name="menuDc" autocomplete="off">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-menuUrl">메뉴주소</label></th>
                        <td colspan="3">
                            <input type="text" id="popUpM-menuUrl" class="w100p" name="menuUrl" autocomplete="off">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="">메뉴순서</label></th>
                        <td colspan="3" >
                            <div class="menuSetBox">
                                <div class="menuListBox">
                                    <h4>1차 메뉴</h4>
                                    <div class="menuBoxInner">
                                        <ul id="popUpM-ulDepth1" class="cDisable">
                                        </ul>
                                    </div>
                                </div>
                                <div class="menuListBox">
                                    <h4>2차 메뉴</h4>
                                    <div class="menuBoxInner">
                                        <ul id="popUpM-ulDepth2" class="cDisable">
                                        </ul>
                                    </div>
                                </div>
                                <div class="menuListBox">
                                    <h4>3차 메뉴</h4>
                                    <div class="menuBoxInner">
                                        <ul id="popUpM-ulDepth3" class="cDisable">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
					<tr id="MenuUseYn">
                        <th scope="row"><label for="">사용여부</label></th>
                        <td>
                            <input type="radio" class="rdRdo" id="indictYnM1" name="indictYnM" value="Y" >사용
                            <label for="indictYnM1" class="rdSel"></label>
                            <input type="radio" class="rdRdo" id="indictYnM2" name="indictYnM" value="N">미사용
                            <label for="indictYnM2" class="rdSel"></label>
                        </td>
                    </tr> 
                    <tr>
                        <th><label for="">권한부여</label></th>
                        <td colspan="3">
                            <table class="popInTblInner">
                                <colgroup>
                                    <col width="" />
                                    <col width="" />
                                </colgroup>
                                <tbody id="popUpM-tbodyData-role">
	                                <tr>
                                        <td>ADMIN (관리자)</td>
                                        <td class="txtC">
                                            <input type="checkbox" class="rdChk" id="popUpM-chkAdminP" auth="" name="chkAdminP" value="1" />
                                            <label class="chkSel" for="popUpM-chkAdminP"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MANAGER (매니져)</td>
                                        <td class="txtC">
                                            <input type="checkbox" class="rdChk" id="popUpM-chkAdminS" auth="" name="chkAdminS" value="2" />
                                            <label class="chkSel" for="popUpM-chkAdminS"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>USER (사용자)</td>
                                        <td class="txtC">
                                            <input type="checkbox" class="rdChk" id="popUpM-chkUserS" auth="" name="chkUserS" value="3" />
                                            <label class="chkSel" for="popUpM-chkUserS"></label>
                                        </td>                                        
                                    </tr> 
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnM-update" class="btn-style1 darkGrayBtn">수정</button>
            <button type="button" id="btnM-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>
<!--메뉴권한 수정 팝업레이어-->