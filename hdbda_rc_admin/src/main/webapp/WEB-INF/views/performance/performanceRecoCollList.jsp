<%------------------------------------------------------------------------
 * TODO > 추천성과분석 > 추천성과 > 아이템별 추천성과
 * 파일명   > performanceRecoCollList.jsp
 * 작성일   > 2019.09.01.
 * 작성자   > CCmedia Service Corp.
------------------------------------------------------------------------%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	
<script type="text/javascript">
var controller = multiApiSet();
var serviceNm = '';
var value;
var columnSetRow = 0;
var columnSetRowIndex = 0;
var itemListCnt;
var page_resultData;
var apiNmGlobal;
var pageOn = false;
var dataPickLoad = false;
var orderBySortDataSet = {'dataSet1Dtp':'','dataSet2Dtp':'','dataSet3Dtp':''};
orderBySortDataSet.dataSet1Dtp = {};
orderBySortDataSet.dataSet2Dtp = {};
orderBySortDataSet.dataSet3Dtp = {};
orderBySortDataSet.dataSet1Dtp.list = {};
orderBySortDataSet.dataSet1Dtp.totalCnt = {};
orderBySortDataSet.dataSet2Dtp.list = {};
orderBySortDataSet.dataSet2Dtp.totalCnt = {};
orderBySortDataSet.dataSet3Dtp.list = {};
orderBySortDataSet.dataSet3Dtp.totalCnt = {};	

$(window).ready(function(){
     pageEvents();
     var startDate 	= $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
     var endDate 	= $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
});

function multiApiSet(){
	return "/performance";
	
	var api_url, api_protocol = location.protocol;
	var api_chkUrl = window.location.href;
	if('' != api_chkUrl) {
		if('http:' == api_protocol) {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
		} else {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
		}
	} else {
		return false;
	}
}

var goPaging_divPageNo1 = function(cPage){
	pageOn = true;
    Paging1(itemListCnt, $("#dsplyCnt1").val(), cPage, "divPageNo1");
};

 function pageEvents(){
	 var i=0;
	 var DataSet1BackUp = {}
	 DataSet1BackUp.list = {};
	 DataSet1BackUp.totalCnt = {};
	 $('#category1').on('change', function() {
			//카테고리 변경시 그래프가 변경됨	
			 startDate = $('#datePickerId1').data(
		    'daterangepicker').startDate.format('YYYYMMDD');
		     endDate = $('#datePickerId1').data(
		    'daterangepicker').endDate.format('YYYYMMDD');
			value = {
						'apiNm':'performance-reco-collection-area',	
						'searchText' : value.searchText,
						'searchOption1' : value.searchOption1,
						'searchTextInt' : parseInt(value.searchTextInt),
						'listOrder' : value.listOrder,
						'firstYmd':startDate,
						'lastYmd':endDate
				}
			serviceNm = 'recommendationStat/performance';
			apiGetForm();
	 });
	 fn_createInitOn();
}
 
 function fn_createInitOn(){
	 if (value != {}) {
			value = {
				'apiNm' : 'performance-reco-collection-area-all',
				'searchText' : '',
				'searchOption1' : '',
				'searchTextInt' : 0,
				'firstIndex' : parseInt($('#firstIndex').val()),
				'lastIndex' : parseInt($('#lastIndex').val()),
				'firstYmd':$('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD'),
				'lastYmd':$('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD')
			}
			serviceNm = 'recommendationStat/performance';
			apiGetForm();
	}
 }
 
function chartOption() {
	var startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
	var endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
	dateFunction1(startDate, endDate);
}
 
 function dateFunction1(startDate, endDate) {
	 
	//차트용 데이터 api호출
	value = {
				'apiNm':'performance-reco-collection-area',	
				'searchText' : value.searchText,
				'searchOption1' : value.searchOption1,
				'searchTextInt' : parseInt(value.searchTextInt),
				'listOrder' : value.listOrder,
				'firstYmd':$('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD'),
				'lastYmd':$('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD')
	}
	serviceNm = 'recommendationStat/performance';
	dataPickLoad = true;
	apiGetForm();
	//리스트용 데이터 api호출
	value = {
			'apiNm' : 'performance-reco-collection-area-all',
			'searchText' : '',
			'searchOption1' : '',
			'searchTextInt' : 0,
			'firstYmd':$('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD'),
			'lastYmd':$('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD')
	}
	serviceNm = 'recommendationStat/performance';
	$('#firstIndex').val(1);
	$('#lastIndex').val($("#dsplyCnt1").val());
	pageOn = false;
	apiGetForm();
}
 
 function echartMakeData1(data, title, value1, value2, value3) {
     var jsonArray = new Array();

     if(data && data.length > 0) {
    	 
// 		for (var i in data) {  // in 연산자는 상속계통을 모두 검색하여 맴버가 존재하는지 확인한다. (반복문중 가장 느리다.)
// 			if (data.hasOwnProperty(i)) {   // hasOwnProperty를 통해 해당 객체의 맴버인지 확인을 한다.
// 				console.log(data[i]);
// 			}
// 		}
    	 
         for(var key in data) {
             var itemInfo = data[key];
             var object = new Object();
				
             var month = itemInfo.yyyymmdd.substring(4,6)
             var day = itemInfo.yyyymmdd.substring(6,8)
             var date = month + "/" + day;
             object.title = date;
				object.value1 = itemInfo.cnt;
				object.value2 = itemInfo.rc_cnt;
// 				object.value3 = itemInfo.sum;
             jsonArray.push(object);
         }
      } else if(data != null) {
         var object = new Object();
         object.title = itemInfo[title];
         object.value1 = data[value1];
         object.value2 = data[value2];
//          object.value3 = data[value3];
         jsonArray.push(object);
     }
     return jsonArray;
 }

 function fn_dataOrderSet(val){
			if (val == "yyyymmdd_asc") {
				orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
					return(a.rc_item > b.rc_item) ? - 1 : (a.rc_item < b.rc_item) ? 1 : 0;
				});
				value.listOrder = "yyyymmdd_desc";
			} else {
				//ID순으로 오름차순 정렬
				orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
					return(a.rc_item < b.rc_item) ? - 1 : (a.rc_item > b.rc_item) ? 1 : 0;
				});
				value.listOrder = "yyyymmdd_asc";
			}
 }
 
 function nodata() {
     var html;

     html = ' <div class="noDataDiv">';
     html += ' <div class="dTxt">';
     html += ' <i class="ti-alert"></i>';
     html += ' <p>* 데이터가 없습니다.</p>';
     html += ' </div>';
     html += ' </div>';

     return html;
 }
 
 function fn_createChart(ajaxData, totalCnt, order){
	 
	 var chartId = 'graph-flot-points1';
	 
	 if(totalCnt > 0 ) {
			 if (order == "desc") {
				 ajaxData.sort(function(a,b){
					return(a.yyyymmdd > b.yyyymmdd) ? - 1 : (a.yyyymmdd < b.yyyymmdd) ? 1 : 0;
				});
			} else {
				ajaxData.sort(function(a,b){
					return(a.yyyymmdd < b.yyyymmdd) ? - 1 : (a.yyyymmdd > b.yyyymmdd) ? 1 : 0;
				});
			}
		 
	        for(var i=0; i<ajaxData.length; i++){
	    		eval("date" + i + "=" +ajaxData[i].yyyymmdd);
	    	} 
			var json_data = echartMakeData1(ajaxData, '타이틀','cnt', 'rc_cnt');
	        var titleDate =['추천제품클릭건수', '추천제품구매건수'];
	        var p_color = ['#2828cd','#a0a0ff'];
// 	        var json_data = echartMakeData1(ajaxData, '타이틀','cnt', 'rc_cnt', 'sum');
// 	        var titleDate =['추천제품클릭건수', '추천제품구매건수', '구매금액'];
// 	        var p_color = ['#2828cd','#a0a0ff','#ca77b4'];
			barOverlabChart(chartId, json_data, titleDate, '10', p_color, 'Y', ' 건/명');	        
	    } else {
	        $('#'+chartId).removeAttributes().html(nodata());
	    }
 }

	function apiGetForm() {
		var url = controller+'/'+ serviceNm + "/" + value.apiNm + ".do";
		var param = {};
		var succMs = "테이블이 조회 되었습니다.";
		var errorMs = "시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.";
		var netErrMs = "네트워크 통신 에러";
		param = {
			'searchText' : value.searchText,
			'searchOption1' : value.searchOption1,
			'searchOption' : $("#searchOptionNm option:selected" ).val(),
			'searchTextInt' : parseInt(value.searchTextInt),
			'listOrder' : value.listOrder,
			'firstIndex' : parseInt($('#firstIndex').val()),
			'lastIndex' : parseInt($('#lastIndex').val()),
			'firstYmd' :  value.firstYmd,
			'lastYmd' :  value.lastYmd
		};
		fn_isAjaxSet(param, url, succMs, errorMs, netErrMs);
	}

	function fn_isAjaxSet(param, url, succMs, errorMs, netErrMs) {
		setTimeout(function(){
		$.ajax({
			type : 'POST',
			url : url,
			dataType : 'JSON',
			data : param,
			beforeSend:function(){
				maskOn2();
		        $("#loading2").show();
			},
			complete:function(){
			},
			success : function(data) {
				if (data.returnErrMsg == "") {
					dataSet = data.list.dataSet;
					totalCnt = data.list.totalCnt;
					page_resultData = data.list.dataSet;
					apiNmGlobal = data.apiNm;
					var dataSetPick = new Array();
					if (data.apiNm == "performance-reco-collection-area-all") {
						orderBySortDataSet.dataSet2Dtp.list = dataSet;
	              		orderBySortDataSet.dataSet2Dtp.totalCnt = totalCnt;
	              		value.startDate = $('#datePickerId1').data(
	              	     'daterangepicker').startDate.format('YYYYMMDD');
	              		value.endDate = $('#datePickerId1').data(
	              	     'daterangepicker').endDate.format('YYYYMMDD');
	              	     
						if (pageOn == false) {
							Paging1(totalCnt, $("#dsplyCnt1").val(), 1, "divPageNo1");
							 startDate = $('#datePickerId1').data(
						     'daterangepicker').startDate.format('YYYYMMDD');
						     endDate = $('#datePickerId1').data(
						     'daterangepicker').endDate.format('YYYYMMDD');
						     if (dataPickLoad == false) {
						    	 value = {
					           				'apiNm':'performance-reco-collection-area',	
					           				'searchText' : value.searchText,
					           				'searchOption1' : value.searchOption1,
					           				'searchTextInt' : parseInt(value.searchTextInt),
					           				'listOrder' : value.listOrder,
					           				'firstYmd':startDate,
					           				'lastYmd':endDate
					           		}
						    	 	serviceNm = 'recommendationStat/performance';
									apiGetForm(); 
						     }
							
							
						}
							goOrder('goOrder_id');					
					}

					 	if (data.apiNm=="performance-reco-collection-area") {
					 		for(i=0;i<dataSet.length;i++){
					 			if ($('#category1').val()=='custGrp1') {
					 				dataSetPick[i] = {}
					 				dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].view_cnt_1;
					 				dataSetPick[i].rc_cnt = dataSet[i].buy_cnt_1;
					 				dataSetPick[i].sum = dataSet[i].buy_price_sum_1;
								}
					 			if ($('#category1').val()=='custGrp2') {
					 				dataSetPick[i] = {}
					 				dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].view_cnt_2;
					 				dataSetPick[i].rc_cnt = dataSet[i].buy_cnt_2;
					 				dataSetPick[i].sum = dataSet[i].buy_price_sum_2;
								}
					 			if ($('#category1').val()=='custGrp3') {
					 				dataSetPick[i] = {}
					 				dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].view_cnt_3;
					 				dataSetPick[i].rc_cnt = dataSet[i].buy_cnt_3;
					 				dataSetPick[i].sum = dataSet[i].buy_price_sum_3;
								}
					 			if ($('#category1').val()=='custGrp4') {
					 				dataSetPick[i] = {}
					 				dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].view_cnt_4;
					 				dataSetPick[i].rc_cnt = dataSet[i].buy_cnt_4;
					 				dataSetPick[i].sum = dataSet[i].buy_price_sum_4;
								}
					 			if ($('#category1').val()=='custGrp5') {
					 				dataSetPick[i] = {}
					 				dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].view_cnt_5;
					 				dataSetPick[i].rc_cnt = dataSet[i].buy_cnt_5;
					 				dataSetPick[i].sum = dataSet[i].buy_price_sum_5;
								}
					 			
					 			
					 			if ($('#category1').val()=='view1') {
					 				dataSetPick[i] = {}
					 				dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].view_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].view_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].view_sum;
								}
					 			if ($('#category1').val()=='view2') {
					 				dataSetPick[i] = {}
					 				dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].view_cust_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].view_cust_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].view_sum;
					 				
								}
					 			if ($('#category1').val()=='view3') {
					 				dataSetPick[i] = {}
					 				dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].view_item_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].view_item_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].view_item_sum;
								}
					 			
								if ($('#category1').val()=='cart1') {
									dataSetPick[i] = {}
									dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].cart_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].cart_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].cart_sum;
								}
								
								if ($('#category1').val()=='cart2') {
									dataSetPick[i] = {}
									dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].cart_cust_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].cart_cust_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].cart_cust_sum;
								}
								
								if ($('#category1').val()=='cart3') {
									dataSetPick[i] = {}
									dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].cart_item_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].cart_item_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].cart_item_sum;
								}

								if ($('#category1').val()=='search1') {
									dataSetPick[i] = {}
									dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].search_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].search_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].search_sum;
								}
								
								if ($('#category1').val()=='search2') {
									dataSetPick[i] = {}
									dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].search_cust_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].search_cust_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].search_cust_sum;
								}
								
								if ($('#category1').val()=='search3') {
									dataSetPick[i] = {}
									dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].search_item_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].search_item_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].search_item_sum;
								}

								if ($('#category1').val()=='buy1') {
									dataSetPick[i] = {}
									dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].buy_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].buy_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].buy_sum;
								}
								
								if ($('#category1').val()=='buy2') {
									dataSetPick[i] = {}
									dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].buy_cust_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].buy_cust_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].buy_cust_sum;
								}
								
								if ($('#category1').val()=='buy3') {
									dataSetPick[i] = {}
									dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].buy_item_cnt;
					 				dataSetPick[i].rc_cnt = dataSet[i].buy_item_rc_cnt;
					 				dataSetPick[i].sum = dataSet[i].buy_item_sum;
								}
								
								if ($('#category1').val()=='buy4') {
									dataSetPick[i] = {}
									dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
					 				dataSetPick[i].cnt = dataSet[i].buy_price_sum;
					 				dataSetPick[i].rc_cnt = dataSet[i].buy_price_rc_sum;
					 				dataSetPick[i].sum = dataSet[i].buy_price_sum;
								}
							}
					 		$('#totalCnt').empty().append("총 <b>"+addComma(totalCnt)+"</b> 개");
					 		orderBySortDataSet.dataSet1Dtp.list = dataSetPick;
			              	orderBySortDataSet.dataSet1Dtp.totalCnt = totalCnt;
							fn_createChart(dataSetPick, totalCnt, 'asc');
						}
				} else {
                     alert(errorMs + "(" + data.returnErrMsg + ")");
				}
				$("#loading2").hide();
                maskOff2();
			},
			error : function(e) {
				alert(netErrMs + ": " + e);
				$("#loading2").hide();
                maskOff2();
			}
		});
		}, 1);
	}

	function fn_createEmptySelect() {
		var htmlSet = "";	
		htmlSet += '<colgroup>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';

		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '<col>';
		htmlSet += '		<col>';
		htmlSet += '		<col>';
		htmlSet += '	</colgroup>';

		htmlSet += '	<tr>';
		htmlSet += '		<th></th>';
		htmlSet += '		<th colspan="9">제품상세(수)</th>';
		htmlSet += '		<th colspan="9">장바구니(수)</th>';
		htmlSet += '	</tr>	';

		htmlSet += '	<tr>';
		htmlSet += '<th rowspan="2">일자</th>';
		htmlSet += '<th colspan="3">클릭</th>';
		htmlSet += '<th colspan="3">고객</th>';
		htmlSet += '<th colspan="3">제품</th>';

		htmlSet += '<th colspan="3">장바구니</th>';
		htmlSet += '<th colspan="3">고객</th>';
		htmlSet += '<th colspan="3">제품</th>';
		htmlSet += '</tr>';
		htmlSet += '<tr>';
		htmlSet += '<th>전체</th>';
		htmlSet += '<th>추천</th>';
		htmlSet += '<th>비율</th>';

		htmlSet += '<th>전체</th>';
		htmlSet += '<th>추천</th>';
		htmlSet += '<th>비율</th>';

		htmlSet += '<th>전체</th>';
		htmlSet += '<th>추천</th>';
		htmlSet += '<th>비율</th>';

		htmlSet += '<th>전체</th>';
		htmlSet += '<th>추천</th>';
		htmlSet += '<th>비율</th>';

		htmlSet += '<th>전체</th>';
		htmlSet += '<th>추천</th>';
		htmlSet += '<th>비율</th>';

		htmlSet += '<th>전체</th>';
		htmlSet += '<th>추천</th>';
		htmlSet += '<th>비율</th>';
		htmlSet += '</tr>';
		htmlSet += '<tr>';
		htmlSet += '<td colspan="19">';
		htmlSet += 'No data available in table';
		htmlSet += '</td>';
		htmlSet += '</tr>';
		$("#viewDateTbl1").empty();
		$("#viewDateTbl1").append(htmlSet);
	}

	function fn_createSearchList(dataList, totalCnt) {
		var htmlSet = "";
		var day,month,year,date;
		if (dataList.length == 0) {
			fn_createEmptySelect();
			return;
		}
		htmlSet += '<colgroup>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '	<col>';
		htmlSet += '</colgroup>';

// 		htmlSet += '	<tr>';
// 		htmlSet += '		<th></th>';
// 		htmlSet += '		<th colspan="9">제품상세(수)</th>';
// 		htmlSet += '		<th colspan="9">장바구니(수)</th>';
// 		htmlSet += '		<th colspan="9">검색건(수)</th>';
// 		htmlSet += '		<th colspan="14">구매건(수)</th>';
// 		htmlSet += '	<tr>';
		
		htmlSet += '	<tr>';
		htmlSet += '<th rowspan="2">일자</th>';
		htmlSet += '<th colspan="3">일반회원</th>';
// 		htmlSet += '<th colspan="3">고객</th>';
// 		htmlSet += '<th colspan="3">제품</th>';

		htmlSet += '<th colspan="3">공공기관</th>';
// 		htmlSet += '<th colspan="3">고객</th>';
// 		htmlSet += '<th colspan="3">제품</th>';

		htmlSet += '<th colspan="3">사회적경제기업</th>';
// 		htmlSet += '<th colspan="3">고객</th>';
// 		htmlSet += '<th colspan="3">키워드</th>';
		
// 		htmlSet += '<th colspan="3">전체</th>';
		htmlSet += '<th colspan="3">STORE 36.5매장</th>';
		htmlSet += '<th colspan="3">유통채널</th>';		
// 		htmlSet += '<th colspan="3">금액</th>';
		htmlSet += '</tr>';
		
		htmlSet += '<tr>';
		htmlSet += '	<th>추천제품<br>클릭건수</th>';
		htmlSet += '	<th>추천제품<br>구매건수</th>';
		htmlSet += '	<th>구매금액</th>';

		htmlSet += '	<th>추천제품<br>클릭건수</th>';
		htmlSet += '	<th>추천제품<br>구매건수</th>';
		htmlSet += '	<th>구매금액</th>';

		htmlSet += '	<th>추천제품<br>클릭건수</th>';
		htmlSet += '	<th>추천제품<br>구매건수</th>';
		htmlSet += '	<th>구매금액</th>';

		htmlSet += '	<th>추천제품<br>클릭건수</th>';
		htmlSet += '	<th>추천제품<br>구매건수</th>';
		htmlSet += '	<th>구매금액</th>';

		htmlSet += '	<th>추천제품<br>클릭건수</th>';
		htmlSet += '	<th>추천제품<br>구매건수</th>';
		htmlSet += '	<th>구매금액</th>';
		htmlSet += '</tr>';
		
		orderBySortDataSet.dataSet1Dtp.list = new Array();
		if(dataList != null && dataList.length > 0){
			for (i = 0; i < dataList.length; i++) {
				num = parseInt($("#firstIndex").val()) + parseInt(i);
				htmlSet += '<tr>';

				//제품상세(수)			
				//일자
				year = dataList[i].yyyymmdd.substr(0,4);
				month = "-"+dataList[i].yyyymmdd.substr(4,2);
				day = "-"+dataList[i].yyyymmdd.substr(6,2);
				date = year+month+day;
				htmlSet += '<td>'+date+'</td>';
				
				htmlSet += '<td class="txtR">'+addComma(dataList[i].view_cnt_1)+'</td>';
				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_cnt_1)+'</td>';
				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_price_sum_1)+'</td>';
				
				htmlSet += '<td class="txtR">'+addComma(dataList[i].view_cnt_2)+'</td>';
				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_cnt_2)+'</td>';
				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_price_sum_2)+'</td>';
				
				htmlSet += '<td class="txtR">'+addComma(dataList[i].view_cnt_3)+'</td>';
				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_cnt_3)+'</td>';
				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_price_sum_3)+'</td>';
				
				htmlSet += '<td class="txtR">'+addComma(dataList[i].view_cnt_4)+'</td>';
				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_cnt_4)+'</td>';
				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_price_sum_4)+'</td>';
				
				htmlSet += '<td class="txtR">'+addComma(dataList[i].view_cnt_5)+'</td>';
				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_cnt_5)+'</td>';
				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_price_sum_5)+'</td>';
				
				//클릭- 전체, 추천, 비율
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].view_cnt)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].view_rc_cnt)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].view_sum)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(parseInt(parseInt(dataList[i].view_rc_cnt) / parseInt(dataList[i].view_cnt) * 100))+'</td>';
				
				//고객- 전체, 추천, 비율
// 				htmlSet += '<td>'+addComma(dataList[i].view_cust_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(dataList[i].view_cust_rc_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(parseInt(parseInt(dataList[i].view_cust_rc_cnt) / parseInt(dataList[i].view_cust_cnt) * 100))+'%</td>';
				
				//제품-전체, 추천, 비율
// 				htmlSet += '<td>'+addComma(dataList[i].view_item_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(dataList[i].view_item_rc_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(parseInt(parseInt(dataList[i].view_item_rc_cnt) / parseInt(dataList[i].view_item_cnt) * 100))+'%</td>';
				
				//장바구니(수)
				//장바구니- 전체, 추천, 비율
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].cart_cnt)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].cart_rc_cnt)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].cart_sum)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(parseInt(parseInt(dataList[i].cart_rc_cnt) / parseInt(dataList[i].cart_cnt) * 100))+'</td>';
				
				//고객- 전체, 추천, 비율
// 				htmlSet += '<td>'+addComma(dataList[i].cart_cust_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(dataList[i].cart_cust_rc_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(parseInt(parseInt(dataList[i].cart_cust_rc_cnt) / parseInt(dataList[i].cart_cust_cnt) * 100))+'%</td>';
				
				//제품- 전체, 추천, 비율
// 				htmlSet += '<td>'+addComma(dataList[i].cart_item_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(dataList[i].cart_item_rc_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(parseInt(parseInt(dataList[i].cart_item_rc_cnt) / parseInt(dataList[i].cart_item_cnt) * 100))+'%</td>';
				
				//검색
				//검색 건수- 전체, 추천, 비율
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].search_cnt)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].search_rc_cnt)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].search_sum)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(parseInt(parseInt(dataList[i].search_rc_cnt) / parseInt(dataList[i].search_cnt) * 100))+'</td>';
				
				//검색 고객수 - 추천 - 전체, 추천, 비율
// 				htmlSet += '<td>'+addComma(dataList[i].search_cust_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(dataList[i].search_cust_rc_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(parseInt(parseInt(dataList[i].search_cust_rc_cnt) / parseInt(dataList[i].search_cust_cnt) * 100))+'%</td>';
				
				//검색 키워드수 - 전체, 추천, 비율
// 				htmlSet += '<td>'+addComma(dataList[i].search_item_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(dataList[i].search_item_rc_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(parseInt(parseInt(dataList[i].search_item_rc_cnt) / parseInt(dataList[i].search_item_cnt) * 100))+'%</td>';
				
				//구매
				//구매건수 - 전체, 추천, 비율
// 				htmlSet += '<td>'+addComma(dataList[i].buy_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(dataList[i].buy_rc_cnt)+'</td>';
// 				htmlSet += '<td>'+addComma(parseInt(parseInt(dataList[i].buy_rc_cnt) / parseInt(dataList[i].buy_cnt) * 100))+'</td>';
				
				//구매고객수 - 전체, 추천, 비율
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_cust_cnt)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_cust_rc_cnt)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_cust_sum)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(parseInt(parseInt(dataList[i].buy_cust_rc_cnt) / parseInt(dataList[i].buy_cust_cnt) * 100))+'</td>';
				
	 			//구매제품수 - 전체, 추천, 비율
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_item_cnt)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_item_rc_cnt)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(dataList[i].buy_item_sum)+'</td>';
// 				htmlSet += '<td class="txtR">'+addComma(parseInt(parseInt(dataList[i].buy_item_rc_cnt) / parseInt(dataList[i].buy_item_cnt) * 100))+'</td>';
				
	 			//구매 금액 - 전체, 추천, 비율
// 	 			if (dataList[i].buy_price_sum == null) dataList[i].buy_price_sum = 0;
// 	 			if (dataList[i].buy_price_rc_sum == null) dataList[i].buy_price_rc_sum = 0;
// 				htmlSet += '<td>'+addComma(parseInt(dataList[i].buy_price_sum))+'</td>';
// 				htmlSet += '<td>'+addComma(parseInt(dataList[i].buy_price_rc_sum))+'</td>';
// 				htmlSet += '<td>'+addComma(parseInt(parseInt(dataList[i].buy_price_rc_sum) / parseInt(dataList[i].buy_price_sum) * 100))+'%</td>';
				
				htmlSet += '</tr>';
			}
		}
		$("#viewDateTbl1").empty();
		$("#viewDateTbl1").append(htmlSet);

		itemListCnt = totalCnt;
		$('#totalCnt').empty().append("총 <b>"+addComma(totalCnt)+"</b> 개");
		$('#goOrder').unbind('click').bind('click',function(){
			orderChange();
			goOrder('goOrder_id');
		});
	}
	
	function orderChange(){
		if (value.listOrder == "id_asc") {
			value.listOrder = "id_desc";
		} else {
			value.listOrder = "id_asc";
		}
	}

	function addComma(num) {
	    var regexp = /\B(?=(\d{3})+(?!\d))/g;
	    return num.toString().replace(regexp,',');
	}
	
	function goOrder(val) {
		if (val == "goOrder_id") {
			if (value.listOrder == "" || value.listOrder == undefined) {
				value.listOrder = "id_desc";
			}
			//ID순으로 오름차순 정렬
			if (value.listOrder == "id_asc") {
				orderBySortDataSet.dataSet2Dtp.list.sort(function(a, b) {
					return (a.yyyymmdd < b.yyyymmdd) ? -1
							: (a.yyyymmdd > b.yyyymmdd) ? 1 : 0;
				});
				value.listOrder = "id_asc";
			} else {
				orderBySortDataSet.dataSet2Dtp.list.sort(function(a, b) {
					return (a.yyyymmdd > b.yyyymmdd) ? -1
							: (a.yyyymmdd < b.yyyymmdd) ? 1 : 0;
				});
				value.listOrder = "id_desc";
			}
		}
		
		fn_createSearchList(orderBySortDataSet.dataSet2Dtp.list,orderBySortDataSet.dataSet2Dtp.totalCnt);

	}

	function renderDataByHtml1(displayCnt, pageNo) {
		var startDate = $('#datePickerId1').data(
	     'daterangepicker').startDate.format('YYYYMMDD');
	     var endDate = $('#datePickerId1').data(
	     'daterangepicker').endDate.format('YYYYMMDD');
	     
		value.apiNm = "performance-reco-collection-area-all";
		value.firstYmd = startDate,
		value.lastYmd = endDate
		serviceNm = 'recommendationStat/performance';
		$('#viewDateTbl1').empty(innerHtml);

		var innerHtml = '';
		var idxFrom, idxTo = 0;

		idxFrom = displayCnt * (pageNo - 1);
		idxTo = idxFrom + (displayCnt - 1);
		$("#firstIndex").val(idxFrom + 1);
		$("#lastIndex").val(idxTo + 1);
		if (pageOn == true) {
			apiGetForm();
		}
	}
	
	function linkPage(pageNo) {
		$('#firstIndex').val(1);
		$('#lastIndex').val($("#dsplyCnt1").val());
		pageOn = false;
		dataPickLoad = true;
		value = {
				'apiNm' : 'performance-reco-collection-area-all',
				'searchText' : '',
				'searchOption1' : '',
				'searchTextInt' : 0,
				'firstIndex' : parseInt($('#firstIndex').val()),
				'lastIndex' : parseInt($('#lastIndex').val()),
				'firstYmd':$('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD'),
				'lastYmd':$('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD')
		}
		serviceNm = 'recommendationStat/performance';
		apiGetForm();
	}
</script>

<div class="mConBox">
    <div class="mCont">
        <div class="row" >
            <div class="col-md-4" id="subtitleA">
                <h2><i class="ti-layers"></i> 추천 성과</h2>
            </div>
            <div class="col-md-8">
                <div class="pageRoute">
                </div>
            </div>
        </div>

        	<!-- end row -->
		<form:form modelAttribute="areaPreviewParam" id="listForm"
			method="post">
<!-- 			<input type="hidden" name="dsplyCnt1" id="dsplyCnt1" value="10" /> -->
			<input type="hidden" name="firstIndex" id="firstIndex" value="1" />
			<input type="hidden" name="lastIndex" id="lastIndex" value="10" />

			<div class="row">
				<div class="col-md-12 text-right">
					<div class="conBox" style="min-height: 400px;">
						<div class="setBoxListArea mr10 pull-left">
							<select name="category" id="category1">
								<option value="custGrp1" selected>일반회원</option>
								<option value="custGrp2">공공기관</option>
								<option value="custGrp3">사회적경제기업</option>
								<option value="custGrp4">STORE 36.5매장</option>
								<option value="custGrp5">유통채널</option>
								
<!-- 								<option value="view1" selected>제품상세_클릭</option> -->
<!-- 								<option value="view2">제품상세_고객</option> -->
<!-- 								<option value="view3">제품상세_제품</option> -->

<!-- 								<option value="cart1">장바구니_장바구니</option> -->
<!-- 								<option value="cart2">장바구니_고객</option> -->
<!-- 								<option value="cart3">장바구니_제품</option> -->

<!-- 								<option value="search1">검색건_전체</option> -->
<!-- 								<option value="search2">검색건_고객</option> -->
<!-- 								<option value="search3">검색건_키워드</option> -->

<!-- 								<option value="buy1">구매건_전체</option> -->
<!-- 								<option value="buy2">구매건_고객</option> -->
<!-- 								<option value="buy3">구매건_제품</option> -->
<!-- 								<option value="buy4">구매건_금액</option> -->
							</select>
						</div>



	                    <select id="searchOptionNm" onchange="chartOption()">
	                    	<option value="">전체</option>
<!-- 	                    	<option value="WEB">PC + Mobile</option> -->
<!-- 	                    	<option value="APP">IOS + Android</option> -->
<!-- 	                    	<option value="MAA">Mobile + IOS + Android</option> -->
	                    	<option value="P">PC</option>
	                    	<option value="M">Mobile</option>
<!-- 	                    	<option value="AI">IOS</option> -->
<!-- 							<option value="AA">Android</option> -->
                   		</select>
						<div class="dateRange">
							<label class="labelHidden2" for="datePickerId1"></label> <input
								type="text" id="datePickerId1"> <i class="ti-calendar"></i>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="gInBox" id="graph-flot-points1"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row tableTop" id="tableTop2">

				<div class="col-md-12">
					<div class="conBox" style="min-height: 400px;">		
						
						<div class="row">
							<div class="col-md-33 select-set-3">
								<p id="totalCnt">
									총 <b>0</b> 개
								</p>
							</div>
							<div class="col-md-9 text-right select-sys-1">
								<button type="button" id="btnExcel" class="exDwBtn">엑셀</button>
								<select id="dsplyCnt1" name="dsplyCnt1" onchange="linkPage(1);">
									<option value="10" selected="selected">10개씩 보기</option>
									<option value="20">20개씩 보기</option>
								</select>
							</div>
						</div>						
						
						<div class="row" style="height:100%;width:100%;overflow:auto">
								<div class="col-md-12">							
									<table class="viewDateTbl wide1600"  id="viewDateTbl1" >
<!-- 									<table class="viewDateTbl wide2960"  id="viewDateTbl1" > -->
									</table>					
								</div>

						<div class="row">
							<div class="col-md-2"></div>
							<!-- end row -->
							<div class="row">
								<div id="divPageNo1" class="col-md-12 pageNum"></div>
							</div>
							<!-- end row -->

							<div class="col-md-2 select-set-3"></div>
						</div>
					</div>
				</div>
			</div>
		</form:form>
        
    </div>
</div>
<div id="loading2" style="z-index:1002; display:none;">
    <div class="sk-cube-grid" style="z-index:1002">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
    <div class="sk-cube-txt" style="z-index:1002">초록마을</div>
</div>

<form id="excelInitForm" name="excelInitForm" />

<script type="text/javascript">
var controller = multiApiSet();
var userActionCnt=0;
var page_resultData;

$(document).ready(function() {
	$("#btnExcel").click(function(){
		
	    var startDate = $('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD');
	    var endDate = $('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD');
	    var url = '';  
	    var param = new Object;
	    var date = '';
	    
	    var limitOption = $("#dsplyCnt1").val();
	    var pageNo = $("#divPageNo1 .pageActive").text();
	    	    
	    param.firstYmd = startDate;
	    param.lastYmd = endDate;
	    param.dsplyCnt = limitOption;
	    param.dsplyNo = pageNo;
	    
	    param.excelFileName = "추천성과";
	    param.excelTitle 	= "추천성과";
	    param.api = "performance-reco-collection-area-all";
		
		$('#excelInitForm').clone().attr('id', 'excelFrm').css('display', 'none').insertAfter( $('#excelInitForm') );
		$('#excelFrm').append('<input type="hidden" name="firstYmd" id="firstYmd" value="' + param.firstYmd + '" />');
		$('#excelFrm').append('<input type="hidden" name="lastYmd" id="lastYmd" value="' + param.lastYmd + '" />');
		$('#excelFrm').append('<input type="hidden" name="dsplyCnt" id="dsplyCnt" value="' + param.dsplyCnt + '" />');
		$('#excelFrm').append('<input type="hidden" name="dsplyNo" id="dsplyNo" value="' + param.dsplyNo + '" />');
		$('#excelFrm').append('<input type="hidden" name="excelFileName" id="excelFileName" value="' + param.excelFileName + '" />');
		$('#excelFrm').append('<input type="hidden" name="excelTitle" id="excelTitle" value="' + param.excelTitle + '" />');
		$('#excelFrm').append('<input type="hidden" name="api" id="api" value="' + param.api + '" />');
		
		$('#param').val(param);
		$('#excelFrm').attr('action', '/common/excelDownload.do');
		$('#excelFrm').attr('method', 'post');
		$('#excelFrm').submit();
		$('#excelFrm').remove();
	});
});
</script>
