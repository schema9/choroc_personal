<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	int columnCnt = 16;
%>
<table style="width:1200px;">
	<tr>
		<td>
			<table style="width:100%;" border="0">
				<tbody>
				<tr>
					<td colspan="<%=columnCnt%>" style="text-align:center;font-size:24px;font-weight:bold;">
					${excelTitle}
					</td>
				</tr>
				<tr>
					<td colspan="<%=columnCnt%>" style="heigth:20px;"></td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table style="width:100%;" border="1">
				<thead>
					<tr>
						<th rowspan="2" style="background-color:#1f3a93; color: white; ">일자</th>
						<th colspan="3" style="background-color:#1f3a93; color: white; ">일반회원</th>
						<th colspan="3" style="background-color:#1f3a93; color: white; ">공공기관</th>
						<th colspan="3" style="background-color:#1f3a93; color: white; ">사회적경제기업</th>
						<th colspan="3" style="background-color:#1f3a93; color: white; ">STORE 36.5매장</th>
						<th colspan="3" style="background-color:#1f3a93; color: white; ">유통채널</th>
					</tr>
					<tr>
						<th style="background-color:#1f3a93; color: white; ">클릭건수</th>
						<th style="background-color:#1f3a93; color: white; ">구매건수</th>
						<th style="background-color:#1f3a93; color: white; ">구매금액</th>
						<th style="background-color:#1f3a93; color: white; ">클릭건수</th>
						<th style="background-color:#1f3a93; color: white; ">구매건수</th>
						<th style="background-color:#1f3a93; color: white; ">구매금액</th>
						<th style="background-color:#1f3a93; color: white; ">클릭건수</th>
						<th style="background-color:#1f3a93; color: white; ">구매건수</th>
						<th style="background-color:#1f3a93; color: white; ">구매금액</th>
						<th style="background-color:#1f3a93; color: white; ">클릭건수</th>
						<th style="background-color:#1f3a93; color: white; ">구매건수</th>
						<th style="background-color:#1f3a93; color: white; ">구매금액</th>
						<th style="background-color:#1f3a93; color: white; ">클릭건수</th>
						<th style="background-color:#1f3a93; color: white; ">구매건수</th>
						<th style="background-color:#1f3a93; color: white; ">구매금액</th>
					</tr>
				</thead>
			
				<tbody>
					<c:forEach items="${excelList}" var="list" varStatus="status">
						<tr>
							<td style="text-align: center;">${list.yyyymmdd}</td>
							<td><fmt:formatNumber value="${list.view_cnt_1}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.buy_cnt_1}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.buy_price_sum_1}" pattern="#,###" /></td>
							
							<td><fmt:formatNumber value="${list.view_cnt_2}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.buy_cnt_2}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.buy_price_sum_2}" pattern="#,###" /></td>
							
							<td><fmt:formatNumber value="${list.view_cnt_3}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.buy_cnt_3}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.buy_price_sum_3}" pattern="#,###" /></td>
							
							<td><fmt:formatNumber value="${list.view_cnt_4}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.buy_cnt_4}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.buy_price_sum_4}" pattern="#,###" /></td>
							
							<td><fmt:formatNumber value="${list.view_cnt_5}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.buy_cnt_5}" pattern="#,###" /></td>
							<td><fmt:formatNumber value="${list.buy_price_sum_5}" pattern="#,###" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</td>
	</tr>
</table>
