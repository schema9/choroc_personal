<%------------------------------------------------------------------------
 * TODO > 추천성과분석 > 추천성과 > 영역별 추천성과
 * 파일명   > performanceRecoAreaList.jsp
 * 작성일   > 2019.09.01.
 * 작성자   > CCmedia Service Corp.
------------------------------------------------------------------------%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	
<script type="text/javascript">
var controller = multiApiSet();
var serviceNm = '';
var value;
var columnSetRow = 0;
var columnSetRowIndex = 0;
var itemListCnt;
var page_resultData;
var apiNmGlobal;
var pageOn = false;
var dataPickLoad = false;
var orderBySortDataSet = {'dataSet1Dtp':'','dataSet2Dtp':'','dataSet3Dtp':''};
orderBySortDataSet.dataSet1Dtp = {};
orderBySortDataSet.dataSet2Dtp = {};
orderBySortDataSet.dataSet3Dtp = {};
orderBySortDataSet.dataSet1Dtp.list = {};
orderBySortDataSet.dataSet1Dtp.totalCnt = {};
orderBySortDataSet.dataSet2Dtp.list = {};
orderBySortDataSet.dataSet2Dtp.totalCnt = {};
orderBySortDataSet.dataSet3Dtp.list = {};
orderBySortDataSet.dataSet3Dtp.totalCnt = {};	

$(window).ready(function(){
     pageEvents();
     var startDate = $('#datePickerId1').data(
     'daterangepicker').startDate.format('YYYYMMDD');
     var endDate = $('#datePickerId1').data(
     'daterangepicker').endDate.format('YYYYMMDD');
});

function multiApiSet(){
	return "/performance";
	
	
	
	var api_url, api_protocol = location.protocol;
	var api_chkUrl = window.location.href;
	if('' != api_chkUrl) {
		if('http:' == api_protocol) {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
		} else {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
		}
	} else {
		return false;
	}
}

var goPaging_divPageNo1 = function(cPage){
	pageOn = true;
    Paging1(itemListCnt, $("#dsplyCnt1").val(), cPage, "divPageNo1");
};

 function pageEvents(){
	 var i=0;
	 var DataSet1BackUp = {}
	 DataSet1BackUp.list = {};
	 DataSet1BackUp.totalCnt = {};
	 $('#category1').on('change', function() {
			//카테고리 변경시 그래프가 변경됨	
			 startDate = $('#datePickerId1').data(
		    'daterangepicker').startDate.format('YYYYMMDD');
		     endDate = $('#datePickerId1').data(
		    'daterangepicker').endDate.format('YYYYMMDD');
			value = {
						'apiNm':'performance-reco-collection-area2',	
						'searchText' : value.searchText,
						'searchOption1' : value.searchOption1,
						'searchTextInt' : parseInt(value.searchTextInt),
						'listOrder' : value.listOrder,
						'firstYmd':startDate,
						'lastYmd':endDate
				}
			serviceNm = 'recommendationStat/performance';
			apiGetForm();
	 });
	 fn_createInitOn();
}
 
 function fn_createInitOn(){
	 if (value != {}) {
			value = {
				'apiNm' : 'performance-reco-collection-area2-all',
				'searchText' : '',
				'searchOption1' : '',
				'searchTextInt' : 0,
				'firstIndex' : parseInt($('#firstIndex').val()),
				'lastIndex' : parseInt($('#lastIndex').val()),
				'firstYmd':$('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD'),
				'lastYmd':$('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD')
			}
			serviceNm = 'recommendationStat/performance';
			apiGetForm();
	}
 }
 
 function dateFunction1(startDate, endDate) {
	//차트용 데이터 api호출
	value = {
				'apiNm':'performance-reco-collection-area2',	
				'searchText' : value.searchText,
				'searchOption1' : value.searchOption1,
				'searchTextInt' : parseInt(value.searchTextInt),
				'listOrder' : value.listOrder,
				'firstYmd':$('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD'),
				'lastYmd':$('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD')
	}
	serviceNm = 'recommendationStat/performance';
	dataPickLoad = true;
	apiGetForm();
	//리스트용 데이터 api호출
	value = {
			'apiNm' : 'performance-reco-collection-area2-all',
			'searchText' : '',
			'searchOption1' : '',
			'searchTextInt' : 0,
			'firstYmd':$('#datePickerId1').data('daterangepicker').startDate.format('YYYYMMDD'),
			'lastYmd':$('#datePickerId1').data('daterangepicker').endDate.format('YYYYMMDD')
	}
	serviceNm = 'recommendationStat/performance';
	$('#firstIndex').val(1);
	$('#lastIndex').val($("#dsplyCnt1").val());
	pageOn = false;
	apiGetForm();
}
 
 function echartMakeData1(data, title, value1) {
     var jsonArray = new Array();

     if(data && data.length > 0) {
         for(var key in data) {
             var itemInfo = data[key];
             var object = new Object();
				
             var month = itemInfo.yyyymmdd.substring(4,6)
             var day = itemInfo.yyyymmdd.substring(6,8)
             var date = month + "/" + day;
             object.title = date;
				object.value1 = itemInfo.view_cnt;
 				object.value2 = itemInfo.pge_area_nm;
             jsonArray.push(object);
         }
      } else if(data != null) {
         var object = new Object();
         object.title = itemInfo[title];
         object.value1 = data[value1];
          object.value2 = data[value2];
         jsonArray.push(object);
     }
     return jsonArray;
 }

 function echartMakeData4(ajaxDataVal, title_name) {

	    var jsonArray =  [];
	    var itemList = ajaxDataVal;
	    if(itemList && itemList.length > 0) {
	        for(var i =0;i<title_name.length;i++){
	            j = 0;
	            var object = [];
	            for(var key in itemList) {
	                var itemInfo = itemList[key];
	                if(itemInfo["pge_area_nm"] == title_name[i]) {
	                    object.push(itemInfo["cnt"]);
	                    j++;
	                }
	            }
	            jsonArray.push(object);
	        }
	    }
	    return jsonArray;
	}
 
 function echartMakeData5(ajaxDataVal, title, dateType) {

	    var col_data = [] ;
	    var tmpCol_data = [] ;
	    var itemList = ajaxDataVal;
	    if(itemList && itemList.length > 0) {
	        for(var key in itemList) {
	            var itemInfo = itemList[key];
	            if($.inArray(itemInfo[title], tmpCol_data) === -1) {
	                if('A' == dateType) {
	                    col_data.push(fn_util_getDate_ymdToFmt(itemInfo[title], 'MM/DD'));
	                } else {
	                    col_data.push(itemInfo[title]);
	                }
	                tmpCol_data.push(itemInfo[title]);
	            }
	        }
	    }
	    return col_data;
	}

 
 function fn_dataOrderSet(val){
			if (val == "yyyymmdd_asc") {
				orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
					return(a.rc_item > b.rc_item) ? - 1 : (a.rc_item < b.rc_item) ? 1 : 0;
				});
				value.listOrder = "yyyymmdd_desc";
			} else {
				//ID순으로 오름차순 정렬
				orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
					return(a.rc_item < b.rc_item) ? - 1 : (a.rc_item > b.rc_item) ? 1 : 0;
				});
				value.listOrder = "yyyymmdd_asc";
			}
 }
 
 function fn_createChart(ajaxData, totalCnt, order){
	 var chartId = 'graph-flot-points1';
	 if(totalCnt > 0 ) {
		 	var title_name = new Array();
		 	var dataVal = new Array();
		 	var chart_title = new Array();
	        for(var i=0; i<ajaxData.length; i++){
	    		eval("date" + i + "=" +ajaxData[i].yyyymmdd);
	    	} 
	        var dateType="";
	        var title_name = echartMakeData5(ajaxData, 'pge_area_nm', '');
	        var json_data5 = echartMakeData4(ajaxData, title_name);
	        var titleData2 = echartMakeData5(ajaxData, 'yyyymmdd', dateType);
	        lineMutiChart('graph-flot-points1', title_name, json_data5, titleData2);
	        
	    } else {
	        $('#'+chartId).removeAttributes().html(nodata());
	    }
 }
 
 function nodata() {
     var html;

     html = ' <div class="noDataDiv">';
     html += ' <div class="dTxt">';
     html += ' <i class="ti-alert"></i>';
     html += ' <p>* 데이터가 없습니다.</p>';
     html += ' </div>';
     html += ' </div>';

     return html;
 }

	function apiGetForm() {
		var url = controller+'/'+ serviceNm + "/" + value.apiNm + ".do";
		var param = {};
		var succMs = "테이블이 조회 되었습니다.";
		var errorMs = "시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.";
		var netErrMs = "네트워크 통신 에러";
		param = {
			'searchText' : value.searchText,
			'searchOption1' : value.searchOption1,
			'searchTextInt' : parseInt(value.searchTextInt),
			'listOrder' : value.listOrder,
			'firstIndex' : parseInt($('#firstIndex').val()),
			'lastIndex' : parseInt($('#lastIndex').val()),
			'firstYmd' :  value.firstYmd,
			'lastYmd' :  value.lastYmd
		};
		fn_isAjaxSet(param, url, succMs, errorMs, netErrMs);
	}

	function fn_isAjaxSet(param, url, succMs, errorMs, netErrMs) {
		setTimeout(function(){
		$.ajax({
			type : 'POST',
			url : url,
			dataType : 'JSON',
			data : param,
			beforeSend:function(){
				maskOn2();
		        $("#loading2").show();
			},
			complete:function(){
			},
			success : function(data) {
				if (data.returnErrMsg == "") {
					dataSet = data.list.dataSet;
					totalCnt = data.list.totalCnt;
					page_resultData = data.list.dataSet;
					apiNmGlobal = data.apiNm;
					
					var dataSetPick = new Array();
					if (data.apiNm == "performance-reco-collection-area2-all") {
						orderBySortDataSet.dataSet2Dtp.list = dataSet;
	              		orderBySortDataSet.dataSet2Dtp.totalCnt = totalCnt;
	              		value.startDate = $('#datePickerId1').data(
	              	     'daterangepicker').startDate.format('YYYYMMDD');
	              		value.endDate = $('#datePickerId1').data(
	              	     'daterangepicker').endDate.format('YYYYMMDD');
	              	     
						if (pageOn == false) {
							Paging1(totalCnt, $("#dsplyCnt1").val(), 1, "divPageNo1");
							 startDate = $('#datePickerId1').data(
						     'daterangepicker').startDate.format('YYYYMMDD');
						     endDate = $('#datePickerId1').data(
						     'daterangepicker').endDate.format('YYYYMMDD');
						     if (dataPickLoad == false) {
						    	 value = {
					           				'apiNm':'performance-reco-collection-area2',	
					           				'searchText' : value.searchText,
					           				'searchOption1' : value.searchOption1,
					           				'searchTextInt' : parseInt(value.searchTextInt),
					           				'listOrder' : value.listOrder,
					           				'firstYmd':startDate,
					           				'lastYmd':endDate
					           		}
						    	 	serviceNm = 'recommendationStat/performance';
									apiGetForm(); 
						     }
							
							
						}
							goOrder('goOrder_id');					
					}
					 	if (data.apiNm=="performance-reco-collection-area2") {
					 		for(i=0;i<dataSet.length;i++){
								dataSetPick[i] = {};
				 				dataSetPick[i].yyyymmdd = dataSet[i].yyyymmdd;
				 				dataSetPick[i].cnt = dataSet[i].view_cnt;
				 				dataSetPick[i].pge_area_nm = dataSet[i].pge_area_nm;
							}
					 		orderBySortDataSet.dataSet1Dtp.list = dataSetPick;
			              	orderBySortDataSet.dataSet1Dtp.totalCnt = totalCnt;
							fn_createChart(dataSetPick, totalCnt, 'asc');
						}
				} else {
                     alert(errorMs + "(" + data.returnErrMsg + ")");
				}
				$("#loading2").hide();
                maskOff2();
			},
			error : function(e) {
				alert(netErrMs + ": " + e);
				$("#loading2").hide();
                maskOff2();
			}
		});
		}, 1);
	}

	
	function fn_createEmptySelect() {
		$('#totalCnt').empty().append("총 <b> 0 </b> 개");
		$("#viewDateTbl1").empty();
	}
	
	function dataSetList(dataList){

		var dataListSearch = new Array();
		var dataListSearchSub = new Array();
		
		var dataAreaId = [] ;
		for (var i = 0; i < dataList.length; i++) {
			if(i == 0) {
				dataAreaId.push(dataList[i].pge_area_id);
			} else {
				if(!dataAreaId.includes(dataList[i].pge_area_id)) {
					dataAreaId.push(dataList[i].pge_area_id);
				}
			}
		}

		for (var i = 0; i < dataList.length; i++) {
			dataListSearch[i] = {}
			dataListSearch[i].yyyymmdd = new Array();
			dataListSearch[i].pge_area_nm = new Array();
			dataListSearch[i].pge_area_id = new Array();
			dataListSearch[i].view_cnt = new Array();
			for (var ii = 0; ii < dataList.length; ii++) {
				if (dataList[i].yyyymmdd == dataList[ii].yyyymmdd) {
					dataListSearch[i].yyyymmdd = dataList[ii].yyyymmdd;
					dataListSearch[i].pge_area_nm[iii] = dataList[ii].pge_area_nm;
					dataListSearch[i].pge_area_id[iii] = dataList[ii].pge_area_id;
					dataListSearch[i].view_cnt[iii] = dataList[ii].view_cnt;
				}
			}
		}

		dataListSearchSub = dataListSearch.filter(function(a) {
			var key = Object.keys(a).map(function(k) {
				return a[k];
			}).join('|');
			if (!this[key]) {
				return this[key] = true;
			}
		}, Object.create(null));

		dataListSearchSub.sort(function(a, b) {
			return (a.yyyymmdd > b.yyyymmdd) ? -1
					: (a.yyyymmdd < b.yyyymmdd) ? 1 : 0;
		});
		
		return dataListSearchSub;
	}
	
	function fn_createSearchList(dataList, totalCnt) {
		var htmlSet = "";
		var day,month,year,date;
		//var dataListSearchSub = new Array();
	
		if (dataList.length == 0) {
			fn_createEmptySelect();
			return;
		}
		
		//dataListSearchSub = dataSetList(dataList);
		
		var dataAreaId = [] ;
		for (var i = 0; i < dataList.length; i++) {
			if(i == 0) {
				dataAreaId.push(dataList[i].pge_area_id);
			} else {
				if(!dataAreaId.includes(dataList[i].pge_area_id)) {
					dataAreaId.push(dataList[i].pge_area_id);
				}
			}
		}
		
		var dataAreaYyyyMmDd = [] ;
		for (var i = 0; i < dataList.length; i++) {
			if(i == 0) {
				dataAreaYyyyMmDd.push(dataList[i].yyyymmdd);
			} else {
				if(!dataAreaYyyyMmDd.includes(dataList[i].yyyymmdd)) {
					dataAreaYyyyMmDd.push(dataList[i].yyyymmdd);
				}
			}
		}
		
		htmlSet += '<colgroup>';
		htmlSet += '<col>';
		htmlSet += '	</colgroup>';

		htmlSet += '	<tr>';
		htmlSet += '		<th>날짜</th>';
		var iii = 1;
		totalCnt = dataAreaYyyyMmDd.length;
/*  		for (i = 0; i < dataListSearchSub.length; i++) {
			for (ii = 0; ii < dataListSearchSub[i].pge_area_nm.length; ii++) {
					htmlSet += '<th>' + dataListSearchSub[i].pge_area_nm[ii] + '</th>';
				iii++;
			}
			break;
		} */
		// 그리드 타이틀
		for (var i = 0; i < dataAreaId.length; i++) {
			for (var j = 0; j < dataList.length; j++) {
				if(dataAreaId[i] == dataList[j].pge_area_id) {
					htmlSet += '<th>' + dataList[j].pge_area_nm + '</th>';
					break;
				}				
			}
		}

		htmlSet += '	</tr>';

		// 데이터
		for (var i = 0; i < dataAreaYyyyMmDd.length; i++) {
			htmlSet += '<tr>';
			year = dataAreaYyyyMmDd[i].substr(0, 4);
			month = "-" + dataAreaYyyyMmDd[i].substr(4, 2);
			day = "-" + dataAreaYyyyMmDd[i].substr(6, 2);
			date = year + month + day;
			htmlSet += '<td>' + date + '</td>';
			
			for (var j = 0; j < dataAreaId.length; j++) {
				var chkCnt = 0;
				for (var k = 0; k < dataList.length; k++) {
					if(dataAreaYyyyMmDd[i] == dataList[k].yyyymmdd && dataAreaId[j] ==  dataList[k].pge_area_id) {
						htmlSet += '<td>' + dataList[k].view_cnt + '</td>';
						chkCnt = 1;
						break;
					}
				}
				if(chkCnt == 0) {
					htmlSet += '<td> 0 </td>';
				}
			}
			htmlSet += '</tr>';
		}
		
		
		/* for (i = 0; i < dataListSearchSub.length; i++) {
			num = parseInt($("#firstIndex").val()) + parseInt(i);
			htmlSet += '<tr>';

			if (dataListSearchSub[i].yyyymmdd != null && dataListSearchSub[i].yyyymmdd != "") {
				year = dataListSearchSub[i].yyyymmdd.substr(0, 4);
				month = "-" + dataListSearchSub[i].yyyymmdd.substr(4, 2);
				day = "-" + dataListSearchSub[i].yyyymmdd.substr(6, 2);
				date = year + month + day;
			} else {
				date = "";
			}

			htmlSet += '<td>' + date + '</td>';
			for (ii = 0; ii < dataListSearchSub[i].pge_area_nm.length; ii++) {
				htmlSet += '<td>' + dataListSearchSub[i].view_cnt[ii] + '</td>';
			}

			htmlSet += '</tr>';
		} */
		$("#viewDateTbl1").empty();
		$("#viewDateTbl1").append(htmlSet);

		itemListCnt = totalCnt;
		$('#totalCnt').empty().append("총 <b>" + addComma(totalCnt) + "</b> 개");
		// 		$('#goOrder').unbind('click').bind('click',function(){
		// 			orderChange();
		// 			goOrder('goOrder_id');
		// 		});
	}

	function orderChange() {
		if (value.listOrder == "id_asc") {
			value.listOrder = "id_desc";
		} else {
			value.listOrder = "id_asc";
		}
	}

	function addComma(num) {
		var regexp = /\B(?=(\d{3})+(?!\d))/g;
		return num.toString().replace(regexp, ',');
	}

	function goOrder(val) {
		if (val == "goOrder_id") {
			if (value.listOrder == "" || value.listOrder == undefined) {
				value.listOrder = "id_desc";
			}
			//ID순으로 오름차순 정렬
			if (value.listOrder == "id_asc") {
				orderBySortDataSet.dataSet2Dtp.list.sort(function(a, b) {
					return (a.yyyymmdd < b.yyyymmdd) ? -1
							: (a.yyyymmdd > b.yyyymmdd) ? 1 : 0;
				});
				value.listOrder = "id_asc";
			} else {
				orderBySortDataSet.dataSet2Dtp.list.sort(function(a, b) {
					return (a.yyyymmdd > b.yyyymmdd) ? -1
							: (a.yyyymmdd < b.yyyymmdd) ? 1 : 0;
				});
				value.listOrder = "id_desc";
			}
		}

		fn_createSearchList(orderBySortDataSet.dataSet2Dtp.list,
				orderBySortDataSet.dataSet2Dtp.totalCnt);

	}

	function renderDataByHtml1(displayCnt, pageNo) {
		var startDate = $('#datePickerId1').data('daterangepicker').startDate
				.format('YYYYMMDD');
		var endDate = $('#datePickerId1').data('daterangepicker').endDate
				.format('YYYYMMDD');

		value.apiNm = "performance-reco-collection-area2-all";
		value.firstYmd = startDate, value.lastYmd = endDate
		serviceNm = 'recommendationStat/performance';
		$('#viewDateTbl1').empty(innerHtml);

		var innerHtml = '';
		var idxFrom, idxTo = 0;

		idxFrom = displayCnt * (pageNo - 1);
		idxTo = idxFrom + (displayCnt - 1);
		$("#firstIndex").val(idxFrom + 1);
		$("#lastIndex").val(idxTo + 1);
		if (pageOn == true) {
			apiGetForm();
		}
	}

	function linkPage(pageNo) {
		$('#firstIndex').val(1);
		$('#lastIndex').val($("#dsplyCnt1").val());
		pageOn = false;
		dataPickLoad = true;
		//Paging1(totalCnt, $("#dsplyCnt1").val(), 1, "divPageNo1");
		value = {
			'apiNm' : 'performance-reco-collection-area2-all',
			'searchText' : '',
			'searchOption1' : '',
			'searchTextInt' : 0,
			'firstIndex' : parseInt($('#firstIndex').val()),
			'lastIndex' : parseInt($('#lastIndex').val()),
			'firstYmd' : $('#datePickerId1').data('daterangepicker').startDate
					.format('YYYYMMDD'),
			'lastYmd' : $('#datePickerId1').data('daterangepicker').endDate
					.format('YYYYMMDD')
		}
		serviceNm = 'recommendationStat/performance';
		apiGetForm();
	}
</script>

<div class="mConBox">
    <div class="mCont">
        <div class="row" >
            <div class="col-md-4" id="subtitleA">
                <h2><i class="ti-layers"></i> 추천 성과</h2>
            </div>
            <div class="col-md-8">
                <div class="pageRoute">
                </div>
            </div>
        </div>

        <!-- end row -->
		<form:form modelAttribute="areaPreviewParam" id="listForm" method="post">
			<input type="hidden" name="dsplyCnt1" id="dsplyCnt1" value="10" />
			<input type="hidden" name="firstIndex" id="firstIndex" value="1" />
			<input type="hidden" name="lastIndex" id="lastIndex" value="10" />

			<div class="row">
				<div class="col-md-12 text-right">
					<div class="conBox" style="min-height: 400px;">
						<div class="dateRange">
							<label class="labelHidden2" for="datePickerId1"></label> <input
								type="text" id="datePickerId1"> <i class="ti-calendar"></i>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="gInBox" id="graph-flot-points1"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row tableTop" id="tableTop2">

				<div class="col-md-12">
					<div class="conBox" style="min-height: 400px;">		
						
						<div class="row">
							<div class="col-md-33 select-set-3">
								<p id="totalCnt">
									총 <b>0</b> 개
								</p>
							</div>
							<div class="col-md-9 text-right select-sys-1">
<!-- 								<button type="button" class="exDwBtn">엑셀</button> -->
							</div>
<!-- 							<div class="col-md-9 text-right select-sys-1"> -->
<!-- 								<select id="dsplyCnt1" name="dsplyCnt1" onchange="linkPage(1);"> -->
<!-- 									<option value="10" selected="selected">10개씩 보기</option> -->
<!-- 									<option value="20">20개씩 보기</option> -->
<!-- 								</select> -->
<!-- 							</div> -->
						</div>						
						
						<div class="row" style="height:100%;width:100%;overflow:auto">
							<div class="col-md-12">							
								<table class="viewDateTbl wide2960"  id="viewDateTbl1" >
								</table>					
							</div>

							<div class="row">
								<div class="col-md-2"></div>
								<div class="row">
	<!-- 								<div id="divPageNo1" class="col-md-12 pageNum"></div> -->
								</div>
								<div class="col-md-2 select-set-3"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form:form>

	</div>
</div>
<div id="loading2" style="z-index:1002; display:none;">
    <div class="sk-cube-grid" style="z-index:1002">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
    <div class="sk-cube-txt" style="z-index:1002">초록마을</div>
</div>