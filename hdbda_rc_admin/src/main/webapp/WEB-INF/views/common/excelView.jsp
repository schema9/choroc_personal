<%@ page language="java" contentType="application/vnd.ms-excel; charset=UTF-8" pageEncoding="UTF-8" %>
<%-- <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> --%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="ctx" value="${pageContext.request.contextPath}" />

<c:set var="fileName_this" value="${excelFileName}" />

<%
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);
	Date currTime = new Date();
	String mTime = sdf.format(currTime);

	String excelFileName = (String) pageContext.getAttribute("fileName_this");
	excelFileName = excelFileName.replaceAll(" ", "_");
	excelFileName = java.net.URLEncoder.encode(excelFileName, "UTF-8");

	/*
	response.reset();
	response.setContentType("application/vnd.ms-excel;");
	response.setHeader("Content-Type", "doesn/matter;charset=UTF-8;");

	response.setHeader("Content-Disposition", "attachment; filename=" + new String( (fileName).getBytes("KSC5601"), "8859_1") + "_" + mTime + ".xls;");
	response.setHeader("Content-Description", "JSP Generated Data");
	*/

	response.setContentType("application/octer-stream");
	response.setHeader("Content-Transper-Encoding", "binary");
	response.setHeader("Content-Disposition", "attachement; filename=" + excelFileName + "_" + mTime + ".xls;");
%>

<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>엑셀 다운로드 공통 처리 페이지</title>
</head>

<body>
	
	<div>
		<jsp:include page="/WEB-INF/views/${contentPage}.jsp" flush="false" />
	</div>
	
</body>

</html>

