<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page import="java.io.*"%>
<%@ page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator"%>
<%@ taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page"%>

<%@ taglib prefix="ui" uri="http://www.ebro.com/ctl/ui"%>
<%@ page import="com.choroc.admin.dto.ServiceInfo" %>
<%@ page import="com.choroc.admin.dto.DomainInfo" %>
<%@ page import="com.choroc.admin.dto.AuthUser" %>

<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
	request.setCharacterEncoding("UTF-8");

	String serviceId = "";
	String serviceNm = "";
	String domain = "";
	String accessKey = "";
	String domainNm = "";
	int roleId = 0;
	
	String test = "";
	Date beforeLoginDate = null;
	String beforeLoginDt = "";
	
	String contextPath = request.getContextPath();
	response.setContentType("text/html; charset=UTF-8");
%>
<spring:htmlEscape defaultHtmlEscape="true" />
