<%------------------------------------------------------------------------
- Menu  : 추천환경관리 > 추천영역미리보기 > 제품별 추천제품
- ID    : RECO_AP_002
- Ref.  : -
- URI   : -
- History
- 2018-12-10 created by ljy
------------------------------------------------------------------------%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../common/common.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	
<script type="text/javascript">
var controller = multiApiSet();
var serviceNm = '';
var value;
var columnSetRow = 0;
var columnSetRowIndex = 0;
var itemListCnt;
var page_resultData;
var apiNmGlobal;
var pageOn = false;
var categoryCd = "";
var usr_gubun = "일반회원";
var orderBySortDataSet = {'dataSet1Dtp':'','dataSet2Dtp':'','dataSet3Dtp':''};
orderBySortDataSet.dataSet1Dtp = {};
orderBySortDataSet.dataSet2Dtp = {};
orderBySortDataSet.dataSet3Dtp = {};
orderBySortDataSet.dataSet1Dtp.list = {};
orderBySortDataSet.dataSet1Dtp.totalCnt = {};
orderBySortDataSet.dataSet2Dtp.list = {};
orderBySortDataSet.dataSet2Dtp.totalCnt = {};
orderBySortDataSet.dataSet3Dtp.list = {};
orderBySortDataSet.dataSet3Dtp.totalCnt = {};	

$(window).ready(function(){
     pageEvents();
});

function multiApiSet(){
	return "/environment/areaPreview";

	var api_url, api_protocol = location.protocol;
	var api_chkUrl = window.location.href;
	if('' != api_chkUrl) {
		if('http:' == api_protocol) {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
		} else {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
		}
	} else {
		return false;
	}
}

var goPaging_divPageNo1 = function(cPage){
	pageOn = true;
    Paging1(itemListCnt, $("#dsplyCnt1").val(), cPage, "divPageNo1");
};
 function pageEvents(){
	 fn_createCategory();
	 
	// 추천MD관리 등록 팝업
	$('#btnPopUpAdd').unbind('click').bind('click',function(){
		fn_util_divPopUpControl('divPopUpA',true);
// 		fn_setColumnTitle();
	});
	
    // 저장 
    $('#btnA-insert').unbind('click').bind('click',function(){
        fn_add();
    });
    
    // 취소
    $('#btnA-cancel').unbind('click').bind('click',function(){
    	$('#popUpA-content_cd').val("");
    	fn_util_divPopUpControl('divPopUpA',false);
    });
	
	
 }
 
 function openpopup(){
		fn_util_divPopUpControl('divPopUpA',true);
// 		fn_setColumnTitle();
 }
 
//등록팝업 - 저장
 function fn_add(){
 	//var sortation = $('#searchOptionNm').val();  // 제품, 키워드 구분
 	var content_cd = $('#popUpA-content_cd').val();
 	var create_id = $('#popUpA-create_id').val();
 	
 	var url = '/environment/algorithm/recommand/manager/rc-md-insert.do'
//  	var url = '/environment/algorithm/recommand/manager/rc-except-insert.do'
 			+ '?sortation=P' //+ sortation 
 			+ '&content_cd=' + content_cd 
 			+ '&create_id=' + create_id
 			; 
 	
 	if (fn_isValid_insert()){
 		if (confirm("등록 하시겠습니까?")) {
 		   	var param = $('#addForm').serialize(); // 객체 직렬화
 		    $.ajax({
 		    type: 'POST',
 		    url: url,
 		    crossDomain:true,
 		    async:false,
 		   	success:function(resultData){
 		   	if(resultData != null){
 		   		if (resultData.result == "입력한 값이 모두 중복입니다.") {
 			    	alert(resultData.result);
 		   		}else{
 		   			alert(resultData.result);
 		   	    	$('#popUpA-content_cd').val("");
 		   			fn_util_divPopUpControl('divPopUpA',false);
//  		   			listAjax('P' , '', $('#dsplyCnt1').val(), '1'); // 제품 , 검색어, 리스트 갯수 , 1페이지
//  			        listAjax($('#searchOptionNm').val() , '', $('#dsplyCnt1').val(), '1'); // 제품 , 검색어, 리스트 갯수 , 1페이지
 		   		}
 		    }
 		    },
 		    error:function(e){
 		    alert('서버와의 통신에 실패하였습니다.');
 		    }
 		});
 		}
 	}
 }
 
//예외처리
 function fn_isValid_insert(){
     var isValid = false;
 	if ($('#popUpA-content_cd').val() == "") {
 			alert('제품 코드를 입력해주세요');
 			$('#popUpA-content_cd').focus();
 		} else {
 			isValid = true;
 		}
 		return isValid;
 }
 
 function fn_selectCustGroup(){
	var cust_group = $("#cust_group option:selected").text();
	
	if($('#subtitleA h2').text() != ""){
		$('#subtitleB').empty().append('<h2> (' + cust_group + ')</h2>');	
	}
 }
 
function fn_categoryCdSet() {	
	if ($('#category1').val() == 'RC_CF_BUY'){
		categoryCd = 'RC_CF_BUY';
// 		categoryCd = 'RC_CF_BUY';
	}
	
	if ($('#category1').val() == 'RC_CF_VIEW'){
		categoryCd = 'RC_CF_VIEW';
// 		categoryCd = 'RC_CF_BUY_CATE';
	}
	
	if ($('#category1').val() == 'RC_VIEW_VIEW'){
		categoryCd = 'RC_VIEW';
	}
}
	 
function fn_createCategory(){
	value = {
			'apiNm' : 'category-select',
			'algrthCd':'RC_CF_BUY,RC_CF_VIEW,RC_VIEW_VIEW',
// 			'algrthCd':'rc_view_view,rc_view_buy,rc_buy_buy,rc_view_search,rc_md_item',
// 			'algrthCd':'rc_search_view,rc_search_search,rc_search_buy,rc_view_search,MD_ITEM,rc_view_view,rc_view_buy,rc_buy_buy',
// 			'algrthCd':'ar_view_view,ar_view_buy,ar_buy_buy,ar_view_srh',
			'custGroup' : $("#cust_group option:selected").val(),
			'searchText' : '',
// 			'searchTextInt' : 0,
			'firstIndex' : parseInt($('#firstIndex').val()),
			'lastIndex' : parseInt($('#lastIndex').val())
		}
	serviceNm = 'recommand';
	apiGetForm();
}

function fn_createCategorySelect(dataList,totalCnt) {
	var htmlSet = "";
	
	htmlSet += '<div class="col-md-9">';
	htmlSet += '<select name="cust_group" id="cust_group" onchange="fn_selectCustGroup();">';
	htmlSet += '	<option value="1">일반회원</option>';
	htmlSet += '	<option value="2">사회적경제기업</option>';
	htmlSet += '	<option value="3">공공기관</option>';
	htmlSet += '	<option value="4">STORE 36.5매장</option>';
	htmlSet += '	<option value="5">유통채널</option>';

// 	htmlSet += '	<option value="product-reco">일반회원</option>';
// 	htmlSet += '	<option value="search-product-reco">공공기관</option>';
// 	htmlSet += '	<option value="search-word-reco">사회적경제기업</option>';
// 	htmlSet += '	<option value="user-reco">STORE 36.5매장</option>';
// 	htmlSet += '	<option value="custom-reco">유통채널</option>';
	htmlSet += '</select>';
	
	htmlSet += '<span style="display:inline-block;width:20px;"></span>';
	
	
	htmlSet += '<select name="category" id="category1">';	
	if(dataList != null){
		for (i=0;i<dataList.length;i++){
			htmlSet += '<option value="'+dataList[i].algrth_id+'">'+dataList[i].algrth_nm+'</option>';	
		}		
	}
	htmlSet += '</select>';
	htmlSet += '</div>';
	
// 	htmlSet += '<div class="col-md-33 text-right">';
// 	htmlSet += '<button type="button" title="추천MD 제품을 추가할 수 있습니다." id="btnPopUpAdd" class="AddDefaultBtn" onclick="openpopup();">';
// 	htmlSet += '	<i class="fa fa-plus"></i> 추천MD관리 등록';
// 	htmlSet += '</button>';
// 	htmlSet += '</div>';
	
	$("#tableTop3").css("display","none");
	$("#conBox1").empty();
	$("#viewDateTbl1").empty();
	$("#viewDateTbl2").empty();
	$("#conBox1").append(htmlSet);
	
	
	fn_selectCustGroup($("#cust_group option:selected").text());
	
	
	
	if (dataList.length == 0) {
		fn_createEmptySelect();
		return;
	}
	$("#category1, #cust_group").on('change', function() {
		 fn_categoryCdSet();
			value = {
				'apiNm' : 'product-reco',
				'categoryCd' : categoryCd,
				'custGroup' : $("#cust_group option:selected").val(),
				'searchText' : '',
				'searchOption1' : '',
// 				'searchTextInt' : 0,
				'firstIndex' : parseInt($('#firstIndex').val()),
				'lastIndex' : parseInt($('#lastIndex').val())
			}
			
			fn_changeSearchOption1();
			
			$("#searchText1").val("");
			serviceNm = 'recommand/logging';
			apiGetForm();
	 });
		fn_createInitOn();
}
 
 function fn_createInitOn(){
	 if (value != {}) {
		 fn_categoryCdSet();
			value = {	
				'apiNm' : 'product-reco',
				'categoryCd' : categoryCd,
				'custGroup' : $("#cust_group option:selected").val(),
				'searchText' : '',
				'searchOption1' : '',
// 				'searchTextInt' : 0,
				'firstIndex' : parseInt($('#firstIndex').val()),
				'lastIndex' : parseInt($('#lastIndex').val())
			}
			serviceNm = 'recommand/logging';
			apiGetForm();
	}
 }
 
 function fn_changeSearchOption1(){
	 	var htmlSet = "";
	 	
// 	 	if ($('#category1').val() == 'RC_CF_BUY' || $('#category1').val() == 'RC_CF_VIEW'){
// 	 		htmlSet += '<option value="CUSTNAME">고객명</option>';
// 	 		htmlSet += '<option value="CUSTINSTTNAME">기관명</option>';
// 	 	}

	 	if ($('#cust_group').val() == '3'){
	 		htmlSet += '<option value="CUSTINSTTNAME">기관명</option>';
	 	} else {
	 		htmlSet += '<option value="CUSTNAME">고객명</option>';
	 	}
		
		htmlSet += '<option value="ID">제품코드</option>';
		htmlSet += '<option value="NAME">제품명</option>';
	 
	 	$("#searchOption1").empty();
	 	$("#searchOption1").append(htmlSet);
	 
	 }
 
 
 // 리스트 목록 클릭 - 상세리스트
 function searchListDetailAll(val,val2){
	 var m_api = $("#cust_group option:selected").val();
	 var d_api = "product-reco-product-all";
	 
// 	 if(m_api == "product-reco") {		// 일반회원
// 		 d_api = "product-reco-product-all";
// 	 } else if(m_api == "search-product-reco") {	// 공공기관
// 		 d_api = "search-product-reco-product-all";
// 	 } else if(m_api == "search-word-reco") {		//사회적경제기업
// 		 d_api = "search-word-reco-word-all";
// 	 } else if(m_api == "user-reco") {		// STORE 36.5매장
// 		 d_api = "user-reco-product-all";
// 	 } else if(m_api == "custom-reco") {	// 유통채널
// 		 d_api = "custom-reco-detail";
// 	 }

	 value = {
			'apiNm' : d_api,
// 			'apiNm' : 'product-reco-product-all',
		 	'tableNm' : $('#category1').val(),
			'itemId' : val,
			'categoryCd': val2,
			'custGroup' : $("#cust_group option:selected").val(),
			'searchText' : '',
			'searchOption1' : '',
// 			'searchTextInt' : 0,
			'listOrder' : 0,
			'firstIndex' : 0,
			'lastIndex' : 0
	 }
	 serviceNm = 'recommand';
    apiGetForm();
 }

	function apiGetForm() {
		var url = controller +'/'+ serviceNm + "/" + value.apiNm + ".do";
		var param = {};
		var succMs = "테이블이 조회 되었습니다.";
		var errorMs = "시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.";
		var netErrMs = "네트워크 통신 에러";
		param = {
			'tableNm' : value.tableNm,
			'itemId' : value.itemId,
			'categoryCd' : value.categoryCd,
			'algrthCd' : value.algrthCd,
			'custGroup' : value.custGroup,
			'searchText' : $.trim(value.searchText),
			'searchOption1' : value.searchOption1,
			'firstIndex' : parseInt($('#firstIndex').val()),
			'lastIndex' : parseInt($('#lastIndex').val())
		};
		fn_isAjaxSet(param, url, succMs, errorMs, netErrMs);
	}

	function fn_isAjaxSet(param, url, succMs, errorMs, netErrMs) {
		setTimeout(function(){
		$.ajax({
			type : 'POST',
			url : url,
			dataType : 'JSON',
			data : param,
			beforeSend:function(){
				maskOn2();
		        $("#loading2").show();
			},
			complete:function(){
			},
			success : function(data) {
				if (data.returnErrMsg == "") {
					dataSet = data.list.dataSet;
					totalCnt = data.list.totalCnt;
					page_resultData = data.list.dataSet;
					apiNmGlobal = data.apiNm;
					
		         	if (data.apiNm=="category-select") fn_createCategorySelect(dataSet,totalCnt);
					
					if (data.apiNm == "product-reco") {
						orderBySortDataSet.dataSet2Dtp.list = dataSet;
	              		orderBySortDataSet.dataSet2Dtp.totalCnt = totalCnt;
	              		if(dataSet.length > 0){
							if (pageOn == false){
								Paging1(totalCnt, $("#dsplyCnt1").val(), 1, "divPageNo1");
								
								if ($('#category1').val() == 'RC_CF_BUY' || $('#category1').val() == 'RC_CF_VIEW'){
									fn_createSearchListCF(dataSet, totalCnt);
								} else {
									fn_createSearchList(dataSet, totalCnt);	
								}
								
																
							}	    
	              		} else {
							fn_createEmptySelect();
							maskOff2();
	              		}
					}
					
					 if (data.apiNm=="product-reco-product-all") {
			           		orderBySortDataSet.dataSet3Dtp.list = dataSet;
			              	orderBySortDataSet.dataSet3Dtp.totalCnt = totalCnt;
			              	goOrder("goOrder_idx");
		               }
				} else {
                     alert(errorMs + "(" + data.returnErrMsg + ")");
				}
				$("#loading2").hide();
                maskOff2();
			},
			error : function(e) {
				alert(netErrMs + ": " + e);
				$("#loading2").hide();
                maskOff2();
			}
		});
		}, 1);
	}

	function fn_createEmptySelect() {
		
		var htmlSet = '';
		htmlSet += '<colgroup>';
		
		htmlSet += '<col width="80px" style="display:none;">';	// 고객명
		htmlSet += '<col width="80px" style="display:none;">';	// 고객명

		htmlSet += '<col width="80px">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="250px">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="200px">';
		htmlSet += '<col width="5%">';
		htmlSet += '<col width="10%">';
		htmlSet += '<col width="10%">';
		htmlSet += '<col width="10%">';
		htmlSet += '</colgroup>';

		htmlSet += '<tr>';
		
		htmlSet += '<th rowspan="2" style="display:none;">고객명</th>';
		htmlSet += '<th rowspan="2" style="display:none;">기관명</th>';

		htmlSet += '<th rowspan="2">제품코드</th>';
		htmlSet += '<th rowspan="2">제품이미지</th>';
		htmlSet += '<th rowspan="2">제품명</th>';
		htmlSet += '<th rowspan="2">판매가</th>';
		htmlSet += '<th rowspan="2">판매자명</th>';
		htmlSet += '<th colspan="4">카테고리</th>';
		htmlSet += '</tr>';
		htmlSet += '<tr>';
		htmlSet += '<th>대</th>';
		htmlSet += '<th>중</th>';
		htmlSet += '<th>소</th>';
		htmlSet += '<th>세</th>';
		htmlSet += '</tr>';
		htmlSet += '<tr><td colspan="20">No data available in table</td></tr>';
		$("#viewDateTbl1").empty();
		$("#viewDateTbl1").append(htmlSet);
		
		$("#tableTop3").css("display","none");
		$("#viewDateTbl2").empty();
		$('#totalCount').empty().text("0");
	}
	
	function fn_createEmptySelect2() {
		var htmlSet = '';
		htmlSet += '<colgroup>';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="100px">';
		htmlSet += '<col width="450px">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="250px">';
		htmlSet += '<col width="5%">';
		htmlSet += '<col width="10%">';
		htmlSet += '<col width="10%">';
		htmlSet += '<col width="10%">';
		htmlSet += '</colgroup>';

		htmlSet += '<tr>';
		htmlSet += '<th rowspan="2">제품ID</th>';
		htmlSet += '<th rowspan="2">순위</th>';
		htmlSet += '<th rowspan="2">제품이미지</th>';
		htmlSet += '<th rowspan="2">제품명</th>';
		htmlSet += '<th rowspan="2">판매가</th>';
		htmlSet += '<th rowspan="2">판매자명</th>'
		htmlSet += '<th colspan="4">카테고리</th>';
		htmlSet += '</tr>';
		htmlSet += '<tr>';
		htmlSet += '<th>대</th>';
		htmlSet += '<th>중</th>';
		htmlSet += '<th>소</th>';
		htmlSet += '<th>세</th>';
		htmlSet += '</tr>';
		htmlSet += '<tr><td colspan="20">No data available in table</td></tr>';
		$('#totalCount2').empty().text("0");
		$("#viewDateTbl2").empty();
		$("#viewDateTbl2").append(htmlSet);
	}
	
	function addComma(num) {
	    var regexp = /\B(?=(\d{3})+(?!\d))/g;
	    return num.toString().replace(regexp,',');
	}
	
	
	
	function fn_createSearchListCF  (dataList, totalCnt) {
		var htmlSet = "";
		htmlSet += '<colgroup>';
		htmlSet += '<col width="80px" style="display:none;">';	// 고객명
		htmlSet += '<col width="80px" style="display:none;">';	// 기관명
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="250px">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="200px">';
		htmlSet += '<col width="5%">';
		htmlSet += '<col width="10%">';
		htmlSet += '<col width="10%">';
		htmlSet += '<col width="10%">';
		htmlSet += '</colgroup>';
		
		htmlSet += '<tr>';
		htmlSet += '<th rowspan="2"  style="display:none;">고객명</th>';
		htmlSet += '<th rowspan="2"  style="display:none;">기관명</th>';
		htmlSet += '<th rowspan="2">제품코드</th>';
		htmlSet += '<th rowspan="2">제품이미지</th>';
		htmlSet += '<th rowspan="2">제품명</th>';
		htmlSet += '<th rowspan="2">판매가</th>';
		htmlSet += '<th rowspan="2">판매자명</th>'
		htmlSet += '<th colspan="4">카테고리</th>';
		htmlSet += '</tr>';
		htmlSet += '<tr>';
		htmlSet += '<th>대</th>';
		htmlSet += '<th>중</th>';
		htmlSet += '<th>소</th>';
		htmlSet += '<th>세</th>';
		htmlSet += '</tr>';
		for (i = 0; i < dataList.length; i++) {
			if (dataList[i].cust_name == null || dataList[i].cust_name == undefined) dataList[i].cust_name="";
			if (dataList[i].cust_instt_name == null || dataList[i].cust_instt_name == undefined) dataList[i].cust_instt_name="";
			if (dataList[i].item_name == null || dataList[i].item_name == undefined) dataList[i].item_name="";
			if (dataList[i].catea1_cd == null || dataList[i].catea1_cd == undefined) dataList[i].catea1_cd="";
			if (dataList[i].catea2_cd == null || dataList[i].catea2_cd == undefined) dataList[i].catea2_cd="";
			if (dataList[i].catea3_cd == null || dataList[i].catea3_cd == undefined) dataList[i].catea3_cd="";
			if (dataList[i].catea4_cd == null || dataList[i].catea4_cd == undefined) dataList[i].catea4_cd="";
			
			var v_thumb_img = dataList[i].thumb;
			if(v_thumb_img){
				v_thumb_img = '<spring:eval expression="@environment.getProperty('sepp.url')" />' 
							+ dataList[i].thumb
							;	
			} else {
				v_thumb_img = '<spring:eval expression="@environment.getProperty('sepp.url')" />' 
							+ '<spring:eval expression="@environment.getProperty('sepp.noimage.path')" />'
							;
			}
			
			var v_sale_price = 0;
			if($.isNumeric(dataList[i].sale_price)){
				v_sale_price = addComma(Number(dataList[i].sale_price));
			}
			v_sale_price = v_sale_price + "원";
						
			htmlSet += '<tr>';
			htmlSet += '<td style="display:none;">' + dataList[i].cust_name + '</td>';
			htmlSet += '<td style="display:none;">' + dataList[i].cust_instt_name + '</td>';
			htmlSet += '<td>' + dataList[i].item_id + '</td>';
// 			htmlSet += '<td><img src="'+dataList[i].thumb+'"></td>';
// 			htmlSet += '<td><img src="<spring:eval expression="@environment.getProperty('sepp.url')" />' + dataList[i].thumb + '"></td>';
			htmlSet += '<td><img src="' + v_thumb_img + '"></td>';
			
			htmlSet += '<td class="txtL">';
			htmlSet +='<b>'+dataList[i].item_name+'</b>';
			htmlSet += '</td>';
			htmlSet += '<td class="txtR">' + v_sale_price + '</td>';
			htmlSet += '<td class="txtL">' + dataList[i].seller_name + '</td>';
// 			htmlSet += '<td>' + addComma(dataList[i].cnt) + '</td>';
			htmlSet += '<td>' + dataList[i].catea1_nm + '</td>';
			htmlSet += '<td>' + dataList[i].catea2_nm + '</td>';
			htmlSet += '<td>' + dataList[i].catea3_nm + '</td>';
			htmlSet += '<td>' + dataList[i].catea4_nm + '</td>';
			htmlSet += '</tr>';
		}
		$("#tableTop3").css("display", "none");
		$("#viewDateTbl1").empty();
		$("#viewDateTbl2").empty();
		$("#viewDateTbl1").append(htmlSet);
		
// 		if ($('#category1').val() == 'RC_CF_BUY' || $('#category1').val() == 'RC_CF_VIEW'){
// 			$("#divPageNo1").hide();
// 		} else {
// 			$("#divPageNo1").show();				
// 		}
		
		$('#goOrderSearch_idx').unbind('click').bind('click', function() {
			value.apiNm = "product-reco";
			orderChange();
			goOrder(this.id);
		});
		itemListCnt = totalCnt;
		$('#totalCount').empty().text(addComma(totalCnt));
	}

	function fn_createSearchList(dataList, totalCnt) {
		var htmlSet = "";
		htmlSet += '<colgroup>';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="250px">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="200px">';
		htmlSet += '<col width="5%">';
		htmlSet += '<col width="10%">';
		htmlSet += '<col width="10%">';
		htmlSet += '<col width="10%">';
		htmlSet += '</colgroup>';
		
		htmlSet += '<tr>';
		htmlSet += '<th rowspan="2">제품코드</th>';
		htmlSet += '<th rowspan="2">제품이미지</th>';
		htmlSet += '<th rowspan="2">제품명</th>';
		htmlSet += '<th rowspan="2">판매가</th>';
		htmlSet += '<th rowspan="2">판매자명</th>'
		htmlSet += '<th colspan="4">카테고리</th>';
		htmlSet += '</tr>';
		htmlSet += '<tr>';
		htmlSet += '<th>대</th>';
		htmlSet += '<th>중</th>';
		htmlSet += '<th>소</th>';
		htmlSet += '<th>세</th>';
		htmlSet += '</tr>';
		for (i = 0; i < dataList.length; i++) {
			if (dataList[i].item_name == null || dataList[i].item_name == undefined) dataList[i].item_name="";
			if (dataList[i].catea1_cd == null || dataList[i].catea1_cd == undefined) dataList[i].catea1_cd="";
			if (dataList[i].catea2_cd == null || dataList[i].catea2_cd == undefined) dataList[i].catea2_cd="";
			if (dataList[i].catea3_cd == null || dataList[i].catea3_cd == undefined) dataList[i].catea3_cd="";
			if (dataList[i].catea4_cd == null || dataList[i].catea4_cd == undefined) dataList[i].catea4_cd="";
			
			var v_thumb_img = dataList[i].thumb;
			if(v_thumb_img){
				v_thumb_img = '<spring:eval expression="@environment.getProperty('sepp.url')" />' 
							+ dataList[i].thumb
							;	
			} else {
				v_thumb_img = '<spring:eval expression="@environment.getProperty('sepp.url')" />' 
							+ '<spring:eval expression="@environment.getProperty('sepp.noimage.path')" />'
							;
			}
			
			var v_sale_price = 0;
			if($.isNumeric(dataList[i].sale_price)){
				v_sale_price = addComma(Number(dataList[i].sale_price));
			}
			v_sale_price = v_sale_price + "원";
						
			htmlSet += '<tr>';
			htmlSet += '<td>' + dataList[i].item_id + '</td>';
// 			htmlSet += '<td><img src="'+dataList[i].thumb+'"></td>';
// 			htmlSet += '<td><img src="<spring:eval expression="@environment.getProperty('sepp.url')" />' + dataList[i].thumb + '"></td>';
			htmlSet += '<td><img src="' + v_thumb_img + '"></td>';
			
			htmlSet += '<td class="txtL">';
			
// 			if ($('#category1').val() == 'RC_CF_BUY' || $('#category1').val() == 'RC_CF_VIEW'){
// 				htmlSet +='<b>'+dataList[i].item_name+'</b>';
// 			} else {
				htmlSet +='<b><a href="#" id="'+dataList[i].item_id+"|"+ dataList[i].catea1_cd
				+ '" name="'+ dataList[i].item_name
				+ '" onClick="fn_checkForm(this.id,this.name);">'+dataList[i].item_name+'</a></b>';				
// 			}
			
			htmlSet += '</td>';
			htmlSet += '<td class="txtR">' + v_sale_price + '</td>';
			htmlSet += '<td class="txtL">' + dataList[i].seller_name + '</td>';
// 			htmlSet += '<td>' + addComma(dataList[i].cnt) + '</td>';
			htmlSet += '<td>' + dataList[i].catea1_nm + '</td>';
			htmlSet += '<td>' + dataList[i].catea2_nm + '</td>';
			htmlSet += '<td>' + dataList[i].catea3_nm + '</td>';
			htmlSet += '<td>' + dataList[i].catea4_nm + '</td>';
			htmlSet += '</tr>';
		}
		$("#tableTop3").css("display", "none");
		$("#viewDateTbl1").empty();
		$("#viewDateTbl2").empty();
		$("#viewDateTbl1").append(htmlSet);
		
// 		if ($('#category1').val() == 'RC_CF_BUY' || $('#category1').val() == 'RC_CF_VIEW'){
// 			$("#divPageNo1").hide();
// 		} else {
// 			$("#divPageNo1").show();				
// 		}
		
		$('#goOrderSearch_idx').unbind('click').bind('click', function() {
			value.apiNm = "product-reco";
			orderChange();
			goOrder(this.id);
		});
		itemListCnt = totalCnt;
		$('#totalCount').empty().text(addComma(totalCnt));
	}

	function orderChange(){
		if (value.listOrder == "id_asc") {
			value.listOrder = "id_desc";
		} else {
			value.listOrder = "id_asc";
		}
	}

	
	function fn_createSearchDetailList(dataList, totalCnt) {
		var htmlSet = "";
		htmlSet += '<colgroup>';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="100px">';
		htmlSet += '<col width="450px">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="250px">';
		htmlSet += '<col width="5%">';
		htmlSet += '<col width="10%">';
		htmlSet += '<col width="10%">';
		htmlSet += '<col width="10%">';
		htmlSet += '</colgroup>';

		htmlSet += '<tr>';
		htmlSet += '	<th rowspan="2">제품 ID</th>';
		htmlSet += '	<th rowspan="2">순위 </th>';
		htmlSet += '	<th rowspan="2">제품이미지</th>';
		htmlSet += '	<th rowspan="2">제품명</th>';
		htmlSet += '	<th rowspan="2">판매가</th>';
		htmlSet += '	<th rowspan="2">판매자명</th>'
		
		htmlSet += '	<th colspan="4">카테고리</th>';
		htmlSet += '</tr>';
		htmlSet += '<tr>';
		htmlSet += '	<th>대</th>';
		htmlSet += '	<th>중</th>';
		htmlSet += '	<th>소</th>';
		htmlSet += '	<th>세</th>';
		htmlSet += '</tr>';
		if (dataList.length == 0) {
			fn_createEmptySelect2();
			$("#tableTop3").css("display", "block");
			return;
		}

		for (i = 0; i < dataList.length; i++) {
			if (dataList[i].item_name == null || dataList[i].item_name == undefined) dataList[i].item_name="";
			if (dataList[i].cate_1_cd == null || dataList[i].cate_1_cd == undefined) dataList[i].cate_1_cd="";
			if (dataList[i].cate_2_cd == null || dataList[i].cate_2_cd == undefined) dataList[i].cate_2_cd="";
			if (dataList[i].cate_3_cd == null || dataList[i].cate_3_cd == undefined) dataList[i].cate_3_cd="";
			if (dataList[i].cate_4_cd == null || dataList[i].cate_4_cd == undefined) dataList[i].cate_4_cd="";
			
			var v_thumb_img = dataList[i].thumb_url;
			if(v_thumb_img){
				v_thumb_img = '<spring:eval expression="@environment.getProperty('sepp.url')" />' 
							+ dataList[i].thumb_url
							;	
			} else {
				v_thumb_img = '<spring:eval expression="@environment.getProperty('sepp.url')" />' 
							+ '<spring:eval expression="@environment.getProperty('sepp.noimage.path')" />'
							;
			}
			
			var v_sale_price = 0;
			if($.isNumeric(dataList[i].sale_price)){
				v_sale_price = addComma(Number(dataList[i].sale_price));
			}
			v_sale_price = v_sale_price + "원";
			
			htmlSet += '<tr>';
			htmlSet += '<td>' + dataList[i].rc_item_cd + '</td>';
			htmlSet += '<td class="txtR" style="padding-right:30px;">' + dataList[i].rc_item_rank + '</td>';
// 			htmlSet += '<td><img src="'+dataList[i].thumb_url+'"></td>';
// 			htmlSet += '<td><img src="<spring:eval expression="@environment.getProperty('sepp.url')" />'+dataList[i].thumb_url+'"></td>';
			htmlSet += '<td><img src="' + v_thumb_img + '"></td>';
			htmlSet += '<td class="txtL">';
			htmlSet += 		'<b>' + dataList[i].item_name + '</b>';
			htmlSet += '</td>';
			htmlSet += '<td class="txtR">' + v_sale_price + '</td>';
			htmlSet += '<td class="txtL">' + dataList[i].seller_name + '</td>';
			htmlSet += '<td>' + dataList[i].catea1_nm + '</td>';
			htmlSet += '<td>' + dataList[i].catea2_nm + '</td>';
			htmlSet += '<td>' + dataList[i].catea3_nm + '</td>';
			htmlSet += '<td>' + dataList[i].catea4_nm + '</td>';
			htmlSet += '</tr>';
		}
		$('#totalCount2').empty().text(addComma(totalCnt));

		$("#viewDateTbl2").empty();
		$("#viewDateTbl2").append(htmlSet);
		$("#tableTop3").css("display", "block");
	}
	
	
	function fn_createSearchDetailList2(dataList, totalCnt) {
		var htmlSet = "";
		if (dataList.length == 0) {
			fn_createEmptySelect3();
			$("#tableTop3").css("display", "block");
			return;
		}
		
		for (i = 0; i < dataList.length; i++) {
			if(i == 0){
				htmlSet += '<ul>';		
			}else if(i != 0 && i%14 == 0){
				htmlSet += '</ul><ul>';
			}
			if (dataList[i].rc_search == null || dataList[i].rc_search == undefined) dataList[i].rc_search="";
			num = i+1;
			htmlSet += '<li title="' + dataList[i].rc_search + '"><span>'+num+'</span> '+dataList[i].rc_search+'</li>';
		}
			htmlSet += '</ul>';
		
		$('#totalCount2').empty().text(addComma(totalCnt));
		$("#viewDateTbl2").empty();
		$("#viewDateTbl2").append(htmlSet);
		$("#viewDateTbl2").addClass("recoSearchword");
		$("#tableTop3").css("display", "block");
	}
	
	function fn_checkForm(val,val2) {
		var str = val;
    	var strArr = str.split('|');
    	var id = strArr[0];
    	var categoryCd = strArr[1];
		$('#searchDetailNm').empty();
   		$('#searchDetailNm').append(val2);
		searchListDetailAll(id,categoryCd);
	}

	function fn_search() {
		var searchCnt = 0;
		if ($("#searchText1").val() == "") {
			//$("#searchOption1").val("");
		}
		if($("#searchOption1").val() == "") {
			$("#searchText1").val("");
		}
		fn_categoryCdSet();

		value = {
			'apiNm' : 'product-reco',
			'categoryCd'  : categoryCd,
			'custGroup' : $("#cust_group option:selected").val(),
			'searchText' : $("#searchText1").val(),
			'searchOption1' : $("#searchOption1").val(),
			'firstIndex' :0,
			'lastIndex' : 10
		}
		serviceNm = 'recommand/logging';
		pageOn = false;
		apiGetForm();
	}

	function goOrder(val) {
		if ($('#category1').val() != 'ar_view_srh'){
			if (val == "goOrderSearch_idx") {
				if (value.listOrder == "" || value.listOrder == undefined) {
					value.listOrder = "id_desc";
				}
				//ID순으로 오름차순 정렬
				if (value.listOrder == "id_asc") {
					orderBySortDataSet.dataSet2Dtp.list.sort(function(a, b) {
						return (a.item_id < b.item_id) ? -1
								: (a.item_id > b.item_id) ? 1 : 0;
					});
					value.listOrder = "id_asc";
				} else {
					orderBySortDataSet.dataSet2Dtp.list.sort(function(a, b) {
						return (a.item_id > b.item_id) ? -1
								: (a.item_id < b.item_id) ? 1 : 0;
					});
					value.listOrder = "id_desc";
				}
				
				if ($('#category1').val() == 'RC_CF_BUY' || $('#category1').val() == 'RC_CF_VIEW'){
					fn_createSearchListCF(orderBySortDataSet.dataSet2Dtp.list, orderBySortDataSet.dataSet2Dtp.totalCnt);
				} else {
					fn_createSearchList(orderBySortDataSet.dataSet2Dtp.list, orderBySortDataSet.dataSet2Dtp.totalCnt);
				}
				
				return;
			}
			if (val == "goOrder_id") {
				if (value.listOrder2 == "id_asc") {
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
						return(a.rc_item_cd > b.rc_item_cd) ? - 1 : (a.rc_item_cd < b.rc_item_cd) ? 1 : 0;
					});
					value.listOrder2 = "id_desc";
				} else {
					//ID순으로 오름차순 정렬
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
						return(a.rc_item_cd < b.rc_item_cd) ? - 1 : (a.rc_item_cd > b.rc_item_cd) ? 1 : 0;
					});
					value.listOrder2 = "id_asc";
				}
			}
			if (val == "goOrder_idx") {
				if (value.listOrder2 == "id_asc") {
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
						return(a.rc_item_rank > b.rc_item_rank) ? - 1 : (a.rc_item_rank < b.rc_item_rank) ? 1 : 0;
					});
					value.listOrder2 = "id_desc";
				} else {
					//ID순으로 오름차순 정렬
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
						return(a.rc_item_rank < b.rc_item_rank) ? - 1 : (a.rc_item_rank > b.rc_item_rank) ? 1 : 0;
					});
					value.listOrder2 = "id_asc";
				}
			}
			fn_createSearchDetailList(orderBySortDataSet.dataSet3Dtp.list,orderBySortDataSet.dataSet3Dtp.totalCnt);
		} else {
			if (val == "goOrder_idx") {
				if (value.listOrder2 == "id_asc") {
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a, b) {
						return (a.rc_search < b.rc_search) ? -1
								: (a.rc_search > b.rc_search) ? 1 : 0;
					});
					value.listOrder2 = "id_asc";
				} else {
					//ID순으로 오름차순 정렬
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a, b) {
						return (a.rc_search < b.rc_search) ? -1
								: (a.rc_search > b.rc_search) ? 1 : 0;
					});
					value.listOrder2 = "id_desc";
				}
			}
			fn_createSearchDetailList2(orderBySortDataSet.dataSet3Dtp.list,orderBySortDataSet.dataSet3Dtp.totalCnt);	
		}
		
	}

	function renderDataByHtml1(displayCnt, pageNo) {
		$("#searchDetailNm").empty();
		$('#totalCount2').empty().text("0");
		$("#viewDateTbl2").empty();
		$("#tableTop3").css("display", "none");
		fn_categoryCdSet();
		 
		value.apiNm = "product-reco";
		value.categoryCd = categoryCd;
		serviceNm = 'recommand/logging';
		$('#viewDateTbl1').empty(innerHtml);

		var innerHtml = '';
		var idxFrom, idxTo = 0;

		idxFrom = displayCnt * (pageNo - 1);
		idxTo = idxFrom + (displayCnt - 1);
		$("#firstIndex").val(idxFrom + 1);
		$("#lastIndex").val(idxTo + 1);
		if (pageOn == true) {
			apiGetForm();
		}
	}
</script>

<div class="mConBox">
	<div class="mCont">
		<div class="row">
			<div class="col-md-2" id="subtitleA">
			</div>
			<span class="col-md-4" id="subtitleB">
			</span>

			<div class="col-md-6">
				<div class="pageRoute">
				</div>
			</div>
		</div>
		
<!-- 		<div class="row"> -->
<!-- 			<div class="col-md-4" id="subtitleA"> -->
<!-- 			</div> -->
<!-- 			<span class="col-md-4" id="subtitleB"> -->
<!-- 			</span> -->

<!-- 			<div class="col-md-8"> -->
<!-- 				<div class="pageRoute"> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div> -->
		
		<!-- end row -->
		<form:form modelAttribute="areaPreviewParam" id="listForm" method="post" onsubmit="return false"
				onkeydown="javascript:if(event.keyCode==13){fn_search();}">
			<input type="hidden" name="dsplyCnt1" id="dsplyCnt1" value="10" />
			<input type="hidden" name="firstIndex" id="firstIndex" value="1" />
			<input type="hidden" name="lastIndex" id="lastIndex" value="10" />

			<div class="row" id="tableTop1">
				<div class="col-md-12">
					<div class="conBox" id="conBox1" style="min-height: 60px;">
					</div>
				</div>
			</div>

			<div class="row tableTop" id="tableTop2">
				<div class="col-md-12">
					<div class="conBox" style="min-height: 400px;">

						<div class="row">
							<div class="col-md-33">
								<h5>추천 대상 제품</h5>
							</div>
							<div class="col-md-9 select-set-3 text-right">
								<select id="searchOption1" name="searchOption1">
									<option value="CUSTNAME" >고객명</option>
<!-- 									<option value="CUSTINSTTNAME" >기관명</option> -->
                                    <option value="ID" >제품코드</option>
                                    <option value="NAME" >제품명</option>
<!--                                     <option value="CATEGORY" >카테고리 ID</option> -->
                                </select>
								<input type="text" class="searchText" name="searchText" id="searchText1" placeholder="검색어를 입력하세요!">
								<button type="button" class="searchBtn" onclick="fn_search();">
									<i class="ti-search"></i> 검색
								</button>
							</div>
						</div>
						<div class="row">
							<div class="col-md-33 select-set-3">
								<p>
									총 <b id="totalCount">0</b> 리스트
								</p>
							</div>
						</div>

						<div class="row" style="height:680px;width:100%;overflow:auto">
							<div class="col-md-12">							
								<table class="viewDateTbl wide1600"  id="viewDateTbl1" >
								</table>					
							</div>

							<div class="row">
								<div class="col-md-2"></div>
								<div class="row">
									<div id="divPageNo1" class="col-md-12 pageNum"></div>
								</div>
								<div class="col-md-2 select-set-3"></div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</form:form>
	

<!-- row -->

<div class="row tableTop" id="tableTop3" style="display: none;">

	<div class="col-md-12">
		<div class="conBox" style="min-height: 200px;">

			<div class="row">
				<div class="col-md-12">
					<h5>추천 결과 <span id="searchDetailNm"></span></h5>
				</div>
			</div>

			<div class="row">
				<div class="col-md-33 select-set-3">
					<p>
						총 <b id="totalCount2">0</b> 개 검색어
					</p>
				</div>
			</div>
			
			<div class="row" style="height:680px;width:100%;overflow:auto">
				<div class="col-md-12">							
					<table class="viewDateTbl wide2000"  id="viewDateTbl2" >
					</table>					
				</div>
			</div>

		</div>
	</div>

</div>
<!-- row -->
</div>
</div>
<div id="loading2" style="z-index:1002; display:none;">
    <div class="sk-cube-grid" style="z-index:1002">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
    <div class="sk-cube-txt" style="z-index:1002">초록마을</div>    
</div>

<div id="divPopUpA" class="popUp col-md-8">
    <div class="popUpTop">추천MD관리 등록
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form method="POST"  id="addForm">
            	<input type="hidden" id="popUpA-check" name="popUpA-check" />
            	<input type="hidden" id="popUpA-create_id" name="popUpA-create_id" value="${sessionScope.userInfo.userId}"/>
            	<input type="hidden" id="searchWord" name="popUpA-check" /> 
                <table class="popInTbl">
                    <colgroup>
                        <col width="20%"/>
                        <col width="80%"/>
                    </colgroup>
                    <tr>
                    	<th id="sortation">제품 코드</th>
                        <td>
                        	<textarea id="popUpA-content_cd" name="content_cd"  placeholder="제품코드를 입력해주세요." style="min-height:200px"></textarea>
<!--                             <textarea id="popUpA-content_cd" name="content_cd"  placeholder="여러개를 동시에 입력하고 싶은 경우 ',' 를 입력해주세요." style="min-height:200px"></textarea> -->
                        </td>
                    </tr>
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnA-insert" class="btn-style1 darkGrayBtn">저장</button>
            <button type="button" id="btnA-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>