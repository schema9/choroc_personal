<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../common/common.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
var controller = multiApiSet();
var page_resultData;
var itemListCnt;
var pageOn = false;
$(window).ready(function(){
	$('#btn-select').unbind('click').bind('click',function(){
	       apiGetForm();
	   });
});

function multiApiSet(){
	var api_url, api_protocol = location.protocol;
	var api_chkUrl = window.location.href;
	if('' != api_chkUrl) {
		if('http:' == api_protocol) {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
		} else {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
		}
	} else {
		return false;
	}
}

var goPaging_divPageNo1 = function(cPage){
	pageOn = true;
    Paging1(itemListCnt, $("#dsplyCnt1").val(), cPage, "divPageNo1");
};


function apiGetForm(){
	var serviceNm = $('#serviceNm').val();
	var url = controller+'/'+ serviceNm + "/" + $('#apiNm').val();
	var param = {};
	var succMs = "테이블이 조회 되었습니다.";
	var errorMs = "시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.";
	var netErrMs = "네트워크 통신 에러";
	param = {
		'tableNm' : $('#tableNm').val(),
		'itemId' : $('#itemId').val(),
		'categoryCd' : $('#categoryCd').val(),
		'algrthCd':'ah_view_view_catea, ah_view_view_cateb',
		'callDt':$('#callDt').val(),
		'catCd':$('#catCd').val(),
		'kwdType':$('#kwdType').val(),
		'searchText' : $('#searchText').val(),
		'searchOption1' : $('#searchOption1').val(),
		'searchTextInt' : $.trim(parseInt($('#searchTextInt').val())),
		'listOrder' : $('#listOrder').val(),
		'firstIndex' : parseInt($('#firstIndex').val()),
		'lastIndex' : parseInt($('#lastIndex').val())
	};
	fn_isAjaxSet(param, url, succMs, errorMs, netErrMs);
}

function fn_isAjaxSet(param, url, succMs, errorMs, netErrMs) {
	 $.ajax({
        type: 'POST',
        url: url,
        dataType:'JSON',
        data:param,
        async:false,
        success: function(data){
        	if (data.returnErrMsg==""){
        		dataSet=data.list.dataSet;
         	   	totalCnt=data.list.totalCnt;
                
                itemListCnt = totalCnt;
                page_resultData = dataSet;

                if (pageOn == false) Paging1(itemListCnt, $("#dsplyCnt1").val(), 1, "divPageNo1");
           	 	dataListSet(data);
        	} else {
        		$("#getMsg").empty().append(data.returnErrMsg);
        	}
        	
        },
        error : function(e){
        	alert(netErrMs+": "+e);
        }
    });
}

function dataListSet(val){
	var paramHtml = "";
	$("#getMsg").empty();
	$("#getList").empty();
	$("#apiNmView").empty();
	$("#getMsg").append(val.returnErrMsg);
	$("#getList").append("<P style='color:blue'>[조회결과]</P>");
	for(i=0;i<val.list.dataSet.length;i++){
		$('#getList').append(JSON.stringify(val.list.dataSet[i]));
	}
	$("#apiNmView").append($('#apiNm').val());
}
function renderDataByHtml1(displayCnt, pageNo){

    $('#getList').empty(innerHtml);

   var innerHtml = '';
   var idxFrom, idxTo = 0;

   idxFrom = displayCnt * (pageNo-1);
   idxTo = idxFrom + (displayCnt-1);
   $("#firstIndex").val(idxFrom+1);
	if (pageOn == true) apiGetForm();
}
</script>

<div class="mConBox">
    <div class="mCont">
        <div class="row">
            <div class="col-md-4" id="subtitleA">
                <h2><i class="ti-layers"></i></h2>
            </div>

            <div class="col-md-8">
                <div class="pageRoute">
                    <ul>
                        <li><a href="#">HOME</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li class="current"><a href="#">API테스트</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end row -->
  <div class="row">
            <div class="col-md-12">
                <div class="conBox" style="min-height:400px;">
<div class="row">
							</div>
                    <div class="row">        
<form:form action="" method="POST" modelAttribute="apiParam" id="modifyForm">
<p>(	예제: hbdarc / recommand / eoad	)</p>
<p>서비스 이름<input type="text" id="serviceNm" name="serviceNm" value="hbdarc"/></p>
<p>(	예제: category-select / product-reco /product-reco-product-all	/ eoad-naver-kwd-brd)</p>
<p>Api이름<input type="text" id="apiNm" name="apiNm" value="category-select"/></p>
<p>tableNm 인자값<input type="text" id="tableNm" name="tableNm" value="P01"/>(예제: ah_view_view_catea / ah_view_view_cateb )</p>
<p>itemId 인자값<input type="text" id="itemId" name="itemId" value="2072608987"/></p>
<p>categoryCd 인자값<input type="text" id="categoryCd" name="categoryCd" value="RC_VIEW"/></p>
<p>algrthCd 인자값<input type="text" id="algrthCd" name="algrthCd" value="ar_view_view,ar_view_buy,ar_buy_buy,ah_view_view_catea"/></p>
<p>searchText 인자값<input type="text" id="searchText" name="searchText" value="P01"/></p>
<p>searchOption1 인자값<input type="text" id="searchOption1" name="searchOption1" value=""/></p>
<p>searchTextInt 인자값<input type="text" id="searchTextInt" name="searchTextInt" value="0"/></p>
<p>listOrder 인자값<input type="text" id="listOrder" name="listOrder" value="goOrder_idx"/></p>
<p>firstIndex 인자값<input type="text" id="firstIndex" name="firstIndex" value="1"/></p>
<p>lastIndex 인자값<input type="text" id="lastIndex" name="lastIndex" value="10"/></p>
<p>--naver키워드--</p>
<p>callDt 인자값<input type="text" id="callDt" name="callDt" value="20190209050000"/></p>
<p>catCd 인자값<input type="text" id="catCd" name="catCd" value="50000000"/></p>
<p>kwdType 인자값<input type="text" id="kwdType" name="kwdType" value="BRD"/></p>
<input type="hidden" name="dsplyCnt1" id="dsplyCnt1" value="10" />
</form:form>
<div class="row">
								<div class="col-md-33 select-set-3">
<button class="searchBtn" id="btn-select">API호출</button>
</div></div>
<div class="row">
								<div class="col-md-33 select-set-3">
<P>조회한 API 이름 : <span id="apiNmView">${apiNm}</span></P>
</div></div>
<p>============</p>
<p>[에러사항]</p>
<p id="getMsg">없음</p>
<p><br></p>
<p>============</p>
<div class="row">
								<div class="col-md-33 select-set-3">

<p id="getList">${list}</p>
</div></div>
</div></div></div></div>

							</div></div>


