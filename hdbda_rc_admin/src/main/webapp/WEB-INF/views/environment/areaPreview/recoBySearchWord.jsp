<%------------------------------------------------------------------------
- Menu  : 추천환경관리 > 추천영역미리보기 > 검색어별 추천검색어
- ID    : RECO_AP_004
- Ref.  : -
- URI   : -
- History
- 2018-12-10 created by ljy
------------------------------------------------------------------------%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../common/common.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	
<script type="text/javascript">
var controller = multiApiSet();
var serviceNm = '';
var value;
var columnSetRow = 0;
var columnSetRowIndex = 0;
var itemListCnt;
var page_resultData;
var apiNmGlobal;
var pageOn = false;
var categoryCd = "";
var orderBySortDataSet = {'dataSet1Dtp':'','dataSet2Dtp':'','dataSet3Dtp':''};
orderBySortDataSet.dataSet1Dtp = {};
orderBySortDataSet.dataSet2Dtp = {};
orderBySortDataSet.dataSet3Dtp = {};
orderBySortDataSet.dataSet1Dtp.list = {};
orderBySortDataSet.dataSet1Dtp.totalCnt = {};
orderBySortDataSet.dataSet2Dtp.list = {};
orderBySortDataSet.dataSet2Dtp.totalCnt = {};
orderBySortDataSet.dataSet3Dtp.list = {};
orderBySortDataSet.dataSet3Dtp.totalCnt = {};	

$(window).ready(function(){
     pageEvents();
});

function multiApiSet(){
	return "/environment/areaPreview";

	var api_url, api_protocol = location.protocol;
	var api_chkUrl = window.location.href;
	if('' != api_chkUrl) {
		if('http:' == api_protocol) {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
		} else {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
		}
	} else {
		return false;
	}
}

var goPaging_divPageNo1 = function(cPage){
	pageOn = true;
    Paging1(itemListCnt, $("#dsplyCnt1").val(), cPage, "divPageNo1");
};
 function pageEvents(){
	 fn_createCategory();
}
 
 function fn_categoryCdSet_back() {	// common.js
// 		if ($('#category1').val()=='ar_srh_view'){
// 			 categoryCd = 'RC_SEARCH';
// 		 }
// 		 if ($('#category1').val()=='ar_srh_buy'){
// 			 categoryCd = 'RC_BUY';
// 		 }
// 		 if ($('#category1').val()=='ar_srh_srh'){
// 			 categoryCd = 'RC_SEARCH';
// 		 }
	}
 
 function fn_createCategory(){
		value = {
				'apiNm' : 'category-select',
				'algrthCd':'rc_item_search_view,rc_item_search_search,rc_item_search_buy,rc_item_view_search,MD_ITEM,rc_item_view_view,rc_item_view_buy,rc_item_buy_buy',
// 				'algrthCd':'ar_srh_srh',
				'searchText' : '',
				'firstIndex' : parseInt($('#firstIndex').val()),
				'lastIndex' : parseInt($('#lastIndex').val())
			}
		serviceNm = 'recommand';
		apiGetForm();
	}

	function fn_createCategorySelect(dataList,totalCnt) {
		var htmlSet = "";
			htmlSet += '<select name="category" id="category1">';
		for (i=0;i<dataList.length;i++){
			htmlSet += '<option value="'+dataList[i].algrth_id+'">'+dataList[i].algrth_nm+'</option>';	
		}
		htmlSet += '</select>';
		$("#tableTop3").css("display","none");
		$("#conBox1").empty();
		$("#viewDateTbl1").empty();
		$("#viewDateTbl2").empty();
		$("#conBox1").append(htmlSet);
		if (dataList.length == 0) {
			fn_createEmptySelect();
			return;
		}
		$("#category1").on('change', function() {
			 fn_categoryCdSet();
				value = {
					'apiNm' : 'search-word-reco',
					'categoryCd' : categoryCd,
					'searchText' : '',
					'searchOption1' : '',
					'firstIndex' : parseInt($('#firstIndex').val()),
					'lastIndex' : parseInt($('#lastIndex').val())
				}
				$("#searchText1").val("");
				serviceNm = 'recommand/logging';
				apiGetForm();	
		 });
			fn_createInitOn();
	}
 
 function fn_createInitOn(){
	 if (value != {}) {
		 	fn_categoryCdSet();
			value = {
				'apiNm' : 'search-word-reco',
				'categoryCd' : categoryCd,
				'searchText' : '',
				'searchOption1' : '',
				'firstIndex' : parseInt($('#firstIndex').val()),
				'lastIndex' : parseInt($('#lastIndex').val())
			}
			serviceNm = 'recommand/logging';
			apiGetForm();
	}
 }
 
 function searchListDetailAll(val){;
	value = {
				'apiNm' : 'search-word-reco-word-all',
			 	'tableNm' : $('#category1').val(),
				'itemId' : $.trim(val).replace(/ /gi,""),
				'searchText' : '',
				'searchOption1' : '',
				'listOrder' : 0,
				'firstIndex' : 0,
				'lastIndex' : 0
			}
	serviceNm = 'recommand';
    apiGetForm();
 }

	function apiGetForm() {
		var url = controller+'/'+ serviceNm + "/" + value.apiNm + ".do";
		var param = {};
		var succMs = "테이블이 조회 되었습니다.";
		var errorMs = "시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.";
		var netErrMs = "네트워크 통신 에러";
		param = {
			'tableNm' : value.tableNm,
			'itemId' : $.trim(value.itemId).replace(/ /gi,""),
			'categoryCd' : value.categoryCd,
			'algrthCd' : value.algrthCd,
			'searchText' : $.trim(value.searchText),
			'searchOption1' : value.searchOption1,
			'firstIndex' : parseInt($('#firstIndex').val()),
			'lastIndex' : parseInt($('#lastIndex').val())
		};
		fn_isAjaxSet(param, url, succMs, errorMs, netErrMs);
	}

	function fn_isAjaxSet(param, url, succMs, errorMs, netErrMs) {
		setTimeout(function(){
		$.ajax({
			type : 'POST',
			url : url,
			dataType : 'JSON',
			data : param,
			beforeSend:function(){
				maskOn2();
		        $("#loading2").show();
			},
			complete:function(){
			},
			success : function(data) {
				if (data.returnErrMsg == "") {
					dataSet = data.list.dataSet;
					totalCnt = data.list.totalCnt;
					page_resultData = data.list.dataSet;
					apiNmGlobal = data.apiNm;

					if (data.apiNm=="category-select") fn_createCategorySelect(dataSet,totalCnt);
					
					if (data.apiNm == "search-word-reco") {
						if (pageOn == false)
							Paging1(totalCnt, $("#dsplyCnt1").val(), 1, "divPageNo1");
							fn_createSearchList(dataSet, totalCnt);
					}
					
					 if (data.apiNm=="search-word-reco-word-all") {
			           		serviceNm = 'recommand';
					 		orderBySortDataSet.dataSet3Dtp.list = dataSet;
			              	orderBySortDataSet.dataSet3Dtp.totalCnt = totalCnt;
					 		goOrder("goOrder_idx");
		               }	
				} else {
                     alert(errorMs + "(" + data.returnErrMsg + ")");
				}
				$("#loading2").hide();
                maskOff2();
			},
			error : function(e) {
				alert(netErrMs + ": " + e);
				$("#loading2").hide();
                maskOff2();
			}
		});
		}, 1);
	}

	function fn_createEmptySelect() {
		htmlSet +='<colgroup>';
		htmlSet +='<col width="60px">';
		htmlSet +='<col width="">';
		htmlSet +='<col width="20%">';
		htmlSet +='</colgroup>';

		htmlSet +='<tr>';
		htmlSet +='<th>번호</th>';
		htmlSet +='<th>검색어</th>';
		htmlSet +='<th>검색횟수</th>';
		htmlSet +='</tr>';
		htmlSet += '<tr><td colspan="3">조회 결과가 없습니다.</td></tr>';
		$("#viewDateTbl1").empty();
		$("#viewDateTbl1").append(htmlSet);
	}
	
	function fn_createEmptySelect2() {
		var htmlSet = '';
		htmlSet += '<ul>';
		htmlSet += '</ul>';
		$('#totalCount2').empty().text("0");
		$("#viewDateTbl2").empty();
		$("#viewDateTbl2").append(htmlSet);
	}

	function addComma(num) {
	    var regexp = /\B(?=(\d{3})+(?!\d))/g;
	    return num.toString().replace(regexp,',');
	}
	
	function fn_createSearchList(dataList, totalCnt) {
		var htmlSet = "";	
		htmlSet +='<colgroup>';
		htmlSet +='<col width="60px">';
		htmlSet +='<col width="">';
		htmlSet +='<col width="20%">';
		htmlSet +='</colgroup>';

		htmlSet +='<tr>';
		htmlSet +='<th>번호</th>';
		htmlSet +='<th>검색어</th>';
		htmlSet +='<th>검색횟수</th>';
		htmlSet +='</tr>';
		
		for (i=0;i<dataList.length;i++){
			if (dataList[i].search == null || dataList[i].search == undefined) dataList[i].search="";
			num = parseInt($("#firstIndex").val())+parseInt(i);
			htmlSet += '<tr>';
			htmlSet += '<td>'+num+'</td>';
			htmlSet += '<td><a href="#" id="'+dataList[i].search 
					+ '" name="'+ dataList[i].search
					+ '" onClick="fn_checkForm(this.id,this.name);">'
					+ dataList[i].search+'</a></td>';
			htmlSet += '<td>'+addComma(dataList[i].cnt)+'</td>';
			htmlSet += '</tr>';	
		}
		$("#tableTop3").css("display","none");
		$("#viewDateTbl1").empty();
		$("#viewDateTbl2").empty();
		$("#viewDateTbl1").append(htmlSet);
		
		 itemListCnt = totalCnt;
		 $('#totalCount').empty().text(addComma(totalCnt));  
	}

	function goOrder(val) {
		if (val == "goOrder_idx") {
			if (value.listOrder == "id_asc") {
				orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
					return(a.rc_search > b.rc_search) ? - 1 : (a.rc_search < b.rc_search) ? 1 : 0;
				});
				value.listOrder = "id_desc";
			} else {
				//ID순으로 오름차순 정렬
				orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
					return(a.rc_search < b.rc_search) ? - 1 : (a.rc_search > b.rc_search) ? 1 : 0;
				});
				value.listOrder = "id_asc";
			}
		}
		fn_createSearchDetailList(orderBySortDataSet.dataSet3Dtp.list,orderBySortDataSet.dataSet3Dtp.totalCnt);
		
	}
	
	function fn_createSearchDetailList(dataList, totalCnt) {
		var htmlSet = "";
		if (dataList.length == 0) {
			fn_createEmptySelect2();
			$("#tableTop3").css("display", "block");
			return;
		}
		
		
		for (i = 0; i < dataList.length; i++) {
			if(i == 0){
				htmlSet += '<ul>';		
			}else if(i != 0 && i%14 == 0){
				htmlSet += '</ul><ul>';
			}
			if (dataList[i].rc_search == null || dataList[i].rc_search == undefined) dataList[i].rc_search="";
			num = i+1;
			htmlSet += '<li title="' + dataList[i].rc_search + '"><span>'+num+'</span> '+dataList[i].rc_search+'</li>';
		}
 		htmlSet += '</ul>';
		
		$('#totalCount2').empty().text(addComma(totalCnt));
		$("#viewDateTbl2").empty();
		$("#viewDateTbl2").append(htmlSet);
		$("#tableTop3").css("display", "block");
	}
	function fn_checkForm(val,val2) {
		$('#searchDetailNm').empty();
   		$('#searchDetailNm').append(val2);
		searchListDetailAll(val);
	}
	
	function fn_search() {
		var searchCnt = 0;
		if ($("#searchText1").val() == "") {
			//$("#searchOption1").val("");
		} else {
			if($("#searchOption1").val() == "CNT") {
				searchCnt = parseInt($("#searchText1").val());
			}
			if($("#searchOption1").val() == "") {
				$("#searchText1").val("");
			}
		}
		fn_categoryCdSet();
		value = {
			'apiNm' : 'search-word-reco',
			'categoryCd' : categoryCd,
			'searchText' : $("#searchText1").val(),
			'searchOption1' : $("#searchOption1").val(),
			'firstIndex' : 0,
			'lastIndex' : 10
		}
		serviceNm = 'recommand/logging';
		pageOn = false;
		apiGetForm();
	}
	


	function renderDataByHtml1(displayCnt, pageNo) {
		$("#searchDetailNm").empty();
		$('#totalCount2').empty().text("0");
		$("#viewDateTbl2").empty();
		$("#tableTop3").css("display", "none");
		fn_categoryCdSet();
		
		value.apiNm = "search-word-reco";
		value.categoryCd = categoryCd;
		serviceNm = 'recommand/logging';
		$('#viewDateTbl1').empty(innerHtml);

		var innerHtml = '';
		var idxFrom, idxTo = 0;

		idxFrom = displayCnt * (pageNo - 1);
		idxTo = idxFrom + (displayCnt - 1);
		$("#firstIndex").val(idxFrom + 1);
		$("#lastIndex").val(idxTo + 1);
		if (pageOn == true) {
			apiGetForm();
		}
	}

</script>

<div class="mConBox">
	<div class="mCont">
		<div class="row">
			<div class="col-md-4" id="subtitleA">
			</div>

			<div class="col-md-8">
				<div class="pageRoute">
				</div>
			</div>
		</div>
		<!-- end row -->
		<form:form modelAttribute="areaPreviewParam" id="listForm"
			method="post" onsubmit="return false" onkeydown="javascript:if(event.keyCode==13){fn_search();}">
			<input type="hidden" name="dsplyCnt1" id="dsplyCnt1" value="10" />
			<input type="hidden" name="firstIndex" id="firstIndex" value="1" />
			<input type="hidden" name="lastIndex" id="lastIndex" value="10" />

			<div class="row" id="tableTop1">
				<div class="col-md-12">
					<div class="conBox" id="conBox1" style="min-height: 60px;">
					</div>
				</div>
			</div>

			<div class="row tableTop" id="tableTop2">

				<div class="col-md-12">
					<div class="conBox" style="min-height: 400px;">

						<div class="row">
							<div class="col-md-33">
								<h5>추천 대상 리스트</h5>
							</div>
							<div class="col-md-9 select-set-3 text-right">
								<select id="searchOption1" name="searchOption1">
                                    <option value="ID" >검색어</option>
<!--                                     <option value="CNT" >검색횟수</option> -->
                                </select>
								<input type="text" class="searchText"
									name="searchText" id="searchText1" placeholder="검색어를 입력하세요!">
								<button type="button" class="searchBtn" onclick="fn_search();">
									<i class="ti-search"></i> 검색
								</button>
							</div>
						</div>
						<div class="row">
							<div class="col-md-33 select-set-3">
								<p>
									총 <b id="totalCount">0</b> 리스트
								</p>
							</div>
						</div>

						<div class="row">

							<div class="col-md-12">

								<table class="viewDateTbl" id="viewDateTbl1">
									<tr>
										<td colspan=3></td>
									</tr>
								</table>
							</div>
						</div>

						<div class="row">
							<div class="col-md-2"></div>
							<!-- end row -->
							<div class="row">
								<div id="divPageNo1" class="col-md-12 pageNum"></div>
							</div>
							<!-- end row -->

							<div class="col-md-2 select-set-3"></div>
						</div>
					</div>
				</div>
			</div>
		</form:form>
	

<!-- row -->

<div class="row tableTop" id="tableTop3" style="display: none;">

	<div class="col-md-12">
		<div class="conBox" style="min-height: 200px;">

			<div class="row">
				<div class="col-md-12">
					<h5>추천 결과 <span id="searchDetailNm"></span></h5>
				</div>
			</div>

			<div class="row">
				<div class="col-md-33 select-set-3">
					<p>
						총 <b id="totalCount2">0</b> 개 검색어
					</p>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="recoSearchword" id="viewDateTbl2">
					</div>
				</div>
			</div>

		</div>
	</div>

</div>
<!-- row -->
</div>
</div>
<div id="loading2" style="z-index:1002; display:none;">
    <div class="sk-cube-grid" style="z-index:1002">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
    <div class="sk-cube-txt" style="z-index:1002">초록마을</div>
</div>