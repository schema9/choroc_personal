<%------------------------------------------------------------------------
- Menu  : 추천환경관리 > 추천영역미리보기 > 제품별 추천제품
- ID    : RECO_AP_002
- Ref.  : -
- URI   : -
- History
- 2018-12-10 created by ljy
------------------------------------------------------------------------%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../common/common.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	
<script type="text/javascript">
var controller = multiApiSet();
var serviceNm = '';
var value;
var columnSetRow = 0;
var columnSetRowIndex = 0;
var itemListCnt;
var page_resultData;
var apiNmGlobal;
var pageOn = false;
var categoryCd = "";
var orderBySortDataSet = {'dataSet1Dtp':'','dataSet2Dtp':'','dataSet3Dtp':''};
orderBySortDataSet.dataSet1Dtp = {};
orderBySortDataSet.dataSet2Dtp = {};
orderBySortDataSet.dataSet3Dtp = {};
orderBySortDataSet.dataSet1Dtp.list = {};
orderBySortDataSet.dataSet1Dtp.totalCnt = {};
orderBySortDataSet.dataSet2Dtp.list = {};
orderBySortDataSet.dataSet2Dtp.totalCnt = {};
orderBySortDataSet.dataSet3Dtp.list = {};
orderBySortDataSet.dataSet3Dtp.totalCnt = {};	

$(window).ready(function(){
     pageEvents();
});

function multiApiSet(){
	return "/environment/areaPreview";

	var api_url, api_protocol = location.protocol;
	var api_chkUrl = window.location.href;
	if('' != api_chkUrl) {
		if('http:' == api_protocol) {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
		} else {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
		}
	} else {
		return false;
	}
}

var goPaging_divPageNo1 = function(cPage){
	pageOn = true;
    Paging1(itemListCnt, $("#dsplyCnt1").val(), cPage, "divPageNo1");
};
 function pageEvents(){
	fn_createCategory();
 }
 
function fn_categoryCdSet_back() {	// common.js
// 	if ($('#category1').val()=='ar_view_view'){
// 		 categoryCd = 'RC_VIEW';
// 	 }
// 	 if ($('#category1').val()=='ar_view_buy'){
// 		 categoryCd = 'RC_VIEW';
// 	 }
// 	 if ($('#category1').val()=='ar_buy_buy'){
// 		 categoryCd = 'RC_BUY';
// 	 }
// 	 if ($('#category1').val()=='ar_view_srh'){
// 		 categoryCd = 'RC_VIEW';
// 	 }
}
	 
function fn_createCategory(){
	value = {
			'apiNm' : 'category-select',
			'algrthCd':'rc_item_search_view,rc_item_search_search,rc_item_search_buy,rc_item_view_search,MD_ITEM,rc_item_view_view,rc_item_view_buy,rc_item_buy_buy',
// 			'algrthCd':'ar_view_view,ar_view_buy,ar_buy_buy,ar_view_srh',
			'searchText' : '',
// 			'searchTextInt' : 0,
			'firstIndex' : parseInt($('#firstIndex').val()),
			'lastIndex' : parseInt($('#lastIndex').val())
		}
	serviceNm = 'recommand';
	apiGetForm();
}

function fn_createCategorySelect(dataList,totalCnt) {
	var htmlSet = "";
	htmlSet += '<select name="category" id="category1">';	
	if(dataList != null){
		for (i=0;i<dataList.length;i++){
			htmlSet += '<option value="'+dataList[i].algrth_id+'">'+dataList[i].algrth_nm+'</option>';	
		}		
	}
	htmlSet += '</select>';
	
	$("#tableTop3").css("display","none");
	$("#conBox1").empty();
	$("#viewDateTbl1").empty();
	$("#viewDateTbl2").empty();
	$("#conBox1").append(htmlSet);
	if (dataList.length == 0) {
		fn_createEmptySelect();
		return;
	}
	$("#category1").on('change', function() {
		 fn_categoryCdSet();
			value = {
				'apiNm' : 'product-reco',
				'categoryCd' : categoryCd,
				'searchText' : '',
				'searchOption1' : '',
// 				'searchTextInt' : 0,
				'firstIndex' : parseInt($('#firstIndex').val()),
				'lastIndex' : parseInt($('#lastIndex').val())
			}
			$("#searchText1").val("");
			serviceNm = 'recommand/logging';
			apiGetForm();	
	 });
		fn_createInitOn();
}
 
 function fn_createInitOn(){
	 if (value != {}) {
		 fn_categoryCdSet();
			value = {
				'apiNm' : 'product-reco',
				'categoryCd' : categoryCd,
				'searchText' : '',
				'searchOption1' : '',
// 				'searchTextInt' : 0,
				'firstIndex' : parseInt($('#firstIndex').val()),
				'lastIndex' : parseInt($('#lastIndex').val())
			}
			serviceNm = 'recommand/logging';
			apiGetForm();
	}
 }
 
 function searchListDetailAll(val,val2){
	 value = {
			'apiNm' : 'product-reco-product-all',
		 	'tableNm' : $('#category1').val(),
			'itemId' : val,
			'categoryCd': val2,
			'searchText' : '',
			'searchOption1' : '',
// 			'searchTextInt' : 0,
			'listOrder' : 0,
			'firstIndex' : 0,
			'lastIndex' : 0
	 }
	 serviceNm = 'recommand';
    apiGetForm();
 }

	function apiGetForm() {
		var url = controller +'/'+ serviceNm + "/" + value.apiNm + ".do";
		var param = {};
		var succMs = "테이블이 조회 되었습니다.";
		var errorMs = "시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.";
		var netErrMs = "네트워크 통신 에러";
		param = {
			'tableNm' : value.tableNm,
			'itemId' : value.itemId,
			'categoryCd' : value.categoryCd,
			'algrthCd' : value.algrthCd,
			'searchText' : $.trim(value.searchText),
			'searchOption1' : value.searchOption1,
			'firstIndex' : parseInt($('#firstIndex').val()),
			'lastIndex' : parseInt($('#lastIndex').val())
		};
		fn_isAjaxSet(param, url, succMs, errorMs, netErrMs);
	}

	function fn_isAjaxSet(param, url, succMs, errorMs, netErrMs) {
		setTimeout(function(){
		$.ajax({
			type : 'POST',
			url : url,
			dataType : 'JSON',
			data : param,
			beforeSend:function(){
				maskOn2();
		        $("#loading2").show();
			},
			complete:function(){
			},
			success : function(data) {
				if (data.returnErrMsg == "") {
					dataSet = data.list.dataSet;
					totalCnt = data.list.totalCnt;
					page_resultData = data.list.dataSet;
					apiNmGlobal = data.apiNm;
					
		         	if (data.apiNm=="category-select") fn_createCategorySelect(dataSet,totalCnt);
					
					if (data.apiNm == "product-reco") {
						orderBySortDataSet.dataSet2Dtp.list = dataSet;
	              		orderBySortDataSet.dataSet2Dtp.totalCnt = totalCnt;
						if (pageOn == false)
							Paging1(totalCnt, $("#dsplyCnt1").val(), 1, "divPageNo1");
							fn_createSearchList(dataSet, totalCnt);
					}
					
					 if (data.apiNm=="product-reco-product-all") {
			           		orderBySortDataSet.dataSet3Dtp.list = dataSet;
			              	orderBySortDataSet.dataSet3Dtp.totalCnt = totalCnt;
			              	goOrder("goOrder_idx");
		               }
				} else {
                     alert(errorMs + "(" + data.returnErrMsg + ")");
				}
				$("#loading2").hide();
                maskOff2();
			},
			error : function(e) {
				alert(netErrMs + ": " + e);
				$("#loading2").hide();
                maskOff2();
			}
		});
		}, 1);
	}

	function fn_createEmptySelect() {
		var htmlSet = '';
		htmlSet += '<colgroup>';
		htmlSet += '<col width="">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="">';
		htmlSet += '<col width="">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
		htmlSet += '</colgroup>';

		htmlSet += '<tr>';
		htmlSet += '<th -rowspan="2">제품코드</th>';
		htmlSet += '<th -rowspan="2">제품이미지</th>';
		htmlSet += '<th -rowspan="2">제품명</th>';
		htmlSet += '<th -rowspan="2">판매자명</th>';
// 		htmlSet += '<th colspan="4">카테고리 ID</th>';
		htmlSet += '</tr>';
// 		htmlSet += '<tr>';
// 		htmlSet += '<th>대</th>';
// 		htmlSet += '<th>중</th>';
// 		htmlSet += '<th>소</th>';
// 		htmlSet += '<th>세</th>';
// 		htmlSet += '</tr>';
		htmlSet += '<tr><td colspan="8">조회 결과가 없습니다.</td></tr>';
		$("#viewDateTbl1").empty();
		$("#viewDateTbl1").append(htmlSet);
	}
	
	function fn_createEmptySelect2() {
		var htmlSet = '';
		htmlSet += '<colgroup>';
		htmlSet += '<col width="">';
		htmlSet += '<col width="">';
		htmlSet += '<col width="120px">';
		htmlSet += '<col width="">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
		htmlSet += '</colgroup>';

		htmlSet += '<tr>';
		htmlSet += '<th -rowspan="2">제품코드</th>';
		htmlSet += '<th -rowspan="2">순위</th>';
		htmlSet += '<th -rowspan="2">제품이미지</th>';
		htmlSet += '<th -rowspan="2">제품명</th>';
// 		htmlSet += '<th colspan="4">카테고리 ID</th>';
		htmlSet += '</tr>';
// 		htmlSet += '<tr>';
// 		htmlSet += '<th>대</th>';
// 		htmlSet += '<th>중</th>';
// 		htmlSet += '<th>소</th>';
// 		htmlSet += '<th>세</th>';
// 		htmlSet += '</tr>';
		htmlSet += '<tr><td colspan="8">조회 결과가 없습니다.</td></tr>';
		$('#totalCount2').empty().text("0");
		$("#viewDateTbl2").empty();
		$("#viewDateTbl2").append(htmlSet);
	}
	
	function addComma(num) {
	    var regexp = /\B(?=(\d{3})+(?!\d))/g;
	    return num.toString().replace(regexp,',');
	}

	function fn_createSearchList(dataList, totalCnt) {
		var htmlSet = "";
		htmlSet += '<colgroup>';
		htmlSet += '<col width="">';
		htmlSet += '<col width="120px">';
		htmlSet += '<col width="">';
		htmlSet += '<col width="">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
		htmlSet += '</colgroup>';
		
		htmlSet += '<tr>';
		htmlSet += '<th -rowspan="2">제품코드</th>';
		htmlSet += '<th -rowspan="2">제품이미지</th>';
		htmlSet += '<th -rowspan="2">제품명</th>';
		htmlSet += '<th -rowspan="2">판매자명</th>';
// 		htmlSet += '<th colspan="4">카테고리 ID</th>';
		htmlSet += '</tr>';
// 		htmlSet += '<tr>';
// 		htmlSet += '<th>대</th>';
// 		htmlSet += '<th>중</th>';
// 		htmlSet += '<th>소</th>';
// 		htmlSet += '<th>세</th>';
// 		htmlSet += '</tr>';
		for (i = 0; i < dataList.length; i++) {
			if (dataList[i].item_name == null || dataList[i].item_name == undefined) dataList[i].item_name="";
			if (dataList[i].catea1_cd == null || dataList[i].catea1_cd == undefined) dataList[i].catea1_cd="";
			if (dataList[i].catea2_cd == null || dataList[i].catea2_cd == undefined) dataList[i].catea2_cd="";
			if (dataList[i].catea3_cd == null || dataList[i].catea3_cd == undefined) dataList[i].catea3_cd="";
			if (dataList[i].catea4_cd == null || dataList[i].catea4_cd == undefined) dataList[i].catea4_cd="";
			htmlSet += '<tr>';
			htmlSet += '<td>' + dataList[i].item_id + '</td>';
			htmlSet += '<td><img src="'+dataList[i].thumb+'"></td>';
			htmlSet += '<td>';
			htmlSet +='<b><a href="#" id="'+dataList[i].item_id+"|"+ dataList[i].catea1_cd
			+ '" name="'+ dataList[i].item_name
			+ '" onClick="fn_checkForm(this.id,this.name);">'+dataList[i].item_name+'</a></b>';
			htmlSet += '</td>';
			htmlSet += '<td>' + addComma(dataList[i].cnt) + '</td>';
// 			htmlSet += '<td>' + dataList[i].catea1_cd + '</td>';
// 			htmlSet += '<td>' + dataList[i].catea2_cd + '</td>';
// 			htmlSet += '<td>' + dataList[i].catea3_cd + '</td>';
// 			htmlSet += '<td>' + dataList[i].catea4_cd + '</td>';
			htmlSet += '</tr>';
		}
		$("#tableTop3").css("display", "none");
		$("#viewDateTbl1").empty();
		$("#viewDateTbl2").empty();
		$("#viewDateTbl1").append(htmlSet);
		$('#goOrderSearch_idx').unbind('click').bind('click', function() {
			value.apiNm = "product-reco";
			orderChange();
			goOrder(this.id);
		});
		itemListCnt = totalCnt;
		$('#totalCount').empty().text(addComma(totalCnt));
	}

	function orderChange(){
		if (value.listOrder == "id_asc") {
			value.listOrder = "id_desc";
		} else {
			value.listOrder = "id_asc";
		}
	}

	
	function fn_createSearchDetailList(dataList, totalCnt) {
		var htmlSet = "";
		htmlSet += '<colgroup>';
		htmlSet += '<col width="">';
		htmlSet += '<col width="">';
		htmlSet += '<col width="80px">';
		htmlSet += '<col width="">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
// 		htmlSet += '<col width="7%">';
		htmlSet += '</colgroup>';

		htmlSet += '<tr>';
		htmlSet += '	<th -rowspan="2">제품 ID</th>';
		htmlSet += '	<th -rowspan="2">순위 </th>';
		htmlSet += '	<th -rowspan="2">제품이미지</th>';
		htmlSet += '	<th -rowspan="2">제품명</th>';
// 		htmlSet += '	<th colspan="4">카테고리 ID</th>';
		htmlSet += '</tr>';
// 		htmlSet += '<tr>';
// 		htmlSet += '	<th>대</th>';
// 		htmlSet += '	<th>중</th>';
// 		htmlSet += '	<th>소</th>';
// 		htmlSet += '	<th>세</th>';
// 		htmlSet += '</tr>';
		if (dataList.length == 0) {
			fn_createEmptySelect2();
			$("#tableTop3").css("display", "block");
			return;
		}
		for (i = 0; i < dataList.length; i++) {
			if (dataList[i].item_name == null || dataList[i].item_name == undefined) dataList[i].item_name="";
			if (dataList[i].cate_1_cd == null || dataList[i].cate_1_cd == undefined) dataList[i].cate_1_cd="";
			if (dataList[i].cate_2_cd == null || dataList[i].cate_2_cd == undefined) dataList[i].cate_2_cd="";
			if (dataList[i].cate_3_cd == null || dataList[i].cate_3_cd == undefined) dataList[i].cate_3_cd="";
			if (dataList[i].cate_4_cd == null || dataList[i].cate_4_cd == undefined) dataList[i].cate_4_cd="";
			htmlSet += '<tr>';
			htmlSet += '<td>' + dataList[i].rc_item_cd + '</td>';
			htmlSet += '<td>' + dataList[i].rc_item_rank + '</td>';
			htmlSet += '<td><img src="'+dataList[i].thumb_url+'"></td>';
			htmlSet += '<td class="txtL">';
			htmlSet += '<b>'
					+ dataList[i].item_name + '</b>';
			htmlSet += '</td>';
// 			htmlSet += '<td>' + dataList[i].cate_1_cd + '</td>';
// 			htmlSet += '<td>' + dataList[i].cate_2_cd + '</td>';
// 			htmlSet += '<td>' + dataList[i].cate_3_cd + '</td>';
// 			htmlSet += '<td>' + dataList[i].cate_4_cd + '</td>';
			htmlSet += '</tr>';
		}
		$('#totalCount2').empty().text(addComma(totalCnt));

		$("#viewDateTbl2").empty();
		$("#viewDateTbl2").append(htmlSet);
		$("#tableTop3").css("display", "block");
	}
	
	
	function fn_createSearchDetailList2(dataList, totalCnt) {
		var htmlSet = "";
		if (dataList.length == 0) {
			fn_createEmptySelect3();
			$("#tableTop3").css("display", "block");
			return;
		}
		
		for (i = 0; i < dataList.length; i++) {
			if(i == 0){
				htmlSet += '<ul>';		
			}else if(i != 0 && i%14 == 0){
				htmlSet += '</ul><ul>';
			}
			if (dataList[i].rc_search == null || dataList[i].rc_search == undefined) dataList[i].rc_search="";
			num = i+1;
			htmlSet += '<li title="' + dataList[i].rc_search + '"><span>'+num+'</span> '+dataList[i].rc_search+'</li>';
		}
			htmlSet += '</ul>';
		
		$('#totalCount2').empty().text(addComma(totalCnt));
		$("#viewDateTbl2").empty();
		$("#viewDateTbl2").append(htmlSet);
		$("#viewDateTbl2").addClass("recoSearchword");
		$("#tableTop3").css("display", "block");
	}
	
	function fn_checkForm(val,val2) {
		var str = val;
    	var strArr = str.split('|');
    	var id = strArr[0];
    	var categoryCd = strArr[1];
		$('#searchDetailNm').empty();
   		$('#searchDetailNm').append(val2);
		searchListDetailAll(id,categoryCd);
	}

	function fn_search() {
		var searchCnt = 0;
		if ($("#searchText1").val() == "") {
			//$("#searchOption1").val("");
		}
		if($("#searchOption1").val() == "") {
			$("#searchText1").val("");
		}
		fn_categoryCdSet();
		 
		value = {
			'apiNm' : 'product-reco',
			'categoryCd'  : categoryCd,
			'searchText' : $("#searchText1").val(),
			'searchOption1' : $("#searchOption1").val(),
			'firstIndex' :0,
			'lastIndex' : 10
		}
		serviceNm = 'recommand/logging';
		pageOn = false;
		apiGetForm();
	}

	function goOrder(val) {
		if ($('#category1').val() != 'ar_view_srh'){
			if (val == "goOrderSearch_idx") {
				if (value.listOrder == "" || value.listOrder == undefined) {
					value.listOrder = "id_desc";
				}
				//ID순으로 오름차순 정렬
				if (value.listOrder == "id_asc") {
					orderBySortDataSet.dataSet2Dtp.list.sort(function(a, b) {
						return (a.item_id < b.item_id) ? -1
								: (a.item_id > b.item_id) ? 1 : 0;
					});
					value.listOrder = "id_asc";
				} else {
					orderBySortDataSet.dataSet2Dtp.list.sort(function(a, b) {
						return (a.item_id > b.item_id) ? -1
								: (a.item_id < b.item_id) ? 1 : 0;
					});
					value.listOrder = "id_desc";
				}
				 fn_createSearchList(orderBySortDataSet.dataSet2Dtp.list, orderBySortDataSet.dataSet2Dtp.totalCnt);
				return;
			}
			if (val == "goOrder_id") {
				if (value.listOrder2 == "id_asc") {
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
						return(a.rc_item_cd > b.rc_item_cd) ? - 1 : (a.rc_item_cd < b.rc_item_cd) ? 1 : 0;
					});
					value.listOrder2 = "id_desc";
				} else {
					//ID순으로 오름차순 정렬
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
						return(a.rc_item_cd < b.rc_item_cd) ? - 1 : (a.rc_item_cd > b.rc_item_cd) ? 1 : 0;
					});
					value.listOrder2 = "id_asc";
				}
			}
			if (val == "goOrder_idx") {
				if (value.listOrder2 == "id_asc") {
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
						return(a.rc_item_rank > b.rc_item_rank) ? - 1 : (a.rc_item_rank < b.rc_item_rank) ? 1 : 0;
					});
					value.listOrder2 = "id_desc";
				} else {
					//ID순으로 오름차순 정렬
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a,b){
						return(a.rc_item_rank < b.rc_item_rank) ? - 1 : (a.rc_item_rank > b.rc_item_rank) ? 1 : 0;
					});
					value.listOrder2 = "id_asc";
				}
			}
			fn_createSearchDetailList(orderBySortDataSet.dataSet3Dtp.list,orderBySortDataSet.dataSet3Dtp.totalCnt);
		} else {
			if (val == "goOrder_idx") {
				if (value.listOrder2 == "id_asc") {
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a, b) {
						return (a.rc_search < b.rc_search) ? -1
								: (a.rc_search > b.rc_search) ? 1 : 0;
					});
					value.listOrder2 = "id_asc";
				} else {
					//ID순으로 오름차순 정렬
					orderBySortDataSet.dataSet3Dtp.list.sort(function(a, b) {
						return (a.rc_search < b.rc_search) ? -1
								: (a.rc_search > b.rc_search) ? 1 : 0;
					});
					value.listOrder2 = "id_desc";
				}
			}
			fn_createSearchDetailList2(orderBySortDataSet.dataSet3Dtp.list,orderBySortDataSet.dataSet3Dtp.totalCnt);	
		}
		
	}

	function renderDataByHtml1(displayCnt, pageNo) {
		$("#searchDetailNm").empty();
		$('#totalCount2').empty().text("0");
		$("#viewDateTbl2").empty();
		$("#tableTop3").css("display", "none");
		fn_categoryCdSet();
		 
		value.apiNm = "product-reco";
		value.categoryCd = categoryCd;
		serviceNm = 'recommand/logging';
		$('#viewDateTbl1').empty(innerHtml);

		var innerHtml = '';
		var idxFrom, idxTo = 0;

		idxFrom = displayCnt * (pageNo - 1);
		idxTo = idxFrom + (displayCnt - 1);
		$("#firstIndex").val(idxFrom + 1);
		$("#lastIndex").val(idxTo + 1);
		if (pageOn == true) {
			apiGetForm();
		}
	}
</script>

<div class="mConBox">
	<div class="mCont">
		<div class="row">
			<div class="col-md-4" id="subtitleA">
			</div>

			<div class="col-md-8">
				<div class="pageRoute">
				</div>
			</div>
		</div>
		<!-- end row -->
		<form:form modelAttribute="areaPreviewParam" id="listForm"
			method="post" onsubmit="return false" onkeydown="javascript:if(event.keyCode==13){fn_search();}">
			<input type="hidden" name="dsplyCnt1" id="dsplyCnt1" value="10" />
			<input type="hidden" name="firstIndex" id="firstIndex" value="1" />
			<input type="hidden" name="lastIndex" id="lastIndex" value="10" />

			<div class="row" id="tableTop1">
				<div class="col-md-12">
					<div class="conBox" id="conBox1" style="min-height: 60px;">
					</div>
				</div>
			</div>

			<div class="row tableTop" id="tableTop2">

				<div class="col-md-12">
					<div class="conBox" style="min-height: 400px;">

						<div class="row">
							<div class="col-md-33">
								<h5>추천 대상 제품</h5>
							</div>
							<div class="col-md-9 select-set-3 text-right">
								<select id="searchOption1" name="searchOption1">
                                    <option value="ID" >제품코드</option>
                                    <option value="NAME" >제품명</option>
<!--                                     <option value="CATEGORY" >카테고리 ID</option> -->
                                </select>
								<input type="text" class="searchText"
									name="searchText" id="searchText1" placeholder="검색어를 입력하세요!">
								<button type="button" class="searchBtn" onclick="fn_search();">
									<i class="ti-search"></i> 검색
								</button>
							</div>
						</div>
						<div class="row">
							<div class="col-md-33 select-set-3">
								<p>
									총 <b id="totalCount">0</b> 리스트
								</p>
							</div>
						</div>

						<div class="row">

							<div class="col-md-12">

								<table class="viewDateTbl" id="viewDateTbl1">
								</table>
							</div>
						</div>

						<div class="row">
							<div class="col-md-2"></div>
							<!-- end row -->
							<div class="row">
								<div id="divPageNo1" class="col-md-12 pageNum"></div>
							</div>
							<!-- end row -->

							<div class="col-md-2 select-set-3"></div>
						</div>
					</div>
				</div>
			</div>
		</form:form>
	

<!-- row -->

<div class="row tableTop" id="tableTop3" style="display: none;">

	<div class="col-md-12">
		<div class="conBox" style="min-height: 200px;">

			<div class="row">
				<div class="col-md-12">
					<h5>추천 결과 <span id="searchDetailNm"></span></h5>
				</div>
			</div>

			<div class="row">
				<div class="col-md-33 select-set-3">
					<p>
						총 <b id="totalCount2">0</b> 개 검색어
					</p>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<table class="viewDateTbl" id="viewDateTbl2">
					</table>
				</div>
			</div>

		</div>
	</div>

</div>
<!-- row -->
</div>
</div>
<div id="loading2" style="z-index:1002; display:none;">
    <div class="sk-cube-grid" style="z-index:1002">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
    <div class="sk-cube-txt" style="z-index:1002">초록마을</div>
</div>