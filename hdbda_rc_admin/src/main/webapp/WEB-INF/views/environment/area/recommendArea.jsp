<%------------------------------------------------------------------------
 * TODO > 추천영역 관리(조회 , 등록 , 수정)
 * 파일명   > recommendArea.jsp
 * 작성일   > 2018. 12. 06.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 2018. 12. 12
 * 수정자   > 이동환 사원
------------------------------------------------------------------------%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../common/common.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<script type="text/javascript">
	var totalCount=0;
	var resultList;
	var controller = multiApiSet();
	
	$(window).ready(function(){
	    pageEvents();
	    listCountAjax(" ", " ");
	    initFunction();
	});
	
	function multiApiSet(){
		return "/environment/algorithm";
// 		return "/environment/area";
		
		var api_url, api_protocol = location.protocol;
		var api_chkUrl = window.location.href;
		if('' != api_chkUrl) {
			if('http:' == api_protocol) {
				return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
			} else {
				return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
			}
		} else {
			return false;
		}
	}
	
	function listCountAjax(searchOptionNm, searchText){
		var searchText = $('#inpSearch').val();
		var searchOptionNm = $('#searchOptionNm').val();
		if(searchText == null){
	    	searchText = " ";
	    }
		if(searchOptionNm == null){
			searchOptionNm = " ";
		}
		
		var url = controller
				+ '/recommand/manager/rc-area-count.do'
				+ '?searchOptionNm=' + searchOptionNm
				+ '&searchText=' + searchText
				;
		
		$.ajax({
			type: "POST",
			url: url,
			async:false,
			success: function(resultData){
				totalCount = resultData.totalCount;
				$('#listTotalCount').text(totalCount);
	 			Paging1(totalCount, $("#dsplyCnt1").val(), 1, "divPageNo1");
	 			if(totalCount == 0){
	 				var html="";
	 				html += '<tr><td colspan="8" style="text-align:center">No data available in table</td>';
					$('#pv_itemView_tbody1').children().remove();
	                $('#pv_itemView_tbody1').append(html);
	 			}
			},
			error : function(request, status, error){
				alert('서버와의 통신에 실패하였습니다.');
			}
		});
	}
	
	// 페이지 처리 된 리스트 가져오기
	function renderDataByHtml1(dsplyCnt, dsplyNo){
	    var searchOptionNm = $('#searchOptionNm').val();
	    var searchText = $('#inpSearch').val();
	    
	    if(searchOptionNm == null){
	    	searchOptionNm = " ";
	    }
	    if(searchText ==null){
	    	searchText = " ";
	    }
	    
		var url = controller 
				+ '/recommand/manager/rc-area-list.do'
				+ '?searchOptionNm=' + searchOptionNm 
				+ '&searchText=' + searchText 
				+ '&dsplyCnt=' + dsplyCnt 
				+ '&dsplyNo=' + dsplyNo
				;
	    
	    $.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        contentType: "application/json",
	        success:function(resultData)
	        {       	
	        	var html='';
	        	resultList = resultData.resultList;
	        
	        	if(resultList && resultList.length > 0){
					for(var key in resultList){
						var itemInfo = resultList[key];
						html += '<tr>';
			            html += '	<td>' + itemInfo['pge_area_id'] + '</td>';
			            html += '	<td class="txtL">' + itemInfo['pge_area_nm'] + '</td>';
			            html += '	<td class="txtL">' + itemInfo['pge_area_path'] + '</td>';
// 			            html += '	<td>' + itemInfo['input_param'] + '</td>';
			            html += '	<td class="txtL">' + itemInfo['algrth_nm'] + '</td>';
			            if(itemInfo['use_yn'] == 'Y'){
			            	html += '  <td>활성화</td>';
			            }else{
			            	html += '  <td>비활성화</td>';
			            }
			            html += '  <td>' + itemInfo['create_dt'] + '</td>';
			            html += '<td><button type="button" class="tblInBtn btnModifyRow" onclick="fn_update(this, \''+itemInfo['algrth_id']+'\')" id='+ itemInfo['pge_area_id'] +'>수정</button></td>'
			            html += '</tr>';
					}
					$('#pv_itemView_tbody1').children().remove();
	                $('#pv_itemView_tbody1').append(html);
				}else{
					html += '<tr><td colspan="8" style="text-align:center">No data available in table</td>';
					$('#pv_itemView_tbody1').children().remove();
	                $('#pv_itemView_tbody1').append(html);
				}
	        },
	        error:function(e)
	        {
	            alert('서버와의 통신에 실패하였습니다.');
	        }
	    });
	}
	
	function initFunction() 
	{
	    // 10건 20건 콤보상자'
	    $('#dsplyCnt1').change(function(){
	        Paging1(totalCount, parseInt($(this).val()), 1, "divPageNo1");
	    });	
	}

	function pageEvents(){
	
	    // 리스트 - 추천영역 등록 버튼클릭
	    $('#btnPopUpAdd').unbind('click').bind('click',function(){ 
	    	$('#popUpBtn').empty();
	    	$("#crudFlag").val("C");

	        var url = controller 
	        		+ "/recommand/manager/rc-area-insert-algo.do";
	        
	        $.ajax({
	            url:url,
	            crossDomain:true,
	            type:'POST',
	            async:false,
	            success:function(result){
	            	 for(var i=0; i<result.result.length; i++){
	            		var nm = result.result[i].algrth_nm;
	            		var id = result.result[i].algrth_id;
	            		$('#pge_area_id').val(result.pkMax);
	            		var option = $("<option value="+ id+">" + nm +"</option>");
	            		$('#getAlgrthmList').append(option);
	            	 }	
	                 if(result != null){
	                    $("#pgeRelmNm").val("");
	                    $("#pgeRelmPath").val("");
	                    $("#inputParam").val("");
	                    $("#useNm").text(" ON");
	                    $("#crudFlag").val("C");
	                    var checkBox = document.getElementById("checkUseYn");
	                    checkBox.checked=true;
	                    $(".popUpTop").text("추천영역 등록");
	                }
	            },
	            error:function(e){
	                alert('서버와의 통신에 실패하였습니다.');
	            }
	        });
	        
	        // 등록 팝업창 오픈
	        fn_util_divPopUpControl('divPopUpA',true);
	    });
	        
	 	// 추천영역 등록팝업 - 저장 버튼 
	    $('#btnA-insert').unbind('click').bind('click',function(){
	        fn_add();
	    });
	 	
	 	// 검색버튼 클릭 이벤트 [시작페이지"1",글목록수"10"셋팅]
	    $('#btnSearch').unbind('click').bind('click',function(){
	    	var searchText = $('#inpSearch').val();
	    	var searchOptionNm = $('#searchOptionNm').val();
	    	listCountAjax(searchOptionNm, searchText);
	    });
	}

	function fn_search() {
		var searchText = $('#inpSearch').val();
    	var searchOptionNm = $('#searchOptionNm').val();
    	listCountAjax(searchOptionNm, searchText);
	}
	
	// 사용여부 체크 메서드
	function checkFn() {
	    var value = $('#checkUseYn').is(':checked');
	    if (value) {
	        $('#useNm').text(" ON");
	    } else {
	        $('#useNm').text(" OFF");
	    }
	}
	
	// 등록팝업 - 저장 버튼
	function fn_add(){
		if('' == $("#pgeRelmNm").val()) {
	        alert('추쳔영역을 입력해 주세요');
	        $("#pgeRelmNm").focus();
	        return;
	    }
	
	    if('' == $("#pgeRelmPath").val()) {
	        alert('설명을 입력해 주세요');
	        $("#pgeRelmPath").focus();
	        return;
	    }
	
	    // 활성화 여부
	    var checkUseYn = $('#checkUseYn').is(':checked');
	    if(checkUseYn) {
	        $("#useYn").val("Y");
	    } else {
	        $("#useYn").val("N");
	    }
	    
	    // 입력, 수정 공통
	    var pge_area_nm = $('#pgeRelmNm').val();
    	var pge_area_path = $('#pgeRelmPath').val();
    	var algrth_id = $('#getAlgrthmList').val();
    	var use_yn = $("#useYn").val();
    	var input_param = $("#inputParam").val();
	    
	    // 등록 & 수정 구분
	    if ($("#crudFlag").val() == "C") {
	    	var pge_area_id = $('#pge_area_id').val();
	    	var create_id = $('#popUpA-create_id').val();
	    	
	    	var url = controller
	    			+ '/recommand/manager/rc-area-insert.do'
	    			+ '?pge_area_id=' + pge_area_id
	    			+ '&pge_area_nm=' + encodeURI(pge_area_nm)
	    			+ '&pge_area_path=' + encodeURI(pge_area_path)
	    			+ '&input_param=' + input_param
	    			+ '&algrth_id=' + algrth_id
	    			+ '&use_yn=' + use_yn
	    			+ '&create_id=' + create_id
	    			;
	    
	    } else {
	    	var pge_area_id = $('#U_pge_area_id').val();
	    	var modify_id = $('#popUpA-create_id').val();
	    	
	  		var url = controller
	  				+ '/recommand/manager/rc-area-update.do'
	  				+ '?pge_area_id=' + pge_area_id
	  				+ '&pge_area_nm=' + encodeURI(pge_area_nm) 
	  				+ '&pge_area_path=' + encodeURI(pge_area_path)
	  				+ '&input_param=' + input_param
	  				+ '&algrth_id=' + algrth_id
	  				+ '&use_yn=' + use_yn
	  				+ '&modify_id=' + modify_id
	  				;
	  
	    }
		
	    if (confirm("저장 하시겠습니까?")) {
	        $.ajax({
	            type: 'POST',
	            url: url,
	            crossDomain:true,
	            async:false,
	            success:function(resultData){
	                if(resultData != null){
	                    if(0 != resultData.result) {
	                        alert('처리되었습니다.');
	                    } else {
	                        alert('관리자에게 문의 바랍니다.');
	                    }
// 	                    location.reload(true);
	                    location.href = "/environment/area/recommendArea.do";
	                }
	            },
	            error:function(e){
	                alert('서버와의 통신에 실패하였습니다.');
	            }
	        });
	    }
	}

	// 리스트 - 상세관리 - 수정 버튼
	function fn_update(tag,clickId){
		var pge_area_id = tag.id;
		$(".popUpTop").text("추천영역 수정");
		$("#crudFlag").val("U");
	
		var url = controller
				+ "/recommand/manager/rc-area-update-algo.do"
				+ "?pge_area_id=" + pge_area_id
				;
		
		var html='';
		html = '<button type="button" id="btnA-delete" class="btn-style1 darkGrayBtn" onclick="fn_delete()">삭제</button>'
		$('#popUpBtn').empty();
		$('#popUpBtn').append(html);		
		
			$.ajax({
				type : 'POST',
				url : url,
				crossDomain : true,
				async : false,
				success : function(result) {
					if(result != null){
						// 알고리즘 select 태그 생성
		            	for(var i=0; i<result.resultList.length; i++){
		            		var nm = result.resultList[i].algrth_nm;
		            		var id = result.resultList[i].algrth_id;
		            		
		            		if(clickId == result.resultList[i].algrth_id){
		            			option = $("<option selected value="+ id+">"+ nm +"</option>");
		            		}else{
		            			option = $("<option value="+ id+">"+ nm +"</option>");
		            		}
		            		$('#getAlgrthmList').append(option);
		            	 }
					}
					// DB값 태그에 입력
					$('#pgeRelmNm').val(result.result.pge_area_nm);
					$('#U_pge_area_id').val(result.result.pge_area_id);
					$('#pgeRelmPath').val(result.result.pge_area_path);
					$('#useYn').val(result.result.use_yn);
					$('#inputParam').val(result.result.input_param);
					
					var checkBox = document.getElementById("checkUseYn");
                    
					if($('#useYn').val() == "Y"){
						checkBox.checked=true;	
					}else{
						checkBox.checked=false;
					}
					// 활성화 여부 체크 메서드 호출
					checkFn();
				},
				error : function(e) {
					alert('서버와의 통신에 실패하였습니다.');
				}
			});
		// 팝업창 열기
		fn_util_divPopUpControl('divPopUpA',true);
	}
	
function fn_delete(){
	if(confirm("삭제 하시겠습니까?")){
		var pge_area_id = $('#U_pge_area_id').val();
		var modify_id = $('#popUpA-create_id').val();
		
		var url = controller
				+ "/recommand/manager/rc-area-delete.do" 
				+ "?pge_area_id=" + pge_area_id
				+ "&modify_id=" + modify_id
				;
		
			$.ajax({
				type : 'POST',
				url : url,
				crossDomain : true,
				async : false,
				success : function(resultData) {
					if(resultData != null){
	                    if(0 != resultData.result) {
	                        alert('처리되었습니다.');
	                    } else {
	                        alert('관리자에게 문의 바랍니다.');
	                    }
	                    location.reload(true);
	                }
				},
				error : function(e) {
					alert('서버와의 통신에 실패하였습니다.');
				}
			});
		}
	}		
	
	function btnCancel(){
		// 팝업 데이터 초기화
		$('#popUpBtn').empty();
		fn_util_divPopUpControl('divPopUpA',false);
		$('#pge_area_id').val("");
		$('#useYn').val("N");
		$('#crudFlag').val("");
		$('#pgeRelmNm').val("");
		$('#pgeRelmPath').val("");
		$('#inputParam').val("");
		$("#checkUseYn").attr("checked", false);
		checkFn();
		
		// 팝업 Select 태그 초기화
		var selectTag = document.getElementById("getAlgrthmList");
		for(var i=0; i<selectTag.options.length; i++){
			selectTag.options[i] = null;
		}
		selectTag.options.length = 0;
	}
	
	function EnterKey(){
		if(window.event.keyCode == 13){
			fn_add();
		}
		if(window.event.keyCode == 27){
			fn_util_divPopUpControl('divPopUpA', false); // 팝업창 닫기
	        inicialize();
		}
	}

	var goPaging_divPageNo1 = function(cPage)
	{
	    Paging1(totalCount, $("#dsplyCnt1").val(), cPage, "divPageNo1");
	};
	
</script>


<div class="mConBox">
	<div class="mCont">
		<div class="row">
			<div class="col-md-4" id="subtitleA">
				<h2>
					<i class="ti-layers"></i>
				</h2>
			</div>

			<div class="col-md-8">
				<div class="pageRoute">
					<ul>
						<li><a href="#">HOME</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li><a href="#">추천환경 관리</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li class="current"><a href="#">추천영역</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end row -->

		<form  id="listForm"
			method="get" onsubmit="return false" onkeydown="javascript:if(event.keyCode==13){fn_search();}">
			<div class="row">
				<div class="col-md-12">
					<div class="conBox" style="min-height: 400px;">

						<div class="row">
							<div class="col-md-33 select-set-3">
							<p>
									총	<b id="listTotalCount"></b> 개
							</div>
							<div class="col-md-9 text-right select-sys-1">
								<select path="dsplyCnt1"  id="dsplyCnt1" >
									<option value="10">10개씩 보기</option>
									<option value="20">20개씩 보기</option>
								</select>
								<select id="searchOptionNm" path="searchOptionNm">
									<option value="page">NO</option>
<!-- 									<option value="page">페이지 영역</option> -->
									<option value="algo">추천 화면명</option>
								</select>
								<input path="searchText" type="text" id="inpSearch"
									class="searchTxt" placeholder="검색어를 입력하세요" />
								<button type="button" id="btnSearch" class="searchBtn">
									<i class="ti-search"></i> 검색
								</button>
								<button type="button" title="사용자 메뉴 권한을 추가할 수 있습니다."
									id="btnPopUpAdd" class="AddDefaultBtn" name="추천영역 등록">
									<i class="fa fa-plus"></i> 추천영역 등록
								</button>
							</div>
						</div>


						<!-- end row > conBox > col-md-12 > row > form -->
						<div class="row">
							<div class="col-md-12">
								<table class="viewDateTbl" >
									<colgroup>
										<col width="100">
										<col width="350">
										<col width="350">
<%-- 										<col width="180"> --%>
										<col width="350">
										<col width="100">
										<col width="120">
										<col width="100">
									</colgroup>
									<thead>
										<tr>
											<th>NO</th>
<!-- 											<th>페이지<br>영역</th> -->
											<th>추천화면명</th>
											<th>화면위치</th>
<!-- 											<th>input데이터</th> -->
											<th>알고리즘</th>
											<th>상태</th>
											<th>등록일</th>
											<th>상세관리</th>
										</tr>
									</thead>
									<tbody id="pv_itemView_tbody1"></tbody>
								</table>
							</div>
						</div>

						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8 pageNum">
								<!-- Paging With Taglib -->
								<div id="divPageNo1" class="col-md-12 pageNum"></div>
							</div>
							<div class="col-md-2 select-set-3"></div>
						</div>
						
					</div>
				</div>
			</div>
		</form>
	</div>
</div>





<div id="divPopUpA" class="popUp col-md-8">
	<div class="popUpTop">
		<span class="closePop"><i class="fa fa-times"
			aria-hidden="true"></i></span>
	</div>
	
	<div class="popUpMain">
		<div class="popUpCon">
			
			<form method="POST" id="addForm">
				<input type="hidden" name="pge_area_id" id="pge_area_id" value="" />
				<input type="hidden" name="U_pge_area_id" id="U_pge_area_id" value="" />
				<input type="hidden" name="use_yn" id="useYn" />
				<input type="hidden" name="crudFlag" id="crudFlag" value="" />
				<input type="hidden" id="popUpA-create_id" name="popUpA-create_id" value="${sessionScope.userInfo.userId}"/>
				
				<table class="popInTbl">
					<colgroup>
						<col width="30%" />
						<col width="70%" />
					</colgroup>
					<tr>
						<th><label for="pgeRelmNm">추천영역명</label></th>
						<td><input type="text" id="pgeRelmNm" name="pge_area_nm"
							class="w100p" value="" maxlength="100" onkeyup="EnterKey()" /></td>
					</tr>
					<tr>
						<th><label for="pgeRelmPath">화면위치</label></th>
						<td><textarea id="pgeRelmPath" name="pge_area_path"
							class="w100p" value="" maxlength="500"  ></textarea></td>
					</tr>
					<tr style="display:none;">
						<th><label for="inputParam">input데이터</label></th>
						<td><textarea id="inputParam" name="input_param"
							class="w100p" value="" maxlength="500"  ></textarea></td>
					</tr>
					<tr>
						<th><label for="">추천알고리즘</label></th>
						<td>
							<table class="popInTblInner">
								<colgroup>
									<col width="30%" />
									<col width="55%" />
									<col width="15%" />
								</colgroup>
								<!-- 알고리즘 리스트 -->
							</table> <select id="getAlgrthmList" name="algrth_id"
							style="width: 700px;">
						</select>
						</td>
					</tr>
					<tr>
						<th><label for="">상태</label></th>
						<td><input type="checkbox" class="rdChk" id="checkUseYn"
							name="checkUseYn" onclick="checkFn();"> <label
							class="chkSel pullLeft" for="checkUseYn"><span id="useNm">ON</span></label></td>
					</tr>
				</table>
			</form>
			
		</div>
		<div class="popUpBtn">
			<span id="popUpBtn"></span>
			<button type="button" id="btnA-insert" class="btn-style1 darkGrayBtn">저장</button>
			<button type="button" id="btnA-cancel"
				class="btn-style1 lightGrayBtn popCancel" onclick="btnCancel()">취소</button>
		</div>
	</div>
</div>










