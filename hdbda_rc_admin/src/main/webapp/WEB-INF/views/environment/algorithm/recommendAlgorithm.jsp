<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/common.jsp"%>
<% request.setCharacterEncoding("utf-8"); %>

<script type="text/javascript">

var totalCount=0;
var resultList;
var controller = multiApiSet();

$(window).ready(function(){
    pageEvents();
    listCountAjax(" ", " ");
    initFunction();
});

function multiApiSet(){
	return "/environment/algorithm";
	
	
	
	var api_url, api_protocol = location.protocol;
	var api_chkUrl = window.location.href;
	if('' != api_chkUrl) {
		if('http:' == api_protocol) {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
		} else {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
		}
	} else {
		return false;
	}
}

// 리스트 총 갯수 가져오기 및 페이지 처리 함수 호출
function listCountAjax(searchOptionNm, searchText){
	var searchText = $('#inpSearch').val();
	var searchOptionNm = $('#searchOptionNm').val();
	if(searchText == null){
		searchText = " ";
    }
	if(searchOptionNm == null){
		searchOptionNm = " ";
	}
	
	var url = controller
			+ '/recommand/manager/rc-algorithm-count.do'
			+ '?searchOptionNm=' + searchOptionNm
			+ '&searchText=' + searchText
			;
	
	$.ajax({
		type: "POST",
		url: url,
		async:false,
// 		contentType: "application/json",
		success: function(resultData){
			if('' != resultData.returnErrMsg) {
				alert('에러가 발생하였습니다. message : '+resultData.returnErrMsg);
			} else {
				totalCount = resultData.totalCount;
				$('#listTotalCount').text(totalCount);
	 			Paging1(totalCount, $("#dsplyCnt1").val(), 1, "divPageNo1");
	 			if(totalCount == 0){
	 				var html="";
	 				html += '<tr><td colspan="7" style="text-align:center">No data available in table</td>';
					$('#pv_itemView_tbody1').children().remove();
	                $('#pv_itemView_tbody1').append(html);
	 			}
			}
		},
		error : function(request, status, error){
			alert('서버와의 통신에 실패하였습니다.');
		}
	});
}

// 페이지 처리 된 리스트 가져오기
function renderDataByHtml1(dsplyCnt, dsplyNo){
    var searchOptionNm = $('#searchOptionNm').val();
    var searchText = $('#inpSearch').val();
    
    if(searchOptionNm == null){
    	searchOptionNm = "P";
    }
    if(searchText ==null){
    	searchText = " ";
    }
   
    var url = controller
    		+ '/recommand/manager/rc-algorithm-list.do'
    		+ '?searchOptionNm=' + searchOptionNm 
    		+ '&searchText=' + searchText 
    		+ '&dsplyCnt=' + dsplyCnt 
    		+ '&dsplyNo=' + dsplyNo
    		;
    
    $.ajax({
        type: "POST",
        url: url,
        async:false,
//         contentType: "application/json",
        success:function(resultData)
        {       	
        	var html='';
        	if('' != resultData.returnErrMsg) {
        		html += '<tr><td colspan="7" style="text-align:center">No data available in table</td>';
				$('#pv_itemView_tbody1').children().remove();
                $('#pv_itemView_tbody1').append(html);
				alert('에러가 발생하였습니다. message : '+resultData.returnErrMsg);
			} else {
	        	resultList = resultData.resultList;
	        	
	        	if(resultList && resultList.length > 0){
					for(var key in resultList){
						var itemInfo = resultList[key];
						if (itemInfo['algrth_id'] == null) itemInfo['algrth_id'] = "";
						if (itemInfo['algrth_nm'] == null) itemInfo['algrth_nm'] = "";
						if (itemInfo['algrth_dc'] == null) itemInfo['algrth_dc'] = "";
						if (itemInfo['create_dt'] == null) itemInfo['create_dt'] = "";
						if (itemInfo['modify_dt'] == null) itemInfo['modify_dt'] = "";
						html += '<tr>';
			            html += '	<td class="txtL">' + itemInfo['algrth_id'] + '</td>';
			            html += '	<td class="txtL">' + itemInfo['algrth_nm'] + '</td>';
			            html += '	<td class="txtL">' + itemInfo['algrth_dc'] + '</td>';
			            html += '	<td>' + itemInfo['create_dt'] + '</td>';
			            html += '	<td>' + itemInfo['modify_dt'] + '</td>';
			            html += '<td><button type="button" class="tblInBtn btnModifyRow" onclick="modifyAlgorithm(this)" id="'+ itemInfo['algrth_id'] +'">수정</button></td>'
			            html += '</tr>';
					}
					$('#pv_itemView_tbody1').children().remove();
	                $('#pv_itemView_tbody1').append(html);
				}else{
					html += '<tr><td colspan="7" style="text-align:center">No data available in table</td>';
					$('#pv_itemView_tbody1').children().remove();
	                $('#pv_itemView_tbody1').append(html);
				}
			}
        },
        error:function(e)
        {
            alert('서버와의 통신에 실패하였습니다.');
        }
    });
}

function initFunction() 
{
    // 10건 20건 콤보상자'
    $('#dsplyCnt1').change(function(){
        Paging1(totalCount, parseInt($(this).val()), 1, "divPageNo1");
    });	
}


function pageEvents(){
    // close & cancel buttons
    $('#btnM-cancel, #btnA-cancel, .closePop i').unbind('click').bind('click',function(){
        fn_util_divPopUpControl('divPopUpA',false);
        fn_util_divPopUpControl('divPopUpM',false);
        inicialize();
    });

    // 사용자메뉴 권한 추가등록
    $('#btnPopUpAdd').unbind('click').bind('click',function(){
        fn_util_divPopUpControl('divPopUpA',true); // 팝업창 보이기       
        $('#tblPopUpA').find('input:checkbox').prop('disabled',true); 
    });

    // 리스트 - 수정 - 저장
    $('#btnM-update').unbind('click').bind('click',function(){
        fn_modify( $(this).attr('id').replace('btnM-','') );
    });

    // 리스트 - 추천영역 등록 - 저장
    $('#btnA-insert').unbind('click').bind('click',function(){
        fn_add();
    });
    
 	// 검색버튼 클릭 이벤트 [시작페이지"1",글목록수"10"셋팅]
    $('#btnSearch').unbind('click').bind('click',function(){
    	var searchText = $('#inpSearch').val();
    	var searchOptionNm = $('#searchOptionNm').val();
    	listCountAjax(searchOptionNm, searchText);
    });
 	
   
}

function fn_search() {
	var searchText = $('#inpSearch').val();
	var searchOptionNm = $('#searchOptionNm').val();
	listCountAjax(searchOptionNm, searchText);
}

	// 추천알고리즘 추가등록팝업 - 중복체크
	function id_check_insert() {
		var algrth_id = $('#popUpA-algrth_id').val();
		var algrth_nm = $('#popUpA-algrth_nm').val();
		
		var url = controller
				+ '/recommand/manager/rc-algorithm-pk-check.do'
				+ '?algrth_id=' + algrth_id
				+ '&algrth_nm='+algrth_nm
				;
		
		if ($('#popUpA-algrth_id').val() == "") {
			alert('추천알고리즘을 입력하세요');
			$('#popUpA-algrth_id').focus();
		} else {
			$.ajax({
				type : 'POST',
				url : url,
// 				contentType : 'application/json; charset=utf-8',
				crossDomain : true,
				async : true,
				success : function(resultData) {
					if (resultData != null) {
						alert(resultData.result);
						if (resultData.result == "사용 가능한 추천 알고리즘 입니다.") {
							$('#popUpA-check').val(algrth_id);
						}
					}
				},
				error : function(e) {
					alert('서버와의 통신에 실패하였습니다.');
				}
			});
		}
	}

	// 추천알고리즘 수정팝업 - 중복체크
	function id_check_update() {
		var algrth_nm = $('#popUpM-algrth_nm').val();
		
		var url = controller
				+ '/recommand/manager/rc-algorithm-nm-check.do'
				+ '?algrth_nm='+algrth_nm
				;
				
		$.ajax({
			type : 'POST',
			url : url,
// 			contentType : 'application/json; charset=utf-8',
			crossDomain : true,
			async : true,
			success : function(resultData) {
				if (resultData != null) {
					alert(resultData.result);
					if (resultData.result == "사용 가능한 추천 알고리즘 명입니다.") {
						$('#popUpM-check').val(algrth_nm);
					}
				}
			},
			error : function(e) {
				alert('서버와의 통신에 실패하였습니다.');
			}
		});
	}

	// 리스트 - 수정 버튼
	function modifyAlgorithm(tag) {
		var old_id = tag.id;
		var algrth_id = old_id;
		
		var url = controller
				+ '/recommand/manager/rc-algorithm-selectOne.do'
				+ '?algrth_id=' + algrth_id
				;
		
		var html='';
		html = '<button type="button" id="btnA-delete" class="btn-style1 darkGrayBtn" onclick="fn_delete()">삭제</button>'
		$('#popUpBtn').empty();
		$('#popUpBtn').append(html)

		$.ajax({
			type : 'POST',
			url : url,
			crossDomain : true,
			async : false,
			success : function(resultData) {
				if (resultData != null) {
					$('#popUpM-algrth_id').val(resultData.result.algrth_id);
					$('#popUpM-algrth_nm').val(resultData.result.algrth_nm);
					$('#popUpM-algrth_dc').val(resultData.result.algrth_dc);
				}
			},
			error : function(e) {
				alert('서버와의 통신에 실패하였습니다.');
			}
		});

		$('#popUpM-old_id').val(old_id);
		fn_util_divPopUpControl('divPopUpM', true);
		$('#popUpM-algrth_id').focus();
	}
	
	function fn_delete(){
		if(confirm("삭제 하시겠습니까?")){
			var algrth_id = $('#popUpM-algrth_id').val();
			var modify_id = $('#popUpA-create_id').val();
			
			var url = controller
					+ "/recommand/manager/rc_algorithm-delete.do"
					+ "?algrth_id=" + algrth_id 
					+ "&modify_id=" + modify_id
					;
			
				$.ajax({
					type : 'POST',
					url : url,
					crossDomain : true,
					async : false,
					success : function(resultData) {
						if(resultData != null){
		                    if(0 != resultData.result) {
		                        alert('처리되었습니다.');
		                    } else {
		                        alert('관리자에게 문의 바랍니다.');
		                    }
		                    location.reload(true);
		                }
					},
					error : function(e) {
						alert('서버와의 통신에 실패하였습니다.');
					}
				});
			}
		}

	// 수정팝업 - 저장
	function fn_modify(act) {
		var algrth_id = $('#popUpM-algrth_id').val();
		var algrth_nm = $('#popUpM-algrth_nm').val();
		var algrth_dc = $('#popUpM-algrth_dc').val();
		var old_id = $('#popUpM-old_id').val();
		var modify_id = $('#popUpA-create_id').val();
		
		var url = controller
				+ '/recommand/manager/rc-algorithm-update.do'
				+ '?algrth_id=' + algrth_id
				+ '&algrth_nm=' + algrth_nm
				+ '&algrth_dc=' + algrth_dc
				+ '&modify_id=' + modify_id
				;

		if (fn_isValid_update()) {
			if (confirm("수정 하시겠습니까?")) {
				$.ajax({
					type : 'POST',
					url : url,
					crossDomain : true,
					async : false,
					success : function(resultData) {
						if (resultData != null) {
							if (0 != resultData.result) {
								alert('처리되었습니다.');
								fn_util_divPopUpControl('divPopUpA', false); // 팝업창 닫기 
							} else {
								alert('관리자에게 문의 바랍니다.');
							}
								location.reload(true);
							}
						},
						error : function(e) {
							alert('서버와의 통신에 실패하였습니다.');
						}
					});
				}
			}
		}

	// 추천알고리즘 추가등록 팝업 - 저장
	function fn_add() {
		$('#popUpBtn').empty();
		var isValid = true;
		var algrth_id = $('#popUpA-algrth_id').val();
		var algrth_nm = $('#popUpA-algrth_nm').val();
		var algrth_dc = $('#popUpA-algrth_dc').val();
		var create_id = $('#popUpA-create_id').val();
		
		var url = controller
				+ '/recommand/manager/rc-algorithm-insert.do'
				+ '?algrth_id=' + algrth_id
				+ '&algrth_nm=' + algrth_nm
				+ '&algrth_dc=' + algrth_dc
				+ '&create_id=' + create_id
				;

		if (fn_isValid_insert()) {
			if ($('#popUpA-check').val() != algrth_id) {
				alert("먼저 중복확인을 확인해주세요.");
			} else {
				if (confirm("등록 하시겠습니까?")) {
					$.ajax({
								type : 'POST',
								url : url,
								crossDomain : true,
								async : true,
// 								contentType:'application/json; charset=utf-8',
								success : function(resultData) {
									if (resultData != null) {
										if (0 != resultData.result) {
											alert('처리되었습니다.');
											fn_util_divPopUpControl('divPopUpA', false); // 팝업창 닫기 
										} else {
											alert('관리자에게 문의 바랍니다.');
										}
										location.reload(true);
									}
								},
								error : function(e) {
									alert('서버와의 통신에 실패하였습니다.');
								}
							});
				}
			}
		}
	}

	// 추가 등록팝업 예외처리
	function fn_isValid_insert() {
		var isValid = false;
		if ($('#popUpA-algrth_id').val() == "") {
			alert('추천알고리즘을 입력하세요');
			$('#popUpA-algrth_id').focus();
		} else if ($('#popUpA-algrth_nm').val() == "") {
			alert('추천알고리즘명을 입력하세요');
			$('#popUpA-algrth_nm').focus();
		} else if ($('#popUpA-algrth_dc').val() == "") {
			alert('추천알고리즘 설명을 입력하세요');
			$('#popUpA-algrth_dc').focus();
		} else {
			isValid = true;
		}
		return isValid;
	}

	// 수정팝업 예외처리
	function fn_isValid_update() {
		var isValid = false;
		if ($('#popUpM-algrth_id').val() == "") {
			alert('추천알고리즘을 입력하세요');
			$('#popUpA-algrth_id').focus();
		} else if ($('#popUpM-algrth_nm').val() == "") {
			alert('추천알고리즘명을 입력하세요');
			$('#popUpA-algrth_nm').focus();
		} else if ($('#popUpM-algrth_dc').val() == "") {
			alert('추천알고리즘 설명을 입력하세요');
			$('#popUpA-algrth_dc').focus();
		} else {
			isValid = true;
		}
		return isValid;
	}

	// input Text 초기화
	function inicialize() {
		$('#popUpA-algrth_id').val("");
		$('#popUpA-algrth_nm').val("");
		$('#popUpA-algrth_dc').val("");
		$('#popUpM-algrth_id').val("");
		$('#popUpM-algrth_nm').val("");
		$('#popUpM-algrth_dc').val("");
	}

	function linkPage(pageNo) {
		$("#currentPageNo").val(pageNo);
		$("#listForm").attr({
			action : "recommendAlgorithmList.do",
			method : "post"
		}).submit();
	}
	
	function addEnterKey(){
			if(window.event.keyCode == 13){
				fn_add();
			}
			if(window.event.keyCode == 27){
				fn_util_divPopUpControl('divPopUpA', false); // 팝업창 닫기
		        inicialize();
			}
	}
	
	function updateEnterKey(){
		if(window.event.keyCode == 13){
			fn_modify();
		}
		if(window.event.keyCode == 27){
			fn_util_divPopUpControl('divPopUpM', false); // 팝업창 닫기
	        inicialize();
		}
	}
	
	var goPaging_divPageNo1 = function(cPage)
	{
	    Paging1(totalCount, $("#dsplyCnt1").val(), cPage, "divPageNo1");
	};
</script>



<div class="mConBox">
    <div class="mCont">
        <div class="row">
            <div class="col-md-4" id="subtitleA">
                <h2><i class="ti-layers"></i> 추천 알고리즘 관리</h2>
            </div>

            <div class="col-md-8">
                <div class="pageRoute">
                    <ul>
                        <li><a href="#">HOME</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li><a href="#">추천환경관리</a></li>
                        <li><i class="ti-angle-right"></i></li>
                        <li class="current"><a href="#">추천 알고리즘</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <form  id="listForm" method="get" onsubmit="return false" onkeydown="javascript:if(event.keyCode==13){fn_search();}">
            <div class="row">
                <div class="col-md-12">
                    <div class="conBox" style="min-height:400px;">

                        <div class="row">
                            <div class="col-md-33 select-set-3">
                            <p>
                                총	<b id="listTotalCount"></b> 개
                            </div>
                            <div class="col-md-9 text-right select-sys-1">
                                <select path="dsplyCnt1"  id="dsplyCnt1">
                                    <option value="10" >10개씩 보기</option>
                                    <option value="20" >20개씩 보기</option>
                                </select>
                                <select id="searchOptionNm" path="searchOptionNm">
									<option value="id">추천 알고리즘</option>
									<option value="nm">추천 알고리즘 명</option>
								</select>
                                <input path="searchText" type="text" id="inpSearch" class="searchTxt" placeholder="검색어를 입력하세요" value=""/>
                                <button type="button" id="btnSearch" class="searchBtn">
                                    <i class="ti-search"></i> 검색
                                </button>
                                <button type="button" title="추천알고리즘을 추가할 수 있습니다." id="btnPopUpAdd" class="AddDefaultBtn">
                                    <i class="fa fa-plus" ></i> 추천알고리즘 추가등록
                                </button>
                            </div>
                        </div>
                        <!-- end row > conBox > col-md-12 > row > form -->

						<div class="row">
							<div class="col-md-12">
								<table class="viewDateTbl" >
									<colgroup>
										<col width="300">
										<col width="300">
										<col width="600">
										<col width="170">
										<col width="170">
										<col width="100">
									</colgroup>
									<thead>
										<tr>
											<th>추천 알고리즘</th>
											<th>추천 알고리즘 명</th>
											<th>설명</th>
											<th>등록일</th>
											<th>수정일</th>
											<th>상세관리</th>
										</tr>
									</thead>
									<tbody id="pv_itemView_tbody1"></tbody>
								</table>
							</div>
						</div>
						<!-- end row -->

						  <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8 pageNum">
                                <!-- Paging With Taglib -->
                                <div id="divPageNo1" class="col-md-12 pageNum"></div>
                            </div>
                            <div class="col-md-2 select-set-3"></div>
                        </div> 
                        <!-- end row -->
                    </div>
                    <!-- end conBox -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </form>
    </div>
    <!-- end mCont -->
</div>
<!-- end mConBox -->


<!-- 추천 알고리즘 수정-->
<div id="divPopUpM" class="popUp col-md-8" >
    <div class="popUpTop">추천 알고리즘 수정
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form method="POST"  id="modifyForm" autocomplete="off">
                <input type="hidden" id="popUpM-old_id"	name="old_id" value=""/>

                <table id="tblPopUpM" class="popInTbl">
                    <colgroup>
                        <col width="20%"/>
                        <col width="30%" />
                        <col width="20%" />
                        <col width="30%" />
                    </colgroup>
                    <tr>
                        <th><label for="popUpM-algrth_id">알고리즘</label></th>
                        <td colspan="3">
                            <input type="text" id="popUpM-algrth_id" class="w70p" name="algrth_id" autocomplete="off" value="" 
                            	maxlength="30" onkeyup="updateEnterKey()" readonly />
                            <!--                             <button type="button" id="btnA-check" onclick="id_check_update()" style="float:right;"><i class="ti-plus" ></i> 중복확인</button> -->
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-algrth_nm">알고리즘 명</label></th>
                        <td colspan="3">
                            <input type="text" id="popUpM-algrth_nm" class="w70p" name="algrth_nm" autocomplete="off" value=""
                            	 maxlength="100" onkeyup="updateEnterKey()" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpM-algrth_dc">설명</label></th>
                        <td colspan="3">
                            <textarea id="popUpM-algrth_dc" class="w100p" name="algrth_dc" autocomplete="off" value=""
                            	 maxlength="500" style="min-height:200px" >
                            </textarea>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="popUpBtn">
        <span id="popUpBtn"></span>
            <button type="button" id="btnM-update" class="btn-style1 darkGrayBtn">저장</button>
            <button type="button" id="btnM-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>

<!--추천알고리즘 추가등록-->
<div id="divPopUpA" class="popUp col-md-8"  >
    <div class="popUpTop">추천알고리즘 추가등록
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form method="POST"  id="addForm">
                <input type="hidden" id="popUpA-check" name="popUpA-check" />
                <input type="hidden" id="popUpA-create_id" name="popUpA-create_id" value="${sessionScope.userInfo.userId}"/>

                <table id="tblPopUpA" class="popInTbl">
                    <tr>
                        <th><label for="popUpA-algrthNm">추천알고리즘</label></th>
                        <td colspan="3">
                            <input type="text" id="popUpA-algrth_id" class="w70p" name="algrth_id" placeholder="추천 알고리즘을 입력하세요" 
                            	autocomplete="off" value="" maxlength="500" onkeyup="addEnterKey()" />
                            <button type="button" id="btnA-check" onclick="id_check_insert()" style="float:right;"><i class="ti-plus" ></i> 중복확인</button>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-algrthId">추천알고리즘 명</label></th>
                        <td colspan="3">
                            <input type="text" id="popUpA-algrth_nm" class="w70p" name="algrth_nm" placeholder="추천 알고리즘 명을 입력하세요"
                            	 autocomplete="off" value="" maxlength="100" onkeyup="addEnterKey()"/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="popUpA-algrth_dc">설명</label></th>
                        <td colspan="3">
                        	<div style="min-height:200px";>
								<textarea id="popUpA-algrth_dc" name="algrth_dc" placeholder="추천 알고리즘 설명을 입력하세요" 
								autocomplete="off"  value="" maxlength="500" style="min-height:200px"></textarea>
                        	</div>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
	        <div class="popUpBtn">
	            <button type="button" id="btnA-insert" class="btn-style1 darkGrayBtn" >저장</button>
	            <button type="button" id="btnA-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
	        </div>
    </div>
</div>
