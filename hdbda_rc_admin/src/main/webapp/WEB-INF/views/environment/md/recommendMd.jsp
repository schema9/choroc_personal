<%------------------------------------------------------------------------
 * TODO > 추천MD관리 관리(조회 , 등록 , 삭제)
 * 파일명   > recommendException.jsp
 * 작성일   > 2018. 12. 06.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 2018. 12. 12
 * 수정자   > 이동환 사원  
------------------------------------------------------------------------%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../common/common.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<script type="text/javascript">
var controller = multiApiSet();
var totalCount = 0;
var resultList;
var pageOn = false;

$(document).ready(function() {
    pageEvents();
    listAjax($('#searchOptionNm').val(), '', '10', '1'); // 제품또는 키워드 , 검색어, 10개씩 보기 , 1페이지
});

function multiApiSet(){
	return "/environment/algorithm";
	
	var api_url, api_protocol = location.protocol;
	var api_chkUrl = window.location.href;
	if('' != api_chkUrl) {
		if('http:' == api_protocol) {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.servers')" />';
		} else {
			return api_url = '<spring:eval expression="@environment.getProperty('ebro.api.serversHttps')" />';
		}
	} else {
		return false;
	}
}


// 리스트 전체 갯수 카운트 및 페이징 처리
function listAjax(searchOptionNm, searchText, dsplyCnt, dsplyNo){

	var url = controller 
			+ '/recommand/manager/rc-except-list.do'
			+ '?searchOptionNm=' + searchOptionNm 
			+ '&searchText=' + searchText
			;
	
	if(dsplyNo == null){
		var dsplyNo = $("#cPage2").val();
	}
	if(dsplyCnt == null){
		var dsplyCnt = $('#dsplyCnt1').val();
	}
	
	$('#searchWord').val(searchText);
	var searchText = $('#inpSearch').val();
	
	$.ajax({
		type: "POST",
		url: url,
		async:false,
// 		contentType: "application/json",
		success: function(resultData){
			totalCount = resultData.totalCount;
			$('#listTotalCount').text(totalCount);

			var url2 = controller
					 +'/recommand/manager/rc-except-list.do'
					 + '?searchOptionNm=' + searchOptionNm 
					 + '&searchText=' + searchText 
					 + '&dsplyCnt=' + dsplyCnt 
					 + '&dsplyNo=' + dsplyNo
					 ;
			
			ajaxSet(url2);
			if (pageOn == false){
 				Paging1(totalCount, $("#dsplyCnt1").val(), 1, "divPageNo1");
	 			if(totalCount == 0){
	 				var html="";
	 				html += '<tr><td colspan="7" style="text-align:center">No data available in table</td>';
					$('#pv_itemView_tbody1').children().remove();
	                $('#pv_itemView_tbody1').append(html);
	 			}
			}
			var html ='';
		},
		error : function(request, status, error){
			alert('서버와의 통신에 실패하였습니다.');
		}
	});
}

// 페이징 처리 후 리스트 불러오기
function renderDataByHtml1(dsplyCnt, dsplyNo){
	
    var searchOptionNm = $('#searchOptionNm').val();
    if(searchOptionNm == null){
    	searchOptionNm = "P";
    }
   $('#inpSearch').val($('#searchWord').val());
    var searchText = $('#inpSearch').val();
    
   	var url = controller
   			+ '/recommand/manager/rc-except-list.do'
   		 	+ '?searchOptionNm=' + searchOptionNm 
   		 	+ '&searchText=' + searchText 
   		 	+ '&dsplyCnt=' + dsplyCnt 
   		 	+ '&dsplyNo=' + dsplyNo
   		 	;
   
    if (pageOn == true){
    	ajaxSet(url);
    }
}

var goPaging_divPageNo1 = function(cPage)
{
	$("#cPage2").val(cPage);
	pageOn = true;
    Paging1(totalCount, $("#dsplyCnt1").val(), cPage, "divPageNo1");
};

function ajaxSet(url) {
    $.ajax({
        type: "POST",
        url: url,
        async:false,
//         contentType: "application/json",
        success:function(resultData)
        {       	
	        var resultList = resultData.resultList;
        	var html='';
        	if(resultList && resultList.length > 0){
				for(var key in resultList){
					var itemInfo = resultList[key];
					
					html += '<tr>';
		            html += '	<td>' + itemInfo['content_cd'] + '</td>';
		            html += '	<td>' + itemInfo['create_dt'] + '</td>';
		            html += '	<td><button type="button" class="tblInBtn btnModifyRow" id=' + itemInfo['content_cd'] + " " + 'name=' + "'" + itemInfo['sortation'] +"' " + 'onclick="fn_delete(this)">삭제</button></td>';
		            html += '</tr>';
				}
				$('#pv_itemView_tbody1').children().remove();
                $('#pv_itemView_tbody1').append(html);
			}else{
				html += '<tr><td colspan="7" style="text-align:center">No data available in table</td>';
				$('#pv_itemView_tbody1').children().remove();
                $('#pv_itemView_tbody1').append(html);
			}
        },
        error:function(e)
        {
            alert('서버와의 통신에 실패하였습니다.');
        }
    });
}

function pageEvents(){
    // 추천MD관리 등록 팝업
    $('#btnPopUpAdd').unbind('click').bind('click',function(){ 
        fn_util_divPopUpControl('divPopUpA',true);
        fn_setColumnTitle();
    });
        
    // 저장 
    $('#btnA-insert').unbind('click').bind('click',function(){
        fn_add();
    });
    
    // 취소
    $('#btnA-cancel').unbind('click').bind('click',function(){
    	$('#popUpA-content_cd').val("");
    	fn_util_divPopUpControl('divPopUpA',false);
    });
    
    // 검색버튼
    $('#btnSearch').unbind('click').bind('click',function(){
    	var searchText = $('#inpSearch').val();
    	var searchOptionNm = $('#searchOptionNm').val();
    	listAjax(searchOptionNm, searchText, $("#dsplyCnt1").val(), '1');
    	Paging1(totalCount, $("#dsplyCnt1").val(), 1, "divPageNo1");
    });
    
    // 10개씩 보기, 20개씩 보기
    $('#dsplyCnt1').change(function(){
    	listAjax($('#searchOptionNm').val() ,  $('#inpSearch').val(), $('#dsplyCnt1').val(), '1'); // 제품 , 검색어, 갯수 설정대로 보기 , 1페이지
        Paging1(totalCount, parseInt($(this).val()), 1, "divPageNo1");
    });
    
}

function fn_search() {
	var searchText = $('#inpSearch').val();
	var searchOptionNm = $('#searchOptionNm').val();
	listAjax(searchOptionNm, searchText, $('#dsplyCnt1').val(), '1');
	Paging1(totalCount, $("#dsplyCnt1").val(), 1, "divPageNo1");
}

// 추천MD관리 등록 컬럼명 변경
function fn_setColumnTitle(){
	if($('#searchOptionNm').val() == 'P')
		$('#sortation').text('제품 코드');
	else
		$('#sortation').text('키워드 코드');	
}

// 등록팝업 - 저장
function fn_add(){
	var sortation = $('#searchOptionNm').val();  // 제품, 키워드 구분
	var content_cd = $('#popUpA-content_cd').val();
	var create_id = $('#popUpA-create_id').val();
	
	var url = controller
			+ '/recommand/manager/rc-except-insert.do'
			+ '?sortation=' + sortation 
			+ '&content_cd=' + content_cd 
			+ '&create_id=' + create_id
			; 
	
	if (fn_isValid_insert()){
		if (confirm("등록 하시겠습니까?")) {
		   	var param = $('#addForm').serialize(); // 객체 직렬화
		    $.ajax({
		    type: 'POST',
		    url: url,
		    crossDomain:true,
		    async:false,
		   	success:function(resultData){
		   	if(resultData != null){
		   		if (resultData.result == "입력한 값이 모두 중복입니다.") {
			    	alert(resultData.result);
		   		}else{
		   			alert(resultData.result);
		   	    	$('#popUpA-content_cd').val("");
		   			fn_util_divPopUpControl('divPopUpA',false);
			        listAjax($('#searchOptionNm').val() , '', $('#dsplyCnt1').val(), '1'); // 제품 , 검색어, 리스트 갯수 , 1페이지
		   		}
		    }
		    },
		    error:function(e){
		    alert('서버와의 통신에 실패하였습니다.');
		    }
		});
		}
	}
}

// 리스트 - 삭제
function fn_delete(tag){
	var content_cd = tag.id;
	var sortation = tag.name;
	
	var url = controller
			+ '/recommand/manager/rc-except-delete.do'
		 	+ '?sortation=' + sortation 
		 	+ '&content_cd=' + content_cd
		 	;

    if (confirm("삭제 하시겠습니까?")) {
        $.ajax({
            type: 'POST',
            url: url,
            crossDomain:true,
            async:false,
            success:function(resultData){
                if(resultData != null){
                    if(0 != resultData.result) {
                        alert('처리되었습니다.');
                    } else {
                        alert('관리자에게 문의 바랍니다.');
                    }
                    listAjax($('#searchOptionNm').val() , '', $('#dsplyCnt1').val(), '1'); // 제품 , 검색어, 리스트 갯수 , 1페이지
                }
            },
            error:function(e){
                alert('서버와의 통신에 실패하였습니다.');
            }
        });
    }
}

// 예외처리
function fn_isValid_insert(){
    var isValid = false;
	if ($('#popUpA-content_cd').val() == "") {
			alert('제품코드를 입력해주세요');
			$('#popUpA-content_cd').focus();
		} else {
			isValid = true;
		}
		return isValid;
}


// 제품 & 키워드 구분
function sortationSend(){
	var selectTag = document.getElementById("searchOptionNm");
	var selectValue = selectTag.options[selectTag.selectedIndex].value;
	$("#searchOptionNm").val(selectValue);
	var searchOptionNm = selectValue;
	$('#inpSearch').val('');
	$("#dsplyCnt1").val('10');
	var searchText = $('#inpSearch').val();
	pageOn = false;
	$("#cPage2").val('1');
	
	listAjax(searchOptionNm, searchText, $("#dsplyCnt1").val(), '1');	
} 

/*  function addEnterKey(){
	if(window.event.keyCode == 13){
		fn_add();
	}
	if(window.event.keyCode == 27){
		fn_util_divPopUpControl('divPopUpA', false); // 팝업창 닫기
        inicialize();
	}
}  */
</script>


<div class="mConBox">
	<div class="mCont">
		<div class="row">
			<div class="col-md-4" id="subtitleA">
				<h2>
					<i class="ti-layers"></i>
				</h2>
			</div>

			<div class="col-md-8">
				<div class="pageRoute">
					<ul>
						<li><a href="#">HOME</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li><a href="#">추천환경 관리</a></li>
						<li><i class="ti-angle-right"></i></li>
						<li class="current"><a href="#">추천MD 관리</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end row -->
		
		<form id="listForm" method="get" onsubmit="return false"  onkeydown="javascript:if(event.keyCode==13){fn_search();}">
		<input type="hidden" name="cPage2" id="cPage2" value='1'/>
			<div class="row">
				<div class="col-md-12">
					<div class="conBox" style="min-height: 400px;">
						<div class="row">
							<div class="col-md-33 select-set-3">
								<p>
											총	<b id="listTotalCount"></b> 개
								<select id="searchOptionNm" onchange="sortationSend()">
									<option value="P">제품</option>
									<option value="K">키워드</option>
								</select>
								</p>
							</div>
							<div class="col-md-9 text-right select-sys-1">
								<select name="dsplyCnt1" id="dsplyCnt1" >
									<option value="10">10개씩 보기</option>
									<option value="20">20개씩 보기</option>
								</select>
								<input path="searchText" type="text" id="inpSearch"
									class="searchTxt" placeholder="검색어를 입력하세요"/> 
								<button type="button" id="btnSearch" class="searchBtn">
									<i class="ti-search"></i> 검색
								</button>
								<button type="button" title="추천MD 제품을 추가할 수 있습니다." id="btnPopUpAdd" class="AddDefaultBtn">
									<i class="fa fa-plus"></i> 추천MD관리 등록
								</button>
							</div>
						</div>
						
						<!-- end row > conBox > col-md-12 > row > form -->
						<div class="row">
							<div class="col-md-12">
								<table class="viewDateTbl" >
									<colgroup>
										<col width="500">
										<col width="170">
										<col width="100"> 
									</colgroup>
									<thead>
										<tr>
											<th>코드</th>
											<th>등록일</th>
											<th>상세관리</th>
										</tr>
									</thead>
									<tbody id="pv_itemView_tbody1"></tbody>
								</table>
							</div>
						</div>

						 <div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8 pageNum">
								<!-- Paging With Taglib -->
								<div id="divPageNo1" class="col-md-12 pageNum"></div>
							</div>
							<div class="col-md-2 select-set-3"></div>
						</div>
						 
					</div>
				</div>
			</div>
		</form>
	</div>
</div>



<div id="divPopUpA" class="popUp col-md-8">
    <div class="popUpTop">추천MD관리 등록
        <span class="closePop"><i class="fa fa-times" aria-hidden="true"></i></span>
    </div>
    <div class="popUpMain">
        <div class="popUpCon">
            <form:form method="POST"  id="addForm">
            	<input type="hidden" id="popUpA-check" name="popUpA-check" />
            	<input type="hidden" id="popUpA-create_id" name="popUpA-create_id" value="${sessionScope.userInfo.userId}"/>
            	<input type="hidden" id="searchWord" name="popUpA-check" /> 
                <table class="popInTbl">
                    <colgroup>
                        <col width="20%"/>
                        <col width="80%"/>
                    </colgroup>
                    <tr>
                    	<th id="sortation"></th>
                        <td>
                            <textarea id="popUpA-content_cd" name="content_cd"  placeholder="여러개를 동시에 입력하고 싶은 경우 ',' 를 입력해주세요. "
                            style="min-height:200px"></textarea>
                        </td>
                    </tr>
                </table>
            </form:form>
        </div>
        <div class="popUpBtn">
            <button type="button" id="btnA-insert" class="btn-style1 darkGrayBtn">저장</button>
            <button type="button" id="btnA-cancel" class="btn-style1 lightGrayBtn popCancel">취소</button>
        </div>
    </div>
</div>

</div>