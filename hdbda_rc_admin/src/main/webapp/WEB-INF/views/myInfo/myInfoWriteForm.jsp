<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">

function passwordCheck() {
    var pwRegExp = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,20}/;

    var chk_num = $("#userPassword").val().search(/[0-9]/g);
    var chk_eng = $("#userPassword").val().search(/[a-z]/ig);

    if ($("#oldUserPassword").val().trim() == "") {
        alert("기존 비밀번호를 입력하세요.");
        $("#oldUserPassword").focus();
        return;
    }

    if ($("#userPassword").val().trim() == "") {
        alert("신규 비밀번호를 입력하세요.");
        $("#userPassword").focus();
        return;
    }

    if ($("#reUserPassword").val().trim() == "") {
        alert("신규 비밀번호 확인을 입력하세요.");
        $("#reUserPassword").focus();
        return;
    }

    if ($("#userPassword").val().trim() != $("#reUserPassword").val().trim()) {
        alert("신규 비밀번호와 비밀번호 확인이 다릅니다.");
        $("#userPassword").focus();
        return;
    }

    if (!pwRegExp.test($("#userPassword").val())) {
        alert("잘못된 비밀번호 형식입니다. 8~20자 이내의 영문자,숫자,특수문자를 혼합해서 입력하세요.");
        $("#userPassword").val("");
        $("#reUserPassword").val("");
        $("#userPassword").focus();
        return;
    }

    if (confirm("변경 하시겠습니까?")) {
        $.ajax({
            type: "POST",
            url: "getPasswordCheck.do",
            data: $("#writeForm").serialize(),
            success: function(data) {
                if( ! $.isEmptyObject(data) ) {
                    if(0 == data.result) {
                        alert("기존 비밀번호를 잘못 입력하셨습니다.");
                        $("#oldUserPassword").val("");
                        $("#oldUserPassword").focus();
                        return;
                    } else {
                    	pwdHisCntCheck();
                    }
                }
            },
            error : function(e) {
                alert("시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.");	
            }
        });
    }
}

function pwdHisCntCheck() {
    $.ajax({
        type: "POST",
        url: "myInfoHisCheck.do",
        data: $("#writeForm").serialize(),
        success: function(data) {
            if( ! $.isEmptyObject(data) ) {
                if(data.result == 0) {
                    savePassword();
                    return;
                } 
                if(data.result == -1) {
                    alert("데이터베이스 조회 에러: 관리자에게 문의해 주시기 바랍니다.");
                    savePassword();
                    return;
                }  else {
                	alert("해당 비밀번호는 기존에 이미 사용했던 비밀번호입니다. 신규 비밀번호를 입력해 주시기 바랍니다.");
                     $("#userPassword").val("");
                     $("#reUserPassword").val("");
                	return;
                }
                
            }
        },
        error : function(e) {
            alert("시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.");	
        }
    });
}

function savePassword() {
    $.ajax({
        type: "POST",
        url: "myInfoWrite.do",
        data: $("#writeForm").serialize(),
        success: function(data) {
            if( ! $.isEmptyObject(data) ) {
                if(0 == data.result) {        // 수정결과 카운트1
                    alert("오류가 발생하였습니다.");  
                } else {
                    alert("변경 되었습니다.");
                    location.href="/index/adminDashboard.do";
                }
            }
        },
        error : function(e) {
            alert("시스템에러 : 웹서비스 관리자에게 문의해 주시기 바랍니다.");	
        }
    });
}
</script>

<form action="<%=request.getContextPath()%>/myinfo/myInfoWrite.do" method="post" id="writeForm">
<input type="hidden" name="RecordCountPerPage" value="1" />
<input type="hidden" name="pageSize" value="10" />    		
<div class="mConBox">
    <div class="mCont">
        <div class="row">
            <div class="col-md-4" id="subtitleA">
            </div>

            <div class="col-md-8">
                <div class="pageRoute">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="conBox" style="min-height:200px;">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="popInTbl">
                                <colgroup>
                                    <col width="15%">
                                    <col width="65%">
                                </colgroup>
                                <tr>
                                    <th><label for="">아이디</label></th>
                                    <td>${authUser.userId}</td>
                                </tr>
                                <tr>
                                    <th><label for="">권한</label></th>
									<td>${authUser.userRoleDc}</td>
                                </tr>
                                <tr>
                                    <th><label for="oldUserPassword">기존 비밀번호</label></th>
                                    <td class="pwTd">
                                        <input type="password" id="oldUserPassword" name="oldUserPassword"  title="기존 비밀번호" maxlength="20" />
                                    </td>
                                </tr>
                                <tr>
                                    <th><label for="userPassword">신규 비밀번호</label></th>
                                    <td class="pwTd">
                                        <input type="password" id="userPassword" name="userPassword" title="신규 비밀번호" maxlength="20"/>
                                        <p>비밀번호는 8~20자 이내의 영문자,숫자,특수문자를 포함해서 사용해주십시오.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <th><label for="reUserPassword">신규 비밀번호 확인</label></th>
                                    <td class="pwTd">
                                        <input type="password" id="reUserPassword" name="reUserPassword" title="신규 비밀번호 확인" maxlength="20"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="popUpBtn text-center">
                                <button class="darkGrayBtn" onClick="passwordCheck();return false;">저장</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</form>