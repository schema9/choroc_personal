

var mnChk = 'off';

// 좌측메뉴 높이 설정
function leftH(){
	
	var rH = $(".right-col").height();

	var wH = parseInt(rH) + parseInt('100');


	if( wH < $(window).height() ){
		$(".left-col").height($(window).height());
	}else{
		$(".left-col").height(wH);
	}

}





// 좌측메뉴 넣기
function inMenu(a){
	
	var pageSet = $("#pageSet").val();

	var mHtml = "";

	var sHtml = "";
	
//	if()

	mHtml +="<ul>";
	
	mHtml +="<li ><a href='#' onclick='return false;'><i class='ti-hand-point-up'></i> Ⅰ. 추천 API</a> <i class='ti-angle-down'></i>";
	mHtml +="<ul class='subMenu'>";
	mHtml +="<li><a href='MYG_API_001.html'>1. 추천 API 소개</a></li>";
	mHtml +="<li><a href='MYG_API_002.html'>2. 추천 API</a></li>";
	mHtml +="</ul>";
	mHtml +="</li>";
	mHtml +="</ul>";







	$("#nav").html(mHtml);	

	$("#allMenuInner").html(mHtml);	



	if(pageSet == 'MYG_API_001'){
		onMenuSet('1','1');
	}

	if(pageSet == 'MYG_API_002'){
		onMenuSet('1','2');
	}
}


// 메뉴열고 닫고 체크
function onMenuSet(a,b){
	$("#nav ul > li:nth-child("+a+")").not("#nav ul > li > ul.subMenu > li").addClass('active');
	$("#nav ul > li:nth-child("+a+")").find('.ti-angle-down').attr('class','ti-angle-up');
	$("#nav ul > li:nth-child("+a+") > ul.subMenu").addClass('onMenu');
	$("#nav ul > li:nth-child("+a+") > ul > li:nth-child("+b+")").addClass('active');	
}



var chkE = true;

// 메뉴가 열렸었는지 닫었었는지 체크
function chkM(a){
	
	if(chkE){
		chkE = false;
	}else{
		chkE = true;
	}

}

//메뉴열기
function menuOpen(){
	$(".logoTop .topBtn").css("background","#274298");
	$(".logoTop").removeClass('hideMenuTop');
    $(".nav").removeClass('hideMenu');

	$(".onMenu").show();

	$(".left-col").css("left","0px");
	
	if( $(window).width() > '1024' ){
		$(".lecoTop").css("padding-left","250px");
		$(".right-col").css("padding-left","250px");
	}

	$(".left-col .nav ul").css("margin-left","0px");
	$(".left-col .nav ul li > i").show();

}

//메뉴닫기
function menuClose(){
	$(".logoTop").addClass('hideMenuTop');
    $(".nav").addClass('hideMenu');
	$(".subMenu").hide();
	$(".left-col").css("left","-190px");
	
	$(".lecoTop").css("padding-left","60px");
	$(".right-col").css("padding-left","60px");

	$(".logoTop .topBtn").css("background","#274298");
	$(".left-col .nav ul").css("margin-left","189px");
	$(".left-col .nav ul li > i").hide();

	allMenuOff();

}

//메뉴를 자동으로 닫고 열고체크
function menuSet(){

	if( $(".logoTop").hasClass('logoTop hideMenuTop')){
		menuOpen();
		mnChk = 'off';
	}else{
		menuClose();
		mnChk = 'on';
	}
}


//전체메뉴넣기
function allMenuOn(){
	$(".nav ul li.allMenu > i").attr('class','ti-angle-left');
	$("#viewAllMenu").addClass('openM');
	$("#viewAllMenu").css('left','0px');
}


//전체메뉴넣기
function allMenuOff(){
	$(".nav ul li.allMenu > i").attr('class','ti-angle-right');
	$("#viewAllMenu").removeClass('openM');
	$("#viewAllMenu").css('left','-1920px');
}

// 로딩박스
function loadingBox(){
	
	var lHtml = "";

	lHtml += '<div class="sk-cube-grid">';
	lHtml += '<div class="sk-cube sk-cube1"></div>';
	lHtml += '<div class="sk-cube sk-cube2"></div>';
	lHtml += '<div class="sk-cube sk-cube3"></div>';
	lHtml += '<div class="sk-cube sk-cube4"></div>';
	lHtml += '<div class="sk-cube sk-cube5"></div>';
	lHtml += '<div class="sk-cube sk-cube6"></div>';
	lHtml += '<div class="sk-cube sk-cube7"></div>';
	lHtml += '<div class="sk-cube sk-cube8"></div>';
	lHtml += '<div class="sk-cube sk-cube9"></div>';
	lHtml += '</div>';

	lHtml += '<div class="sk-cube-txt">초록마을</div>';

	$("#loading").html(lHtml);

}

function exmBoxContr(a){

	if( $(a).parent().parent().parent().parent().parent().find('td.preBox').parent('tr').css('display') == "none" ){
		$(a).text('접기');
		$(a).parent().parent().parent().parent().parent().find('td.preBox').parent('tr').show();
	}else{
		$(a).text('펼치기');
		$(a).parent().parent().parent().parent().parent().find('td.preBox').parent('tr').hide();
	}

}


$(window).ready(function(){
	

//	$("th.exmBox").html('');
	
	$('.exmBoxCont').click(function(){
		exmBoxContr(this);
		
	});

	

	loadingBox();

	inMenu();

	leftH(); // 좌측메뉴 높이 설정

	// 화면사이즈에 따라 메뉴열기/닫기
	if( $(window).width() > '1200' ){
		menuOpen();
	}else{
		menuClose();
	}

});

$(window).scroll(function(){
	if( $(window).scrollTop() > 300 ){
		$(".goTopBtn").fadeIn();
	}else{
		$(".goTopBtn").fadeOut();
	}
});

$(window).resize(function(){
	leftH(); // 좌측메뉴 높이 설정

	if(mnChk == "off"){
  		// 화면사이즈에 따라 메뉴열기/닫기
		if( $(window).width() > '1200' ){
			menuOpen();
		}else{
			menuClose();
		}
	}

	//메인에 카운트
//	mainBoxSizeChk();

});



$(function(){
	


	$(".nav ul > li > a").click(function(){

	

			if($(this).parent('li').hasClass('allMenu')){
				
				if($("#viewAllMenu").hasClass('openM')){
					allMenuOff();
				}else{
					allMenuOn();
				}

			}else{

			menuOpen();

				if( $(this).parent('li').hasClass('active') ){
					
					$(this).parent('li').removeClass('active');
					$(this).parent('li').find('ul').slideUp('300');
					$(this).parent('li').find('ul').removeClass('onMenu');
					
					$(this).parent('li').find('i.ti-angle-up').attr('class','ti-angle-down');

				}else{
					
					$(this).parent('li').addClass('active');
					$(this).parent('li').find('ul').slideDown('300');
					$(this).parent('li').find('ul').addClass('onMenu');
					$(this).parent('li').find('i.ti-angle-down').attr('class','ti-angle-up');

				}

			}


	});

	$("#topBtn").click(function(){
		menuSet();
	});





});




$(window).load(function(){
	
		$("#loading").fadeOut('300');
});

