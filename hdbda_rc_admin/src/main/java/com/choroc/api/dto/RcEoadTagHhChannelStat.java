package com.choroc.api.dto;
/**
 * TODO > [HUE] hbda_srvc.rc_tag_hh_channel_stat
 * 파일명   > RcEoadTagHhChannelStat.java
 * 작성일   > 2018. 12. 27.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public class RcEoadTagHhChannelStat {

	private String startDate; 
	private String endDate;   
	private String date;
	private String dateType;
	
	private String yyyymmdd;
	private String hh;
	private String tag_type;
	private String channel;
	
	private double all_cnt;
	private double device_id_cnt;
	private double item_id_cnt;
	private double keyword_cnt;
	private double camptrac_cd_cnt;
	private double login_after_cnt;
	private double qty_sum;
	private double price_sum;
		
	private double pc_cnt;
	private double pc_device;
	private double pc_item;
	
	private double mobile_cnt;
	private double mobile_device;
	private double mobile_item;
	
	private double ios_cnt;
	private double ios_device;
	private double ios_item;
	
	private double android_cnt;
	private double android_device;
	private double android_item;
	
	private double pc_view;
	private double pc_cart;
	private double pc_buy;
	private double mobile_view;
	private double mobile_cart;
	private double mobile_buy;
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDateType() {
		return dateType;
	}

	public void setDateType(String dateType) {
		this.dateType = dateType;
	}	
	
	public double getPc_cnt() {
		return pc_cnt;
	}

	public void setPc_cnt(double pc_cnt) {
		this.pc_cnt = pc_cnt;
	}

	public double getPc_device() {
		return pc_device;
	}

	public void setPc_device(double pc_device) {
		this.pc_device = pc_device;
	}

	public double getPc_item() {
		return pc_item;
	}

	public void setPc_item(double pc_item) {
		this.pc_item = pc_item;
	}

	public double getMobile_cnt() {
		return mobile_cnt;
	}

	public void setMobile_cnt(double mobile_cnt) {
		this.mobile_cnt = mobile_cnt;
	}

	public double getMobile_device() {
		return mobile_device;
	}

	public void setMobile_device(double mobile_device) {
		this.mobile_device = mobile_device;
	}

	public double getMobile_item() {
		return mobile_item;
	}

	public void setMobile_item(double mobile_item) {
		this.mobile_item = mobile_item;
	}

	public double getIos_cnt() {
		return ios_cnt;
	}

	public void setIos_cnt(double ios_cnt) {
		this.ios_cnt = ios_cnt;
	}

	public double getIos_device() {
		return ios_device;
	}

	public void setIos_device(double ios_device) {
		this.ios_device = ios_device;
	}

	public double getIos_item() {
		return ios_item;
	}

	public void setIos_item(double ios_item) {
		this.ios_item = ios_item;
	}

	public double getAndroid_cnt() {
		return android_cnt;
	}

	public void setAndroid_cnt(double android_cnt) {
		this.android_cnt = android_cnt;
	}

	public double getAndroid_device() {
		return android_device;
	}

	public void setAndroid_device(double android_device) {
		this.android_device = android_device;
	}

	public double getAndroid_item() {
		return android_item;
	}

	public void setAndroid_item(double android_item) {
		this.android_item = android_item;
	}
	
	private String hbda_etl_reg_date;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getYyyymmdd() {
		return yyyymmdd;
	}

	public void setYyyymmdd(String yyyymmdd) {
		this.yyyymmdd = yyyymmdd;
	}

	public String getHh() {
		return hh;
	}

	public void setHh(String hh) {
		this.hh = hh;
	}

	public String getTag_type() {
		return tag_type;
	}

	public void setTag_type(String tag_type) {
		this.tag_type = tag_type;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public double getAll_cnt() {
		return all_cnt;
	}

	public void setAll_cnt(double all_cnt) {
		this.all_cnt = all_cnt;
	}

	public double getDevice_id_cnt() {
		return device_id_cnt;
	}

	public void setDevice_id_cnt(double device_id_cnt) {
		this.device_id_cnt = device_id_cnt;
	}

	public double getItem_id_cnt() {
		return item_id_cnt;
	}

	public void setItem_id_cnt(double item_id_cnt) {
		this.item_id_cnt = item_id_cnt;
	}

	public double getKeyword_cnt() {
		return keyword_cnt;
	}

	public void setKeyword_cnt(double keyword_cnt) {
		this.keyword_cnt = keyword_cnt;
	}

	public double getCamptrac_cd_cnt() {
		return camptrac_cd_cnt;
	}

	public void setCamptrac_cd_cnt(double camptrac_cd_cnt) {
		this.camptrac_cd_cnt = camptrac_cd_cnt;
	}

	public double getLogin_after_cnt() {
		return login_after_cnt;
	}

	public void setLogin_after_cnt(double login_after_cnt) {
		this.login_after_cnt = login_after_cnt;
	}

	public double getQty_sum() {
		return qty_sum;
	}

	public void setQty_sum(double qty_sum) {
		this.qty_sum = qty_sum;
	}

	public double getPrice_sum() {
		return price_sum;
	}

	public void setPrice_sum(double price_sum) {
		this.price_sum = price_sum;
	}

	public String getHbda_etl_reg_date() {
		return hbda_etl_reg_date;
	}

	public void setHbda_etl_reg_date(String hbda_etl_reg_date) {
		this.hbda_etl_reg_date = hbda_etl_reg_date;
	}

	public String getDateFormat(String date) {
		String year = date.substring(0,4);
		String month = date.substring(4, 6);
		String day = date.substring(6, 8);
		String formatDate = year + "-" + month + "-" + day;
		return formatDate;
	}

	public double getPc_view() {
		return pc_view;
	}

	public void setPc_view(double pc_view) {
		this.pc_view = pc_view;
	}

	public double getPc_cart() {
		return pc_cart;
	}

	public void setPc_cart(double pc_cart) {
		this.pc_cart = pc_cart;
	}

	public double getPc_buy() {
		return pc_buy;
	}

	public void setPc_buy(double pc_buy) {
		this.pc_buy = pc_buy;
	}

	public double getMobile_view() {
		return mobile_view;
	}

	public void setMobile_view(double mobile_view) {
		this.mobile_view = mobile_view;
	}

	public double getMobile_cart() {
		return mobile_cart;
	}

	public void setMobile_cart(double mobile_cart) {
		this.mobile_cart = mobile_cart;
	}

	public double getMobile_buy() {
		return mobile_buy;
	}

	public void setMobile_buy(double mobile_buy) {
		this.mobile_buy = mobile_buy;
	}
	
	

}
