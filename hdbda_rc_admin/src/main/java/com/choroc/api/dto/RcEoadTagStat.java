package com.choroc.api.dto;

public class RcEoadTagStat {
	
	public RcEoadTagStat() {
		this.view_cnt=0;
		this.view_rc_cnt=0;
		this.view_cust_cnt=0;
		this.view_cust_rc_cnt=0;
		this.view_item_cnt=0;
		this.view_item_rc_cnt=0;
		this.cart_cnt=0;
		this.cart_rc_cnt=0;
		this.cart_cust_cnt=0;
		this.cart_cust_rc_cnt=0;
		this.cart_item_cnt=0;
		this.cart_item_rc_cnt=0;
		this.search_cnt=0;
		this.search_rc_cnt=0;
		this.search_cust_cnt=0;
		this.search_cust_rc_cnt=0;
		this.search_item_cnt=0;
		this.search_item_rc_cnt=0;
		this.buy_cnt=0;
		this.buy_rc_cnt=0;
		this.buy_cust_cnt=0;
		this.buy_cust_rc_cnt=0;
		this.buy_item_cnt=0;
		this.buy_item_rc_cnt=0;
		this.buy_price_sum=0;
		this.buy_price_rc_sum=0;
	}
	
	// 조회기준
	private String yyyymmdd;
	
	private String startDate; // 시작일
	private String endDate;   // 종료일
	
	
	// 클릭
	private int view_cnt=0;
	private int view_rc_cnt=0;
	private int view_cust_cnt=0;
	private int view_cust_rc_cnt=0;
	private int view_item_cnt=0;
	private int view_item_rc_cnt=0;
	
	// 장바구니
	private int cart_cnt=0;
	private int cart_rc_cnt=0;
	private int cart_cust_cnt=0;
	private int cart_cust_rc_cnt=0;
	private int cart_item_cnt=0;
	private int cart_item_rc_cnt=0;
	
	// 검색
	private int search_cnt=0;
	private int search_rc_cnt=0;
	private int search_cust_cnt=0;
	private int search_cust_rc_cnt=0;
	private int search_item_cnt=0;
	private int search_item_rc_cnt=0;
	
	// 구매
	private int buy_cnt=0;
	private int buy_rc_cnt=0;
	private int buy_cust_cnt=0;
	private int buy_cust_rc_cnt=0;
	private int buy_item_cnt=0;
	private int buy_item_rc_cnt=0;
	private double buy_price_sum=0;
	private double buy_price_rc_sum=0;
	
	
	public String getYyyymmdd() {
		return yyyymmdd;
	}
	public void setYyyymmdd(String yyyymmdd) {
		this.yyyymmdd = yyyymmdd;
	}
	public int getView_cnt() {
		return view_cnt;
	}
	public void setView_cnt(int view_cnt) {
		this.view_cnt = view_cnt;
	}
	public int getView_rc_cnt() {
		return view_rc_cnt;
	}
	public void setView_rc_cnt(int view_rc_cnt) {
		this.view_rc_cnt = view_rc_cnt;
	}
	public int getView_cust_cnt() {
		return view_cust_cnt;
	}
	public void setView_cust_cnt(int view_cust_cnt) {
		this.view_cust_cnt = view_cust_cnt;
	}
	public int getView_cust_rc_cnt() {
		return view_cust_rc_cnt;
	}
	public void setView_cust_rc_cnt(int view_cust_rc_cnt) {
		this.view_cust_rc_cnt = view_cust_rc_cnt;
	}
	public int getView_item_cnt() {
		return view_item_cnt;
	}
	public void setView_item_cnt(int view_item_cnt) {
		this.view_item_cnt = view_item_cnt;
	}
	public int getView_item_rc_cnt() {
		return view_item_rc_cnt;
	}
	public void setView_item_rc_cnt(int view_item_rc_cnt) {
		this.view_item_rc_cnt = view_item_rc_cnt;
	}
	public int getCart_cnt() {
		return cart_cnt;
	}
	public void setCart_cnt(int cart_cnt) {
		this.cart_cnt = cart_cnt;
	}
	public int getCart_rc_cnt() {
		return cart_rc_cnt;
	}
	public void setCart_rc_cnt(int cart_rc_cnt) {
		this.cart_rc_cnt = cart_rc_cnt;
	}
	public int getCart_cust_cnt() {
		return cart_cust_cnt;
	}
	public void setCart_cust_cnt(int cart_cust_cnt) {
		this.cart_cust_cnt = cart_cust_cnt;
	}
	public int getCart_cust_rc_cnt() {
		return cart_cust_rc_cnt;
	}
	public void setCart_cust_rc_cnt(int cart_cust_rc_cnt) {
		this.cart_cust_rc_cnt = cart_cust_rc_cnt;
	}
	public int getCart_item_cnt() {
		return cart_item_cnt;
	}
	public void setCart_item_cnt(int cart_item_cnt) {
		this.cart_item_cnt = cart_item_cnt;
	}
	public int getCart_item_rc_cnt() {
		return cart_item_rc_cnt;
	}
	public void setCart_item_rc_cnt(int cart_item_rc_cnt) {
		this.cart_item_rc_cnt = cart_item_rc_cnt;
	}
	public int getSearch_cnt() {
		return search_cnt;
	}
	public void setSearch_cnt(int search_cnt) {
		this.search_cnt = search_cnt;
	}
	public int getSearch_rc_cnt() {
		return search_rc_cnt;
	}
	public void setSearch_rc_cnt(int search_rc_cnt) {
		this.search_rc_cnt = search_rc_cnt;
	}
	public int getSearch_cust_cnt() {
		return search_cust_cnt;
	}
	public void setSearch_cust_cnt(int search_cust_cnt) {
		this.search_cust_cnt = search_cust_cnt;
	}
	public int getSearch_cust_rc_cnt() {
		return search_cust_rc_cnt;
	}
	public void setSearch_cust_rc_cnt(int search_cust_rc_cnt) {
		this.search_cust_rc_cnt = search_cust_rc_cnt;
	}
	public int getSearch_item_cnt() {
		return search_item_cnt;
	}
	public void setSearch_item_cnt(int search_item_cnt) {
		this.search_item_cnt = search_item_cnt;
	}
	public int getSearch_item_rc_cnt() {
		return search_item_rc_cnt;
	}
	public void setSearch_item_rc_cnt(int search_item_rc_cnt) {
		this.search_item_rc_cnt = search_item_rc_cnt;
	}
	public int getBuy_cnt() {
		return buy_cnt;
	}
	public void setBuy_cnt(int buy_cnt) {
		this.buy_cnt = buy_cnt;
	}
	public int getBuy_rc_cnt() {
		return buy_rc_cnt;
	}
	public void setBuy_rc_cnt(int buy_rc_cnt) {
		this.buy_rc_cnt = buy_rc_cnt;
	}
	public int getBuy_cust_cnt() {
		return buy_cust_cnt;
	}
	public void setBuy_cust_cnt(int buy_cust_cnt) {
		this.buy_cust_cnt = buy_cust_cnt;
	}
	public int getBuy_cust_rc_cnt() {
		return buy_cust_rc_cnt;
	}
	public void setBuy_cust_rc_cnt(int buy_cust_rc_cnt) {
		this.buy_cust_rc_cnt = buy_cust_rc_cnt;
	}
	public int getBuy_item_cnt() {
		return buy_item_cnt;
	}
	public void setBuy_item_cnt(int buy_item_cnt) {
		this.buy_item_cnt = buy_item_cnt;
	}
	public int getBuy_item_rc_cnt() {
		return buy_item_rc_cnt;
	}
	public void setBuy_item_rc_cnt(int buy_item_rc_cnt) {
		this.buy_item_rc_cnt = buy_item_rc_cnt;
	}
	public double getBuy_price_sum() {
		return buy_price_sum;
	}
	public void setBuy_price_sum(double buy_price_sum) {
		this.buy_price_sum = buy_price_sum;
	}
	public double getBuy_price_rc_sum() {
		return buy_price_rc_sum;
	}
	public void setBuy_price_rc_sum(double buy_price_rc_sum) {
		this.buy_price_rc_sum = buy_price_rc_sum;
	}
	
	
	
}
