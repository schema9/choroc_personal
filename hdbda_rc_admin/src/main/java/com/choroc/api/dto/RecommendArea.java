package com.choroc.api.dto;

public class RecommendArea {
	
	private String pge_area_id;
	private String pge_area_nm;
	private String pge_area_dc;
	private String pge_area_path;
	private String input_param;
	private String use_yn;
	private String del_yn;
	private String create_id;
	private String create_dt;
	private String modify_id;
	private String modify_dt;
	private String pge_area_type;
	
	private String algrth_id;
	private String algrth_nm;
	private String algrth_dc;
	
	
	public String getPge_area_id() {
		return pge_area_id;
	}
	public void setPge_area_id(String pge_area_id) {
		this.pge_area_id = pge_area_id;
	}
	public String getPge_area_nm() {
		return pge_area_nm;
	}
	public void setPge_area_nm(String pge_area_nm) {
		this.pge_area_nm = pge_area_nm;
	}
	public String getPge_area_dc() {
		return pge_area_dc;
	}
	public void setPge_area_dc(String pge_area_dc) {
		this.pge_area_dc = pge_area_dc;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getDel_yn() {
		return del_yn;
	}
	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}
	public String getCreate_id() {
		return create_id;
	}
	public void setCreate_id(String create_id) {
		this.create_id = create_id;
	}
	public String getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(String create_dt) {
		this.create_dt = create_dt;
	}
	public String getModify_id() {
		return modify_id;
	}
	public void setModify_id(String modify_id) {
		this.modify_id = modify_id;
	}
	public String getModify_dt() {
		return modify_dt;
	}
	public void setModify_dt(String modify_dt) {
		this.modify_dt = modify_dt;
	}
	public String getPge_area_type() {
		return pge_area_type;
	}
	public void setPge_area_type(String pge_area_type) {
		this.pge_area_type = pge_area_type;
	}
	public String getAlgrth_id() {
		return algrth_id;
	}
	public void setAlgrth_id(String algrth_id) {
		this.algrth_id = algrth_id;
	}
	public String getAlgrth_nm() {
		return algrth_nm;
	}
	public void setAlgrth_nm(String algrth_nm) {
		this.algrth_nm = algrth_nm;
	}
	public String getAlgrth_dc() {
		return algrth_dc;
	}
	public void setAlgrth_dc(String algrth_dc) {
		this.algrth_dc = algrth_dc;
	}
	public String getPge_area_path() {
		return pge_area_path;
	}
	public void setPge_area_path(String pge_area_path) {
		this.pge_area_path = pge_area_path;
	}
	public String getInput_param() {
		return input_param;
	}
	public void setInput_param(String input_param) {
		this.input_param = input_param;
	}
	
	
}
