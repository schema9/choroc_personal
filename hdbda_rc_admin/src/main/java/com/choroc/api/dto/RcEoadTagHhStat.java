package com.choroc.api.dto;

import java.math.BigInteger;

/**
 * TODO > [HUE] rc_tag_hh_stat
 * 파일명   > RcEoadTagHhStat.java
 * 작성일   > 2018. 12. 26.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public class RcEoadTagHhStat {

	private String startDate; // 시작일
	private String endDate;   // 종료일
	private String date;
	private String dateType;
	private String yyyymmdd;
	private String hh;
	private String tag_type;
	
	private double all_cnt;
	private double device_id_cnt;
	private double item_id_cnt;
	private double keyword_cnt;
	private double camptrac_cd_cnt;
	private double login_after_cnt;
	private double qty_sum;
	private double price_sum;
	
	private String hbda_etl_reg_date;
	
	private double view_click_1;
	private double view_click_2;
	private double view_click_3;
	private double view_click_4;
	private double view_click_5;
	
	private double view_click;
	private double view_cust;
	private double view_product;
	
	private double cart_click;
	private double cart_cust;
	private double cart_product;
	
	private double jjim_click;
	private double jjim_cust;
	private double jjim_product;
	
	private double search_click;
	private double search_cust;
	private double search_product;
	
	private double buy_click;
	private double buy_cust;
	private double buy_product;
	private double buy_price;
	
	private double pc_view;
	private double pc_cart;
	private double pc_buy;
	private double mobile_view;
	private double mobile_cart;
	private double mobile_buy;
	
	
	
	public String getDateType() {
		return dateType;
	}
	
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	
	public double getPrice_sum() {
		return price_sum;
	}
	
	public void setPrice_sum(double price_sum) {
		this.price_sum = price_sum;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public double getView_click() {
		return view_click;
	}
	
	public void setView_click(double view_click) {
		this.view_click = view_click;
	}

	public double getView_cust() {
		return view_cust;
	}

	public void setView_cust(double view_cust) {
		this.view_cust = view_cust;
	}

	public double getView_product() {
		return view_product;
	}

	public void setView_product(double view_product) {
		this.view_product = view_product;
	}

	public double getCart_click() {
		return cart_click;
	}

	public void setCart_click(double cart_click) {
		this.cart_click = cart_click;
	}

	public double getCart_cust() {
		return cart_cust;
	}

	public void setCart_cust(double cart_cust) {
		this.cart_cust = cart_cust;
	}

	public double getCart_product() {
		return cart_product;
	}

	public void setCart_product(double cart_product) {
		this.cart_product = cart_product;
	}

	public double getJjim_click() {
		return jjim_click;
	}

	public void setJjim_click(double jjim_click) {
		this.jjim_click = jjim_click;
	}

	public double getJjim_cust() {
		return jjim_cust;
	}

	public void setJjim_cust(double jjim_cust) {
		this.jjim_cust = jjim_cust;
	}

	public double getJjim_product() {
		return jjim_product;
	}

	public void setJjim_product(double jjim_product) {
		this.jjim_product = jjim_product;
	}

	public double getSearch_click() {
		return search_click;
	}

	public void setSearch_click(double search_click) {
		this.search_click = search_click;
	}

	public double getSearch_cust() {
		return search_cust;
	}

	public void setSearch_cust(double search_cust) {
		this.search_cust = search_cust;
	}

	public double getSearch_product() {
		return search_product;
	}

	public void setSearch_product(double search_product) {
		this.search_product = search_product;
	}

	public double getBuy_click() {
		return buy_click;
	}

	public void setBuy_click(double buy_click) {
		this.buy_click = buy_click;
	}

	public double getBuy_cust() {
		return buy_cust;
	}

	public void setBuy_cust(double buy_cust) {
		this.buy_cust = buy_cust;
	}

	public double getBuy_product() {
		return buy_product;
	}

	public void setBuy_product(double buy_product) {
		this.buy_product = buy_product;
	}

	public double getBuy_price() {
		return buy_price;
	}

	public void setBuy_price(double buy_price) {
		this.buy_price = buy_price;
	}

	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getYyyymmdd() {
		return yyyymmdd;
	}
	public void setYyyymmdd(String yyyymmdd) {
		this.yyyymmdd = yyyymmdd;
	}
	public String getHh() {
		return hh;
	}
	public void setHh(String hh) {
		this.hh = hh;
	}
	public String getTag_type() {
		return tag_type;
	}
	public void setTag_type(String tag_type) {
		this.tag_type = tag_type;
	}
	public double getAll_cnt() {
		return all_cnt;
	}
	public void setAll_cnt(double all_cnt) {
		this.all_cnt = all_cnt;
	}
	public double getDevice_id_cnt() {
		return device_id_cnt;
	}
	public void setDevice_id_cnt(double device_id_cnt) {
		this.device_id_cnt = device_id_cnt;
	}
	public double getItem_id_cnt() {
		return item_id_cnt;
	}
	public void setItem_id_cnt(double item_id_cnt) {
		this.item_id_cnt = item_id_cnt;
	}
	public double getKeyword_cnt() {
		return keyword_cnt;
	}
	public void setKeyword_cnt(double keyword_cnt) {
		this.keyword_cnt = keyword_cnt;
	}
	public double getCamptrac_cd_cnt() {
		return camptrac_cd_cnt;
	}
	public void setCamptrac_cd_cnt(double camptrac_cd_cnt) {
		this.camptrac_cd_cnt = camptrac_cd_cnt;
	}
	public double getLogin_after_cnt() {
		return login_after_cnt;
	}
	public void setLogin_after_cnt(int login_after_cnt) {
		this.login_after_cnt = login_after_cnt;
	}
	public double getQty_sum() {
		return qty_sum;
	}
	public void setQty_sum(int qty_sum) {
		this.qty_sum = qty_sum;
	}
	public String getHbda_etl_reg_date() {
		return hbda_etl_reg_date;
	}
	public void setHbda_etl_reg_date(String hbda_etl_reg_date) {
		this.hbda_etl_reg_date = hbda_etl_reg_date;
	}
		
	public String getDateFormat(String date) {
		String year = date.substring(0,4);
		String month = date.substring(4, 6);
		String day = date.substring(6, 8);
		String formatDate = year + "-" + month + "-" + day;
		return formatDate;
	}

	public double getView_click_1() {
		return view_click_1;
	}

	public void setView_click_1(double view_click_1) {
		this.view_click_1 = view_click_1;
	}

	public double getView_click_2() {
		return view_click_2;
	}

	public void setView_click_2(double view_click_2) {
		this.view_click_2 = view_click_2;
	}

	public double getView_click_3() {
		return view_click_3;
	}

	public void setView_click_3(double view_click_3) {
		this.view_click_3 = view_click_3;
	}

	public double getView_click_4() {
		return view_click_4;
	}

	public void setView_click_4(double view_click_4) {
		this.view_click_4 = view_click_4;
	}

	public double getView_click_5() {
		return view_click_5;
	}

	public void setView_click_5(double view_click_5) {
		this.view_click_5 = view_click_5;
	}

	public void setLogin_after_cnt(double login_after_cnt) {
		this.login_after_cnt = login_after_cnt;
	}

	public void setQty_sum(double qty_sum) {
		this.qty_sum = qty_sum;
	}

	public double getPc_view() {
		return pc_view;
	}

	public void setPc_view(double pc_view) {
		this.pc_view = pc_view;
	}

	public double getPc_cart() {
		return pc_cart;
	}

	public void setPc_cart(double pc_cart) {
		this.pc_cart = pc_cart;
	}

	public double getPc_buy() {
		return pc_buy;
	}

	public void setPc_buy(double pc_buy) {
		this.pc_buy = pc_buy;
	}

	public double getMobile_view() {
		return mobile_view;
	}

	public void setMobile_view(double mobile_view) {
		this.mobile_view = mobile_view;
	}

	public double getMobile_cart() {
		return mobile_cart;
	}

	public void setMobile_cart(double mobile_cart) {
		this.mobile_cart = mobile_cart;
	}

	public double getMobile_buy() {
		return mobile_buy;
	}

	public void setMobile_buy(double mobile_buy) {
		this.mobile_buy = mobile_buy;
	}
	
	
	
	
}
