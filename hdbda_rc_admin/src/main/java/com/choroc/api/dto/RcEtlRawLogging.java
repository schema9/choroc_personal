package com.choroc.api.dto;

/**
 * TODO > [HUE] hbda_srvc.etl_raw_logging
 * 파일명   > ETL_RAW_Log.java
 * 작성일   > 2018. 12. 11.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */

public class RcEtlRawLogging {

	String task_id;
	String etl_status;
	String sys_nm;
	String tbl_nm;
	String reg_dt;
	String etl_load_dd;
	String etl_value;
	String etl_message;
	
	
	
	public String getEtl_message() {
		return etl_message;
	}
	public void setEtl_message(String etl_message) {
		this.etl_message = etl_message;
	}
	public String getEtl_value() {
		return etl_value;
	}
	public void setEtl_value(String etl_value) {
		this.etl_value = etl_value;
	}
	public String getEtl_load_dd() {
		return etl_load_dd;
	}
	public void setEtl_load_dd(String etl_load_dd) {
		this.etl_load_dd = etl_load_dd;
	}
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getEtl_status() {
		return etl_status;
	}
	public void setEtl_status(String etl_status) {
		this.etl_status = etl_status;
	}
	public String getSys_nm() {
		return sys_nm;
	}
	public void setSys_nm(String sys_nm) {
		this.sys_nm = sys_nm;
	}
	public String getTbl_nm() {
		return tbl_nm;
	}
	public void setTbl_nm(String tbl_nm) {
		this.tbl_nm = tbl_nm;
	}
	public String getReg_dt() {
		return reg_dt;
	}
	public void setReg_dt(String reg_dt) {
		this.reg_dt = reg_dt;
	}
	
}
