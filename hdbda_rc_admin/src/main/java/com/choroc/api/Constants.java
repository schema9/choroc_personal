package com.choroc.api;

public class Constants {
    public final static String URI_PTN_ELOG = "etl-log";
    public final static String URI_PTN_CATEGORY_LIST = "category-select";
    
    public final static String URI_PTN_PRODUCT_RECO = "product-reco";
    public final static String URI_PTN_PRODUCT_RECO_PRODUCT_ALL = "product-reco-product-all";
    
    
    public final static String URI_PTN_USER_RECO = "user-reco";
    public final static String URI_PTN_USER_RECO_PRODUCT = "user-reco-product";
    public final static String URI_PTN_USER_RECO_PRODUCT_ALL = "user-reco-product-all";
    public final static String URI_PTN_PRODUCT_RECO_PRODUCT = "product-reco-product";
    public final static String URI_PTN_SEARCH_PRODUCT_RECO = "search-product-reco";
    public final static String URI_PTN_SEARCH_PRODUCT_RECO_PRODUCT = "search-product-reco-product";
    public final static String URI_PTN_SEARCH_PRODUCT_RECO_PRODUCT_ALL = "search-product-reco-product-all";
    public final static String URI_PTN_SEARCH_WORD_RECO = "search-word-reco";
    public final static String URI_PTN_SEARCH_WORD_RECO_WORD = "search-word-reco-word";
    public final static String URI_PTN_SEARCH_WORD_RECO_WORD_ALL = "search-word-reco-word-all";
    public final static String URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA = "performance-reco-collection-area";
    public final static String URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA_ALL = "performance-reco-collection-area-all";
    public final static String URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA2 = "performance-reco-collection-area2";
    public final static String URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA2_ALL = "performance-reco-collection-area2-all";
    public final static String URI_PTN_MAIN_PERFORMANCE = "main-dashboard-performance";
    public final static String URI_PTN_MAIN_CHART = "main-dashboard-chart"; 
    
    public final static String URI_PTN_ANALYSIS_DAYS_COUNT = "analysis-days-count";
    public final static String URI_PTN_ANALYSIS_DAYS_LIST = "analysis-days-list";
    public final static String URI_PTN_ANALYSIS_DAYS_CHART = "analysis-days-chart";
    public final static String URI_PTN_ANALYSIS_CATE_COUNT = "analysis-cate-count";
    public final static String URI_PTN_ANALYSIS_CATE_LIST = "analysis-cate-list";
    public final static String URI_PTN_ANALYSIS_CATE_CHART = "analysis-cate-chart";
    
    public final static String URI_PTN_RECOMMEND_MD_MANAGEMENT_LIST = "rc-md-list";
    public final static String URI_PTN_RECOMMEND_MD_MANAGEMENT_INSERT = "rc-md-insert";
    public final static String URI_PTN_RECOMMEND_MD_MANAGEMENT_DELETE = "rc-md-delete";
    
    public final static String URI_PTN_RECOMMEND_EXCEPTION_MANAGEMENT_LIST = "rc-except-list";
    public final static String URI_PTN_RECOMMEND_EXCEPTION_MANAGEMENT_INSERT = "rc-except-insert";
    public final static String URI_PTN_RECOMMEND_EXCEPTION_MANAGEMENT_DELETE = "rc-except-delete";
    
    public final static String URI_PTN_RECOMMEND_ALGORITHM_COUNT = "rc-algorithm-count";
    public final static String URI_PTN_RECOMMEND_ALGORITHM_LIST = "rc-algorithm-list";
    public final static String URI_PTN_RECOMMEND_ALGORITHM_PK_CHECK = "rc-algorithm-pk-check";
    public final static String URI_PTN_RECOMMEND_ALGORITHM_INSERT = "rc-algorithm-insert";
    public final static String URI_PTN_RECOMMEND_ALGORITHM_SELECTONE = "rc-algorithm-selectOne";
    public final static String URI_PTN_RECOMMEND_ALGORITHM_NM_CHECK = "rc-algorithm-nm-check";
    public final static String URI_PTN_RECOMMEND_ALGORITHM_UPDATE = "rc-algorithm-update";
    public final static String URI_PTN_RECOMMEND_ALGORITHM_DELETE = "rc_algorithm-delete";
    public final static String URI_PTN_RECOMMEND_AREA_COUNT = "rc-area-count";
    public final static String URI_PTN_RECOMMEND_AREA_LIST = "rc-area-list";
    public final static String URI_PTN_RECOMMEND_AREA_INSERT_ALGO = "rc-area-insert-algo";
    public final static String URI_PTN_RECOMMEND_AREA_INSERT = "rc-area-insert";
    public final static String URI_PTN_RECOMMEND_AREA_UPDATE_ALGO = "rc-area-update-algo";
    public final static String URI_PTN_RECOMMEND_AREA_UPDATE = "rc-area-update";
    public final static String URI_PTN_RECOMMEND_AREA_DELETE = "rc-area-delete";
    public final static String URI_PTN_NAVER_KWD_BRD = "eoad-naver-kwd-brd"; 
    public final static String URI_PTN_CUSTOM_RECO = "custom-reco";
    public final static String URI_PTN_CUSTOM_RECO_DETAIL = "custom-reco-detail";
    
}
