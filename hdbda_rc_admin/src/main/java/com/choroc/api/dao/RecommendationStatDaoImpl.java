package com.choroc.api.dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.choroc.api.dao.RecommendationStatDao;
import com.choroc.api.dto.ApiInfo;
import com.choroc.api.dto.RcEoadTagHhChannelStat;
import com.choroc.api.dto.RcEoadTagHhStat;
import com.choroc.api.dto.RcEoadTagStat;


/**
 * TODO >
 * 파일명   > AlgorithmDaoImpl.java
 * 작성일   > 2018. 11. 22.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
@Repository
public class RecommendationStatDaoImpl implements RecommendationStatDao {

	@Inject
	private SqlSession sqlSession;
	
	private static final String SQL_PATH_MAIN_DASHBOARD = "api.rcMainMapper.";
	private static final String SQL_PATH_ETL_LOG = "api.rcEtlLogMapper.";
	private static final String SQL_PATH_PERFORMANCE_RECO = "api.rcPerformanceRecoMapper.";
	
	/* RC_TAG_STAT 조회(실적) */
    public Map<String, Object> selectRcEoadTagStat(ApiInfo apiParam) throws SQLException{
    	Map<String, Object> map = new HashMap<>();
    	List<RcEoadTagStat>  list = new ArrayList<RcEoadTagStat>();
    	RcEoadTagStat todayMap = new RcEoadTagStat();
    	RcEoadTagStat yesterdayMap = new RcEoadTagStat();
    	
    	SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
    	Date date = null;
    	
    	try {
			date = format.parse(apiParam.getDate());
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	cal.add(Calendar.DATE, -1);
    	
    	String today = apiParam.getDate();
    	String yesterday = format.format(cal.getTime());
    	
    	
    	try {
    		 todayMap= sqlSession.selectOne(SQL_PATH_MAIN_DASHBOARD + "selectRcEoadTagStat", today);
    		 yesterdayMap = sqlSession.selectOne(SQL_PATH_MAIN_DASHBOARD + "selectRcEoadTagStat", yesterday);
    		 if(todayMap == null) {
    			 RcEoadTagStat todayDto = new RcEoadTagStat();
    			 todayDto.setYyyymmdd(today);
    			 todayMap = todayDto;
    		 }
    		 if(yesterdayMap== null) {
    			 RcEoadTagStat yesterdayDto = new RcEoadTagStat();
    			 yesterdayDto.setYyyymmdd(yesterday);
    			 yesterdayMap = yesterdayDto;
    		 }
    		 list.add(todayMap);
    		 list.add(yesterdayMap);
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	
    	
		int view_rate = 0;
		try {
			// 오늘
			view_rate = (list.get(0).getView_rc_cnt() / list.get(0).getView_cnt()) * 100;
			// 어제
			view_rate = (list.get(1).getView_rc_cnt() / list.get(1).getView_cnt()) * 100;
		}catch(Exception e) {
			view_rate = 0;
		}
    	
    	map.put("list", list);
    	return map;
    }
	
	
    /* rc_tag_stat 조회(차트) */
	public Map<String, Object> selectRcEoadTagStatChart(ApiInfo apiParam) throws SQLException{
		Map<String, Object> map = new HashMap<>();
		List<RcEoadTagStat> list = new ArrayList<RcEoadTagStat>();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date date = null;
		try {
			date = format.parse(apiParam.getDate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, -6);
		String firstDay = format.format(cal.getTime());
		
		try {
			for(int i=0; i<=6; i++) {
				RcEoadTagStat dto = new RcEoadTagStat();
				if(i == 0) {
					dto = sqlSession.selectOne(SQL_PATH_MAIN_DASHBOARD + "selectRcEoadTagStat", firstDay);
					if(dto == null) {
						RcEoadTagStat empty = new RcEoadTagStat();
						empty.setYyyymmdd(firstDay);
						dto = empty;
					}
				}else {
					cal.add(Calendar.DATE, +1);
					String day = format.format(cal.getTime());
					//System.out.println(day);
					dto= sqlSession.selectOne(SQL_PATH_MAIN_DASHBOARD + "selectRcEoadTagStat", day);
					if(dto == null) {
						RcEoadTagStat empty = new RcEoadTagStat();
						empty.setYyyymmdd(day);
						dto = empty;
					}
				}
				list.add(dto);
			}
		}catch(Exception e) {
			e.printStackTrace();
    	}
    	
		map.put("list", list);
		return map;
	}
	
	
	/* 추천 수집 분석 (일별/시간대별) 갯수 조회*/
	public Map<String,Object> selectDaysCount(ApiInfo apiParam) throws SQLException{
		Map<String, Object> map = new HashMap<String, Object>();
		int listCount = sqlSession.selectOne(SQL_PATH_ETL_LOG + "selectDaysCount", apiParam);
		map.put("listCount", listCount);
		return map;
	}
		
	public Map<String,Object> selectDaysList(ApiInfo apiParam) throws SQLException{
		List<RcEoadTagHhStat> dataList = null;
		Map<String, Object> map = new HashMap<String, Object>();
		dataList = sqlSession.selectList(SQL_PATH_ETL_LOG + "selectDaysList", apiParam);	
		map.put("list", dataList);
		return map; 
	}
	
	/* rc_tag_hh_stat 조회(차트) */
	public Map<String, Object> selectRcEoadTagStatChart2(ApiInfo apiParam) throws SQLException {
		
		Map<String, Object> map = new HashMap<>();
		List<RcEoadTagHhStat> list = new ArrayList<RcEoadTagHhStat>();
		RcEoadTagHhStat rcEoadTagHhStat = new RcEoadTagHhStat();
		
		if(apiParam.getDateType().equals("A")) {
			
			rcEoadTagHhStat.setStartDate(apiParam.getFirstYmd());
			rcEoadTagHhStat.setEndDate(apiParam.getLastYmd());
//			rcEoadTagHhStat.setStartDate(rcEoadTagHhStat.getDateFormat(apiParam.getFirstYmd()));
//			rcEoadTagHhStat.setEndDate(rcEoadTagHhStat.getDateFormat(apiParam.getLastYmd()));
			rcEoadTagHhStat.setDateType(apiParam.getDateType());
				    	
			list = sqlSession.selectList(SQL_PATH_ETL_LOG + "selectRcEoadTagHhStat", rcEoadTagHhStat);
			map.put("list", list);
		}
		else if(apiParam.getDateType().equals("B")) {
			
			rcEoadTagHhStat.setDate(apiParam.getDate());
//			rcEoadTagHhStat.setDate(rcEoadTagHhStat.getDateFormat(apiParam.getDate()));
			rcEoadTagHhStat.setDateType(apiParam.getDateType());
									
			list = sqlSession.selectList(SQL_PATH_ETL_LOG + "selectRcEoadTagHhStat", rcEoadTagHhStat);
			map.put("list", list);
		}
								
		return map;
	}
	
	public Map<String,Object> selectCateCount(ApiInfo apiParam) throws SQLException {
		Map<String, Object> map = new HashMap<String, Object>();
		int listCount = sqlSession.selectOne(SQL_PATH_ETL_LOG + "selectCateCount", apiParam);
		map.put("listCount", listCount);
		return map;
	}
	
	public Map<String,Object> selectCateList(ApiInfo apiParam) throws SQLException {
		List<RcEoadTagHhChannelStat> dataList = null;
		Map<String, Object> map = new HashMap<String, Object>();
		dataList = sqlSession.selectList(SQL_PATH_ETL_LOG + "selectCateList", apiParam);
		map.put("list", dataList);
		return map; 
	}
	
	/* rc_tag_hh_channel_stat 조회(차트) */
	public Map<String, Object> selectRcEoadTagStatChart3(ApiInfo apiParam) throws SQLException {
		
		Map<String, Object> map = new HashMap<>();
		List<RcEoadTagHhChannelStat> list = new ArrayList<RcEoadTagHhChannelStat>();
		RcEoadTagHhChannelStat rcEoadTagHhChannelStat = new RcEoadTagHhChannelStat();
		
		if(apiParam.getDateType().equals("A")) {
		
			rcEoadTagHhChannelStat.setStartDate(apiParam.getFirstYmd());
			rcEoadTagHhChannelStat.setEndDate(apiParam.getLastYmd());
//			rcEoadTagHhChannelStat.setStartDate(rcEoadTagHhChannelStat.getDateFormat(apiParam.getFirstYmd()));
//			rcEoadTagHhChannelStat.setEndDate(rcEoadTagHhChannelStat.getDateFormat(apiParam.getLastYmd()));
			rcEoadTagHhChannelStat.setDateType(apiParam.getDateType());	    		    
	    			    	
	    	list = sqlSession.selectList(SQL_PATH_ETL_LOG + "selectRcEoadTagHhChannelStat", rcEoadTagHhChannelStat);
			map.put("list", list);
		}
		else if(apiParam.getDateType().equals("B")) {
			
			rcEoadTagHhChannelStat.setDate(apiParam.getDate());
//			rcEoadTagHhChannelStat.setDate(rcEoadTagHhChannelStat.getDateFormat(apiParam.getDate()));
			rcEoadTagHhChannelStat.setDateType(apiParam.getDateType());
			
	    	list = sqlSession.selectList(SQL_PATH_ETL_LOG + "selectRcEoadTagHhChannelStat", rcEoadTagHhChannelStat);
			map.put("list", list);
		}
				
		return map;
	}

	
	
	// 추천영역
	public Map<String, Object> selectRecoAreaList(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();

		List selectList = sqlSession.selectList(SQL_PATH_PERFORMANCE_RECO + "selectRecoAreaList", apiParam);
		int selectListTotalCnt = sqlSession.selectOne(SQL_PATH_PERFORMANCE_RECO + "selectRecoAreaListTotalCnt",apiParam);
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", selectListTotalCnt);
		return mapObjTotal;
	}
	
	// 추천영역 전체
	public Map<String, Object> selectRecoAreaAllList(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();

		List selectList = sqlSession.selectList(SQL_PATH_PERFORMANCE_RECO + "selectRecoAreaListAll", apiParam);
		int selectListTotalCnt = sqlSession.selectOne(SQL_PATH_PERFORMANCE_RECO + "selectRecoAreaListTotalCntAll",apiParam);
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", selectListTotalCnt);
		return mapObjTotal;
	}
			
	// 추천영역2
	public Map<String, Object> selectRecoArea2List(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();

		List selectList = sqlSession.selectList(SQL_PATH_PERFORMANCE_RECO + "selectRecoArea2List", apiParam);
		int selectListTotalCnt = sqlSession.selectOne(SQL_PATH_PERFORMANCE_RECO + "selectRecoArea2ListTotalCnt",apiParam);
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", selectListTotalCnt);
		return mapObjTotal;
	}

	// 추천영역2 전체
	public Map<String, Object> selectRecoArea2AllList(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();

		List selectList = sqlSession.selectList(SQL_PATH_PERFORMANCE_RECO + "selectRecoArea2ListAll", apiParam);
		int selectListTotalCnt = sqlSession.selectOne(SQL_PATH_PERFORMANCE_RECO + "selectRecoArea2ListTotalCntAll",apiParam);
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", selectListTotalCnt);
		return mapObjTotal;
	}
	

}
