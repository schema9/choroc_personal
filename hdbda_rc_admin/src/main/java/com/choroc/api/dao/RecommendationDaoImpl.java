package com.choroc.api.dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.choroc.api.dao.RecommendationDao;
import com.choroc.api.dto.ApiInfo;
import com.choroc.api.dto.RcEoadTagHhChannelStat;
import com.choroc.api.dto.RcEoadTagHhStat;
import com.choroc.api.dto.RcEoadTagStat;


/**
 * TODO >
 * 파일명   > AlgorithmDaoImpl.java
 * 작성일   > 2018. 11. 22.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
@Repository
public class RecommendationDaoImpl implements RecommendationDao {

	@Inject
	private SqlSession sqlSession;
	
	private static final String SQL_PATH_AREA_PREVIEW = "api.rcAreaPreviewMapper.";
	
	// 알고리즘에서 카테고리 리스트 호출. 공통
	public Map<String,Object> selectCategoryList(ApiInfo apiParam) throws SQLException {
//		return DumpData.selectCategoryRaw(apiParam);
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();
		List selectList = null;
		String returnMsg = "";
		
		selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewCategory", apiParam);
		
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", "");
		mapObjTotal.put("returnMsg", returnMsg);
		return mapObjTotal;
	}
	
	//상품별 추천 상품 - 3댑스 리스트
//	public Map<String,Object> selectProductRecoProdList(ApiInfo apiParam) throws SQLException {
//		Map<String, Object> mapObjTotal = null;
//		mapObjTotal = new HashMap();
//		mapObjTotal = selectCacheItem(apiParam, "selectProductRecoProdAllList");
//		return mapObjTotal;
//	}
//	
	////상품별 추천 상품 - 3댑스 전체리스트
	//@Cacheable(cacheNames = "cache01Set", key = "#apiParam.tableNm +'|'+ #apiParam.itemId")
	public Map<String,Object> selectProductRecoProdAllList(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();
		List selectList = null;
		int totalCnt = 0;
		String returnMsg = "";
		
		if (apiParam.getTableNm() == "NaN" || apiParam.getTableNm().equals("NaN")) {	
    		returnMsg = " 604(tableNm이 존재하지 않음)";
    	}
		if (apiParam.getItemId() == "NaN" || apiParam.getItemId().equals("NaN")) {
    		returnMsg += " 605(itemId이 존재하지 않음)";
    	}
		
		
		switch (apiParam.getTableNm()) {
		
	        case "RC_CF_BUY" :
				selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemViewViewAll", apiParam);
				if(selectList != null) {
					totalCnt = selectList.size();
				}
	        	break;	        
		
	        case "RC_CF_VIEW" :
	        	selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemViewViewAll", apiParam);
//				selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemCfBuyMainAll", apiParam);
	        	if(selectList != null) {
	        		totalCnt = selectList.size();
	        	}	
	        	break;
	        	
	        case "RC_VIEW_VIEW" :
	        	selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemViewViewAll", apiParam);
//				selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemCfBuyCategoryAll", apiParam);
	        	if(selectList != null) {
	        		totalCnt = selectList.size();	
	        	}	        	
	        	break;
	        	

	        	
	        default:
	            try {
					throw new Exception("statistice not found algorithm");
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
		
		
//		if (apiParam.getTableNm().equals("ar_view_view")) selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemViewViewAll", apiParam);
//		if (apiParam.getTableNm().equals("ar_view_buy")) selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemViewBuyAll", apiParam);
//		if (apiParam.getTableNm().equals("ar_buy_buy")) selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemBuyBuyAll", apiParam);
//		if (apiParam.getTableNm().equals("ar_view_srh")) selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemViewSrhAll", apiParam);
//		totalCnt = selectList.size();
		
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", totalCnt);
		mapObjTotal.put("returnMsg", returnMsg);
		return mapObjTotal;
	}
	

	// 사용자별 추천 상품 - 3댑스 리스트
//	public Map<String, Object> selectUserRecoProdList(ApiInfo apiParam) throws SQLException {
//		Map<String, Object> mapObjTotal = null;
//		mapObjTotal = new HashMap();
//		mapObjTotal = selectCacheItem(apiParam, "selectUserRecoProdAllList");
//		return mapObjTotal;
//	}

	// 사용자별 추천 상품 - 3댑스 전체리스트
//	@Cacheable(cacheNames = "cache01Set", key = "#apiParam.tableNm")
//	@Cacheable(cacheNames = "cache01Set", key = "#apiParam.tableNm +'|'+ #apiParam.itemId")
	public Map<String, Object> selectUserRecoProdAllList(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();
		List selectList = null;
		int totalCnt = 0;
		String returnMsg = "";
		
		if (apiParam.getTableNm() == "NaN" || apiParam.getTableNm().equals("NaN")) {	
    		returnMsg = " 604(tableNm이 존재하지 않음)";
    	}
		if (apiParam.getItemId() == "NaN" || apiParam.getItemId().equals("NaN")) {
    		returnMsg += " 605(itemId이 존재하지 않음)";
    	}
		
		if (apiParam.getTableNm().equals("cf_user_buy")) selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selecRcCfBuyItemAll", apiParam);
		if (apiParam.getTableNm().equals("cf_user_srh")) selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selecRcCfSearchItemAll", apiParam);
		if (apiParam.getTableNm().equals("cf_user_view")) selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selecRcCfViewItemAll", apiParam);
		if (apiParam.getTableNm().equals("cf_user_buy_ie")) selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selecRcCfBuyIeItemAll", apiParam);
		if (apiParam.getTableNm().equals("cf_user_view_ie")) selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selecRcCfViewIeItemAll", apiParam);
		
		if(selectList != null) {
			totalCnt = selectList.size();	
		}
		
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", totalCnt);
		mapObjTotal.put("returnMsg", returnMsg);
		return mapObjTotal;
	}
				


	// 검색어별 추천 상품 - 3댑스 리스트
//	public Map<String, Object> selectSrhProductRecoProdList(ApiInfo apiParam) throws SQLException {
//		Map<String, Object> mapObjTotal = null;
//		mapObjTotal = new HashMap();
//		mapObjTotal = selectCacheItem(apiParam, "selectSrhProductRecoProdAllList");
//		return mapObjTotal;
//	}

	//// 검색어별 추천 상품 - 3댑스 전체리스트
	//@Cacheable(cacheNames = "cache01Set", key = "#apiParam.tableNm +'|'+ #apiParam.itemId")
    public Map<String, Object> selectSrhProductRecoProdAllList(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();
		List selectList = null;
		String getItemId = null;
		int totalCnt = 0;
		String returnMsg = "";
		
		if (apiParam.getTableNm() == "NaN" || apiParam.getTableNm().equals("NaN")) {	
    		returnMsg = " 604(tableNm이 존재하지 않음)";
    	}
		if (apiParam.getItemId() == "NaN" || apiParam.getItemId().equals("NaN")) {
    		returnMsg += " 605(itemId이 존재하지 않음)";
    	}
		
		if (apiParam.getTableNm().equals("ar_srh_view")) {
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemSrhViewAll", apiParam);
			if (selectList.size() == 0) {
				getItemId = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcViewNoSearch", apiParam);
				if (getItemId != null) {
					apiParam.setItemId(getItemId);
					selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemSrhViewAll", apiParam);
				}
			}
		}
		if (apiParam.getTableNm().equals("ar_srh_buy")) {
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemSrhBuyAll",
					apiParam);
			if (selectList.size() == 0) {
				getItemId = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcViewNoSearch", apiParam);
				if (getItemId != null) {
					apiParam.setItemId(getItemId);
					selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemSrhBuyAll", apiParam);
				}
			}
		}	

		if(selectList != null) {
			totalCnt = selectList.size();	
		}		
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", totalCnt);
		mapObjTotal.put("returnMsg", returnMsg);
		return mapObjTotal;
	}
	
		// 검색어별 추천 검색어 - 3댑스 리스트
//		public Map<String, Object> selectSrhWordRecoWordList(ApiInfo apiParam) throws SQLException {
//			Map<String, Object> mapObjTotal = null;
//			mapObjTotal = new HashMap();
//			mapObjTotal = selectCacheItem(apiParam, "selectSrhWordRecoProdAllList");
//			return mapObjTotal;
//		}

		//// 검색어별 추천 검색어 - 3댑스 전체리스트
		//@Cacheable(cacheNames = "cache01Set", key = "#apiParam.tableNm +'|'+ #apiParam.itemId")
		public Map<String, Object> selectSrhWordRecoWordAllList(ApiInfo apiParam) throws SQLException {
			Map<String, Object> mapObjTotal = null;
			mapObjTotal = new HashMap();
			List selectList = null;
			String getItemId = null;
			int totalCnt = 0;
			String returnMsg = "";
			
			if (apiParam.getTableNm() == "NaN" || apiParam.getTableNm().equals("NaN")) {	
	    		returnMsg = " 604(tableNm이 존재하지 않음)";
	    	}
			if (apiParam.getItemId() == "NaN" || apiParam.getItemId().equals("NaN")) {
	    		returnMsg += " 605(itemId이 존재하지 않음)";
	    	}

			if (apiParam.getTableNm().equals("ar_srh_srh")) {
				selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemSrhSrhAll",
						apiParam);
				if (selectList.size() == 0) {
					getItemId = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcViewNoSearch", apiParam);
					if (getItemId != null) {
						apiParam.setItemId(getItemId);
						selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemSrhSrhAll",
								apiParam);
					}
				}
			}
			
			if(selectList != null) {
				totalCnt = selectList.size();	
			}
			
			mapObjTotal.put("dataSet", selectList);
			mapObjTotal.put("totalCnt", totalCnt);
			mapObjTotal.put("returnMsg", returnMsg);
			return mapObjTotal;
		}
		
	//// 사용자정의별 추천 상품 - 3댑스 전체리스트
	public Map<String, Object> selectCustomRecoProdAllList(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();
		List selectList = null;
		int totalCnt = 0;
		String returnMsg = "";

		if (apiParam.getTableNm() == "NaN" || apiParam.getTableNm().equals("NaN")) {	
    		returnMsg = " 604(tableNm이 존재하지 않음)";
    	}
		if (apiParam.getItemId() == "NaN" || apiParam.getItemId().equals("NaN")) {
    		returnMsg += " 605(itemId이 존재하지 않음)";
    	}
		if (apiParam.getCategoryCd() == "NaN" || apiParam.getCategoryCd().equals("NaN")) {	
    		returnMsg = " 606(categoryCd이 존재하지 않음)";
    	}
		
		if (apiParam.getTableNm().equals("ah_view_view_catea")) selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemViewViewCateaAll", apiParam);
		if (apiParam.getTableNm().equals("ah_view_view_cateb")) selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProductItemViewViewCatebAll", apiParam);
		
		if(selectList != null) {
			totalCnt = selectList.size();	
		}
		
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", totalCnt);
		mapObjTotal.put("returnMsg", returnMsg);
		return mapObjTotal;
	}
	
	
	
	
	
	
	
	
	
	//사용자별 추천 상품 - 2댑스 리스트
	public Map<String,Object> selectUserRecoList(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();
		List selectList = null;
		int selectListTotalCnt = 0;
				
		if (apiParam.getCategoryCd().equals("cf_user_buy")) {
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcCfBuyDevice", apiParam);
			selectListTotalCnt = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcCfBuyDeviceTotalCnt", apiParam);
		}
		if (apiParam.getCategoryCd().equals("cf_user_srh")) {
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcCfSearchDevice", apiParam);
			selectListTotalCnt = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcCfSearchDeviceTotalCnt", apiParam);
		}
		if (apiParam.getCategoryCd().equals("cf_user_view")) {
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcCfViewDevice", apiParam);
			selectListTotalCnt = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcCfViewDeviceTotalCnt", apiParam);
		}
		if (apiParam.getCategoryCd().equals("cf_user_buy_ie")) {
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcCfBuyIeDevice", apiParam);
			selectListTotalCnt = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcCfBuyIeDeviceTotalCnt", apiParam);
		}
		if (apiParam.getCategoryCd().equals("cf_user_view_ie")) {
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcCfViewIeDevice", apiParam);
			selectListTotalCnt = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcCfViewIeDeviceTotalCnt", apiParam);
		}
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", selectListTotalCnt);
		return mapObjTotal;
	}
				
	// 상품별 추천 상품 - 2댑스 리스트
	public Map<String, Object> selectProductRecoList(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();
		List selectList = null;
		int selectListTotalCnt = 0;

		
		if("RC_CF_BUY".equals(apiParam.getCategoryCd())) {
			apiParam.setTableNm("RC_ITEM_CF_BUY");
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcCfProduct", apiParam);
//			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcCfBuyProduct", apiParam);
		} else if("RC_CF_VIEW".equals(apiParam.getCategoryCd())) {
			apiParam.setTableNm("RC_ITEM_CF_VIEW");
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcCfProduct", apiParam);
//			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcCfViewProduct", apiParam);
		} else {
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewProduct", apiParam);
		}
		
//		int selectListTotalCnt = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcViewProductTotalCnt",apiParam);
		
		if(selectList != null) {
			selectListTotalCnt = selectList.size();
		}
		
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", selectListTotalCnt);
		return mapObjTotal;
	}
	
	// 검색어별 추천 상품 - 2댑스 리스트
	public Map<String, Object> selectSrhProductRecoList(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();

		List selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewSearch", apiParam);
		int selectListTotalCnt = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcViewSearchTotalCnt",
				apiParam);
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", selectListTotalCnt);
		return mapObjTotal;
	}
	
// 검색어별 추천 검색어 - 2댑스 리스트
public Map<String, Object> selectSrhWordRecoList(ApiInfo apiParam) throws SQLException {
	Map<String, Object> mapObjTotal = null;
	mapObjTotal = new HashMap();

	List selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewSearch", apiParam);
	int selectListTotalCnt = sqlSession
			.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcViewSearchTotalCnt", apiParam);
	mapObjTotal.put("dataSet", selectList);
	mapObjTotal.put("totalCnt", selectListTotalCnt);
	return mapObjTotal;
}

//사용자정의별 추천 상품 - 2댑스 리스트
	public Map<String, Object> selectCustomRecoList(ApiInfo apiParam) throws SQLException {
		Map<String, Object> mapObjTotal = null;
		mapObjTotal = new HashMap();
		List selectList = null;
		int selectListTotalCnt = 0;

		if (apiParam.getCategoryCd().equals("ah_view_view_catea")) {
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewCateA", apiParam);
			selectListTotalCnt = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcViewCateATotalCnt", apiParam);
		}
		
		if (apiParam.getCategoryCd().equals("ah_view_view_cateb")) {
			selectList = sqlSession.selectList(SQL_PATH_AREA_PREVIEW + "selectRcViewCateB", apiParam);
			selectListTotalCnt = sqlSession.selectOne(SQL_PATH_AREA_PREVIEW + "selectRcViewCateBTotalCnt", apiParam);
		}
		
		mapObjTotal.put("dataSet", selectList);
		mapObjTotal.put("totalCnt", selectListTotalCnt);
		return mapObjTotal;
	}

}
