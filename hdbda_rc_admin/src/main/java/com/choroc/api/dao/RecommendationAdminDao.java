package com.choroc.api.dao;

import java.sql.SQLException;
import java.util.List;

import com.choroc.api.dto.ApiInfo;
import com.choroc.api.dto.RecommendAlgorithm;
import com.choroc.api.dto.RecommendArea;
import com.choroc.api.dto.RecommendException;
import com.choroc.api.dto.RecommendMd;


public interface RecommendationAdminDao {
	
	int selectRecommendAlgorithmCount(ApiInfo param) throws SQLException;
	
	List<RecommendAlgorithm> selectRecommendAlgorithmList(ApiInfo param) throws SQLException;

	String checkRecommendAlgorithm(ApiInfo param) throws SQLException;
	
	int insertRecommendAlgorithm(ApiInfo param) throws SQLException;
	
	RecommendAlgorithm selectOneRecommendAlgorithm(ApiInfo param) throws SQLException;
	
	String nmCheckRecommendAlgorithm(ApiInfo param) throws SQLException;

	int updateReommendAlgorithm(ApiInfo param) throws SQLException;
	
	int deleteRecommendAlgorithm(ApiInfo param) throws SQLException;
	
	int countRecommendArea(ApiInfo param) throws SQLException;
	
	List<RecommendArea> selectListRecommendArea(ApiInfo param) throws SQLException;
	
	List<RecommendArea> selectAlgoListRecommendArea(ApiInfo param) throws SQLException;
	
	int pkMaxRecommendArea() throws SQLException;
	
	int insertRecommendArea(ApiInfo param) throws SQLException;
	
	RecommendArea selectOneRecommendArea(ApiInfo param) throws SQLException;
	
	int modifyRecommendArea(ApiInfo param) throws SQLException;
	
	int deleteRecommendArea(ApiInfo param) throws SQLException;
	
	int selectRecommendExceptionCount(ApiInfo param) throws SQLException;
	
	List<RecommendException> selectRecommendExceptionList(ApiInfo param) throws SQLException;
	
	String insertRecommendException(RecommendException param) throws SQLException;
	
	int deleteRecommendException(RecommendException param) throws SQLException;	
	
	
	int selectRecommendMdCount(ApiInfo param) throws SQLException;
	
	List<RecommendMd> selectRecommendMdList(ApiInfo param) throws SQLException;
	
	String insertRecommendMd(RecommendMd param) throws SQLException;
	
	int deleteRecommendMd(RecommendMd param) throws SQLException;
	
	
	
}
