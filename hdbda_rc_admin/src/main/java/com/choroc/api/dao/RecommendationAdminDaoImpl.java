package com.choroc.api.dao;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.choroc.api.dto.ApiInfo;
import com.choroc.api.dto.RecommendAlgorithm;
import com.choroc.api.dto.RecommendArea;
import com.choroc.api.dto.RecommendException;
import com.choroc.api.dto.RecommendMd;

/**
 * TODO >
 * 파일명   > AlgorithmDaoImpl.java
 * 작성일   > 2018. 11. 22.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
@Repository
public class RecommendationAdminDaoImpl implements RecommendationAdminDao {

	@Inject
	private SqlSession sqlSession;
	
	private static final String SQL_PATH_ALGORITHM 				= "api.rcAlgorithmMapper.";
	private static final String SQL_PATH_AREA 					= "api.rcAreaMapper.";
	private static final String SQL_PATH_EXCEPTION_MANAGEMENT 	= "api.rcExceptionManagementMapper.";
	private static final String SQL_PATH_MD_MANAGEMENT 			= "api.rcMdManagementMapper.";
	
    // 추천 제외관리 리스트 총 갯수
    public int selectRecommendExceptionCount(ApiInfo apiParam) throws SQLException {
    	return sqlSession.selectOne(SQL_PATH_EXCEPTION_MANAGEMENT + "selectExceptionTotalCount", apiParam);
    }
    
    // 추천 제외관리 리스트 목록
    public List<RecommendException> selectRecommendExceptionList(ApiInfo apiParam) throws SQLException {
    	return sqlSession.selectList(SQL_PATH_EXCEPTION_MANAGEMENT + "selectExceptionList", apiParam);
    }
    
    // 추천 제외관리 입력
    public String insertRecommendException(RecommendException param) throws SQLException{
    	String msg = null;
		List<String> duplList = new ArrayList<String>();
		List<String> passList = new ArrayList<String>();
		StringBuilder duplStr = new StringBuilder(" 는 중복된 코드입니다.");
		StringBuilder passStr = new StringBuilder(" 는 사용 가능한 코드입니다.");
		String[] beforeArray = param.getContent_cd().split(",");
		String empty = "";
		
		// DB에 중복된 값이 있는지 확인
		for(int i=0; i<beforeArray.length; i++) {
			// 입력 값 중에 공백값이 있으면
			if(beforeArray[i].trim().equals(empty)) {
				msg = "공백값은 입력할 수 없습니다";
				return msg;
			}else {
				RecommendException daoParam = new RecommendException();
				daoParam.setContent_cd(beforeArray[i].replace(" ", ""));
				daoParam.setSortation(param.getSortation());
				int result = sqlSession.selectOne(SQL_PATH_EXCEPTION_MANAGEMENT + "checkException", daoParam);
				if(result == 1) {
					duplList.add(beforeArray[i].replace(" ", ""));
				}else {
					passList.add(beforeArray[i].replace(" ", ""));
				}
			}
		}
		
		// 중복된 코드가 있으면 
		if(duplList.size() != 0) {
			for(int i=0; i<duplList.size(); i++) {
				// 두개 이상이면 콤마 추가
				if(i >=1 ) {
					duplStr.insert(0, ", ");
				}
				String duplCd = String.format("[%s]", duplList.get(i));
				duplStr.insert(0, duplCd);
			}
			msg = duplStr.toString();
		// 중복된 코드가 없으면
		}else if(duplList.size() == 0){
			for(int i=0; i<passList.size(); i++) {
				// 두개 이상이면 콤마 추가
				if(i >=1 ) {
					passStr.insert(0, ", ");
				}
				String passCd = String.format("[%s]", passList.get(i));
				passStr.insert(0, passCd);
			}
			/*msg = passStr.toString();*/
			msg = "등록 가능한 코드입니다.";
		}
    	
    	int result =0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		int insertSize = passList.size();
		
		if(passList.size() == 0 ) {
			msg = "입력한 값이 모두 중복입니다.";
			return msg;
		}else {
			for(int i=0; i<passList.size(); i++) {
				RecommendException dto = new RecommendException();
				dto.setContent_cd(passList.get(i));
				dto.setSortation(param.getSortation());
				dto.setCreate_id(param.getCreate_id());
				dto.setCreate_dt(sdf.format(d));
				int rs = sqlSession.insert(SQL_PATH_EXCEPTION_MANAGEMENT + "insertException", dto);
				result += rs;
			}
			result = insertSize + result;
			if(result == 0 ) {
				msg = "관리자에게 문의 바랍니다.";
			}else {
				msg = "처리되었습니다.";
			}
			return msg;
		}
	}
    
    // 추천 제외관리 삭제
    public int deleteRecommendException(RecommendException param) throws SQLException{
		int result = 0;
		result = sqlSession.delete(SQL_PATH_EXCEPTION_MANAGEMENT + "deleteException", param);
		return result;
	}
    
    //추천 알고리즘 리스트 총 갯수
    public int selectRecommendAlgorithmCount(ApiInfo apiParam) throws SQLException {
    	int result = 0;
    	result = sqlSession.selectOne(SQL_PATH_ALGORITHM + "selectCount", apiParam);
    	return result;
    }
    
    // 추천 알고리즘 리스트
    public List<RecommendAlgorithm> selectRecommendAlgorithmList (ApiInfo apiParam) throws SQLException {
    	return sqlSession.selectList(SQL_PATH_ALGORITHM + "selectList", apiParam);
    }
    
    // 추천 알고리즘, 알고리즘 명 체크
    public String checkRecommendAlgorithm (ApiInfo apiParam) throws SQLException {
    	String msg = null;
			int result = sqlSession.selectOne(SQL_PATH_ALGORITHM + "pkCheck" , apiParam);
			if (result == 0) {
				msg = "사용 가능한 추천 알고리즘 입니다.";
			}else {
				msg = "이미 등록된 '추천 알고리즘' 입니다.";
			}
		return msg;
    }
    
    // 추천 알고리즘 입력
    public int insertRecommendAlgorithm (ApiInfo apiParam) throws SQLException {
    	RecommendAlgorithm param = new RecommendAlgorithm();
    	param = apiParam.getRc_algorithm();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		param.setCreate_dt(sdf.format(d));
    	return sqlSession.insert(SQL_PATH_ALGORITHM + "insert", param);
    }
    
    // 추천 알고리즘 수정 데이터 1개 가져오기
    public RecommendAlgorithm selectOneRecommendAlgorithm(ApiInfo apiParam) throws SQLException {
    	RecommendAlgorithm result = new RecommendAlgorithm();
    	result = sqlSession.selectOne(SQL_PATH_ALGORITHM + "selectOne", apiParam);
    	return result;
    }
    
    // 추천 알고리즘 수정 - 알고리즘명 체크
    public String nmCheckRecommendAlgorithm (ApiInfo apiParam) throws SQLException {
    	String msg = null;
		int result = sqlSession.selectOne(SQL_PATH_ALGORITHM + "nmCheck", apiParam.getRc_algorithm()); 
		if (result >= 1) {
			msg = "이미 등록된 '추천 알고리즘 명' 입니다.";
		} else {
			msg = "사용 가능한 추천 알고리즘 명입니다.";
		}
		return msg;
    }
    
    // 추천 알고리즘 수정
    public int updateReommendAlgorithm (ApiInfo apiParam) throws SQLException{
    	int result = 0;
    	RecommendAlgorithm param = new RecommendAlgorithm();
    	param = apiParam.getRc_algorithm();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		param.setModify_dt(sdf.format(d));
    	result = sqlSession.update(SQL_PATH_ALGORITHM + "update", param);
    	return result;
    }
    
    // 추천 알고리즘 삭제
    public int deleteRecommendAlgorithm (ApiInfo apiParam) throws SQLException{
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
    	apiParam.getRc_algorithm().setModify_dt(sdf.format(d));
    	int result = sqlSession.update(SQL_PATH_ALGORITHM + "delete", apiParam.getRc_algorithm());
    	return result;
    }
    
    // 추천영역 리스트 총 갯수
    public int countRecommendArea (ApiInfo apiParam) throws SQLException{
    	return sqlSession.selectOne(SQL_PATH_AREA + "Count", apiParam);
    }
    
    // 추천영역 리스트
    public List<RecommendArea> selectListRecommendArea (ApiInfo apiParam) throws SQLException{
    	return sqlSession.selectList(SQL_PATH_AREA + "List", apiParam);
    }
    
    // 추천영역 입력팝업 select태그 가져오기
    public List<RecommendArea> selectAlgoListRecommendArea (ApiInfo apiParam) throws SQLException{
    	return sqlSession.selectList(SQL_PATH_AREA + "algoList", apiParam);
    }
    
    // 추천영역 등록팝업 pge_area_id 최대값 가져오기
    public int pkMaxRecommendArea () throws SQLException{
    	return sqlSession.selectOne(SQL_PATH_AREA + "totalCountPlusOne");
    }
    
    // 추천영역 등록
    public int insertRecommendArea (ApiInfo apiParam) throws SQLException{
    	RecommendArea rc_area = apiParam.getRc_area();
    	rc_area.setDel_yn("N");
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		rc_area.setCreate_dt(sdf.format(d));
    	return sqlSession.insert(SQL_PATH_AREA + "insert", rc_area);
    }
    
    // 추천영역 수정화면 (수정 할 데이터1개 가져오기)
   public RecommendArea selectOneRecommendArea (ApiInfo apiParam) throws SQLException {
	   return sqlSession.selectOne(SQL_PATH_AREA + "modifySelectOne", apiParam.getRc_area());
   }
    
    // 추천영역 수정
   public int modifyRecommendArea (ApiInfo apiParam) throws SQLException{
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	   Date d = new Date();
	   apiParam.getRc_area().setModify_dt(sdf.format(d));
	   return sqlSession.update(SQL_PATH_AREA + "modify", apiParam.getRc_area());
   }
   
   // 추천영역 삭제 - del_yn = 'Y'로 변경
   public int deleteRecommendArea (ApiInfo apiParam) throws SQLException {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	   Date d = new Date();
	   apiParam.getRc_area().setModify_dt(sdf.format(d));
	   return sqlSession.update(SQL_PATH_AREA + "delete", apiParam.getRc_area());
   }
   
   // 추천 MD관리 리스트 총 갯수
   public int selectRecommendMdCount(ApiInfo apiParam) throws SQLException {
   	return sqlSession.selectOne(SQL_PATH_MD_MANAGEMENT + "selectMdTotalCount", apiParam);
   }
   
   // 추천 MD관리 리스트 목록
   public List<RecommendMd> selectRecommendMdList(ApiInfo apiParam) throws SQLException {
   	return sqlSession.selectList(SQL_PATH_MD_MANAGEMENT + "selectMdList", apiParam);
   }
   
   // 추천 MD관리 입력
   public String insertRecommendMd(RecommendMd param) throws SQLException{
   	String msg = null;
		List<String> duplList = new ArrayList<String>();
		List<String> passList = new ArrayList<String>();
		StringBuilder duplStr = new StringBuilder(" 는 중복된 코드입니다.");
		StringBuilder passStr = new StringBuilder(" 는 사용 가능한 코드입니다.");
		String[] beforeArray = param.getContent_cd().split(",");
		String empty = "";
		
		// DB에 중복된 값이 있는지 확인
		for(int i=0; i<beforeArray.length; i++) {
			// 입력 값 중에 공백값이 있으면
			if(beforeArray[i].trim().equals(empty)) {
				msg = "공백값은 입력할 수 없습니다";
				return msg;
			}else {
				RecommendException daoParam = new RecommendException();
				daoParam.setContent_cd(beforeArray[i].replace(" ", ""));
				daoParam.setSortation(param.getSortation());
				int result = sqlSession.selectOne(SQL_PATH_MD_MANAGEMENT + "checkMd", daoParam);
				if(result == 1) {
					duplList.add(beforeArray[i].replace(" ", ""));
				}else {
					passList.add(beforeArray[i].replace(" ", ""));
				}
			}
		}
		
		// 중복된 코드가 있으면 
		if(duplList.size() != 0) {
			for(int i=0; i<duplList.size(); i++) {
				// 두개 이상이면 콤마 추가
				if(i >=1 ) {
					duplStr.insert(0, ", ");
				}
				String duplCd = String.format("[%s]", duplList.get(i));
				duplStr.insert(0, duplCd);
			}
			msg = duplStr.toString();
		// 중복된 코드가 없으면
		}else if(duplList.size() == 0){
			for(int i=0; i<passList.size(); i++) {
				// 두개 이상이면 콤마 추가
				if(i >=1 ) {
					passStr.insert(0, ", ");
				}
				String passCd = String.format("[%s]", passList.get(i));
				passStr.insert(0, passCd);
			}
			/*msg = passStr.toString();*/
			msg = "등록 가능한 코드입니다.";
		}
   	
   	int result =0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		int insertSize = passList.size();
		
		if(passList.size() == 0 ) {
			msg = "입력한 값이 모두 중복입니다.";
			return msg;
		}else {
			for(int i=0; i<passList.size(); i++) {
				RecommendException dto = new RecommendException();
				dto.setContent_cd(passList.get(i));
				dto.setSortation(param.getSortation());
				dto.setCreate_id(param.getCreate_id());
				dto.setCreate_dt(sdf.format(d));
				int rs = sqlSession.insert(SQL_PATH_MD_MANAGEMENT + "insertMd", dto);
				result += rs;
			}
			result = insertSize + result;
			if(result == 0 ) {
				msg = "관리자에게 문의 바랍니다.";
			}else {
				msg = "처리되었습니다.";
			}
			return msg;
		}
	}
   
   // 추천 제외관리 삭제
   public int deleteRecommendMd(RecommendMd param) throws SQLException{
		int result = 0;
		result = sqlSession.delete(SQL_PATH_MD_MANAGEMENT + "deleteMd", param);
		return result;
	}
}
