package com.choroc.api.dao;

import java.sql.SQLException;
import java.util.Map;

import com.choroc.api.dto.ApiInfo;

public interface RecommendationDao {
	
	Map<String,Object> selectCategoryList(ApiInfo apiParam) throws SQLException;
	
	Map<String,Object> selectProductRecoProdAllList(ApiInfo apiParam) throws SQLException;
	
	
	
	Map<String,Object> selectUserRecoProdAllList(ApiInfo apiParam) throws SQLException;
	
//	Map<String,Object> selectUserRecoProdList(ApiInfo apiParam) throws SQLException;
	
//	Map<String,Object> selectProductRecoProdList(ApiInfo apiParam) throws SQLException;
		
//	Map<String,Object> selectSrhProductRecoProdList(ApiInfo apiParam) throws SQLException;
	
	Map<String,Object> selectSrhProductRecoProdAllList(ApiInfo apiParam) throws SQLException;
	
//	Map<String,Object> selectSrhWordRecoWordList(ApiInfo apiParam) throws SQLException;
	
	Map<String,Object> selectSrhWordRecoWordAllList(ApiInfo apiParam) throws SQLException;
	
	Map<String,Object> selectCustomRecoProdAllList(ApiInfo apiParam) throws SQLException;
	
	
	Map<String,Object> selectUserRecoList(ApiInfo apiParam) throws SQLException;
	
	Map<String,Object> selectProductRecoList(ApiInfo apiParam) throws SQLException;
	
	Map<String,Object> selectSrhProductRecoList(ApiInfo apiParam) throws SQLException;
	
	Map<String,Object> selectSrhWordRecoList(ApiInfo apiParam) throws SQLException;
	
	Map<String,Object> selectCustomRecoList(ApiInfo apiParam) throws SQLException;
	
}
