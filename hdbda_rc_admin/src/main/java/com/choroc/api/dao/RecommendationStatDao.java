package com.choroc.api.dao;

import java.sql.SQLException;
import java.util.Map;

import com.choroc.api.dto.ApiInfo;

public interface RecommendationStatDao {
	
	Map<String,Object> selectRcEoadTagStat(ApiInfo param) throws SQLException;
	
	Map<String,Object> selectRcEoadTagStatChart(ApiInfo param) throws SQLException;

	// recommend
	Map<String,Object> selectDaysCount(ApiInfo param) throws SQLException;
	
	Map<String,Object> selectDaysList(ApiInfo param) throws SQLException;
	
	Map<String,Object> selectRcEoadTagStatChart2(ApiInfo param) throws SQLException;
	
	Map<String,Object> selectCateCount(ApiInfo param) throws SQLException;
	
	Map<String,Object> selectCateList(ApiInfo param) throws SQLException;
	
	Map<String,Object> selectRcEoadTagStatChart3(ApiInfo param) throws SQLException;
	
	// performance
	Map<String,Object> selectRecoAreaList(ApiInfo param) throws SQLException;
	
	Map<String,Object> selectRecoAreaAllList(ApiInfo param) throws SQLException;
	
	Map<String,Object> selectRecoArea2List(ApiInfo param) throws SQLException;
	
	Map<String,Object> selectRecoArea2AllList(ApiInfo param) throws SQLException;
	
	
	
	
	
}
