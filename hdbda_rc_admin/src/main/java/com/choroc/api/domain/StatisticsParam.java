package com.choroc.api.domain;

import java.util.Arrays;

import org.springframework.util.StringUtils;

public class StatisticsParam {

    private String api;
    private String domain;
    private String serviceId;
    private String eventType;
    private String period;
    private String accessKey;
    private String pageArea;
    private String[] pageAreaTokens;
    private String algorithmSet;
    private String[] algorithmSetTokens;
    private String algorithm;
    private String[] algorithmTokens;
    private String itemId;
    private String site;
    private String startDate;
    private String date;
    private String endDate;
    private String callback;
    private String searchText;
    private String searchOption;
    private String listOrder;
    private String itemStatus;
    private String time;
    private String areaId;
    private String[] areaIdTokens;
    private int dsplyNo;
    private int dsplyCnt;
    private String orderNm;
    private String orderAscDesc;
    private String dateType;
    private String remoteHost;
    private String[] serviceIdTokens;
    private String masterDb;
    private int mainAreaId;
    private int firstIndexInt;
    private int lastIndexInt;

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String[] getPageAreaTokens() {
        return pageAreaTokens;
    }
    public void setPageAreaTokens(String[] pageAreaTokens) {
        this.pageAreaTokens = pageAreaTokens;
    }
    public String getPageArea() {
        return pageArea;
    }
    public void setPageArea(String pageArea) {
        this.pageArea = pageArea;
        if(!StringUtils.isEmpty(pageArea)){
            pageArea = pageArea.replaceAll(" ", "");
            this.pageAreaTokens = pageArea.split(",");
        }
    }
    public String getEventType() {
        return eventType;
    }
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
    public String getApi() {
        return api;
    }
    public void setApi(String api) {
        this.api = api;
    }
    public String getDomain() {
        return domain;
    }
    public void setDomain(String domain) {
        this.domain = domain;
    }
    public String getServiceId() {
        return serviceId;
    }
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
    public String getPeriod() {
        return period;
    }
    public void setPeriod(String period) {
        this.period = period;
    }
    public String getAccessKey() {
        return accessKey;
    }
    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }
    public String getItemId() {
        return itemId;
    }
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
    public String getSite() {
        return site;
    }
    public void setSite(String site) {
        this.site = site;
    }
    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    public String getCallback() {
        return callback;
    }
    public void setCallback(String callback) {
        this.callback = callback;
    }
    public String getSearchText() {
        return searchText;
    }
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }
    public String getSearchOption() {
        return searchOption;
    }
    public void setSearchOption(String searchOption) {
        this.searchOption = searchOption;
    }
    public String getListOrder() {
        return listOrder;
    }
    public void setListOrder(String listOrder) {
        this.listOrder = listOrder;
    }
    public String getItemStatus() {
        return itemStatus;
    }
    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public String getAreaId() {return areaId;}
    public void setAreaId(String areaId) {this.areaId = areaId;}
    public int getDsplyNo() {
        return dsplyNo;
    }
    public void setDsplyNo(int dsplyNo) {
        this.dsplyNo = dsplyNo;
    }
    public int getDsplyCnt() {
        return dsplyCnt;
    }
    public void setDsplyCnt(int dsplyCnt) {
        this.dsplyCnt = dsplyCnt;
    }
    public String getOrderNm() {
        return orderNm;
    }
    public void setOrderNm(String orderNm) {
        this.orderNm = orderNm;
    }
    public String getOrderAscDesc() {
        return orderAscDesc;
    }
    public void setOrderAscDesc(String orderAscDesc) {
        this.orderAscDesc = orderAscDesc;
    }
    public String[] getAreaIdTokens() {return areaIdTokens;}
    public void setAreaIdTokens(String[] areaIdTokens) {this.areaIdTokens = areaIdTokens;}
    public String getDateType() {
        return dateType;
    }
    public void setDateType(String dateType) {
        this.dateType = dateType;
    }
    public String getRemoteHost() {
        return remoteHost;
    }
    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }
    public String[] getServiceIdTokens() {
        return serviceIdTokens;
    }
    public void setServiceIdTokens(String[] serviceIdTokens) {
        this.serviceIdTokens = serviceIdTokens;
    }
    
    public int getFirstIndexInt() {
		return firstIndexInt;
	}
	public void setFirstIndexInt(int firstIndexInt) {
		this.firstIndexInt = firstIndexInt;
	}
	public int getLastIndexInt() {
		return lastIndexInt;
	}
	public void setLastIndexInt(int lastIndexInt) {
		this.lastIndexInt = lastIndexInt;
	}
	public String getMasterDb() {return masterDb;}
    public void setMasterDb(String masterDb) {this.masterDb = masterDb;}
    public int getMainAreaId() {return mainAreaId;}
    public void setMainAreaId(int mainAreaId) {this.mainAreaId = mainAreaId;}

    /**
     * @return the algorithmSet
     */
    public String getAlgorithmSet() {
        return algorithmSet;
    }
    /**
     * @param algorithmSet the algorithmSet to set
     */
    public void setAlgorithmSet(String algorithmSet) {
        this.algorithmSet = algorithmSet;
        if(!StringUtils.isEmpty(algorithmSet)){
            this.algorithmSet = algorithmSet.replaceAll(" ", "");
            this.algorithmSetTokens = algorithmSet.split(",");
        }
    }
    /**
     * @return the algorithmSetTokens
     */
    public String[] getAlgorithmSetTokens() {
        return algorithmSetTokens;
    }
    /**
     * @param algorithmSetTokens the algorithmSetTokens to set
     */
    public void setAlgorithmSetTokens(String[] algorithmSetTokens) {
        this.algorithmSetTokens = algorithmSetTokens;
    }
    /**
     * @return the algorithm
     */
    public String getAlgorithm() {
        return algorithm;
    }
    /**
     * @param algorithm the algorithm to set
     */
    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
        if(!StringUtils.isEmpty(algorithm)){
            this.algorithm = algorithm.replaceAll(" ", "");
            this.algorithmTokens = algorithm.split(",");
        }
    }
    /**
     * @return the algorithmTokens
     */
    public String[] getAlgorithmTokens() {
        return algorithmTokens;
    }
    /**
     * @param algorithmTokens the algorithmTokens to set
     */
    public void setAlgorithmTokens(String[] algorithmTokens) {
        this.algorithmTokens = algorithmTokens;
    }
    @Override
    public String toString() {
        return "StatisticsParam [api=" + api + ", domain=" + domain
                + ", serviceId=" + serviceId + ", eventType=" + eventType
                + ", period=" + period + ", accessKey=" + accessKey
                + ", pageArea=" + pageArea + ", pageAreaTokens="
                + Arrays.toString(pageAreaTokens) + ", algorithmSet="
                + algorithmSet + ", algorithmSetTokens="
                + Arrays.toString(algorithmSetTokens) + ", algorithm="
                + algorithm + ", algorithmTokens="
                + Arrays.toString(algorithmTokens) + ", itemId=" + itemId
                + ", site=" + site + ", startDate="
                + startDate + ", date=" + date + ", endDate=" + endDate
                + ", callback=" + callback + "]";
    }


}