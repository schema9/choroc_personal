package com.choroc.api.service;

import java.util.Map;

import com.choroc.api.dto.ApiInfo;

public interface RecommendationStatService {

	//추천수집분석
	Map<String, Object> recommend(ApiInfo apiParam) throws Exception;
	
	//추천성과분석 - 추천성과
	Map<String, Object> performance(ApiInfo apiParam);
	
	//추천성과분석 - 메인페이지 그래프
	Map<String, Object> index(ApiInfo apiParam);
		
	/* 추천 수집 분석 (일별/시간대별) 갯수 조회*/
	//int selectDaysCount(StatisticsParam param);
	
	/* 추천 수집 분석 (일별/시간대별) 리스트 조회*/
	//List<RcTagHhStat> selectCollection1List(StatisticsParam param);
		
	/* 추천 수집 분석 (일별/시간대별 항목별) 갯수 조회*/
	//int selectCollection2Count(RcTagHhChannelStat rcTagHhChannelStat);	
	
	/* 추천 수집 분석 (일별/시간대별 항목별) 리스트 조회*/
	//List<RcTagHhChannelStat> selectCollection2List(StatisticsParam param);
	
	/* 추천 성과 (일별/시간대별) 갯수 조회*/
	//int selectCollection3Count(RcTagHhStat rcTagHhStat);
	
	/* 추천 성과 (일별/시간대별) 리스트 조회*/
	//List<RcTagStat> selectCollection3List(StatisticsParam param);
	
	
	/* 추천 수집 분석 (일별/시간대별) 차트 */
	//List<Map<String, Object>> selectCollectionChart1(Map<String, Object> paramMap);
	
}
