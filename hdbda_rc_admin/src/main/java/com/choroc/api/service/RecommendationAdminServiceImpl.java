package com.choroc.api.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.choroc.api.Constants;
import com.choroc.api.dao.RecommendationAdminDao;
import com.choroc.api.dto.ApiInfo;
import com.choroc.api.dto.RecommendAlgorithm;
import com.choroc.api.dto.RecommendArea;
import com.choroc.api.dto.RecommendException;
import com.choroc.api.dto.RecommendMd;

@Service
public class RecommendationAdminServiceImpl  implements RecommendationAdminService {

	@Inject
	private RecommendationAdminDao recommendationAdminDao;
//	
	private static Logger log = LoggerFactory.getLogger(RecommendationAdminServiceImpl.class);
		
	//추천 환경관리 - 알고리즘 관리, 추천영역 관리, 추천제외관리
	@Transactional
	public Map<String, Object> manager(ApiInfo apiParam) {
		String api = apiParam.getApi();
		Map<String, Object> rtnMapVal = new HashMap<String, Object>();
		switch (api) {
		case Constants.URI_PTN_RECOMMEND_ALGORITHM_COUNT:
			try {
				int result = recommendationAdminDao.selectRecommendAlgorithmCount(apiParam);
				rtnMapVal.put("totalCount", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_ALGORITHM_LIST:
			try {
				List<RecommendAlgorithm> list = new ArrayList<RecommendAlgorithm>();
				list = recommendationAdminDao.selectRecommendAlgorithmList(apiParam);
				rtnMapVal.put("list", list);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_ALGORITHM_PK_CHECK:
			try {
				String result = recommendationAdminDao.checkRecommendAlgorithm(apiParam);
				rtnMapVal.put("result", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_ALGORITHM_INSERT:
			try {
				int result = recommendationAdminDao.insertRecommendAlgorithm(apiParam);
				rtnMapVal.put("result", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_ALGORITHM_SELECTONE:
			try {
				RecommendAlgorithm rc_algorithm = new RecommendAlgorithm();
				rc_algorithm = recommendationAdminDao.selectOneRecommendAlgorithm(apiParam);
				rtnMapVal.put("result", rc_algorithm);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_ALGORITHM_NM_CHECK:
			try {
				String result = recommendationAdminDao.nmCheckRecommendAlgorithm(apiParam);
				rtnMapVal.put("result", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_ALGORITHM_UPDATE:
			try {
				int result = recommendationAdminDao.updateReommendAlgorithm(apiParam);
				rtnMapVal.put("result", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_ALGORITHM_DELETE:
			try {
				int result = recommendationAdminDao.deleteRecommendAlgorithm(apiParam);
				rtnMapVal.put("result", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_AREA_COUNT:
			try {
				int result = recommendationAdminDao.countRecommendArea(apiParam);
				rtnMapVal.put("totalCount", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_AREA_LIST:
			try {
				List<RecommendArea> list = recommendationAdminDao.selectListRecommendArea(apiParam);
				rtnMapVal.put("list", list);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_AREA_INSERT_ALGO:
			try {
				List<RecommendArea> list = recommendationAdminDao.selectAlgoListRecommendArea(apiParam);
				int pkMax = recommendationAdminDao.pkMaxRecommendArea(); 
				rtnMapVal.put("list", list);
				rtnMapVal.put("pkMax", pkMax);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_AREA_INSERT:
			try {
				int result = recommendationAdminDao.insertRecommendArea(apiParam);
				rtnMapVal.put("result", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_AREA_UPDATE_ALGO:
			try {
				List<RecommendArea> list = recommendationAdminDao.selectAlgoListRecommendArea(apiParam);
				rtnMapVal.put("list", list);
				RecommendArea rc_area = recommendationAdminDao.selectOneRecommendArea(apiParam);
				rtnMapVal.put("result", rc_area);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_AREA_UPDATE:
			try {
				int result = recommendationAdminDao.modifyRecommendArea(apiParam);
				rtnMapVal.put("result", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_AREA_DELETE:
			try {
				int result = recommendationAdminDao.deleteRecommendArea(apiParam);
				rtnMapVal.put("result", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;	
		case Constants.URI_PTN_RECOMMEND_EXCEPTION_MANAGEMENT_LIST:  // 추천제외관리 리스트
			try {
				int result = recommendationAdminDao.selectRecommendExceptionCount(apiParam);
				rtnMapVal.put("totalCount", result);
				List<RecommendException>  list = new ArrayList<RecommendException>();
				list = recommendationAdminDao.selectRecommendExceptionList(apiParam);
				
				rtnMapVal.put("list", list);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_EXCEPTION_MANAGEMENT_INSERT:
			try {
				RecommendException param = new RecommendException();
				param = apiParam.getRc_except();
				String result = recommendationAdminDao.insertRecommendException(param);
				rtnMapVal.put("result", result);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_EXCEPTION_MANAGEMENT_DELETE:	
			try {
				RecommendException param = new RecommendException();
				param = apiParam.getRc_except();
				int result = recommendationAdminDao.deleteRecommendException(param);
				rtnMapVal.put("result", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_MD_MANAGEMENT_LIST:  // 추천MD관리 리스트
			try {
				int result = recommendationAdminDao.selectRecommendMdCount(apiParam);
				rtnMapVal.put("totalCount", result);
				List<RecommendMd>  list = new ArrayList<RecommendMd>();
				list = recommendationAdminDao.selectRecommendMdList(apiParam);
				
				rtnMapVal.put("list", list);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_MD_MANAGEMENT_INSERT:
			try {
				RecommendMd param = new RecommendMd();
				param = apiParam.getRc_md();
				String result = recommendationAdminDao.insertRecommendMd(param);
				rtnMapVal.put("result", result);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_RECOMMEND_MD_MANAGEMENT_DELETE:	
			try {
				RecommendMd param = new RecommendMd();
				param = apiParam.getRc_md();
				int result = recommendationAdminDao.deleteRecommendMd(param);
				rtnMapVal.put("result", result);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		default:
			try {
				throw new Exception("statistice not found api");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return rtnMapVal;
	}
	
	
	
	
}
