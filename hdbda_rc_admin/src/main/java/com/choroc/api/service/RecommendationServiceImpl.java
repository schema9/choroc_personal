package com.choroc.api.service;

import java.sql.SQLException;
import java.util.Map;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.choroc.api.Constants;
import com.choroc.api.dao.RecommendationDao;
import com.choroc.api.dto.ApiInfo;

@Service
public class RecommendationServiceImpl  implements RecommendationService {

	@Inject
	private RecommendationDao recommendationDao;
//	
	private static Logger log = LoggerFactory.getLogger(RecommendationServiceImpl.class);
	
	//추천 환경관리 - 추천영역 미리보기
	@Transactional
	public Map<String,Object> areaPreview(ApiInfo apiParam) {
		String api = apiParam.getApi();
		Map<String, Object> rtnMapVal = null;
		switch (api) {
	
        /*공용코드용 api셋*/
        case Constants.URI_PTN_CATEGORY_LIST:
        	try {
        		rtnMapVal = recommendationDao.selectCategoryList(apiParam);
        	} catch (SQLException e) {
    			e.printStackTrace();
    		}
        break;
        
        /*상품별 추천 상품*/
//      case Constants.URI_PTN_PRODUCT_RECO_PRODUCT:
//      	try {
//      		rtnMapVal = recommendationDao.selectProductRecoProdList(apiParam);
//      	} catch (SQLException e) {
//  			e.printStackTrace();
//  		}
//          break;
      case Constants.URI_PTN_PRODUCT_RECO_PRODUCT_ALL:
      	try {
      		rtnMapVal = recommendationDao.selectProductRecoProdAllList(apiParam);
      	} catch (SQLException e) {
  			e.printStackTrace();
  		}
          break;
        
        /*사용자별 추천 상품*/
        case Constants.URI_PTN_USER_RECO_PRODUCT_ALL:
        	try {
        		rtnMapVal = recommendationDao.selectUserRecoProdAllList(apiParam);
        	} catch (SQLException e) {
    			e.printStackTrace();
    		}
        	break;
//        case Constants.URI_PTN_USER_RECO_PRODUCT:
//        	try {
//        		rtnMapVal = recommendationDao.selectUserRecoProdList(apiParam);
//        	} catch (SQLException e) {
//    			e.printStackTrace();
//    		}
//            break;
                    
        /*검색어별 추천 상품*/
//        case Constants.URI_PTN_SEARCH_PRODUCT_RECO_PRODUCT:
//        	try {
//        		rtnMapVal = recommendationDao.selectSrhProductRecoProdList(apiParam);
//        	} catch (SQLException e) {
//    			e.printStackTrace();
//    		}
//            break;
        case Constants.URI_PTN_SEARCH_PRODUCT_RECO_PRODUCT_ALL:
        	try {
        		rtnMapVal = recommendationDao.selectSrhProductRecoProdAllList(apiParam);
        	} catch (SQLException e) {
    			e.printStackTrace();
    		}
            break;
            
		/* 검색어별 추천 검색어 */
//		case Constants.URI_PTN_SEARCH_WORD_RECO_WORD:
//			try {
//				rtnMapVal = recommendationDao.selectSrhWordRecoWordList(apiParam);
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//			break;
		case Constants.URI_PTN_SEARCH_WORD_RECO_WORD_ALL:
			try {
				rtnMapVal = recommendationDao.selectSrhWordRecoWordAllList(apiParam);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
			
		/*	사용자정의별 추천 상세 */	
		case Constants.URI_PTN_CUSTOM_RECO_DETAIL:
			try {
				rtnMapVal = recommendationDao.selectCustomRecoProdAllList(apiParam);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
        default:
            try {
				throw new Exception("statistice not found api");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return rtnMapVal;
	}
	
	@Transactional
	public Map<String,Object> areaPreviewLogging(ApiInfo apiParam) {
		String api = apiParam.getApi();
		Map<String, Object> rtnMapVal = null;
		switch (api) {
		
        /*상품별 추천 상품*/
        case Constants.URI_PTN_PRODUCT_RECO:
        	try {
        		rtnMapVal = recommendationDao.selectProductRecoList(apiParam);
        	} catch (SQLException e) {
    			e.printStackTrace();
    		}
            break;
        
        
        /*사용자별 추천 상품*/
        case Constants.URI_PTN_USER_RECO:
        	try {
        		rtnMapVal = recommendationDao.selectUserRecoList(apiParam);
        	} catch (SQLException e) {
    			e.printStackTrace();
    		}
            break;
            
        /*검색어별 추천 상품*/
        case Constants.URI_PTN_SEARCH_PRODUCT_RECO:
        	try {
        		rtnMapVal = recommendationDao.selectSrhProductRecoList(apiParam);
        	} catch (SQLException e) {
    			e.printStackTrace();
    		}
            break;
      
		/* 검색어별 추천 검색어 */
		case Constants.URI_PTN_SEARCH_WORD_RECO:
			try {
				rtnMapVal = recommendationDao.selectSrhWordRecoList(apiParam);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
			
		/*	사용자정의별 추천 리스트*/
		case Constants.URI_PTN_CUSTOM_RECO:
			try {
				rtnMapVal = recommendationDao.selectCustomRecoList(apiParam);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;		
        default:
            try {
				throw new Exception("statistice not found api");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return rtnMapVal;
	}
}
