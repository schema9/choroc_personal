package com.choroc.api.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.choroc.api.Constants;
import com.choroc.api.dao.RecommendationStatDao;
import com.choroc.api.domain.StatisticsParam;
import com.choroc.api.dto.ApiInfo;
import com.choroc.api.dto.RcEoadTagHhChannelStat;
import com.choroc.api.dto.RcEoadTagHhStat;
import com.choroc.api.dto.RcEoadTagStat;

@Service
public class RecommendationStatServiceImpl  implements RecommendationStatService {

	@Inject
	private RecommendationStatDao recommendationStatDao;
//	
	private static Logger log = LoggerFactory.getLogger(RecommendationStatServiceImpl.class);
	
	@Transactional
	public Map<String,Object> recommend(ApiInfo apiParam) throws Exception {
		String api = apiParam.getApi();
		Map<String, Object> rtnMapVal = null;
		switch (api) {
		
		case Constants.URI_PTN_ANALYSIS_DAYS_COUNT:
			try {
				// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 cnt
				rtnMapVal = recommendationStatDao.selectDaysCount(apiParam);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			break;		
		
		case Constants.URI_PTN_ANALYSIS_DAYS_LIST:
			try {
				// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 list
				rtnMapVal = recommendationStatDao.selectDaysList(apiParam);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			break;			
		
		case Constants.URI_PTN_ANALYSIS_DAYS_CHART:
			try {
				// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 chart
				rtnMapVal = recommendationStatDao.selectRcEoadTagStatChart2(apiParam);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			break;			
			
		case Constants.URI_PTN_ANALYSIS_CATE_COUNT:
			try {
				// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 항목별 cnt
				rtnMapVal = recommendationStatDao.selectCateCount(apiParam);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			break;			
						
		case Constants.URI_PTN_ANALYSIS_CATE_LIST:
			try {
				// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 항목별 list
				rtnMapVal = recommendationStatDao.selectCateList(apiParam);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			break;			
			
		case Constants.URI_PTN_ANALYSIS_CATE_CHART:
			try {
				// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 항목별 chart
				rtnMapVal = recommendationStatDao.selectRcEoadTagStatChart3(apiParam);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			break;
			
        default:
            try {
				throw new Exception("statistice not found api");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return rtnMapVal;
	}

	@Transactional
	public Map<String,Object> performance(ApiInfo apiParam) {
		String api = apiParam.getApi();
		Map<String, Object> rtnMapVal = null;
		switch (api) {

		case Constants.URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA:
			try {
				rtnMapVal = recommendationStatDao.selectRecoAreaList(apiParam);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA_ALL:
			try {
				rtnMapVal = recommendationStatDao.selectRecoAreaAllList(apiParam);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA2:
			try {
				rtnMapVal = recommendationStatDao.selectRecoArea2List(apiParam);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA2_ALL:
			try {
				rtnMapVal = recommendationStatDao.selectRecoArea2AllList(apiParam);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
		 default:
         try {
				throw new Exception("statistice not found api");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return rtnMapVal;
	}
	
	@Transactional
	public Map<String,Object> index(ApiInfo apiParam) {
		String api = apiParam.getApi();
		Map<String, Object> rtnMapVal = null;
		switch (api) {

		case Constants.URI_PTN_MAIN_PERFORMANCE:
			try {
				rtnMapVal = recommendationStatDao.selectRcEoadTagStat(apiParam);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
		case Constants.URI_PTN_MAIN_CHART:
			try {
				rtnMapVal = recommendationStatDao.selectRcEoadTagStatChart(apiParam);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			break;
		 default:
         try {
				throw new Exception("statistice not found api");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return rtnMapVal;
	}
	

	
	/*아래는 재작업 예정인 코드*/
	
	/* 추천 수집 분석 (일별/시간대별) 갯수 조회*/
	/*
	@Override
	public int selectCollection1Count(RcTagHhStat rcTagHhStat) {
		int listCount = 0;
		
		try 
		{
			listCount = recommendationStatDao.selectCollection1Count(rcTagHhStat);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return listCount;
	}
	*/
	
	
	/* 추천 수집 분석 (일별/시간대별) 리스트 조회 */
	/*
	@Override
	public List<RcTagHhStat> selectCollection1List(StatisticsParam param) {
		List<RcTagHhStat> dataList = null;
		
		try 
		{	
			dataList = recommendationStatDao.selectCollection1List(param);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dataList;	
	}
	*/
	
	/* 추천 수집 분석 (일별/시간대별 항목별) 갯수 조회*/
	/*
	@Override
	public int selectCollection2Count(RcTagHhChannelStat rcTagHhChannelStat) {
		int listCount = 0;
		
		try 
		{
			listCount = recommendationStatDao.selectCollection2Count(rcTagHhChannelStat);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return listCount;
	}
	*/
	
	
	/* 추천 수집 분석 (일별/시간대별 항목별) 리스트 조회*/
	/*
	@Override
	public List<RcTagHhChannelStat> selectCollection2List(StatisticsParam param) {
		List<RcTagHhChannelStat> dataList = null;
		
		try 
		{	
			dataList = recommendationStatDao.selectCollection2List(param);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dataList;	
	}
	*/
	
	/* 추천 성과 (일별/시간대별) 갯수 조회*/
	/*
	@Override
	public int selectCollection3Count(RcTagHhStat rcTagHhStat) {
		int listCount = 0;
		
		try 
		{
			listCount = recommendationStatDao.selectCollection3Count(rcTagHhStat);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return listCount;
	}	
	*/
	
	/* 추천 성과 (일별/시간대별) 리스트 조회 */
	/*
	@Override
	public List<RcTagStat> selectCollection3List(StatisticsParam param) {
		List<RcTagStat> dataList = null;
		
		try 
		{	
			dataList = recommendationStatDao.selectCollection3List(param);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dataList;	
	}
*/
	/*
	@Override
	public List<Map<String, Object>> selectCollectionChart1(Map<String, Object> paramMap) {
		List<Map<String, Object>> result = null;
        try {
            result = recommendationStatDao.selectCollectionChart1(paramMap);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
	}
	*/
}
