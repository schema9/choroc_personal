package com.choroc.api.service;

import java.util.Map;

import com.choroc.api.dto.ApiInfo;

public interface RecommendationAdminService {
	
	//추천 환경관리 - 추천제외관리
	Map<String, Object> manager(ApiInfo apiParam);
}
