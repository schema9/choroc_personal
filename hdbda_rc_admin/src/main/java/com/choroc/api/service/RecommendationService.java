package com.choroc.api.service;

import java.util.Map;

import com.choroc.api.dto.ApiInfo;

public interface RecommendationService {
	
	//추천 환경관리 - 추천영역 미리보기의 3댑스 데이터
	Map<String, Object> areaPreview(ApiInfo apiParam);
	
	//추천 환경관리 - 추천영역 미리보기의 2댑스 데이터
	Map<String, Object> areaPreviewLogging(ApiInfo apiParam);
}
