package com.choroc.api.util;

import com.choroc.api.Constants;
import com.choroc.api.domain.StatisticsParam;

import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

public class Validator {

    public static String paramValidator(HttpServletRequest request, StatisticsParam param) throws Exception {

        String resErrMsg = null;

        // Api NULL 체크
        if(StringUtils.isEmpty(param.getApi())) {
            resErrMsg = "api";
            return resErrMsg;
        }

        switch (param.getApi()) {
        	case Constants.URI_PTN_ELOG:
            break;
        	case Constants.URI_PTN_CATEGORY_LIST:
            break;
        	case Constants.URI_PTN_USER_RECO:
            break;
        	case Constants.URI_PTN_USER_RECO_PRODUCT:
            break;
        	case Constants.URI_PTN_USER_RECO_PRODUCT_ALL:
            break;
        	case Constants.URI_PTN_PRODUCT_RECO:
            break;
        	case Constants.URI_PTN_PRODUCT_RECO_PRODUCT:
            break;
        	case Constants.URI_PTN_PRODUCT_RECO_PRODUCT_ALL:
            break;
        	case Constants.URI_PTN_SEARCH_PRODUCT_RECO:
            break;
        	case Constants.URI_PTN_SEARCH_PRODUCT_RECO_PRODUCT:
			break;
        	case Constants.URI_PTN_SEARCH_PRODUCT_RECO_PRODUCT_ALL:
			break;
        	case Constants.URI_PTN_SEARCH_WORD_RECO:
			break;
        	case Constants.URI_PTN_SEARCH_WORD_RECO_WORD:
			break;
			case Constants.URI_PTN_SEARCH_WORD_RECO_WORD_ALL:
			break;
			case Constants.URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA:
			break;
			case Constants.URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA_ALL:
			break;
			case Constants.URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA2:
			break;
			case Constants.URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA2_ALL:
			break;
			case Constants.URI_PTN_MAIN_PERFORMANCE:
			break;
			case Constants.URI_PTN_MAIN_CHART:
			break;
			case Constants.URI_PTN_RECOMMEND_EXCEPTION_MANAGEMENT_LIST:
			break;
			case Constants.URI_PTN_RECOMMEND_EXCEPTION_MANAGEMENT_INSERT:
			break;
			case Constants.URI_PTN_RECOMMEND_EXCEPTION_MANAGEMENT_DELETE:
			break;
			case Constants.URI_PTN_RECOMMEND_MD_MANAGEMENT_LIST:
			break;
			case Constants.URI_PTN_RECOMMEND_MD_MANAGEMENT_INSERT:
			break;
			case Constants.URI_PTN_RECOMMEND_MD_MANAGEMENT_DELETE:
			break;
			case Constants.URI_PTN_RECOMMEND_ALGORITHM_COUNT:
			break;
			case Constants.URI_PTN_RECOMMEND_ALGORITHM_LIST:
			break;
			case Constants.URI_PTN_RECOMMEND_ALGORITHM_PK_CHECK:
			break;
			case Constants.URI_PTN_RECOMMEND_ALGORITHM_INSERT:
			break;
			case Constants.URI_PTN_RECOMMEND_ALGORITHM_SELECTONE:
			break;
			case Constants.URI_PTN_RECOMMEND_ALGORITHM_NM_CHECK:
			break;
			case Constants.URI_PTN_RECOMMEND_ALGORITHM_UPDATE:
			break;
			case Constants.URI_PTN_RECOMMEND_ALGORITHM_DELETE:
			break;
			case Constants.URI_PTN_RECOMMEND_AREA_COUNT:
			break;
			case Constants.URI_PTN_RECOMMEND_AREA_LIST:
			break;
			case Constants.URI_PTN_RECOMMEND_AREA_INSERT_ALGO:
			break;
			case Constants.URI_PTN_RECOMMEND_AREA_INSERT:
			break;
			case Constants.URI_PTN_RECOMMEND_AREA_UPDATE_ALGO:
			break;
			case Constants.URI_PTN_RECOMMEND_AREA_UPDATE:
			break;
			case Constants.URI_PTN_RECOMMEND_AREA_DELETE:
			break;
			case Constants.URI_PTN_ANALYSIS_DAYS_COUNT:
			break;
			case Constants.URI_PTN_ANALYSIS_DAYS_LIST:
			break;
			case Constants.URI_PTN_ANALYSIS_DAYS_CHART:
			break;
			case Constants.URI_PTN_ANALYSIS_CATE_COUNT:
			break;
			case Constants.URI_PTN_ANALYSIS_CATE_LIST:
			break;
			case Constants.URI_PTN_ANALYSIS_CATE_CHART:
			break;
			case Constants.URI_PTN_NAVER_KWD_BRD:
			break;
			case Constants.URI_PTN_CUSTOM_RECO:
			break;
			case Constants.URI_PTN_CUSTOM_RECO_DETAIL:
			break;	
        	default:
                resErrMsg = "api";
        }
        return resErrMsg;
    }

    public static String setParam(HttpServletRequest request, StatisticsParam param) throws Exception {

        String resErrMsg = null;

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String time = request.getParameter("time");
        String itemId = request.getParameter("itemID");
        String date = request.getParameter("date");
        String dsplyNo = request.getParameter("dsplyNo");
        String dsplyCnt = request.getParameter("dsplyCnt");
        String orderNm  = request.getParameter("orderNm");
        String orderAscDesc = request.getParameter("orderAscDesc");
        String searchOption = request.getParameter("searchOption");
        String searchText = request.getParameter("searchText");
        String pageArea = request.getParameter("pageArea");
        String algorithm = request.getParameter("algorithm");
        String areaId = request.getParameter("areaID");
        String areaNm = request.getParameter("areaNm");
        String dateType = request.getParameter("dateType");
        String algorithmSet = request.getParameter("algorithmSet");
        String listOrder = request.getParameter("listOrder");
        String firstIndex = request.getParameter("firstIndex");
        String lastIndex = request.getParameter("lastIndex");
        
        param.setDateType(dateType);
        param.setStartDate(startDate);
        param.setEndDate(endDate);
        param.setTime(time);
        param.setItemId(itemId);
        param.setDate(date);
        param.setOrderNm(orderNm);
        param.setOrderAscDesc(orderAscDesc);
        param.setSearchOption(searchOption);
        param.setSearchText(searchText);
        param.setPageArea(pageArea);
        param.setAlgorithm(algorithm);
        param.setAreaId(areaId);
        param.setAlgorithmSet(algorithmSet);
        param.setListOrder(listOrder);

        if(!StringUtils.isEmpty(dateType)) {
            param.setDateType(dateType.toUpperCase());
            if(!"A".equals(param.getDateType())
                    && !"B".equals(param.getDateType())
                    && !"C".equals(param.getDateType())) {
                resErrMsg = "dateType";
                return resErrMsg;
            }
        }
        try {
            if(!StringUtils.isEmpty(dsplyCnt)) {
                param.setDsplyCnt(Integer.parseInt(dsplyCnt));
            } else {
                param.setDsplyCnt(10);
            }
        } catch (Exception e) {
            resErrMsg = "dsplyCnt";
            return resErrMsg;
        }
        try {
            if(!StringUtils.isEmpty(dsplyNo)) {
                int tmpDsplyCnt = param.getDsplyCnt();
                int tmpDsplyNo = Integer.parseInt(dsplyNo);
                tmpDsplyNo = (tmpDsplyNo - 1) * tmpDsplyCnt;
                param.setDsplyNo(tmpDsplyNo);
            } else {
                param.setDsplyNo(0);
            }
        } catch (Exception e) {
            resErrMsg = "dsplyNo";
            return resErrMsg;
        }
        try {
        	if(!StringUtils.isEmpty(firstIndex)) {
        		param.setFirstIndexInt(Integer.parseInt(firstIndex));
        	} else {
        		param.setFirstIndexInt(1);
        	}
        } catch (Exception e) {
            resErrMsg = "602(firstIndex)";
            return resErrMsg;
        }
        try {
        	if(!StringUtils.isEmpty(lastIndex)) {
        		param.setLastIndexInt(Integer.parseInt(lastIndex));
        	} else {
        		param.setLastIndexInt(10);
        	}
        } catch (Exception e) {
            resErrMsg = "603(lastIndex)";
            return resErrMsg;
        }
        
        if(10 != param.getDsplyCnt() && 20 != param.getDsplyCnt()) {
            resErrMsg = "dsplyCnt";
            return resErrMsg;
        }

        return resErrMsg;
    }

    public static boolean isValidDate(String startDate, String endDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        sdf.setLenient(false);

        if (StringUtils.isEmpty(startDate)) return false;
        if (StringUtils.isEmpty(endDate)) return false;

        try {
            sdf.format(sdf.parse(startDate));
            sdf.format(sdf.parse(endDate));

            int day = (int) ((sdf.parse(endDate).getTime() - sdf.parse(startDate).getTime()) / 1000 / 60 / 60 / 24);
            if(0 > day) {
                return false;
            }
            if(90 < day) {
                return false;
            }
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public static boolean isValidOneDate(String date) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        sdf.setLenient(false);

        if (StringUtils.isEmpty(date)) return false;

        try {
            sdf.format(sdf.parse(date));
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    
}
