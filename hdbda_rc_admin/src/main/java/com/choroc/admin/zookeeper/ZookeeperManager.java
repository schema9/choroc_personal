package com.choroc.admin.zookeeper;


import java.io.IOException;
import java.util.List;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZookeeperManager implements Watcher {
	private static final Logger logger = LoggerFactory.getLogger(ZookeeperManager.class);

	private static class Holder {
		static final ZookeeperManager instance = new ZookeeperManager();
	}
	public static ZookeeperManager getInstance() {
		return Holder.instance;
	}

	private boolean bInit = false;
	private ZooKeeper zk = null;

	private ZookeeperManager() {
	}

	// 앙상블 내에 ZooKeeper 서버를 모두 연결 스트링에 지정한다면, ZooKeeper 서버 한 대에 장애가 발생해도 다음 서버로 자동 재접속한다.
	public synchronized void init(String connectionString) throws ZookeeperManagerException {
		if (bInit) return;

		// ZooKeeper 세션을 생성한다.
		try {
			this.zk = new ZooKeeper(connectionString, 3000, this);
			this.zk.exists("/", false);
		} catch (IOException | KeeperException | InterruptedException ex) {
			throw new ZookeeperManagerException(ex);
		} // session timeout 3초

		this.bInit = true;
	}

	public synchronized void close() {
		if (null != this.zk) try { this.zk.close(); } catch (InterruptedException ex) { logger.error(ex.getMessage(), ex); }

		this.bInit = false;
	}

	public List<String> getChildrenList(String path) throws ZookeeperManagerException {
		try {
			return this.zk.getChildren(path, false);
		} catch (KeeperException | InterruptedException ex) {
			throw new ZookeeperManagerException(ex);
		}
	}

	public void deleteNodeForPast(String path, long nowTime, long retentions) throws ZookeeperManagerException {
		try {
			Stat stat = this.zk.exists(path, false);
			long createdNodeTime = stat.getCtime();
			if ((nowTime - createdNodeTime) > retentions) this.zk.delete(path, -1);
		} catch (KeeperException | InterruptedException ex) {
			throw new ZookeeperManagerException(ex);
		}
	}

	public boolean exists(String path) throws ZookeeperManagerException {
		Stat stat;
		try {
			stat = this.zk.exists(path, false);
		} catch (KeeperException ex) {
			throw new ZookeeperManagerException(ex);
		} catch (InterruptedException ex) {
			throw new ZookeeperManagerException(ex);
		}
		if (null == stat) return false;
		return true;
	}

	public void delete(String path) throws ZookeeperManagerException {
		try {
			this.zk.delete(path, -1);
		} catch (KeeperException ex) {
			throw new ZookeeperManagerException(ex);
		} catch (InterruptedException ex) {
			throw new ZookeeperManagerException(ex);
		}
	}

	public void setData(String path, byte[] data) throws ZookeeperManagerException {
		try {
			this.zk.setData(path, data, -1);
		} catch (KeeperException ex) {
			throw new ZookeeperManagerException(ex);
		} catch (InterruptedException ex) {
			throw new ZookeeperManagerException(ex);
		}
	}

	public byte[] getData(String path) throws ZookeeperManagerException {
		try {
			return this.zk.getData(path, false, null);
		} catch (KeeperException ex) {
			throw new ZookeeperManagerException(ex);
		} catch (InterruptedException ex) {
			throw new ZookeeperManagerException(ex);
		}
	}

	public void create(String path) throws ZookeeperManagerException {
		try {
			this.zk.create(path, null, Ids.OPEN_ACL_UNSAFE,CreateMode.PERSISTENT);
		} catch (KeeperException ex) {
			throw new ZookeeperManagerException(ex);
		} catch (InterruptedException ex) {
			throw new ZookeeperManagerException(ex);
		}
	}

	public void createRecursivly(String path) throws ZookeeperManagerException {
		this.createRecursivly(path, CreateMode.PERSISTENT);
	}

	public void createRecursivly(String path, CreateMode mode) throws ZookeeperManagerException {
		try {
			if (path.length() > 0 && null == this.zk.exists(path, this)) {
				String temp = path.substring(0, path.lastIndexOf("/"));
				createRecursivly(temp);
				
				try {
					this.zk.create(path, null, Ids.OPEN_ACL_UNSAFE, mode);
				} catch (KeeperException ex) {
					if (ex.getMessage().startsWith("KeeperErrorCode = NodeExists for ")) return;
					else throw ex;
				}
			}else{
				return;
			}
		} catch (KeeperException ex) {
			throw new ZookeeperManagerException(ex);
		} catch (InterruptedException ex) {
			throw new ZookeeperManagerException(ex);
		}
	}

	@Override
	public void process(WatchedEvent we) {
		logger.debug("{}", we);
	}
}
