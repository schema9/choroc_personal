package com.choroc.admin.zookeeper;


public class ZookeeperManagerException extends Exception {
	private static final long serialVersionUID = 8666268076830343256L;

	public ZookeeperManagerException() {
		super();
	}

	public ZookeeperManagerException(String message, Throwable cause) {
		super(message, cause);
	}

	public ZookeeperManagerException(String message) {
		super(message);
	}

	public ZookeeperManagerException(Throwable cause) {
		super(cause);
	}

}
