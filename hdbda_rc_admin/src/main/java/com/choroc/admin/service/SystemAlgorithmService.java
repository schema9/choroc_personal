package com.choroc.admin.service;

import java.util.List;

import com.choroc.admin.domain.SystemAlgorithmParam;
import com.choroc.admin.dto.SystemAlgorithmInfo;

/**
 * TODO  > 추천 환경 관리 > 추천 위젯 설정
 * 파일명   > SystemAlgorithmService.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 8.
 * 수정일   > 
 * 수정자   > 
 */
public interface SystemAlgorithmService {

	public List<SystemAlgorithmInfo> selectRecoAlgoMgmtRows(SystemAlgorithmParam param);

	public SystemAlgorithmInfo selectRecoAlgoMgmtRow(String algoId);

	public int selectRecoAlgoMgmtTotalCount(SystemAlgorithmParam param);

	public int selectRecoAlgoMgmtCountForValidKey(String algoId);

	public int insertRecoAlgoMgmtRow(SystemAlgorithmInfo param);

	public int updateRecoAlgoMgmtRow(SystemAlgorithmInfo param);

	public int deleteRecoAlgoMgmtRow(SystemAlgorithmInfo param);
}
