package com.choroc.admin.service;

import com.choroc.admin.util.ZValue;

/**
 * TODO  > 회원가입
 * 파일명   > UserRegisterService.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 9.
 * 수정일   > 
 * 수정자   > 
 */
public interface UserRegisterService {

	/*도메인 name 중복 체크*/
	public int getRegisterDomainNameCheck(ZValue zvl);

	/*도메인 url 중복 체크*/
	public int getRegisterDomainUrlCheck(ZValue zvl);
	
	/*신규회원 가입*/
	public int newUserRegister(ZValue zvl);
}
