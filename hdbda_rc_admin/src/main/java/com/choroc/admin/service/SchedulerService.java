package com.choroc.admin.service;

import java.util.List;

import com.choroc.admin.domain.SchedulerParam;
import com.choroc.admin.dto.Scheduler;

/**
 * TODO > 스케줄러 서비스
 * 파일명   > SchedulerService.java
 * 작성일   > 2018. 12. 4.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public interface SchedulerService {

	/*스케줄러 목록*/
	public List<Scheduler> selectListRows(SchedulerParam schedulerParam);
	
	/*스케줄러 목록의 총갯수*/
	public int selectListTotalCount(SchedulerParam schedulerParam);
	
	/*스케줄러 등록*/
	public int insertRow(Scheduler scheduler);
	
	/*스케줄러 조회*/
	public Scheduler selectOneRow(int job_id);
	
	/*스케줄러 수정*/
	public int updateRow(Scheduler scheduler);
	
	/* 잡 수동 실행*/
	public int insertRedo(Scheduler shceduler);
	
	/* 잡 관련 데이터 삭제 */
	public int deleteAll(Scheduler scheduler);
	
	/* 에이전트 리스트 조회 */
	public List<Scheduler> selectAgentList();
	
	/* 스키마 단위 ETL 수동실행 */
	public int etlExecute(Scheduler scheduler);
	
}
