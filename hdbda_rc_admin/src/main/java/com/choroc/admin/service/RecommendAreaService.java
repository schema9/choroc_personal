package com.choroc.admin.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.choroc.admin.domain.RecommendAreaParam;
import com.choroc.admin.dto.Algorithm;
import com.choroc.admin.dto.RecommendAreaInfo;

/**
 * TODO  > 추천환경관리 > 추천알고리즘 설정 인터페이스
 * 파일명   > RecommendAreaService.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 8.
 * 수정일   > 
 * 수정자   > 
 */
public interface RecommendAreaService {
	
	// 리스트
	public List<RecommendAreaInfo> getRecommendAreaList(RecommendAreaParam param) throws SQLException;

	public int getRecommendAreaCount(RecommendAreaParam param) throws SQLException;

	// 등록
	public List<Algorithm> getAlgorithmList() throws SQLException;
	
	public String checkRecommendArea(RecommendAreaParam param) throws SQLException;
	
	public int insertRecommendArea(RecommendAreaParam param, HttpServletRequest request) throws SQLException;
	
	
	// 수정
	public RecommendAreaInfo selectOneRecommendArea(RecommendAreaParam param) throws SQLException;
	
	public int updateRecommendArea(RecommendAreaParam param, HttpServletRequest request) throws SQLException;

}
