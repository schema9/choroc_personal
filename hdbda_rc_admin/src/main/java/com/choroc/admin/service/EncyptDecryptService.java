package com.choroc.admin.service;

/**
 * TODO  >
 * 파일명   > EncyptDecryptService.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 9.
 * 수정일   > 
 * 수정자   > 
 */
public interface EncyptDecryptService {
	
	/** Encrypt / Decrypt Key */
	public static final String ENC_DEC_KEY =  "POLARIS";
		
	/* Plain Text를 Encrypt 하여 Base64 Encoding */
	public String encryptString(String plainText);
	
	/*Encrypted Text를 Decrypt하여 Base64 Deocoding 한다.*/
	public String decryptString(String encryptedText);
}
