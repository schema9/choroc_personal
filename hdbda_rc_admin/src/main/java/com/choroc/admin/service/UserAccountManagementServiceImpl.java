package com.choroc.admin.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.choroc.admin.dao.UserAccountManagementDao;
import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.dto.AccountInfo;
import com.choroc.admin.dto.ServiceInfo;
import com.choroc.admin.util.ZValue;

/**
 * TODO  > 권한 관리 > 사용자 관리 > 사용자 계정 관리
 * 파일명   > UserAccountManagementServiceImpl.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 8.
 * 수정일   > 
 * 수정자   > 
 */

@Service
public class UserAccountManagementServiceImpl  implements UserAccountManagementService {
	 
	@Autowired
	private UserAccountManagementDao userAccountManagementDao;
	
	private static Logger log = LoggerFactory.getLogger(UserAccountManagementServiceImpl.class);
	
	/*사용자 계정 목록*/
	@Override
	public List<AccountInfo> selectUserAccountRows(AccountParam param) {
		// TODO Auto-generated method stub
		
		try {
			return userAccountManagementDao.selectUserAccountRows(param);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/*사용자 계정 상세*/
	@Override
	public AccountInfo selectUserAccountRow(String param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ServiceInfo> selectServiceRowsByDomainID(int param) {
		// TODO Auto-generated method stub
		return null;
	}

	/*선택된 사용자의 서비스 목록*/
	@Override
	public List<ServiceInfo> selectServiceRowsByUserID(String param) {
		// TODO Auto-generated method stub
		return null;
	}

	/*사용자 계정 총 수*/
	@Override
	public int selectUserAccountTotalCount(AccountParam param) {
		// TODO Auto-generated method stub
		try {
			return userAccountManagementDao.selectUserAccountTotalCount(param);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	/*사용자 계정 유효 계정 수*/
	@Override
	public int selectUserAccountValidUserCount(String param) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*사용자 계정 등록*/
	@Override
	public int userAccountAdd(AccountInfo accountInfo) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*사용자 계정 수정*/
	@Override
	public int userAccountModify(AccountInfo accountInfo) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*사용자 계정 사용제어 변경*/
	@Override
	public int updateUserAccountUseYn(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*사용자 비밀번호 수정*/
	@Override
	public int updateUserAccountUserPassword(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*사용자가 가입 후 또는 비밀번호 변경 후 최초 로그인일 경우 비밀번호를 변경한다.*/
	@Override
	public int modifyFirstLoginPassword(ZValue zValue) throws Exception {
		int result = 0;
		
		try {
			result = userAccountManagementDao.modifyFirstLoginPassword(zValue);
			
			// 비밀번호 변경시 변경내역 저장
			userAccountManagementDao.insertUserPasswordHis(zValue);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	/*비밀번호 변경 기간이 로그인한 일로 3개월 이상 일때 처리 로직 팝업으로 공지후 연장 처리*/
	@Override
	public int modifyPasswordPeriod(AccountParam param) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	/*기존 비밀번호 검사*/
	@Override
	public int getPasswordCheck(AccountParam param) {
		// TODO Auto-generated method stub
		try {
			return userAccountManagementDao.selectPasswordCheck(param);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 3;
		}
		//return 0;
	}

	/*히스토리 테이블에서 사용자의 변경 비밀번호가 기존에 쓰였는지 조회*/
	@Override
	public int selectUserPwdHis(AccountParam param) {
		// TODO Auto-generated method stub
		try {
			return userAccountManagementDao.selectUserPwdHis(param);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
		//return 0;
	}
	
	
	
	/*비밀번호 수정*/
	@Override
	@Transactional
	public int modifyUserPassword(AccountParam param) {
		// TODO Auto-generated method stub
		try {
			userAccountManagementDao.updateUserPassword(param);
			userAccountManagementDao.updateUserPasswordHis(param);
			return 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 3;
		}
	}
	
	
	/*아이디 중복체크*/
	@Override
	public int selectAccountUserIdCountForValidKey(AccountParam param) {
		// TODO Auto-generated method stub
		try {
			return userAccountManagementDao.selectAccountUserIdCountForValidKey(param);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 3;
		}
		//return 0;
		
	}
	
	/*권한 코드 목록*/
	@Override
	public List<AccountInfo> selectAuthCd(AccountParam param) {
		// TODO Auto-generated method stub
		
		try {
			return userAccountManagementDao.selectAuthCd(param);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	/*사용자등록*/
	@Override
	@Transactional
	public int createAccountUser(AccountParam param) {
		// TODO Auto-generated method stub
		try {
			userAccountManagementDao.createAccountUser(param);
			userAccountManagementDao.createAccountUserMap(param);
			return 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 3;
		}
	}
	/*비밀번호 초기화*/
	@Override
	public int userPwdReset(AccountParam param) {
		// TODO Auto-generated method stub
		try {
			userAccountManagementDao.userPwdReset(param);
			return 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 3;
		}
	}
	@Transactional
	public int userModifySet(AccountParam param) {
		// TODO Auto-generated method stub
		try {
			userAccountManagementDao.userModifySet(param);
			userAccountManagementDao.userModifySetMap(param);
			return 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 3;
		}
	}

}
