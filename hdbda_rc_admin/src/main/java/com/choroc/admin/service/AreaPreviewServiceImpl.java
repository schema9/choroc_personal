package com.choroc.admin.service;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choroc.admin.dao.AreaPreviewDao;
import com.choroc.admin.dao.SystemManagerDao;
import com.choroc.admin.domain.SystemManagerParam;
import com.choroc.admin.dto.SystemManagerInfo;
@Service
public class AreaPreviewServiceImpl  implements AreaPreviewService {

	@Autowired
	private AreaPreviewDao areaPreviewDao;
	
	private static Logger log = LoggerFactory.getLogger(AreaPreviewServiceImpl.class);
	
	/*추천화면 데이터 목록*/
	@Override
	public List<SystemManagerParam> selectRecoAreaRow(SystemManagerParam param) {
		try {
				return areaPreviewDao.selectRecoAreaRow(param);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*추천화면 데이터 총 수*/
	@Override
	public int selectRecoAreaTotalCount(SystemManagerParam param) {
		try {
			return areaPreviewDao.selectRecoAreaTotalCount(param);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}		
}
