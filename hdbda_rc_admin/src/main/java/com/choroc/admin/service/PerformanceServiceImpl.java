package com.choroc.admin.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * TODO  > 추천 성과 분석 > 추천 영역별
 * 파일명   > PerformanceServiceImpl.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 8.
 * 수정일   > 
 * 수정자   > 
 */

@Service
public class PerformanceServiceImpl implements PerformanceService {

	//@Autowired
	//private PerformanceDao mySqlPerformanceDao;	
	
	/*추천 영역별 상세 카운트*/
	@Override
	public int getAreaAlgorithmSetCnt(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*추천 영역별 알고리즘 상세 카운트*/
	@Override
	public int getAreaAlgorithmCnt(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*추천 영역별 알고리즘 추천영역 List*/
	@Override
	public List<Map<String, Object>> getAreaAlgorithmChartList(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	/*추천 알고리즘 SET List*/
	@Override
	public List<Map<String, Object>> getAlgorithmSetList(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	/*추천 알고리즘 SET 카운트*/
	@Override
	public int getAlgorithmSetCnt(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*추천 알고리즘 알고릳즘 Set List*/
	@Override
	public List<Map<String, Object>> getAlgorithmAlgorithmSetList(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	/*추천 알고리즘 List*/
	@Override
	public List<Map<String, Object>> getAlgorithmList(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	/*추천 알고리즘  카운트*/
	@Override
	public int getAlgorithmCnt(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*추천 알고리즘 알고리즘 List*/
	@Override
	public List<Map<String, Object>> getAlgorithmAlgorithmList(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	/*방문자 대비 주문 페이지 카운트*/
	@Override
	public int getPervisitorOrderPageCnt(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*방문자 대비 주문 페이지 리스트*/
	@Override
	public List<Map<String, Object>> getPervisitorOrderPageList(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	/*추천영역 별 알고리즘 리스트 박스*/
	@Override
	public List<Map<String, Object>> getRcmAreaAlgorithmBox(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	/*추천영역 별 알고리즘 SET*/
	@Override
	public List<Map<String, Object>> getRcmAreaAlgorithmSet(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	/*추천영역 별 알고리즘 SET 카운트*/
	@Override
	public int getRcmAreaAlgorithmSetCnt(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*추천영역 별 알고리즘 SET 리스트*/
	@Override
	public List<Map<String, Object>> getRcmAreaAlgorithmSetList(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

}
