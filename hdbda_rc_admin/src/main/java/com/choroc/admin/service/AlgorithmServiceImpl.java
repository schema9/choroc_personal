package com.choroc.admin.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.choroc.admin.dao.AlgorithmDao;
import com.choroc.admin.domain.AlgorithmParam;
import com.choroc.admin.dto.Algorithm;

/**
 * TODO > 알고리즘탭 리스트 조회, 알고리즘 필터 중복확인 , 등록, 수정 파일명 > AlgorithmServiceImpl.java
 * 작성일 > 2018. 11. 22. 작성자 > CCmedia Service Corp. 수정일 > 수정자 >
 */
@Service
public class AlgorithmServiceImpl implements AlgorithmService {

	private static final Logger logger = LoggerFactory.getLogger(AlgorithmServiceImpl.class);

	@Inject
	private AlgorithmDao algorithmDao;

	// 알고리즘탭 리스트
	@Override
	public List<Algorithm> getAlgorithmList(AlgorithmParam param) throws SQLException {
		List<Algorithm> resultList = algorithmDao.getAlgorithmList(param);
		return resultList;
	}

	@Override
	public int selectAlgorithmTotalCount(AlgorithmParam param) {
		int result = 0;
		try {
			result = algorithmDao.selectAlgorithmTotalCount(param);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String checkAlgorithm(Map<String, Object> paramMap) {
		String msg = null;
		try {
			int result = algorithmDao.checkAlgorithm(paramMap);
			if (result == 1) {
				msg = "중복된 추천 알고리즘 명입니다.";
			} else {
				msg = "사용 가능한 추천 알고리즘 명입니다.";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;
	}

	@Override
	public int insertAlgorithm(Map<String, Object> paramMap, HttpServletRequest request) {
		int result = 0;
		try {
			paramMap.put("algrth_id", request.getParameter("algrth_id"));
			paramMap.put("algrth_nm", request.getParameter("algrth_nm"));
			paramMap.put("algrth_dc", request.getParameter("algrth_dc"));
			paramMap.put("create_id", "admin");
			paramMap.put("use_yn", "Y");
			paramMap.put("del_yn", "N");
			result = algorithmDao.insertAlgorithm(paramMap);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Algorithm selectOneAlgorithm(Map<String, Object> paramMap, HttpServletRequest request) throws SQLException {
		Algorithm result = null;
		paramMap.put("algrth_id", request.getParameter("algrth_id"));
		result = algorithmDao.selectOneAlgorithm(paramMap);
		return result;
	}

	@Override
	public int updateAlgorithm(Map<String, Object> paramMap, HttpServletRequest request) {
		int result = 0;
		try {
			paramMap.put("algrth_id", request.getParameter("algrth_id"));
			paramMap.put("algrth_nm", request.getParameter("algrth_nm"));
			paramMap.put("algrth_dc", request.getParameter("algrth_dc"));
			paramMap.put("old_id", request.getParameter("old_id"));
			paramMap.put("use_yn", "Y");
			paramMap.put("del_yn", "N");
			paramMap.put("create_id", "admin");
			paramMap.put("modify_id", "admin");
			result = algorithmDao.updateAlgorithm(paramMap);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

}