package com.choroc.admin.service;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.choroc.admin.dao.SystemMonitoringDaoImpl;
import com.choroc.admin.domain.MonitoringParam;
import com.choroc.admin.dto.Monitoring;
import com.choroc.admin.dto.Scheduler;

@Service
public class SystemMonitoringServiceImpl implements SystemMonitoringService{
	
	private static final Logger logger = LoggerFactory.getLogger(SystemMonitoringServiceImpl.class);

	@Inject
	private SystemMonitoringDaoImpl monitoringDao;
	
	@Override
	public List<Monitoring> selectMonitoringList(MonitoringParam param)  throws SQLException {
		List<Monitoring> resultList = monitoringDao.selectMonitoringList(param);
		return resultList;
	}

	@Override
	public int selectMonitoringTotalCount(MonitoringParam param)  throws SQLException{
		int result = 0;
		try {
			result = monitoringDao.selectMonitoringTotalCount(param);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public Scheduler schedulerDetailView(MonitoringParam param) throws SQLException {
		Scheduler schd = monitoringDao.schedulerDetailView(param);
		return schd;
	}
	
}
