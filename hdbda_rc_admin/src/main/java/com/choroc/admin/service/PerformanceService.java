package com.choroc.admin.service;

import java.util.List;
import java.util.Map;

/**
 * TODO  > 추천 성과 분석 Service
 * 파일명   > PerformanceService.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 8.
 * 수정일   > 
 * 수정자   > 
 */
public interface PerformanceService {

	/*추천 영역별 상세 카운트*/
	int getAreaAlgorithmSetCnt(Map<String, Object> paramMap);
	
	/*추천 영역별 알고리즘 상세 카운트*/
	int getAreaAlgorithmCnt(Map<String, Object> paramMap);
	
	/*추천 영역별 알고리즘 추천영역 List*/
	List<Map<String, Object>> getAreaAlgorithmChartList(Map<String, Object> paramMap);
	
	/*추천 알고리즘 SET List*/
	List<Map<String, Object>> getAlgorithmSetList(Map<String, Object> paramMap);

	/*추천 알고리즘 SET 카운트*/
	int getAlgorithmSetCnt(Map<String, Object> paramMap);
	
	/*추천 알고리즘 알고릳즘 Set List*/
	List<Map<String, Object>> getAlgorithmAlgorithmSetList(Map<String, Object> paramMap);
	
	/*추천 알고리즘 List*/
	List<Map<String, Object>> getAlgorithmList(Map<String, Object> paramMap);
	
	/*추천 알고리즘  카운트*/
	int getAlgorithmCnt(Map<String, Object> paramMap);
	
	/*추천 알고리즘 알고리즘 List*/
	List<Map<String, Object>> getAlgorithmAlgorithmList(Map<String, Object> paramMap);
	
	/*방문자 대비 주문 페이지 카운트*/
	int getPervisitorOrderPageCnt(Map<String, Object> paramMap);
	
	/*방문자 대비 주문 페이지 리스트*/
	List<Map<String, Object>> getPervisitorOrderPageList(Map<String, Object> paramMap);
	
	/*추천영역 별 알고리즘 리스트 박스*/
	List<Map<String, Object>> getRcmAreaAlgorithmBox(Map<String, Object> paramMap);
	
	/*추천영역 별 알고리즘 SET*/
	List<Map<String, Object>> getRcmAreaAlgorithmSet(Map<String, Object> paramMap);
	
	/*추천영역 별 알고리즘 SET 카운트*/
	int getRcmAreaAlgorithmSetCnt(Map<String, Object> paramMap);
	
	/*추천영역 별 알고리즘 SET 리스트*/
	List<Map<String, Object>> getRcmAreaAlgorithmSetList(Map<String, Object> paramMap);
}
