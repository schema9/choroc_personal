package com.choroc.admin.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.choroc.admin.domain.LoginParam;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.util.ZValue;

/**
 * TODO  >
 * 파일명   > UserService.java
 * 작성일   > 2018. 11. 20.
 * 작성자   > Copyright CCmediaService
 * 수정일   > 
 * 수정자   > 
 */
public interface UserService {
	
	// 로그인 유저 정보
	public AuthUser getLoginUser(LoginParam param);
	
	// 로그인 성공시 정보 업데이트
	public void loginSuccessProcess(ZValue zvl);
	
	// 로그인 성공시 정보 업데이트(마지막 로그인 날짜 제외)
	public void loginSuccessProcessExcludeLoginDt(ZValue zvl);

	// 로그인 실패
	public String loginFaileProcess(ZValue zvl);

	// 로그인 실패횟수 업데이트 
	public int loginFaileCount(ZValue zvl);
	
	// 계정 잠금 및 실패 횟수 초기화
	public int acntLockLimitExpirationProcess(ZValue zvl);
	

	
/*	
	public String getEncryptedPassword(LoginParam param);

	

	public int loginSuccessProcess(ZValue zvl);

	

	public List<AuthUser> getLockMember();

	public int modifyLockMember(AuthUser param);
	*/
}
