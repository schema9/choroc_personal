package com.choroc.admin.service;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.choroc.admin.dao.RecommendExceptionDao;
import com.choroc.admin.domain.RecommendExceptionParam;
import com.choroc.admin.dto.RecommendException;

/**
	 * 
	 * 추천 제외 관리 
	 *
	 */

@Service
public class RecommendExceptionServiceImpl implements RecommendExceptionService {

	private static final Logger logger = LoggerFactory.getLogger(RecommendExceptionServiceImpl.class);
	
	@Inject
	private RecommendExceptionDao recommendExceptionDao;
	
	@Override
	public List<RecommendException> selectExceptionList(RecommendExceptionParam param) throws SQLException{
		List<RecommendException> resultList = null;
		resultList = recommendExceptionDao.selectExceptionList(param);
		return resultList;
	}
	
	@Override
	public int selectExceptionTotalCount(RecommendExceptionParam param) throws SQLException{
		int result = 0;
		result = recommendExceptionDao.selectExceptionTotalCount(param);
		return result;
	}
	
	@Override
	public String checkException(RecommendExceptionParam param) throws SQLException{
		// 초기 설정
		String msg = null;
		List<String> duplList = new ArrayList<String>();
		List<String> passList = new ArrayList<String>();
		StringBuilder duplStr = new StringBuilder(" 는 중복된 코드입니다.");
		StringBuilder passStr = new StringBuilder(" 는 사용 가능한 코드입니다.");
		String[] beforeArray = param.getContent_cd().split(",");
		String empty = "";
		
		// DB에 중복된 값이 있는지 확인
		for(int i=0; i<beforeArray.length; i++) {
			// 입력 값 중에 공백값이 있으면
			if(beforeArray[i].trim().equals(empty)) {
				msg = "공백값은 입력할 수 없습니다";
				return msg;
			}else {
				RecommendExceptionParam daoParam = new RecommendExceptionParam();
				daoParam.setContent_cd(beforeArray[i].replace(" ", ""));
				daoParam.setSortation(param.getSortation());
				int result = recommendExceptionDao.checkException(daoParam);
				if(result == 1) {
					duplList.add(beforeArray[i].replace(" ", ""));
				}else {
					passList.add(beforeArray[i].replace(" ", ""));
				}
			}
		}
		
		// 중복된 코드가 있으면 
		if(duplList.size() != 0) {
			for(int i=0; i<duplList.size(); i++) {
				// 두개 이상이면 콤마 추가
				if(i >=1 ) {
					duplStr.insert(0, ", ");
				}
				String duplCd = String.format("[%s]", duplList.get(i));
				duplStr.insert(0, duplCd);
			}
			msg = duplStr.toString();
		// 중복된 코드가 없으면
		}else if(duplList.size() == 0){
			for(int i=0; i<passList.size(); i++) {
				// 두개 이상이면 콤마 추가
				if(i >=1 ) {
					passStr.insert(0, ", ");
				}
				String passCd = String.format("[%s]", passList.get(i));
				passStr.insert(0, passCd);
			}
			/*msg = passStr.toString();*/
			msg = "등록 가능한 코드입니다.";
		}
		return msg;
	}
	
	@Override
	public int insertException(RecommendException param) throws SQLException{
		int result =0;
		List<RecommendException> insertList = new ArrayList<RecommendException>();
		
		String[] array = param.getContent_cd().split(",");
		for(int i=0; i<array.length; i++) {
			RecommendException dto = new RecommendException();
			dto.setContent_cd(array[i].replaceAll(" ", ""));
			dto.setSortation(param.getSortation());
			insertList.add(dto);
		}
		
		result = recommendExceptionDao.insertException(insertList);
		return result;
	}
	
	@Override
	public RecommendException selectOneException(RecommendExceptionParam param) throws SQLException{
		RecommendException recommendException;
		recommendException = recommendExceptionDao.selectOneException(param);
		return recommendException;
	}
	
	@Override
	public int updateException(RecommendExceptionParam param) throws SQLException{
		int result = 0;
		result = recommendExceptionDao.updateException(param);
		return result;
	}
	
	@Override
	public int deleteException(RecommendException param) throws SQLException{
		int result = 0;
		result = recommendExceptionDao.deleteException(param);
		return result;
	}
	
}
