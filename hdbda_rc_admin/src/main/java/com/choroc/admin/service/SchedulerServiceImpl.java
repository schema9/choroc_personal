package com.choroc.admin.service;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.choroc.admin.dao.SchedulerDao;
import com.choroc.admin.domain.SchedulerParam;
import com.choroc.admin.dto.Scheduler;
import com.choroc.admin.dto.SchedulerTask;
import com.choroc.admin.util.DateUtil;
import com.eBrother.common.kafka.producer.KafkaProducerManager;

/**
 * TODO > 스케줄러 서비스 구현
 * 파일명   > SchedulerServiceImpl.java
 * 작성일   > 2018. 12. 4.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */

@Configuration
@Component
@PropertySource("classpath:application-context.properties")
@Service
public class SchedulerServiceImpl implements SchedulerService{

	@Inject
	private SchedulerDao schedulerDao; 
	
	@Autowired
	private Environment env;
	
	private static final Logger logger = LoggerFactory.getLogger(SchedulerServiceImpl.class);
	
	private KafkaProducerManager kpm = null;
	
	
	//카프카 설정
	@Value("${kafka.brokers}")
	private String brokers;
	
	@Value("${kafka.retries}")
	private int retries;
	
	@Value("${kafka.batch.size}")
	private int batchSize;
	
	@Value("${kafka.buffer.memory}")
	private int bufferMemory;
	
	@Value("${executor.max.threads.number}")
	private int thread;
	
	
	
	/* 스케줄러 목록 */
	@Override
	public List<Scheduler> selectListRows(SchedulerParam schedulerParam) {
		
		List<Scheduler> schedulerList = new ArrayList<>(Arrays.asList());
		try
		{				
			schedulerList = schedulerDao.selectListRows(schedulerParam);
		}
		catch (Exception e)	{	
			
			e.printStackTrace();
		}	
		return schedulerList;
	}

	/* 스케줄러 레코드 총갯수 */
	@Override
	public int selectListTotalCount(SchedulerParam schedulerParam) {
		
		int totalCount = 0;
		try
		{				
			totalCount = schedulerDao.selectListTotalCount(schedulerParam);
		}
		catch (Exception e)	{	
			
			e.printStackTrace();
		}	
		return totalCount;		
	}

	/*스케줄러 등록*/	
	@Override
	public int insertRow(Scheduler scheduler) {
		
		int insertResult = 0;
		try
		{				
			insertResult = schedulerDao.insertRow(scheduler);
		}
		catch (Exception e)	{	
			
			e.printStackTrace();
		}	
		return insertResult;
	}

	/*스케줄러 조회*/
	@Override
	public Scheduler selectOneRow(int job_id) {
		
		Scheduler scheduler = null;
		try
		{				
			scheduler = schedulerDao.selectOneRow(job_id);
		}
		catch (Exception e)	{	
			
			e.printStackTrace();
		}	
		return scheduler;
	}

	/*스케줄러 수정*/
	@Override
	public int updateRow(Scheduler scheduler) {

		int updateResult = 0;
		try
		{				
			updateResult = schedulerDao.updateRow(scheduler);
		}
		catch (Exception e)	{	
			
			e.printStackTrace();
		}	
		return updateResult;
	}
	
	/* 에이전트 리스트 조회 */
	@Override
	public List<Scheduler> selectAgentList(){
		List<Scheduler> agentList = null;
		try {
			agentList = schedulerDao.selectAgentList();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return agentList;
	}
	
	/* 잡 데이터 (job, task, his 테이블)삭제 */
	@Transactional
	@Override
	public int deleteAll(Scheduler scheduler) {
		int result = 0;
		try {
			result = schedulerDao.deleteAll(scheduler);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	/* 스케줄러 수동 실행 */
	@Transactional
	@Override
	public int insertRedo(Scheduler scheduler) {
		int result = 0;
		SchedulerTask task =  new SchedulerTask();
		
		/* 카프카 매니저 실행 */
		kpm = new KafkaProducerManager(brokers, retries, batchSize, bufferMemory);
		ExecutorService executor = Executors.newFixedThreadPool(thread);
		
		try {
			
			// 카프카 메시지 전송
			String curr = DateUtil.getCurrentDateWithPattern("yyyyMMddHHmmss");
			
			// TASK 테이블 컬럼
			String jobId = Integer.toString(scheduler.getJob_id());
			String taskId = String.format("%s_%s_%s_%s", scheduler.getEtl_tgt_sys(), scheduler.getEtl_tgt_tbl(), jobId, curr);
			String agentId = scheduler.getAgent_id();
			String cron_expr = scheduler.getCron_expr();
			String execProg = scheduler.getExec_prog();
			String paramStr = scheduler.getParam_str();
			String optionStr = scheduler.getOption_str();
			String etlTgtSys = scheduler.getEtl_tgt_sys();
			String etlTgtTbl = scheduler.getEtl_tgt_tbl();
			String lstChgDatCond = scheduler.getLst_chg_dat_cond();
			String jobType = scheduler.getJob_type();
			
			// RC_BD_META_TABLE_INFO 
			Scheduler meta_table = schedulerDao.getMetaTable(scheduler);
			
			String loadType = "";
			String chgDatBasCol = "";
			String chgDatCondPtrn = "";
			if(null != meta_table) {
				loadType = meta_table.getLoad_type();
				chgDatBasCol = meta_table.getChg_dat_bas_col();
				chgDatCondPtrn = meta_table.getChg_dat_cond_ptrn();
			}
			
			String statCd = "EX";
			// DB에서 agent_id로 에이전트 정보(ip, host) 가져오기
			Scheduler agent_info = null;
			agent_info = schedulerDao.getAgentInfo(scheduler);
			
			String agentIp = agent_info.getAgent_ip();
			String agentHost = agent_info.getAgent_host();
			String sendStr = String.format("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s", jobId, taskId, agentId, execProg, agentIp, agentHost, etlTgtSys, etlTgtTbl, loadType, lstChgDatCond, chgDatBasCol, chgDatCondPtrn, paramStr, optionStr, jobType, statCd, curr);
			
			logger.debug("#########################################################################");
			logger.debug("Job ID             : " + jobId);
			logger.debug("Task ID            : " + taskId);
			logger.debug("Agent ID           : " + agentId);
			logger.debug("Exec Program       : " + execProg);
			logger.debug("Agent IP           : " + agentIp);
			logger.debug("Agent Host         : " + agentHost);
			logger.debug("Target ETL System  : " + etlTgtSys);
			logger.debug("Target ETL Table   : " + etlTgtTbl);
			logger.debug("Load Type          : " + loadType);
			logger.debug("Last Change        : " + lstChgDatCond);
			logger.debug("ETL Base col       : " + chgDatBasCol);
			logger.debug("Col Pattern        : " + chgDatCondPtrn);
			logger.debug("Parameter String   : " + paramStr);
			logger.debug("Option String      : " + optionStr);
			logger.debug("Job Type           : " + jobType);
			logger.debug("Status             : " + statCd);
			logger.debug("Send Time          : " + curr);
			logger.debug("#########################################################################");
			
			// RC_BD_SCHD_TASK 입력
			task.setTask_id(taskId);
			task.setJob_id(jobId);
			task.setCron_expr(cron_expr);
			task.setExec_prog(execProg);
			if(task.getParam_str() != null) {
				task.setParam_str(paramStr);
			}
//			task.setParam_str(paramStr);
			if(task.getOption_str() != null) {
				task.setOption_str(optionStr);
			}
			task.setEtl_tgt_sys(etlTgtSys);
			task.setEtl_tgt_tbl(etlTgtTbl);
			if(task.getLst_chg_dat_cond() != null) {
				task.setLst_chg_dat_cond(lstChgDatCond);
			}
			task.setReg_ddtm(curr);
			task.setJob_type(jobType);
			
			// TASK 테이블 INSERT
			result = schedulerDao.insertRedo(task);
			
			// 카프카 메시지 전송
			kpm.send("__AGENT_CMD__", sendStr);
			kpm.flush();
			
			// RC_BD_SCHD_EXEC_HIS 수정
			task.setStts_cd("S");
			task.setStts_dtl("메시지 전송");
			task.setStrt_ddtm(curr);
			
			result += schedulerDao.updateJobStatus(task);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;		
	}
	
	/* 스키마 단위 ETL 수동실행 */
	@Transactional
	@Override
	public int etlExecute(Scheduler scheduler) {
		int result = 0;
		
		
		// 해당 시스템의 ETL 잡 모두 가져오기
		try {
			List<Scheduler> list =  schedulerDao.etlList(scheduler);
			
			// 리스트 목록 카프카로 전송
			for(int i=0; i<list.size(); i++) {
				insertRedo(list.get(i));
			}
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 3;
		}
	}
}
