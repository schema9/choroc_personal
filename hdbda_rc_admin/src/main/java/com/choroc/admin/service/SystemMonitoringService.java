package com.choroc.admin.service;

import java.sql.SQLException;
import java.util.List;

import com.choroc.admin.domain.MonitoringParam;
import com.choroc.admin.dto.Monitoring;
import com.choroc.admin.dto.Scheduler;

public interface SystemMonitoringService {
	
	public int selectMonitoringTotalCount (MonitoringParam param) throws SQLException;
	
	public List<Monitoring> selectMonitoringList (MonitoringParam param) throws SQLException;
	
	public Scheduler schedulerDetailView(MonitoringParam param) throws SQLException;
}
