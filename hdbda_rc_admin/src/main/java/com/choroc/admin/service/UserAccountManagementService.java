package com.choroc.admin.service;

import java.util.List;
import java.util.Map;

import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.dto.AccountInfo;
import com.choroc.admin.dto.ServiceInfo;
import com.choroc.admin.util.ZValue;

/**
 * TODO  > 권한 관리 > 사용자 관리 > 사용자 계정 관리
 * 파일명   > UserAccountManagementService.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 8.
 * 수정일   > 
 * 수정자   > 
 */

public interface UserAccountManagementService {
	
	/*사용자 계정 목록*/
	public List<AccountInfo> selectUserAccountRows(AccountParam param);
	
	/*사용자 계정 상세*/
	public AccountInfo selectUserAccountRow(String param);
	
	public List<ServiceInfo> selectServiceRowsByDomainID(int param);
	
	/*선택된 사용자의 서비스 목록*/
	public List<ServiceInfo> selectServiceRowsByUserID(String param);
	
	/*사용자 계정 총 수*/
	public int selectUserAccountTotalCount(AccountParam param);
    
	/*사용자 계정 유효 계정 수*/
	public int selectUserAccountValidUserCount(String param);
    
	/*사용자 계정 등록*/
	public int userAccountAdd(AccountInfo accountInfo);
    
	/*사용자 계정 수정*/
	public int userAccountModify(AccountInfo accountInfo);
    
	/*사용자 계정 사용제어 변경*/
	public int updateUserAccountUseYn(Map<String, Object> param);
    
	/*사용자 비밀번호 수정*/
	public int updateUserAccountUserPassword(Map<String, Object> param);

    /*사용자가 가입 후 또는 비밀번호 변경 후 최초 로그인일 경우 비밀번호를 변경한다.*/
    public int modifyFirstLoginPassword(ZValue zValue) throws Exception;

    /*비밀번호 변경 기간이 로그인한 일로 3개월 이상 일때 처리 로직 팝업으로 공지후 연장 처리*/
    public int modifyPasswordPeriod(AccountParam param) throws Exception;
    
    /*기존 비밀번호 검사*/
    public int getPasswordCheck(AccountParam param) throws Exception;

    /*비밀번호 수정*/
    public int modifyUserPassword(AccountParam param);
    
    /*아이디 중복 검사*/
	public int selectAccountUserIdCountForValidKey(AccountParam param);
	/*권한코드 조회*/
	public List<AccountInfo> selectAuthCd(AccountParam param);
	/*관리자를 통한 유저 생성*/
	public int createAccountUser(AccountParam param);
	/*비밀번호 초기화*/
	public int userPwdReset(AccountParam param);
	/*관리자를 통한 사용자 정보변경*/
	public int userModifySet(AccountParam param);
	/*비밀번호 변경시, 히스토리 테이블에서 예전에 사용하던 비밀번호인지 조회*/
	public int selectUserPwdHis(AccountParam param);
}
