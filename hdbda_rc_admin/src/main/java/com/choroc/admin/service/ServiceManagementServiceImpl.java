package com.choroc.admin.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.choroc.admin.domain.ServiceParam;
import com.choroc.admin.domain.ServiceResult;
import com.choroc.admin.util.ZValue;


/**
 * TODO >
 * 파일명   > ServiceManagementServiceImpl.java
 * 작성일   > 2018. 11. 22.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
@Service
public class ServiceManagementServiceImpl implements ServiceManagementService {

	@Override
	public List<ServiceResult> getServiceList(ServiceParam param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getServiceListTotalCount(ServiceParam param) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ZValue getService(ZValue zvl) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int createService(ZValue zvl) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int modifyService(ZValue zvl) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int removeService(ZValue zvl) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int restoreService(ZValue zvl) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<ZValue> getAllAlgorithmList(ZValue zvl) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
