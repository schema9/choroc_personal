package com.choroc.admin.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.choroc.admin.domain.SystemAlgorithmParam;
import com.choroc.admin.dto.SystemAlgorithmInfo;


/**
 * TODO  > 추천 환경 관리 > 추천 위젯 설정
 * 파일명   > SystemAlgorithmServiceImpl.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 8.
 * 수정일   > 
 * 수정자   > 
 */
@Service
public class SystemAlgorithmServiceImpl implements SystemAlgorithmService {

	@Override
	public List<SystemAlgorithmInfo> selectRecoAlgoMgmtRows(SystemAlgorithmParam param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SystemAlgorithmInfo selectRecoAlgoMgmtRow(String algoId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int selectRecoAlgoMgmtTotalCount(SystemAlgorithmParam param) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int selectRecoAlgoMgmtCountForValidKey(String algoId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insertRecoAlgoMgmtRow(SystemAlgorithmInfo param) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateRecoAlgoMgmtRow(SystemAlgorithmInfo param) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteRecoAlgoMgmtRow(SystemAlgorithmInfo param) {
		// TODO Auto-generated method stub
		return 0;
	}
	
/*	
	@Autowired
	private SystemAlgorithmDao mySqlSystemAlgorithmDao;

	//추천 알고리즘 관리 목록
	@Override
	public List<SystemAlgorithmInfo> selectRecoAlgoMgmtRows(SystemAlgorithmParam param) {
		List<SystemAlgorithmInfo> result = null;
		try {
			result = mySqlSystemAlgorithmDao.selectRecoAlgoMgmtRows(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		 return result;
	}


	// 추천 알고리즘 관리 상세
	@Override
	public SystemAlgorithmInfo selectRecoAlgoMgmtRow(String algoId) {
        SystemAlgorithmInfo result = null;
		try {
			result = mySqlSystemAlgorithmDao.selectRecoAlgoMgmtRow(algoId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	// 추천 알고리즘 관리 총 수
	@Override
	public int selectRecoAlgoMgmtTotalCount(SystemAlgorithmParam param) {
		int result = 0;
		try {
			result = mySqlSystemAlgorithmDao.selectRecoAlgoMgmtTotalCount(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}


	//추천 알고리즘 관리 중복 검사
	@Override
	public int selectRecoAlgoMgmtCountForValidKey(String algoId) {
		int result = -1;
		try {
			result = mySqlSystemAlgorithmDao.selectRecoAlgoMgmtCountForValidKey(algoId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	// 추천 알고리즘 관리 등록
	@Override
	public int insertRecoAlgoMgmtRow(SystemAlgorithmInfo param)  {
		int result = 0;
		try {
			result = mySqlSystemAlgorithmDao.insertRecoAlgoMgmtRow(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}


	//천 알고리즘 관리 수정
	@Override
	public int updateRecoAlgoMgmtRow(SystemAlgorithmInfo param)  {
		int result = 0;
		try {
			result = mySqlSystemAlgorithmDao.updateRecoAlgoMgmtRow(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}


	//추천 알고리즘 관리 삭제
	@Override
	public int deleteRecoAlgoMgmtRow(SystemAlgorithmInfo param)  {
		int result = 0;
		try {
			result = mySqlSystemAlgorithmDao.deleteRecoAlgoMgmtRow(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}*/

}
