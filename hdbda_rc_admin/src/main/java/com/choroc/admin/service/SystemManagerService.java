package com.choroc.admin.service;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.choroc.admin.domain.SystemManagerMultiParam;
import com.choroc.admin.domain.SystemManagerParam;
import com.choroc.admin.dto.ServiceInfo;
import com.choroc.admin.dto.SystemManagerInfo;

/**
 * TODO  > 시스템 관리 > 메타 관리
 * 파일명   > UserAccountManagementService.java
 * 작성자   > ljy
 * 작성일   > 2018. 11. 29.
 * 수정일   > 
 * 수정자   > 
 */

public interface SystemManagerService {
	
	/*사용자 계정 목록*/
	public List<SystemManagerInfo> selectMetaDataRows(SystemManagerParam param);
	
	/*사용자 계정 총 수*/
	public int selectMetaDataTotalCount(SystemManagerParam param);
	
	/*사용자 계정 총 수*/
	public int insertMetaDataRows(SystemManagerParam param);
	public int insertMetaDataColumnRows(SystemManagerMultiParam param);
	public int updateMetaDataRows(SystemManagerParam param);
	public int updateMetaDataColumnRows(SystemManagerMultiParam param);
	public int deleteMetaDataRows(SystemManagerParam param);
	public List<SystemManagerInfo> selectMetaDataDetail(SystemManagerParam param);
	public int tableDataEarlyLoading(SystemManagerParam param);
}
