package com.choroc.admin.service;

import java.util.Map;

public interface ExcelService {
	
	public Map<String, Object> mappedExcelList(Map<String, Object> param);

	public Map<String, Object> mappedExcelListFromJson(Map<String, Object> param);
}
