package com.choroc.admin.service;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.choroc.admin.dao.AlgorithmDao;
import com.choroc.admin.domain.CommonCodeParam;
import com.choroc.admin.dto.CommonCodeInfo;

/**
 * TODO  > 시스템 관리 > 추천 알고리즘 타입 관리
 * 파일명   > CommonCodeManagementServiceImpl.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 9.
 * 수정일   > 
 * 수정자   > 
 */
@Service
public class CommonCodeManagementServiceImpl implements CommonCodeManagementService {

	private static final Logger logger = LoggerFactory.getLogger(CommonCodeManagementServiceImpl.class);
	
	@Inject
	private AlgorithmDao algorithmDao;
	
	@Override
	public List<CommonCodeInfo> selectCommonCodeRows(CommonCodeParam param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CommonCodeInfo selectCommonCodeRow(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int selectCommonCodeTotalCount(CommonCodeParam param) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int selectCommonCodeCountForValidKey(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int selectCommonCodeCountForUsing(String param) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CommonCodeInfo> selectCommonGroupRows(String groupCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int recoAlgoTypeModifyRow(CommonCodeInfo param) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insertCommonCodeRow(CommonCodeInfo param) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	
/*
	@Autowired
	private CommonCodeManagementDao mySqlCommonCodeManagementDao;
	
	//공통 코드 목록
	@Override
	public List<CommonCodeInfo> selectCommonCodeRows(CommonCodeParam param) {
		List<CommonCodeInfo> result = null;
		try {
			result = mySqlCommonCodeManagementDao.selectCommonCodeRows(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	// 공통 코드 상세
	@Override
	public CommonCodeInfo selectCommonCodeRow(Map<String, Object> param) {
		CommonCodeInfo result = null;
		try {
			result = mySqlCommonCodeManagementDao.selectCommonCodeRow(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	// 공통 코드 총 수
	@Override
	public int selectCommonCodeTotalCount(CommonCodeParam param) {
		int result = 0;
		try {
			result = mySqlCommonCodeManagementDao.selectCommonCodeTotalCount(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	// 공통 코드 중복 검사
	@Override
	public int selectCommonCodeCountForValidKey(Map<String, Object> param) {
		int result = -1;
		try {
			result = mySqlCommonCodeManagementDao.selectCommonCodeCountForValidKey(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	// 선택된 공통 코드를 사용하고 있는지 확인 (추천 알고리즘 타입)
	@Override
	public int selectCommonCodeCountForUsing(String param) {
		int result = -1;
		try {
			result = mySqlCommonCodeManagementDao.selectCommonCodeCountForUsing(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	//공통 코드 목록 (그룹 선택)
	@Override
	public List<CommonCodeInfo> selectCommonGroupRows(String groupCode) {
		List<CommonCodeInfo> result = null;
		try {
			result = mySqlCommonCodeManagementDao.selectCommonGroupRows(groupCode);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	// 공통 코드 수정 (수정,삭제 : 추천 알고리즘 타입)
	@Override
	public int recoAlgoTypeModifyRow(CommonCodeInfo commonCodeInfo) {
		int result = 0;
		int onUseCodeCount = 0;
		boolean isDeleteMode
				= commonCodeInfo.getControlMode().toUpperCase().equals("D") ? true : false;
		try {
			if (StringUtil.isNotEmpty(commonCodeInfo.getCode())
					&& StringUtil.isNotEmpty(commonCodeInfo.getModifyId())) {
				onUseCodeCount
						= mySqlCommonCodeManagementDao.selectCommonCodeCountForUsing(commonCodeInfo.getCode());

				if (isDeleteMode && onUseCodeCount == 0) {
					result = mySqlCommonCodeManagementDao.deleteCommonCodeRow(commonCodeInfo);
				} else if (!isDeleteMode) {
					result = mySqlCommonCodeManagementDao.updateCommonCodeRow(commonCodeInfo);
				}
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}


    // 공통 코드 등록 (추천 알고리즘 타입)
	@Override
	public int insertCommonCodeRow(CommonCodeInfo param) {
		int result = 0;
		try {
			result = mySqlCommonCodeManagementDao.insertCommonCodeRow(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	*/
}
