package com.choroc.admin.service;

import java.sql.SQLException;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.choroc.admin.dao.UserDao;
import com.choroc.admin.domain.AuthUserParam;
import com.choroc.admin.domain.LoginParam;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.util.ZValue;

/**
 * TODO > 로그인 관련 서비스 
 * 파일명   > UserServiceImpl.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 9.
 * 수정일   > 
 * 수정자   > 
 */
@Service
public class UserServiceImpl implements UserService {

	@Inject
	private UserDao userDao;	
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Override
	public AuthUser getLoginUser(LoginParam param) {

		AuthUserParam authUserParam = new AuthUserParam();
		AuthUser authUser = new AuthUser();
		AuthUser result = null;
		
		authUser.setUserId(param.getLoginUserId());
		authUser.setUserPassword(param.getLoginUserPassword());
		authUserParam.setAuthUser(authUser);
		
		try {	
			result = userDao.selectLoginUser(authUserParam);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;				
	}
	
	// 로그인 성공시 정보 업데이트	
	@Override
	public void loginSuccessProcess(ZValue zvl) {
		try {
			userDao.loginSuccessProcess(zvl);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// 로그인 성공시 정보 업데이트(마지막 로그인 날짜 제외)
	@Override
	public void loginSuccessProcessExcludeLoginDt(ZValue zvl) {
		try {
			userDao.loginSuccessProcessExcludeLoginDt(zvl);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	// 로그인 실패횟수 업데이트 
	@Override
	public int loginFaileCount(ZValue zvl) {
		
		int result = 0;
		
		try {
			result = userDao.loginFaileCount(zvl);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	
	
	// 계정 잠금 및 실패 횟수 초기화
	@Override
	public int acntLockLimitExpirationProcess(ZValue zvl) {

		int result = 0;

		try {
			result = userDao.acntLockLimitExpirationProcess(zvl);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}
	
	
	
	@Override
	public String loginFaileProcess(ZValue zvl) {

		String result = "";

/*		try {
			mySqlUserDao.loginFaileProcess(zvl);
			result = mySqlUserDao.selectUserIdLockYn(zvl);
		} catch (SQLException e) {
			e.printStackTrace();
		}
*/
		return result;
	}




	
	

/*	@Override
	public String getEncryptedPassword(LoginParam param) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int loginSuccessProcess(ZValue zvl) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int acntLockLimitExpirationProcess(ZValue zvl) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<AuthUser> getLockMember() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int modifyLockMember(AuthUser param) {
		// TODO Auto-generated method stub
		return 0;
	}

	*/
	
	/////////////////////   예전 코드   ///////////////////////////////////////////////////////////// 
	
/*	@Autowired
	private UserDao mySqlUserDao;
	
	@Override
	public AuthUser getLoginUser(LoginParam param) {
		AuthUserParam authUserParam = new AuthUserParam();
		AuthUser authUser = new AuthUser();

		authUser.setUserPassword(param.getLoginUserPassword());
		authUser.setUserId(param.getLoginUserId());
		authUserParam.setAuthUser(authUser);
		
		AuthUser result = null;
		try {
			result = mySqlUserDao.selectLoginUser(authUserParam);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String getEncryptedPassword(LoginParam param) {
		AuthUserParam authUserParam = new AuthUserParam();
		AuthUser authUser = new AuthUser();

		authUser.setUserPassword(param.getLoginUserPassword());
		authUser.setUserId(param.getLoginUserId());
		authUserParam.setAuthUser(authUser);
		
		String result = null;
		try {
			result = mySqlUserDao.selectEncryptedPassword(authUserParam);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int loginSuccessProcess(ZValue zvl) {

		int result = 0;

		try {
			result = mySqlUserDao.loginSuccessProcess(zvl);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}


	@Override
	public List<AuthUser> getLockMember() {
		List<AuthUser> result = null;
		try {
			result = mySqlUserDao.selectLockMember();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int modifyLockMember(AuthUser param) {

		int result = 0;

		try {
			result = mySqlUserDao.updateLockMember(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}*/
}
