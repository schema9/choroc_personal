package com.choroc.admin.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.choroc.admin.dao.RecommendAreaDao;
import com.choroc.admin.dao.RecommendAreaDaoImpl;
import com.choroc.admin.domain.RecommendAreaParam;
import com.choroc.admin.dto.Algorithm;
import com.choroc.admin.dto.RecommendAreaInfo;


/**
 * TODO  > 추천환경관리 > 추천알고리즘 설정 
 * 파일명   > RecommendAreaServiceImpl.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 8.
 * 수정일   > 
 * 수정자   > 
 */

@Service
public class RecommendAreaServiceImpl implements RecommendAreaService {

	private static final Logger logger = LoggerFactory.getLogger(RecommendAreaServiceImpl.class);
	@Inject
	private RecommendAreaDao recommendAreaDao; 
	

	
	@Override
	public List<RecommendAreaInfo> getRecommendAreaList(RecommendAreaParam param) throws SQLException {
		logger.info("추천영역-알고리즘&페이지영역 테이블 가져오기");
		List<RecommendAreaInfo> resultList = null;
		resultList = recommendAreaDao.getRecommendAreaList(param);
		return resultList;
	}

	@Override
	public int getRecommendAreaCount(RecommendAreaParam param) throws SQLException {
		int result;
		result = recommendAreaDao.getRecommendAreaCount(param);
		return result;
	}
	
	
	@Override
	public List<Algorithm> getAlgorithmList() throws SQLException{
		List<Algorithm> dto;
		dto = recommendAreaDao.getAlgorithmList();
		return dto;
	}
	
	
	@Override
	public String checkRecommendArea(RecommendAreaParam param) throws SQLException{
		String msg = null;
			int result = recommendAreaDao.checkRecommendArea(param);
			if(result == 1) {
				msg = "중복";
			}else {
				msg = "사용 가능";
			}
		return msg;
	}
	
	@Override
	public int insertRecommendArea(RecommendAreaParam param, HttpServletRequest request) throws SQLException{
		int result =0;
		param.setPge_area_id(request.getParameter("pge_area_id"));
		param.setPge_area_nm(request.getParameter("pge_area_nm"));
		param.setPge_area_dc(request.getParameter("pge_area_dc"));
		param.setUse_yn(request.getParameter("use_yn"));
		param.setAlgrth_id(request.getParameter("algrth_id"));
		result = recommendAreaDao.insertRecommendArea(param);
		return result;
	}
		
	@Override
	public RecommendAreaInfo selectOneRecommendArea(RecommendAreaParam param) throws SQLException {
		RecommendAreaInfo dto;
		dto =  recommendAreaDao.selectOneRecommendArea(param);
		return dto;
	}

	@Override
	public int updateRecommendArea(RecommendAreaParam param, HttpServletRequest request) throws SQLException {
		logger.info("추천영역-추천화면 추가등록-페이지영역 테이블 등록/수정 하기");
		param.setPge_area_id(request.getParameter("pge_area_id"));
		param.setPge_area_nm(request.getParameter("pge_area_nm"));
		param.setPge_area_dc(request.getParameter("pge_area_dc"));
		param.setUse_yn(request.getParameter("use_yn"));
		param.setAlgrth_id(request.getParameter("algrth_id"));
		int result = recommendAreaDao.updateRecommendArea(param);
		return result;
	}

/*	
	@Override
	public int deleteRecommendArea(RecommendAreaInfo info) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}
*/  
}
