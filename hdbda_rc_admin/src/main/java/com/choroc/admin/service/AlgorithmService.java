package com.choroc.admin.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.choroc.admin.domain.AlgorithmParam;
import com.choroc.admin.dto.Algorithm;

/**
 * TODO > 알고리즘탭 리스트 조회, 알고리즘 필터 중복확인 , 등록, 수정 
 * 파일명   > AlgorithmService.java
 * 작성일   > 2018. 11. 22.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public interface AlgorithmService {

	// 알고리즘탭 리스트
	public List<Algorithm> getAlgorithmList(AlgorithmParam param) throws SQLException;
	
	// 알고리즘 전체 수
	int selectAlgorithmTotalCount(AlgorithmParam param);
	
	// 알고리즘 중복 체크
	String checkAlgorithm(Map<String, Object> paramMap);
	
	// 알고리즘 추가
	int insertAlgorithm(Map<String, Object> paramMap, HttpServletRequest request);
	
	// 알고리즘 수정
	int updateAlgorithm(Map<String, Object> paramMap, HttpServletRequest request);
	
	// 수정할 알고리즘 1개 불러오기
	public Algorithm selectOneAlgorithm(Map<String, Object> paramMap, HttpServletRequest request) throws SQLException;
	
}
