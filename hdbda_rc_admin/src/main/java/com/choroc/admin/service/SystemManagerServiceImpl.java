package com.choroc.admin.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.choroc.admin.dao.SystemManagerDao;
import com.choroc.admin.dao.UserAccountManagementDao;
import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.domain.SystemManagerMultiParam;
import com.choroc.admin.domain.SystemManagerParam;
import com.choroc.admin.dto.AccountInfo;
import com.choroc.admin.dto.Scheduler;
import com.choroc.admin.dto.ServiceInfo;
import com.choroc.admin.dto.SystemManagerInfo;

/**
 * TODO  > 권한 관리 > 사용자 관리 > 사용자 계정 관리
 * 파일명   > UserAccountManagementServiceImpl.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 8.
 * 수정일   > 
 * 수정자   > 
 */

@Service
public class SystemManagerServiceImpl  implements SystemManagerService {

	@Autowired
	private SystemManagerDao systemManagerDao;
	
	@Autowired
	private SchedulerServiceImpl ssi = new SchedulerServiceImpl();
	
	private static Logger log = LoggerFactory.getLogger(SystemManagerServiceImpl.class);

	/*메타데이터 목록*/
	@Override
	public List<SystemManagerInfo> selectMetaDataRows(SystemManagerParam param) {
		try {
				return systemManagerDao.selectSystemManagerRows(param);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/*메타데이터 총 수*/
	@Override
	public int selectMetaDataTotalCount(SystemManagerParam param) {
		try {
			return systemManagerDao.selectSystemManagerTotalCount(param);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}		
	
	
	
	/*메타등록*/
	@Override
	@Transactional
	public int insertMetaDataRows(SystemManagerParam param) {
		Map<String,Object> listMap = new HashMap<String, Object>();		
		HashMap<String, Object> staffMap;
		List<Map<String, Object>> staffList = new ArrayList<Map<String, Object>>();
		String sysNms = "";
		String enTblNms = "";
		String enColNm = "";
		String krColNm = "";
		String oraDatType = "";
		String hvdatType = "";
		String kdDatType = "";
		String pkYn = "";
		String pkOrdr = "";
		String nullable = "";
		String ptntColYn = "";
		String ptntColConvSntx = "";
		String sstvInfColYn = "";
		String sstvInfType = "";
		String chgDatBasColYn = "";
		String colOrdr = "";
		
		// pkYn 배열 안에 한 개이상의 Y가 있으면 HAS_PK_YN(기본키존재여부) Y값으로 변경
		param.setHasPkYn("N");
		String[] pkYnArray = param.getPkYn();
		String y = "Y";
		for(int j=0; j<pkYnArray.length; j++) {
			if(pkYnArray[j].equals(y)) {
				param.setHasPkYn(y);
				break;
			}
		}

		for (int i=0;i<param.getColOrdr().length-1;i++) {
			sysNms = param.getSysNms()[i];
			enTblNms = param.getEnTblNms()[i];
			enColNm = param.getEnColNm()[i];
			krColNm = param.getKrColNm()[i];
			oraDatType = param.getOraDatType()[i];
			hvdatType = param.getHvdatType()[i];
			kdDatType = param.getKdDatType()[i];
			pkYn = param.getPkYn()[i];
			pkOrdr = param.getPkOrdr()[i];
			nullable = param.getNullable()[i];
//			ptntColYn = param.getPtntColYn()[i];
//			ptntColConvSntx = param.getPtntColConvSntx()[i];
			sstvInfColYn = param.getSstvInfColYn()[i];
			sstvInfType = "";
			chgDatBasColYn = "";
			colOrdr = param.getColOrdr()[i];
			
			staffMap = new HashMap<String, Object>();
			staffMap.put("sysNms",sysNms);
			staffMap.put("enTblNms",enTblNms);
			staffMap.put("enColNm",enColNm);
			staffMap.put("krColNm",krColNm);
			staffMap.put("oraDatType",oraDatType);
			staffMap.put("hvdatType",hvdatType);
			staffMap.put("kdDatType",kdDatType);
			staffMap.put("pkYn",pkYn);
			staffMap.put("pkOrdr",pkOrdr);
			staffMap.put("nullable",nullable);
			staffMap.put("ptntColYn",ptntColYn);
			staffMap.put("ptntColConvSntx",ptntColConvSntx);
			staffMap.put("sstvInfColYn",sstvInfColYn);
			staffMap.put("sstvInfType",sstvInfType);
			staffMap.put("chgDatBasColYn",chgDatBasColYn);
			staffMap.put("colOrdr",colOrdr);	
			staffList.add(staffMap);
		}
		listMap.put("list", staffList);
		// TODO Auto-generated method stub
		try {
			systemManagerDao.insertMetaDataTable(param);
			systemManagerDao.insertMetaDataColumnRows(listMap);
			return 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 3;
		}
	}

	@Override
	public int insertMetaDataColumnRows(SystemManagerMultiParam param) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/*메타수정*/
	@Override
	@Transactional
	public int updateMetaDataRows(SystemManagerParam param) {
		Map<String,Object> listMap = new HashMap<String, Object>();		
		HashMap<String, Object> staffMap;
		List<Map<String, Object>> staffList = new ArrayList<Map<String, Object>>();

		String sysNms = "";
		String enTblNms = "";
		String enColNm = "";
		String krColNm = "";
		String oraDatType = "";
		String hvdatType = "";
		String kdDatType = "";
		String pkYn = "";
		String pkOrdr = "";
		String nullable = "";
		String ptntColYn = "";
		String ptntColConvSntx = "";
		String sstvInfColYn = "";
		String sstvInfType = "";
		String chgDatBasColYn = "";
		String colOrdr = "";
		
		for (int i=0;i<param.getColOrdr().length;i++) {
			 sysNms = param.getSysNms()[i];
			 enTblNms = param.getEnTblNms()[i];
			 enColNm = param.getEnColNm()[i];
			 krColNm = param.getKrColNm()[i];
			 oraDatType = param.getOraDatType()[i];
			 hvdatType = param.getHvdatType()[i];
			 kdDatType = param.getKdDatType()[i];
			 pkYn = param.getPkYn()[i];
			 pkOrdr = param.getPkOrdr()[i];
			 nullable = param.getNullable()[i];
//			 ptntColYn = param.getPtntColYn()[i];
//			 ptntColConvSntx = param.getPtntColConvSntx()[i];
			 sstvInfColYn = param.getSstvInfColYn()[i];
			 sstvInfType = "";
			 chgDatBasColYn = "";
			 colOrdr = param.getColOrdr()[i];
			 
			 if(sysNms.equals("null")) sysNms = "";
			 if(enTblNms.equals("null")) enTblNms = "";
			 if(enColNm.equals("null")) enColNm = "";
			 if(krColNm.equals("null")) krColNm = "";
			 if(oraDatType.equals("null")) oraDatType = "";
			 if(hvdatType.equals("null")) hvdatType = "";
			 if(kdDatType.equals("null")) kdDatType = "";
			 if(pkYn.equals("null")) pkYn = "";
			 if(pkOrdr.equals("null")) pkOrdr = "";
			 if(nullable.equals("null")) nullable = "";
			 if(ptntColYn.equals("null")) ptntColYn = "";
			 if(ptntColConvSntx.equals("null")) ptntColConvSntx = "";
			 if(sstvInfColYn.equals("null")) sstvInfColYn = "";
			 if(colOrdr.equals("null")) colOrdr = "";
			 
			staffMap = new HashMap<String, Object>();
			staffMap.put("sysNms",sysNms);
			staffMap.put("enTblNms",enTblNms);
			staffMap.put("enColNm",enColNm);
			staffMap.put("krColNm",krColNm);
			staffMap.put("oraDatType",oraDatType);
			staffMap.put("hvdatType",hvdatType);
			staffMap.put("kdDatType",kdDatType);
			staffMap.put("pkYn",pkYn);
			staffMap.put("pkOrdr",pkOrdr);
			staffMap.put("nullable",nullable);
			staffMap.put("ptntColYn",ptntColYn);
			staffMap.put("ptntColConvSntx",ptntColConvSntx);
			staffMap.put("sstvInfColYn",sstvInfColYn);
			staffMap.put("sstvInfType",sstvInfType);
			staffMap.put("chgDatBasColYn",chgDatBasColYn);
			staffMap.put("colOrdr",colOrdr);	
			staffList.add(staffMap);
		}
		listMap.put("list", staffList);
		// TODO Auto-generated method stub
		try {
			systemManagerDao.updateMetaDataTable(param);
			systemManagerDao.deleteMetaDataColumnRows(param);
			systemManagerDao.insertMetaDataColumnRows(listMap);
			//systemManagerDao.updateMetaDataColumnRows(listMap);
			return 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 3;
		}
	}

	@Override
	public int updateMetaDataColumnRows(SystemManagerMultiParam param) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/*메타삭제 전체*/
	@Override
	@Transactional
	public int deleteMetaDataRows(SystemManagerParam param) {
		// TODO Auto-generated method stub
		try {
			systemManagerDao.deleteMetaDataTable(param);
			systemManagerDao.deleteMetaDataColumnRows(param);
			return 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 3;
		}
	}

	/*메타데이터 목록*/
	@Override
	public List<SystemManagerInfo> selectMetaDataDetail(SystemManagerParam param) {
		try {
				return systemManagerDao.selectMetaDataDetail(param);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/* 테이블 데이터 초기 적재 메시지 전송 로직 */
	@Override
	@Transactional
	public int tableDataEarlyLoading (SystemManagerParam param) {
		
		try {
			// RC_BD_SCHD_JOB 테이블의 job_lst_chg_dat_cond 를 null 로 업데이트
			int result = systemManagerDao.jobLstChgDatCondNull(param.getEnTblNm());
			
			if(result==1) {
				// 스케줄러 dto에 필요한 파라미터들을 RC_BD_SCHD_JOB 테이블에서 가져와 담아줌
				Scheduler schd = systemManagerDao.jobInfo(param.getEnTblNm());
				
				// 카프카 메시지 전송 (재적재)
				ssi.insertRedo(schd);
				
				return 1;
			}else {
				return 3;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return 3;
		}
	}
}
