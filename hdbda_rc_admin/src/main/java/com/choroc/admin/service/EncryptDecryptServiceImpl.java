package com.choroc.admin.service;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * TODO  > 암호화 및 복호화
 * 파일명   > EncryptDecryptServiceImpl.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 9.
 * 수정일   > 
 * 수정자   > 
 */
@Service
public class EncryptDecryptServiceImpl implements EncyptDecryptService {

	private static Logger log = LoggerFactory.getLogger(EncryptDecryptServiceImpl.class);

	@Override
	public String encryptString(String plainText) {
		String result = null;
		if (plainText != null && plainText.length() > 0) {
			try {
				result = encrypt(plainText, EncyptDecryptService.ENC_DEC_KEY);
			} catch (Exception e) {
				log.error(e.getLocalizedMessage());
			}
		}
		return result;
	}

	@Override
	public String decryptString(String encryptedText) {
		String result = null;
		if (encryptedText != null && encryptedText.length() > 0) {
			try {
				result = decrypt(encryptedText, EncyptDecryptService.ENC_DEC_KEY);
			} catch (Exception e) {
				log.error(e.getLocalizedMessage());
			}
		}
		return result;
	}

	/**
	 * AES Decryption
	 * 
	 * @param text
	 * @param key
	 * @return
	 * @throws Exception
	 */
	private String decrypt(String text, String key) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		byte[] keyBytes = new byte[16];
		byte[] b = key.getBytes("UTF-8");
		int len = b.length;
		if (len > keyBytes.length)
			len = keyBytes.length;
		System.arraycopy(b, 0, keyBytes, 0, len);
		SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
		cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

		byte[] results = null;
		//byte[] results = cipher.doFinal(Base64.decodeBase64(text));
		return new String(results, "UTF-8");
	}

	/**
     * AES Encryption
     * @param text
     * @param key
     * @return
     * @throws Exception
     */
    private String encrypt(String text, String key) throws Exception
    {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] keyBytes= new byte[16];
        byte[] b= key.getBytes("UTF-8");
        int len= b.length;
        if (len > keyBytes.length) len = keyBytes.length;
        System.arraycopy(b, 0, keyBytes, 0, len);
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
        cipher.init(Cipher.ENCRYPT_MODE,keySpec,ivSpec);

        byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
        return new String(results);
        //return new String(Base64.encodeBase64(results));
    }
}
