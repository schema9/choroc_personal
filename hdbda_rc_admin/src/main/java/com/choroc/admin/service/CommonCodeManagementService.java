package com.choroc.admin.service;

import java.util.List;
import java.util.Map;

import com.choroc.admin.domain.CommonCodeParam;
import com.choroc.admin.dto.CommonCodeInfo;

public interface CommonCodeManagementService {
	
	public List<CommonCodeInfo> selectCommonCodeRows(CommonCodeParam param);

	public CommonCodeInfo selectCommonCodeRow(Map<String, Object> param);

	public int selectCommonCodeTotalCount(CommonCodeParam param);

	public int selectCommonCodeCountForValidKey(Map<String, Object> param);

	public int selectCommonCodeCountForUsing(String param);

	public List<CommonCodeInfo> selectCommonGroupRows(String groupCode);

	public int recoAlgoTypeModifyRow(CommonCodeInfo param);

	public int insertCommonCodeRow(CommonCodeInfo param);
}
