package com.choroc.admin.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.choroc.admin.domain.AuthUserParam;
import com.choroc.admin.domain.MenuParam;
import com.choroc.admin.dto.DomainInfo;
import com.choroc.admin.dto.Menu;
import com.choroc.admin.dto.MenuInfo;
import com.choroc.admin.dto.MenuMap;
import com.choroc.admin.dto.RoleMenuMapping;
import com.choroc.admin.util.ZValue;

/**
 * TODO  > 권한 관리 > 사용자 관리 > 사용자 메뉴 권한 관리
 * 파일명   > MenuService.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 9.
 */
public interface MenuService {

	/*메뉴 리스트*/
	List<ZValue> selectMenuList(ZValue zvl) throws Exception;
	
	/*메뉴 목록*/
	public List<MenuInfo> selectMenuRows(MenuParam param);
	
	/*유효한 모든 메뉴 목록*/
	public List<MenuInfo> selectAllMenu();
	
	/*선택한 상위 메뉴포함 목록*/
	public List<MenuInfo> selectAll2Menu(ZValue zvl);
	
	/*메뉴 목록의 총 수*/
	public int selectMenuTotalCount(MenuParam param);
		
	/*Top Menu 목록만을 추출*/
	public List<Menu> getMyTopMenus(AuthUserParam param);
		
	/*권한 리스트*/
	public List<ZValue> retrieveAuthorList();
	
	/*메뉴*/
	public ZValue selectMenu(ZValue zvl) throws Exception;
	
	/*상위 메뉴 ID*/
	public int selectUpperMenuId(ZValue zvl) throws Exception;
	
	/*메뉴 등록*/
	public void insertMenu(ZValue zvl, HttpServletRequest request) throws Exception;	
	
	/*메뉴 삭제*/
	public void deleteMenu(int deleteMenuId) throws Exception;
	
	/* new 메뉴 조회 */
	public Menu selectOneRow(int seq) throws Exception;
	
	/* new 메뉴 권한*/
	public List<MenuMap> selectAuthorId(int seq) throws Exception;
	
	/*new 메뉴 수정*/
	public void updateMenu(ZValue zvl, HttpServletRequest request) throws Exception;
	
	/*new 메뉴권한 체크*/
	public List<ZValue> selectDistinctAuthorId(ZValue zvl) throws Exception;
}
