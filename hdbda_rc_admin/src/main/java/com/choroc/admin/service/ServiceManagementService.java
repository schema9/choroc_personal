package com.choroc.admin.service;

import java.util.List;

import com.choroc.admin.domain.ServiceParam;
import com.choroc.admin.domain.ServiceResult;
import com.choroc.admin.util.ZValue;

/**
 * TODO > 서비스 목록을 호출
 * 파일명   > ServiceManagementService.java
 * 작성일   > 2018. 11. 22.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public interface ServiceManagementService {

	// 서비스 목록을 호출한다.
	public List<ServiceResult> getServiceList(ServiceParam param);

	// 서비스 목록의 전체 개수를 호출한다.
	public int getServiceListTotalCount(ServiceParam param);

	// 서비스의 상세 정보를 추출한다.
	public ZValue getService(ZValue zvl);

	// 서비스를 등록한다.
	public int createService(ZValue zvl);

    // 서비스 정보를 변경한다.
	public int modifyService(ZValue zvl);

	// 서비스의 상태를 'DELETED'로 변경한다.(삭제 마킹 : 목록에 보여야 함);
	public int removeService(ZValue zvl);

	// 서비스의 상태를 'COMPLETE'로 변경한다.(복구처리)
	public int restoreService(ZValue zvl);

	// 해당 서비스에 사용중인 알고리즘 리스트, 사용되고 있지않은 알고리즘 리스트를 추출한다.
	List<ZValue> getAllAlgorithmList(ZValue zvl) throws Exception;
}
