package com.choroc.admin.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.choroc.admin.dao.MenuDao;
import com.choroc.admin.domain.AuthUserParam;
import com.choroc.admin.domain.MenuParam;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.dto.Menu;
import com.choroc.admin.dto.MenuInfo;
import com.choroc.admin.dto.MenuMap;
import com.choroc.admin.util.ZValue;

/**
 * TODO  > 권한 관리 > 사용자 관리 > 사용자 메뉴 권한 관리
 * 파일명   > MenuServiceImpl.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 9.
 */

@Service  
public class MenuServiceImpl implements MenuService {

	private static final Logger logger = LoggerFactory.getLogger(MenuServiceImpl.class);
	
	@Inject
	private MenuDao menuDao;
	
	/*메뉴 리스트*/
	@Override
	public List<ZValue> selectMenuList(ZValue zvl) throws Exception {
		return menuDao.selectMenuList(zvl);
	}	
		
	/*메뉴 목록*/
	@Override
	public List<MenuInfo> selectMenuRows(MenuParam param) {
		
		List<MenuInfo> result = new ArrayList<MenuInfo>(Arrays.asList());
		try {
			logger.info(" menuDao.selectMenuRows(param) 메뉴목록 :::::::::");
			result = menuDao.selectMenuRows(param);
			//logger.info("[메뉴목록 ]" + result);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/*권한 List*/
	@Override
	public List<ZValue> retrieveAuthorList() {		
		List<ZValue> zValue = null;	
		try {
			zValue = menuDao.retrieveAuthorList();
		}catch (Exception e) {
			e.printStackTrace();
		}	
		return zValue;
	}

	/*유효한 모든 메뉴 목록*/
	@Override
	public List<MenuInfo> selectAllMenu() {
		List<MenuInfo> result = new ArrayList<MenuInfo>(Arrays.asList());
        try {
        	logger.info(" menuDao.selectAllMenu() 유효한 모든 메뉴 목록 :::::::");
            result = menuDao.selectAllMenu();
        } catch (SQLException e) {
            e.printStackTrace();
        }	
		return result;
	}
	
	/*선택한 상위 메뉴포함 목록*/
	@Override
	public List<MenuInfo> selectAll2Menu(ZValue zvl) {
		List<MenuInfo> result = new ArrayList<MenuInfo>(Arrays.asList());
		try {
			 result = menuDao.selectAll2Menu(zvl);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/* 메뉴등록 */
	@Override
	public void insertMenu(ZValue zvl, HttpServletRequest request) throws Exception {
		
		HttpSession session = request.getSession();
		AuthUser userInfo = (AuthUser)session.getAttribute("userInfo");
		
		String menuNm       = zvl.getValue("menuNm");
		String description  = zvl.getValue("description");
		String url 			= zvl.getValue("url");
		String level 		= zvl.getValue("level");
		String indictYn 	= zvl.getValue("indictYn");
		String upperMenuId 	= zvl.getValue("upperMenuId");
		String menuId = "";		
		
		ArrayList menuIdList = zvl.getArrayList("menuId[]"); // 메뉴 레벨
		ArrayList authList   = zvl.getArrayList("auth[]");
		
		logger.info(">>>>> menuIdList 갯수" + menuIdList.size());
		logger.info(">>>>> menuIdList" + menuIdList);
		logger.info(">>>>> authList" + authList);
		
		// 메뉴 등록 ,수정
		for(int i=0; i < menuIdList.size(); i++) {
			
			String id = menuIdList.get(i).toString();
			logger.info("String id: " + id);
			ZValue iZvl = new ZValue();
			iZvl.put("outputOrdr", i+1); // 메뉴 정렬순서 SORT_ORDER
			iZvl.put("registId", userInfo.getUserId());
			
			// 메뉴ID가 없으면 메뉴추가
			if(StringUtils.isEmpty(id)) {
				logger.info("메뉴 추가 - if(StringUtils.isEmpty(id)) ");
				iZvl.put("menuNm", menuNm);
				iZvl.put("description", description);
				iZvl.put("url", url);
				iZvl.put("indictYn", indictYn);
				iZvl.put("upperMenuId", upperMenuId);
				
				menuDao.insertMenu(iZvl);
				menuId = iZvl.getValue("menuId");
			} 
		}
		
		// 메뉴 권한 메핑 등록
		for(int i=0; i < authList.size(); i++) {
			String authorId = authList.get(i).toString();
			ZValue iZvl = new ZValue();
			//iZvl.put("registId", userInfo.getUserId());
			iZvl.put("registId", "SYSTEM");
			iZvl.put("menuId", menuId);
			iZvl.put("authorId", authorId);
			
			menuDao.insertAuthorMenuMap(iZvl);
		}	
	}
	
	
	/*메뉴 수정*/
	@Override
	public void updateMenu(ZValue zvl, HttpServletRequest request) throws Exception {
						
		HttpSession session = request.getSession();
		String menuNm = zvl.getValue("menuNm");
		String description = zvl.getValue("description");
		String url = zvl.getValue("url");
		String indictYn = zvl.getValue("indictYn");
		String updateMenuId = zvl.getValue("updateMenuId");
		AuthUser user = (AuthUser)session.getAttribute("userLoginInfo");
		
		ArrayList menuIdList = zvl.getArrayList("menuId[]"); // 메뉴 레벨
		ArrayList authList = zvl.getArrayList("auth[]");     // 메뉴 권한
				
		logger.info(">>>>>>>>> menuNm: " + menuNm);
		logger.info(">>>>>>>>> description: " + description);
		logger.info(">>>>>>>>> url: " + url);
		logger.info(">>>>>>>>> indictYn: " + indictYn);
		logger.info(">>>>>>>>> updateMenuId: " + updateMenuId);
		logger.info(">>>>>>>>> menuIdList: " + menuIdList);
		logger.info(">>>>>>>>> 메뉴갯수 : " + menuIdList.size());
		logger.info(">>>>>>>>> authList: " + authList);

		for(int i=0; i < menuIdList.size(); i++) {
			
			logger.info(">>>>>>>>> :" + (i+1) + " 번째 메뉴ID: " +  menuIdList.get(i).toString() );
			String id = menuIdList.get(i).toString();
			
			ZValue iZvl = new ZValue();
			iZvl.put("outputOrdr", i+1);  // 정렬순서
			iZvl.put("modifyId", user.getUserId());
			iZvl.put("menuId", id);
			
			if(updateMenuId.equals(id)) {
				logger.info(">>>>>>>>> if(menuId.equals(id)) { " );
				iZvl.put("menuNm", menuNm);
				iZvl.put("description", description);
				iZvl.put("url", url);
				iZvl.put("indictYn", indictYn);
			}
			
			// 메뉴 수정
			menuDao.updateMenu(iZvl);
		}
		// 메뉴 권한 삭제
		menuDao.deleteAuthorMenuMap(zvl);
		
		// 메뉴 권한 등록
		for(int i=0; i < authList.size(); i++) {
			String auth = authList.get(i).toString(); // 메뉴권한리스트 [1:관리자, 2:담당자 ,3:사용자]
			ZValue iZvl = new ZValue();
			iZvl.put("menuId", updateMenuId);
			iZvl.put("authorId", auth);
			iZvl.put("registId", "SYSTEM");
			menuDao.insertAuthorMenuMap(iZvl);
		}
	}

	/*메뉴목록*/
	@Override
	public ZValue selectMenu(ZValue zvl) throws Exception {
		logger.info("selectMenu(ZValue zvl) 수정전 메뉴 불러오기");
		return menuDao.selectMenu(zvl);
	}

	/* 상위메뉴권한 체크 */
	@Override
	public List<ZValue> selectDistinctAuthorId(ZValue zvl) throws Exception {	
		return menuDao.selectDistinctAuthorId(zvl);
	}

	/*메뉴 목록의 총 수*/
	@Override
	public int selectMenuTotalCount(MenuParam param) {
		
		int result = 0;
		try {
			result = menuDao.selectMenuTotalCount(param);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	/*상위 메뉴 ID*/
	@Override
	public int selectUpperMenuId(ZValue zvl) throws Exception {
		return menuDao.selectUpperMenuId(zvl);
	}

	/*Top Menu 목록만을 추출*/
	@Override
	public List<Menu> getMyTopMenus(AuthUserParam param) {
		List<Menu> result = null;
		try {
			result = menuDao.selectMyTopMenus(param);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/*메뉴 삭제*/
	@Override
	public void deleteMenu(int deleteMenuId) throws Exception {
		menuDao.deleteMenu(deleteMenuId);
	}

	/*메뉴 조회 */
	@Override
	public Menu selectOneRow(int seq) {
		
		Menu menu = null;
		try {
			menu = menuDao.selectOneRow(seq);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return menu;
	}

	/*메뉴 권한 */
	@Override
	public List<MenuMap> selectAuthorId(int seq) throws Exception {
		return menuDao.selectAuthorId(seq);
	}

}
