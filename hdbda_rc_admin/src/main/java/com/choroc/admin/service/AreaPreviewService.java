package com.choroc.admin.service;

import java.util.List;

import com.choroc.admin.domain.SystemManagerParam;

public interface AreaPreviewService {
	public List<SystemManagerParam> selectRecoAreaRow(SystemManagerParam param);
	public int selectRecoAreaTotalCount(SystemManagerParam param);
}
