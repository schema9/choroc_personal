package com.choroc.admin.domain;

/**
 * TODO > 로그인(아이디 , 패스워드) 파라미터 
 * 파일명   > LoginParam.java
 * 작성일   > 2018. 11. 23.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public class LoginParam extends CommonAdminToolParam {
	
	private static final long serialVersionUID = 1L;
	
	private String loginUserId;
	private String loginUserPassword;

	public String getLoginUserId() {
		return loginUserId;
	}

	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}

	public String getLoginUserPassword() {
		return loginUserPassword;
	}

	public void setLoginUserPassword(String loginUserPassword) {
		this.loginUserPassword = loginUserPassword;
	}

	@Override
	public String toString() {
		return "LoginParam [loginUserId=" + loginUserId + ", loginUserPassword=" + loginUserPassword + "]";
	}
		
}