package com.choroc.admin.domain;

import com.choroc.admin.dto.AuthUser;

public class AuthUserParam extends CommonAdminToolParam {

	private AuthUser authUser;
	
	private int menuId;

	public AuthUser getAuthUser() {
		return authUser;
	}

	public void setAuthUser(AuthUser authUser) {
		this.authUser = authUser;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}	
}