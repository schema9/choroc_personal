package com.choroc.admin.domain;

import java.io.Serializable;

import com.choroc.admin.web.PaginationInfo;

public class CommonAdminToolParam extends PaginationInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String api;
	private String domain;
	private int domainId;
	private String domainNm;
	private String domainUrl;
	private String serviceId;
	private String uid;
	private String site;
	private String activeDomainKey;
	private String activeServiceKey;
	private String activeMenuKey;
	private String leftMenuKey;
	private String authUserId;
	private String masterDb;
	
	private String ossvi;
	private String ossts;
	private String ossct;
	private String ossdi;
	private String ossdwp;
	private String user_opt;
	
	/**
	 * @return the api
	 */
	public String getApi() {
		return api;
	}

	/**
	 * @param api the api to set
	 */
	public void setApi(String api) {
		this.api = api;
	}

	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * @param domain the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

    public int getDomainId() {
        return domainId;
    }

    public void setDomainId(int domainId) {
        this.domainId = domainId;
    }

    public String getDomainNm() {
        return domainNm;
    }

    public void setDomainNm(String domainNm) {
        this.domainNm = domainNm;
    }

    public String getDomainUrl() {
        return domainUrl;
    }

    public void setDomainUrl(String domainUrl) {
        this.domainUrl = domainUrl;
    }

    /**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * @param uid the uid to set
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**
	 * @return the site
	 */
	public String getSite() {
		return site;
	}

	/**
	 * @param site the site to set
	 */
	public void setSite(String site) {
		this.site = site;
	}

	/**
	 * @return the activeDomainKey
	 */
	public String getActiveDomainKey() {
		return activeDomainKey;
	}

	/**
	 * @param activeDomainKey the activeDomainKey to set
	 */
	public void setActiveDomainKey(String activeDomainKey) {
		this.activeDomainKey = activeDomainKey;
	}

	/**
	 * @return the activeServiceKey
	 */
	public String getActiveServiceKey() {
		return activeServiceKey;
	}

	/**
	 * @param activeServiceKey the activeServiceKey to set
	 */
	public void setActiveServiceKey(String activeServiceKey) {
		this.activeServiceKey = activeServiceKey;
	}

	/**
	 * @return the activeMenuKey
	 */
	public String getActiveMenuKey() {
		return activeMenuKey;
	}

	/**
	 * @param activeMenuKey the activeMenuKey to set
	 */
	public void setActiveMenuKey(String activeMenuKey) {
		this.activeMenuKey = activeMenuKey;
	}

	/**
	 * @return the leftMenuKey
	 */
	public String getLeftMenuKey() {
		return leftMenuKey;
	}

	/**
	 * @param leftMenuKey the leftMenuKey to set
	 */
	public void setLeftMenuKey(String leftMenuKey) {
		this.leftMenuKey = leftMenuKey;
	}

	/**
	 * @return the authUserId
	 */
	public String getAuthUserId() {
		return authUserId;
	}
	
	
	
	public String getOssvi() {
		return ossvi;
	}

	public void setOssvi(String ossvi) {
		this.ossvi = ossvi;
	}

	public String getOssts() {
		return ossts;
	}

	public void setOssts(String ossts) {
		this.ossts = ossts;
	}

	public String getOssct() {
		return ossct;
	}

	public void setOssct(String ossct) {
		this.ossct = ossct;
	}

	public String getOssdi() {
		return ossdi;
	}

	public void setOssdi(String ossdi) {
		this.ossdi = ossdi;
	}

	public String getOssdwp() {
		return ossdwp;
	}

	public void setOssdwp(String ossdwp) {
		this.ossdwp = ossdwp;
	}

	public String getUser_opt() {
		return user_opt;
	}

	public void setUser_opt(String user_opt) {
		this.user_opt = user_opt;
	}

	/**
	 * @param authUserId the authUserId to set
	 */
	public void setAuthUserId(String authUserId) {
		this.authUserId = authUserId;
	}
	public String getMasterDb() { return masterDb;	}

	public void setMasterDb(String masterDb) {	this.masterDb = masterDb;	}

	@Override
	public String toString() {
		return "CommonAdminToolParam [api=" + api + ", domain=" + domain
				+ ", serviceId=" + serviceId + ", uid=" + uid + ", site="
				+ site + ", activeDomainKey=" + activeDomainKey
				+ ", activeServiceKey=" + activeServiceKey + ", activeMenuKey="
				+ activeMenuKey + ", leftMenuKey=" + leftMenuKey
				+ ", authUserId=" + authUserId + ", toString()="
				+ super.toString() + "]";
	}
}
