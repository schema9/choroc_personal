package com.choroc.admin.domain;

import java.io.Serializable;

public class MenuParam extends CommonAdminToolParam implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int totalCount;
	private String listOrder;
	private String searchText;
	private String searchOption;

	// display variables
	private int menuId;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getListOrder() {
		return listOrder;
	}

	public void setListOrder(String listOrder) {
		this.listOrder = listOrder;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
}
