package com.choroc.admin.domain;

public class PageAreaParam extends CommonAdminToolParam {

	private static final long serialVersionUID = 1L;
	
	private int pgeRelmId; // 페이지영역아이디
	private String pgeRelmNm; // 페이지영역명
	private String pgeRelmDc; // 페이지영역설명
	private String widgId; // 위젯아이디
	private Integer totalCount;

	public int getPgeRelmId() {
		return pgeRelmId;
	}

	public void setPgeRelmId(int pgeRelmId) {
		this.pgeRelmId = pgeRelmId;
	}

	public String getPgeRelmNm() {
		return pgeRelmNm;
	}

	public void setPgeRelmNm(String pgeRelmNm) {
		this.pgeRelmNm = pgeRelmNm;
	}

	public String getPgeRelmDc() {
		return pgeRelmDc;
	}

	public void setPgeRelmDc(String pgeRelmDc) {
		this.pgeRelmDc = pgeRelmDc;
	}

	public String getWidgId() {
		return widgId;
	}

	public void setWidgId(String widgId) {
		this.widgId = widgId;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
}
