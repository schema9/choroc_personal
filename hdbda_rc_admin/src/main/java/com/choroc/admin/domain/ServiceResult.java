package com.choroc.admin.domain;

import com.choroc.admin.dto.ServiceInfo;

public class ServiceResult extends ServiceInfo {

	private static final long serialVersionUID = 1L;
	
	private String domainNm;
	private String statusValue;

	/**
	 * @return the domainNm
	 */
	public String getDomainNm() {
		return domainNm;
	}

	/**
	 * @param domainNm the domainNm to set
	 */
	public void setDomainNm(String domainNm) {
		this.domainNm = domainNm;
	}

	/**
	 * @return the statusValue
	 */
	public String getStatusValue() {
		return statusValue;
	}

	/**
	 * @param statusValue the statusValue to set
	 */
	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ServiceResult [domainNm=" + domainNm + ", statusValue=" + statusValue + ", toString()="
				+ super.toString() + "]";
	}
}
