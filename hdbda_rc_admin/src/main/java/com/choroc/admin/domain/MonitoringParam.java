package com.choroc.admin.domain;

import java.io.Serializable;

public class MonitoringParam extends CommonAdminToolParam implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int totalCount;
	private String searchText;
	private String searchOption1;
	private String searchOption2;
	private String searchOption3;
	
	private String task_id;
	private String job_id;
	private String stts_cd;
	private String stts_dtl;
	private String strt_ddtm;
	private String end_ddtm;
	private String reg_ddtm;
	
	private String job_nm;
	

	
	public String getSearchOption3() {
		return searchOption3;
	}

	public void setSearchOption3(String searchOption3) {
		this.searchOption3 = searchOption3;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchOption1() {
		return searchOption1;
	}

	public void setSearchOption1(String searchOption1) {
		this.searchOption1 = searchOption1;
	}

	public String getSearchOption2() {
		return searchOption2;
	}

	public void setSearchOption2(String searchOption2) {
		this.searchOption2 = searchOption2;
	}

	public String getTask_id() {
		return task_id;
	}

	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}

	public String getJob_id() {
		return job_id;
	}

	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}

	public String getStts_cd() {
		return stts_cd;
	}

	public void setStts_cd(String stts_cd) {
		this.stts_cd = stts_cd;
	}

	public String getStts_dtl() {
		return stts_dtl;
	}

	public void setStts_dtl(String stts_dtl) {
		this.stts_dtl = stts_dtl;
	}

	public String getStrt_ddtm() {
		return strt_ddtm;
	}

	public void setStrt_ddtm(String strt_ddtm) {
		this.strt_ddtm = strt_ddtm;
	}

	public String getEnd_ddtm() {
		return end_ddtm;
	}

	public void setEnd_ddtm(String end_ddtm) {
		this.end_ddtm = end_ddtm;
	}

	public String getReg_ddtm() {
		return reg_ddtm;
	}

	public void setReg_ddtm(String reg_ddtm) {
		this.reg_ddtm = reg_ddtm;
	}

	public String getJob_nm() {
		return job_nm;
	}

	public void setJob_nm(String job_nm) {
		this.job_nm = job_nm;
	}
	
}
