package com.choroc.admin.domain;

import java.util.List;

import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.dto.ServiceInfo;

public class AccountResult extends AuthUser {

	private static final long serialVersionUID = 1L;
	
	private String domainId;
	private String domainNm;
	private String serviceId;
	private String serviceNm;
	private String memberStatus;
	private String useControl;
	private String writeType;
	
	private String ext1;
	private String ext2;
	private String ext3;
	
	private String phone1;
	private String phone2;
	private String phone3;
	
	private List<ServiceInfo> myServiceList;
	
	/**
	 * @return the domainId
	 */
	public String getDomainId() {
		return domainId;
	}
	/**
	 * @param domainId the domainId to set
	 */
	public void setDomainId(String domainId) {
		this.domainId = domainId;
	}
	/**
	 * @return the domainNm
	 */
	public String getDomainNm() {
		return domainNm;
	}
	/**
	 * @param domainNm the domainNm to set
	 */
	public void setDomainNm(String domainNm) {
		this.domainNm = domainNm;
	}
	/**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}
	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	/**
	 * @return the serviceNm
	 */
	public String getServiceNm() {
		return serviceNm;
	}
	/**
	 * @param serviceNm the serviceNm to set
	 */
	public void setServiceNm(String serviceNm) {
		this.serviceNm = serviceNm;
	}
	/**
	 * @return the memberStatus
	 */
	public String getMemberStatus() {
		return memberStatus;
	}
	/**
	 * @param memberStatus the memberStatus to set
	 */
	public void setMemberStatus(String memberStatus) {
		this.memberStatus = memberStatus;
	}
	/**
	 * @return the useControl
	 */
	public String getUseControl() {
		return useControl;
	}
	/**
	 * @param useControl the useControl to set
	 */
	public void setUseControl(String useControl) {
		this.useControl = useControl;
	}
	/**
	 * @return the writeType
	 */
	public String getWriteType() {
		return writeType;
	}
	/**
	 * @param writeType the writeType to set
	 */
	public void setWriteType(String writeType) {
		this.writeType = writeType;
	}
	/**
	 * @return the ext1
	 */
	public String getExt1() {
		return ext1;
	}
	/**
	 * @param ext1 the ext1 to set
	 */
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	/**
	 * @return the ext2
	 */
	public String getExt2() {
		return ext2;
	}
	/**
	 * @param ext2 the ext2 to set
	 */
	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}
	/**
	 * @return the ext3
	 */
	public String getExt3() {
		return ext3;
	}
	/**
	 * @param ext3 the ext3 to set
	 */
	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}
	/**
	 * @return the phone1
	 */
	public String getPhone1() {
		return phone1;
	}
	/**
	 * @param phone1 the phone1 to set
	 */
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	/**
	 * @return the phone2
	 */
	public String getPhone2() {
		return phone2;
	}
	/**
	 * @param phone2 the phone2 to set
	 */
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	/**
	 * @return the phone3
	 */
	public String getPhone3() {
		return phone3;
	}
	/**
	 * @param phone3 the phone3 to set
	 */
	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}
	/**
	 * @return the myServiceList
	 */
	public List<ServiceInfo> getMyServiceList() {
		return myServiceList;
	}
	/**
	 * @param myServiceList the myServiceList to set
	 */
	public void setMyServiceList(List<ServiceInfo> myServiceList) {
		this.myServiceList = myServiceList;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AccountResult [domainId=" + domainId + ", domainNm="
				+ domainNm + ", serviceId=" + serviceId + ", serviceNm="
				+ serviceNm + ", memberStatus=" + memberStatus
				+ ", useControl=" + useControl + ", ext1=" + ext1 + ", ext2="
				+ ext2 + ", ext3=" + ext3 + ", phone1=" + phone1 + ", phone2="
				+ phone2 + ", phone3=" + phone3 + ", myServiceList="
				+ myServiceList + ", toString()=" + super.toString() + "]";
	}
	
}
