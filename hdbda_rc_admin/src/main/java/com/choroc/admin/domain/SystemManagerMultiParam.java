package com.choroc.admin.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import com.google.gson.JsonObject;

/**
 * TODO  >
 * 파일명   > AccountParam.java
 * 작성일   > 2018. 11. 21.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public class SystemManagerMultiParam extends CommonAdminToolParam implements Serializable {

	// common
	private static final long serialVersionUID = 1L;
	private int totalCount;
	private String listOrder;
	private String searchText;
	private String searchOption;
	private String searchOption1;
	private String searchOption2;
	private String searchService;

	// display variables
	//메타데이터 테이블 관련
	private String userId; // USER_ID; 사용자아이디;
	private String sysNm;		//시스템명
	private String enTblNm;		//영문테이블명
	private String krTblNm;		//국문테이블명
	private String loadType;	//적재타입
	private String ptntTblYn;	//파티션테이블여부
	private String regDdtm;		//등록일
	private String useYn;		//사용유무
	private String metaData;
	
	//메타데이터 컬럼 관련
	private String sysNms;		//시스템명
	private String enTblNms;		//영문테이블명
	private String enColNm;	//영문컬럼명
	private String krColNm;	//국문컬럼명
	private String oraDatType;  //ORCALE DATA TYPE
	private String hvdatType;   //HIVE DATA TYPE
	private String kdDatType;   //KUDU DATA TYPE
	private String pkYn;       //pk여부
	private String pkOrdr;     //pk순서
	private String nullable;      //not null 허용여부
	private String ptntColYn;   //파티션컬럼여부
	private String ptntColConvSntx; //파티션컬럼변환식
	private String sstvInfColYn;    //민감정보 포함여부
	private String sstvInfType; //민감정보 분류
	private String chgDatBasColYn;  //변경적재기준컬럼여부
	private String colOrdr;  //컬럼 순번
	private String tableDataSet;
	private List<SystemManagerParam> metaDataSet;
	
	
//	private String currentPageNo;	//현재 페이지
	private int firstIndex;		
	private int recordCountPerPage;


	public void addMetaList(JSONObject metaDataSet){
		if(this.metaDataSet==null)
			this.metaDataSet = new ArrayList<SystemManagerParam>();
		
		SystemManagerParam list = new SystemManagerParam();
//		list.setSysNms(metaDataSet.get("sysNms").toString());		//시스템명
//		list.setEnTblNms(metaDataSet.get("enTblNms").toString());		//영문테이블명
//		list.setEnColNm(metaDataSet.get("enColNm").toString());	//영문컬럼명
//		list.setKrColNm(metaDataSet.get("krColNm").toString());	//국문컬럼명
//		list.setOraDatType(metaDataSet.get("oraDatType").toString());  //ORCALE DATA TYPE
//		list.setHvdatType(metaDataSet.get("hvdatType").toString());   //HIVE DATA TYPE
//		list.setKdDatType(metaDataSet.get("kdDatType").toString());   //KUDU DATA TYPE
//		list.setPkYn(metaDataSet.get("pkYn").toString());       //pk여부
//		list.setPkOrdr(metaDataSet.get("pkOrdr").toString());     //pk순서
//		list.setNullable(metaDataSet.get("nullable").toString());       //not null 허용여부
//		list.setPtntColYn(metaDataSet.get("ptntColYn").toString());    //파티션컬럼여부
//		list.setPtntColConvSntx(metaDataSet.get("ptntColConvSntx").toString());  //파티션컬럼변환식
//		list.setSstvInfColYn(metaDataSet.get("sstvInfColYn").toString());     //민감정보 포함여부
//		list.setSstvInfType(metaDataSet.get("sstvInfType").toString());  //민감정보 분류
//		list.setChgDatBasColYn(metaDataSet.get("chgDatBasColYn").toString());   //변경적재기준컬럼여부
//		list.setColOrdr(metaDataSet.get("colOrdr").toString());   //컬럼 순번
		this.metaDataSet.add(list);
		
	}
	
	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getListOrder() {
		return listOrder;
	}

	public void setListOrder(String listOrder) {
		this.listOrder = listOrder;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}

	public String getSearchService() {
		return searchService;
	}

	public void setSearchService(String searchService) {
		this.searchService = searchService;
	}

	public int getRecordCountPerPage() {
		return recordCountPerPage;
	}

	public void setRecordCountPerPage(int recordCountPerPage) {
		this.recordCountPerPage = recordCountPerPage;
	}

	public int getFirstIndex() {
		return firstIndex;
	}

	public void setFirstIndex(int firstIndex) {
		this.firstIndex = firstIndex;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSysNm() {
		return sysNm;
	}

	public void setSysNm(String sysNm) {
		this.sysNm = sysNm;
	}

	public String getEnTblNm() {
		return enTblNm;
	}

	public void setEnTblNm(String enTblNm) {
		this.enTblNm = enTblNm;
	}

	public String getKrTblNm() {
		return krTblNm;
	}

	public void setKrTblNm(String krTblNm) {
		this.krTblNm = krTblNm;
	}

	public String getLoadType() {
		return loadType;
	}

	public void setLoadType(String loadType) {
		this.loadType = loadType;
	}

	public String getPtntTblYn() {
		return ptntTblYn;
	}

	public void setPtntTblYn(String ptntTblYn) {
		this.ptntTblYn = ptntTblYn;
	}

	public String getRegDdtm() {
		return regDdtm;
	}

	public void setRegDdtm(String regDdtm) {
		this.regDdtm = regDdtm;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getSearchOption1() {
		return searchOption1;
	}

	public void setSearchOption1(String searchOption1) {
		this.searchOption1 = searchOption1;
	}

	public String getSearchOption2() {
		return searchOption2;
	}

	public void setSearchOption2(String searchOption2) {
		this.searchOption2 = searchOption2;
	}

	public String getOraDatType() {
		return oraDatType;
	}

	public void setOraDatType(String oraDatType) {
		this.oraDatType = oraDatType;
	}

	public String getHvdatType() {
		return hvdatType;
	}

	public void setHvdatType(String hvdatType) {
		this.hvdatType = hvdatType;
	}

	public String getKdDatType() {
		return kdDatType;
	}

	public void setKdDatType(String kdDatType) {
		this.kdDatType = kdDatType;
	}

	public String getPkYn() {
		return pkYn;
	}

	public void setPkYn(String pkYn) {
		this.pkYn = pkYn;
	}

	public String getPkOrdr() {
		return pkOrdr;
	}

	public void setPkOrdr(String pkOrdr) {
		this.pkOrdr = pkOrdr;
	}

	public String getNullable() {
		return nullable;
	}

	public void setNullable(String nullable) {
		this.nullable = nullable;
	}

	public String getPtntColYn() {
		return ptntColYn;
	}

	public void setPtntColYn(String ptntColYn) {
		this.ptntColYn = ptntColYn;
	}

	public String getPtntColConvSntx() {
		return ptntColConvSntx;
	}

	public void setPtntColConvSntx(String ptntColConvSntx) {
		this.ptntColConvSntx = ptntColConvSntx;
	}

	public String getSstvInfColYn() {
		return sstvInfColYn;
	}

	public void setSstvInfColYn(String sstvInfColYn) {
		this.sstvInfColYn = sstvInfColYn;
	}

	public String getSstvInfType() {
		return sstvInfType;
	}

	public void setSstvInfType(String sstvInfType) {
		this.sstvInfType = sstvInfType;
	}

	public String getChgDatBasColYn() {
		return chgDatBasColYn;
	}

	public void setChgDatBasColYn(String chgDatBasColYn) {
		this.chgDatBasColYn = chgDatBasColYn;
	}

	public String getColOrdr() {
		return colOrdr;
	}

	public void setColOrdr(String colOrdr) {
		this.colOrdr = colOrdr;
	}

	public String getEnColNm() {
		return enColNm;
	}

	public void setEnColNm(String enColNm) {
		this.enColNm = enColNm;
	}

	public String getKrColNm() {
		return krColNm;
	}

	public void setKrColNm(String krColNm) {
		this.krColNm = krColNm;
	}

	public String getTableDataSet() {
		return tableDataSet;
	}

	public void setTableDataSet(String tableDataSet) {
		this.tableDataSet = tableDataSet;
	}

	public List<SystemManagerParam> getMetaDataSet() {
		return metaDataSet;
	}

	public void setMetaDataSet(List<SystemManagerParam> metaDataSet) {
		this.metaDataSet = metaDataSet;
	}

	public String getSysNms() {
		return sysNms;
	}

	public void setSysNms(String sysNms) {
		this.sysNms = sysNms;
	}

	public String getEnTblNms() {
		return enTblNms;
	}

	public void setEnTblNms(String enTblNms) {
		this.enTblNms = enTblNms;
	}

	

}
