package com.choroc.admin.domain;

import java.io.Serializable;

public class SystemAlgorithmParam extends CommonAdminToolParam implements Serializable {

	private static final long serialVersionUID = 1L;

	private String algId;

	private String listOrder;

	private String searchText;

	private String searchOption;

	public String getAlgId() {
		return algId;
	}

	public void setAlgId(String algId) {
		this.algId = algId;
	}

	public String getListOrder() {
		return listOrder;
	}

	public void setListOrder(String listOrder) {
		this.listOrder = listOrder;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}
}
