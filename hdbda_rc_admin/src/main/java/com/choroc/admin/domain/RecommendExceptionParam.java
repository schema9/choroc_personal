package com.choroc.admin.domain;

import java.io.Serializable;

public class RecommendExceptionParam extends CommonAdminToolParam implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	private int totalCount;
	private String searchText;
	private String searchOptionYn;
	private String searchOptionNm;
	
	
	private String sortation;
	private String content_cd;
	private String create_dt;
	
	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getSearchOptionYn() {
		return searchOptionYn;
	}
	public void setSearchOptionYn(String searchOptionYn) {
		this.searchOptionYn = searchOptionYn;
	}
	public String getSearchOptionNm() {
		return searchOptionNm;
	}
	public void setSearchOptionNm(String searchOptionNm) {
		this.searchOptionNm = searchOptionNm;
	}
	public String getSortation() {
		return sortation;
	}
	public void setSortation(String sortation) {
		this.sortation = sortation;
	}
	public String getContent_cd() {
		return content_cd;
	}
	public void setContent_cd(String content_cd) {
		this.content_cd = content_cd;
	}
	public String getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(String create_dt) {
		this.create_dt = create_dt;
	}
	
	@Override
	public String toString() {		
		return "RecommendExceptionParam:" + "[searchOptionYn:" + searchOptionYn + " searchOptionNm:" + searchOptionNm + " searchText:" + searchText +"]";
		//return super.toString();
	}
	
}
