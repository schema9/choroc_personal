package com.choroc.admin.domain;

import java.io.Serializable;

public class ServiceParam extends CommonAdminToolParam implements Serializable {

	private static final long serialVersionUID = 1L;

	private int totalCount;

	private String listOrder;

	private String searchText;

	private String searchOption;

	private int roleId;

	/**
	 * @return the totalCount
	 */
	public int getTotalCount() {
		return totalCount;
	}

	/**
	 * @param totalCount the totalCount to set
	 */
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * @return the listOrder
	 */
	public String getListOrder() {
		return listOrder;
	}

	/**
	 * @param listOrder the listOrder to set
	 */
	public void setListOrder(String listOrder) {
		this.listOrder = listOrder;
	}

	/**
	 * @return the searchText
	 */
	public String getSearchText() {
		return searchText;
	}

	/**
	 * @param searchText the searchText to set
	 */
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	/**
	 * @return the searchOption
	 */
	public String getSearchOption() {
		return searchOption;
	}

	/**
	 * @param searchOption the searchOption to set
	 */
	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}

	/**
	 * @return the roleId
	 */
	public int getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ServiceParam [totalCount=" + totalCount + ", listOrder=" + listOrder + ", searchText=" + searchText
				+ ", roleId=" + roleId + ", toString()=" + super.toString() + "]";
	}
}
