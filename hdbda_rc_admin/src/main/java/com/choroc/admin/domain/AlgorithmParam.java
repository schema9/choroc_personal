package com.choroc.admin.domain;

import java.io.Serializable;
import java.util.Date;

import com.choroc.admin.dto.PageArea;

public class AlgorithmParam extends CommonAdminToolParam implements Serializable {

	private static final long serialVersionUID = 1L;

	private int totalCount;
	private String searchText;
	private String searchOptionYn;
	private String searchOptionNm;

	private String algrth_id;
	private String algrth_nm;
	private String algrth_dc;
	private String use_yn;
	private String del_yn;
	private String create_id;
	private Date create_dt;
	private String modify_id;
	private Date modify_dt;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchOptionYn() {
		return searchOptionYn;
	}

	public void setSearchOptionYn(String searchOptionYn) {
		this.searchOptionYn = searchOptionYn;
	}

	public String getSearchOptionNm() {
		return searchOptionNm;
	}

	public void setSearchOptionNm(String searchOptionNm) {
		this.searchOptionNm = searchOptionNm;
	}

	public String getAlgrth_id() {
		return algrth_id;
	}

	public void setAlgrth_id(String algrth_id) {
		this.algrth_id = algrth_id;
	}

	public String getAlgrth_nm() {
		return algrth_nm;
	}

	public void setAlgrth_nm(String algrth_nm) {
		this.algrth_nm = algrth_nm;
	}

	public String getAlgrth_dc() {
		return algrth_dc;
	}

	public void setAlgrth_dc(String algrth_dc) {
		this.algrth_dc = algrth_dc;
	}

	public String getUse_yn() {
		return use_yn;
	}

	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}

	public String getDel_yn() {
		return del_yn;
	}

	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}

	public String getCreate_id() {
		return create_id;
	}

	public void setCreate_id(String create_id) {
		this.create_id = create_id;
	}

	public Date getCreate_dt() {
		return create_dt;
	}

	public void setCreate_dt(Date create_dt) {
		this.create_dt = create_dt;
	}

	public String getModify_id() {
		return modify_id;
	}

	public void setModify_id(String modify_id) {
		this.modify_id = modify_id;
	}

	public Date getModify_dt() {
		return modify_dt;
	}

	public void setModify_dt(Date modify_dt) {
		this.modify_dt = modify_dt;
	}

	@Override
	public String toString() {
		return "AlgorithmParam:" + "[searchOptionYn:" + searchOptionYn + " searchOptionNm:" + searchOptionNm
				+ " searchText:" + searchText + "]";
		// return super.toString();
	}

}
