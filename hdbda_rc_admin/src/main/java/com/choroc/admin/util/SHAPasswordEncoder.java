package com.choroc.admin.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * TODO >
 * 파일명   > SHAPasswordEncoder.java
 * 작성일   > 2018. 11. 23.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
@SuppressWarnings("deprecation")
public class SHAPasswordEncoder implements PasswordEncoder{
	
	private static final Logger logger = LoggerFactory.getLogger(SHAPasswordEncoder.class);
	
    private ShaPasswordEncoder shaPasswordEncoder;
    private Object salt = null;

    public SHAPasswordEncoder() {
    	logger.info(":::::::::::::::::::: SHAPasswordEncoder() ::::::::::::::::::::");
        shaPasswordEncoder = new ShaPasswordEncoder();
    }

    public SHAPasswordEncoder(int sha) {
        shaPasswordEncoder = new ShaPasswordEncoder(sha);
    }

    public void setEncodeHashAsBase64(boolean encodeHashAsBase64) {
        shaPasswordEncoder.setEncodeHashAsBase64(encodeHashAsBase64);
    }

    public void setSalt(Object salt) {
        this.salt = salt;
    }

    @Override
    public String encode(CharSequence rawPassword) {
        return shaPasswordEncoder.encodePassword(rawPassword.toString(), salt);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return shaPasswordEncoder.isPasswordValid(encodedPassword, rawPassword.toString(), salt);
    }	

}
