package com.choroc.admin.util;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.core.env.Environment;

import net.coobird.thumbnailator.Thumbnails;
import net.sf.json.JSONException;

public class ShopUtils {
	
	private static Environment env;

	public static final int IMAGE_LARGE = 1200;
	
	/**
	 * 상품 썸네일 호출
	 * @author [2017-06-01] minae.yun
	 * @param itemUserCode
	 * @param fileName
	 * @param sizeName
	 * @return
	 */
	public static String loadImage(String itemUserCode, String fileName, String sizeName) {

		//상품에 등록된 이미지가 없을경우
		if (fileName == null || "".equals(fileName) || fileName.indexOf("/") > -1 || !fileName.contains("."))
			return ShopUtils.getNoImagePath();

		//호출한 사이즈가 존재하는지 확인
		boolean type = false;
		for (String inputSize : getThumbnailType()) {
			if (sizeName.equals(inputSize)) type = true;
		}

		if (!type) return ShopUtils.getNoImagePath();

		int pos = fileName.lastIndexOf( "." );
		String ext = fileName.substring( pos + 1 );
		String reFileName = fileName.substring(0, pos);
		String imageSrc = "";
		int imageSize = 0;

		//관리자 설정이 사이즈별 썸네일 저장일때
		if (reFileName.contains("(") && reFileName.contains(")")) {
			//이미지 이름에 ()괄호가 있을경우
			pos = fileName.lastIndexOf( "(" );
			ext = fileName.substring( pos, fileName.length());
			reFileName = reFileName.substring(0, 14) + sizeName + ext;
			imageSrc = "/upload/item/" + itemUserCode + "/" + reFileName;
		} else {
			//if (reFileName.length()>14) reFileName = reFileName.substring(0, 14) + sizeName;
			if(reFileName.startsWith("2")) reFileName = reFileName.substring(0, 14) + sizeName;
			imageSrc = "/upload/item/" + itemUserCode + "/" + reFileName + "." + ext;
		}

		return imageSrc;
	}


	/**
	 * 상품 썸네일 호출
	 * @author [2017-06-01] minae.yun
	 * @param imageSrc
	 * @param sizeName
	 * @return
	 */
	public static String loadImage(String imageSrc, String sizeName) {

		//상품에 등록된 이미지가 없을경우
		if (imageSrc == null || "".equals(imageSrc) || !imageSrc.contains(".")) return imageSrc;

		//호출한 사이즈가 존재하는지 확인
		boolean type = false;
		for (String inputSize : getThumbnailType()) {
			if (sizeName.equals(inputSize)) type = true;
		}

		if (!type) return ShopUtils.getNoImagePath();


		int pos = imageSrc.lastIndexOf( "." );
		int tag = imageSrc.lastIndexOf( "/" );
		String ext = imageSrc.substring( pos + 1 );
		String fileName = imageSrc.substring(tag + 1, pos);
		String fileRoot = imageSrc.substring(0, tag);
		String targetImageSrc = "";
		int imageSize = 0;

		//관리자 설정이 사이즈별 썸네일 저장일때
		if(fileName.startsWith("2")) fileName = fileName.substring(0, 14) + sizeName;
		//if (fileName.length()>14) fileName = fileName.substring(0, 14) + sizeName;
		imageSrc = fileRoot + "/" + fileName + "." + ext;
		targetImageSrc = imageSrc;

		return targetImageSrc;
	}
	
	//public static final String[] THUMBNAIL_TYPE = new String[] {"XS", "S", "M", "L"};
	/**
	 * 관리자 썸네일 설정에서 저장한 이미지 사이즈를 리턴
	 * @author [2017-06-09]minae.yun
	 * @return
	 */
	public static String[] getThumbnailType() {

		String jsonSize = "{\"list\":[{\"sizeName\":\"XS\",\"size\":\"150\"},{\"sizeName\":\"S\",\"size\":\"300\"},{\"sizeName\":\"M\",\"size\":\"500\"},{\"sizeName\":\"L\",\"size\":\"1200\"}]}"; //json 데이터

		if (jsonSize == null) {
			return  new String[] {"L"};
		}

		JSONObject jsonObject = (JSONObject) JSONValue.parse(jsonSize);  //json 파싱
		JSONArray infoArray = (JSONArray) jsonObject.get("list"); //list의 배열을 추출
		String[] THUMBNAIL_TYPE = new String[infoArray.size()];

		try {
			for(int i=0; i<infoArray.size(); i++){
				//배열 안에 있는것도 JSON형식 이기 때문에 JSON Object 로 추출
				JSONObject listObject = (JSONObject) infoArray.get(i);

				//list배열 안에 데이터으로 추출
				String listSizeName = (String) listObject.get("sizeName");

				THUMBNAIL_TYPE[i] = listSizeName;
			}

		} catch (JSONException e) {
			//default 고정 이미지 사이즈
			THUMBNAIL_TYPE = new String[] {"XS", "S", "M", "L"};
		}

		return THUMBNAIL_TYPE;
	}
	
	/**
	 * 관리자 썸네일 설정에서 입력한 값으로 이미지 사이즈를 리턴
	 * @author [2017-06-09]minae.yun
	 * @param sizeName
	 * @return
	 */
	public static int getImageSize(String sizeName) {

		//호출한 사이즈가 존재하는지 확인
		boolean type = false;
		for (String inputSize : getThumbnailType()) {
			if (sizeName.equals(inputSize)) type = true;
		}

		//제일 큰 사이즈 default
		if (!type) return IMAGE_LARGE;

		int imageSize = 0;
		
		String jsonSize = "{\"list\":[{\"sizeName\":\"XS\",\"size\":\"150\"},{\"sizeName\":\"S\",\"size\":\"300\"},{\"sizeName\":\"M\",\"size\":\"500\"},{\"sizeName\":\"L\",\"size\":\"1200\"}]}"; //json 데이터

		if (jsonSize.isEmpty()) {
			return IMAGE_LARGE;
		}

		JSONObject jsonObject = (JSONObject) JSONValue.parse(jsonSize);  //json 파싱
		JSONArray infoArray = (JSONArray) jsonObject.get("list"); //list의 배열을 추출

		try {
			for(int i=0; i<infoArray.size(); i++){
				//배열 안에 있는것도 JSON형식 이기 때문에 JSON Object 로 추출
				JSONObject listObject = (JSONObject) infoArray.get(i);

				//list배열 안에 데이터으로 추출
				String listSizeName = (String) listObject.get("sizeName");
				int listSize = Integer.parseInt((String)listObject.get("size"));

				if (sizeName.equals(listSizeName)) imageSize = listSize;
			}

		} catch (JSONException e) {
			imageSize = IMAGE_LARGE; //제일 큰 사이즈 default
		}

		return imageSize;
	}


	/**
	 * 가로, 세로 둘다 한계치보다 큰 이미지를 가로,세로중 작은 쪽을 한계치에 맞추고 그 비율에 따라 썸네일 생성
	 * @author 이상우 [2017-03-31]
	 * @param image
	 * @param limit
	 * @return
	 * @throws IOException
	 */
	public static BufferedImage getThumbnailImage(BufferedImage image, int limit) throws IOException {

		image = Thumbnails.of(image).size(limit, limit).asBufferedImage();

		return image;
	}
	
	/**
	 * 이미지 없을때 보이는 이미지경로
	 * @return
	 */
	public static String getNoImagePath() {
		return env.getProperty("sepp.noimage.path", "");
	}

}

