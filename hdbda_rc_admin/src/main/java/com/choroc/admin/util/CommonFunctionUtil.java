package com.choroc.admin.util;

import org.slf4j.Logger;
import org.slf4j.MDC;

public class CommonFunctionUtil {

	/**
	 * Make random strings
	 * 
	 * @param stringLength : length of return value
	 * @param isNumber     : is include Numbers
	 * @param isSpecial    : is include Special characters
	 * @return
	 */
	public static String randomString(int stringLength, boolean isNumber, boolean isSpecial) {
		/*
		 * A (65) ~ Z (90); a (97) ~ z (122); 2 (50) ~ 9 (57); special character :
		 * !@#$%^&*-_?~
		 */
		String result;
		StringBuffer buffer = new StringBuffer();
		StringBuffer sc = new StringBuffer("!@#$%^&*-_?~");

		for (int i = 0; i < stringLength; i++) {
			int current;
			if (isNumber) {
				current = (int) Math.floor(Math.random() * 72) + 50;
			} else {
				current = (int) Math.floor(Math.random() * 57) + 65;
			}

			// escape [ \ ] ^ _ ` : ; < = > ? @ I O
			if (current >= 91 && current <= 96) {
				i--;
			} else if (current >= 58 && current <= 64) {
				i--;
			} else if (current == 73 || current == 79) {
				i--;
			} else {
				buffer.append((char) current);
			}

		}

		if (isSpecial) {
			// replace to spacial character
			buffer.setCharAt((int) (Math.random() * stringLength - 1),
					sc.charAt((int) (Math.random() * sc.length() - 1)));
			buffer.setCharAt((int) (Math.random() * stringLength - 1),
					sc.charAt((int) (Math.random() * sc.length() - 1)));
		}

		// Capital character for 1st.
		buffer.setCharAt(0, (char) ((Math.random() * 26) + 65));

		result = buffer.toString();
		return result;
	}

	public static void adminActionLog(Logger logger, ZValue zvalue) {

		MDC.put("userId", zvalue.get("userId").toString());
		MDC.put("conectIp", zvalue.get("conectIp").toString());
		MDC.put("occrrncUrl", zvalue.get("occrrncUrl").toString());
		MDC.put("fnct", zvalue.get("fnct").toString());
		MDC.put("action", zvalue.get("action").toString());
		MDC.put("paramtr", zvalue.get("paramtr").toString());
		MDC.put("befUpd", zvalue.get("befUpd").toString());
		MDC.put("aftUpd", zvalue.get("aftUpd").toString());
		logger.info((String) zvalue.get("rslt"));

		MDC.clear();
	}
}
