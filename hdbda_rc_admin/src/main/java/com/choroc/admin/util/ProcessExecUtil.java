package com.choroc.admin.util;


import java.io.IOException;

public class ProcessExecUtil {
	private static ProcessExecUtil g_instance = null;

	private ProcessExecUtil() {
	}

	public static int exec(String[] command) {
		if (null == g_instance) {
			g_instance = new ProcessExecUtil();
		}

		ProcessBuilder pb = new ProcessBuilder(command).inheritIO();

		try {
			Process process = pb.start();
			process.waitFor();
			process.destroy();

			return process.exitValue();
		} catch (IOException | InterruptedException ex) {
			throw new RuntimeException(ex);
		}
	}
}
