package com.choroc.admin.util;


import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class Configuration {
	private Map<String, String> propertiesMap = null;

	public Configuration(String configFilePath) {
		this.propertiesMap = new HashMap<>();

		_init(configFilePath);
	}

	private void _init(String configFilePath) {
		FileReader reader = null;
		try {
			reader = new FileReader(configFilePath);
			InputSource is = new InputSource(reader);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			Document document = factory.newDocumentBuilder().parse(is);

			_parse(document);
		} catch (Throwable ex) {
			throw new RuntimeException(ex);
		} finally {
			if (null != reader) try { reader.close(); } catch (IOException e) { ; }
		}
	}
	private void _parse(Document document) throws XPathExpressionException {
		XPath xpath = XPathFactory.newInstance().newXPath();

		NodeList node = (NodeList)xpath.compile("//configurations/config/@id").evaluate(document, XPathConstants.NODESET);
		for (int i=0; i < node.getLength(); i++) {
			String id = node.item(i).getNodeValue();
			String val = _getValueForID(id, document, xpath);
			this.propertiesMap.put(id, val);
		}
	}
	private String _getValueForID(String id, Document document, XPath xpath) throws XPathExpressionException {
		Node node = (Node)xpath.compile(String.format("//configurations/config[@id='%s']", id)).evaluate(document, XPathConstants.NODE);
		if (null == node) return "";
		return node.getTextContent();
	}

	public Map<String, String> getPropertiesMap() {
		return this.propertiesMap;
	}

	public String get(String id) {
		return this.propertiesMap.get(id);
	}
	public int getInt(String id) {
		String val = this.get(id);
		if (null == val) return 0;
		try {
			return Integer.parseInt(val);
		} catch (NumberFormatException ex) {
			return 0;
		}
	}
	public boolean getBoolean(String id) {
		String val = this.get(id);
		if (null == val) return false;

		try {
			return Boolean.parseBoolean(val);
		} catch (Throwable ex) {
			return false;
		}
	}
}
