package com.choroc.admin.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.choroc.admin.controller.LoginController;
import com.choroc.admin.filter.XSSUtil;

/**
 * TODO  > 쿠키 유틸
 * 파일명   > CookieUtil.java
 * 작성자   > CCmedia Service Corp.
 * 작성일   > 2018. 11. 9.
 * 수정일   > 
 * 수정자   > 
 */

public class CookieUtil {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
    public CookieUtil() {}
    public static void setCookie(HttpServletResponse response, String cookieName, String cookieValue, String domain, boolean isSecure, boolean isHttpOnly) 
    		throws UnsupportedEncodingException {
    	String cookieValueEnc = URLEncoder.encode(cookieValue, "utf-8"); // 인코딩
    	Cookie cookie = new Cookie(cookieName, cookieValueEnc); // 쿠키 생성
    	cookie.setDomain(domain);
    	cookie.setSecure(isSecure);
    	cookie.setMaxAge(-1);       // 쿠키 삭제   
        response.addCookie(cookie); // 쿠키 전송 
    }

    public static void setCookie(HttpServletResponse response, String cookieName, String domain, boolean isSecure, boolean isHttpOnly) 
    		throws UnsupportedEncodingException {
    	
        Cookie cookie = new Cookie(XSSUtil.removeCRLF(cookieName), (String)null);
        cookie.setDomain(domain);
        cookie.setSecure(isSecure);
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }
    
    public static String getCookie(HttpServletRequest request, String cookieNm) throws Exception 
    {
        Cookie[] cookies = request.getCookies();
        
        if(null == cookies) {
            return "";
        } 
        else {
            String cookieValue = null;       
            // 쿠키명으로 쿠키값 추출
            for(int i = 0; i < cookies.length; ++i) {
                if(cookieNm.equals(cookies[i].getName())) {
                    cookieValue = URLDecoder.decode(cookies[i].getValue(), "utf-8");
                    break;
                }
            }
            // 유저아이디 쿠키 반환
            return cookieValue;
        }
    }
}