package com.choroc.admin.util;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class WebFactoryUtil {
	public static ZValue getAttributesInit(HttpServletRequest request) {
		ZValue zvl = getAllAttributes(request);
		zvl.put("uBeanId", getID(request));

		HttpSession httpSession = request.getSession();
		return zvl;
	}

	@SuppressWarnings("rawtypes")
	public static ZValue getAllAttributes(HttpServletRequest request) {
		ZValue zvl = new ZValue();
		String name = "";
		Enumeration e = request.getParameterNames();
		String temp_param_url = ""; // 항상 가지고 다녀야 할 파라미터들...
		while (e.hasMoreElements()) {
			name = (String) e.nextElement();
			if (name.indexOf("tsch") > -1 || name.indexOf("ttxt") > -1 || name.indexOf("parent") > -1) {
				if (temp_param_url.length() > 0) {
					temp_param_url += "&";
				}
				temp_param_url += name + "=" + request.getParameter(name);
			}

			String value = request.getParameter(name);
			String[] values = request.getParameterValues(name);

			if (values != null && values.length > 1) {
				zvl.put(name, values);
			} else {
				zvl.put(name, value);
			}

		}
		zvl.put("params", temp_param_url);
		return zvl;
	}

	public static String getID(HttpServletRequest request) {
		String id = "";
		String return_id = null;
		String url = request.getRequestURI();
		if (url.indexOf("/crm/") > -1) {
			String[] temp = url.split("/");
			if (temp.length > 2) {
				return_id = temp[2];
			}
		} else {
			id = url.substring(url.lastIndexOf("/") + 1, url.length());
			String[] temp = id.split("[.]");
			if (temp.length > 0) {
				return_id = temp[0];
			}
		}
		return return_id;
	}

}
