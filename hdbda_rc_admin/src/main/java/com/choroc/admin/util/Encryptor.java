package com.choroc.admin.util;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 * TODO > 암호화
 * 파일명   > Encryptor.java
 * 작성일   > 2019. 2. 11.
 * 작성자   > CCmedia Service Corp.
 */
public class Encryptor {

	public static void main(String[] args) {
		
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		// 복호화에 필요한 key
		encryptor.setPassword("hmall");
 
		// 암호화
		String prod_password = encryptor.encrypt("");
		
		System.out.println(">>" + prod_password + "<<");
	}

}
