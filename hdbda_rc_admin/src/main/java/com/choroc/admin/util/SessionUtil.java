package com.choroc.admin.util;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtil {

	public SessionUtil() {
	}

	public static void setSessionAttribute(HttpServletRequest request, String keyStr, String valStr) throws Exception {
		HttpSession session = request.getSession();
		session.setAttribute(keyStr, valStr);
	}

	public static void setSessionAttribute(HttpServletRequest request, String keyStr, Object obj) throws Exception {
		HttpSession session = request.getSession();
		session.setAttribute(keyStr, obj);
	}

	public static Object getSessionAttribute(HttpServletRequest request, String keyStr) throws Exception {
		HttpSession session = request.getSession();
		return session.getAttribute(keyStr);
	}

	public static String getSessionValuesString(HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();
		String returnVal = "";

		String sessionKey;
		for (Enumeration e = session.getAttributeNames(); e.hasMoreElements(); returnVal = returnVal + "[" + sessionKey
				+ " : " + session.getAttribute(sessionKey) + "]") {
			sessionKey = (String) e.nextElement();
		}

		return returnVal;
	}

	public static void removeSessionAttribute(HttpServletRequest request, String keyStr) throws Exception {
		HttpSession session = request.getSession();
		session.removeAttribute(keyStr);
	}
}
