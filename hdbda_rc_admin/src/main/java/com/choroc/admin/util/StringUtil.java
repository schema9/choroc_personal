package com.choroc.admin.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringUtil {

	private static final Logger log = LoggerFactory.getLogger(StringUtil.class);
	private static final char WHITE_SPACE = ' ';

	public StringUtil() {
	}

	public static boolean isNull(String str) {
		if (str != null) {
			str = str.trim();
		}

		return str == null || "".equals(str);
	}

	public static boolean isAlpha(String str) {
		if (str == null) {
			return false;
		} else {
			int size = str.length();
			if (size == 0) {
				return false;
			} else {
				for (int i = 0; i < size; ++i) {
					if (!Character.isLetter(str.charAt(i))) {
						return false;
					}
				}

				return true;
			}
		}
	}

	public static boolean isAlphaNumeric(String str) {
		if (str == null) {
			return false;
		} else {
			int size = str.length();
			if (size == 0) {
				return false;
			} else {
				for (int i = 0; i < size; ++i) {
					if (!Character.isLetterOrDigit(str.charAt(i))) {
						return false;
					}
				}

				return true;
			}
		}
	}

	public static String integer2string(int integer) {
		return "" + integer;
	}

	public static String long2string(long longdata) {
		return String.valueOf(longdata);
	}

	public static String float2string(float floatdata) {
		return String.valueOf(floatdata);
	}

	public static String double2string(double doubledata) {
		return String.valueOf(doubledata);
	}

	public static String null2void(String str) {
		if (isNull(str)) {
			str = "";
		}

		return str;
	}

	public static int string2integer(String str) {
		return isNull(str) ? 0 : Integer.parseInt(str);
	}

	public static float string2float(String str) {
		return isNull(str) ? 0.0F : Float.parseFloat(str);
	}

	public static double string2double(String str) {
		return isNull(str) ? 0.0D : Double.parseDouble(str);
	}

	public static long string2long(String str) {
		return isNull(str) ? 0L : Long.parseLong(str);
	}

	public static String null2string(String str, String defaultValue) {
		return isNull(str) ? defaultValue : str;
	}

	public static int string2integer(String str, int defaultValue) {
		return isNull(str) ? defaultValue : Integer.parseInt(str);
	}

	public static float string2float(String str, float defaultValue) {
		return isNull(str) ? defaultValue : Float.parseFloat(str);
	}

	public static double string2double(String str, double defaultValue) {
		return isNull(str) ? defaultValue : Double.parseDouble(str);
	}

	public static long string2long(String str, long defaultValue) {
		return isNull(str) ? defaultValue : Long.parseLong(str);
	}

	public static boolean equals(String source, String target) {
		return null2void(source).equals(null2void(target));
	}

	public static String toSubString(String str, int beginIndex, int endIndex) {
		return equals(str, "") ? str
				: (str.length() < beginIndex ? ""
						: (str.length() < endIndex ? str.substring(beginIndex) : str.substring(beginIndex, endIndex)));
	}

	public static String toSubString(String source, int beginIndex) {
		return equals(source, "") ? source : (source.length() < beginIndex ? "" : source.substring(beginIndex));
	}

	public static int search(String source, String target) {
		int result = 0;
		String strCheck = new String(source);

		for (int i = 0; i < source.length(); strCheck = strCheck.substring(i)) {
			int loc = strCheck.indexOf(target);
			if (loc == -1) {
				break;
			}

			++result;
			i = loc + target.length();
		}

		return result;
	}

	public static String trim(String str) {
		return str.trim();
	}

	public static String ltrim(String str) {
		int index = 0;

		while (32 == str.charAt(index++)) {
			;
		}

		if (index > 0) {
			str = str.substring(index - 1);
		}

		return str;
	}

	public static String rtrim(String str) {
		int index = str.length();

		do {
			--index;
		} while (32 == str.charAt(index));

		if (index < str.length()) {
			str = str.substring(0, index + 1);
		}

		return str;
	}

	public static String concat(String str1, String str2) {
		StringBuffer sb = new StringBuffer(str1);
		sb.append(str2);
		return sb.toString();
	}

	public static String lPad(String str, int len, char pad) {
		return lPad(str, len, pad, false);
	}

	public static String lPad(String str, int len, char pad, boolean isTrim) {
		if (isNull(str)) {
			return null;
		} else {
			if (isTrim) {
				str = str.trim();
			}

			for (int i = str.length(); i < len; ++i) {
				str = pad + str;
			}

			return str;
		}
	}

	public static String rPad(String str, int len, char pad) {
		return rPad(str, len, pad, false);
	}

	public static String rPad(String str, int len, char pad, boolean isTrim) {
		if (isNull(str)) {
			return null;
		} else {
			if (isTrim) {
				str = str.trim();
			}

			for (int i = str.length(); i < len; ++i) {
				str = str + pad;
			}

			return str;
		}
	}

	public static String alignLeft(String str, int length) {
		return alignLeft(str, length, false);
	}

	public static String alignLeft(String str, int length, boolean isEllipsis) {
		StringBuffer temp;
		if (str.length() > length) {
			if (isEllipsis) {
				temp = new StringBuffer(length);
				temp.append(str.substring(0, length - 3));
				temp.append("...");
				return temp.toString();
			} else {
				return str.substring(0, length);
			}
		} else {
			temp = new StringBuffer(str);

			for (int i = 0; i < length - str.length(); ++i) {
				temp.append(' ');
			}

			return temp.toString();
		}
	}

	public static String alignRight(String str, int length) {
		return alignRight(str, length, false);
	}

	public static String alignRight(String str, int length, boolean isEllipsis) {
		StringBuffer temp;
		if (str.length() > length) {
			if (isEllipsis) {
				temp = new StringBuffer(length);
				temp.append(str.substring(0, length - 3));
				temp.append("...");
				return temp.toString();
			} else {
				return str.substring(0, length);
			}
		} else {
			temp = new StringBuffer(length);

			for (int i = 0; i < length - str.length(); ++i) {
				temp.append(' ');
			}

			temp.append(str);
			return temp.toString();
		}
	}

	public static String alignCenter(String str, int length) {
		return alignCenter(str, length, false);
	}

	public static String alignCenter(String str, int length, boolean isEllipsis) {
		StringBuffer temp;
		if (str.length() > length) {
			if (isEllipsis) {
				temp = new StringBuffer(length);
				temp.append(str.substring(0, length - 3));
				temp.append("...");
				return temp.toString();
			} else {
				return str.substring(0, length);
			}
		} else {
			temp = new StringBuffer(length);
			int leftMargin = (length - str.length()) / 2;
			int rightMargin;
			if (leftMargin * 2 == length - str.length()) {
				rightMargin = leftMargin;
			} else {
				rightMargin = leftMargin + 1;
			}

			int i;
			for (i = 0; i < leftMargin; ++i) {
				temp.append(' ');
			}

			temp.append(str);

			for (i = 0; i < rightMargin; ++i) {
				temp.append(' ');
			}

			return temp.toString();
		}
	}

	public static String capitalize(String str) {
		return !isNull(str) ? str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase() : str;
	}

	public static boolean isPatternMatch(String str, String pattern) throws Exception {
		Matcher matcher = Pattern.compile(pattern).matcher(str);
		log.debug("" + matcher);
		return matcher.matches();
	}

	public static String toEng(String kor) throws UnsupportedEncodingException {
		return isNull(kor) ? null : new String(kor.getBytes("KSC5601"), "8859_1");
	}

	public static String toKor(String en) throws UnsupportedEncodingException {
		return isNull(en) ? null : new String(en.getBytes("8859_1"), "euc-kr");
	}

	public static int countOf(String str, String charToFind) {
		int findLength = charToFind.length();
		int count = 0;

		for (int idx = str.indexOf(charToFind); idx >= 0; idx = str.indexOf(charToFind, idx + findLength)) {
			++count;
		}

		return count;
	}

	public static String encodePassword(String password, String algorithm) {
		byte[] unencodedPassword = password.getBytes();
		MessageDigest md = null;

		try {
			md = MessageDigest.getInstance(algorithm);
		} catch (Exception var7) {
			log.error("Exception: " + var7);
			return password;
		}

		md.reset();
		md.update(unencodedPassword);
		byte[] encodedPassword = md.digest();
		StringBuffer buf = new StringBuffer();

		for (int i = 0; i < encodedPassword.length; ++i) {
			if ((encodedPassword[i] & 255) < 16) {
				buf.append("0");
			}

			buf.append(Long.toString((long) (encodedPassword[i] & 255), 16));
		}

		return buf.toString();
	}

	public static String swapFirstLetterCase(String str) {
		StringBuffer sbuf = new StringBuffer(str);
		sbuf.deleteCharAt(0);
		if (Character.isLowerCase(str.substring(0, 1).toCharArray()[0])) {
			sbuf.insert(0, str.substring(0, 1).toUpperCase());
		} else {
			sbuf.insert(0, str.substring(0, 1).toLowerCase());
		}

		return sbuf.toString();
	}

	public static String trim(String origString, String trimString) {
		int startPosit = origString.indexOf(trimString);
		if (startPosit != -1) {
			int endPosit = trimString.length() + startPosit;
			return origString.substring(0, startPosit) + origString.substring(endPosit);
		} else {
			return origString;
		}
	}

	public static String getLastString(String origStr, String strToken) {
		StringTokenizer str = new StringTokenizer(origStr, strToken);

		String lastStr;
		for (lastStr = ""; str.hasMoreTokens(); lastStr = str.nextToken()) {
			;
		}

		return lastStr;
	}

	public static String[] getStringArray(String str, String strToken) {
		if (str.indexOf(strToken) == -1) {
			return new String[] { str };
		} else {
			StringTokenizer st = new StringTokenizer(str, strToken);
			String[] stringArray = new String[st.countTokens()];

			for (int i = 0; st.hasMoreTokens(); ++i) {
				stringArray[i] = st.nextToken();
			}

			return stringArray;
		}
	}

	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}

	public static String replace(String str, String replacedStr, String replaceStr) {
		String newStr = "";
		if (str.indexOf(replacedStr) != -1) {
			String s1 = str.substring(0, str.indexOf(replacedStr));
			String s2 = str.substring(str.indexOf(replacedStr) + 1);
			newStr = s1 + replaceStr + s2;
		}

		return newStr;
	}

	public static boolean isPatternMatching(String str, String pattern) throws Exception {
		if (pattern.indexOf(42) >= 0) {
			pattern = pattern.replaceAll("\\*", ".*");
		}

		pattern = "^" + pattern + "$";
		return Pattern.matches(pattern, str);
	}

	public static boolean containsMaxSequence(String str, String maxSeqNumber) {
		int occurence = 1;
		int max = string2integer(maxSeqNumber);
		if (str == null) {
			return false;
		} else {
			int sz = str.length();

			for (int i = 0; i < sz - 1; ++i) {
				if (str.charAt(i) == str.charAt(i + 1)) {
					++occurence;
					if (occurence == max) {
						return true;
					}
				} else {
					occurence = 1;
				}
			}

			return false;
		}
	}

	public static boolean containsInvalidChars(String str, char[] invalidChars) {
		if (str != null && invalidChars != null) {
			int strSize = str.length();
			int validSize = invalidChars.length;

			for (int i = 0; i < strSize; ++i) {
				char ch = str.charAt(i);

				for (int j = 0; j < validSize; ++j) {
					if (invalidChars[j] == ch) {
						return true;
					}
				}
			}

			return false;
		} else {
			return false;
		}
	}

	public static boolean containsInvalidChars(String str, String invalidChars) {
		return str != null && invalidChars != null ? containsInvalidChars(str, invalidChars.toCharArray()) : true;
	}

	public static boolean isNumeric(String str) {
		if (str == null) {
			return false;
		} else {
			int sz = str.length();
			if (sz == 0) {
				return false;
			} else {
				for (int i = 0; i < sz; ++i) {
					if (!Character.isDigit(str.charAt(i))) {
						return false;
					}
				}

				return true;
			}
		}
	}

	public static String reverse(String str) {
		return str == null ? null : (new StringBuffer(str)).reverse().toString();
	}

	public static String fillString(String originalStr, char ch, int cipers) {
		int originalStrLength = originalStr.length();
		if (cipers < originalStrLength) {
			return null;
		} else {
			int difference = cipers - originalStrLength;
			StringBuffer strBuf = new StringBuffer();

			for (int i = 0; i < difference; ++i) {
				strBuf.append(ch);
			}

			strBuf.append(originalStr);
			return strBuf.toString();
		}
	}

	public static final boolean isEmptyTrimmed(String foo) {
		return foo == null || foo.trim().length() == 0;
	}

	public static List getTokens(String lst, String separator) {
		ArrayList tokens = new ArrayList();
		if (lst != null) {
			StringTokenizer st = new StringTokenizer(lst, separator);

			while (st.hasMoreTokens()) {
				try {
					String e = st.nextToken().trim();
					tokens.add(e);
				} catch (Exception var5) {
					var5.printStackTrace();
				}
			}
		}

		return tokens;
	}

	public static List getTokens(String lst) {
		return getTokens(lst, ",");
	}

	public static String convertToCamelCase(String targetString, char posChar) {
		StringBuffer result = new StringBuffer();
		boolean nextUpper = false;
		String allLower = targetString.toLowerCase();

		for (int i = 0; i < allLower.length(); ++i) {
			char currentChar = allLower.charAt(i);
			if (currentChar == posChar) {
				nextUpper = true;
			} else {
				if (nextUpper) {
					currentChar = Character.toUpperCase(currentChar);
					nextUpper = false;
				}

				result.append(currentChar);
			}
		}

		return result.toString();
	}

	public static String convertToCamelCase(String underScore) {
		return convertToCamelCase(underScore, '_');
	}

	public static String convertToUnderScore(String camelCase) {
		String result = "";

		for (int i = 0; i < camelCase.length(); ++i) {
			char currentChar = camelCase.charAt(i);
			if (i > 0 && Character.isUpperCase(currentChar)) {
				result = result.concat("_");
			}

			result = result.concat(Character.toString(currentChar).toLowerCase());
		}

		return result;
	}

	public static String isNullToString(Object object) {
		String string = "";
		if (object != null) {
			string = object.toString().trim();
		}

		return string;
	}
}
