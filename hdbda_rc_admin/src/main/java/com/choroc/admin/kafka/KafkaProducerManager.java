package com.choroc.admin.kafka;


import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class KafkaProducerManager {
	private KafkaProducer<byte[], byte[]> producer = null;

	public KafkaProducerManager(String brokers, int retries, int batchSize, int bufferMemory) {
		if (0 >= retries) retries = 4;
		if (0 >= batchSize) batchSize = 16384;
		if (0 >= bufferMemory) bufferMemory = 33554432;

		_init(brokers, retries, batchSize, bufferMemory);
	}

	private void _init(String brokers, int retries, int batchSize, int bufferMemory) {
		Properties props = new Properties();
		props.put("bootstrap.servers", brokers);
		props.put("acks", "all");
		props.put("retries", retries);
		props.put("batch.size", batchSize);
		props.put("linger.ms", 1);
		props.put("buffer.memory", bufferMemory);
		props.put("key.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");

		this.producer = new KafkaProducer<>(props);
	}

	public Future<RecordMetadata> send(String topic, String str) {
		byte[] bytes;
		try {
			bytes = str.getBytes("utf8");
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace(System.err);

			return null;
		}
		ProducerRecord<byte[], byte[]> record = new ProducerRecord<byte[], byte[]>(topic, bytes);
		return this.producer.send(record);
	}

	public void flush() {
		this.producer.flush();
	}

	public void close() {
		this.producer.close();
	}
}
