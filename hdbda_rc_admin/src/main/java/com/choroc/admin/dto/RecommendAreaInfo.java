package com.choroc.admin.dto;

import java.util.Date;

import com.choroc.admin.domain.AbstractAdminToolObject;

public class RecommendAreaInfo {

	private static final long serialVersionUID = 1L;

	private String pge_area_id; // 페이지영역 아이디
	private String pge_area_nm; // 추천영역
	private String pge_area_dc; // 설명
	// 변경
	private String algrth_id; // 알고리즘아이디 (PK)
	private String algrth_nm;

	// 추가
	private String algrth_dc;
	private String use_yn; // 사용여부
	private String del_yn; // 삭제여부
	private String create_id; // 생성자 아이디
	private Date create_dt; // 생성일시
	private String modify_id;
	private Date modify_dt;

	public Date getCreate_dt() {
		return create_dt;
	}

	public void setCreate_dt(Date create_dt) {
		this.create_dt = create_dt;
	}

	public Date getModify_dt() {
		return modify_dt;
	}

	public void setModify_dt(Date modify_dt) {
		this.modify_dt = modify_dt;
	}

	public String getPge_area_id() {
		return pge_area_id;
	}

	public void setPge_area_id(String pge_area_id) {
		this.pge_area_id = pge_area_id;
	}

	public String getPge_area_nm() {
		return pge_area_nm;
	}

	public void setPge_area_nm(String pge_area_nm) {
		this.pge_area_nm = pge_area_nm;
	}

	public String getPge_area_dc() {
		return pge_area_dc;
	}

	public void setPge_area_dc(String pge_area_dc) {
		this.pge_area_dc = pge_area_dc;
	}

	public String getUse_yn() {
		return use_yn;
	}

	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}

	public String getDel_yn() {
		return del_yn;
	}

	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}

	public String getAlgrth_id() {
		return algrth_id;
	}

	public void setAlgrth_id(String algrth_id) {
		this.algrth_id = algrth_id;
	}

	public String getAlgrth_nm() {
		return algrth_nm;
	}

	public void setAlgrth_nm(String algrth_nm) {
		this.algrth_nm = algrth_nm;
	}

	public String getAlgrth_dc() {
		return algrth_dc;
	}

	public void setAlgrth_dc(String algrth_dc) {
		this.algrth_dc = algrth_dc;
	}

	public String getCreate_id() {
		return create_id;
	}

	public void setCreate_id(String create_id) {
		this.create_id = create_id;
	}

	public String getModify_id() {
		return modify_id;
	}

	public void setModify_id(String modify_id) {
		this.modify_id = modify_id;
	}

}
