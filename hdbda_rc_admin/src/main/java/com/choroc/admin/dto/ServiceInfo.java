package com.choroc.admin.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.AbstractAdminToolObject;

@Repository
public class ServiceInfo extends AbstractAdminToolObject implements Serializable {
	
	private String serviceId;
	private int domainId;
	private String serviceNm;
	private String serviceUrl;
	private String serviceDc;
	private Date beginDe;
	private Date endDe;
	private String serviceSttusCode;
	private int sortOrder;

	private String[] usedList;
	private String[] unUsedList;

	private String domainUrl;

	public String getDomainUrl() { return domainUrl;	}

	public void setDomainUrl(String domainUrl) { this.domainUrl = domainUrl; }


	/**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}
	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	/**
	 * @return the domainId
	 */
	public int getDomainId() {
		return domainId;
	}
	/**
	 * @param domainId the domainId to set
	 */
	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}
	/**
	 * @return the serviceNm
	 */
	public String getServiceNm() {
		return serviceNm;
	}
	/**
	 * @param serviceNm the serviceNm to set
	 */
	public void setServiceNm(String serviceNm) {
		this.serviceNm = serviceNm;
	}
	
	/**
	 * @return the serviceUrl
	 */
	public String getServiceUrl() {
		return serviceUrl;
	}
	/**
	 * @param serviceUrl the serviceUrl to set
	 */
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	/**
	 * @return the serviceDc
	 */
	public String getServiceDc() {
		return serviceDc;
	}
	/**
	 * @param serviceDc the serviceDc to set
	 */
	public void setServiceDc(String serviceDc) {
		this.serviceDc = serviceDc;
	}
	/**
	 * @return the beginDe
	 */
	public Date getBeginDe() {
		return beginDe;
	}
	/**
	 * @param beginDe the beginDe to set
	 */
	public void setBeginDe(Date beginDe) {
		this.beginDe = beginDe;
	}
	/**
	 * @return the endDe
	 */
	public Date getEndDe() {
		return endDe;
	}
	/**
	 * @param endDe the endDe to set
	 */
	public void setEndDe(Date endDe) {
		this.endDe = endDe;
	}
	/**
	 * @return the serviceSttusCode
	 */
	public String getServiceSttusCode() {
		return serviceSttusCode;
	}
	/**
	 * @param serviceSttusCode the serviceSttusCode to set
	 */
	public void setServiceSttusCode(String serviceSttusCode) {
		this.serviceSttusCode = serviceSttusCode;
	}
	
	/**
	 * @return the sortOrder
	 */
	public int getSortOrder() {
		return sortOrder;
	}
	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */


	public String[] getUsedList() {
		return usedList; 		
	}
	public void setUsedList(String[] usedList) {
		this.usedList = usedList;
	}

	public String[] getUnUsedList() {
		return unUsedList;
	}
	public void setUnUsedList(String[] unUsedList) {
		this.unUsedList = unUsedList;
	}
	
	@Override
	public String toString() {
		return "ServiceInfo [serviceId=" + serviceId + ", domainId=" + domainId
				+ ", serviceNm=" + serviceNm + ", serviceUrl="
				+ serviceUrl + ", serviceDc=" + serviceDc + ", beginDe="
				+ beginDe + ", endDe=" + endDe + ", serviceSttusCode=" + serviceSttusCode
				+ ", sortOrder=" + sortOrder
				+ ", usedList=" + usedList
				+ ", unUsedList=" + unUsedList
				+ ", toString()=" + super.toString() + "]";
	}
}
