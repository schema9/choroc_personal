package com.choroc.admin.dto;

/**
 * TODO > 메뉴정보
 * 파일명   > MenuInfo.java
 * 작성일   > 2018. 11. 28.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public class MenuInfo extends Menu {
	private String controlMode; // M: modify; D: delete;
	//private String displayLevel;   // 화면 항목 [ 레벨 ]
	private String combinedSeq; // 메뉴 일련 번호들

	/**
	 * Role_ID usage 1  관리자 , 2:매니져 , 3:사용자 ==== if added number 1 for
	 * combine ===== 2: 1; * 3: 2; * 4: 3 * 5: 1, 2; * 6: 1, 3; * 7: 2, 3 * 9: 1, 2,
	 * 3
	 */
	private int combinedRoleId;    // 권한 아이디
	private int rolePlatformAdmin; // 권한 [ 관리자 ] 0:false, 1:true
	private int roleServiceAdmin;  // 권한 [ 매니져 ] 0:false, 1:true
	private int roleServiceUser;   // 권한 [ 사용자 ] 0:false, 1:true
	private int depth1st;          // 메뉴 레벨 [ 1 depth ]
	private int depth2nd;          // 메뉴 레벨 [ 2 depth ]
	private int depth3rd;          // 메뉴 레벨 [ 3 depth ]
	private String seqGroup;       // 일련번호 묶음
	private String menuPath;
	
	public String getMenuPath() {
		return menuPath;
	}

	public void setMenuPath(String menuPath) {
		this.menuPath = menuPath;
	}

	public String getControlMode() {
		return controlMode;
	}

	public void setControlMode(String controlMode) {
		this.controlMode = controlMode;
	}

/*	public String getDisplayLevel() {
		return displayLevel;
	}

	public void setDisplayLevel(String displayLevel) {
		this.displayLevel = displayLevel;
	}*/

	public String getCombinedSeq() {
		return combinedSeq;
	}

	public void setCombinedSeq(String combinedSeq) {
		this.combinedSeq = combinedSeq;
	}

	public int getCombinedRoleId() {
		return combinedRoleId;
	}

	public void setCombinedRoleId(int combinedRoleId) {
		this.combinedRoleId = combinedRoleId;
	}

	public int getRolePlatformAdmin() {
		return rolePlatformAdmin;
	}

	public void setRolePlatformAdmin(int rolePlatformAdmin) {
		this.rolePlatformAdmin = rolePlatformAdmin;
	}

	public int getRoleServiceAdmin() {
		return roleServiceAdmin;
	}

	public void setRoleServiceAdmin(int roleServiceAdmin) {
		this.roleServiceAdmin = roleServiceAdmin;
	}

	public int getRoleServiceUser() {
		return roleServiceUser;
	}

	public void setRoleServiceUser(int roleServiceUser) {
		this.roleServiceUser = roleServiceUser;
	}

	public int getDepth1st() {
		return depth1st;
	}

	public void setDepth1st(int depth1st) {
		this.depth1st = depth1st;
	}

	public int getDepth2nd() {
		return depth2nd;
	}

	public void setDepth2nd(int depth2nd) {
		this.depth2nd = depth2nd;
	}

	public int getDepth3rd() {
		return depth3rd;
	}

	public void setDepth3rd(int depth3rd) {
		this.depth3rd = depth3rd;
	}

	public String getSeqGroup() {
		return seqGroup;
	}

	public void setSeqGroup(String seqGroup) {
		this.seqGroup = seqGroup;
	}
}
