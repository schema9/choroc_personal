package com.choroc.admin.dto;

import com.choroc.admin.domain.AbstractAdminToolObject;

public class ServiceUserMapping extends AbstractAdminToolObject {
	
	private int seq;
	private String serviceId;
	private String userId;

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "ServiceUserMapping [seq=" + seq + ", serviceId=" + serviceId + ", userId=" + userId + ", toString()="
				+ super.toString() + "]";
	}
}
