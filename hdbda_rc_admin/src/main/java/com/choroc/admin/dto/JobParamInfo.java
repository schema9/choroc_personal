package com.choroc.admin.dto;

import java.io.Serializable;

import com.choroc.admin.domain.AbstractAdminToolObject;

public class JobParamInfo extends AbstractAdminToolObject implements Serializable {

	private String jobID;
	private String jobDc;
	private String jobParamJson;
	private String controlMode;

	public String getJobID() {
		return jobID;
	}

	public void setJobID(String jobID) {
		this.jobID = jobID;
	}

	public String getJobDc() {
		return jobDc;
	}

	public void setJobDc(String jobDc) {
		this.jobDc = jobDc;
	}

	public String getJobParamJson() {
		return jobParamJson;
	}

	public void setJobParamJson(String jobParamJson) {
		this.jobParamJson = jobParamJson;
	}

	public String getControlMode() {
		return controlMode;
	}

	public void setControlMode(String controlMode) {
		this.controlMode = controlMode;
	}

}
