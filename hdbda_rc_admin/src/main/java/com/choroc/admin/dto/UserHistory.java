package com.choroc.admin.dto;

import java.io.Serializable;

public class UserHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	private long rnum;
	
	private String uid;
	
	private String serviceId;
	
	private String domain;
	
	private long	createTime;
	
	private long updateTime;

	public long getRnum() {
		return rnum;
	}

	public void setRnum(long rnum) {
		this.rnum = rnum;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "UserHistory [rnum=" + rnum + ", uid=" + uid + ", serviceId="
				+ serviceId + ", domain=" + domain + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}

}
