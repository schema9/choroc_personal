package com.choroc.admin.dto;

import com.choroc.admin.domain.AbstractAdminToolObject;

public class UserRoleMapping extends AbstractAdminToolObject {

	private int seq;
	private int roleId;
	private String userId;

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "UserRoleMapping [seq=" + seq + ", roleId=" + roleId + ", userId=" + userId + ", toString()="
				+ super.toString() + "]";
	}

}
