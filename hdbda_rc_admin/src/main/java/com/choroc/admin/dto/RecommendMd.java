package com.choroc.admin.dto;

import java.util.Date;

public class RecommendMd {
	
	
	private String sortation;
	private String content_cd;
	private Date create_dt;
	private String create_id;
	
	
	
	public String getCreate_id() {
		return create_id;
	}
	public void setCreate_id(String create_id) {
		this.create_id = create_id;
	}
	public String getSortation() {
		return sortation;
	}
	public void setSortation(String sortation) {
		this.sortation = sortation;
	}
	public String getContent_cd() {
		return content_cd;
	}
	public void setContent_cd(String content_cd) {
		this.content_cd = content_cd;
	}
	public Date getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(Date create_dt) {
		this.create_dt = create_dt;
	}
	
	

}
