package com.choroc.admin.dto;

import java.io.Serializable;

import com.choroc.admin.domain.AbstractAdminToolObject;

public class CommonCodeInfo extends AbstractAdminToolObject implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String code;
	private String groupCode;
	private String codeNm;
	private String codeDc;
	private String controlMode;
	private String writeType;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCodeNm() {
		return codeNm;
	}

	public void setCodeNm(String codeNm) {
		this.codeNm = codeNm;
	}

	public String getCodeDc() {
		return codeDc;
	}

	public void setCodeDc(String codeDc) {
		this.codeDc = codeDc;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getControlMode() {
		return controlMode;
	}

	public void setControlMode(String controlMode) {
		this.controlMode = controlMode;
	}

	public String getWriteType() {
		return writeType;
	}

	public void setWriteType(String writeType) {
		this.writeType = writeType;
	}
}
