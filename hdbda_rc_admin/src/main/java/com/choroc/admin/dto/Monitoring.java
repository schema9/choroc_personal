package com.choroc.admin.dto;

public class Monitoring {

	private String task_id;
	private String job_id;
	private String stts_cd;
	private String stts_dtl;
	private String etl_tgt_tbl;
	private String excu_ddtm;
	private String strt_ddtm;
	private String end_ddtm;
	private String reg_ddtm;
	
	private String job_nm;

	
	
	
	public String getTask_id() {
		return task_id;
	}

	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}

	public String getJob_id() {
		return job_id;
	}

	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}

	public String getStts_cd() {
		return stts_cd;
	}

	public void setStts_cd(String stts_cd) {
		this.stts_cd = stts_cd;
	}

	public String getStts_dtl() {
		return stts_dtl;
	}

	public void setStts_dtl(String stts_dtl) {
		this.stts_dtl = stts_dtl;
	}

	public String getEtl_tgt_tbl() {
		return etl_tgt_tbl;
	}

	public void setEtl_tgt_tbl(String etl_tgt_tbl) {
		this.etl_tgt_tbl = etl_tgt_tbl;
	}

	public String getStrt_ddtm() {
		return strt_ddtm;
	}

	public void setStrt_ddtm(String strt_ddtm) {
		this.strt_ddtm = strt_ddtm;
	}

	public String getEnd_ddtm() {
		return end_ddtm;
	}

	public void setEnd_ddtm(String end_ddtm) {
		this.end_ddtm = end_ddtm;
	}

	public String getReg_ddtm() {
		return reg_ddtm;
	}

	public void setReg_ddtm(String reg_ddtm) {
		this.reg_ddtm = reg_ddtm;
	}

	public String getJob_nm() {
		return job_nm;
	}

	public void setJob_nm(String job_nm) {
		this.job_nm = job_nm;
	}

	public String getExcu_ddtm() {
		return excu_ddtm;
	}

	public void setExcu_ddtm(String excu_ddtm) {
		this.excu_ddtm = excu_ddtm;
	}
	
	
	
	
	
}
