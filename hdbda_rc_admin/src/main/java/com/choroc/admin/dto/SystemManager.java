package com.choroc.admin.dto;

import java.io.Serializable;
import java.util.Date;

import com.choroc.admin.domain.AbstractAdminToolObject;

public class SystemManager extends AbstractAdminToolObject implements Serializable {

	// database columns
	private String userId; // USER_ID; 사용자아이디;
	private int roleId;
	
	//메타데이터 테이블 관련
	private String sysNm;		//시스템명
	private String enTblNm;		//영문테이블명
	private String krTblNm;		//국문테이블명
	private String loadType;	//적재타입
	private String ptntTblYn;	//파티션테이블여부
	private String regDdtm;		//등록일
	private String useYn;		//사용유무
	private String chgDatBasCol;	// ETL 변경 조건 컬럼
	
	//메타데이터 컬럼 관련
	private String enColNm;	//영문컬럼명
	private String krColNm;	//국문컬럼명
	private String oraDatType;  //ORCALE DATA TYPE
	private String hvdatType;   //HIVE DATA TYPE
	private String kdDatType;   //KUDU DATA TYPE
	private String pkYn;       //pk여부
	private String pkOrdr;     //pk순서
	private String nullable;      //not null 허용여부
	private String ptntColYn;   //파티션컬럼여부
	private String ptntColConvSntx; //파티션컬럼변환식
	private String sstvInfColYn;    //민감정보 포함여부
	private String sstvInfType; //민감정보 분류
	private String chgDatBasColYn;  //변경적재기준컬럼여부
	private String colOrdr;  //컬럼 순번
	
	
	public String getChgDatBasCol() {
		return chgDatBasCol;
	}
	public void setChgDatBasCol(String chgDatBasCol) {
		this.chgDatBasCol = chgDatBasCol;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSysNm() {
		return sysNm;
	}
	public void setSysNm(String sysNm) {
		this.sysNm = sysNm;
	}
	public String getEnTblNm() {
		return enTblNm;
	}
	public void setEnTblNm(String enTblNm) {
		this.enTblNm = enTblNm;
	}
	public String getKrTblNm() {
		return krTblNm;
	}
	public void setKrTblNm(String krTblNm) {
		this.krTblNm = krTblNm;
	}
	public String getLoadType() {
		return loadType;
	}
	public void setLoadType(String loadType) {
		this.loadType = loadType;
	}
	public String getPtntTblYn() {
		return ptntTblYn;
	}
	public void setPtntTblYn(String ptntTblYn) {
		this.ptntTblYn = ptntTblYn;
	}
	public String getRegDdtm() {
		return regDdtm;
	}
	public void setRegDdtm(String regDdtm) {
		this.regDdtm = regDdtm;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public String getEnColNm() {
		return enColNm;
	}
	public void setEnColNm(String enColNm) {
		this.enColNm = enColNm;
	}
	public String getKrColNm() {
		return krColNm;
	}
	public void setKrColNm(String krColNm) {
		this.krColNm = krColNm;
	}
	public String getOraDatType() {
		return oraDatType;
	}
	public void setOraDatType(String oraDatType) {
		this.oraDatType = oraDatType;
	}
	public String getHvdatType() {
		return hvdatType;
	}
	public void setHvdatType(String hvdatType) {
		this.hvdatType = hvdatType;
	}
	public String getKdDatType() {
		return kdDatType;
	}
	public void setKdDatType(String kdDatType) {
		this.kdDatType = kdDatType;
	}
	public String getPkYn() {
		return pkYn;
	}
	public void setPkYn(String pkYn) {
		this.pkYn = pkYn;
	}
	public String getPkOrdr() {
		return pkOrdr;
	}
	public void setPkOrdr(String pkOrdr) {
		this.pkOrdr = pkOrdr;
	}
	public String getNullable() {
		return nullable;
	}
	public void setNullable(String nullable) {
		this.nullable = nullable;
	}
	public String getPtntColYn() {
		return ptntColYn;
	}
	public void setPtntColYn(String ptntColYn) {
		this.ptntColYn = ptntColYn;
	}
	public String getPtntColConvSntx() {
		return ptntColConvSntx;
	}
	public void setPtntColConvSntx(String ptntColConvSntx) {
		this.ptntColConvSntx = ptntColConvSntx;
	}
	public String getSstvInfColYn() {
		return sstvInfColYn;
	}
	public void setSstvInfColYn(String sstvInfColYn) {
		this.sstvInfColYn = sstvInfColYn;
	}
	public String getSstvInfType() {
		return sstvInfType;
	}
	public void setSstvInfType(String sstvInfType) {
		this.sstvInfType = sstvInfType;
	}
	public String getChgDatBasColYn() {
		return chgDatBasColYn;
	}
	public void setChgDatBasColYn(String chgDatBasColYn) {
		this.chgDatBasColYn = chgDatBasColYn;
	}
	public String getColOrdr() {
		return colOrdr;
	}
	public void setColOrdr(String colOrdr) {
		this.colOrdr = colOrdr;
	}
}

	
