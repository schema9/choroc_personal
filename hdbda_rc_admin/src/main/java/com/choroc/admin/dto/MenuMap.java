package com.choroc.admin.dto;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.AbstractAdminToolObject;

/**
 * TODO >
 * 파일명   > MenuMap.java
 * 작성일   > 2019. 1. 9.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */

@Repository
public class MenuMap extends AbstractAdminToolObject implements Serializable{

	private int seq;
	private int role_id;
	private int menu_id;
	private char readable_yn;
	private char writeable_yn;
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public int getRole_id() {
		return role_id;
	}
	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}
	public int getMenu_id() {
		return menu_id;
	}
	public void setMenu_id(int menu_id) {
		this.menu_id = menu_id;
	}
	public char getReadable_yn() {
		return readable_yn;
	}
	public void setReadable_yn(char readable_yn) {
		this.readable_yn = readable_yn;
	}
	public char getWriteable_yn() {
		return writeable_yn;
	}
	public void setWriteable_yn(char writeable_yn) {
		this.writeable_yn = writeable_yn;
	}
	
}
