package com.choroc.admin.dto;

import java.io.Serializable;

public class ItemInfo implements Serializable {
	
	private String itemId;
	private String serviceId;
	private String itemType;
	private String promotionId;
	private String categoryId;
	private String brandId;
	private String itemTitle;
	private String itemDesc;
	private long itemPrice;
	private String itemUrl;
	private String itemThumbnail;
	private String itemStatus;
	private long createDateTime;
	private long modifyDateTime;
	/**
	 * @return the itemId
	 */
	public String getItemId() {
		return itemId;
	}
	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	/**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}
	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	/**
	 * @return the itemType
	 */
	public String getItemType() {
		return itemType;
	}
	/**
	 * @param itemType the itemType to set
	 */
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	/**
	 * @return the promotionId
	 */
	public String getPromotionId() {
		return promotionId;
	}
	/**
	 * @param promotionId the promotionId to set
	 */
	public void setPromotionId(String promotionId) {
		this.promotionId = promotionId;
	}
	/**
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return categoryId;
	}
	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	/**
	 * @return the brandId
	 */
	public String getBrandId() {
		return brandId;
	}
	/**
	 * @param brandId the brandId to set
	 */
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	/**
	 * @return the itemTitle
	 */
	public String getItemTitle() {
		return itemTitle;
	}
	/**
	 * @param itemTitle the itemTitle to set
	 */
	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}
	/**
	 * @return the itemDesc
	 */
	public String getItemDesc() {
		return itemDesc;
	}
	/**
	 * @param itemDesc the itemDesc to set
	 */
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	/**
	 * @return the itemPrice
	 */
	public long getItemPrice() {
		return itemPrice;
	}
	/**
	 * @param itemPrice the itemPrice to set
	 */
	public void setItemPrice(long itemPrice) {
		this.itemPrice = itemPrice;
	}
	/**
	 * @return the itemUrl
	 */
	public String getItemUrl() {
		return itemUrl;
	}
	/**
	 * @param itemUrl the itemUrl to set
	 */
	public void setItemUrl(String itemUrl) {
		this.itemUrl = itemUrl;
	}
	/**
	 * @return the itemThumbnail
	 */
	public String getItemThumbnail() {
		return itemThumbnail;
	}
	/**
	 * @param itemThumbnail the itemThumbnail to set
	 */
	public void setItemThumbnail(String itemThumbnail) {
		this.itemThumbnail = itemThumbnail;
	}
	/**
	 * @return the itemStatus
	 */
	public String getItemStatus() {
		return itemStatus;
	}
	/**
	 * @param itemStatus the itemStatus to set
	 */
	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}
	/**
	 * @return the createDateTime
	 */
	public long getCreateDateTime() {
		return createDateTime;
	}
	/**
	 * @param createDateTime the createDateTime to set
	 */
	public void setCreateDateTime(long createDateTime) {
		this.createDateTime = createDateTime;
	}
	/**
	 * @return the modifyDateTime
	 */
	public long getModifyDateTime() {
		return modifyDateTime;
	}
	/**
	 * @param modifyDateTime the modifyDateTime to set
	 */
	public void setModifyDateTime(long modifyDateTime) {
		this.modifyDateTime = modifyDateTime;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ItemInfo [itemId=" + itemId + ", serviceId=" + serviceId
				+ ", itemType=" + itemType + ", promotionId=" + promotionId
				+ ", categoryId=" + categoryId + ", brandId=" + brandId
				+ ", itemTitle=" + itemTitle + ", itemDesc=" + itemDesc
				+ ", itemPrice=" + itemPrice + ", itemUrl=" + itemUrl
				+ ", itemThumbnail=" + itemThumbnail + ", itemStatus="
				+ itemStatus + ", createDateTime=" + createDateTime
				+ ", modifyDateTime=" + modifyDateTime + "]";
	}
	

}
