package com.choroc.admin.dto;

import java.io.Serializable;
import java.util.Date;

import com.choroc.admin.domain.AbstractAdminToolObject;

/**
 * TODO > 사용자 계정정보 DTO
 * 파일명   > AuthUser.java
 * 작성일   > 2018. 11. 13.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public class AuthUser extends AbstractAdminToolObject implements Serializable {

	private static final long serialVersionUID = 1L;
	private String userId; 			// 사용자아이디                - [USER_ID]
	private String userPassword;   	// 사용자비밀번호 	  - [USER_PASSWORD] 
	private String lastConectIp;   	// 최종 접속 IP  	  - [LAST_CONECT_IP]
	private String acntLockYn; 		// 계정잠금 여부               - [ACNT_LOCK_YN]
	private String drmncyYn; 		// 휴면계정 여부               - [DRMNCY_YN]
	private Date lastLoginSuccesDt; // 최종 로그인 성공 일시    - [LAST_LOGIN_SUCCES_DT]
	private Date lastLoginFailrDt; 	// 최종 로그인 실패 일시    - [LAST_LOGIN_FAILR_DT]
	private Date acntLockDt; 		// 계정 잠금 일시              - [ACNT_LOCK_DT]
	private Date drmncyDt;         	// 휴면계정 일시               - [DRMNCY_DT]
	private Date lastPasswdChgDt; 	// 최종 비밀번호 변경일시  - [LAST_PASSWD_CHG_DT]
	private int loginFailrDtCo; 	// 로그인 실패 횟수           - [LOGIN_FAILR_DT_CO]
	private String passwordResetYn; // 비밀번호 초기화 여부     - [PASSWORD_RESET_YN]
	private String domainUseYn; 	// 도메인 사용유무            - [MNG_DOMAIN_INFO.USE_YN]
	private int roleId; 			// 권한 아이디
	private String userRoleDc;      // 권한 설명
	private int lastLoginDiffDate;  // 최종 로그인 경과 개월
	private int lastPasswdDiffDate; // 비밀번호 변경 후 경과 개월
	private int acntLockDiffDate; 	// 계정잠금 후 경과 시간(분)

	// getter and setter
	public String getUserRoleDc() {
		return userRoleDc;
	}

	public void setUserRoleDc(String userRoleDc) {
		this.userRoleDc = userRoleDc;
	}
	
	// getter and setter
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getLastConectIp() {
		return lastConectIp;
	}

	public void setLastConectIp(String lastConectIp) {
		this.lastConectIp = lastConectIp;
	}

	public String getAcntLockYn() {
		return acntLockYn;
	}

	public void setAcntLockYn(String acntLockYn) {
		this.acntLockYn = acntLockYn;
	}

	public String getDrmncyYn() {
		return drmncyYn;
	}

	public void setDrmncyYn(String drmncyYn) {
		this.drmncyYn = drmncyYn;
	}

	public Date getLastLoginSuccesDt() {
		return lastLoginSuccesDt;
	}

	public void setLastLoginSuccesDt(Date lastLoginSuccesDt) {
		this.lastLoginSuccesDt = lastLoginSuccesDt;
	}

	public Date getLastLoginFailrDt() {
		return lastLoginFailrDt;
	}

	public void setLastLoginFailrDt(Date lastLoginFailrDt) {
		this.lastLoginFailrDt = lastLoginFailrDt;
	}

	public Date getAcntLockDt() {
		return acntLockDt;
	}

	public void setAcntLockDt(Date acntLockDt) {
		this.acntLockDt = acntLockDt;
	}

	public Date getDrmncyDt() {
		return drmncyDt;
	}

	public void setDrmncyDt(Date drmncyDt) {
		this.drmncyDt = drmncyDt;
	}

	public Date getLastPasswdChgDt() {
		return lastPasswdChgDt;
	}

	public void setLastPasswdChgDt(Date lastPasswdChgDt) {
		this.lastPasswdChgDt = lastPasswdChgDt;
	}

	public int getLoginFailrDtCo() {
		return loginFailrDtCo;
	}

	public void setLoginFailrDtCo(int loginFailrDtCo) {
		this.loginFailrDtCo = loginFailrDtCo;
	}

	public int getLastLoginDiffDate() {
		return lastLoginDiffDate;
	}

	public void setLastLoginDiffDate(int lastLoginDiffDate) {
		this.lastLoginDiffDate = lastLoginDiffDate;
	}
	
	public int getLastPasswdDiffDate() {
		return lastPasswdDiffDate;
	}

	public void setLastPasswdDiffDate(int lastPasswdDiffDate) {
		this.lastPasswdDiffDate = lastPasswdDiffDate;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getAcntLockDiffDate() {
		return acntLockDiffDate;
	}

	public void setAcntLockDiffDate(int acntLockDiffDate) {
		this.acntLockDiffDate = acntLockDiffDate;
	}

	public String getPasswordResetYn() {
		return passwordResetYn;
	}

	public void setPasswordResetYn(String passwordResetYn) {
		this.passwordResetYn = passwordResetYn;
	}

	public String getDomainUseYn() {
		return domainUseYn;
	}

	public void setDomainUseYn(String domainUseYn) {
		this.domainUseYn = domainUseYn;
	}
}
