package com.choroc.admin.dto;

public class SchedulerTask {
	
	private String task_id;
	private String job_id;
	private String cron_expr;
	private String exec_prog;
	private String param_str;
	private String option_str;
	private String etl_tgt_sys;
	private String etl_tgt_tbl;
	private String lst_chg_dat_cond;
	private String reg_ddtm;
	private String stts_cd;
	private String stts_dtl;
	private String strt_ddtm;
	private String end_ddtm;
	private String job_type;
	
	
	
	
	
	public String getStrt_ddtm() {
		return strt_ddtm;
	}
	public void setStrt_ddtm(String strt_ddtm) {
		this.strt_ddtm = strt_ddtm;
	}
	public String getJob_type() {
		return job_type;
	}
	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}
	public String getStts_cd() {
		return stts_cd;
	}
	public void setStts_cd(String stts_cd) {
		this.stts_cd = stts_cd;
	}
	public String getStts_dtl() {
		return stts_dtl;
	}
	public void setStts_dtl(String stts_dtl) {
		this.stts_dtl = stts_dtl;
	}
	public String getEnd_ddtm() {
		return end_ddtm;
	}
	public void setEnd_ddtm(String end_ddtm) {
		this.end_ddtm = end_ddtm;
	}
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}
	public String getCron_expr() {
		return cron_expr;
	}
	public void setCron_expr(String cron_expr) {
		this.cron_expr = cron_expr;
	}
	public String getExec_prog() {
		return exec_prog;
	}
	public void setExec_prog(String exec_prog) {
		this.exec_prog = exec_prog;
	}
	public String getParam_str() {
		return param_str;
	}
	public void setParam_str(String param_str) {
		this.param_str = param_str;
	}
	public String getOption_str() {
		return option_str;
	}
	public void setOption_str(String option_str) {
		this.option_str = option_str;
	}
	public String getEtl_tgt_sys() {
		return etl_tgt_sys;
	}
	public void setEtl_tgt_sys(String etl_tgt_sys) {
		this.etl_tgt_sys = etl_tgt_sys;
	}
	public String getEtl_tgt_tbl() {
		return etl_tgt_tbl;
	}
	public void setEtl_tgt_tbl(String etl_tgt_tbl) {
		this.etl_tgt_tbl = etl_tgt_tbl;
	}
	public String getLst_chg_dat_cond() {
		return lst_chg_dat_cond;
	}
	public void setLst_chg_dat_cond(String lst_chg_dat_cond) {
		this.lst_chg_dat_cond = lst_chg_dat_cond;
	}
	public String getReg_ddtm() {
		return reg_ddtm;
	}
	public void setReg_ddtm(String reg_ddtm) {
		this.reg_ddtm = reg_ddtm;
	}
	
	
}
