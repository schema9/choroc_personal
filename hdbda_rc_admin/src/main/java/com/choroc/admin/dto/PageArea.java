package com.choroc.admin.dto;

import java.io.Serializable;

import com.choroc.admin.domain.AbstractAdminToolObject;

public class PageArea extends AbstractAdminToolObject implements Serializable {

	private static final long serialVersionUID = 1L;
	// RCM_PAGE_AREA			    // 페이지 영역 정보
    private int     pgeRelmId;		// PGE_RELM_ID			페이지영역아이디	NOT NULL
    private String  serviceId; 		// SERVICE_ID 			서비스아이디		NOT NULL
    private String  pgeRelmNm; 		// PGE_RELM_NM 			페이지영역명		NOT NULL
    private String  pgeRelmDc; 		// PGE_RELM_DC 			페이지영역설명

    public int getPgeRelmId() {
        return pgeRelmId;
    }

    public void setPgeRelmId(int pgeRelmId) {
        this.pgeRelmId = pgeRelmId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getPgeRelmNm() {
        return pgeRelmNm;
    }

    public void setPgeRelmNm(String pgeRelmNm) {
        this.pgeRelmNm = pgeRelmNm;
    }

    public String getPgeRelmDc() {
        return pgeRelmDc;
    }

    public void setPgeRelmDc(String pgeRelmDc) {
        this.pgeRelmDc = pgeRelmDc;
    }
}
