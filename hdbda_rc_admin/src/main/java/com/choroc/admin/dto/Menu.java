package com.choroc.admin.dto;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.AbstractAdminToolObject;

@Repository
public class Menu extends AbstractAdminToolObject implements Serializable {
	// database columns [ MNG_MENU ]
	private int seq;             // SEQ; 일련 번호;
	private String menuNm;       // MENU_NM; 메뉴 명;
	private String menuUrl;      // MENU_URL; 메뉴 URL;
	private String menuDc;       // MENU_DC; 메뉴 설명;
	private String menuIconUrl;  // MENU_ICON_URL; 메뉴 아이콘 URL;
	private int upperMenuId;     // UPPER_MENU_ID; 상위 메뉴 아이디;
	private int sortOrder;       // SORT_ORDER; 정렬 순서;
	private String cssClassNm;   // CSS_CLASS_NM; CSS 클래스명;

	// user additional items
	private int roleId;
	private int menuId;
	private String menuTitle;
	private int parentId;
	private String menuDesc;
	private String cssClass;
	private boolean hasChild;
	private List<Menu> childs;
	private boolean hasDepth1;
	private List<Menu> depth1;
	private boolean hasDepth2;
	private List<Menu> depth2;
	private boolean leaf;
	private int displayLevel;
			
	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	public String getMenuNm() {
		return menuNm;
	}

	public void setMenuNm(String menuNm) {
		this.menuNm = menuNm;
	}

	public String getMenuUrl() {
		return menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	public String getMenuDc() {
		return menuDc;
	}

	public void setMenuDc(String menuDc) {
		this.menuDc = menuDc;
	}

	public String getMenuIconUrl() {
		return menuIconUrl;
	}

	public void setMenuIconUrl(String menuIconUrl) {
		this.menuIconUrl = menuIconUrl;
	}

	public int getUpperMenuId() {
		return upperMenuId;
	}

	public void setUpperMenuId(int upperMenuId) {
		this.upperMenuId = upperMenuId;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getCssClassNm() {
		return cssClassNm;
	}

	public void setCssClassNm(String cssClassNm) {
		this.cssClassNm = cssClassNm;
	}

	public String getMenuTitle() {
		return menuTitle;
	}

	public void setMenuTitle(String menuTitle) {
		this.menuTitle = menuTitle;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public String getMenuDesc() {
		return menuDesc;
	}

	public void setMenuDesc(String menuDesc) {
		this.menuDesc = menuDesc;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public boolean isHasChild() {
		return hasChild;
	}

	public void setHasChild(boolean hasChild) {
		this.hasChild = hasChild;
	}

	public List<Menu> getChilds() {
		return childs;
	}

	public void setChilds(List<Menu> childs) {
		this.childs = childs;
	}

	public boolean isHasDepth1() {
		return hasDepth1;
	}

	public void setHasDepth1(boolean hasDepth1) {
		this.hasDepth1 = hasDepth1;
	}

	public List<Menu> getDepth1() {
		return depth1;
	}

	public void setDepth1(List<Menu> depth1) {
		this.depth1 = depth1;
	}

	public boolean isHasDepth2() {
		return hasDepth2;
	}

	public void setHasDepth2(boolean hasDepth2) {
		this.hasDepth2 = hasDepth2;
	}

	public List<Menu> getDepth2() {
		return depth2;
	}

	public void setDepth2(List<Menu> depth2) {
		this.depth2 = depth2;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getDisplayLevel() {
		return displayLevel;
	}

	public void setDisplayLevel(int displayLevel) {
		this.displayLevel = displayLevel;
	}

}
