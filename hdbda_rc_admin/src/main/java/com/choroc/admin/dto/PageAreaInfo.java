package com.choroc.admin.dto;

import java.io.Serializable;

public class PageAreaInfo extends PageArea implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String controlMode; // M: modify; D: delete;
	private int domainId;
	private String domainNm;
	private String domainUrl;
	private String widgId;

	public String getControlMode() {
		return controlMode;
	}

	public void setControlMode(String controlMode) {
		this.controlMode = controlMode;
	}

	public int getDomainId() {
		return domainId;
	}

	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

	public String getDomainNm() {
		return domainNm;
	}

	public void setDomainNm(String domainNm) {
		this.domainNm = domainNm;
	}

	public String getDomainUrl() {
		return domainUrl;
	}

	public void setDomainUrl(String domainUrl) {
		this.domainUrl = domainUrl;
	}

	public String getWidgId() {
		return widgId;
	}

	public void setWidgId(String widgId) {
		this.widgId = widgId;
	}

}
