package com.choroc.admin.dto;

import java.util.List;

public class SystemManagerInfo extends SystemManager {

    private String controlMode;                     // M: modify; D: delete;
    private List<ServiceInfo> serviceListOfDomain;  // service list for all in specified domain
    private List<ServiceInfo> serviceListOfUser;    // service list within user ID
    private String [] stringList;

    // MNG_PLATFORM_ROLES
    private int roleId;             // ROLE_ID;             권한아이디
    private String roleIconUrl;     // ROLE_ICON_URL;       권한아이콘 URL;
    private String userRoleNm;      // USER_ROLE_NM;        사용자권한명;
    private String userRoleDc;      // USER_ROLE_DC;        사용자권한설명;

    // MNG_DOMAIN_INFO
    private int domainId;           // SEQ;                 도메인아이디;
    private String domainNm;        // DOMAIN_NM;           도메인명;
    private String domainURL;       // DOMAIN_URL;          도메인 URL;
    private String domainDc;        // DOMAIN_DC;           도메인 설명;
    private String domainEmail;     // DOMAIN_EMAIL;        도메인 대표 이메일;
    private String domainSttusCode; // DOMAIN_STTUS_CODE;   도메인 상태코드;
    private String domainImageURL;  // IMAGE_URL;           이미지 URL;

    // MNG_SERVICE_INFO
    private String serviceId;       // SERVICE_ID;          서비스아이디;
    private String serviceNm;       // SERVICE_NM;          서비스명;
    private String serviceDc;       // SERVICE_DC;          서비스 설명;
    private String serviceSttusCode;// SERVICE_STTUS_CODE;  서비스 상태;

    public String getControlMode() {
        return controlMode;
    }

    public void setControlMode(String controlMode) {
        this.controlMode = controlMode;
    }

    public List<ServiceInfo> getServiceListOfDomain() {
        return serviceListOfDomain;
    }

    public void setServiceListOfDomain(List<ServiceInfo> serviceListOfDomain) {
        this.serviceListOfDomain = serviceListOfDomain;
    }

    public List<ServiceInfo> getServiceListOfUser() {
        return serviceListOfUser;
    }

    public void setServiceListOfUser(List<ServiceInfo> serviceListOfUser) {
        this.serviceListOfUser = serviceListOfUser;
    }

    @Override
    public int getRoleId() {
        return roleId;
    }

    @Override
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleIconUrl() {
        return roleIconUrl;
    }

    public void setRoleIconUrl(String roleIconUrl) {
        this.roleIconUrl = roleIconUrl;
    }

    public String getUserRoleNm() {
        return userRoleNm;
    }

    public void setUserRoleNm(String userRoleNm) {
        this.userRoleNm = userRoleNm;
    }

    public String getUserRoleDc() {
        return userRoleDc;
    }

    public void setUserRoleDc(String userRoleDc) {
        this.userRoleDc = userRoleDc;
    }

    public int getDomainId() {
        return domainId;
    }

    public void setDomainId(int domainId) {
        this.domainId = domainId;
    }

    public String getDomainNm() {
        return domainNm;
    }

    public void setDomainNm(String domainNm) {
        this.domainNm = domainNm;
    }

    public String getDomainURL() {
        return domainURL;
    }

    public void setDomainURL(String domainURL) {
        this.domainURL = domainURL;
    }

    public String getDomainDc() {
        return domainDc;
    }

    public void setDomainDc(String domainDc) {
        this.domainDc = domainDc;
    }

    public String getDomainEmail() {
        return domainEmail;
    }

    public void setDomainEmail(String domainEmail) {
        this.domainEmail = domainEmail;
    }

    public String getDomainSttusCode() {
        return domainSttusCode;
    }

    public void setDomainSttusCode(String domainSttusCode) {
        this.domainSttusCode = domainSttusCode;
    }

    public String getDomainImageURL() {
        return domainImageURL;
    }

    public void setDomainImageURL(String domainImageURL) {
        this.domainImageURL = domainImageURL;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceNm() {
        return serviceNm;
    }

    public void setServiceNm(String serviceNm) {
        this.serviceNm = serviceNm;
    }

    public String getServiceDc() {
        return serviceDc;
    }

    public void setServiceDc(String serviceDc) {
        this.serviceDc = serviceDc;
    }

    public String getServiceSttusCode() {
        return serviceSttusCode;
    }

    public void setServiceSttusCode(String serviceSttusCode) {
        this.serviceSttusCode = serviceSttusCode;
    }

    public String[] getStringList() {
        return stringList;
    }

    public void setStringList(String[] stringList) {
        this.stringList = stringList;
    }	
}
