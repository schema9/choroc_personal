package com.choroc.admin.dto;

import java.io.Serializable;
import java.util.List;

import com.choroc.admin.domain.AbstractAdminToolObject;

public class DomainInfo extends AbstractAdminToolObject implements Serializable {
	
	private int seq;
	private int domainId;
	private String domainNm;
	private String domainUrl;
	private String domainDc;
	private String domainEmail;
	private String imageUrl;
	private int sortOrder;
	private String beginDe;
	private String endDe;
	private String domainSttusCode;
	private String codeNm;
	private String accessKey;
	private String status;
	private String writeType;
	private String userId;
	private String userIdCreateDt;
	private String apiIsuYn;
	private String apiIsuDt;
	
	private List<ServiceInfo> services;

	private int expirationDiffDate;
	private String expirationYn;


	/**
	 * @return the seq
	 */
	public int getSeq() {
		return seq;
	}
	/**
	 * @param seq the seq to set
	 */
	public void setSeq(int seq) {
		this.seq = seq;
	}
	/**
	 * @return the domainId
	 */
	public int getDomainId() {
		return domainId;
	}
	/**
	 * @param domainId the domainId to set
	 */
	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}
	/**
	 * @return the domainNm
	 */
	public String getDomainNm() {
		return domainNm;
	}
	/**
	 * @param domainNm the domainNm to set
	 */
	public void setDomainNm(String domainNm) {
		this.domainNm = domainNm;
	}
	/**
	 * @return the domainUrl
	 */
	public String getDomainUrl() {
		return domainUrl;
	}
	/**
	 * @param domainUrl the domainUrl to set
	 */
	public void setDomainUrl(String domainUrl) {
		this.domainUrl = domainUrl;
	}
	/**
	 * @return the domainDc
	 */
	public String domainDc() {
		return domainDc;
	}
	/**
	 * @param domainDc the domainDc to set
	 */
	public void setDomainDc(String domainDc) {
		this.domainDc = domainDc;
	}
	/**
	 * @return the domainEmail
	 */
	public String domainEmail() {
		return domainEmail;
	}
	/**
	 * @param domainEmail the domainEmail to set
	 */
	public void setDomainEmail(String domainEmail) {
		this.domainEmail = domainEmail;
	}

	public String getBeginDe() {
		return beginDe;
	}
	public void setBeginDe(String beginDe) {
		this.beginDe = beginDe;
	}

	public String getEndDe() {
		return endDe;
	}
	public void setEndDe(String endDe) {
		this.endDe = endDe;
	}

	public String getDomainSttusCode() {
		return domainSttusCode;
	}
	public void setDomainSttusCode(String domainSttusCode) {
		this.domainSttusCode = domainSttusCode;
	}

	public String getCodeNm() {
		return codeNm;
	}
	public void setCodeNm(String codeNm) {
		this.codeNm = codeNm;
	}



	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserIdCreateDt() {
		return userIdCreateDt;
	}
	public void setUserIdCreateDt(String userIdCreateDt) {
		this.userIdCreateDt = userIdCreateDt;
	}

	public String getApiIsuYn() {
		return apiIsuYn;
	}
	public void setApiIsuYn(String apiIsuYn) {
		this.apiIsuYn = apiIsuYn;
	}

	public String getApiIsuDt() {
		return apiIsuDt;
	}
	public void setApiIsuDt(String apiIsuDt) {
		this.apiIsuDt = apiIsuDt;
	}



	/**
	 * @return the accessKey
	 */
	public String getAccessKey() {
		return accessKey;
	}
	/**
	 * @param accessKey the accessKey to set
	 */
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	/**
	 * @return the services
	 */
	public List<ServiceInfo> getServices() {
		return services;
	}
	/**
	 * @param services the services to set
	 */
	public void setServices(List<ServiceInfo> services) {
		this.services = services;
	}
	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}
	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return the sortOrder
	 */
	public int getSortOrder() {
		return sortOrder;
	}
	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	/**
	 * @return the writeType
	 */
	public String getWriteType() {
		return writeType;
	}
	
	/**
	 * @param writeType the writeType to set
	 */
	public void setWriteType(String writeType) {
		this.writeType = writeType;
	}


	public int getExpirationDiffDate() { return expirationDiffDate; }
	public void setExpirationDiffDate(int expirationDiffDate) { this.expirationDiffDate = expirationDiffDate; }

	public String getExpirationYn() { return expirationYn; }
	public void setExpirationYn(String expirationYn) { this.expirationYn = expirationYn; }

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DomainInfo [domainId=" + domainId
				+ ", domainNm =" + domainNm
				+ ", domainUrl=" + domainUrl
				+ ", domainDc=" + domainDc
				+ ", domainEmail=" + domainEmail
				+ ", imageUrl=" + imageUrl
				+ ", sortOrder=" + sortOrder
				+ ", beginDe=" + beginDe
				+ ", endDe=" + endDe
				+ ", domainSttusCode=" + domainSttusCode
				+ ", userId=" + userId

				+ ", userIdCreateDt=" + userIdCreateDt
				+ ", apiIsuYn=" + apiIsuYn
				+ ", apiIsuDt=" + apiIsuDt

				+ ", codeNm=" + codeNm

				+ ", accessKey=" + accessKey
				+ ", status=" + status
				+ ", services=" + services
				+ ", toString()=" + super.toString() + "]";
	}
	
}
