package com.choroc.admin.dto;

import java.io.Serializable;

import com.choroc.admin.domain.AbstractAdminToolObject;

public class RoleMenuMapping extends AbstractAdminToolObject implements Serializable {
		
	private static final long serialVersionUID = 1L;
	// database columns [ MNG_ROLE_MENU_MAP ]
	private int seq; // SEQ; 일련 번호;
	private int roleId; // ROLE_ID; 권한 아이디;
	private int menuId; // MENU_ID; 메뉴 아이디;
	private String readableYn; // READABLE_YN; 읽기 가능 여부;
	private String writeableYn; // WRITEABLE_YN; 쓰기 가능 여부;

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	public String getReadableYn() {
		return readableYn;
	}

	public void setReadableYn(String readableYn) {
		this.readableYn = readableYn;
	}

	public String getWriteableYn() {
		return writeableYn;
	}

	public void setWriteableYn(String writeableYn) {
		this.writeableYn = writeableYn;
	}
}
