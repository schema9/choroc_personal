package com.choroc.admin.dto;

import java.io.Serializable;

import com.choroc.admin.domain.AbstractAdminToolObject;

public class SystemAlgorithmInfo extends AbstractAdminToolObject implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String algoId;
	private String algoNm;
	private String dfltAlgoYn;
	private String algoDc;
	private String algoTyNm;
	private String algoTableNm;
	private String controlMode;
	private String codeNm;

	public String getAlgoId() {
		return algoId;
	}

	public void setAlgoId(String algoId) {
		this.algoId = algoId;
	}

	public String getAlgoNm() {
		return algoNm;
	}

	public void setAlgoNm(String algoNm) {
		this.algoNm = algoNm;
	}

	public String getDfltAlgoYn() {
		return dfltAlgoYn;
	}

	public void setDfltAlgoYn(String dfltAlgoYn) {
		this.dfltAlgoYn = dfltAlgoYn;
	}

	public String getAlgoDc() {
		return algoDc;
	}

	public void setAlgoDc(String algoDc) {
		this.algoDc = algoDc;
	}

	public String getAlgoTyNm() {
		return algoTyNm;
	}

	public void setAlgoTyNm(String algoTyNm) {
		this.algoTyNm = algoTyNm;
	}

	public String getAlgoTableNm() {
		return algoTableNm;
	}

	public void setAlgoTableNm(String algoTableNm) {
		this.algoTableNm = algoTableNm;
	}

	public String getControlMode() {
		return controlMode;
	}

	public void setControlMode(String controlMode) {
		this.controlMode = controlMode;
	}

	public String getCodeNm() {
		return codeNm;
	}

	public void setCodeNm(String codeNm) {
		this.codeNm = codeNm;
	}
}
