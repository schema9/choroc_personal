package com.choroc.admin.filter;

import java.util.regex.Pattern;

/**
 * @FileName:XSSUtil.java
 * @Author  :KimSangSu
 * @Date    :2018. 11. 7.
 * @기능설명  :
 * @변경이력  : 
 */

public class XSSUtil {
	public XSSUtil() {
	}

	public static String clearXSSMinimum(String value) {
		if (value != null && !value.trim().equals("")) {
			String returnValue = value.replaceAll("&", "&amp;");
			returnValue = returnValue.replaceAll("<", "&lt;");
			returnValue = returnValue.replaceAll(">", "&gt;");
			returnValue = returnValue.replaceAll("\"", "&#34;");
			returnValue = returnValue.replaceAll("\'", "&#39;");
			return returnValue;
		} else {
			return "";
		}
	}

	public static String clearXSSMaximum(String value) {
		String returnValue = clearXSSMinimum(value);
		returnValue = returnValue.replaceAll("%00", (String) null);
		returnValue = returnValue.replaceAll("%", "&#37;");
		returnValue = returnValue.replaceAll("\\.\\./", "");
		returnValue = returnValue.replaceAll("\\.\\.\\\\", "");
		returnValue = returnValue.replaceAll("\\./", "");
		returnValue = returnValue.replaceAll("%2F", "");
		return returnValue;
	}

	public static String filePathBlackList(String value) {
		if (value != null && !value.trim().equals("")) {
			String returnValue = value.replaceAll("\\.\\./", "");
			returnValue = returnValue.replaceAll("\\.\\.\\\\", "");
			return returnValue;
		} else {
			return "";
		}
	}

	public static String filePathReplaceAll(String value) {
		if (value != null && !value.trim().equals("")) {
			String returnValue = value.replaceAll("/", "");
			returnValue = returnValue.replaceAll("\\", "");
			returnValue = returnValue.replaceAll("\\.\\.", "");
			returnValue = returnValue.replaceAll("&", "");
			return returnValue;
		} else {
			return "";
		}
	}

	public static String filePathWhiteList(String value) {
		return value;
	}

	public static boolean isIPAddress(String str) {
		Pattern ipPattern = Pattern.compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}");
		return ipPattern.matcher(str).matches();
	}

	public static String removeCRLF(String parameter) {
		return parameter.replaceAll("\r", "").replaceAll("\n", "");
	}

	public static String removeSQLInjectionRisk(String parameter) {
		return parameter.replaceAll("\\p{Space}", "").replaceAll("\\*", "").replaceAll("%", "").replaceAll(";", "")
				.replaceAll("-", "").replaceAll("\\+", "").replaceAll(",", "");
	}

	public static String removeOSCmdRisk(String parameter) {
		return parameter.replaceAll("\\p{Space}", "").replaceAll("\\*", "").replaceAll("|", "").replaceAll(";", "");
	}
}
