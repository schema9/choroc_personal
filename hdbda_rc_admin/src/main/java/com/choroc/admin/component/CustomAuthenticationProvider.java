package com.choroc.admin.component;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.choroc.admin.util.SHAPasswordEncoder;

public class CustomAuthenticationProvider implements AuthenticationProvider {

	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> security.authentication.AuthenticationProvider <<<<<<<<<<<<<<<<<<<<<<<<<<<" );
		logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> authenticate <<<<<<<<<<<<<<<<<<<<<<<<<<<" );
		
		String user_id = (String)authentication.getPrincipal();
		String user_pw = (String)authentication.getCredentials();
		
		List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
		roles.add(new SimpleGrantedAuthority("ROLE_USER"));
		
		SHAPasswordEncoder shaPasswordEncoder = new SHAPasswordEncoder(256);
		shaPasswordEncoder.setEncodeHashAsBase64(true);
		user_pw = shaPasswordEncoder.encode(user_pw);
		
		UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(user_id, user_pw, roles);
		result.setDetails(new CustomUserDetails(user_id, user_pw));
		
		return result;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		
		logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> security.authentication.AuthenticationProvider <<<<<<<<<<<<<<<<<<<<<<<<<<<" );
		logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> supports <<<<<<<<<<<<<<<<<<<<<<<<<<<" );
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
