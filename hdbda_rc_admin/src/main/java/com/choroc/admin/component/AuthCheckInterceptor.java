package com.choroc.admin.component;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.choroc.admin.domain.AuthUserParam;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.dto.Menu;
import com.choroc.admin.service.MenuService;

/**
 * TODO > 인터셉터 
 * 파일명   > AuthCheckInterceptor.java
 * 작성일   > 2018. 11. 21.
 * 작성자   > CCmedia Service Corp.
 */
public class AuthCheckInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger(AuthCheckInterceptor.class);

	@Autowired
	private MenuService menuService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		logger.info("### [AuthCheckInterceptor preHandle() ] ###");
		
		HttpSession session = request.getSession();
		// 기존 세션정보를 삭제
		/*
		if(session.getAttribute("login") == null || session.getAttribute("userLoginInfo") == null) {
			logger.info("### 현재 로그인 상태가 아닙니다.");		
			response.sendRedirect("/login/login.do");
			return false;
		}
		*/
		
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		logger.info("### [Interceptor postHandle()] ###");
		
		/* 사용자 메뉴  */
		AuthUserParam authUserParam = new AuthUserParam();
		HttpSession session = null;
		session = request.getSession();
		AuthUser user = (AuthUser)session.getAttribute("userInfo");
		authUserParam.setAuthUser(user);
		
		// Menu
		List<Menu> myTopMenus = null;
		if (user != null) {
			myTopMenus = menuService.getMyTopMenus(authUserParam);
		}
		
		if(myTopMenus != null) {
			modelAndView.addObject("myTopMenus", myTopMenus);
		}

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {	
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

	
	public static String isNullToString(Object object) {	
		String string = "";		
		if(object != null) {
			string = object.toString().trim();
		}

		return string;
	}
	
}