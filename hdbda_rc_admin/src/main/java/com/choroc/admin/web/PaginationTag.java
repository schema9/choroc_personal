package com.choroc.admin.web;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

/**
 * TODO > 페이징 태그
 * 파일명   > PaginationTag.java
 * 작성일   > 2018. 11. 27.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public class PaginationTag extends TagSupport {
	
	private static final long serialVersionUID = 1L;
	private PaginationInfo paginationInfo;
	private String type;
	private String jsFunction;

	public PaginationTag() {
	}

	public int doEndTag() throws JspException {
		try {
			JspWriter e = this.pageContext.getOut();
			WebApplicationContext ctx = RequestContextUtils.getWebApplicationContext(this.pageContext.getRequest(), this.pageContext.getServletContext());
			//WebApplicationContext ctx = null;
			this.paginationInfo.setServletContextRoot(ctx.getServletContext().getContextPath());
			Object paginationManager;
			if (ctx.containsBean("paginationManager")) {
				paginationManager = (PaginationManager) ctx.getBean("paginationManager");
			} else {
				paginationManager = new DefaultPaginationManager();
			}

			PaginationRenderer paginationRenderer = ((PaginationManager) paginationManager).getRendererType(this.type);
			String contents = paginationRenderer.renderPagination(this.paginationInfo, this.jsFunction);
			e.println(contents);
			return 6;
		} catch (IOException var6) {
			throw new JspException();
		}
	}

	public void setJsFunction(String jsFunction) {
		this.jsFunction = jsFunction;
	}

	public void setPaginationInfo(PaginationInfo paginationInfo) {
		this.paginationInfo = paginationInfo;
	}

	public void setType(String type) {
		this.type = type;
	}
}
