package com.choroc.admin.web;

public interface PaginationRenderer {
	
	String renderPagination(PaginationInfo var1, String var2);
}
