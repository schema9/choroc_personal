package com.choroc.admin.web;

public class PaginationInfo {
	
    private String servletContextRoot;
    private int currentPageNo;
    private int recordCountPerPage;
    private int pageSize;
    private int totalRecordCount;
    private int totalPageCount;
    private int firstPageNoOnPageList;
    private int lastPageNoOnPageList;
    private int firstRecordIndex;
    private int lastRecordIndex;

    public PaginationInfo() {
    }

    public String getServletContextRoot() {
        return this.servletContextRoot;
    }

    public void setServletContextRoot(String servletContextRoot) {
        this.servletContextRoot = servletContextRoot;
    }

    public int getRecordCountPerPage() {
        return this.recordCountPerPage;
    }

    public void setRecordCountPerPage(int recordCountPerPage) {
        this.recordCountPerPage = recordCountPerPage;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getCurrentPageNo() {
        return this.currentPageNo <= 0?1:this.currentPageNo;
    }

    public void setCurrentPageNo(int currentPageNo) {
        if(currentPageNo <= 0) {
            this.currentPageNo = 1;
        } else {
            this.currentPageNo = currentPageNo;
        }

    }

    public void setTotalRecordCount(int totalRecordCount) {
        this.totalRecordCount = totalRecordCount;
    }

    public int getTotalRecordCount() {
        return this.totalRecordCount;
    }

    public int getTotalPageCount() {
        this.totalPageCount = (this.getTotalRecordCount() - 1) / this.getRecordCountPerPage() + 1;
        return this.totalPageCount;
    }

    public int getFirstPageNo() {
        return 1;
    }

    public int getLastPageNo() {
        return this.getTotalPageCount();
    }

    public int getFirstPageNoOnPageList() {
        this.firstPageNoOnPageList = (this.getCurrentPageNo() - 1) / this.getPageSize() * this.getPageSize() + 1;
        return this.firstPageNoOnPageList;
    }

    public int getLastPageNoOnPageList() {
        this.lastPageNoOnPageList = this.getFirstPageNoOnPageList() + this.getPageSize() - 1;
        if(this.lastPageNoOnPageList > this.getTotalPageCount()) {
            this.lastPageNoOnPageList = this.getTotalPageCount();
        }

        return this.lastPageNoOnPageList;
    }

    public int getFirstRecordIndex() {
        this.firstRecordIndex = (this.getCurrentPageNo() - 1) * this.getRecordCountPerPage();
        return this.firstRecordIndex;
    }

    public int getLastRecordIndex() {
        this.lastRecordIndex = this.getCurrentPageNo() * this.getRecordCountPerPage();
        return this.lastRecordIndex;
    }

	@Override
	public String toString() {
		
		return " PaginationInfo(페이징인포)[currentPageNo=" + currentPageNo + ", recordCountPerPage=" + recordCountPerPage + 
				                " , pageSize=" + pageSize  +   " , totalRecordCount=" + totalRecordCount + 
				                " , totalPageCount=" + totalPageCount + " , firstPageNoOnPageList=" + firstPageNoOnPageList +
				                " , lastPageNoOnPageList=" + lastPageNoOnPageList + " , firstRecordIndex=" + firstRecordIndex + 
				                " , lastRecordIndex=" + lastRecordIndex + "]";
	}
        
}
