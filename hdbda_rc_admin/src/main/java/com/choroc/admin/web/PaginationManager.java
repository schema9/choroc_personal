package com.choroc.admin.web;

public interface PaginationManager {
	
    PaginationRenderer getRendererType(String var1);
    
}
