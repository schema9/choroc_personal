package com.choroc.admin.web;

public class DefaultPaginationRenderer extends AbstractPaginationRenderer {
	public DefaultPaginationRenderer() {
		this.firstPageLabel = "<a href=\"#\" onclick=\"{0}({1}); return false;\">{2}... </a>&#160;";
		this.previousPageLabel = "<a href=\"#\" onclick=\"{0}({1}); return false;\"><<</a>&#160;";
		this.currentPageLabel = "<strong>{0}</strong>&#160;";
		this.otherPageLabel = "<a href=\"#\" onclick=\"{0}({1}); return false;\">{2}</a>&#160;";
		this.nextPageLabel = "<a href=\"#\" onclick=\"{0}({1}); return false;\">>></a>&#160;";
		this.lastPageLabel = "<a href=\"#\" onclick=\"{0}({1}); return false;\"> ...{2}</a>&#160;";
	}

	public String renderPagination(PaginationInfo paginationInfo, String jsFunction) {
		return super.renderPagination(paginationInfo, jsFunction);
	}

}
