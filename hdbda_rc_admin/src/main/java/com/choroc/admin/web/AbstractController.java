package com.choroc.admin.web;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;

public class AbstractController {
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	protected final Logger logDB = LoggerFactory.getLogger("adminActionDB");

	@Autowired
	protected MessageSource messageSource = null;

	public AbstractController() {
	}

	public String getMessage(String messageCode, Object[] messageParameters, String defaultMessage) {
		Locale locale = Locale.getDefault();
		return this.getMessage(messageCode, messageParameters, defaultMessage, locale);
	}

	public String getMessage(String messageCode, Object[] messageParameters, String defaultMessage, Locale locale) {
		return this.messageSource.getMessage(messageCode, messageParameters, defaultMessage, locale);
	}

	public Object getRequestAttribute(String name) {
		return RequestContextHolder.currentRequestAttributes().getAttribute(name, 1);
	}

	public void setRequestAttribute(String name, Object value) {
		RequestContextHolder.currentRequestAttributes().setAttribute(name, value, 1);
	}

	public WebApplicationContext getWebApplicationContext() {
		return ContextLoader.getCurrentWebApplicationContext();
	}
}
