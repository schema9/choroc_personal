package com.choroc.admin.web;

import java.text.MessageFormat;

public class AbstractPaginationRenderer implements PaginationRenderer {
	public String firstPageLabel;
	public String previousPageLabel;
	public String currentPageLabel;
	public String otherPageLabel;
	public String nextPageLabel;
	public String lastPageLabel;

	public AbstractPaginationRenderer() {
	}

	public String renderPagination(PaginationInfo paginationInfo, String jsFunction) {
		StringBuffer strBuff = new StringBuffer();
		String servletContextRoot = paginationInfo.getServletContextRoot();
		int firstPageNo = paginationInfo.getFirstPageNo();
		int firstPageNoOnPageList = paginationInfo.getFirstPageNoOnPageList();
		int totalPageCount = paginationInfo.getTotalPageCount();
		int pageSize = paginationInfo.getPageSize();
		int lastPageNoOnPageList = paginationInfo.getLastPageNoOnPageList();
		int currentPageNo = paginationInfo.getCurrentPageNo();
		int lastPageNo = paginationInfo.getLastPageNo();
		if (totalPageCount > pageSize && firstPageNoOnPageList > pageSize) {
			strBuff.append(MessageFormat.format(this.previousPageLabel,
					new Object[] { jsFunction, Integer.toString(firstPageNo), Integer.toString(firstPageNo) }));
			strBuff.append(MessageFormat.format(this.firstPageLabel,
					new Object[] { jsFunction, Integer.toString(firstPageNoOnPageList - 1), servletContextRoot }));
		}

		for (int i = firstPageNoOnPageList; i <= lastPageNoOnPageList; ++i) {
			if (i == currentPageNo) {
				strBuff.append(MessageFormat.format(this.currentPageLabel, new Object[] { Integer.toString(i) }));
			} else {
				strBuff.append(MessageFormat.format(this.otherPageLabel,
						new Object[] { jsFunction, Integer.toString(i), Integer.toString(i) }));
			}
		}

		if (totalPageCount > pageSize && lastPageNoOnPageList < totalPageCount) {
			strBuff.append(MessageFormat.format(this.lastPageLabel, new Object[] { jsFunction,
					Integer.toString(firstPageNoOnPageList + pageSize), servletContextRoot }));
			strBuff.append(MessageFormat.format(this.nextPageLabel,
					new Object[] { jsFunction, Integer.toString(lastPageNo), Integer.toString(lastPageNo) }));
		}

		return strBuff.toString();
	}
}
