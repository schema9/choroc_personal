package com.choroc.admin.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.service.UserAccountManagementService;
import com.choroc.admin.util.SHAPasswordEncoder;
import com.choroc.admin.web.AbstractController;

/**
 * TODO > My 정보 > 비밀번호 수정
 * 파일명   > MyInfoController.java
 * 작성자   > CCmedia Service Corp.
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/myinfo")
//@PropertySource("classpath:application-context.properties")
public class MyInfoController extends AbstractController {
	/** The UserAccountManagement service. */
	@Autowired
	private UserAccountManagementService userAccountManagementService;
   	
	/**
	 * Inits the binder.
	 * 
	 * @param binder the binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.KOREA);
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	/* My 정보 > 비밀번호 수정 init-search */
	@RequestMapping(value="/myInfoForm.do")
	public @ResponseBody ModelAndView myInfoForm(HttpServletRequest request, HttpServletResponse response, ModelMap model) {

		ModelAndView mav = new ModelAndView("myInfo/myInfoWriteForm");
		AuthUser authUser = (AuthUser)getRequestAttribute("userInfo");
		mav.addObject("authUser", authUser);
		
		return mav;
	}

	/**
	 * 사용자 개인 정보 수정.
	 * @param accountParam
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/getPasswordCheck.do", method = RequestMethod.POST)
	public @ResponseBody ModelAndView getPasswordCheck(@ModelAttribute("accountParam") AccountParam accountParam,
        HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("jsonView");
		AuthUser authUser = (AuthUser)getRequestAttribute("userInfo");

		SHAPasswordEncoder shaPasswordEncoder = new SHAPasswordEncoder(256);
		shaPasswordEncoder.setEncodeHashAsBase64(true);
		accountParam.setOldUserPassword(shaPasswordEncoder.encode(accountParam.getOldUserPassword()));
		accountParam.setUserId(authUser.getUserId());
		String messageCode = "";
		int result = 3;
		try {
			result = userAccountManagementService.getPasswordCheck(accountParam);
		} catch (Exception e) {
			e.printStackTrace();
			result = 3;
		}

		if (result == 3) {
			messageCode = "E";
		} else {
			messageCode = "OK";
		}
		mv.addObject("result", result);
		mv.addObject("messageCode", messageCode);
	    return mv;
	    
		
	}
	

	/**
	 * 사용자 비밀번호 수정.
	 * @param accountParam
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	
	@RequestMapping(value = "/myInfoWrite.do", method = RequestMethod.POST)
	public @ResponseBody ModelAndView writeMyInfo(@ModelAttribute("accountParam") AccountParam accountParam,
        HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("jsonView");
		String messageCode = "";
		AuthUser authUser = (AuthUser)getRequestAttribute("userInfo");
		accountParam.setModifyId(authUser.getUserId());
		accountParam.setUserId(authUser.getUserId());
		accountParam.setCreateId(authUser.getCreateId());
		accountParam.setCreateDt(authUser.getCreateDt());
		accountParam.setModifyDt(authUser.getModifyDt());

		SHAPasswordEncoder shaPasswordEncoder = new SHAPasswordEncoder(256);
		shaPasswordEncoder.setEncodeHashAsBase64(true);
		
		accountParam.setOldUserPassword(shaPasswordEncoder.encode(accountParam.getOldUserPassword()));
		accountParam.setUserPassword(shaPasswordEncoder.encode(accountParam.getUserPassword()));
		
		int result = userAccountManagementService.modifyUserPassword(accountParam);
		if (result == 3) {
			messageCode = "E";
		} else {
			messageCode = "OK";
		}

		mv.addObject("result", result);
		mv.addObject("messageCode", messageCode);
	    return mv;
	}	
	
	/**
	 * 사용자 비밀번호 히스토리 테이블 조회.
	 * @param accountParam
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = "/myInfoHisCheck.do", method = RequestMethod.POST)
	public @ResponseBody ModelAndView myInfoHisCheck(@ModelAttribute("accountParam") AccountParam accountParam,
        HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("jsonView");
		String messageCode = "";
		AuthUser authUser = (AuthUser)getRequestAttribute("userInfo");
		accountParam.setUserId(authUser.getUserId());
		SHAPasswordEncoder shaPasswordEncoder = new SHAPasswordEncoder(256);
		shaPasswordEncoder.setEncodeHashAsBase64(true);
		
		accountParam.setUserPassword(shaPasswordEncoder.encode(accountParam.getUserPassword()));
		
		int result = userAccountManagementService.selectUserPwdHis(accountParam);
		if (result == -1) {
			messageCode = "E";
		} else {
			messageCode = "OK";
		}

		mv.addObject("result", result);
		mv.addObject("messageCode", messageCode);
	    return mv;
	}	
}
