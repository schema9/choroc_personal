package com.choroc.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.choroc.admin.component.CustomUserDetails;
import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.domain.LoginParam;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.service.UserAccountManagementService;
import com.choroc.admin.util.SHAPasswordEncoder;
import com.choroc.admin.util.ZValue;
import com.choroc.admin.web.AbstractController;

/**
 * TODO  > 로그인 체크 컨트롤러 
 * 파일명   > LoginCheckController.java
 * 작성자   > CCmediaService Corp.
 * 작성일   > 2018. 11. 9.
 */

@Controller
@RequestMapping(value = "/loginCheck")
public class LoginCheckController extends AbstractController {

	/** The UserAccountManagement service. */
	@Autowired
	private UserAccountManagementService userAccountManagementService;


	/**
	 * 사용자가 가입 후 또는 비밀번호 변경 후 최초 로그인일 경우 비밀번호를 변경한다.
	 * @param param
	 * @param accountParam
	 * @param modelMap
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/firstLoginProcess.do", method = {RequestMethod.POST,RequestMethod.GET})
	public String firstLoginProcess(LoginParam param, @ModelAttribute("accountParam") AccountParam accountParam, ModelMap modelMap,
									HttpServletRequest request, @RequestParam("userId") String userId, @RequestParam("userPassword") String userPassword) throws Exception {
				
		HttpSession session = request.getSession();

		// 비밀번호 암호화...
		SHAPasswordEncoder shaPasswordEncoder = new SHAPasswordEncoder(256);
		shaPasswordEncoder.setEncodeHashAsBase64(true);
		userPassword = shaPasswordEncoder.encode(userPassword);

		String systemMsg = "비밀번호가 정상적으로 변경되지 않았습니다. 확인 후 다시 시도해 주시기 바랍니다..";
		
		try {
			// 비밀번호 재등록...
			ZValue zValue = new ZValue();
			zValue.put("userId", userId);
			zValue.put("userPassword", userPassword);
						
			accountParam.setUserPassword(userPassword);
			accountParam.setUserId(userId);
			
			// 기존에 입력된 비밀번호 조회후 동일 비밀번호는 사용금지
			int result = userAccountManagementService.selectUserPwdHis(accountParam);
			if (result > 0) {
				String usedPwMsg = "해당 비밀번호는 기존에 이미 사용했던 비밀번호입니다. 신규 비밀번호를 입력해 주시기 바랍니다.";
				modelMap.addAttribute("usedPwMsg", usedPwMsg);
			} else {
				// 비밀번호 변경후 로그인 정보 업데이트
			    userAccountManagementService.modifyFirstLoginPassword(zValue);
			    session.removeAttribute("tempInfo");
				session.removeAttribute("userInfo");
				session.removeAttribute("userLoginInfo");
				session.removeAttribute("login");
				session.invalidate();                // 세션 초기화 

				String firstLoginMsg = "비밀번호가 변경되었습니다. 다시 로그인 해주세요";
				modelMap.addAttribute("firstLoginMsg", firstLoginMsg);
			}
		}catch (Exception e){

			modelMap.addAttribute("systemMsg", systemMsg);
		}
		
		return  "/login/loginForm";
	}


	/**
	 * 비밀번호 변경 기간이 로그인한 일로 3개월 이상 일때 처리 로직 팝업으로 공지후 연장 처리
	 * @param param
	 * @param accountParam
	 * @param redirectAttr
	 * @param result
	 * @param status
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/passwordPeriodProcess", method = {RequestMethod.POST,RequestMethod.GET})
	public String passwordPeriodProcess(LoginParam param, @ModelAttribute("accountParam") AccountParam accountParam, RedirectAttributes redirectAttr,
										BindingResult result, SessionStatus status, ModelMap modelMap,
										HttpServletRequest request , HttpServletResponse response) throws Exception {

		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();

		param.setLoginUserId(userDetails.getUsername());
		param.setLoginUserPassword(userDetails.getPassword());

		AuthUser authUser = (AuthUser)getRequestAttribute("userInfo");
		accountParam.setUseYn("Y");
		accountParam.setModifyId(authUser.getUserId());

		return "redirect:/login/login.do";
	}
}
