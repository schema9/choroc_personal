package com.choroc.admin.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.choroc.admin.domain.SystemManagerParam;
import com.choroc.admin.dto.SystemManagerInfo;
import com.choroc.admin.service.SystemManagerService;
import com.choroc.admin.web.AbstractController;

/**
 * TODO > 시스템 관리 > 메타 관리
 * 파일명   > SystemManagerController.java
 * 작성자   > CCmedia Service Corp.
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/administrator/systemManager")
@PropertySource("classpath:application-context.properties")
public class SystemManagerController extends AbstractController{
	
	 /**
     * The UserAccountManagement service.
     */
    @Autowired
    private SystemManagerService systemManagerService;
    
    @Autowired
    private Environment env;
   	
    /* 시스템 관리 > 메타 관리 init-search */
    @RequestMapping(value = "/systemMetaList.do")
    public @ResponseBody ModelAndView systemMetaList(@ModelAttribute("metaParam") SystemManagerParam metaParam, @RequestParam(value="rKey", defaultValue="defalutValue") String pKey,HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    	ModelAndView mav = new ModelAndView("meta/systemMetaManage");
    	String actionResult = pKey;
  
        // 한 페이지에 게시되는 게시물 건수     
 		if (metaParam.getRecordCountPerPage() == 0) {
 			metaParam.setRecordCountPerPage(Integer.parseInt(env.getProperty("property.pagination.recordcountperpage")));
 		}
        // 페이징 리스트의 사이즈
 		metaParam.setPageSize(Integer.parseInt(env.getProperty("property.pagination.pagesize")));

        String currentPageNo = request.getParameter("currentPageNo");
        if (currentPageNo != null) {
        	metaParam.setCurrentPageNo(Integer.parseInt(currentPageNo));
        }
        mav.addObject("firstIndex", metaParam.getFirstRecordIndex());
		mav.addObject("recordCountPerPage", metaParam.getRecordCountPerPage());
        int totalCount = systemManagerService.selectMetaDataTotalCount(metaParam);

        List<SystemManagerInfo> systemMetaList = systemManagerService.selectMetaDataRows(metaParam);
 
        metaParam.setTotalRecordCount(totalCount);
       
        mav.addObject("systemMsg", actionResult);
        mav.addObject("systemMetaParam", metaParam);
        mav.addObject("systemMetaList", systemMetaList);
        return mav;
    }
    
    @ResponseBody
    @RequestMapping(value = "/systemMetaDataDetail.do")
    public ModelAndView systemMetaDataDetail(@ModelAttribute("metaParam") SystemManagerParam metaParam, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    	ModelAndView mav = new ModelAndView("jsonView");
	String messageCode = "";
	List<SystemManagerInfo>  result = systemManagerService.selectMetaDataDetail(metaParam);
	if (result == null) {
		messageCode = "E";
	} else {
		messageCode = "OK";
	}
	mav.addObject("result", result);
	mav.addObject("messageCode", messageCode);
    return mav;
    }
    
    @RequestMapping(value = "/metaDataRegSet.do", method = RequestMethod.POST)
    public @ResponseBody ModelAndView metaDataRegSet(@ModelAttribute("metaParam") SystemManagerParam metaParam, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    ModelAndView mav = new ModelAndView("redirect:/administrator/useraccount/accountList.do");
	int result = 3;
	
	result = systemManagerService.insertMetaDataRows(metaParam);
	if (result == 3) {
		metaParam.setActionResult("정보 등록에 실패했습니다.");
	} else {
		metaParam.setActionResult("정보 등록에 성공했습니다.");
	}
    mav.addObject("systemMsg", metaParam.getActionResult());
    return mav;
    }
    
    @RequestMapping(value = "/metaDataModSet.do", method = RequestMethod.POST)
    public @ResponseBody ModelAndView metaDataModSet(@ModelAttribute("metaParam") SystemManagerParam metaParam, RedirectAttributes redirectAttributes,HttpServletRequest request, HttpServletResponse response, ModelMap model) {
	ModelAndView mav = new ModelAndView("redirect:/administrator/useraccount/accountList.do");
	String actionType = metaParam.getActionType();
	int result = 3;
	if (actionType.equals("M")) {
		result = systemManagerService.updateMetaDataRows(metaParam);
		if (result == 3) {
			metaParam.setActionResult("정보 수정에 실패했습니다.");
		} else {
			metaParam.setActionResult("정보 수정에 성공했습니다.");
		}
	}
	if (actionType.equals("D")) {
		result = systemManagerService.deleteMetaDataRows(metaParam);
		if (result == 3) {
			metaParam.setActionResult("정보 삭제에 실패했습니다.");
		} else {
			metaParam.setActionResult("정보 삭제에 성공했습니다.");
		}
	}
	if (actionType.equals("R")) {
		result = systemManagerService.tableDataEarlyLoading(metaParam);
		if (result == 3) {
			metaParam.setActionResult("초기 적재 메시지 전송에 실패했습니다.");
		} else {
			metaParam.setActionResult("초기 적재 메시지 전송에 성공했습니다.");
		}
	}
    mav.addObject("systemMsg", metaParam.getActionResult());
	return mav;
    }
    
    @RequestMapping(value = "/metaDataDelSet.do", method = RequestMethod.POST)
    public @ResponseBody ModelAndView metaDataDelSet(@ModelAttribute("metaParam") SystemManagerParam metaParam, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    ModelAndView mav = new ModelAndView("redirect:/administrator/useraccount/accountList.do");
	int result = 3;
	
	result = systemManagerService.deleteMetaDataRows(metaParam);
	if (result == 3) {
		metaParam.setActionResult("정보 삭제에 실패했습니다.");
	} else {
		metaParam.setActionResult("정보 삭제에 성공했습니다.");
	}

    mav.addObject("systemMsg", metaParam.getActionResult());
    return mav;
    }
    
    @CrossOrigin
    @ResponseBody
    @RequestMapping(value="/apiTest.do", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView apiTest(@ModelAttribute("param") SystemManagerParam param, HttpServletRequest request, HttpServletResponse response, Map<String,Object> commandMap) throws Exception{
		ModelAndView mv = new ModelAndView("test/apiTest");
	    List<Map<String,Object>> list = null;

	    mv.addObject("returnErrMsg","");
		mv.addObject("list",list);
		mv.addObject("apiNm","");
		return mv;
    }
}