package com.choroc.admin.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.domain.RecommendAreaParam;
import com.choroc.admin.dto.Algorithm;
import com.choroc.admin.dto.RecommendAreaInfo;
import com.choroc.admin.service.RecommendAreaService;
import com.choroc.admin.web.AbstractController;

/**
 * TODO  > 추천 환경관리 > 추천 영역
 * 파일명   > AreaController.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/environment/area")
public class AreaController extends AbstractController {

	private static final Logger logger = LoggerFactory.getLogger(AreaController.class);

	@Autowired
	private RecommendAreaService recommendAreaService;

	/**
	 * Inits the binder.
	 *
	 * @param binder the binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.KOREA);
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	/* 추천 환경관리 > 추천 영역 init */
	@RequestMapping(value = "/recommendArea")
	public ModelAndView getRecommendPageArea(@ModelAttribute("recommendAreaParam") RecommendAreaParam param,
			HttpServletRequest request, HttpServletResponse response, ModelMap model) throws SQLException {

		ModelAndView mv = new ModelAndView("environment/area/recommendArea");
		return mv;
	}

	
	
	
	
	
	
	
	
	// 등록팝업 알고리즘 리스트(select 태그)
	@ResponseBody
	@RequestMapping(value = "/selectAlgorithmList", method = RequestMethod.POST)
	public ModelAndView selectAlgorithmList(HttpServletRequest request, HttpServletResponse response)
			throws SQLException {

		ModelAndView mv = new ModelAndView("environment/area/recommendArea");
		List<Algorithm> result = null;
		result = recommendAreaService.getAlgorithmList();

		mv.addObject("result", result);
		mv.setViewName("jsonView");

		return mv;
	}

	// 중복체크
	@ResponseBody
	@RequestMapping(value = "/checkRecommnedArea", method = RequestMethod.POST)
	public ModelAndView checkRecommnedArea(@RequestBody RecommendAreaInfo dto, HttpServletRequest request,
			HttpServletResponse response) throws SQLException {

		ModelAndView mv = new ModelAndView("jsonView");
		RecommendAreaParam param = new RecommendAreaParam();
		param.setPge_area_id(dto.getPge_area_id());

		String result = recommendAreaService.checkRecommendArea(param);
		logger.info("결과 메시지: " + result);
		mv.addObject("result", result);

		return mv;
	}

	// 등록팝업 - 저장
	@RequestMapping(value = "/createRecommendArea", method = RequestMethod.POST)
	public ModelAndView createRecommendArea(HttpServletRequest request, HttpServletResponse response)
			throws SQLException {

		ModelAndView mv = new ModelAndView();
		RecommendAreaParam param = new RecommendAreaParam();
		int result = recommendAreaService.insertRecommendArea(param, request);

		logger.info("결과 : " + result);
		mv.addObject("result", result);
		mv.setViewName("jsonView");

		return mv;
	}

	// 수정팝업 데이터 가져오기
	@ResponseBody
	@RequestMapping(value = "/selectOneRecommendArea.do", method = RequestMethod.POST)
	public ModelAndView selectOneRecommendException(@ModelAttribute RecommendAreaInfo info, HttpServletRequest request,
			HttpServletResponse response) throws SQLException {

		ModelAndView mv = new ModelAndView("environment/area/recommendArea");
		RecommendAreaParam param = new RecommendAreaParam();
		param.setPge_area_id(info.getPge_area_id());

		RecommendAreaInfo result = recommendAreaService.selectOneRecommendArea(param);
		List<Algorithm> resultList = null;
		resultList = recommendAreaService.getAlgorithmList();

		logger.info("결과 : " + result);
		mv.addObject("result", result);
		mv.addObject("resultList", resultList);

		mv.setViewName("jsonView");
		return mv;
	}

	// 수정팝업 - 저장
	@RequestMapping(value = "/updateRecommendArea", method = RequestMethod.POST)
	public ModelAndView updateRecommendArea(HttpServletRequest request, HttpServletResponse response)
			throws SQLException {

		ModelAndView mv = new ModelAndView();
		RecommendAreaParam param = new RecommendAreaParam();
		int result = recommendAreaService.updateRecommendArea(param, request);

		logger.info("결과 : " + result);
		mv.addObject("result", result);

		mv.setViewName("jsonView");
		return mv;
	}
}
