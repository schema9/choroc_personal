package com.choroc.admin.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.domain.RecommendExceptionParam;
import com.choroc.admin.dto.RecommendException;
import com.choroc.admin.service.RecommendExceptionService;
import com.choroc.admin.web.AbstractController;

/**
 * TODO  > 추천 환경관리 > 추천 제외 관리
 * 파일명   > ExceptionManagementController.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/environment/exception")
@PropertySource("classpath:application-context.properties")
public class ExceptionManagementController extends AbstractController {

	@Autowired
	private Environment env;
   	
	@Autowired
	private RecommendExceptionService exceptionService;

	private static final Logger logger = LoggerFactory.getLogger(ExceptionManagementController.class);

	/* 추천 환경관리 > 추천 제외 관리 init */
	@ResponseBody
	@RequestMapping(value = "/RecommendExceptionList.do")
	public ModelAndView selectRecommendExceptionList(
			@ModelAttribute RecommendExceptionParam recommendExceptionParam,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {

		ModelAndView mv = new ModelAndView("/environment/exception/recommendException");

		// 한 페이지에 게시되는 게시물 건수
		if (recommendExceptionParam.getRecordCountPerPage() == 0) {
			recommendExceptionParam.setRecordCountPerPage(Integer.parseInt(env.getProperty("property.pagination.recordcountperpage")));
		}
		// 페이징 리스트의 사이즈
		recommendExceptionParam.setPageSize(Integer.parseInt(env.getProperty("property.pagination.pagesize")));

		String currentPageNo = request.getParameter("currentPageNo");
		if (currentPageNo != null) {
			recommendExceptionParam.setCurrentPageNo(Integer.parseInt(currentPageNo));
		}

		mv.addObject("firstIndex", recommendExceptionParam.getFirstRecordIndex());
		mv.addObject("recordCountPerPage", recommendExceptionParam.getRecordCountPerPage());

/*		if (null == recommendExceptionParam.getSortation()) {
			// 첫페이지 접속
			recommendExceptionParam.setSortation("P");
		} else {
			// 그이후...
			recommendExceptionParam.setSortation(recommendExceptionParam.getSortation());
		}*/

		return mv;
	}
	
	

	// 추천제외관리 중복체크
	@ResponseBody
	@RequestMapping(value = "/checkRecommnedExcpetion", method = RequestMethod.POST)
	public ModelAndView checkRecommendException(@RequestBody RecommendException recommendException,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {

		ModelAndView mv = new ModelAndView("jsonView");
		RecommendExceptionParam param = new RecommendExceptionParam();
		param.setContent_cd(recommendException.getContent_cd());
		param.setSortation(recommendException.getSortation());

		String result = exceptionService.checkException(param);
		logger.info("결과 메시지: " + result);
		mv.addObject("result", result);

		return mv;
	}

	// 추천제외관리 등록
	@ResponseBody
	@RequestMapping(value = "/createRecommendException.do", method = RequestMethod.POST)
	public ModelAndView createRecommendException(@ModelAttribute RecommendException param, HttpServletRequest request,
			HttpServletResponse response) throws SQLException {

		ModelAndView mv = new ModelAndView();
		int result = exceptionService.insertException(param);

		logger.info("결과 : " + result);
		mv.addObject("result", result);

		mv.setViewName("jsonView");
		return mv;
	}

	// 추천제외관리 삭제
	@ResponseBody
	@RequestMapping(value = "/deleteRecommendException.do", method = RequestMethod.POST)
	public ModelAndView deleteRecommendException(@ModelAttribute RecommendException param, HttpServletRequest request,
			HttpServletResponse response) throws SQLException {

		ModelAndView mv = new ModelAndView();
		int result = exceptionService.deleteException(param);

		logger.info("결과 : " + result);
		mv.addObject("result", result);

		mv.setViewName("jsonView");
		return mv;
	}

}
