package com.choroc.admin.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.dto.AccountInfo;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.service.UserAccountManagementService;
import com.choroc.admin.util.SHAPasswordEncoder;
import com.choroc.admin.web.AbstractController;


/**
 * TODO > 시스템 관리 > 사용자 관리
 * 파일명   > UserAccountManagementController.java
 * 작성자   > CCmedia Service Corp.
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/administrator/useraccount")
@PropertySource("classpath:application-context.properties")
public class UserAccountManagementController extends AbstractController {

	
    /**
     * The UserAccountManagement service.
     */
    @Autowired
    private UserAccountManagementService userAccountManagementService;

    /**
     * The env.
     */
    @Autowired
    private Environment env;
   	
    /**
     * Initialize the binder.
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.KOREA);
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
    
    /* 시스템 관리 > 사용자 관리 init-search */
    @RequestMapping(value = "/accountList.do")
    public @ResponseBody ModelAndView selectUserAccountRows(@ModelAttribute("accountParam") AccountParam param, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        ModelAndView mav = new ModelAndView("administrator/userAccountManage");
        AuthUser authUser = (AuthUser) getRequestAttribute("userInfo");

		// 한 페이지에 게시되는 게시물 건수
		if (param.getRecordCountPerPage() == 0) {
			param.setRecordCountPerPage(Integer.parseInt(env.getProperty("property.pagination.recordcountperpage")));
		}
		// 페이징 리스트의 사이즈
		param.setPageSize(Integer.parseInt(env.getProperty("property.pagination.pagesize")));

         String currentPageNo = request.getParameter("currentPageNo");
         if (currentPageNo != null) {
        	 param.setCurrentPageNo(Integer.parseInt(currentPageNo));
         }
         mav.addObject("firstIndex", param.getFirstRecordIndex());
         mav.addObject("recordCountPerPage", param.getRecordCountPerPage());

        param.setUserId(authUser.getUserId());
        param.setRoleId(authUser.getRoleId());

        int totalCount = userAccountManagementService.selectUserAccountTotalCount(param);

        List<AccountInfo> accountList
                = userAccountManagementService.selectUserAccountRows(param);
        List<AccountInfo>  selectAuthCdList = userAccountManagementService.selectAuthCd(param);
        
        param.setTotalRecordCount(totalCount);
        mav.addObject("accountParam", param);
        mav.addObject("accountAuthCdList", selectAuthCdList);
        mav.addObject("accountList", accountList);
        return mav;
    }
    
    /**
     * count user ID
     * @param userId
     * @return
     */
    
    @RequestMapping(value = "/isDuplicateUserId.do", method = RequestMethod.POST)
   	public @ResponseBody ModelAndView selectAllMenu(@ModelAttribute("accountParam") AccountParam param, HttpServletRequest request, HttpServletResponse response){
       	ModelAndView mv = new ModelAndView("jsonView");
       	String messageCode = "";
       	int result = userAccountManagementService.selectAccountUserIdCountForValidKey(param);
    	if (result == 3) {
    		messageCode = "E";
    	} else {
    		messageCode = "OK";
    	}
    	mv.addObject("result", result);
    	mv.addObject("messageCode", messageCode);
   		return mv;
   	}
    
    @RequestMapping(value = "/isGetAuthCd.do")
	public @ResponseBody ModelAndView selectAuthCd(@ModelAttribute("accountParam") AccountParam param, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
	ModelAndView mv = new ModelAndView("jsonView");
	String messageCode = "";
	List<AccountInfo>  result = userAccountManagementService.selectAuthCd(param);

	if (result == null) {
		messageCode = "E";
	} else {
		messageCode = "OK";
	}
		mv.addObject("result", result);
		mv.addObject("messageCode", messageCode);
       return mv;
    }
    
    //시스템관리 > 사용자관리 > 사용자 추가
    @RequestMapping(value = "/createAccountUser.do", method = RequestMethod.POST)
   	public @ResponseBody ModelAndView createAccountUser(@ModelAttribute("accountParam") AccountParam param, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
   	ModelAndView mv = new ModelAndView("jsonView");
    AuthUser authUser = (AuthUser) getRequestAttribute("userInfo");
    SHAPasswordEncoder shaPasswordEncoder = new SHAPasswordEncoder(256);
	shaPasswordEncoder.setEncodeHashAsBase64(true);
    param.setCreateId(authUser.getUserId());
    param.setUserPassword(shaPasswordEncoder.encode(param.getUserId()));
	String messageCode = "";
   	int result = userAccountManagementService.createAccountUser(param);
   	if (result == 3) {
		messageCode = "E";
	} else {
		messageCode = "OK";
	}
   	mv.addObject("result", result);
	mv.addObject("messageCode", messageCode);
    return mv;
    }
    @RequestMapping(value = "/userPwdReset.do", method = RequestMethod.POST)
   	public @ResponseBody ModelAndView userPwdReset(@ModelAttribute("accountParam") AccountParam param, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
   	ModelAndView mv = new ModelAndView("jsonView");
   	AuthUser authUser = (AuthUser) getRequestAttribute("userInfo");
   	SHAPasswordEncoder shaPasswordEncoder = new SHAPasswordEncoder(256);
	shaPasswordEncoder.setEncodeHashAsBase64(true);
    param.setModifyId(authUser.getUserId()); 
    param.setUserPassword(shaPasswordEncoder.encode(param.getUserId()));
   	String messageCode = "";
   	int result = userAccountManagementService.userPwdReset(param);
   	if (result == 3) {
		messageCode = "E";
	} else {
		messageCode = "OK";
	}
   	mv.addObject("result", result);
	mv.addObject("messageCode", messageCode);
    return mv;
    }
    
    @RequestMapping(value = "/userModifySet.do", method = RequestMethod.POST)
   	public @ResponseBody ModelAndView userModifySet(@ModelAttribute("accountParam") AccountParam param, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
   	ModelAndView mv = new ModelAndView("jsonView");
   	AuthUser authUser = (AuthUser) getRequestAttribute("userInfo");
    param.setModifyId(authUser.getUserId());  
   	String messageCode = "";
   	int result = userAccountManagementService.userModifySet(param);
   	if (result == 3) {
		messageCode = "E";
	} else {
		messageCode = "OK";
	}
   	mv.addObject("result", result);
	mv.addObject("messageCode", messageCode);
    return mv;
    }
}
