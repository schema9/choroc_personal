package com.choroc.admin.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.domain.CommonCodeParam;
import com.choroc.admin.domain.SystemAlgorithmParam;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.dto.CommonCodeInfo;
import com.choroc.admin.dto.SystemAlgorithmInfo;
import com.choroc.admin.service.CommonCodeManagementService;
import com.choroc.admin.service.SystemAlgorithmService;
import com.choroc.admin.util.StringUtil;
import com.choroc.admin.web.AbstractController;

/**
 * TODO  > 추천 환경 관리 > 추천 위젯 설정
 * 파일명   > SystemAlgorithmController.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 8.
 */

@Controller
@RequestMapping(value = "/system/algorithm")
//@PropertySource("classpath:application-context.properties")
public class SystemAlgorithmController extends AbstractController {

	/** The SystemAlgorithmService. */
	@Autowired
	private SystemAlgorithmService systemAlgorithmService;

	/** The CommonCodeManagementService. */
	@Autowired
	private CommonCodeManagementService commonCodeManagementService;

	/** The env. */
	@Autowired
	private Environment env;

	/**
	 * Inits the binder.
	 *
	 * @param binder the binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.KOREA);
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	/**
	 * 알고리즘 Type 목록. (rows)
	 * 
	 * @param commonCodeInfo
	 * @param param
	 * @param request
	 * @param response
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/recommendAlgoTypeList")
	public ModelAndView selectRecommendAlgoTypeRows(@ModelAttribute("recommendAlgoType") CommonCodeInfo commonCodeInfo,
			@ModelAttribute("recommendAlgoTypeParam") CommonCodeParam param, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		ModelAndView mav = new ModelAndView("system/recommendAlgoType");

		// 한 페이지에 게시되는 게시물 건수
		if (param.getRecordCountPerPage() == 0) {
			param.setRecordCountPerPage(Integer.parseInt(env.getProperty("property.pagination.recordcountperpage")));
		}
		// 페이징 리스트의 사이즈
		param.setPageSize(Integer.parseInt(env.getProperty("property.pagination.pagesize")));

		String currentPageNo = request.getParameter("currentPageNo");
		if (currentPageNo != null) {
			param.setCurrentPageNo(Integer.parseInt(currentPageNo));
		}

		mav.addObject("firstIndex", param.getFirstRecordIndex());
		mav.addObject("recordCountPerPage", param.getRecordCountPerPage());

		// 알고리즘 Type 목록
		param.setGroupCode("ALGORITHM");
		List<CommonCodeInfo> recommendAlgoTypeList = commonCodeManagementService.selectCommonCodeRows(param);
		int totalCount = commonCodeManagementService.selectCommonCodeTotalCount(param);

		param.setTotalRecordCount(totalCount);

		mav.addObject("recommendAlgoTypeParam", param);
		mav.addObject("recommendAlgoTypeList", recommendAlgoTypeList);

		return mav;
	}

	/**
	 * Algorithm Type - A Row
	 * 
	 * @param request
	 * @param response
	 * @return CommonCodeInfo
	 */
	@RequestMapping(value = "/recoAlgoTypeSelectRow", method = RequestMethod.POST)
	public ModelAndView selectRecoAlgoTypeRow(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		CommonCodeInfo result = null;
		Map<String, Object> reqParams = new HashMap<>();

		reqParams.put("code", request.getParameter("code"));
		reqParams.put("groupCode", "ALGORITHM");

		if (StringUtil.isNotEmpty(reqParams.get("code").toString())) {
			result = commonCodeManagementService.selectCommonCodeRow(reqParams);
		}

		mv.addObject("result", result);

		return mv;
	}

	/**
	 * Algorithm Type - Count the Row
	 * 
	 * @param request
	 * @param response
	 * @return Integer
	 */
	@RequestMapping(value = "/recoAlgoTypeCodeCountForValidKey", method = RequestMethod.POST)
	public @ResponseBody ModelAndView selectRecoAlgoTypeCountForValidKey(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = -1;
		Map<String, Object> reqParams = new HashMap<>();

		reqParams.put("code", request.getParameter("code"));
		reqParams.put("groupCode", "ALGORITHM");

		if (StringUtil.isNotEmpty(request.getParameter("code"))) {
			result = commonCodeManagementService.selectCommonCodeCountForValidKey(reqParams);
		}

		mv.addObject("result", result);

		return mv;
	}

	/**
	 * Algorithm Type - Count the Row in use
	 * 
	 * @param request
	 * @param response
	 * @return Integer
	 */
	@RequestMapping(value = "/recoAlgoTypeCodeCountForUsing", method = RequestMethod.POST)
	public @ResponseBody ModelAndView selectRecoAlgoTypeCountForUsing(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = -1;

		String code = request.getParameter("code");

		if (StringUtil.isNotEmpty(request.getParameter("code"))) {
			result = commonCodeManagementService.selectCommonCodeCountForUsing(code);
		}

		mv.addObject("result", result);

		return mv;
	}

	/**
	 * Algorithm Type - Add A Row
	 * 
	 * @param commonCodeInfo
	 * @param request
	 * @param response
	 * @return integer 0 : duplicated code integer 1 : success
	 */
	@RequestMapping(value = "/recoAlgoTypeAddRow", method = RequestMethod.POST)
	public @ResponseBody ModelAndView insertRecoAlgoTypeRow(
			@ModelAttribute("commonCodeInfo") CommonCodeInfo commonCodeInfo, HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = 0;
		int duplicateCode = -1;
		AuthUser authUser = (AuthUser) getRequestAttribute("userInfo");

		if (StringUtil.isNotEmpty(commonCodeInfo.getCode()) && StringUtil.isNotEmpty(authUser.getUserId())) {

			Map<String, Object> reqParams = new HashMap<>();

			reqParams.put("code", commonCodeInfo.getCode());
			reqParams.put("groupCode", "ALGORITHM");

			if (StringUtil.isNotEmpty(request.getParameter("code"))) {
				duplicateCode = commonCodeManagementService.selectCommonCodeCountForValidKey(reqParams);
			}
			if (duplicateCode == 0) {
				commonCodeInfo.setCreateId(authUser.getUserId());
				commonCodeInfo.setGroupCode("ALGORITHM");
				result = commonCodeManagementService.insertCommonCodeRow(commonCodeInfo);
			}
		}

		mv.addObject("result", result);

		return mv;
	}

	/**
	 * Algorithm Type - Modify Row [ update & delete ]
	 * 
	 * @param commonCodeInfo
	 * @param request
	 * @param response
	 * @return Integer
	 */
	@RequestMapping(value = "/recoAlgoTypeModifyRow", method = RequestMethod.POST)
	public @ResponseBody ModelAndView updateRecoAlgoTypeRow(
			@ModelAttribute("commonCodeInfo") CommonCodeInfo commonCodeInfo, HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = 0;
		int onUseCodeCount = 0;
		AuthUser authUser = (AuthUser) getRequestAttribute("userInfo");
		commonCodeInfo.setModifyId(authUser.getUserId());
		commonCodeInfo.setGroupCode("ALGORITHM");

		result = commonCodeManagementService.recoAlgoTypeModifyRow(commonCodeInfo);

		mv.addObject("result", result);

		return mv;
	}

	/**
	 * the List of Common code by Group code
	 * 
	 * @param request
	 * @param response
	 * @return List<CommonCodeInfo>
	 */
	@RequestMapping(value = "recoAlgoTypeCmmCodeList", method = RequestMethod.POST)
	public @ResponseBody ModelAndView recoAlgoTypeCmmCodeList(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		List<CommonCodeInfo> result = new ArrayList<CommonCodeInfo>(Arrays.asList());
		String groupCode = "ALGORITHM";

		if (StringUtil.isNotEmpty(groupCode)) {
			result = commonCodeManagementService.selectCommonGroupRows(groupCode);
		}

		mv.addObject("result", result);

		return mv;
	}

	/**
	 * Algorithm Management - Rows
	 * 
	 * @param systemAlgorithmInfo
	 * @param param
	 * @param request
	 * @param response
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/recommendAlgoList")
	public ModelAndView selectRecommendAlgoMgmtRows(
			@ModelAttribute("recommendAlgo") SystemAlgorithmInfo systemAlgorithmInfo,
			@ModelAttribute("recommendAlgoParam") SystemAlgorithmParam param, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		ModelAndView mav = new ModelAndView("system/recommendAlgo");

		// 한 페이지에 게시되는 게시물 건수
		if (param.getRecordCountPerPage() == 0) {
			param.setRecordCountPerPage(Integer.parseInt(env.getProperty("property.pagination.recordcountperpage")));
		}
		// 페이징 리스트의 사이즈
		param.setPageSize(Integer.parseInt(env.getProperty("property.pagination.pagesize")));

		String currentPageNo = request.getParameter("currentPageNo");
		if (currentPageNo != null) {
			param.setCurrentPageNo(Integer.parseInt(currentPageNo));
		}

		mav.addObject("firstIndex", param.getFirstRecordIndex());
		mav.addObject("recordCountPerPage", param.getRecordCountPerPage());

		// 알고리즘 목록
		List<SystemAlgorithmInfo> recommendAlgoList = systemAlgorithmService.selectRecoAlgoMgmtRows(param);
		int totalCount = systemAlgorithmService.selectRecoAlgoMgmtTotalCount(param);

		param.setTotalRecordCount(totalCount);

		mav.addObject("recommendAlgoParam", param);
		mav.addObject("recommendAlgoList", recommendAlgoList);

		return mav;
	}

	/**
	 * Algorithm Management - A Row
	 * 
	 * @param request
	 * @param response
	 * @return SystemAlgorithm
	 */
	@RequestMapping(value = "/recoAlgoMgmtSelectRow", method = RequestMethod.POST)
	public ModelAndView selectRecoAlgoMgmtRow(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		SystemAlgorithmInfo result = null;
		String algoId = request.getParameter("algoId");

		if (StringUtil.isNotEmpty(algoId)) {
			result = systemAlgorithmService.selectRecoAlgoMgmtRow(algoId);
		}

		mv.addObject("result", result);

		return mv;
	}

	/**
	 * Algorithm Management - Count the Row
	 * 
	 * @param request
	 * @param response
	 * @return Integer
	 */
	@RequestMapping(value = "/recoAlgoMgmtAlgoIdCountForValidKey", method = RequestMethod.POST)
	public @ResponseBody ModelAndView selectRecoAlgoMgmtCountForValidKey(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = -1;
		String algoId = request.getParameter("algoId");

		if (StringUtil.isNotEmpty(algoId)) {
			result = systemAlgorithmService.selectRecoAlgoMgmtCountForValidKey(algoId);
		}

		mv.addObject("result", result);

		return mv;
	}

	/**
	 * Algorithm Management - Add A Row
	 * 
	 * @param systemAlgorithmInfo
	 * @param request
	 * @param response
	 * @return integer 0 : duplicated code integer 1 : success
	 */
	@RequestMapping(value = "/recoAlgoMgmtAddRow", method = RequestMethod.POST)
	public @ResponseBody ModelAndView insertRecoAlgoMgmtRow(
			@ModelAttribute("systemAlgorithm") SystemAlgorithmInfo systemAlgorithmInfo, HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = 0;
		int duplicateCode = -1;
		AuthUser authUser = (AuthUser) getRequestAttribute("userInfo");

		if (StringUtil.isNotEmpty(systemAlgorithmInfo.getAlgoId()) && StringUtil.isNotEmpty(authUser.getUserId())) {
			duplicateCode = systemAlgorithmService.selectRecoAlgoMgmtCountForValidKey(systemAlgorithmInfo.getAlgoId());
		}
		if (duplicateCode == 0) {
			systemAlgorithmInfo.setCreateId(authUser.getUserId());
			result = systemAlgorithmService.insertRecoAlgoMgmtRow(systemAlgorithmInfo);
		}

		mv.addObject("result", result);

		return mv;
	}

	/**
	 * Algorithm Management - Modify Row [ update & delete ]
	 * 
	 * @param systemAlgorithmInfo
	 * @param request
	 * @param response
	 * @return Integer
	 */
	@RequestMapping(value = "/recoAlgoMgmtModifyRow", method = RequestMethod.POST)
	public @ResponseBody ModelAndView updateRecoAlgoMgmtRow(
			@ModelAttribute("systemAlgorithm") SystemAlgorithmInfo systemAlgorithmInfo, HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = 0;
		boolean isDeleteMode = systemAlgorithmInfo.getControlMode().toUpperCase().equals("D") ? true : false;
		AuthUser authUser = (AuthUser) getRequestAttribute("userInfo");
		systemAlgorithmInfo.setModifyId(authUser.getUserId());

		if (StringUtil.isNotEmpty(systemAlgorithmInfo.getAlgoId()) && StringUtil.isNotEmpty(authUser.getUserId())) {
			if (isDeleteMode) {
				result = systemAlgorithmService.deleteRecoAlgoMgmtRow(systemAlgorithmInfo);
			} else {
				result = systemAlgorithmService.updateRecoAlgoMgmtRow(systemAlgorithmInfo);
			}
		}

		mv.addObject("result", result);

		return mv;
	}

}
