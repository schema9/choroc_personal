package com.choroc.admin.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.api.domain.StatisticsParam;
import com.choroc.api.dto.ApiInfo;
import com.choroc.api.dto.RcEoadTagHhChannelStat;
import com.choroc.api.dto.RcEoadTagHhStat;
import com.choroc.api.service.RecommendationStatService;
import com.choroc.api.util.Validator;

/**
 * TODO > 추천 성과 분석 > 추천 수집분석
 * 파일명   > RecommendResultAnalysisController.java
 * 작성자   > CCmedia Service Corp.
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/collection/analysis/")
public class RecommendResultAnalysisController {
	 
	/** service. */
	@Autowired
	private RecommendationStatService recommendationStatService;
	
	/* 추천 성과분석 > 추천 수집분석 > 일별 시간대별 init */
	@RequestMapping(value="days/recommend")
	public ModelAndView selectRecommendRows1(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("recommend/rcTagHhStat");	    
		return mav;
	}
	
	/* 추천 성과분석 > 추천 수집분석 > 일별 시간대별 항목별 init */
	@RequestMapping(value="category/recommend")
	public ModelAndView selectRecommendRows2(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("recommend/rcTagHhChannelStat");
		return mav;
	}
	
	
	
	//추천 수집(추천성과분석)  - 추천성과 관련 api
    @RequestMapping(value="recommend/daysCount/{api}", method= {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView selectRecommendDaysCount(@ModelAttribute("param") ApiInfo apiParam, @PathVariable String api,HttpServletRequest request, HttpServletResponse response) throws Exception {	
		ModelAndView mv = new ModelAndView("jsonView");
		StatisticsParam param = new StatisticsParam();
        param.setApi(api);
        
        String resErrMsg;
        Map<String,Object> list = null;
        try {
            // param set
            resErrMsg = Validator.setParam(request, param);

            // param check
            if(StringUtils.isEmpty(resErrMsg)) {
                resErrMsg = Validator.paramValidator(request, param);
            }

            // resErrMsg가 null 이 아닌경우 400 error 리턴
            if(!StringUtils.isEmpty(resErrMsg)) {
                //response.sendError(400, "Error : invalid parameter [ "+resErrMsg+" ]");
            	mv.addObject("returnErrMsg",resErrMsg+" 에러");
            } else {
            	apiParam.setFirstIndexInt(param.getFirstIndexInt());
            	apiParam.setLastIndexInt(param.getLastIndexInt());
                list = recommendationStatService.recommend(apiParam);
        		mv.addObject("returnErrMsg","");
            }
           
        } catch(Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
		mv.addObject("list",list);
		mv.addObject("apiNm",param.getApi());
		
		return mv;
	}
    
    /* 추천 성과분석 > 추천 수집 분석 > 일별 시간대별 차트 */
    @RequestMapping(value="/recommand/analysis/{api}", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView selectRcTagChart2(@ModelAttribute("param") ApiInfo apiParam, @PathVariable String api,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

        	ModelAndView mv = new ModelAndView("jsonView");
        	Map<String,Object> resultMap = new HashMap<String, Object>();        	
        	List<RcEoadTagHhStat> list = new ArrayList<RcEoadTagHhStat>();
        	
        	StatisticsParam param = new StatisticsParam();
            param.setApi(api);
            
            String resErrMsg;
    		try {
    			// param set
    			resErrMsg = Validator.setParam(request, param);

    			// param check
    			if (StringUtils.isEmpty(resErrMsg)) {
    				resErrMsg = Validator.paramValidator(request, param);
    			}
			
				resultMap = recommendationStatService.recommend(apiParam);
				list = (List<RcEoadTagHhStat>) resultMap.get("list");
				mv.addObject(list);
				
				mv.addObject("returnErrMsg", "");
    			
    		} catch (Exception e) {
    			e.printStackTrace();
    			throw new Exception(e.getMessage());
    		}
        	
        	mv.addObject("resultList", list);
    	
    	return mv;
    }    
	
    @RequestMapping(value="/recommend/daysList/{api}", method= {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView selectListRcEoadTagHhStat(@ModelAttribute("param") ApiInfo apiParam, @PathVariable String api,
  			HttpServletRequest request, HttpServletResponse response) throws Exception {
    
    	ModelAndView mv = new ModelAndView("jsonView"); 
    	Map<String,Object> resultMap = new HashMap<String, Object>();
    	List<RcEoadTagHhStat> list = new ArrayList<RcEoadTagHhStat>();
    	StatisticsParam param = new StatisticsParam();  
    	
        apiParam.setApi(api);
        apiParam.setDateType(apiParam.getDateType());
        
  		if(apiParam.getDateType().equals("A")) { 
//  			apiParam.setFirstYmd(apiParam.getDateFormat(apiParam.getFirstYmd()));
//  			apiParam.setLastYmd(apiParam.getDateFormat(apiParam.getLastYmd()));
  		} else {
  				apiParam.setDate(apiParam.getFirstYmd());
//  			apiParam.setDate(apiParam.getDateFormat(apiParam.getFirstYmd()));
  		}

        apiParam.setDsplyCnt(apiParam.getDsplyCnt());
        apiParam.setDsplyNo(apiParam.getDsplyNo());
        
        String resErrMsg;
        try {
            resErrMsg = Validator.setParam(request, param);
            
            if (StringUtils.isEmpty(resErrMsg)) {
                resErrMsg = Validator.paramValidator(request, param);
            }   
            
            resultMap = recommendationStatService.recommend(apiParam);
            list = (List<RcEoadTagHhStat>)resultMap.get("list");
            //mv.addObject("list", resultMap.get("list"));
            mv.addObject(list);
            mv.addObject("returnErrMsg", "");
        } catch (Exception e) {
            e.printStackTrace();
            mv.addObject("returnErrMsg", e.getMessage());
        }
        
        mv.addObject("list", list);
    	return mv;
    }
    
    @RequestMapping(value="/recommend/cateCount/{api}", method= {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView selectRecommendCateCount(@ModelAttribute("param") ApiInfo apiParam, @PathVariable String api,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
  	
  	ModelAndView mv = new ModelAndView("jsonView");
  	Map<String,Object> resultMap = new HashMap<String, Object>();
  	StatisticsParam param = new StatisticsParam();
  	
  	apiParam.setApi(api);
  	apiParam.setDateType(apiParam.getDateType());

		if(apiParam.getDateType().equals("A")) { 	
//			apiParam.setFirstYmd(apiParam.getDateFormat(apiParam.getFirstYmd()));
//			apiParam.setLastYmd(apiParam.getDateFormat(apiParam.getLastYmd()));
		} else {
			apiParam.setDate(apiParam.getFirstYmd());
//			apiParam.setDate(apiParam.getDateFormat(apiParam.getFirstYmd()));
		}
	  	
	  	String resErrMsg;
	  	try {
	        resErrMsg = Validator.setParam(request, param);
	        
	        if (StringUtils.isEmpty(resErrMsg)) {
	            resErrMsg = Validator.paramValidator(request, param);
	        } 	
	        resultMap = recommendationStatService.recommend(apiParam);
	        
	        mv.addObject("listCount", resultMap.get("listCount"));
	        mv.addObject("returnErrMsg", "");
	  	} catch (Exception e) {
	  		e.printStackTrace();
	  		mv.addObject("returnErrMsg", e.getMessage());
		}     	           	
	  	return mv;
    }
    
    @RequestMapping(value="/recommend/cateList/{api}", method= {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView selectListRcEoadTagHhChannelStat(@ModelAttribute("param") ApiInfo apiParam, @PathVariable String api,
  			HttpServletRequest request, HttpServletResponse response) throws Exception {
    
    	ModelAndView mv = new ModelAndView("jsonView"); 
    	Map<String,Object> resultMap = new HashMap<String, Object>();
    	List<RcEoadTagHhChannelStat> list = new ArrayList<RcEoadTagHhChannelStat>();
    	StatisticsParam param = new StatisticsParam();  
    	
        apiParam.setApi(api);
        apiParam.setDateType(apiParam.getDateType());
        
  		if(apiParam.getDateType().equals("A")) { 
//  			apiParam.setFirstYmd(apiParam.getDateFormat(apiParam.getFirstYmd()));
//  			apiParam.setLastYmd(apiParam.getDateFormat(apiParam.getLastYmd()));
  		} else {
  				apiParam.setDate(apiParam.getFirstYmd());
//  			apiParam.setDate(apiParam.getDateFormat(apiParam.getFirstYmd()));
  		}

        apiParam.setDsplyCnt(apiParam.getDsplyCnt());
        apiParam.setDsplyNo(apiParam.getDsplyNo());
        
        String resErrMsg;
        try {
            resErrMsg = Validator.setParam(request, param);
            
            if (StringUtils.isEmpty(resErrMsg)) {
                resErrMsg = Validator.paramValidator(request, param);
            }   
            apiParam.setFirstIndexInt(param.getFirstIndexInt());
        	apiParam.setLastIndexInt(param.getLastIndexInt());
            resultMap = recommendationStatService.recommend(apiParam);
            list = (List<RcEoadTagHhChannelStat>)resultMap.get("list");
            
            mv.addObject(list);
            mv.addObject("returnErrMsg", "");
        } catch (Exception e) {
            e.printStackTrace();
            mv.addObject("returnErrMsg", e.getMessage());
        }
        
        mv.addObject("list", list);
    	return mv;
    }   
    
	

		
}
