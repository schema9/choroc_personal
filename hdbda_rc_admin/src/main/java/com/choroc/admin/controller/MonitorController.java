package com.choroc.admin.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.domain.MonitoringParam;
import com.choroc.admin.dto.Monitoring;
import com.choroc.admin.dto.Scheduler;
import com.choroc.admin.service.SystemMonitoringService;
import com.choroc.admin.web.AbstractController;

/**
 * TODO > 시스템 관리 > 모니터링
 * 파일명   > MonitorController.java
 * 작성자   > CCmedia Service Corp.
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/system/monitoring")
public class MonitorController extends AbstractController {

	@Autowired
	private Environment env;
	
	@Autowired
	private SystemMonitoringService monitoringService;
	
	private static final Logger logger = LoggerFactory.getLogger(MonitorController.class);
 
	/* 시스템 관리 > 모니터링 init-search */
	@RequestMapping(value="/monitorList")
	public ModelAndView selectScheduleRows(@ModelAttribute("MonitoringParam") MonitoringParam param, 
			HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("system/monitorList");

		// 한 페이지에 게시되는 게시물 건수
		if (param.getRecordCountPerPage() == 0) {
			param.setRecordCountPerPage(Integer.parseInt(env.getProperty("property.pagination.recordcountperpage")));
		}
		// 페이징 리스트의 사이즈
		param.setPageSize(Integer.parseInt(env.getProperty("property.pagination.pagesize")));
		
		String currentPageNo = request.getParameter("currentPageNo");
		if (currentPageNo != null) {
			param.setCurrentPageNo(Integer.parseInt(currentPageNo));
		}

		mv.addObject("firstIndex", param.getFirstRecordIndex());
		mv.addObject("recordCountPerPage", param.getRecordCountPerPage());
		
		
		if(null == param.getStrt_ddtm() && null == param.getEnd_ddtm()) {
			SimpleDateFormat formatFrom = new SimpleDateFormat("yyyyMMdd");
			Date date = new Date();
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -6); 
	
			String StartDate = formatFrom.format(cal.getTime());
			String endDate = formatFrom.format(date);
			
			param.setStrt_ddtm(StartDate);
			param.setEnd_ddtm(endDate);
			
			System.out.println("일주일전: " + StartDate);
			System.out.println("오늘날짜: " + endDate);
			
		} else {
		
			System.out.println("시작일 : " + param.getStrt_ddtm() + "  " + "종료일: " + param.getEnd_ddtm());
		}
				
		try {
			List<Monitoring> result = monitoringService.selectMonitoringList(param);
			int totalCount = monitoringService.selectMonitoringTotalCount(param);
			param.setTotalRecordCount(totalCount);
			
			mv.addObject("resultList", result);
			mv.addObject("totalCount", totalCount);
			mv.addObject("monitoringParam", param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return mv;
	}
	
	@RequestMapping(value="/jobDetail")
	public ModelAndView selectDetailRows(@ModelAttribute("MonitoringParam") MonitoringParam param, HttpServletRequest request) {

		ModelAndView mav = new ModelAndView("system/jobDetail");
		logger.info("selectDetailRows task_id: " + param.getTask_id());		
		mav.addObject("task_id", param.getTask_id());
		
		return mav;
	}
	
	@RequestMapping(value="/schedulerDetailView", method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView SchedulerDetailView(@ModelAttribute("MonitoringParam") MonitoringParam param, HttpServletRequest request) throws UnsupportedEncodingException {
		
		ModelAndView mav = new ModelAndView("system/schedulerDetailView");
		//System.out.println("잡 이름: " + URLDecoder.decode(request.getParameter("job_nm"), "UTF-8"));
		param.setJob_nm(URLDecoder.decode(request.getParameter("job_nm"), "UTF-8"));
		
		try {
			
			Scheduler schd = monitoringService.schedulerDetailView(param);
			mav.addObject("result", schd);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return mav;
	}
	
}
