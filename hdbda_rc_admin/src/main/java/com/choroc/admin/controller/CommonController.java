package com.choroc.admin.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.choroc.admin.domain.AuthUserParam;
import com.choroc.admin.domain.LoginParam;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.service.UserService;
import com.choroc.admin.util.AesUtil;
import com.choroc.admin.util.CookieUtil;
import com.choroc.admin.util.SHAPasswordEncoder;
import com.choroc.admin.util.StringUtil;
import com.choroc.admin.util.ZValue;
import com.choroc.admin.web.AbstractController;
import com.choroc.api.Constants;
import com.choroc.api.domain.StatisticsParam;
import com.choroc.api.dto.ApiInfo;
import com.choroc.api.dto.RcEoadTagHhChannelStat;
import com.choroc.api.dto.RcEoadTagHhStat;
import com.choroc.api.service.RecommendationStatService;
import com.choroc.api.util.Validator;

/**
 * TODO > 로그인 컨트롤러
 * 파일명   > LoginController.java
 * 작성자   > CCmedia Service Corp.
 * 작성일   > 2018. 11. 9.
 */

@Controller
@RequestMapping(value = "/common")
@PropertySource("classpath:application-context.properties")
public class CommonController extends AbstractController {
	
	@Autowired
	private RecommendationStatService recommendationStatService;

	@Autowired
	private Environment env;

	private static final Logger logger = LoggerFactory.getLogger(CommonController.class);
	
	@RequestMapping(value="/excelDownload.do" , method = {RequestMethod.GET, RequestMethod.POST})
	public String excelDownload(@ModelAttribute("param") ApiInfo apiParam, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		
		String api = apiParam.getApi();
		String contentPage = "";
		
		String excelFileName = "";
		excelFileName = apiParam.getExcelFileName();
		String excelTitle = "";		
		excelTitle = apiParam.getExcelTitle();
		
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("api", api);
		map.put("excelFileName", excelFileName);
		map.put("excelTitle", excelTitle);
		
		modelMap.addAttribute("excelFileName", excelFileName);
		modelMap.addAttribute("excelTitle"	, excelTitle);
		
		try {
			Map<String,Object> resultMap = new HashMap<String, Object>();
            
    		switch (api) {
				case Constants.URI_PTN_ANALYSIS_DAYS_COUNT:
					// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 cnt
					break;		
				
				case Constants.URI_PTN_ANALYSIS_DAYS_LIST:
					// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 list		/////////////////////////////////////
					resultMap = recommendationStatService.recommend(apiParam);
		            List<RcEoadTagHhStat> daysList = new ArrayList<RcEoadTagHhStat>();
		            daysList = (List<RcEoadTagHhStat>)resultMap.get("list");
					modelMap.addAttribute("excelList", daysList);
					modelMap.addAttribute("dateType", apiParam.getDateType());
					
					contentPage = "recommend/rcTagHhStatX";
					
					break;			
				
				case Constants.URI_PTN_ANALYSIS_DAYS_CHART:
					// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 chart
					break;			
					
				case Constants.URI_PTN_ANALYSIS_CATE_COUNT:
					// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 항목별 cnt
					break;			
								
				case Constants.URI_PTN_ANALYSIS_CATE_LIST:
					// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 항목별 list		/////////////////////////////////////
					resultMap = recommendationStatService.recommend(apiParam);
		            List<RcEoadTagHhChannelStat> cateList = new ArrayList<RcEoadTagHhChannelStat>();
		            cateList = (List<RcEoadTagHhChannelStat>)resultMap.get("list");
					modelMap.addAttribute("excelList", cateList);
					modelMap.addAttribute("dateType", apiParam.getDateType());
					
					contentPage = "recommend/rcTagHhChannelStatX";
					
					break;			
					
				case Constants.URI_PTN_ANALYSIS_CATE_CHART:
					// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 항목별 chart
					break;
					
				case Constants.URI_PTN_PERFORMANCE_RECO_COLLECTION_AREA_ALL:
					// 추천 성과분석 > 추천 수집분석 > 일별 시간대별 항목별 list		/////////////////////////////////////
					StatisticsParam param = new StatisticsParam();
					String resErrMsg = "";
					
		            // param set
		            resErrMsg = Validator.setParam(request, param);

		            // param check
		            if(StringUtils.isEmpty(resErrMsg)) {
		                resErrMsg = Validator.paramValidator(request, param);
		            }
		            
	            	apiParam.setFirstIndexInt(param.getFirstIndexInt());
	            	apiParam.setLastIndexInt(param.getLastIndexInt());
					
					resultMap = recommendationStatService.performance(apiParam);
					
					List recoCollectionList = (List)resultMap.get("dataSet");
					
					modelMap.addAttribute("excelList", recoCollectionList);
					
					contentPage = "performance/performanceRecoCollListX";
					
					break;
					
					
		        default:
		            try {
						throw new Exception("statistice not found api");
					} catch (Exception e) {
						e.printStackTrace();
					}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	    modelMap.addAttribute("contentPage", contentPage);
		
		String retPage = "common/excelView";
		
		return retPage;
	}
	

}