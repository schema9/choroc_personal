package com.choroc.admin.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.domain.SchedulerParam;
import com.choroc.admin.dto.Scheduler;
import com.choroc.admin.service.SchedulerService;
import com.choroc.admin.web.AbstractController;

/**
 * TODO > 시스템 관리 > 스케줄러 관리
 * 파일명   > SchedulerController.java
 * 작성자   > CCmedia Service Corp.
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/system/job")
@PropertySource("classpath:application-context.properties")
public class SchedulerController extends AbstractController {
	
	@Autowired
	private SchedulerService schedulerService;
	
	@Autowired
	private Environment env;
	 
	/* 시스템 관리 > 스케줄러 관리 init-search */
	@RequestMapping(value="/schedulerList")
	public ModelAndView selectScheduleRows(@ModelAttribute("schedulerParam") SchedulerParam schedulerParam, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mav = new ModelAndView("system/schedulerManage");
		
        // 한 페이지에 게시되는 게시물 건수
        if (schedulerParam.getRecordCountPerPage() == 0) {
        	schedulerParam.setRecordCountPerPage(Integer.parseInt(env.getProperty("property.pagination.recordcountperpage")));
        }
        // 페이징 리스트의 사이즈
        schedulerParam.setPageSize(Integer.parseInt(env.getProperty("property.pagination.pagesize")));
        
        String currentPageNo = request.getParameter("currentPageNo");
        if (currentPageNo != null) {
        	schedulerParam.setCurrentPageNo(Integer.parseInt(currentPageNo));
        }
               
        mav.addObject("firstIndex", schedulerParam.getFirstRecordIndex());
        mav.addObject("recordCountPerPage", schedulerParam.getRecordCountPerPage());	

        //스케줄러 목록
        List<Scheduler> schedulerList = schedulerService.selectListRows(schedulerParam);
        //스케줄러 갯수
        int totalCount = schedulerService.selectListTotalCount(schedulerParam);
        schedulerParam.setTotalRecordCount(totalCount);
        
        mav.addObject("schedulerList", schedulerList);
        mav.addObject("schedulerParam", schedulerParam);
        mav.addObject("totalCnt", totalCount);
        
		return mav;
	}
	
	@RequestMapping(value = "/selectAgentList", method = RequestMethod.POST)
	public ModelAndView selectAgentList() {

		ModelAndView mav = new ModelAndView("jsonView");
		
		List<Scheduler> agentList =null;
		agentList = schedulerService.selectAgentList();

		mav.addObject("agentList", agentList);

		return mav;
	}
	
	@RequestMapping(value = "/schedulerInsert", method = RequestMethod.POST)
	public @ResponseBody ModelAndView insertRecoAlgoMgmtRow(@ModelAttribute("scheduler") Scheduler scheduler, HttpServletRequest request,
															HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");

		int result = 0;
		result = schedulerService.insertRow(scheduler);

		mav.addObject("result", result);

		return mav;
	}
	
	
	@RequestMapping(value = "/schedulerSelect", method = RequestMethod.POST)
	public ModelAndView selectOneRow(@RequestParam("job_id") int job_id) {

		ModelAndView mav = new ModelAndView("jsonView");

		Scheduler scheduler = null;
		scheduler = schedulerService.selectOneRow(job_id);
		
		List<Scheduler> agentList =null;
		agentList = schedulerService.selectAgentList();

		mav.addObject("result", scheduler);
		mav.addObject("agentList", agentList);

		return mav;
	}
	
	
	@RequestMapping(value = "/schedulerUpdate", method = RequestMethod.POST)
	public @ResponseBody ModelAndView updateRecoAlgoMgmtRow(@ModelAttribute("scheduler") Scheduler scheduler) {

		ModelAndView mav = new ModelAndView("jsonView");

		int updateResult = 0;
		updateResult = schedulerService.updateRow(scheduler);
		
		mav.addObject("result", updateResult);
		
		return mav;
	}
	
	@RequestMapping(value = "/schedulerRedo", method = {RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody ModelAndView redo(@ModelAttribute("scheduler") Scheduler scheduler) {

		ModelAndView mav = new ModelAndView("jsonView");

		int insertResult = 0;
		insertResult = schedulerService.insertRedo(scheduler);

		mav.addObject("result", insertResult);
		
		return mav;
	}
	
	@RequestMapping(value = "/schedulerDelete", method = {RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody ModelAndView schedulerDelete(@ModelAttribute("scheduler") Scheduler scheduler) {
		
		ModelAndView mav = new ModelAndView("jsonView");
		schedulerService.deleteAll(scheduler);
		
		return mav;
	}
	
	
	@RequestMapping(value = "/schedulerApply", method = {RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody ModelAndView schedulerApply() {
		ModelAndView mav = new ModelAndView("jsonView");
		
		HttpClient client = new HttpClient();
		String url = env.getProperty("choroc.scheduler.path");
		HttpMethod method = new GetMethod(url);
		
		try {
			int statusCode = client.executeMethod(method);
			
			mav.addObject("msg", method.getResponseBodyAsString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return mav;
	}
	
	/* 스키마 단위 ETL 수동실행 */
	@RequestMapping(value = "/schedulerEtl", method = {RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody ModelAndView etl(@ModelAttribute("scheduler") Scheduler scheduler) {

		ModelAndView mav = new ModelAndView("jsonView");
		
		int insertResult = 0;
		insertResult = schedulerService.etlExecute(scheduler);

		mav.addObject("result", insertResult);
		
		return mav;
	}	
	
	
}
