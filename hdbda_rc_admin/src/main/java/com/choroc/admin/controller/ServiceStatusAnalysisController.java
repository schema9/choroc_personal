package com.choroc.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.web.AbstractController;

/**
 * TODO > 서비스 현황 분석 > 사용자행동데이터 조회
 * 파일명   > ServiceStatusAnalysisController.java
 * 작성일   > 2018. 11. 21.
 * 작성자   > CCmedia Service Corp.
 */

@Controller
@RequestMapping(value = "/service/report")
public class ServiceStatusAnalysisController extends AbstractController {

	private static final Logger logger = LoggerFactory.getLogger(ServiceStatusAnalysisController.class);
	
/*	@Autowired
	private ServiceStatusAnalysisService serviceStatusAnalysisService;

	@Autowired
	ExcelService excelService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.KOREA);
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}*/
	
	/** 
	 * /service/report/*.do에 매칭되는 모든 jsp페이지 
	 */
	@RequestMapping("/*.do")
	public ModelAndView viewServiceStatusAnalysisPage(HttpServletRequest request, ModelMap model) {

		logger.info("@RequestMapping /service/report/*.do");
		String reqUri = request.getRequestURI();
		String tmpUri = reqUri.substring(reqUri.lastIndexOf("/") + 1);
		if(log.isDebugEnabled()){
			log.debug("*** Request uri=[" + reqUri +"], tmpUri=" + tmpUri);
		}

		String finalUri = tmpUri.substring(0, tmpUri.indexOf("."));

		if(log.isDebugEnabled()){
			log.debug("*** finalUri=[" + finalUri +"]");
		}

		ModelAndView mav = new ModelAndView("service/report/" + finalUri);
		logger.info("ModelAndView service/report/" + finalUri);
		return mav;
	}
	
	
	/**
	 * 방문자 대비 주문 페이지 리스트
	 * @param paramMap
	 * @param request
	 * @param response
	 * @return
	 */
/*	@ResponseBody
	@RequestMapping(value="/getUserBehaviorRecordList.do" , method = RequestMethod.POST)
	public ModelAndView getUserBehaviorRecordList (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> result = serviceStatusAnalysisService.getUserBehaviorRecordList(paramMap);

		mv.addObject("result", result);

		return mv;
	}*/

	/**
	 * 방문자 대비 주문 페이지 카운트
	 * @param paramMap
	 * @param request
	 * @param response
	 * @return
	 */
/*	@ResponseBody
	@RequestMapping(value="/getUserBehaviorRecordCnt.do" , method = RequestMethod.POST)
	public ModelAndView getUserBehaviorRecordCnt (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = serviceStatusAnalysisService.getUserBehaviorRecordCnt(paramMap);

		mv.addObject("result", result);

		return mv;
	}*/

	/**
	 * Excel download Component For Json data
	 * @param request
	 * @param response
	 * @return
	 */
/*	@RequestMapping("/downloadExcel")
	public ModelAndView excelTransform(HttpServletRequest request, HttpServletResponse response){
		Map<String,Object> excelParam = new HashMap<>();
        Map<String,Object> excelList = new HashMap<>();
        DomainInfo domainInfo = (DomainInfo)getRequestAttribute("domainInfo");
        ServiceInfo serviceInfo = (ServiceInfo)getRequestAttribute("serviceInfo");
		String reqJson = request.getParameter("inpDataList");
		String reqListName = request.getParameter("inpListName");

        excelParam.put("jsonData",reqJson);
        excelParam.put("listName",reqListName);
        excelParam.put("p_domainNm",domainInfo.getDomainNm());
        excelParam.put("p_domainUrl",domainInfo.getDomainUrl());
        excelParam.put("serviceId",serviceInfo.getServiceId());

        if("user-action".equals(reqListName)){

            String startDate    = request.getParameter("startDate");
            String endDate      = request.getParameter("endDate");
            String orderNm      = request.getParameter("orderNm");
            String orderAseDesc = request.getParameter("orderAseDesc");

            excelParam.put("startDate",startDate);
            excelParam.put("endDate",endDate);
            excelParam.put("orderNm",orderNm);
            excelParam.put("orderAseDesc",orderAseDesc);

            excelList = excelService.mappedExcelList(excelParam);
        }else{
            excelList = excelService.mappedExcelListFromJson(excelParam);
        }
        return new ModelAndView("excelView","excelList", excelList);
	}	*/
}
