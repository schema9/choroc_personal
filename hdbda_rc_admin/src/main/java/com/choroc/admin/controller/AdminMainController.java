package com.choroc.admin.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.web.AbstractController;
import com.choroc.api.domain.StatisticsParam;
import com.choroc.api.dto.ApiInfo;
import com.choroc.api.dto.RcEoadTagStat;
import com.choroc.api.service.RecommendationStatService;
import com.choroc.api.util.Validator;

/**
 * TODO  >
 * 파일명   > AdminMainController.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2018. 11. 9.
 */

@Controller
@RequestMapping(value = "/index")
public class AdminMainController extends AbstractController {
   	
	private static final Logger logger = LoggerFactory.getLogger(AdminMainController.class);
	
	/** service. */
	@Autowired
	private RecommendationStatService recommendationStatService;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.KOREA);
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	/**
	 * /index/*.do에 매칭되는 모든 jsp페이지
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/*.do")
	public ModelAndView modelView(HttpServletRequest request, ModelMap model){

		String reqUri = request.getRequestURI();
		String tmpUri = reqUri.substring(reqUri.lastIndexOf("/") + 1);
		if(log.isDebugEnabled()){
			log.debug("*** Request uri=[" + reqUri +"], tmpUri=" + tmpUri);
		}

		String finalUri = tmpUri.substring(0, tmpUri.indexOf("."));

		if(log.isDebugEnabled()){
			log.debug("*** finalUri=[" + finalUri +"]");
		}
		
		ModelAndView mav = new ModelAndView("index/" + finalUri);
		
		return mav;
	}
	
	/* 메인 대쉬보드 init */
	@RequestMapping(value="/adminDashboard.do")
	public ModelAndView dashboard(HttpServletRequest request, ModelMap model){

		String reqUri = request.getRequestURI();
		String tmpUri = reqUri.substring(reqUri.lastIndexOf("/") + 1);
		
		logger.info("*** Request uri=[" + reqUri +"], tmpUri=" + tmpUri);
		if(log.isDebugEnabled()){
			log.debug("*** Request uri=[" + reqUri +"], tmpUri=" + tmpUri);	
		}

		String finalUri = tmpUri.substring(0, tmpUri.indexOf("."));
        
		logger.info("*** finalUri=[" + finalUri +"]");
		if(log.isDebugEnabled()){
			log.debug("*** finalUri=[" + finalUri +"]");
		}
		
		ModelAndView mav = new ModelAndView("index/adminDashboard");
		return mav;
	}	
	
	
    @RequestMapping(value="/report/performance/{api}", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView selectRcTagStat(@ModelAttribute("param") ApiInfo apiParam, @PathVariable String api,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
    	ModelAndView mv = new ModelAndView("jsonView");
    	Map<String,Object> resultMap = new HashMap<String, Object>();
    	List<RcEoadTagStat> list = new ArrayList<RcEoadTagStat>();
    	
    	StatisticsParam param = new StatisticsParam();
        param.setApi(api);
        String resErrMsg;
		try {
			// param set
			resErrMsg = Validator.setParam(request, param);

			// param check
			if (StringUtils.isEmpty(resErrMsg)) {
				resErrMsg = Validator.paramValidator(request, param);
			}

			// resErrMsg가 null 이 아닌경우 400 error 리턴
			if (!StringUtils.isEmpty(resErrMsg)) {
				mv.addObject("returnErrMsg", resErrMsg + " 에러");
			} else {
				resultMap = recommendationStatService.index(apiParam);
				
				list = (List<RcEoadTagStat>) resultMap.get("list");
				mv.addObject("resultList", list);
				mv.addObject("returnErrMsg", "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
    	
    	mv.addObject("resultList", list);
    	
    	return mv;
    }
    
    
    @RequestMapping(value="/report/performanceChart/{api}", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView selectRcTagChart(@ModelAttribute("param") ApiInfo apiParam, @PathVariable String api,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

        	ModelAndView mv = new ModelAndView("jsonView");
        	Map<String,Object> resultMap = new HashMap<String, Object>();
        	List<RcEoadTagStat> list = new ArrayList<RcEoadTagStat>();
        	
        	StatisticsParam param = new StatisticsParam();
            param.setApi(api);
            String resErrMsg;
    		try {
    			// param set
    			resErrMsg = Validator.setParam(request, param);

    			// param check
    			if (StringUtils.isEmpty(resErrMsg)) {
    				resErrMsg = Validator.paramValidator(request, param);
    			}

    			// resErrMsg가 null 이 아닌경우 400 error 리턴
    			if (!StringUtils.isEmpty(resErrMsg)) {
    				mv.addObject("returnErrMsg", resErrMsg + " 에러");
    			} else {
    				resultMap = recommendationStatService.index(apiParam);
    				
    				list = (List<RcEoadTagStat>) resultMap.get("list");
    				mv.addObject("resultList", list);
    				mv.addObject("returnErrMsg", "");
    			}
    		} catch (Exception e) {
    			e.printStackTrace();
    			throw new Exception(e.getMessage());
    		}
        	
        	mv.addObject("resultList", list);
    	
    	return mv;
    }

}
