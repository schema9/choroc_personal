package com.choroc.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.domain.MenuParam;
import com.choroc.admin.dto.Menu;
import com.choroc.admin.dto.MenuInfo;
import com.choroc.admin.dto.MenuMap;
import com.choroc.admin.service.MenuService;
import com.choroc.admin.util.WebFactoryUtil;
import com.choroc.admin.util.ZValue;
import com.choroc.admin.web.AbstractController;
import com.google.gson.Gson;

/**
 * TODO > 시스템 관리 > 메뉴 권한 관리
 * 파일명   > MenuManagementController.java
 * 작성자   > CCmedia Service Corp.
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/administrator/menu")
@PropertySource("classpath:application-context.properties")
public class MenuManagementController extends AbstractController {
	
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private Environment env;
	
	/* 시스템 관리 > 메뉴 권한 관리 init-search */
    @RequestMapping(value="/menuList.do" )
    public ModelAndView selectMenuRows(@ModelAttribute("menuInfo") MenuInfo menuInfo, @ModelAttribute("menuParam") MenuParam param, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    	 
    	ModelAndView mav = new ModelAndView("administrator/menuManage");
        
        // 한 페이지에 게시되는 게시물 건수
        if (param.getRecordCountPerPage() == 0) {
            param.setRecordCountPerPage(Integer.parseInt(env.getProperty("property.pagination.recordcountperpage")));
        }
        // 페이징 리스트의 사이즈
        param.setPageSize(Integer.parseInt(env.getProperty("property.pagination.pagesize")));
        
        String currentPageNo = request.getParameter("currentPageNo");
        if (currentPageNo != null) {
            param.setCurrentPageNo(Integer.parseInt(currentPageNo));
        }              
        mav.addObject("firstIndex", param.getFirstRecordIndex());
        mav.addObject("recordCountPerPage", param.getRecordCountPerPage());
			      
        // 메뉴 목록
        List<MenuInfo> menuList = menuService.selectMenuRows(param);
        int totalCount = menuService.selectMenuTotalCount(param);
       
        param.setTotalRecordCount(totalCount);
        
        mav.addObject("menuParam", param);
        mav.addObject("menuList", menuList);
        mav.addObject("totalCnt", totalCount);

        return mav;
    }	
	

	/**
	 * 메뉴 등록 화면
	 * @param request
	 * @param response
	 * @return modelAndView
	 * @throws Exception
	 */
	@RequestMapping(value="/insertPopUpMenu.do")
	public ModelAndView createMenu2(HttpServletRequest request, HttpServletResponse response) throws Exception {
				
    	ModelAndView mav = new ModelAndView("jsonView");
    	
		List<MenuInfo> result = menuService.selectAllMenu();      // 메뉴목록
		List<ZValue> authList =	menuService.retrieveAuthorList(); // 시스템 권한
		
		Gson gson = new Gson();
		String jsonList = gson.toJson(result);
		
		mav.addObject("result", result);
		mav.addObject("authList", authList);
		mav.addObject("jsonList", jsonList);
		
		return mav;
	}

	
	/**
	 * 메뉴 등록
	 * @param request
	 * @param response
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value="/insertMenu.do")
	public ModelAndView insertMenu(HttpServletRequest request, HttpServletResponse response) throws Exception { 
		
		ModelAndView model = new ModelAndView();
		ZValue zvl = WebFactoryUtil.getAttributesInit(request);
		
		try	{
			menuService.insertMenu(zvl, request);
		}
		catch(Exception e) {
			e.printStackTrace();
		}		
		model.setViewName( "jsonView" );
		
		return model;
	}	
	

	/* 메뉴 화면  수정버튼 선택  */
	@RequestMapping(value = "/menuSelect.do", method = RequestMethod.POST)
	public ModelAndView selectOneRow(@RequestParam("seq") int seq , @RequestParam("upperMenuId") int upperMenuId){

		ModelAndView mav = new ModelAndView("jsonView");
		
		Menu menu = null;
		List<MenuInfo> result = null;
		List<ZValue> authList = null;
		List<MenuMap> userAuthList = null;
		List<ZValue> resultList = null;
		ZValue zvl = new ZValue();
		
		zvl.put("menuId", String.valueOf(upperMenuId));
		
		if(upperMenuId == 0)
			zvl.put("upperMenuId", "N");
		else 
			zvl.put("upperMenuId", String.valueOf(upperMenuId));
				
		try {
			menu = menuService.selectOneRow(seq);          // 선택메뉴
			result = menuService.selectAll2Menu(zvl);      // 수정선택한 메뉴 리스트
			authList =	menuService.retrieveAuthorList();  // 권한리스트
			userAuthList = menuService.selectAuthorId(seq); // 권한메뉴 맵핑테이블에 등록된 권한
			resultList = menuService.selectDistinctAuthorId(zvl); // 상위메뉴 권한
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		mav.addObject("menu", menu);
		mav.addObject("result", result);
		mav.addObject("authList", authList);
		mav.addObject("userAuthList", userAuthList);
		mav.addObject("resultList", resultList);
		
		return mav;
	}	
	
	/**
	 * 메뉴 권한 체크
	 * @param request
	 * @param response
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/menuAuthChek.do", method = RequestMethod.POST)
	public ModelAndView menuAuthChek(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ModelAndView mav =new ModelAndView("jsonView");
		ZValue zvl = WebFactoryUtil.getAttributesInit(request);
		
		List<ZValue> resultList = menuService.selectDistinctAuthorId(zvl);
		mav.addObject("resultList", resultList);

		return mav;
	}

	/**
	 * 메뉴 수정
	 * @param request
	 * @param response
	 * @return 
	 * @throws Exception
	 */	
	@RequestMapping(value="/updateMenu.do")
	public ModelAndView updateMenu(HttpServletRequest request, HttpServletResponse response) throws Exception { 
		
		ModelAndView model = new ModelAndView("jsonView");		
		ZValue zvl = WebFactoryUtil.getAttributesInit(request);
		
		try {
			menuService.updateMenu(zvl, request);		
		}catch(Exception e) {
			e.printStackTrace();
		}
				
		return model;
	}	
			
}