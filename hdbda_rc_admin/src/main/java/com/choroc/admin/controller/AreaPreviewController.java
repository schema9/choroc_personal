package com.choroc.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.domain.AreaPreviewParam;
import com.choroc.admin.service.AlgorithmService;
import com.choroc.admin.web.AbstractController;
import com.choroc.api.domain.StatisticsParam;
import com.choroc.api.dto.ApiInfo;
import com.choroc.api.service.RecommendationService;
import com.choroc.api.util.Validator;

/**
 * TODO  > 추천 환경관리 > 추천 알고리즘 미리보기
 * 파일명   > AreaPreviewController.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/environment/areaPreview")
@PropertySource("classpath:application-context.properties")
public class AreaPreviewController extends AbstractController{
	
	@Autowired
	private RecommendationService recommendationService;
	
	/* 추천 환경관리 > 추천 알고리즘 미리보기 > 상품별 추천상품 init */
    @CrossOrigin
    @RequestMapping(value = "/productList.do")
    public @ResponseBody ModelAndView recoByProductList(@ModelAttribute("areaPreviewParam") AreaPreviewParam param,HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    	ModelAndView mav = new ModelAndView("environment/areaPreview/recoByProduct");
        return mav;
    }
    
    /* 추천 환경관리 > 추천 알고리즘 미리보기 > 검색어별 추천상품 init */
    @RequestMapping(value = "/searchWordRecoPrdList.do")
    public @ResponseBody ModelAndView recoBySearchProductList(@ModelAttribute("areaPreviewParam") AreaPreviewParam param,HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    	ModelAndView mav = new ModelAndView("environment/areaPreview/recoBySearchProduct");
        return mav;
    }
    
    /* 추천 환경관리 > 추천 알고리즘 미리보기 > 검색어별 추천검색어 init */
    @RequestMapping(value = "/searchWordRecoWordList.do")
    public @ResponseBody ModelAndView recoBySearchWordList(@ModelAttribute("areaPreviewParam") AreaPreviewParam param,HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    	ModelAndView mav = new ModelAndView("environment/areaPreview/recoBySearchWord");
        return mav;
    }
    
    /* 추천 환경관리 > 추천 알고리즘 미리보기 > 사용자별 추천상품 init */
    @RequestMapping(value = "/userList.do")
    public @ResponseBody ModelAndView recoByUserList(@ModelAttribute("areaPreviewParam") AreaPreviewParam param,HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    	ModelAndView mav = new ModelAndView("environment/areaPreview/recoByUser");
        return mav;
    }
    
    /* 추천 환경관리 > 추천 알고리즘 미리보기 > 사용자정의별 추천상품 init */
    @RequestMapping(value = "/searchCustomRecoPrdList.do")
    public @ResponseBody ModelAndView searchCustomRecoPrdList(@ModelAttribute("areaPreviewParam") AreaPreviewParam param,HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    	ModelAndView mav = new ModelAndView("environment/areaPreview/recoByCustomProduct");
        return mav;
    }

    
    @RequestMapping(value="/recommand/{api}", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView areaPreview(@ModelAttribute("apiParam") ApiInfo apiParam, HttpServletRequest request, HttpServletResponse response, Map<String,Object> commandMap, @PathVariable String api) throws Exception{	
		ModelAndView mv = new ModelAndView("jsonView");
		StatisticsParam param = new StatisticsParam();
        param.setApi(api);
        String resErrMsg;
        Map<String,Object> list = null;
        try {

            // param set
            resErrMsg = Validator.setParam(request, param);

            // param check
            if(StringUtils.isEmpty(resErrMsg)) {
                resErrMsg = Validator.paramValidator(request, param);
            }

            // resErrMsg가 null 이 아닌경우 400 error 리턴
            if(!StringUtils.isEmpty(resErrMsg)) {
                //response.sendError(400, "Error : invalid parameter [ "+resErrMsg+" ]");
            	mv.addObject("returnErrMsg",resErrMsg+" 에러");
            } else {
            	
            	apiParam.setFirstIndexInt(param.getFirstIndexInt());
            	apiParam.setLastIndexInt(param.getLastIndexInt());
            	
                list = recommendationService.areaPreview(apiParam);
        		mv.addObject("returnErrMsg",list.get("returnMsg"));
            }
           
        } catch(Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
		mv.addObject("list",list);
		mv.addObject("apiNm",param.getApi());
		
		return mv;
	}
    
    @RequestMapping(value="recommand/logging/{api}", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView areaPreviewLogging(@ModelAttribute("apiParam") ApiInfo apiParam, HttpServletRequest request, HttpServletResponse response, Map<String,Object> commandMap, @PathVariable String api) throws Exception{	
		ModelAndView mv = new ModelAndView("jsonView");
		StatisticsParam param = new StatisticsParam();
        param.setApi(api);
        
        String resErrMsg;
        Map<String,Object> list = null;
        try {
            // param set
            resErrMsg = Validator.setParam(request, param);

            // param check
            if(StringUtils.isEmpty(resErrMsg)) {
                resErrMsg = Validator.paramValidator(request, param);
            }

            // resErrMsg가 null 이 아닌경우 400 error 리턴
            if(!StringUtils.isEmpty(resErrMsg)) {
                //response.sendError(400, "Error : invalid parameter [ "+resErrMsg+" ]");
            	mv.addObject("returnErrMsg",resErrMsg);
            } else {
            	apiParam.setFirstIndexInt(param.getFirstIndexInt());
            	apiParam.setLastIndexInt(param.getLastIndexInt());
            	
                list = recommendationService.areaPreviewLogging(apiParam);
        		mv.addObject("returnErrMsg","");
            }
           
        } catch(Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
		mv.addObject("list",list);
		mv.addObject("apiNm",param.getApi());
		
		return mv;
	}
    
    
    
    
    
    
    
    
    
    
    
    
    
    @RequestMapping(value = "/recommendApi.do")
    public ModelAndView recommendAPI(HttpServletRequest request, HttpServletResponse response) {
    	ModelAndView mav = new ModelAndView("environment/widget/recommendApi");
        return mav;
    }
    
    @RequestMapping(value = "/ht.do")
    public @ResponseBody ModelAndView hbdaApiTest(@ModelAttribute("areaPreviewParam") AreaPreviewParam param,HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    	ModelAndView mav = new ModelAndView("environment/areaPreview/apiTest");
        return mav;
    }
}