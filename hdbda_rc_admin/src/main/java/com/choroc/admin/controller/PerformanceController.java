package com.choroc.admin.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.domain.AreaPreviewParam;
import com.choroc.admin.dto.DomainInfo;
import com.choroc.admin.dto.ServiceInfo;
import com.choroc.admin.service.ExcelService;
import com.choroc.admin.service.PerformanceService;
import com.choroc.admin.web.AbstractController;
import com.choroc.api.domain.StatisticsParam;
import com.choroc.api.dto.ApiInfo;
import com.choroc.api.service.RecommendationStatService;
import com.choroc.api.util.Validator;

/**
 * TODO  > 추천 성과 분석 > 추천 성과
 * 파일명   > PerformanceController.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/performance")
@PropertySource("classpath:application-context.properties")
public class PerformanceController extends AbstractController {

	/** service. */
	@Autowired
	private RecommendationStatService recommendationStatService;
	
	@Autowired
	private PerformanceService performanceService;
	
	/** The env. */
	@Autowired
	private Environment env;

	@Autowired
	ExcelService excelService;
	
	/**
	 * Inits the binder.
	 * 
	 * @param binder the binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.KOREA);
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}	
	
	
	/* 추천 성과분석 > 추천성과 > 아이템별 추천성과 init */
	@RequestMapping(value="/recoCollectionList.do")
	public @ResponseBody ModelAndView recoCollectionList(@ModelAttribute("areaPreviewParam") AreaPreviewParam param,HttpServletRequest request, HttpServletResponse response, ModelMap model) {
	    ModelAndView mav = new ModelAndView("performance/performanceRecoCollList");
		return mav;
	}
	
	/* 추천 성과분석 > 추천성과 > 영역별 추천성과 init */
	@RequestMapping(value="/recoAreaList.do")
	public @ResponseBody ModelAndView recoAreaCollectionList(@ModelAttribute("areaPreviewParam") AreaPreviewParam param,HttpServletRequest request, HttpServletResponse response, ModelMap model) {
	    ModelAndView mav = new ModelAndView("performance/performanceRecoAreaList");
		return mav;
	}
	
	
    @RequestMapping(value="/recommendationStat/performance/{api}", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView performance(@ModelAttribute("apiParam") ApiInfo apiParam, HttpServletRequest request, HttpServletResponse response, Map<String,Object> commandMap, @PathVariable String api) throws Exception{	
		ModelAndView mv = new ModelAndView("jsonView");
		StatisticsParam param = new StatisticsParam();
        param.setApi(api);
        
        String resErrMsg;
        Map<String,Object> list = null;
        try {
            // param set
            resErrMsg = Validator.setParam(request, param);

            // param check
            if(StringUtils.isEmpty(resErrMsg)) {
                resErrMsg = Validator.paramValidator(request, param);
            }

            // resErrMsg가 null 이 아닌경우 400 error 리턴
            if(!StringUtils.isEmpty(resErrMsg)) {
                //response.sendError(400, "Error : invalid parameter [ "+resErrMsg+" ]");
            	mv.addObject("returnErrMsg",resErrMsg+" 에러");
            } else {
            	apiParam.setFirstIndexInt(param.getFirstIndexInt());
            	apiParam.setLastIndexInt(param.getLastIndexInt());
                list = recommendationStatService.performance(apiParam);
        		mv.addObject("returnErrMsg","");
            }
           
        } catch(Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
		mv.addObject("list",list);
		mv.addObject("apiNm",param.getApi());
		
		return mv;
	}
	
	
    
    
    
	
	
	
	

	/**
	 * 추천 영역별 화면 목록
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/*.do")
	public ModelAndView modelView(HttpServletRequest request, ModelMap model){
		
		String reqUri = request.getRequestURI();
		String tmpUri = reqUri.substring(reqUri.lastIndexOf("/") + 1);
		
		
		if(log.isDebugEnabled()){
			log.debug("*** Request uri=[" + reqUri +"], tmpUri=" + tmpUri);
		}
		
		String finalUri = tmpUri.substring(0, tmpUri.indexOf("."));
		
		if(log.isDebugEnabled()){
			log.debug("*** finalUri=[" + finalUri +"]");
		}

		ModelAndView mav = new ModelAndView("performance/" + finalUri, model);
		
		return mav;
	}

    /**
     * 추천 영역별 상세 카운트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getAreaAlgorithmSetCnt.do" , method = RequestMethod.POST)
	public ModelAndView getAreaAlgorithmSetCnt (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = performanceService.getAreaAlgorithmSetCnt(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 추천 영역별 알고리즘 상세 카운트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getAreaAlgorithmCnt.do" , method = RequestMethod.POST)
	public ModelAndView getAreaAlgorithmCnt (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		paramMap.put("masterDb",env.getProperty("platform.mysql.master.db").toString());
		int result = performanceService.getAreaAlgorithmCnt(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 추천 영역별 알고리즘 추천영역 리스트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getAreaAlgorithmChartList.do" , method = RequestMethod.POST)
	public ModelAndView getAreaAlgorithmChartList (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> result = performanceService.getAreaAlgorithmChartList(paramMap);

		mv.addObject("result", result);

		return mv;
	}
	
    /**
     * 추천알고리즘 SET 리스트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getAlgorithmSetList.do" , method = RequestMethod.POST)
	public ModelAndView getAlgorithmSetList (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> result = performanceService.getAlgorithmSetList(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 추천 영역별 알고리즘 상세 카운트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getAlgorithmSetCnt.do" , method = RequestMethod.POST)
	public ModelAndView getAlgorithmSetCnt (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = performanceService.getAlgorithmSetCnt(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 추천 알고리즘 알고리즘 SEt 리스트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getAlgorithmAlgorithmSetList.do" , method = RequestMethod.POST)
	public ModelAndView getAlgorithmAlgorithmSetList (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> result = performanceService.getAlgorithmAlgorithmSetList(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 추천알고리즘  리스트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getAlgorithmList.do" , method = RequestMethod.POST)
	public ModelAndView getAlgorithmList (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		paramMap.put("masterDb",env.getProperty("platform.mysql.master.db").toString());
		List<Map<String, Object>> result = performanceService.getAlgorithmList(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 추천 알고리즘 상세 카운트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getAlgorithmCnt.do" , method = RequestMethod.POST)
	public ModelAndView getAlgorithmCnt (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		paramMap.put("masterDb",env.getProperty("platform.mysql.master.db").toString());

		int result = performanceService.getAlgorithmCnt(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 추천 알고리즘 리스트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getAlgorithmAlgorithmList.do" , method = RequestMethod.POST)
	public ModelAndView getAlgorithmAlgorithmList (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		paramMap.put("masterDb",env.getProperty("platform.mysql.master.db").toString());

		List<Map<String, Object>> result = performanceService.getAlgorithmAlgorithmList(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 방문자 대비 주문 페이지 카운트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getPervisitorOrderPageCnt.do" , method = RequestMethod.POST)
	public ModelAndView getPervisitorOrderPageCnt (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = performanceService.getPervisitorOrderPageCnt(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 방문자 대비 주문 페이지 리스트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getPervisitorOrderPageList.do" , method = RequestMethod.POST)
	public ModelAndView getPervisitorOrderPageList (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> result = performanceService.getPervisitorOrderPageList(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 추천영역 별 알고리즘 리스트 박스
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getRcmAreaAlgorithmBox.do" , method = RequestMethod.POST)
	public ModelAndView getRcmAreaAlgorithmBox (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> result = performanceService.getRcmAreaAlgorithmBox(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 추천영역 별 알고리즘 SET
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getRcmAreaAlgorithmSet.do" , method = RequestMethod.POST)
	public ModelAndView getRcmAreaAlgorithmSet (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> result = performanceService.getRcmAreaAlgorithmSet(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 추천영역 별 알고리즘 SET 카운트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getRcmAreaAlgorithmSetCnt.do" , method = RequestMethod.POST)
	public ModelAndView getRcmAreaAlgorithmSetCnt (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		int result = performanceService.getRcmAreaAlgorithmSetCnt(paramMap);

		mv.addObject("result", result);

		return mv;
	}

    /**
     * 추천영역 별 알고리즘 SET 리스트
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/getRcmAreaAlgorithmSetList.do" , method = RequestMethod.POST)
	public ModelAndView getRcmAreaAlgorithmSetList (
			@RequestBody Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mv = new ModelAndView("jsonView");

		paramMap.put("masterDb",env.getProperty("platform.mysql.master.db").toString());

		List<Map<String, Object>> result = performanceService.getRcmAreaAlgorithmSetList(paramMap);

		mv.addObject("result", result);

		return mv;
	}

	/**
	 * Excel download Component For database data
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/downloadExcel")
	public ModelAndView excelTransform(HttpServletRequest request, HttpServletResponse response){
	    Map<String,Object> excelParam = new HashMap<String,Object>();
        Map<String,Object> excelList = new HashMap<String,Object>();
        DomainInfo domainInfo = (DomainInfo)getRequestAttribute("domainInfo");
        ServiceInfo serviceInfo = (ServiceInfo)getRequestAttribute("serviceInfo");

        String reqJson      = request.getParameter("inpDataList");
		String reqListName  = request.getParameter("inpListName");
		String startDate    = request.getParameter("startDate");
		String endDate      = request.getParameter("endDate");
		String searchOption = request.getParameter("searchOption");
		String searchText   = request.getParameter("searchText");
		String orderNm      = request.getParameter("orderNm");
		String orderAseDesc = request.getParameter("orderAseDesc");
		String htmlData = request.getParameter("htmlData");

        excelParam.put("p_domainNm",domainInfo.getDomainNm());
        excelParam.put("p_domainUrl",domainInfo.getDomainUrl());
        excelParam.put("serviceId",serviceInfo.getServiceId());
		excelParam.put("jsonData",reqJson);
		excelParam.put("listName",reqListName);
		excelParam.put("startDate",startDate);
		excelParam.put("endDate",endDate);
		excelParam.put("searchOption",searchOption);
		excelParam.put("searchText",searchText);
		excelParam.put("orderNm",orderNm);
		excelParam.put("orderAseDesc",orderAseDesc);
        excelParam.put("htmlData",htmlData);
        excelParam.put("masterDb",env.getProperty("platform.mysql.master.db").toString());

        if("svc-reco-result".equals(reqListName) || "reco-area-result".equals(reqListName)){
            excelList.put("listName",reqListName);
            excelList.put("htmlData",htmlData);
        }else {
            excelList = excelService.mappedExcelList(excelParam);
        }

		return new ModelAndView("excelView","excelList", excelList);
	}
	

}
