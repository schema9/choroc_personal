package com.choroc.admin.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choroc.admin.domain.AlgorithmParam;
import com.choroc.admin.dto.Algorithm;
import com.choroc.admin.service.AlgorithmService;
import com.choroc.admin.web.AbstractController;
import com.choroc.api.domain.StatisticsParam;
import com.choroc.api.dto.ApiInfo;
import com.choroc.api.dto.RecommendAlgorithm;
import com.choroc.api.dto.RecommendArea;
import com.choroc.api.dto.RecommendException;
import com.choroc.api.dto.RecommendMd;
import com.choroc.api.service.RecommendationAdminService;
import com.choroc.api.util.Validator;

/**
 * TODO  > 추천 환경관리 > 추천 알고리즘
 * 파일명   > AlgorithmController.java
 * 작성자   > CCmediaService.co.kr
 * 작성일   > 2019.09.01.
 */

@Controller
@RequestMapping(value = "/environment/algorithm")
public class AlgorithmController extends AbstractController {
	
	@Autowired
	private AlgorithmService algorithmService;
	
	@Autowired
	private RecommendationAdminService recommendationAdminService;

	private static final Logger logger = LoggerFactory.getLogger(AlgorithmController.class);

	/* 추천 환경관리 > 추천 알고리즘 init */
	@ResponseBody
	@RequestMapping(value = "/recommendAlgorithmList")
	public ModelAndView getAlgorithmList(@ModelAttribute("AlgorithmParam") AlgorithmParam param,
			Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response)
			throws SQLException {

		ModelAndView mv = new ModelAndView("/environment/algorithm/recommendAlgorithm");
		return mv;
	}

    @RequestMapping(value="recommand/manager/{api}", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView recommandManager(@ModelAttribute("apiParam") ApiInfo apiParam, HttpServletRequest request, HttpServletResponse response, Map<String,Object> commandMap, @PathVariable String api) throws Exception{	
		ModelAndView mv = new ModelAndView("jsonView");
		StatisticsParam param = new StatisticsParam();
		param.setApi(api);
		apiParam.setApi(api);
		String resErrMsg;
	
		Map<String, Object> resultMap = new HashMap<String, Object>();
		    
		//알고리즘 관리
		if (api.equals("rc-algorithm-count")) {
			// List<RecommendAlgorithm> list = null;
		}
		if (api.equals("rc-algorithm-list")) {
			// List<RecommendAlgorithm> list = null;
		}
		if (api.equals("rc-algorithm-pk-check")) {
			// List<RecommendAlgorithm> list = null;
		}
		if (api.equals("rc-algorithm-insert")) {
			RecommendAlgorithm rc_algorithm = new RecommendAlgorithm();
			rc_algorithm.setAlgrth_id(request.getParameter("algrth_id"));
	    	rc_algorithm.setAlgrth_nm(request.getParameter("algrth_nm"));
	    	rc_algorithm.setAlgrth_dc(request.getParameter("algrth_dc"));
	    	rc_algorithm.setCreate_id(request.getParameter("create_id"));
	    	apiParam.setRc_algorithm(rc_algorithm);
		}
		if (api.equals("rc-algorithm-selectOne")) {
			// List<RecommendAlgorithm> list = null;
		}
		if (api.equals("rc-algorithm-nm-check")) {
			RecommendAlgorithm rc_algorithm = new RecommendAlgorithm();
	    	rc_algorithm.setAlgrth_nm(request.getParameter("algrth_nm"));
	    	apiParam.setRc_algorithm(rc_algorithm);
		}
		if (api.equals("rc-algorithm-update")) {
			RecommendAlgorithm rc_algorithm = new RecommendAlgorithm();
	    	rc_algorithm.setAlgrth_id(request.getParameter("algrth_id"));
	    	rc_algorithm.setAlgrth_nm(request.getParameter("algrth_nm"));
	    	rc_algorithm.setAlgrth_dc(request.getParameter("algrth_dc"));
	    	rc_algorithm.setModify_id(request.getParameter("modify_id"));
	    	apiParam.setRc_algorithm(rc_algorithm);
		}
		if (api.equals("rc_algorithm-delete")) {
			RecommendAlgorithm rc_algorithm = new RecommendAlgorithm();
	    	rc_algorithm.setAlgrth_id(request.getParameter("algrth_id"));
	    	rc_algorithm.setModify_id(request.getParameter("modify_id"));
	    	apiParam.setRc_algorithm(rc_algorithm);
		}
		        
		//추천영역 관리
		if (api.equals("rc-area-count")) {
			// List<RecommendArea> list = null;
		}
		if (api.equals("rc-area-list")) {
			// List<RecommendArea> list = null;
		}
		if (api.equals("rc-area-insert-algo")) {
			// List<RecommendArea> list = null;
		}
		if (api.equals("rc-area-insert")) {
			RecommendArea rc_area = new RecommendArea();
			rc_area.setPge_area_id(request.getParameter("pge_area_id"));
			rc_area.setPge_area_nm(request.getParameter("pge_area_nm"));
			rc_area.setPge_area_path(request.getParameter("pge_area_path"));
			rc_area.setInput_param(request.getParameter("input_param"));
			rc_area.setAlgrth_id(request.getParameter("algrth_id"));
			rc_area.setUse_yn(request.getParameter("use_yn"));
			rc_area.setCreate_id(request.getParameter("create_id"));
			apiParam.setRc_area(rc_area);
	
		}
		if (api.equals("rc-area-update-algo")) {
			// List<RecommendArea> list = null;
			RecommendArea rc_area = new RecommendArea();
	
			rc_area.setPge_area_id(request.getParameter("pge_area_id"));
			apiParam.setRc_area(rc_area);
		}
		if (api.equals("rc-area-update")) {
			RecommendArea rc_area = new RecommendArea();
			rc_area.setPge_area_id(request.getParameter("pge_area_id"));
			rc_area.setPge_area_nm(request.getParameter("pge_area_nm"));
			rc_area.setPge_area_path(request.getParameter("pge_area_path"));
			rc_area.setInput_param(request.getParameter("input_param"));
			rc_area.setAlgrth_id(request.getParameter("algrth_id"));
			rc_area.setUse_yn(request.getParameter("use_yn"));
			rc_area.setModify_id(request.getParameter("modify_id"));
			apiParam.setRc_area(rc_area);
		}
		if (api.equals("rc-area-delete")) {
			RecommendArea rc_area = new RecommendArea();
			rc_area.setPge_area_id(request.getParameter("pge_area_id"));
			rc_area.setModify_id(request.getParameter("modify_id"));
			apiParam.setRc_area(rc_area);
		}
		
		// 추천 제외관리
	/*		if (api.equals("rc-except-list")) {
			
			
			// List<RecommendException> list = null;
		}*/
		if (api.equals("rc-except-insert")) {
			RecommendException rc_except = new RecommendException();
			rc_except.setContent_cd(request.getParameter("content_cd"));
			rc_except.setCreate_id(request.getParameter("create_id"));
			rc_except.setSortation(request.getParameter("sortation"));
			apiParam.setRc_except(rc_except);
	
		}
		if (api.equals("rc-except-delete")) {
			RecommendException rc_except = new RecommendException();
			rc_except.setContent_cd(request.getParameter("content_cd"));
			rc_except.setSortation(request.getParameter("sortation"));
			apiParam.setRc_except(rc_except);
		}
		
		if (api.equals("rc-md-insert")) {
			RecommendMd rc_md = new RecommendMd();
			rc_md.setContent_cd(request.getParameter("content_cd"));
			rc_md.setCreate_id(request.getParameter("create_id"));
			rc_md.setSortation(request.getParameter("sortation"));
			apiParam.setRc_md(rc_md);
	
		}
		if (api.equals("rc-md-delete")) {
			RecommendMd rc_md = new RecommendMd();
			rc_md.setContent_cd(request.getParameter("content_cd"));
			rc_md.setSortation(request.getParameter("sortation"));
			apiParam.setRc_md(rc_md);
		}
		
		try {
			// param set
			resErrMsg = Validator.setParam(request, param);
	
			// param check
			if (StringUtils.isEmpty(resErrMsg)) {
				resErrMsg = Validator.paramValidator(request, param);
			}
	
			// resErrMsg가 null 이 아닌경우 400 error 리턴
			if (!StringUtils.isEmpty(resErrMsg)) {
				// response.sendError(400, "Error : invalid parameter [ "+resErrMsg+" ]");
				mv.addObject("returnErrMsg", resErrMsg + " 에러");
			} else {
				resultMap = recommendationAdminService.manager(apiParam);
				
				//System.out.println("apiParam: " + apiParam);
				//System.out.println("resultMap: " + resultMap);
				
				// mv.addObject("returnErrMsg","");
				
				//알고리즘 관리
				if (api.equals("rc-algorithm-count")) {
					int totalCount = (int) resultMap.get("totalCount");
	        		mv.addObject("returnErrMsg","");
	        		mv.addObject("totalCount", totalCount);
				}
				if (api.equals("rc-algorithm-list")) {
					List<RecommendAlgorithm> list = (List<RecommendAlgorithm>) resultMap.get("list");
	        		mv.addObject("returnErrMsg","");
	        		mv.addObject("resultList", list);
				}
				if (api.equals("rc-algorithm-pk-check")) {
					String result = (String) resultMap.get("result");
	        		mv.addObject("returnErrMsg","");
	        		mv.addObject("result", result);
				}
				if (api.equals("rc-algorithm-insert")) {
					int result = (int) resultMap.get("result");
	        		mv.addObject("returnErrMsg","");
	        		mv.addObject("result", result);
				}
				if (api.equals("rc-algorithm-selectOne")) {
					RecommendAlgorithm result = (RecommendAlgorithm) resultMap.get("result");
					mv.addObject("returnErrMsg","");
	        		mv.addObject("result", result);
				}
				if (api.equals("rc-algorithm-nm-check")) {
					String result = (String) resultMap.get("result");
	        		mv.addObject("returnErrMsg","");
	        		mv.addObject("result", result);
				}
				if (api.equals("rc-algorithm-update")) {
					int  result = (int) resultMap.get("result");
	        		mv.addObject("returnErrMsg","");
	        		mv.addObject("result", result);
				}
				if (api.equals("rc_algorithm-delete")) {
					int  result = (int) resultMap.get("result");
	        		mv.addObject("returnErrMsg","");
	        		mv.addObject("result", result);
				}
				
				//추천영역 관리
				if (api.equals("rc-area-count")) {
					int totalCount = (int) resultMap.get("totalCount");
					mv.addObject("returnErrMsg", "");
					mv.addObject("totalCount", totalCount);
				}
				if (api.equals("rc-area-list")) {
					List<RecommendArea> list = (List<RecommendArea>) resultMap.get("list");
					mv.addObject("returnErrMsg", "");
					mv.addObject("resultList", list);
				}
				if (api.equals("rc-area-insert-algo")) {
					List<RecommendArea> list = (List<RecommendArea>) resultMap.get("list");
					int pkMax = (int) resultMap.get("pkMax");
					mv.addObject("returnErrMsg", "");
					mv.addObject("pkMax", pkMax);
					mv.addObject("result", list);
				}
				if (api.equals("rc-area-insert")) {
					int result = (int) resultMap.get("result");
					mv.addObject("returnErrMsg", "");
					mv.addObject("result", result);
				}
				if (api.equals("rc-area-update-algo")) {
					List<RecommendArea> list = (List<RecommendArea>) resultMap.get("list");
					RecommendArea rc_area = (RecommendArea) resultMap.get("result");
					mv.addObject("returnErrMsg", "");
					mv.addObject("resultList", list);
					mv.addObject("result", rc_area);
				}
				if (api.equals("rc-area-update")) {
					int result = (int) resultMap.get("result");
					mv.addObject("returnErrMsg", "");
					mv.addObject("result", result);
				}
				if (api.equals("rc-area-delete")) {
					int result = (int) resultMap.get("result");
					mv.addObject("returnErrMsg", "");
					mv.addObject("result", result);
				}
				
				//제외 관리
				if (api.equals("rc-except-list")) {
					
					int totalCount = (int) resultMap.get("totalCount");
					List<RecommendArea> list = (List<RecommendArea>) resultMap.get("list");
					
					//System.out.println("totalCount: " + totalCount);
					//System.out.println("List<RecommendArea> list : " + list);
					
					mv.addObject("returnErrMsg", "");
					mv.addObject("totalCount", totalCount);
					mv.addObject("resultList", list);
				}
				if (api.equals("rc-except-insert")) {
					String result = (String) resultMap.get("result");
					mv.addObject("returnErrMsg", "");
					mv.addObject("result", result);
				}
				if (api.equals("rc-except-delete")) {
	        		int result = (int) resultMap.get("result");
	        		mv.addObject("returnErrMsg","");
	        		mv.addObject("result", result);   	
				}
				
				//MD 관리
				if (api.equals("rc-md-list")) {
					
					int totalCount = (int) resultMap.get("totalCount");
					List<RecommendArea> list = (List<RecommendArea>) resultMap.get("list");
					
					//System.out.println("totalCount: " + totalCount);
					//System.out.println("List<RecommendArea> list : " + list);
					
					mv.addObject("returnErrMsg", "");
					mv.addObject("totalCount", totalCount);
					mv.addObject("resultList", list);
				}
				if (api.equals("rc-md-insert")) {
					String result = (String) resultMap.get("result");
					mv.addObject("returnErrMsg", "");
					mv.addObject("result", result);
				}
				if (api.equals("rc-md-delete")) {
	        		int result = (int) resultMap.get("result");
	        		mv.addObject("returnErrMsg","");
	        		mv.addObject("result", result);   	
				}
			}
	
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	
		return mv;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// 알고리즘 중복체크
	@ResponseBody
	@RequestMapping(value = "/checkRecommendAlgorithm.do", method = RequestMethod.POST)
	public ModelAndView checkAlgorithm(@RequestBody Map<String, Object> paramMap, HttpServletRequest request,
			HttpServletResponse response) throws SQLException {

		ModelAndView mv = new ModelAndView("");
		String result = algorithmService.checkAlgorithm(paramMap);

		logger.info("결과 메시지: " + result);
		mv.addObject("result", result);

		mv.setViewName("jsonView");
		return mv;
	}

	// 알고리즘 입력
	@ResponseBody
	@RequestMapping(value = "/createRecommendAlgorithm.do", method = RequestMethod.POST)
	public ModelAndView createAlgorithm(Map<String, Object> paramMap, HttpServletRequest request,
			HttpServletResponse response) throws SQLException {

		ModelAndView mv = new ModelAndView("/environment/algorithm/recommendAlgorithmList.do");
		int result = algorithmService.insertAlgorithm(paramMap, request);

		logger.info("결과 : " + result);
		mv.addObject("result", result);

		mv.setViewName("jsonView");
		return mv;
	}

	// 수정 버튼을 누른 알고리즘 1개
	@ResponseBody
	@RequestMapping(value = "/selectOneAlgorithm.do", method = RequestMethod.POST)
	public ModelAndView selectOneAlgorithm(Map<String, Object> paramMap, HttpServletRequest request,
			HttpServletResponse response) throws SQLException {

		ModelAndView mv = new ModelAndView("/environment/algorithm/recommendAlgorithmList.do");
		Algorithm result = algorithmService.selectOneAlgorithm(paramMap, request);
		logger.info("결과 : " + result);
		mv.addObject("result", result);

		mv.setViewName("jsonView");
		return mv;
	}

	// 알고리즘 수정
	@ResponseBody
	@RequestMapping(value = "/updateRecommendAlgorithm.do", method = RequestMethod.POST)
	public ModelAndView updateAlgorithm(Map<String, Object> paramMap, HttpServletRequest request,
			HttpServletResponse response) throws SQLException {

		ModelAndView mv = new ModelAndView("/environment/algorithm/recommendAlgorithmList.do");
		int result = algorithmService.updateAlgorithm(paramMap, request);

		logger.info("결과 : " + result);
		mv.addObject("result", result);

		mv.setViewName("jsonView");
		return mv;
	}
}
