package com.choroc.admin.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.choroc.admin.domain.AuthUserParam;
import com.choroc.admin.domain.LoginParam;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.service.UserService;
import com.choroc.admin.util.AesUtil;
import com.choroc.admin.util.CookieUtil;
import com.choroc.admin.util.SHAPasswordEncoder;
import com.choroc.admin.util.StringUtil;
import com.choroc.admin.util.ZValue;
import com.choroc.admin.web.AbstractController;

/**
 * TODO > 로그인 컨트롤러
 * 파일명   > LoginController.java
 * 작성자   > CCmedia Service Corp.
 * 작성일   > 2018. 11. 9.
 */

@Controller
@RequestMapping(value = "/login")
@PropertySource("classpath:application-context.properties")
public class LoginController extends AbstractController {

	@Autowired
	private UserService userService;

	@Autowired
	private Environment env;

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	/**
	 * 로그인 화면
	 * @param param
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/login.do" , method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView getLoginForm(@ModelAttribute("login") LoginParam param, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mav = new ModelAndView("login/loginForm");

		String loginCookie   = null;

		if(param.getPageSize() == 0) {
			param.setPageSize(Integer.parseInt(env.getProperty("property.pagination.pagesize")));
		}
		if(param.getRecordCountPerPage() == 0) {
			param.setRecordCountPerPage(Integer.parseInt(env.getProperty("property.pagination.recordcountperpage")));
		}

		String cookieName = env.getProperty("choroc.cookie.key", "");
		String cookiePath = env.getProperty("choroc.cookie.path", "/");
		String filterCookie = null;
		try {
			// 쿠키명으로 쿠키값 취득
			loginCookie = CookieUtil.getCookie(request, cookieName);
			if(!StringUtils.isEmpty(loginCookie)) {
				filterCookie = loginCookie.replaceAll("\r","").replaceAll("\n","");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 쿠키값이 있으면 쿠키설정하여 jsp로 전달
		if (!StringUtil.isEmpty(loginCookie)) {
			try {
				Cookie cookie = new Cookie(cookieName, filterCookie);
				cookie.setPath(cookiePath);
				int t = Integer.parseInt("86400"); // 쿠키저장기간 30일 (60*60*24=86400) 1일*30일
				cookie.setMaxAge(t);
				cookie.setSecure(true);
				response.addCookie(cookie);
			} catch(Exception e)	{
				e.printStackTrace();
			}
		}
		modelMap.addAttribute("systemMsg", null);
		
		modelMap.addAttribute("ossvi", param.getOssvi());
		modelMap.addAttribute("ossts", param.getOssts());
		modelMap.addAttribute("ossct", param.getOssct());
		modelMap.addAttribute("ossdi", param.getOssdi());
		modelMap.addAttribute("ossdwp", param.getOssdwp());
		
		return mav;
	}

	/**
	 * 로그인 처리
	 * @param param
	 * @param redirectAttr
	 * @param result
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/loginProcess.do" , method = RequestMethod.POST)
	public String loginProcess( @ModelAttribute("login") LoginParam param, RedirectAttributes redirectAttr, ModelMap modelMap,
			                    HttpServletRequest request, HttpServletResponse response, Model model) {

		Date now = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.KOREA);
		String userLoginTime = simpleDateFormat.format(now);

		AuthUser user = null;
		String redirectUrl = null;
		String systemMsg = "시스템 에러가 발생하였습니다. 관리자에게 문의 후 다시 이용해주시기 바랍니다.";

		Date lastLoginSuccesDt = null;
		int lastLoginDiffDate = 0;

		// 패스워드 암호화
	    SHAPasswordEncoder shaPasswordEncoder = new SHAPasswordEncoder(256);
		shaPasswordEncoder.setEncodeHashAsBase64(true);
	    param.setLoginUserPassword(shaPasswordEncoder.encode(param.getLoginUserPassword()));
	    param.setLoginUserId(param.getLoginUserId());

		HttpSession session = request.getSession();
		
		if(param.getUser_opt() != null && !param.getUser_opt().equals("") && param.getUser_opt().equals("SSO")){
			String salt = param.getOssts();
			String iv = param.getOssvi();
			String encLoginUserId = param.getLoginUserId();
			String encLoginUserPassword = param.getLoginUserPassword();
			
			AesUtil aesUtil = new AesUtil();
//            String encryptId = aesUtil.encrypt(salt, iv, plaintext);
//            System.out.println("encryptId : " + encryptId);
            
            try {
            	
				String decryptId 	= aesUtil.decrypt(salt, iv, encLoginUserId);
				param.setLoginUserId(decryptId);
//				param.setLoginUserId(param.getLoginUserId());
//				param.setLoginUserPassword(decryptPwd);
//				param.setLoginUserPassword(encLoginUserPassword);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

		// 로그인 유저 조회
		user = userService.getLoginUser(param);

		if(user != null) {
			model.addAttribute("userVO", user); // 아이디로 조회한 사용자정보 세션
		} else {
			systemMsg = "아이디가 존재하지 않습니다.";
			modelMap.addAttribute("systemMsg", systemMsg);
			return "login/loginForm";
		}

		if(user.getUserId() != null && user.getUserId().length() > 0) {			
			try {
				String acntLockYn = user.getAcntLockYn();          // 계정 잠금 유무
				int acntLockDiffDate = user.getAcntLockDiffDate(); // 계정이 잠기고 경과한 시간(분)

				// 사용자 계정이 잠겼을 경우
				if(acntLockYn.equals("Y")) {
					// 계정이 잠긴지 30분 이전이면 로그인 페이지로 이동
					if(acntLockDiffDate <= 30) {
						//systemMsg = "비밀번호를 5회 이상 잘못 입력하여 계정이 잠겨 있습니다. 30분후 다시 이용해주시기 바랍니다.";
						systemMsg = "비밀번호가 5회 이상 틀렸습니다. 30분간 시스템에 로그인 할수 없습니다."; // "[계정잠금 일시:" + strAcntLockDt + "]";
						session.removeAttribute("tempInfo");
						session.invalidate();
						modelMap.addAttribute("systemMsg", systemMsg);
						logger.info(">>> 계정이" + acntLockDiffDate + " 분 동안 잠겨 있습니다. >>>");
						return "login/loginForm";
					}
				}
				
				if(!param.getUser_opt().equals("SSO")){
					// 비밀번호 입력오류 처리.
					if(!param.getLoginUserPassword().equals(user.getUserPassword())) {

						int loginFailCount = user.getLoginFailrDtCo(); // 비밀번호 실패 횟수조회
						loginFailCount ++; 							   // 비밀번호 틀린 횟수 갱신
						logger.info("### 비밀번호 오류 횟수 : " + loginFailCount + " ###");

						ZValue zvl = new ZValue();
						zvl.put("userId", user.getUserId());
						zvl.put("loginFailCount", loginFailCount);

						userService.loginFaileCount(zvl); // 비밀번호 오류 횟수 업데이트

						// 비밀번호 5회이상 입력 오류시 30분간 로그인 중지 처리.
						if(loginFailCount >= 5) {
						   systemMsg = "비밀번호가 5회 이상 틀렸습니다. 30분간 시스템에 로그인 할수 없습니다.";
						   userService.acntLockLimitExpirationProcess(zvl); // 로그인 실패 계정 잠금처리
						   logger.info("### 비밀번호가 5회 이상 틀렸습니다. 30분간 시스템에 로그인 할수 없습니다. ###");
						}
						// 비밀번호 입력 오류 처리.
						else {
							systemMsg = "비밀번호가 틀렸습니다." + " [오류횟수:" + loginFailCount + "]" ;
							logger.info(systemMsg);
						}

						session.removeAttribute("tempInfo");
						session.removeAttribute("userInfo");
						session.invalidate();
						modelMap.addAttribute("systemMsg", systemMsg);

						return "login/loginForm";
					}					
				}
				
				


					logger.info("################################################################################################");
					logger.info("#                                                                                              #");
					logger.info("#                                                                                              #");
					logger.info("#                                                                                              #");
					logger.info("#                                            LOG IN                                            #");
					logger.info("#                                                                                              #");
					logger.info("#                                                                                              #");
					logger.info("#                                                                                              #");
					logger.info("################################################################################################");

					if(user.getLastLoginSuccesDt() != null) {
						lastLoginSuccesDt = user.getLastLoginSuccesDt();  // 최종 로그인 날짜
						lastLoginDiffDate  = user.getLastLoginDiffDate();  // 최종 로그인후 경과개월
					}

					session.setAttribute("login", user);
					session.setAttribute("userLoginInfo", user);
					session.setAttribute("userLoginTime", userLoginTime); // 로그인 시간

					AuthUserParam authUserParam = new AuthUserParam();
					authUserParam.setAuthUser(user);
					session.setAttribute("userInfo", user);  // 세션명 변경 필요 .. LoginUser
					session.removeAttribute("tempInfo");

					String userId = user.getUserId();
					String conectIp = request.getRemoteHost();
					ZValue zvl = new ZValue();
					zvl.put("userId", userId);
					zvl.put("conectIp", conectIp);

					// 최종 로그인 이후 3개월 경과시 처리.
					if(lastLoginDiffDate >= 3) {
						userService.loginSuccessProcessExcludeLoginDt(zvl);
						logger.info(">>>>>>>>>> 최종 로그인 이후 3개월 경과시 처리. ");
					}
					else {
						logger.info(">>>>>>>>>> 3개월이내 로그인 기록 존재");
						userService.loginSuccessProcess(zvl); // 로그인 성공 정보 업데이트
					}

				    redirectUrl = "redirect:/index/adminDashboard.do"; // 메인창으로 이동
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			String passwordResetYn = user.getPasswordResetYn();    // 비번 변경 유무
			int lastPasswdDiffDate = user.getLastPasswdDiffDate(); // 비번 변경 경과 날짜
			logger.info("### 최종 로그인  접속날짜: " + user.getLastLoginSuccesDt());
			logger.info("### 계정 잠김 처리 유무   : " + user.getAcntLockYn());
			logger.info("### 계정 잠김 경과 (분) : " + user.getAcntLockDiffDate());
			logger.info("### 계정 휴면 처리 유무   : " + user.getDrmncyYn());
			logger.info("### 최종 비번 변경 날짜   : " + user.getLastPasswdChgDt());
			logger.info("### 비번 변경 경과 날짜   : " + user.getLastPasswdDiffDate());
			logger.info("********** 비번 변경 유무        : " + passwordResetYn);
			logger.info("********** 비번 변경 경과개월 : " + lastPasswdDiffDate);
			logger.info("********** 최종 로그인 날짜     : " + lastLoginSuccesDt);
			logger.info("********** 최종 로그인후 경과개월     : " + lastLoginDiffDate);

			
			redirectUrl = "redirect:/index/adminDashboard.do"; // 메인창으로 이동
//			// 최초 시스템에 로그인 후  비밀번호 수정화면으로 이동.
//			if( lastLoginSuccesDt == null || "".equals(lastLoginSuccesDt.toString()) || "Y".equals(passwordResetYn.toString())) {
//				// 사용자 최초로 로그인 할 경우
//				redirectAttr.addAttribute("userId", user.getUserId());
//				redirectAttr.addAttribute("changePasswdMsg", "firstLogin");
//				redirectUrl = "redirect:/index/firstLoginProcess.do";
//			}
//
//			// 3개월 동안 비밀번호 변경 없는경우 비밀번호 수정화면으로 이동.
//			if(lastPasswdDiffDate >= 3 ) {
//				redirectAttr.addAttribute("changePasswdMsg", "overDatePw");
//				redirectUrl = "redirect:/index/firstLoginProcess.do";
//			}
//
//			// 마지막 로그인 후 3개월 경과시 비밀번호 수정화면으로 이동
//			if(lastLoginDiffDate >=3) {
//				redirectAttr.addAttribute("changePasswdMsg", "overDateLogin");
//				redirectUrl = "redirect:/index/firstLoginProcess.do";
//			}

			return redirectUrl;
	}


	/**
	 * 로그아웃 처리
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/logout.do", method = RequestMethod.POST)
	public String logout( HttpServletRequest request) {

		HttpSession session = request.getSession();
		session.removeAttribute("userInfo");
		session.removeAttribute("login");
		session.removeAttribute("userLoginInfo");
		session.removeAttribute("userLoginTime");
		session.invalidate();

		logger.info("################################################################################################");
		logger.info("#                                                                                              #");
		logger.info("#                                                                                              #");
		logger.info("#                                                                                              #");
		logger.info("#                                            LOG OUT                                           #");
		logger.info("#                                                                                              #");
		logger.info("#                                                                                              #");
		logger.info("#                                                                                              #");
		logger.info("################################################################################################");

		return "redirect:/login/login.do";
	}

}