package com.choroc.admin.dao;

import java.sql.SQLException;

import com.choroc.admin.domain.AuthUserParam;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.util.ZValue;

/**
 * TODO  >
 * 파일명   > UserDao.java
 * 작성일   > 2018. 11. 20.
 * 작성자   > Copyright CCmediaService
 * 수정일   > 
 * 수정자   > 
 */
public interface UserDao {
	
	// 로그인 아이디로 DB에 저장된 유저정보 조회 
	public AuthUser selectLoginUser(AuthUserParam param) throws SQLException;
	
	// 로그인 성공시 정보 업데이트
	public void loginSuccessProcess(ZValue zvl) throws SQLException;
	
	 // 로그인 성공시 정보 업데이트
	public void loginSuccessProcessExcludeLoginDt(ZValue zvl) throws SQLException;
	
	// 로그인 실패횟수 업데이트
	public int loginFaileCount (ZValue zvl) throws SQLException;
	
	
	
	// 계정 잠금 및 실패 횟수 초기화
	public int acntLockLimitExpirationProcess(ZValue zvl) throws SQLException;
	
/*	public String selectEncryptedPassword(AuthUserParam param)  throws SQLException;

	public int loginFaileProcess(ZValue zvl) throws SQLException;

	public String selectUserIdLockYn(ZValue zvl) throws SQLException;

	public int loginSuccessProcess(ZValue zvl) throws SQLException;

	

	public List<AuthUser> selectLockMember() throws SQLException;

	public int updateLockMember(AuthUser param) throws SQLException;*/
}
