package com.choroc.admin.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MySqlBaseDao {

	private static Logger log = LoggerFactory.getLogger(MySqlBaseDao.class);
	
	public static final String DATASOURCE_PLATFORM = "platform";

	public Object queryForList(String sqlId, SqlSessionTemplate sqlMapClient) throws SQLException {
		Object ret = sqlMapClient.selectList(sqlId);
		
		return ret;
		
	}
	
	public Object queryForObject(String sqlId, SqlSessionTemplate sqlMapClient) throws SQLException {
		Object ret = sqlMapClient.selectOne(sqlId);
		
		return ret;
	}
	
	public Object queryForList(String sqlId, Object params, SqlSessionTemplate sqlMapClient) throws SQLException {
		Object ret = sqlMapClient.selectList(sqlId, params);

		return ret;

	}

	public Object queryForObject(String sqlId, Object params, SqlSessionTemplate sqlMapClient) throws SQLException {
		Object ret = sqlMapClient.selectOne(sqlId, params);

		return ret;
	}

	public Object queryForList(String sqlId, Object params, String domain, SqlSessionTemplate sqlMapClient) throws SQLException {
		Object ret = sqlMapClient.selectList(sqlId, params);
		
		return ret;
	}
	
	public Object queryForObject(String sqlId, Object params, String domain, SqlSessionTemplate sqlMapClient) throws SQLException {
		Object ret = sqlMapClient.selectOne(sqlId, params);

		return ret;
	}


	@Deprecated
	public int insert(String sqlId, Object params, String domain, String seqId, SqlSessionTemplate sqlMapClient) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		int returnValue = 0;	
		
		returnValue = (int) sqlMapClient.insert(sqlId, params);
	
		if(returnValue > 0 && StringUtils.isNotEmpty(domain)){
			if(sqlMapClient != null){
				if(params instanceof HashMap){
					((HashMap)params).put(seqId, returnValue);
				}else if(StringUtils.isNotEmpty(seqId)){
					Class<? extends Object> clazz = params.getClass();
					String setMethodName = "set" + seqId.substring(0, 1).toUpperCase() + seqId.substring(1, seqId.length());
					Method method = clazz.getMethod(setMethodName, new Class[] {int.class});
					method.invoke(params, new Object[]{returnValue});					
				
					sqlMapClient.insert(sqlId, params);
				}
			}else{
				throw new IllegalArgumentException("Unknown Domain!!");
			}	
		}

		return returnValue;		
	}
	
	
	/**
	 * TRANSACTION 적용을 위한 Insert. 	
	 */
	public int add(String sqlId, Object params, String domain, String seqId, Integer referenceId, SqlSessionTemplate sqlMapClient) throws SQLException{
		try
		{
			if(referenceId == null)
			{
				int ret = (int) sqlMapClient.insert(sqlId, params);
				
				return ret;
			}
			else
			{	
				if(referenceId > 0 && StringUtils.isNotEmpty(domain)){

					if(sqlMapClient != null){
						if(params instanceof HashMap){
							((HashMap)params).put(seqId, referenceId.intValue());
						}else if(StringUtils.isNotEmpty(seqId)){
							Class<? extends Object> clazz = params.getClass();
							String setMethodName = "set" + seqId.substring(0, 1).toUpperCase() + seqId.substring(1, seqId.length());
							Method method = clazz.getMethod(setMethodName, new Class[] {int.class});
							method.invoke(params, new Object[]{referenceId.intValue()});					
						
							sqlMapClient.insert(sqlId, params);
						}
					}else{
						throw new IllegalArgumentException("Unknown Domain!!");
					}	
				}
			}
		}catch(Exception e)
		{
			throw new SQLException(e);
		}
	
		
		return 0;		
	}

	public int addWithoutSeq(String sqlId, Object params, String domain, Integer referenceId, SqlSessionTemplate sqlMapClient) throws SQLException{

		int returnValue = 0;

		try
		{
			if(referenceId == null)
			{
				sqlMapClient.insert(sqlId, params);
			}
			else
			{	
				if(referenceId > 0 && StringUtils.isNotEmpty(domain)){
					if(sqlMapClient != null){

						returnValue = sqlMapClient.insert(sqlId, params);

						return returnValue;
					}else{
						throw new IllegalArgumentException("Unknown Domain!!");
					}	
				}
			}
		}catch(Exception e)
		{
			throw new SQLException(e);
		}
	
		
		return 0;		
	}

	@Deprecated
	public int update(String sqlId, Object params, String domain, SqlSessionTemplate sqlMapClient) throws SQLException{
		int returnValue = 0;

		returnValue = sqlMapClient.update(sqlId, params);

		if(returnValue > 0 && StringUtils.isNotEmpty(domain)){
			if(sqlMapClient != null){
				returnValue += sqlMapClient.update(sqlId, params);
			}else{
				throw new IllegalArgumentException("Unknown Domain!!");
			}
		}

		return returnValue;
	}

	public int update(String sqlId, Object params, SqlSessionTemplate sqlMapClient) throws SQLException{
		int returnValue = 0;

		returnValue = sqlMapClient.update(sqlId, params);

		return returnValue;
	}
	
	/**
	 * 해당 도메인에만 업데이트 할경우
	 * 개요:
	 * @Method Name : updateWithDomain
	 * @history
	 * ---------------------------------------------------------------------------------
	 *  변경일                    작성자                    변경내용
	 * ---------------------------------------------------------------------------------
	 *  2015. 3. 4.     Taejin Son<son@bluedigm.com>      최초 작성   
	 * ---------------------------------------------------------------------------------
	 * @param sqlId
	 * @param params
	 * @param domain
	 * @return
	 * @throws SQLException
	 */
	
	public int updateWithDomain(String sqlId, Object params, String domain, SqlSessionTemplate sqlMapClient) throws SQLException{
		int returnValue = 0;
		
		if(StringUtils.isNotEmpty(domain)){
			if(sqlMapClient != null){
				returnValue = sqlMapClient.update(sqlId, params);
			}else{
				throw new IllegalArgumentException("Unknown Domain!!");
			}
			
		}
		return returnValue;		
	}
	
	/**
	 * 
	 * TRANSACTION 적용을 위한 Update. 
	 * 
	 */
	public int update(String sqlId, Object params, String domain, Integer referenceId, SqlSessionTemplate sqlMapClient) throws SQLException{
		
		if(referenceId == null)
		{			
			int ret = sqlMapClient.update(sqlId, params);
			
			return ret;
		}
		else
		{
			if(referenceId.intValue() > 0 && StringUtils.isNotEmpty(domain)){
				if(sqlMapClient != null){
					referenceId += sqlMapClient.update(sqlId, params);
				}else{
					throw new IllegalArgumentException("Unknown Domain!!");
				}
				
				return referenceId;
			}
		}

		return 0;		
	}
	
	public int updateWithoutSeq(String sqlId, Object params, String domain, Integer referenceId, SqlSessionTemplate sqlMapClient) throws SQLException{
		if(referenceId == null)
		{			
			sqlMapClient.update(sqlId, params);
		}
		else
		{
			if(referenceId.intValue() > 0 && StringUtils.isNotEmpty(domain)){
				if(sqlMapClient != null){
					referenceId += sqlMapClient.update(sqlId, params);
				}else{
					throw new IllegalArgumentException("Unknown Domain!!");
				}
				
				return referenceId;
			}
		}

		return 0;		
	}
	
	public int insert(String sqlId, Object params, String domain, SqlSessionTemplate sqlMapClient) throws SQLException{
		return update(sqlId, params, domain, sqlMapClient);
	}

	public int insert(String sqlId, Object params, SqlSessionTemplate sqlMapClient) throws SQLException{
		int returnValue;
		returnValue = sqlMapClient.insert(sqlId, params);

		return returnValue;
	}

	public int delete(String sqlId, Object params, String domain, SqlSessionTemplate sqlMapClient) throws SQLException{
		return update(sqlId, params, domain, sqlMapClient);
	}

	public int delete(String sqlId, Object params, String domain, Integer referenceId, SqlSessionTemplate sqlMapClient) throws SQLException{
		return update(sqlId, params, domain, referenceId, sqlMapClient);
	}

	public int delete(String sqlId, Object params, SqlSessionTemplate sqlMapClient) throws SQLException{
		return update(sqlId, params, sqlMapClient);
	}

	public int deleteWithoutSeq(String sqlId, Object params, String domain, Integer referenceId, SqlSessionTemplate sqlMapClient) throws SQLException{
		if(referenceId == null)
		{			
			sqlMapClient.delete(sqlId, params);
		}
		else
		{
			if(referenceId.intValue() > 0 && StringUtils.isNotEmpty(domain)){
				if(sqlMapClient != null){
					referenceId += sqlMapClient.delete(sqlId, params);
				}else{
					throw new IllegalArgumentException("Unknown Domain!!");
				}
				
				return referenceId;
			}
		}

		return 0;		
	}
	
}
