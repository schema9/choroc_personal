package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.choroc.admin.domain.RecommendAreaParam;
import com.choroc.admin.dto.Algorithm;
import com.choroc.admin.dto.RecommendAreaInfo;

public interface RecommendAreaDao {

	// 리스트
	public List<RecommendAreaInfo> getRecommendAreaList(RecommendAreaParam param) throws SQLException;

	public int getRecommendAreaCount(RecommendAreaParam param) throws SQLException;

	// 등록
	public List<Algorithm> getAlgorithmList() throws SQLException;
	
	public int checkRecommendArea(RecommendAreaParam param) throws SQLException;

	public int insertRecommendArea(RecommendAreaParam param) throws SQLException;

	// 수정
	public RecommendAreaInfo selectOneRecommendArea(RecommendAreaParam param) throws SQLException;

	public int updateRecommendArea(RecommendAreaParam param) throws SQLException;

}
