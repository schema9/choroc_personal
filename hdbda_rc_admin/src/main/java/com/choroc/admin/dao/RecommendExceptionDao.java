package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.choroc.admin.domain.RecommendExceptionParam;
import com.choroc.admin.dto.RecommendException;

public interface RecommendExceptionDao {

	// 추천제외관리 리스트
	public List<RecommendException> selectExceptionList(RecommendExceptionParam param) throws SQLException;

	// 추천제외관리 리스트 총 개수
	public int selectExceptionTotalCount(RecommendExceptionParam param) throws SQLException;

	// 추천제외관리 중복 체크
	public int checkException(RecommendExceptionParam param) throws SQLException;

	// 추천제외관리 등록
	public int insertException(List<RecommendException> param) throws SQLException;

	// 추천제외관리 수정할 데이터 한건 가져오기
	public RecommendException selectOneException(RecommendExceptionParam param) throws SQLException;

	// 추천제외관리 수정
	public int updateException(RecommendExceptionParam param) throws SQLException;

	// 추천제외관리 삭제
	public int deleteException(RecommendException param) throws SQLException;

}
