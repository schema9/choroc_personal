package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.domain.SystemManagerMultiParam;
import com.choroc.admin.domain.SystemManagerParam;
import com.choroc.admin.dto.ServiceInfo;
import com.choroc.admin.dto.SystemManagerInfo;

public interface AreaPreviewDao {
    public List<SystemManagerParam> selectRecoAreaRow(SystemManagerParam param) throws SQLException;
    public int selectRecoAreaTotalCount(SystemManagerParam param) throws SQLException;
}
