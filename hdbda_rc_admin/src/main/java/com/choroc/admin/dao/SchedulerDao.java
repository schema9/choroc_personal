package com.choroc.admin.dao;

import java.util.List;

import com.choroc.admin.domain.SchedulerParam;
import com.choroc.admin.dto.Scheduler;
import com.choroc.admin.dto.SchedulerTask;

/**
 * TODO > 스케줄러 DAO
 * 파일명   > SchedulerDao.java
 * 작성일   > 2018. 12. 4.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public interface SchedulerDao {

	/* 스케줄러 목록 */
	public List<Scheduler> selectListRows(SchedulerParam schedulerParam) throws Exception;
	
	/* 스케줄러 레코드 총갯수 */
	public int selectListTotalCount(SchedulerParam schedulerParam) throws Exception;
	
	/* 스케줄러 등록 */
	public int insertRow(Scheduler scheduler) throws Exception;
	
	/* 스케줄러 조회 */
	public Scheduler selectOneRow(int job_id);
	
	/*스케줄러 수정*/
	public int updateRow(Scheduler scheduler) throws Exception;
	
	/* 잡 수동 실행 */
	public int insertRedo(SchedulerTask task) throws Exception;
	
	/* 스케줄러 태스크 기록 수정 */
	public int updateJobStatus(SchedulerTask task) throws Exception;
	
	/* 잡 삭제 */
	public int deleteAll(Scheduler scheduler) throws Exception;
	
	/* 에이전트 리스트 조회 */
	public List<Scheduler> selectAgentList() throws Exception;
	
	/*카프카 전송을 위한 에이전트 정보 가져오기 */
	public Scheduler getAgentInfo(Scheduler scheduler) throws Exception;
	
	/* 메타 테이블 조회 */
	public Scheduler getMetaTable(Scheduler scheduler) throws Exception;
	
	/* 해당 시스템 명의 ETL 리스트 가져오기 */
	public List<Scheduler> etlList(Scheduler scheduler) throws Exception;
	
}
