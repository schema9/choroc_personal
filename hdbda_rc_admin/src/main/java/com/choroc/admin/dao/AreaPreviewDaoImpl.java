package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.domain.SystemManagerMultiParam;
import com.choroc.admin.domain.SystemManagerParam;
import com.choroc.admin.dto.AccountInfo;
import com.choroc.admin.dto.SystemManagerInfo;

@Repository
public class AreaPreviewDaoImpl implements AreaPreviewDao{

	@Inject
	private SqlSession sqlSession;	
	
	private static Logger log = LoggerFactory.getLogger(AreaPreviewDaoImpl.class);
	
	@Override
	public List<SystemManagerParam> selectRecoAreaRow(SystemManagerParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.selectList("environment.area.areaPreview.selectRecoAreaRow", param);
	}
	
	@Override
	public int selectRecoAreaTotalCount(SystemManagerParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("environment.area.areaPreview.selectRecoAreaTotalCount", param);
	}

}
