package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.domain.SystemManagerMultiParam;
import com.choroc.admin.domain.SystemManagerParam;
import com.choroc.admin.dto.Scheduler;
import com.choroc.admin.dto.ServiceInfo;
import com.choroc.admin.dto.SystemManagerInfo;

/**
 * TODO > 권한 관리 > 사용자 관리 > 사용자 계정 관리
 * 파일명   > UserAccountManagementDao.java
 * 작성일   > 2018. 11. 21.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public interface SystemManagerDao {
    public List<SystemManagerInfo> selectSystemManagerRows(SystemManagerParam param) throws SQLException;
    public int selectSystemManagerTotalCount(SystemManagerParam param) throws SQLException;
	public int insertMetaDataTable(SystemManagerParam param) throws SQLException;
	public int insertMetaDataColumnRows(Map<String, Object> listMap) throws SQLException;
	public int updateMetaDataTable(SystemManagerParam param) throws SQLException;
	public int updateMetaDataColumnRows(Map<String, Object> listMap) throws SQLException;
	public int deleteMetaDataTable(SystemManagerParam param) throws SQLException;
	public int deleteMetaDataColumn(Map<String, Object> listMap) throws SQLException;
	public int deleteMetaDataColumnRows(SystemManagerParam param) throws SQLException;
	public List<SystemManagerInfo> selectMetaDataDetail(SystemManagerParam param) throws SQLException;
	
	public int jobLstChgDatCondNull(String etl_tgt_tbl) throws SQLException;
	public Scheduler jobInfo(String etl_tgt_tbl) throws SQLException;
}
