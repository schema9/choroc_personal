package com.choroc.admin.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.SchedulerParam;
import com.choroc.admin.dto.Scheduler;
import com.choroc.admin.dto.SchedulerTask;

/**
 * TODO >
 * 파일명   > SchedulerDaoImpl.java
 * 작성일   > 2018. 12. 4.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
@Repository
public class SchedulerDaoImpl implements SchedulerDao {

	@Inject
	private SqlSession sqlSession;
	private static final String namespace = "system.SchedulerMapper";
	
	/* 스케줄러 목록 */
	@Override
	public List<Scheduler> selectListRows(SchedulerParam schedulerParam) throws Exception {
		return sqlSession.selectList(namespace + ".selectListRows", schedulerParam);
	}

	/* 스케줄러 레코드 총갯수 */
	@Override
	public int selectListTotalCount(SchedulerParam schedulerParam) throws Exception {
		return sqlSession.selectOne(namespace + ".selectListTotalCount", schedulerParam);
	}

	/* 스케줄러 등록 */
	@Override
	public int insertRow(Scheduler scheduler) throws Exception {
		return sqlSession.insert(namespace + ".insertRow", scheduler);
	}

	/* 스케줄러 조회 */
	@Override
	public Scheduler selectOneRow(int job_id) {
		return sqlSession.selectOne(namespace + ".selectOneRow", job_id);
	}

	/* 스케줄러 수정 */
	@Override
	public int updateRow(Scheduler scheduler) throws Exception {
		return sqlSession.update(namespace + ".updateRow", scheduler);
	}
	
	/* 잡 수동 실행 */
	@Override
	public int insertRedo(SchedulerTask task) throws Exception {
		return sqlSession.insert(namespace + ".insertTaskInfo", task);
	}
	
	/*  태스크 기록 수정 */
	@Override
	public int updateJobStatus(SchedulerTask task) throws Exception {
		return sqlSession.update(namespace + ".updateJobStatus", task); 
	}
	
	@Override
	public int deleteAll(Scheduler scheduler) throws Exception {
		return sqlSession.delete(namespace + ".delete", scheduler); 
	}
	
	@Override
	public List<Scheduler> selectAgentList() throws Exception {
		return sqlSession.selectList(namespace + ".agentList");
	}
	
	@Override
	public Scheduler getAgentInfo(Scheduler param) throws Exception{
		return sqlSession.selectOne(namespace + ".getAgentInfo", param);
	}
	
	@Override
	public Scheduler getMetaTable(Scheduler param) throws Exception{
		return sqlSession.selectOne(namespace+".getMetaTable", param);
	}
	
	@Override
	public List<Scheduler> etlList(Scheduler scheduler) throws Exception{
		return sqlSession.selectList(namespace+".etlList", scheduler);
	}
}
