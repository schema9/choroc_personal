package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.choroc.admin.domain.AlgorithmParam;
import com.choroc.admin.dto.Algorithm;

public interface AlgorithmDao {

	// 알고리즘탭 리스트
	public List<Algorithm> getAlgorithmList(AlgorithmParam param) throws SQLException;

	int selectAlgorithmTotalCount(AlgorithmParam param) throws SQLException;

	// 알고리즘 중복 체크
	int checkAlgorithm(Map<String, Object> paramMap) throws SQLException;

	// 알고리즘 추가
	int insertAlgorithm(Map<String, Object> paramMap) throws SQLException;

	// 수정할 알고리즘 1건 불러오기
	public Algorithm selectOneAlgorithm(Map<String, Object> paramMap) throws SQLException;

	// 알고리즘 수정
	int updateAlgorithm(Map<String, Object> paramMap) throws SQLException;
	
}
