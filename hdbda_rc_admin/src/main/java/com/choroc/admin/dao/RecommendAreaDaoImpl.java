package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.RecommendAreaParam;
import com.choroc.admin.dto.Algorithm;
import com.choroc.admin.dto.RecommendAreaInfo;

/**
 * TODO > 파일명 > RecommendAreaDaoImpl.java 작성일 > 2018. 11. 22. 작성자 > CCmedia
 * Service Corp. 수정일 > 2018.12.10 수정자 > 이동환 사원
 */
@Repository
public class RecommendAreaDaoImpl implements RecommendAreaDao {

	@Inject
	private SqlSession sqlSession;

	private static final String namespace = "environment.area.RecommendAreaMapper";

	@Override
	public List<RecommendAreaInfo> getRecommendAreaList(RecommendAreaParam param) throws SQLException {
		return sqlSession.selectList(namespace + ".getRecommendAreaList", param);
	}

	@Override
	public int getRecommendAreaCount(RecommendAreaParam param) throws SQLException {
		return sqlSession.selectOne(namespace + ".getRecommendAreaCount", param);
	}

	@Override
	public List<Algorithm> getAlgorithmList() throws SQLException {
		return sqlSession.selectList(namespace + ".getAlgorithmList");
	}

	@Override
	public int checkRecommendArea(RecommendAreaParam param) throws SQLException {
		return sqlSession.selectOne(namespace + ".checkRecommendArea", param);
	}

	@Override
	public int insertRecommendArea(RecommendAreaParam param) throws SQLException {
		return sqlSession.insert(namespace + ".insertRecommendArea", param);
	}

	@Override
	public RecommendAreaInfo selectOneRecommendArea(RecommendAreaParam param) throws SQLException {
		return sqlSession.selectOne(namespace + ".selectOneRecommendArea", param);
	}

	@Override
	public int updateRecommendArea(RecommendAreaParam param) throws SQLException {
		return sqlSession.update(namespace + ".updateRecommendArea", param);
	}

}
