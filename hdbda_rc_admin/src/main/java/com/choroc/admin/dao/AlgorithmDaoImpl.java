package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.AlgorithmParam;
import com.choroc.admin.dto.Algorithm;

/**
 * TODO >
 * 파일명   > AlgorithmDaoImpl.java
 * 작성일   > 2018. 11. 22.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
@Repository
public class AlgorithmDaoImpl implements AlgorithmDao {

	@Inject
	private SqlSession sqlSession;
	
	private static final String namespace = "environment.AlgorithmMapper";
	
	@Override
	public List<Algorithm> getAlgorithmList(AlgorithmParam param) throws SQLException {
		return sqlSession.selectList(namespace + ".selectAlgorithmList", param);
	}
	
	@Override
	public int selectAlgorithmTotalCount(AlgorithmParam param) throws SQLException {
		return sqlSession.selectOne(namespace + ".selectAlgorithmTotalCount", param);
	}
	
	@Override
	public int checkAlgorithm(Map<String,Object> paramMap) throws SQLException{
		return sqlSession.selectOne(namespace + ".checkAlgorithm", paramMap);
	}
	
	@Override
	public int insertAlgorithm(Map<String, Object> paramMap) throws SQLException{
		return sqlSession.insert(namespace + ".insertAlgorithm", paramMap);
	}
	
	@Override
	public Algorithm selectOneAlgorithm(Map<String, Object> paramMap) throws SQLException{
		return sqlSession.selectOne(namespace + ".selectOneAlgorithm", paramMap); 
	}
	
	@Override
	public int updateAlgorithm(Map<String, Object> paramMap) throws SQLException {
		return sqlSession.update(namespace + ".updateAlgorithm", paramMap);
	}

}
