package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.choroc.admin.domain.AuthUserParam;
import com.choroc.admin.domain.MenuParam;
import com.choroc.admin.dto.Menu;
import com.choroc.admin.dto.MenuInfo;
import com.choroc.admin.dto.MenuMap;
import com.choroc.admin.util.ZValue;


/**
 * TODO  >
 * 파일명   > MenuDao.java
 * 작성일   > 2018. 11. 21.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public interface MenuDao {

	// 메뉴리스트
	public List<ZValue> selectMenuList(ZValue zvl) throws Exception;
	
	// 사용자 권한별 메뉴
	public List<Menu> selectMyTopMenus(AuthUserParam param) throws SQLException;
	
	// 전체 메뉴리스트 
	public List<MenuInfo> selectMenuRows(MenuParam param) throws SQLException;
	
	// 전체 메뉴 갯수
	public int selectMenuTotalCount(MenuParam param) throws SQLException;
	
	// 유효한 모든 메뉴
	public List<MenuInfo> selectAllMenu() throws SQLException;
	
	/*선택한 상위 메뉴포함 목록*/
	public List<MenuInfo> selectAll2Menu(ZValue zvl) throws SQLException;
	
	// 권한리스트
	public List<ZValue> retrieveAuthorList() throws Exception;
	
	// 메뉴 등록
	public int insertMenu(ZValue zvl) throws Exception;
	
	// 메뉴 목록
	public ZValue selectMenu(ZValue zvl) throws Exception;
	
	// new 상위메뉴권한 체크 
	public List<ZValue> selectDistinctAuthorId(ZValue zvl) throws Exception;
	
	// 상위메뉴 ID
	public int selectUpperMenuId(ZValue zvl) throws Exception;
	
	// new 메뉴 수정
	public int updateMenu(ZValue zvl) throws Exception;
	
	// 메뉴삭제
	public void deleteMenu(int deleteMenuId) throws Exception;
	
	// 메뉴조회
	public Menu selectOneRow(int seq);
	
	// new 메뉴권한
	public List<MenuMap> selectAuthorId(int seq) throws Exception;
	
	// new 메뉴맵핑 삭제
	public void deleteAuthorMenuMap(ZValue zvl) throws Exception;
	
	// new 메뉴 권한 메핑 등록
	public int insertAuthorMenuMap(ZValue zvl) throws Exception;
}
