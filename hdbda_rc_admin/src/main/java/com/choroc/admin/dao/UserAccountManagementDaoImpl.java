package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.dto.AccountInfo;
import com.choroc.admin.util.ZValue;

/**
 * TODO > 권한 관리 > 사용자 관리 > 사용자 계정 관리
 * 파일명   > UserAccountManagementDaoImpl.java
 * 작성일   > 2018. 11. 21.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
@Repository
public class UserAccountManagementDaoImpl implements UserAccountManagementDao{

	@Inject
	private SqlSession sqlSession;	
	
	private static Logger log = LoggerFactory.getLogger(UserAccountManagementDaoImpl.class);
	
	@Override
	public int updateFirstLoginPassword(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public List<AccountInfo> selectUserAccountRows(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.selectList("authUser.AuthUserMapper.selectUserList", param);
	}
	
	@Override
	public int selectUserAccountTotalCount(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("authUser.AuthUserMapper.selectUserTotalCount", param);
	}
	
	@Override
	public int selectAccountUserIdCountForValidKey(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("authUser.AuthUserMapper.selectAccountUserIdCount", param);
	}
	
	@Override
	public List<AccountInfo> selectAuthCd(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.selectList("authUser.AuthUserMapper.selectAuthCd", param);
	}
	
	@Override
	public int createAccountUser(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.insert("authUser.AuthUserMapper.insertUserAccount", param);
	}
	@Override
	public int createAccountUserMap(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.insert("authUser.AuthUserMapper.insertUserAccountMap", param);
	}
	@Override
	public int userPwdReset(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.update("authUser.AuthUserMapper.updateUserPwdReset", param);
	}
	@Override
	public int userModifySet(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.update("authUser.AuthUserMapper.updateUserModify", param);
	}
	@Override
	public int userModifySetMap(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.update("authUser.AuthUserMapper.updateUserModifyMap", param);
	}
	@Override
	public int selectPasswordCheck(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("authUser.AuthUserMapper.selectPasswordCheck", param);
	}
	@Override
	public int updateUserPassword(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.update("authUser.AuthUserMapper.updateUserPassword", param);
	}
	@Override
	public int updateUserPasswordHis(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.update("authUser.AuthUserMapper.updateUserPasswordHis", param);
	}
	
	@Override
	public int selectUserPwdHis(AccountParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("authUser.AuthUserMapper.selectUserPwdHis", param);
	}

	@Override
	public int modifyFirstLoginPassword(ZValue zValue) throws Exception {	
		return sqlSession.update("authUser.AuthUserMapper.modifyFirstLoginPassword", zValue);
	}
	
	// 비밀변경 변경시 이력 저장
	@Override
	public void insertUserPasswordHis(ZValue zValue) throws Exception {
		sqlSession.update("authUser.AuthUserMapper.insertUserPasswordHis", zValue);
	}
	
	
		
}
