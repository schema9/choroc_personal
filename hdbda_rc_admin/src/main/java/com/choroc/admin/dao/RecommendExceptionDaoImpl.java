package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.RecommendExceptionParam;
import com.choroc.admin.dto.RecommendException;

/**
	 * 
	 * 추천 제외 관리
	 *
	 */

@Repository
public class RecommendExceptionDaoImpl implements RecommendExceptionDao {
	
	@Inject
	private SqlSession sqlSession;
	
	private static final String namespace = "environment.exception.RecommendExceptionMapper";
	
	@Override
	public List<RecommendException> selectExceptionList(RecommendExceptionParam paramMap) throws SQLException{
		return sqlSession.selectList(namespace + ".selectExceptionList", paramMap); 
	}
	
	@Override
	public int selectExceptionTotalCount(RecommendExceptionParam paramMap) throws SQLException{
		return sqlSession.selectOne(namespace + ".selectExceptionTotalCount", paramMap);
	}
	
	@Override
	public int checkException(RecommendExceptionParam paramMap) throws SQLException{
		return sqlSession.selectOne(namespace + ".checkException", paramMap);
	}
	
	@Override
	public int insertException(List<RecommendException> param) throws SQLException{
		return sqlSession.insert(namespace + ".insertException", param);
	}
	
	@Override
	public RecommendException selectOneException(RecommendExceptionParam paramMap) throws SQLException{
		return sqlSession.selectOne(namespace + ".selectOneException", paramMap);
	}
	
	@Override
	public int updateException(RecommendExceptionParam paramMap) throws SQLException{
		return sqlSession.update(namespace + ".updateException", paramMap);
	}
	
	@Override
	public int deleteException(RecommendException paramMap) throws SQLException{
		return sqlSession.delete(namespace  + ".deleteException", paramMap);
	}
	 
	
}
