package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.AuthUserParam;
import com.choroc.admin.domain.MenuParam;
import com.choroc.admin.dto.Menu;
import com.choroc.admin.dto.MenuInfo;
import com.choroc.admin.dto.MenuMap;
import com.choroc.admin.util.ZValue;

/**
 * TODO  >
 * 파일명   > MySqlMenuDao.java
 * 작성일   > 2018. 11. 20.
 * 작성자   > Copyright CCmediaService
 */
@Repository
public class MenuDaoImpl implements MenuDao {

	@Inject
	private SqlSession sqlSession;
	
	private static final String namespace = "authorize.AuthorizeMapper";
	
	@Override
	public List<ZValue> selectMenuList(ZValue zvl) throws Exception {
		return sqlSession.selectList(namespace + ".selectMenuList", zvl);
	}	
	
	@Override
	public List<Menu> selectMyTopMenus(AuthUserParam param) throws SQLException {
		return sqlSession.selectList(namespace + ".findMyTopMenus", param);
	}

	@Override
	public List<MenuInfo> selectMenuRows(MenuParam param) throws SQLException {
		return sqlSession.selectList(namespace + ".selectMenuRows", param);
	}

	@Override
	public int selectMenuTotalCount(MenuParam param) throws SQLException {
		return sqlSession.selectOne(namespace + ".selectMenuTotalCount", param);
	}

	/*유효한 모든 메뉴 목록*/
	@Override
	public List<MenuInfo> selectAllMenu() throws SQLException {	
		return sqlSession.selectList(namespace + ".selectAllMenu");
	}
		
	/*선택한 상위 메뉴포함 목록*/
	@Override
	public List<MenuInfo> selectAll2Menu(ZValue zvl) throws SQLException {
		return sqlSession.selectList(namespace + ".selectAll2Menu", zvl);
	}

	@Override
	public List<ZValue> retrieveAuthorList() throws Exception {
		return sqlSession.selectList(namespace + ".selectAuthorList");
	}

	// 메뉴 등록
	@Override
	public int insertMenu(ZValue zvl) throws Exception {
		return sqlSession.insert(namespace + ".insertMenu", zvl);
	}

	// 메뉴 목록
	@Override
	public ZValue selectMenu(ZValue zvl) throws Exception {
		return sqlSession.selectOne(namespace + ".selectMenu", zvl);
	}

	// 상위메뉴권한 체크 
	@Override
	public List<ZValue> selectDistinctAuthorId(ZValue zvl) throws Exception {
		return sqlSession.selectList(namespace + ".selectDistinctAuthorId", zvl);
	}

	// 상위메뉴 ID
	@Override
	public int selectUpperMenuId(ZValue zvl) throws Exception {
		return sqlSession.selectOne(namespace + ".selectUpperMenuId", zvl);
	}

	// 메뉴 삭제
	@Override
	public void deleteMenu(int deleteMenuId) throws Exception {
		sqlSession.delete(namespace + ".deleteMenu", deleteMenuId);	
	}

	// 메뉴조회
	@Override
	public Menu selectOneRow(int seq) {
		return sqlSession.selectOne(namespace + ".selectOneRow", seq);
	}

	// 메뉴권한
	@Override
	public List<MenuMap> selectAuthorId(int seq) throws Exception {
		return sqlSession.selectList(namespace + ".selectAuthorId", seq);
	}
	
	// 메뉴 수정	
	@Override
	public int updateMenu(ZValue zvl) throws Exception {
		return sqlSession.update(namespace + ".updateMenu", zvl);
	}

	// 메뉴맵핑 삭제
	@Override
	public void deleteAuthorMenuMap(ZValue zvl) throws Exception {
		sqlSession.delete(namespace + ".deleteAuthorMenuMap", zvl);
	}
	
	// 메뉴 권한 메핑 등록
	@Override
	public int insertAuthorMenuMap(ZValue zvl) throws Exception {
		return sqlSession.insert(namespace + ".insertAuthorMenuMap", zvl);
	}
}
