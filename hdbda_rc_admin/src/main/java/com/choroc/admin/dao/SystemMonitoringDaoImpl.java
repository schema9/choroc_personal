package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.MonitoringParam;
import com.choroc.admin.dto.Monitoring;
import com.choroc.admin.dto.Scheduler;

@Repository
public class SystemMonitoringDaoImpl implements SystemMonitoringDao{
	
	@Inject
	private SqlSession sqlSession;
	
	private static final String namespace = "system.monitorMapper";
	
	@Override
	public List<Monitoring> selectMonitoringList(MonitoringParam param) throws SQLException {
		return sqlSession.selectList(namespace + ".selectMonitoringList", param);
	}
	
	@Override
	public int selectMonitoringTotalCount(MonitoringParam param) throws SQLException {
		return sqlSession.selectOne(namespace + ".selectMonitoringTotalCount", param);
	}
	
	@Override
	public Scheduler schedulerDetailView(MonitoringParam param) throws SQLException {
		return sqlSession.selectOne(namespace + ".schedulerDetailView", param);
	}

	
}
