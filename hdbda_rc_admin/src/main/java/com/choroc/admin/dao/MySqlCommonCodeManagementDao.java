package com.choroc.admin.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MySqlCommonCodeManagementDao {

	private static Logger log = LoggerFactory.getLogger(MySqlCommonCodeManagementDao.class);
	
	/*
	 *  맵퍼파일 호출
	 */
	
	/*
	protected SqlSessionTemplate sqlMapClient;

	public void setSqlMapClient(SqlSessionFactory sqlMapClient) {
		this.sqlMapClient = new SqlSessionTemplate(sqlMapClient);
	}

	@Override
	public List<CommonCodeInfo> selectCommonCodeRows(CommonCodeParam param) throws SQLException {
		return (List<CommonCodeInfo>) this.queryForList("admin.common.selectCommonCodeRows", param, sqlMapClient);
	}

	@Override
	public CommonCodeInfo selectCommonCodeRow(Map<String, Object> param) throws SQLException {
		return (CommonCodeInfo) this.queryForObject("admin.common.selectCommonCodeRow", param, sqlMapClient);
	}

	@Override
	public int selectCommonCodeTotalCount(CommonCodeParam param) throws SQLException {
		return (int) this.queryForObject("admin.common.selectCommonCodeTotalCount", param, sqlMapClient);
	}

	@Override
	public int selectCommonCodeCountForValidKey(Map<String, Object> param) throws SQLException {
		return (int) this.queryForObject("admin.common.selectCommonCodeCountForValidKey", param, sqlMapClient);
	}

	@Override
	public int selectCommonCodeCountForUsing(String code) throws SQLException {
		return (int) this.queryForObject("admin.common.selectCommonCodeCountForUsing", code, sqlMapClient);
	}

	@Override
	public List<CommonCodeInfo> selectCommonGroupRows(String groupCode) throws SQLException {
		return (List<CommonCodeInfo>) this.queryForList("admin.common.selectCommonGroupRows", groupCode, sqlMapClient);
	}

	@Override
	public int insertCommonCodeRow(CommonCodeInfo param) throws SQLException {
		return this.insert("admin.common.insertCommonCodeRow", param, sqlMapClient);
	}

	@Override
	public int updateCommonCodeRow(CommonCodeInfo param) throws SQLException {
		return this.update("admin.common.updateCommonCodeRow", param, sqlMapClient);
	}

	@Override
	public int deleteCommonCodeRow(CommonCodeInfo param) throws SQLException {
		return this.update("admin.common.deleteCommonCodeRow", param, sqlMapClient);
	}
	*/
}
