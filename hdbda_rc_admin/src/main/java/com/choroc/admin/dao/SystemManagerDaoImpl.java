package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.domain.SystemManagerMultiParam;
import com.choroc.admin.domain.SystemManagerParam;
import com.choroc.admin.dto.AccountInfo;
import com.choroc.admin.dto.Scheduler;
import com.choroc.admin.dto.SystemManagerInfo;

/**
 * TODO > 시스템 관리 > 메타 관리
 * 파일명   > UserAccountManagementDaoImpl.java
 * 작성일   > 2018. 11. 21.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
@Repository
public class SystemManagerDaoImpl implements SystemManagerDao{

	@Inject
	private SqlSession sqlSession;	
	
	private static Logger log = LoggerFactory.getLogger(SystemManagerDaoImpl.class);
	
	@Override
	public List<SystemManagerInfo> selectSystemManagerRows(SystemManagerParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.selectList("systemManager.Meta.Mapper.selectMetaDataList", param);
	}
	
	@Override
	public int selectSystemManagerTotalCount(SystemManagerParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("systemManager.Meta.Mapper.selectMetaDataTotalCount", param);
	}
	
	@Override
	public int insertMetaDataTable(SystemManagerParam param){
		// TODO Auto-generated method stub
		return sqlSession.insert("systemManager.Meta.Mapper.insertMetaDataTable", param);
	}
	@Override
	public int insertMetaDataColumnRows(Map<String, Object> listMap){
		// TODO Auto-generated method stub
		return sqlSession.insert("systemManager.Meta.Mapper.insertMetaDataColumnRows", listMap);
	}
	
	@Override
	public int updateMetaDataTable(SystemManagerParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.update("systemManager.Meta.Mapper.updateMetaDataTable", param);
	}
	@Override
	public int updateMetaDataColumnRows(Map<String, Object> listMap) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.update("systemManager.Meta.Mapper.updateMetaDataColumn", listMap);
	}
	
	@Override
	public int deleteMetaDataTable(SystemManagerParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.delete("systemManager.Meta.Mapper.deleteMetaDataTable", param);
	}
	@Override
	public int deleteMetaDataColumn(Map<String, Object> listMap) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.delete("systemManager.Meta.Mapper.deleteMetaDataCalumn", listMap);
	}
	@Override
	public int deleteMetaDataColumnRows(SystemManagerParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.delete("systemManager.Meta.Mapper.deleteMetaDataCalumnAll", param);
	}
	@Override
	public List<SystemManagerInfo> selectMetaDataDetail(SystemManagerParam param) throws SQLException {
		// TODO Auto-generated method stub
		return sqlSession.selectList("systemManager.Meta.Mapper.selectMetaDataDetail", param);
	}
	
	@Override
	public int jobLstChgDatCondNull(String etl_tgt_tbl) throws SQLException {
		return sqlSession.update("systemManager.Meta.Mapper.jobLstChgDatCondNull", etl_tgt_tbl);
	}
	
	@Override
	public Scheduler jobInfo(String etl_tgt_tbl) throws SQLException{
		return sqlSession.selectOne("systemManager.Meta.Mapper.jobInfo", etl_tgt_tbl);
	}
}
