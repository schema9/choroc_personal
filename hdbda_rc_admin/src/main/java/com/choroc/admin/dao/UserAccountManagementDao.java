package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;

import com.choroc.admin.domain.AccountParam;
import com.choroc.admin.dto.AccountInfo;
import com.choroc.admin.util.ZValue;

/**
 * TODO > 권한 관리 > 사용자 관리 > 사용자 계정 관리
 * 파일명   > UserAccountManagementDao.java
 * 작성일   > 2018. 11. 21.
 * 작성자   > CCmedia Service Corp.
 * 수정일   > 
 * 수정자   > 
 */
public interface UserAccountManagementDao {

	// 사용자가 가입 후 또는 비밀번호 변경 후 최초 로그인일 경우 비밀번호를 변경한다.
    public int updateFirstLoginPassword(AccountParam param) throws SQLException;
    public List<AccountInfo> selectUserAccountRows(AccountParam param) throws SQLException;
    public int selectUserAccountTotalCount(AccountParam param) throws SQLException;
	public int selectAccountUserIdCountForValidKey(AccountParam param) throws SQLException;
	public List<AccountInfo> selectAuthCd(AccountParam param) throws SQLException;
	public int createAccountUser(AccountParam param) throws SQLException;
	public int createAccountUserMap(AccountParam param) throws SQLException;
	public int userPwdReset(AccountParam param) throws SQLException;
	public int userModifySet(AccountParam param) throws SQLException;
	public int userModifySetMap(AccountParam param) throws SQLException;
	public int selectPasswordCheck(AccountParam param) throws SQLException;
	public int updateUserPassword(AccountParam param) throws SQLException;
	public int updateUserPasswordHis(AccountParam param) throws SQLException;
	public int selectUserPwdHis(AccountParam param) throws SQLException;
	
	// 사용자 최초 로그인 비밀번호 변경
	public int modifyFirstLoginPassword(ZValue zValue) throws Exception;
	
	// 비밀변경 변경시 이력 저장
	public void insertUserPasswordHis(ZValue zValue) throws Exception;
}
