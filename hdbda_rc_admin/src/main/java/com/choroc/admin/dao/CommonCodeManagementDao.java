package com.choroc.admin.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.choroc.admin.domain.CommonCodeParam;
import com.choroc.admin.dto.CommonCodeInfo;

public interface CommonCodeManagementDao {
	
	public List<CommonCodeInfo> selectCommonCodeRows(CommonCodeParam param) throws SQLException;

	public CommonCodeInfo selectCommonCodeRow(Map<String, Object> param) throws SQLException;

	public int selectCommonCodeTotalCount(CommonCodeParam param) throws SQLException;

	public int selectCommonCodeCountForValidKey(Map<String, Object> param) throws SQLException;

	public int selectCommonCodeCountForUsing(String param) throws SQLException;

	public List<CommonCodeInfo> selectCommonGroupRows(String groupCode) throws SQLException;

	public int insertCommonCodeRow(CommonCodeInfo param) throws SQLException;

	public int updateCommonCodeRow(CommonCodeInfo param) throws SQLException;

	public int deleteCommonCodeRow(CommonCodeInfo param) throws SQLException;
}
