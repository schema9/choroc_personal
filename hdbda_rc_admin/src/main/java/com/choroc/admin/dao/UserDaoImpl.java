package com.choroc.admin.dao;

import java.sql.SQLException;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.choroc.admin.domain.AuthUserParam;
import com.choroc.admin.dto.AuthUser;
import com.choroc.admin.util.ZValue;

/**
 * TODO > 맵퍼파일 - mappers\authUser\AuthUserMapper.xml 
 * 파일명   > UserDaoImpl.java
 * 작성일   > 2018. 11. 20.
 * 작성자   > Copyright CCmediaService
 * 수정일   > 
 * 수정자   > 
 */
@Repository
public class UserDaoImpl implements UserDao {


	@Inject
	private SqlSession sqlSession;	
	
	private static final String namespace = "authUser.AuthUserMapper";
	
	// 로그인 유저 조회
	@Override
	public AuthUser selectLoginUser(AuthUserParam param) throws SQLException {
		
		return sqlSession.selectOne(namespace + ".getLoginUser", param);
	}

	// 로그인 성공시 정보 업데이트	
	@Override
	public void loginSuccessProcess(ZValue zvl) throws SQLException {
		sqlSession.update(namespace + ".loginSuccessProcess", zvl);
	}
	
	// 로그인 성공시 정보 업데이트(마지막 로그인 날짜 제외)
	@Override
	public void loginSuccessProcessExcludeLoginDt(ZValue zvl) throws SQLException {
		sqlSession.update(namespace + ".loginSuccessProcessExcludeLoginDt", zvl);
	}

	// 로그인 실패횟수 업데이트 
	@Override
	public int loginFaileCount(ZValue zvl) throws SQLException {
		
		return sqlSession.update(namespace + ".loginFaileCount", zvl);
	}



	// 계정 잠금 및 실패 횟수 초기화
	@Override
	public int acntLockLimitExpirationProcess(ZValue zvl) throws SQLException {
		
		return sqlSession.update(namespace + ".acntLockLimitExpirationProcess", zvl);
	}
	

}
